<?php
use Illuminate\Support\Facades\Broadcast;
use App\Models\Tickets\Ticket;
/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{idUsuario}', function ($user, $idUsuario) {
    return (int) $user->idUsuario === (int) $idUsuario;
});

Broadcast::channel('user.{id}', function ($user, $id) {
    return (int) $user->idUsuario === (int) $id;
});

Broadcast::channel('chat', function ($user) {
    return [
        'id' => $user->idUsuario
    ];
});

Broadcast::channel('users.{id}', function ($user, $id) {
    return (int) $user->idUsuario === (int) $id;
});

Broadcast::channel('tickets.{ticket}', function ($user, Ticket $ticket) {
    return [
        'id' => $user->idUsuario
    ];
});
