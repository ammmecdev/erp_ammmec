<?php

Route::get('/', 'Auth\LoginController@showLoginForm');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group( ['middleware' => 'auth' ], function()
{
Route::get('/modulos', function () {
	return view('modulos.modules');
})->name('modulos');

//ACTIVIDADES

Route::post('/api/vt/actividades', 'ActividadController@store');

//NOTAS

Route::post('/api/vt/notas/store', 'NotaLeadController@store');
Route::post('/api/vt/comentarios/store', 'ComentarioNotaLeadController@store');

// ROUTES VENTAS

Route::get('/ventas', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('ventas');

//VENTAS - NEGOCIOS

Route::get('/ventas/negocios/seguimiento', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.negocios.seguimiento');

Route::get('/ventas/negocios/embudos', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.negocios.embudos');

Route::get('/ventas/negocios/lista', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.negocios.lista');

Route::get('ventas/negocios/notas', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.negocios.notas');

Route::get('ventas/negocios/notas/{id}', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.negocios.notas');

Route::get('ventas/negocios/{idNegocio}', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.negocios.detalles');

Route::get('ventas/negocios/detalles', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.negocios.detalles');

//NEGOCIOS DETALLES CONTENIDOS
//
Route::post('api/vt/negocios/detalles/notas', 'ContenidosController@store');


//QUITAR
Route::get('ventas/servicios', function () {
	return view('layouts.app');
})->name('vt.servicios');

Route::get('api/vt/servicios', 'NegocioController@getNegociosServicios');

Route::post('api/vt/servicios', 'NegocioController@setClaves');

 //CERRAR SESION
Route::get('logout','Auth\LoginController@logout')->name('logout');

//NEGOCIOS
Route::post('api/vt/negocios', 'NegocioController@store');

	//UPDATE EN SEGUIMIENTO
Route::put('api/vt/seguimiento/negocios',  //UPDATE NEGOCIOS DESDE SEGUIMIENTO
	'NegocioController@update');

Route::put('api/vt/seguimiento/costo',  //UPDATE COSTO DESDE SEGUIMIENTO
	'CostoController@update');

Route::put('api/vt/seguimiento/cotizacion',  //UPDATE COTIZACIÓN DESDE SEGUIMIENTO
	'CotizacionController@update');

Route::put('api/vt/seguimiento/pago',  //UPDATE PAGO DESDE SEGUIMIENTO
	'PagoController@update');

Route::put('api/vt/seguimiento/factura',  //UPDATE FACTURA DESDE SEGUIMIENTO
	'FacturaController@update');

Route::put('api/vt/negocios/tipomoneda', 
	'NegocioController@update_moneda');


	//UPDATE - EVALUACIÓN DE SERVICIO 
Route::put('api/vt/indicadores/evaluacion/pregunta',   //VALOR
	'EvaluacionPreguntaController@update');	

Route::put('api/vt/indicadores/evaluacion',   //VALOR
	'EvaluacionServicioController@update');	


	//UPDATE - PRESUPUESTO
Route::put('api/vt/presupuesto/presupuestosMeses', 'PresupuestoController@update_presupuestoMes');

	//UPDATE - CLAVE DE NEGOCIO
Route::put('api/vt/negocios/clave', 'NegocioController@update_clave');

	//UPDATE - CAMBIO DE EMBUDO
Route::put('api/vt/negocios/{negocio}/embudos', 'NegocioController@update_embudo_negocio');

//VENTAS - ACTIVIDADES

Route::get('ventas/actividades/{idNotification}', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.actividades');

Route::get('ventas/actividades/{actividades}', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.actividades');

Route::get('ventas/actividades', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.actividades');

//VENTAS - CLIENTES

Route::get('ventas/clientes', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.clientes');

//VENTAS - CONTACTOS

Route::get('ventas/contactos', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.contactos');

//VENTAS - PRESUPUESTO

Route::get('ventas/presupuesto', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.preupuesto');

//INDICADORES

	//Clientes facturados
Route::get('ventas/indicadores/clientes-facturados', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.indicadores.clientes-facturados');

Route::delete('vt/indicadores/clienteFacturado/{cliente}', 'ClienteFacturadoController@delete');

	//Cartera vencida
Route::get('ventas/indicadores/cartera-vencida', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.indicadores.cartera-vencida');

	//crecimiento
Route::get('ventas/indicadores/porcentaje-crecimiento', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.indicadores.crecimiento');

	//Porcentaje de ventas
Route::get('ventas/indicadores/porcentaje-ventas', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.indicadores.porcentaje-ventas');

	//Evaluación de servicio
Route::get('ventas/indicadores/evaluacion-servicio', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.indicadores.evaluacion-servicio');

//VENTAS - CONFIGURACION

//EMBUDOS

Route::get('ventas/configuracion/embudos', function () {
	$module = "ventas";
	return view('layouts.app', compact('module'));
})->name('vt.configuracion.embudos');


//NOTIFICACIONES

Route::put('api/notification', 'NotificationsController@read');

Route::get('api/notifications', 'NotificationsController@getNotifications');


//COSTO OPERATIVO

Route::get('/ventas/costos', function () {
	$navbar = 'modulos.ventas.navbar';
	$nav_link = 'costos';
	return view('modulos.ventas.costos.index', compact('navbar', 'nav_link'));
})->name('ventas.costos.index');

Route::get('/ventas/cotizaciones', function () {
	$navbar = 'modulos.ventas.navbar';
	$nav_link = 'cotizaciones';
	return view('modulos.ventas.cotizaciones.index', compact('navbar', 'nav_link'));
})->name('ventas.cotizaciones.index');

// ROUTES SERVICIOS

Route::get('/servicios', function () {
	$navbar = 'modulos.services.navbar';
	$nav_link = '';
	return view('modulos.services.start', compact('navbar', 'nav_link'));
})->name('services');

Route::get('/servicios/cotizaciones', function () {
	$navbar = 'modulos.services.navbar';
	$nav_link = 'costs';
	return view('modulos.services.costs.index', compact('navbar', 'nav_link'));
})->name('costs.index');

Route::get('/servicios/cotizaciones/servicio', function (){
	$navbar = 'modulos.services.navbar';
	$nav_link = 'costs';
	$side_link = 'service';
	return view('modulos.services.costs.service', compact('navbar', 'nav_link', 'side_link'));
})->name('costs.service');

Route::get('/servicios/cotizaciones/cuentaplaneada', function (){
	$navbar = 'modulos.services.navbar';
	$nav_link = 'costs';
	$side_link = 'plannedAccount';
	return view('modulos.services.costs.plannedAccount', compact('navbar', 'nav_link', 'side_link'));
})->name('costs.plannedAccount');

Route::get('/cuentareal', 'CostoController@realAccount')
->name('costs.realAccount');

Route::get('/productividad', 'CostoController@productivity')
->name('costs.productivity');

Route::get('servicios/configuracion/precios', function (){
	$navbar = 'modulos.services.navbar';
	$nav_link = '';
	$side_link = 'prices';
	return view('modulos.services.configuration.prices', compact('navbar', 'nav_link', 'side_link'));
})->name('configuration.prices');

//SERVICIOS NUEVOS


Route::get('/servicios', function (){
	$module = 'servicios';
	return view('layouts.app', compact('module'));
})->name('servicios');

//SERVICIOS - ACTIVIDADES

Route::get('/servicios/actividades', function (){
	$module = 'servicios';
	return view('layouts.app', compact('module'));
})->name('servicios.actividades');


Route::get('/servicios/configuracion/semanas', function (){
	$module = 'servicios';
	return view('layouts.app', compact('module'));
})->name('servicios.configuracion.semanas');

Route::get('/servicios/actividades/reporte/{idReporte}','ReporteActividadController@showReport');

Route::get('/servicios/actividades/cuentas/{idSemana}','ReporteActividadController@generateReportCuentasRegistradas');


//SISTEMAS
//
Route::get('/sistemas', function (){
	$module = 'sistemas';
	return view('layouts.app', compact('module'));
})->name('sistemas');

Route::get('/sistemas/admin', function (){
	$module = 'sistemas';
	return view('layouts.app', compact('module'));
})->name('sistemas.admin');

Route::get('/sistemas/admin/cuentas', function (){
	$module = 'sistemas';
	return view('layouts.app', compact('module'));
})->name('sistemas.admin.cuentas');

Route::get('/sistemas/admin/licencias', function (){
	$module = 'sistemas';
	return view('layouts.app', compact('module'));
})->name('sistemas.admin.licencias');

Route::get('/sistemas/admin/equipos', function (){
	$module = 'sistemas';
	return view('layouts.app', compact('module'));
})->name('sistemas.admin.equipos');


//ROUTES NORMATIVIDAD

Route::get('/calidad', function () {
	$module = "calidad";
	return view('layouts.app', compact('module'));
})->name('calidad');

Route::get('/calidad/procesos', function () {
	$module = "calidad";
	return view('layouts.app', compact('module'));
});

Route::get('/calidad/procesos/{departamento}', function () {
	$module = "calidad";
	return view('layouts.app', compact('module'));
});

Route::get('/calidad/organigrama', function () {
	$module = "calidad";
	return view('layouts.app', compact('module'));
});

Route::get('/calidad/organizacion', function () {
	$module = "calidad";
	return view('layouts.app', compact('module'));
});

});