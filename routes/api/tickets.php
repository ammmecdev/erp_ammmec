<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//TICKETS

Route::get('tickets/{ticket}', 'TicketController@getTicketById');
Route::get('tickets/{ticket}/messages', 'TicketController@getMessagesByTicket');
Route::get('tickets/{ticket}/participants', 'TicketController@getUsersByTicket');
Route::get('tickets/{ticket}/files', 'TicketController@getFilesByTicket');
Route::post('tickets/{ticket}/participants', 'TicketController@addParticipants');
Route::get('files/{file}', 'FileController@downloadFile');
Route::get('tickets/user/{user}', 'TicketController@getTicketsByUser');
Route::get('tickets/user/{user}/admin', 'TicketController@getTicketsByUserAdmin');

//edit service and subject
Route::put('tk/service/subject', 'TicketController@updateTkServiceSubject');
//escalar ticket
Route::put('tk/escalate', 'TicketController@escalateTicket');
//close ticket
Route::put('tk/status', 'TicketController@closeTicket');

// INDICADORES
// Productividad
Route::get('tk/indicators/productivity', 'IndicatorController@getTicketsProductivity');
// Por servicios
Route::get('tk/indicators/services', 'IndicatorController@getTicketsByServices');
// Por subjects
Route::get('tk/indicators/subjects', 'IndicatorController@getTicketsBySubjects');
// Por Area
Route::get('tk/indicators/departaments', 'IndicatorController@getTicketsByDepartament');
// Por attention time
Route::get('tk/indicators/attentionTime', 'IndicatorController@getAttentionTime');

//DEPARTAMENTOS
Route::get('departaments/admins', 'AreaController@getAreasAdmins');
// Route::get('departaments/{area}/tickets', 'AreaController@getTickets');

//SERVICIOS
Route::get('services/departaments/{id}', 'ServiceController@getServicesByDepartament');
Route::get('tk/service/users/{id}', 'ServiceController@getUsersService');
Route::put('tk/services/status', 'ServiceController@updateStatus');
Route::put('tk/service', 'ServiceController@update');
Route::post('tk/service', 'ServiceController@store');
Route::put('tk/service/user', 'ServiceController@detachUser');
Route::post('tk/service/user', 'ServiceController@attachUser');
Route::get('tk/services/users/departament/{id}', 'ServiceController@getUsersByDepartament');


//ASUNTOS
Route::get('subjects/service/{id}', 'SubjectController@getSubjectsByService');
Route::delete('tk/subject/{id}', 'SubjectController@delete');
Route::put('tk/subject', 'SubjectController@store');

