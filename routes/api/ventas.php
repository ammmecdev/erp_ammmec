<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//VENTAS

//ACTIVIDADES

Route::post('/api/vt/actividades', 'ActividadController@store');

Route::get(
    'vt/actividades', 
    'ActividadController@getActividades'
);

Route::put(
    'vt/actividades', 
    'ActividadController@updateActividades'
);

Route::put(
    'vt/actividades/{actividad}/completadas', 
    'ActividadController@updateActividad'
);

Route::post(
    'vt/actividadesN', 
    'ActividadController@testActividad'
);

Route::delete(
    'vt/actividades/{id}', 
    'ActividadController@deleteActividades'
);

//NEGOCIOS

Route::get(
    'vt/negocios', 
    'NegocioController@getNegocios'
);

Route::get(
    'vt/negocio/{negocio}', 
    'NegocioController@getNegocio'
);

Route::get(
    'vt/negocios/join', 
    'NegocioController@getNegociosJoin'
);

Route::get(
    'vt/negocios/actividades', 
    'NegocioController@getNegociosActividades'
);

Route::get(
    'vt/negocios/{negocio}/actividades', 
    'NegocioController@getActividadesByNegocio'
);

Route::get(
    'vt/negocios/{negocio}', 
    'NegocioController@negocioById'
);

Route::post(
    'vt/negocios/estimacion', 
    'NegocioController@store_estimacion'
);

Route::get(
    'vt/negocios/select/estimaciones', 
    'NegocioController@selectEstimaciones'
);


Route::post(
    'vt/negocios/participantes', 
    'ContactoNegocioController@store'
);

Route::get(
    'vt/participantes/{negocio}', 
    'ContactoNegocioController@getParticipantes'
);

Route::post(
    'vt/negocios/seguidores', 
    'UsuarioNegocioController@store'
);

Route::get(
    'vt/seguidores/{negocio}', 
    'UsuarioNegocioController@getSeguidores'
);


/* Route::put(
    'vt/negocios/clave', 
    'NegocioController@update_clave'
); */

Route::get(
    'vt/negocios/clave/consecutivo',    
    'NegocioController@getConsecutivo'
);

Route::get(
    'vt/negocios/select/negocios', 
    'NegocioController@selectNegocios'
);

Route::get(
    'vt/negocios/facturados/negocios', 
    'NegocioController@getNegociosFacturados'
);

Route::get(
    'vt/negocio/{negocio}/historial', 
    'NegocioController@getBitacoras'
);

//REPORTE NEGOCIOS EXCEL
Route::post(
    'vt/negocios/create/excel', 
    'NegocioController@createReportExcel'
);
Route::get(
    'vt/negocios/export/excel', 
    'NegocioController@exportReportExcel'
);



//PRESUPUESTO



Route::post(
    'vt/presupuesto', 
    'Ventas\PresupuestoController@store'
);

Route::post(
    'vt/presupuesto/presupuestosMeses', 
    'Ventas\PresupuestoController@store_presupuestoMes'
);

Route::get(
    'vt/presupuesto', 
    'Ventas\PresupuestoController@getPresupuestos'
);

Route::get(
    'vt/presupuesto/join', 
    'Ventas\PresupuestoController@getPresupuestosJoin'
);

Route::get(
    'vt/presupuesto/byId/{presupuesto}', 
    'Ventas\PresupuestoController@presupuestoById'
);

Route::get(
    'vt/presupuesto/montoMeses', 
    'Ventas\PresupuestoController@getMontoMeses'
);

Route::get(
    'vt/presupuesto/meses', 
    'Ventas\PresupuestoController@getPresupuestosMeses'
);

Route::get(
    'vt/presupuesto/embudo/{idEmbudo}/meses',
    'Ventas\PresupuestoController@getPresupuestosMesesByEmbudo'
);

Route::delete(
    'vt/presupuesto/meses/{id}', 
    'Ventas\PresupuestoController@deletePresupuestoMes'
);

Route::delete(
    'vt/presupuesto/{id}', 
    'Ventas\PresupuestoController@delete'
);

// UNIDAD DE NEGOCIO
Route::get(
    'vt/unidad/select',
    'UnidadNegocioController@selectUnidad'
);

Route::get(
    'vt/unidades-negocio',
    'UnidadNegocioController@getUnidadesNegocio'
);

Route::post(
    'vt/unidades-negocio',
    'UnidadNegocioController@store'
);

Route::put(
    'vt/unidades-negocio/{unidadNegocio}',
    'UnidadNegocioController@update'
);

//CLIENTES FACTURADOS

Route::get(
    'vt/indicadores/clientesFacturados',
    'ClienteFacturadoController@getClientesFacturados'
);

Route::get(
    'vt/indicadores/clientesFacturados/select',
    'ClienteFacturadoController@selectClientesFacturados'
);

Route::post(
    'vt/indicadores/clienteFacturado', 
    'ClienteFacturadoController@store'
);

Route::get(
    'vt/indicadores/clienteFacturado/byId/{id}', 
    'ClienteFacturadoController@clinteFacturadoById'
);

// EVALUACIÓN DE SERVICIO

Route::get(
    'vt/indicadores/evaluaciones',
    'EvaluacionServicioController@getEvaluaciones'
);

Route::get(
    'vt/indicadores/preguntas',
    'PreguntaController@getPreguntas'
);

Route::get(
    'vt/indicadores/evaluaciones/preguntas',
    'EvaluacionPreguntaController@getEvaluacionesPreguntas'
);

Route::get(
    'vt/indicadores/evaluaciones/byId/{id}', 
    'EvaluacionServicioController@evaluacionById'
);

//EQUIPOS

Route::get(
    'vt/equipos/select',
    'EquipoController@selectEquipos'
);

//SERVICIOS

Route::get(
    'vt/servicios/select',
    'ServicioController@selectServicios'
);

Route::get(
    'vt/servicios/{servicio}',
    'ServicioController@servicioById'
);

//ETAPAS

Route::get(
    'vt/embudos/{embudo}/etapas', 
    'Ventas\EmbudoController@getEtapasByEmbudo'
);

Route::post(
    'vt/etapas',
    'Ventas\EtapaController@store'
);

Route::put(
    'vt/etapas/orden',
    'Ventas\EtapaController@updateOrden'
);

Route::put(
    'vt/etapas/{etapa}',
    'Ventas\EtapaController@update'
);

Route::delete(
    'vt/etapas/{etapa}',
    'Ventas\EtapaController@delete'
);

// Route::get(
// 'vt/etapas/select',
// 	'Ventas\EtapaController@selectEtapas'
//);

//CAUSAS

Route::get(
    'vt/causas/select',
    'CausaController@selectCausas'
);

Route::get(
    'vt/causas',
    'CausaController@getCausas'
);

//CLIENTES

Route::get(
    'vt/clientes', 
    'ClienteController@getClientes'
);

Route::post(
    'vt/clientes', 
    'ClienteController@store'
);

Route::put(
    'vt/clientes', 
    'ClienteController@update'
);

Route::get(
    'vt/clientes/select', 
    'ClienteController@selectClientes'
);

Route::get(
    'vt/clientes/like', 
    'ClienteController@selectClientesLike'
);

Route::get(
    'vt/sector/select', 
    'ClienteController@selectSector'
);

Route::get(
    'vt/clientes/{cliente}/contactos/select', 
    'ClienteController@selectContactosByIdCliente'
);

Route::delete(
    'vt/clientes/{cliente}', 
    'ClienteController@delete'
);

Route::get(
    'vt/clientes/clave', 
    'ClienteController@getClaveCliente'
);

Route::get(
    'vt/clientes/{cliente}', 
    'ClienteController@getClienteById'
);

//CONTACTOS

Route::get(
    'vt/contactos',
    'ContactoController@getContactos'
);

Route::get(
    'vt/contactos/{contacto}',
    'ContactoController@getContactoById'
);

Route::post(
    'vt/contactos',
    'ContactoController@store'
);

Route::delete(
    'vt/contactos/{id}',
    'ContactoController@deleteContactos'
);

Route::put(
    'vt/contactos',
    'ContactoController@updateContactos'
);

Route::get(
    'vt/contactos/select',
    'ContactoController@selectContactos'
);

Route::get(
    'vt/contactos/{contacto}/clientes/select',
    'ContactoController@selectClientesByIdContacto'
);

//EMBUDOS

Route::get(
    'vt/embudos/select',
    'Ventas\EmbudoController@selectEmbudos'
);

Route::get(
    'vt/embudos',
    'Ventas\EmbudoController@getEmbudos'
);

Route::post(
    'vt/embudos',
    'Ventas\EmbudoController@store'
);

Route::put(
    'vt/embudos/{embudo}',
    'Ventas\EmbudoController@update'
);

Route::delete(
    'vt/embudos/{embudo}',
    'Ventas\EmbudoController@delete'
);

//COSTOS

Route::post(
    'vt/costos', 
    'CostoController@store'
);
Route::get(
    'vt/costos', 
    'CostoController@getCostos'
);

//NOTAS
Route::get(
    'vt/notas', 
    'NotaLeadController@getNotas'
);
Route::get(
    'vt/actualizaciones/{comentario}', 
    'ComentarioNotaLeadController@getActualizaciones'
);
Route::get(
    'vt/notas/{nota}', 
    'NotaLeadController@selectNotasById'
);
Route::delete(
    'vt/notas/{nota}', 
    'NotaLeadController@deleteNota'
);
Route::put(
    'vt/notaslead', 
    'NotaLeadController@updateNota'
);
Route::put(
    'vt/notas', 
    'NegocioController@updateNota'
);
Route::put(
    'vt/fechaNota', 
    'NotaLeadController@updateFechaNota'
);
Route::get(
    'vt/comentarios/{nota}', 
    'NotaLeadController@getComentariosByIdNota'
);
Route::delete(
    'vt/comentarios/{comentario}', 
    'ComentarioNotaLeadController@deleteComentario'
);
Route::put(
    'vt/comentarios', 
    'ComentarioNotaLeadController@updateComentario'
);
Route::put(
    'vt/comentarioNegocio', 
    'ComentarioNotaLeadController@updateComentarioNegocio'
);
Route::get(
    'storage/{archivo}', 
    'ComentarioNotaLeadController@downloadFile'
);
//EVALUACIÓN DE SERVICIO
Route::post(
    'vt/indicadores/evaluacion', 
    'EvaluacionServicioController@store'
);


//PROYECCIONES

Route::get(
    'vt/proyecciones',
    'Ventas\ProyeccionController@getProyecciones'
);

Route::post(
    'vt/proyecciones',
    'Ventas\ProyeccionController@save'
);


Route::get(
    'vt/negocios/secciones/{seccion}/embudos/{embudo}',
    'NegocioController@getNegociosByCfCuentasExternas'
);