<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//SERVICIOS

//Reporte actividades

//ACTIVIDADES

Route::get(
    'op/ra/actividades',
    'RA\ActividadController@getActividadesAll'
);

Route::get(
    'op/ra/semanas/{idSemana}/cuentas/{tipoCuenta}', 
    'RA\ActividadController@getCuentasRegistradas'
);

Route::get(
    'op/ra/actividades/equipos/{idSemana}', 
    'RA\ActividadController@getEquiposRegistrados'
);

Route::get(
    'op/ra/actividades/{noEmpleado}/{fecha}',
    'RA\ActividadController@getActividades'
);

Route::post(
    'op/ra/actividades',
    'RA\ActividadController@store'
);

Route::put(
    'op/ra/actividades/{actividad}',
    'RA\ActividadController@update'
);

Route::delete(
    'op/ra/actividades/{actividad}',
    'RA\ActividadController@delete'
);
// =====================================================




//REPORTES

Route::get(
    'op/ra/reportes/empleado/{noEmpleado}', 
    'RA\ReporteController@getReportesByEmpleado'
);


Route::get(
    'op/ra/reportes/years/toexport',
    'RA\ReporteController@getYearsDisponiblesToExport'
);

Route::get(
    'op/ra/reportes/semanas/toexport/year/{year}', 
    'RA\ReporteController@getSemanasDisponiblesToExport'
);

Route::get(
    'op/ra/reportes/semana/{idSemana}/export', 
    'RA\ReporteController@exportExcelReport'
);

Route::get(
    'op/ra/reportes/th/semana/{idSemana}/export', 
    'RA\ReporteController@exportExcelReportForTH'
);

Route::post(
    'op/ra/reporte', 
    'RA\ReporteController@generateReport'
);

Route::get(
    'op/ra/reportes/semanas/empleado/{noEmpleado}/year/{year}', 
    'RA\ReporteController@getSemanasDisponibles'
);

Route::get(
    'op/ra/reporte/{idReporte}/download', 
    'RA\ReporteController@downloadReport'
);

Route::get(
    'op/ra/reportes/periodo/export',
    'RA\ReporteController@exportExcelPeriodo'
);
// =====================================================




//CONFIGURACION

//Semanas

Route::get(
    'op/ra/semana/{fecha}',
    'RA\SemanaController@getSemanaByFecha'
);

Route::get(
    'op/ra/configuracion/semanas/{year}', 
    'RA\SemanaController@getSemanas'
);

Route::put(
    'op/ra/configuracion/semanas/{semana}', 
    'RA\SemanaController@updateSemana')
    ;

Route::put(
    'op/ra/configuracion/semanas/activas/{semana}', 
    'RA\SemanaController@updateSemanaActiva'
);

Route::put(
    'op/ra/configuracion/semanas/activas/{semana}',
    'RA\SemanaController@updateSemanaActiva'
);

Route::get(
    'op/ra/indicadores/horas/optimas/{periodo}',
    'RA\HoraOptimaController@getHorasOptimas'
);

Route::post(
    '/op/ra/indicadores/horas/optimas',
    'RA\HoraOptimaController@store'
);

//Costos 

Route::post(
    'op/co/activo', 
    'CO\ConfiguracionController@storeActivo'
);

Route::get(
    'op/co/activo/categorias/select', 
    'CO\ConfiguracionController@getActivoCategorias'
);

Route::get(
    'op/co/activos',
    'CO\ConfiguracionController@getActivos'
);
// ===================================================




// COSTO OPERATIVO (COTIZADOR)
Route::post(
    'op/co/costo',
    'CO\CostoController@store'
);

Route::put(
    'op/co/costo/{costo}',
    'CO\CostoController@update'
);

Route::get(
    'op/co/costos',
    'CO\CostoController@getCostos'
);

Route::get(
    'op/co/costo/{costo}/factores',
    'CO\CostoController@getCostoFactores'
);

Route::get(
    'op/co/costo/{costo}/seguridad',
    'CO\CostoController@getCostoSeguridad'
);

Route::put(
    'op/co/costo/seguridad/{costoSeguridad}',
    'CO\CostoController@updateCostoSeguridad'
);

Route::get(
    'op/co/config/costo/etapa',
    'CO\CostoController@getConfCostoEtapa'
);

Route::delete(
    'op/co/costo/{costo}',
    'CO\CostoController@destroy'
);
// =====================================================



// COSTO OPERATIVO - CUENTA PLANEADA

//Mano de Obra
Route::get(
    'op/co/costo/{costo}/manoObra',
    'CO\CostoController@getManoObra'
);

Route::put(
    'op/co/costo/manoObra/{manoObra}',
    'CO\CostoController@updateManoObra'
);

//Herramienta y equipos
Route::get(
    'op/co/costo/{costo}/herramientaEquipo',
    'CO\CostoController@getHerramientaEquipo'
);

Route::put(
    'op/co/costo/herramientaEquipo/{herramientaEquipo}',
    'CO\CostoController@updateHerramientaEquipo'
);

//gastos especiales
Route::get(
    'op/co/costo/{costo}/gastoEspecial',
    'CO\CostoController@getGastoEspecial'
);

Route::put(
    'op/co/gastoEspecial/{gastoEspecial}',
    'CO\CostoController@updateGastoEspecial'
);

//gastos de Operación
Route::get(
    'op/co/costo/cuentaContableViaticos',
    'CO\CostoController@getCuentaContableViaticos'
);

Route::get(
    'op/co/costo/{costo}/gastoOperacion',
    'CO\CostoController@getGastoOperacion'
);

Route::put(
    'op/co/gastoOperacion/{gastoOperacion}',
    'CO\CostoController@updateGastoOperacion'
);

//observaciones
Route::post(
    'op/co/observacion',
    'CO\CostoController@storeObservacion'
);

Route::get(
    'op/co/costo/{costo}/observaciones',
    'CO\CostoController@getObservaciones'
);

Route::put(
    'op/co/observacion/{observacion}',
    'CO\CostoController@updateObservacion'
);

Route::delete(
    'op/co/observacion/{observacion}',
    'CO\CostoController@destroyObservacion'
);

//materiales e insumos
Route::get(
    'op/co/costo/{costo}/materialInsumo',
    'CO\CostoController@getMaterialInsumo'
);

Route::put(
    'op/co/materialInsumo/{materialInsumo}',
    'CO\CostoController@updateMaterialInsumo'
);

// Reporte cuenta planeada
Route::post(
    'op/co/cp/reporte/upload',
    'CO\CostoController@generateReport'
);

Route::get(
    'op/co/cp/reporte/{name}/download',
    'CO\CostoController@downloadReport'
);

// =====================================================


// COSTO OPERATIVO - CUENTA REAL

Route::post( //Copiar cuenta planeada
    'op/co/costo/cuenta/copy',
    'CO\CostoCuentaRealController@copyCuenta'
);

Route::post( //Copiar Herramienta y equipo para cuenta real
    'op/co/costo/herramientaEquipo/cuentaReal/copy',
    'CO\CostoCuentaRealController@copyHerramientaCR'
);

Route::get( // Horas registradas en Reporte de actividades
    'op/co/cr/manoObra/{cuenta}',
    'CO\CostoCuentaRealController@getManoObra'
);

Route::get( // Sueldos por horas RH
    'op/co/rh/sueldos',
    'CO\CostoCuentaRealController@getSueldos'
);

Route::get( //Gastos registrados en Comprobación de gastos
    'op/co/cr/gastosOperacion/{cuenta}',
    'CO\CostoCuentaRealController@getGastosOperacion'
);

Route::get(
    'op/co/{costo}/cr/materialInsumo',
    'CO\CostoCuentaRealController@getMaterialInsumo'
);

Route::get(
    'op/co/{costo}/cr/herramientaEquipo',
    'CO\CostoCuentaRealController@getHerramientaEquipo'
);

Route::get(
    'op/co/{costo}/cr/gastoEspecial',
    'CO\CostoCuentaRealController@getGastoEspecial'
);










