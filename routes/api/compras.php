<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// SOLICITUD DE SUMINISTROS

// Todas
// Route::get(
//     'co/ss/solicitudes',
//     'SS\SolicitudController@getSolicitudesAll'
// );

// Por usuario
// Route::get(
//     'co/ss/solicitudes/usuarios/{idUsuario}',
//     'SS\SolicitudController@getSolicitudesByUsuario'
// );

// Por autorizar
// Route::get(
//     'co/ss/solicitudes/autorizador/{idUsuario}',
//     'SS\SolicitudController@getSolicitudesByAutorizador'
// );

// Route::get(
//     'co/ss/solicitud/{solicitud}/articulos',
//     'SS\SolicitudController@getArticulos'
// );

// Route::get(
//     'co/ss/etapas',
//     'SS\SolicitudController@getEtapas'
// );

// Route::post(
//     'co/ss/solicitudes',
//     'SS\SolicitudController@store'
// );

// Route::put(
//     'co/ss/solicitud/{solicitud}/etapas/{idEtapa}',
//     'SS\SolicitudController@cambioEtapa'
// );

// Route::get(
//     'co/ss/solicitudes/cuentas/{tipo}',
//     'SS\SolicitudController@getCuentasBySolicitud'
// );

// Route::post(
//     'co/ss/solicitud/upload',
//     'SS\SolicitudController@generateSolicitud'
// );

// Route::get(
//     'co/ss/solicitud/{name}/download',
//     'SS\SolicitudController@downloadSolicitud'
// );


// VALE DE SALIDA
// Route::post(
//     'co/ss/vale/salida/upload',
//     'SS\SolicitudController@generateVale'
// );

// Route::get(
//     'co/ss/vale/salida/{name}/download',
//     'SS\SolicitudController@downloadVale'
// );


//CONFIGURACIÓN

Route::get(
    'co/proveedores',
    'Configuracion\ProveedorController@getProveedores'
);

Route::post(
    'co/proveedor',
    'Configuracion\ProveedorController@store'
);

Route::put(
    'co/proveedor/{proveedor}',
    'Configuracion\ProveedorController@update'
);

Route::delete(
    'co/proveedor/{proveedor}',
    'Configuracion\ProveedorController@destroy'
);

Route::get(
    'co/proveedores/exportar',
    'Configuracion\ProveedorController@exportExcel'
);


// ORDEN DE COMPRA

// Route::post(
//     'co/ss/ordencompra',
//     'SS\OrdenCompraController@store'
// );

// Route::get(
//     'co/ss/solicitud/{solicitud}/ordenescompra',
//     'SS\OrdenCompraController@getOrdenesCompra'
// );

// Route::get(
//     'co/ss/ordencompra/{ordenCompra}/articulos',
//     'SS\OrdenCompraController@getArticulosByOrdenCompra'
// );

// Route::put(
//     'co/ss/ordencompra/{orden}/autorizacion',
//     'SS\OrdenCompraController@autorizacionOrdenCompra'
// );

// Route::put(
//     'co/ss/ordencompra/{orden}/metodoPago',
//     'SS\OrdenCompraController@updateMetodoPago'
// );

// Route::post(
//     'co/ss/ordencompra/upload',
//     'SS\OrdenCompraController@generateOrdenCompra'
// );

// Route::get(
//     'co/ss/ordencompra/{name}/download',
//     'SS\OrdenCompraController@downloadOrdenCompra'
// );

// Route::get(
//     'co/ss/ordencompra/{orden}',
//     'SS\OrdenCompraController@getOrdenCompra'
// );


// ORDEN DE COMPRA - RECEPCIÓN DE MERCANCIA

// Route::post(
//     'co/ss/ordencompra/recepcion',
//     'SS\RecepcionMercanciaController@store'
// );

// Route::get(
//     'co/ss/ordencompra/{orden}/recepciones',
//     'SS\RecepcionMercanciaController@getRecepcionMercanciasByOrdenCompra'
// );

// Route::put(
//     'co/ss/ordencompra/recepcion/articulos',
//     'SS\RecepcionMercanciaController@reassignArticulos'
// );

// Route::get(
//     'co/ss/ordencompra/{orden}/recepcion/articulos',
//     'SS\RecepcionMercanciaController@getRecepcionArticulosByOrdenCompra'
// );

// Route::get(
//     'co/ss/ordencompra/factura/{file}/download',
//     'SS\RecepcionMercanciaController@downloadFile'
// );


// COMENTARIOS
// Route::post(
//     'co/ss/comentarios',
//     'SS\ComentarioController@store'
// );

// Route::get(
//     'co/ss/ordencompra/{orden}/comentario',
//     'SS\ComentarioController@getComentarioByOrdenCompra'
// );

// ARTICULOS
// Route::put(
//     'co/ss/articulo/{articulo}',
//     'SS\SolicitudController@updateArticulo'
// );

// Route::put(
//     'co/ss/articulo/{articulo}/cantidad',
//     'SS\SolicitudController@updateArticuloCantidad'
// );

// Route::put(
//     'co/ss/articulo/{articulo}/output',
//     'SS\SolicitudController@outputArticulo'
// );









// AUTORIZACIONES - PENDIENTES

Route::get(
    'co/sc/solicitudes/autorizaciones',
    'SC\AutorizacionController@getAutorizaciones'
);

Route::put(
    'co/sc/solicitudes/autorizacion/{autorizacion}',
    'SC\AutorizacionController@update'
);


// SOLICITUD DE COMPRA ----------------------------------

// Todas
Route::get(
    'co/sc/solicitudes',
    'SC\SolicitudController@getSolicitudesAll'
);

// Por autorizar
Route::get(
    'co/sc/solicitudes/autorizador/{idUsuario}',
    'SC\SolicitudController@getSolicitudesByAutorizador'
);

Route::put(
    'co/sc/solicitud/{solicitudCompra}/etapas/{idEtapa}',
    'SC\SolicitudController@cambioEtapa'
);

//etapas 
Route::get(
    'co/sc/etapas',
    'SC\SolicitudController@getEtapas'
);

// SOLICITUD DE COMPRA - ORDEN DE COMPRA

Route::get(
    'co/sc/solicitud/{solicitud}/ordenescompra',
    'SC\OrdenCompraController@getOrdenesCompra'
);

Route::get(
    'co/sc/ordencompra/{ordenCompra}',
    'SC\OrdenCompraController@getOrdenCompra'
);

Route::get(
    'co/sc/ordencompra/{ordenCompra}/articulos',
    'SC\OrdenCompraController@getArticulosByOrdenCompra'
);

Route::post(
    'co/sc/ordencompra',
    'SC\OrdenCompraController@store'
);

Route::put(
    'co/sc/ordencompra/{orden}',
    'SC\OrdenCompraController@update'
);

Route::put(
    'co/sc/ordencompra/{orden}/metodoPago',
    'SC\OrdenCompraController@updateMetodoPago'
);

Route::put(
    'co/sc/articulo/{articulo}/descripcion',
    'SC\OrdenCompraController@updateArticuloDescripcion'
);

Route::put(
    'co/sc/ordencompra/{orden}/autorizacion',
    'SC\OrdenCompraController@autorizacionOrdenCompra'
);

Route::post(
    'co/sc/ordencompra/upload',
    'SC\OrdenCompraController@generateOrdenCompra'
);

Route::get(
    'co/sc/ordencompra/{name}/download',
    'SC\OrdenCompraController@downloadOrdenCompra'
);

Route::put(
    'co/sc/ordencompra/{orden}/enviar/autorizacion',
    'SC\SolicitudController@autorizacion'
);

// SOLICITUD DE COMPRA - COMENTARIOS

Route::post(
    'co/sc/comentarios',
    'SC\ComentarioController@store'
);

Route::get(
    'co/sc/ordencompra/{orden}/comentario',
    'SC\ComentarioController@getComentarioByOrdenCompra'
);

// SOLICITUD DE COMPRA - RECEPCIÓN DE MERCANCÍA

Route::get(
    'co/sc/ordencompra/{orden}/recepciones',
    'SC\RecepcionMercanciaController@getRecepcionMercanciasByOrdenCompra'
);
Route::get(
    'co/sc/ordencompra/{orden}/recepcion/articulos',
    'SC\RecepcionMercanciaController@getRecepcionArticulosByOrdenCompra'
);

Route::post(
    'co/sc/ordencompra/recepcion',
    'SC\RecepcionMercanciaController@store'
);

Route::put(
    'co/sc/ordencompra/recepcion/articulos',
    'SC\RecepcionMercanciaController@reassignArticulos'
);

Route::get(
    'co/sc/ordencompra/factura/{file}/download',
    'SC\RecepcionMercanciaController@downloadFile'
);

// RESUMEN

Route::get(
    'co/sc/resumen/solicitudes/exportar',
    'SC\SolicitudController@resumenSolicitudes'
);
