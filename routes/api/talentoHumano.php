<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// CONFIGURACIÓN

// DEPARTAMENTOS
Route::get(
    'th/departamentos',
    'DepartamentoController@getDepartamentos'
);

Route::post(
    'th/departamento',
    'DepartamentoController@store'
);

Route::put(
    'th/departamento/{departamento}',
    'DepartamentoController@update'
);

Route::delete(
    'th/departamento/{departamento}',
    'DepartamentoController@destroy'
);


// AREAS
Route::get(
    'th/areas',
    'AreaController@getAreas'
);

Route::post(
    'th/area',
    'AreaController@store'
);

Route::put(
    'th/area/{area}',
    'AreaController@update'
);

Route::delete(
    'th/area/{area}',
    'AreaController@destroy'
);


// PUESTOS
Route::get(
    'th/puestos',
    'PuestoController@getPuestos'
);

Route::post(
    'th/puesto',
    'PuestoController@store'
);

Route::put(
    'th/puesto/{puesto}',
    'PuestoController@update'
);

Route::delete(
    'th/puesto/{puesto}',
    'PuestoController@destroy'
);


// MOTIVOS DE AUSENCIA
Route::get(
    'th/motivosAusencia',
    'MotivoAusenciaController@getMotivosAusencia'
);

Route::post(
    'th/motivoAusencia',
    'MotivoAusenciaController@store'
);

Route::put(
    'th/motivoAusencia/{motivoAusencia}',
    'MotivoAusenciaController@update'
);

Route::delete(
    'th/motivoAusencia/{motivoAusencia}',
    'MotivoAusenciaController@destroy'
);

// AUTORIZACIONES
Route::get(
    'th/vp/configuracion/autorizadores',
    'AutorizacionController@getAutorizadores'
);

Route::get(
    'th/vp/configuracion/autorizaciones',
    'AutorizacionController@getAutorizaciones'
);

Route::post(
    'th/vp/configuracion/autorizaciones',
    'AutorizacionController@store'
);

Route::put(
    'th/vp/configuracion/autorizaciones/{autorizacion}',
    'AutorizacionController@update'
);

// PUESTOS
Route::get(
    'th/vp/diasFestivos',
    'DiaFestivoController@getDiasFestivos'
);

Route::post(
    'th/vp/diaFestivo',
    'DiaFestivoController@store'
);

Route::put(
    'th/vp/diaFestivo/{diaFestivo}',
    'DiaFestivoController@update'
);

Route::delete(
    'th/vp/diaFestivo/{diaFestivo}',
    'DiaFestivoController@destroy'
);

// EMPLEADOS
Route::get(
    'th/empleados',
    'EmpleadoController@getEmpleados'
);

Route::get(
    'th/empleado/{empleado}',
    'EmpleadoController@getEmpleado'
);

Route::post(
    'th/empleado',
    'EmpleadoController@store'
);

Route::put(
    'th/empleado/{empleado}',
    'EmpleadoController@update'
);

Route::delete(
    'th/empleado/{empleado}',
    'EmpleadoController@destroy'
);

//empleado - reporte actividades
Route::get('th/empleado/noEmpleado/{noEmpleado}', 'EmpleadoController@getEmpleadoByNumeroEmpleado');


// VACACIONES Y PERMISOS
Route::post(
    'th/vp/solicitud',
    'VP\SolicitudController@store'
);
// Todas
Route::get(
    'th/vp/solicitudes',
    'VP\SolicitudController@getSolicitudesAll'
);
// Por usuario
Route::get(
    'th/vp/solicitudes/usuarios/{idUsuario}',
    'VP\SolicitudController@getSolicitudesByUsuario'
);
// Por autorizar
Route::get(
    'th/vp/solicitudes/autorizador/{idUsuario}',
    'VP\SolicitudController@getSolicitudesByAutorizador'
);
// Solicitudes por usuario
Route::get(
    'th/vp/solicitudes/empleado/{idUsuario}',
    'VP\SolicitudController@getSolicitudesByEmpleado'
);
// Comentarios
Route::post(
    'th/vp/comentarios',
    'VP\ComentarioController@store'
);
Route::get(
    'th/vp/solicitud/{idSolicitud}/{idEtapa}/comentario',
    'VP\ComentarioController@getComentario'
);

Route::put(
    'th/vp/solicitud/{solicitud}/semanas',
    'VP\SolicitudController@addSemanas'
);
//download
Route::post(
    'th/vp/solicitud/upload',
    'VP\SolicitudController@uploadReporte'
);
Route::get(
    'th/vp/solicitud/{name}/download',
    'VP\SolicitudController@downloadReporte'
);

// Pendientes (Acumulados de otros años)
Route::get(
    'th/vp/pendientes/{noEmpleado}',
    'VP\SolicitudController@getVacacionesPendientes'
);

// AUTORIZACIONES

Route::put(
    'th/vp/solicitud/{solicitud}/autorizacion',
    'VP\SolicitudController@autorizacion'
);

// Route::put(
//     'th/vp/solicitud/{solicitud}/enviar/autorizacion',
//     'SC\SolicitudController@autorizacion'
// );




