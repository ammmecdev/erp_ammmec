<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//SOLICITUDES
Route::post(
    'alm/ss/solicitud',
    'SS\SolicitudController@store'
);
Route::get(
    'alm/ss/solicitud/{solicitud}',
    'SS\SolicitudController@getSolicitud'
);
Route::put(
    'alm/ss/solicitud/{solicitud}/etapas/{idEtapa}',
    'SS\SolicitudController@cambioEtapa'
);
Route::get(
    'alm/ss/solicitudes/cuentas/{tipo}',
    'SS\SolicitudController@getCuentasBySolicitud'
);
Route::put(
    'alm/ss/archivar/solicitud',
    'SS\SolicitudController@archivarSolicitud'
);



// Todas
Route::get(
    'alm/ss/solicitudes',
    'SS\SolicitudController@getSolicitudesAll'
);

// Por usuario
Route::get(
    'alm/ss/solicitudes/usuarios/{idUsuario}',
    'SS\SolicitudController@getSolicitudesByUsuario'
);

Route::get(
    'alm/ss/solicitud/{name}/download',
    'SS\SolicitudController@downloadSolicitud'
);

Route::post(
    'alm/ss/solicitud/upload',
    'SS\SolicitudController@generateSolicitud'
);

Route::get(
    'alm/ss/solicitud/file/{name}/download',
    'SS\SolicitudController@downloadFile'
);


// SS - ARTICULOS
Route::get(
    'alm/ss/solicitud/{solicitud}/articulos',
    'SS\SolicitudController@getArticulos'
);

Route::get(
    'alm/ss/etapas',
    'SS\SolicitudController@getEtapas'
);

Route::put(
    'alm/ss/solicitar/compra',
    'SS\SolicitudController@solicitarCompra'
);

Route::put(
    'alm/ss/articulo/{articulo}/output',
    'SS\SolicitudController@outputArticulo'
);

// SS - VALE DE SALIDA

Route::post(
    'alm/ss/vale/salida/upload',
    'SS\SolicitudController@generateVale'
);

Route::get(
    'alm/ss/vale/salida/{name}/download',
    'SS\SolicitudController@downloadVale'
);


// INVENTARIOS
Route::get(
    'alm/inventario/activo/{activo}',
    'Inventario\ActivoController@getActivo'
);

Route::get(
    'alm/inventario/activos',
    'Inventario\ActivoController@getActivos'
);

Route::get(
    'alm/activo/categorias/select',
    'Inventario\ActivoController@getCategorias'
);

Route::post( // CARGAR INVENTARIO DE ACTIVOS EN FORMATO CSV
    'alm/inventario/activos/file',
    'Inventario\ActivoController@loadActivos'
);

Route::post(
    'alm/inventario/activo',
    'Inventario\ActivoController@store'
);
 
Route::post( // REGISTRAR VARIOS ACTIVOS
    'alm/inventario/activos',
    'Inventario\ActivoController@storeActivos'
);

Route::put( // ACTUALIZAR VARIOS ACTIVOS 
    'alm/inventario/activos',
    'Inventario\ActivoController@editActivos'
);

Route::put(
    'alm/inventario/activo/{activo}',
    'Inventario\ActivoController@update'
);

Route::put(
    'alm/inventario/activo/edit/{activo}',
    'Inventario\ActivoController@updateActivo'
);

Route::delete(
    'alm/inventario/activo/{activo}',
    'Inventario\ActivoController@destroy'
);

Route::get(
    'alm/activos/like', 
    'Inventario\ActivoController@selectActivosLike'
);





