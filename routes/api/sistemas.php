<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//SISTEMAS

//Cuentas

Route::get(
    'si/admin/cuentas/dominios',
    'CuentaElectronicaController@getDominios'
);
Route::get(
    'si/admin/cuentas/item/{dominio}',
    'CuentaElectronicaController@getProxItem'
);

Route::get(
    'si/admin/cuentas',
    'CuentaElectronicaController@getCuentas'
);
Route::post(
    'si/admin/cuentas',
    'CuentaElectronicaController@store'
);
Route::put(
    'si/admin/cuentas/{idCuenta}',
    'CuentaElectronicaController@update'
);

Route::get(
    'si/admin/categorias/licencias',
    'CategoriaLicenciaController@getCategoriasWhitLicencias'
);
Route::post(
    'si/admin/categorias/licencias',
    'CategoriaLicenciaController@store'
);

Route::get(
    'si/admin/licencias/categorias',
    'CategoriaLicenciaController@getCategorias'
);

Route::post(
    'si/admin/licencias',
    'LicenciaController@store'
);
Route::put(
    'si/admin/licencias/{idLicencia}',
    'LicenciaController@update'
);

Route::get(
    'si/admin/licencias/periodos/{idLicencia}',
    'PeriodoLicenciaController@getPeriodos'
);

Route::post(
    'si/admin/licencias/periodos',
    'PeriodoLicenciaController@store'
);

Route::put(
    'si/admin/licencias/periodos/{idPeriodo}',
    'PeriodoLicenciaController@update'
);

Route::put(
    'si/admin/licencias/periodos/{idPeriodo}/{pagado}',
    'PeriodoLicenciaController@updatePagado'
);

//Equipos

Route::get(
    'si/admin/equipos',
    'EquipoElectronicoController@getEquipos'
);

Route::post(
    'si/admin/equipos',
    'EquipoElectronicoController@store'
);

Route::put(
    'si/admin/equipos/{idEquipo}',
    'EquipoElectronicoController@update'
);

Route::get(
    'si/admin/equipos/claves/{tipo}',
    'EquipoElectronicoController@getProxClave'
);

Route::get(
    'si/servicios-web',
    'ServicioWebController@getServicios'
);

Route::post(
    'si/servicios-web',
    'ServicioWebController@store'
);

Route::get(
    'si/cuentas-servicios/{idCuenta}',
    'CuentaServicioController@getCuentasServicios'
);

Route::post(
    'si/cuentas-servicios',
    'CuentaServicioController@store'
);

Route::put(
    'si/cuentas-servicios/{cuentaServicio}',
    'CuentaServicioController@update'
);
