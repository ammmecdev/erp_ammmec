<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//NORMATIVIDAD

Route::get(
    'nr/area/{codigo}',
    'TalentoHumano\AreaController@getArea'
);

Route::get(
    'nr/area/{area}/procesos',
    'TalentoHumano\AreaController@getProcesos'
);

Route::get(
    'nr/areas/{idArea}/archivos',
    'TalentoHumano\AreaController@getArchivosByArea'
);

Route::get(
    'nr/procedimientos/{idArea}/archivos',
    'Calidad\GC\ProcedimientoController@getArchivosByProcedimiento'
);

Route::get(
    'nr/manual-departamental/{idManual}/show',
    'Calidad\GC\ManualDepartamentalController@show'
);

Route::post(
    'nr/manual-departamental',
    'Calidad\GC\ManualDepartamentalController@store'
);

Route::post(
    'nr/manual-departamental/update',
    'Calidad\GC\ManualDepartamentalController@update'
);

Route::get(
    'nr/procesos/{idArea}/select',
    'Calidad\GC\ProcesoController@selectProcesos'
);
Route::post(
    'nr/procesos',
    'Calidad\GC\ProcesoController@store'
);

Route::put(
    'nr/procesos',
    'Calidad\GC\ProcesoController@update'
);

Route::delete(
    'nr/procesos/{proceso}',
    'Calidad\GC\ProcesoController@delete'
);

Route::post(
    'nr/procedimientos',
    'Calidad\GC\ProcedimientoController@store'
);

Route::put(
    'nr/procedimientos',
    'Calidad\GC\ProcedimientoController@update'
);

Route::delete(
    'nr/procedimientos/{procedimiento}',
    'Calidad\GC\ProcedimientoController@delete'
);

Route::get(
    'nr/archivos/search',
    'Calidad\GC\ArchivoProcedimientoController@searchArchivos'
);

Route::post(
    'nr/archivos',
    'Calidad\GC\ArchivoProcedimientoController@store'
);

Route::post(
    'nr/archivos/update',
    'Calidad\GC\ArchivoProcedimientoController@update'
);

Route::get(
    'nr/archivos/{archivo}',
    'Calidad\GC\ArchivoProcedimientoController@download'
);

Route::get(
    'nr/archivos/{archivo}/show',
    'Calidad\GC\ArchivoProcedimientoController@show'
);

Route::get(
    'nr/puestos/{puesto}/perfil',
    'TalentoHumano\PuestoController@getPerfilPuesto'
);
Route::post(
    'nr/perfil-puesto',
    'Calidad\GC\PerfilPuestoController@store'
);
Route::post(
    'nr/perfil-puesto/update',
    'Calidad\GC\PerfilPuestoController@update'
);

Route::get(
    'nr/organizacion/archivos/{tipo}',
    'Calidad\GC\ArchivoOrganizacionalController@getArchivosByTipo'
);

Route::post(
    'nr/organizacion/archivos',
    'Calidad\GC\ArchivoOrganizacionalController@store'
);
Route::post(
    'nr/organizacion/archivos/update',
    'Calidad\GC\ArchivoOrganizacionalController@update'
);