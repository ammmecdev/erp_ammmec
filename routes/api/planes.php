<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//PLANES DE TRABAJO

Route::put(
    'planes/{plan}',
    'PlanController@update'
);

Route::post(
    'planes',
    'PlanController@store'
);

Route::get(
    'planes/{idUsuario}',
    'PlanController@getPlanes'
);

Route::delete(
    'planes/{plan}',
    'PlanController@destroy'
);

Route::get(
    'plan/{plan}/secciones',
    'PlanController@getSecciones'
);

Route::post(
    'planes/seccion',
    'PlanController@storeSeccion'
);

Route::delete(
    'planes/seccion/{seccion}',
    'PlanController@destroySeccion'
);

Route::get(
    'plan/{plan}/actividades',
    'PlanController@getActividades'
);

Route::post(
    'planes/actividad',
    'PlanController@storeActividad'
);

Route::put(
    'planes/actividad/{actividad}',
    'PlanController@updateActividad'
);

Route::delete(
    'planes/actividad/{actividad}',
    'PlanController@destroyActividad'
);

Route::get(
    'plan/{plan}/responsables',
    'PlanController@getResponsables'
);

Route::post(
    'planes/responsable',
    'PlanController@storeResponsable'
);

Route::get(
    'plan/{plan}/usuarios',
    'PlanController@getUsuariosCompartidos'
);

Route::post(
    'planes/usuarios',
    'PlanController@storePlanUsuario'
);

Route::delete(
    'planes/usuario/{usuario}',
    'PlanController@destroyUsuario'
);
