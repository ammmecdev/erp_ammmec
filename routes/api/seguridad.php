<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//SEGURIDAD

Route::get(
    'se/salud/historial-clinico',
    'HistorialClinicoController@getHistoriales'
);

Route::get(
    'se/salud/historial-clinico/tipos-examenes/select',
    'HistorialClinicoController@getTiposExamenes'
);

Route::get(
    'se/salud/historial-clinico/clinicas/select',
    'HistorialClinicoController@getClinicas'
);

Route::get(
    'se/salud/historial-clinico/{id}/download',
    'HistorialClinicoController@downloadFile'
);

Route::post(
    'se/salud/historial-clinico',
    'HistorialClinicoController@store'
);

Route::put(
    'se/salud/historial-clinico',
    'HistorialClinicoController@update'
);

Route::delete(
    'se/salud/historial-clinico/{id}',
    'HistorialClinicoController@delete'
);
