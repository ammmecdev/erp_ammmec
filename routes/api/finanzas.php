<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//SOLICITUDES

Route::put(
    'fn/cg/solicitud/{solicitud}/timbrar',
    'CG\SolicitudController@timbrarSolicitud'
);

Route::get(
    'fn/cg/solicitud/{solicitud}',
    'CG\SolicitudController@getSolicitud'
);

Route::get(
    'fn/cg/solicitudes',
    'CG\SolicitudController@getSolicitudesAll'
);

Route::get(
    'fn/cg/solicitudes/rango',
    'CG\SolicitudController@getSolicitudesRango'
);

Route::get(
    'fn/cg/solicitudes/usuarios/{idUsuario}',
    'CG\SolicitudController@getSolicitudesByUsuario'
);

Route::get(
    'fn/cg/solicitudes/autorizador/{idUsuario}',
    'CG\SolicitudController@getSolicitudesByAutorizador'
);

Route::post(
    'fn/cg/solicitudes',
    'CG\SolicitudController@store'
);

Route::put(
    'fn/cg/solicitudes/{idSolicitud}',
    'CG\SolicitudController@update'
);

Route::delete(
    'fn/cg/solicitudes/{id}', 
    'CG\SolicitudController@delete'
);

//Reporte

Route::post(
    'fn/cg/solicitud/reporte', 
    'CG\SolicitudController@generateReport'
);

Route::get(
    'fn/cg/solicitud/reporte/{idSolicitud}/download', 
    'CG\SolicitudController@downloadSolicitud'
);

Route::post(
    'fn/cg/solicitud/reporte/{idSolicitud}/delete', 
    'CG\SolicitudController@deleteSolicitud'
);


//Cuentas por solicitud

Route::get(
    'fn/cg/solicitudes/{idSolicitud}/cuentas/{tipo}',
    'CG\SolicitudController@getCuentasBySolicitud'
);

Route::get(
    'fn/cg/solicitudes/viaticos/cuentas/{tipo}',
    'CG\SolicitudController@getCuentas'
);

//RESUMEN

Route::get(
    'fn/cg/solicitudes/resumen/{idSolicitud}',
    'CG\ResumenController@getResumen'
);

Route::post(
    'fn/cg/solicitudes/resumen/{idSolicitud}',
    'CG\ResumenController@store'
);

Route::put(
    'fn/cg/solicitudes/resumen/{idSolicitud}',
    'CG\ResumenController@update'
);
    
// PLANEACIÓN

Route::get(
    'fn/cg/solicitudes/planeacion/gasto/{idSolicitud}',
    'CG\SolicitudController@getPlaneacionGasto'
);

// DISPOSICIÓN EFECTIVO

Route::post(
    'fn/cg/solicitudes/disposicion/efectivo',
    'CG\DisposicionEfectivoController@store'
);

Route::put(
    'fn/cg/solicitudes/disposicion/efectivo',
    'CG\DisposicionEfectivoController@update'
);

Route::get(
    'fn/cg/solicitudes/disposiciones/{idSolicitud}',
    'CG\DisposicionEfectivoController@getDisposicionesEfectivo'
);

Route::delete(
    'fn/cg/solicitudes/disposicion/{disposicion}',
    'CG\DisposicionEfectivoController@delete'
);

Route::get(
    'fn/cg/solicitudes/disposicion/comprobante/{file}/download',
    'CG\DisposicionEfectivoController@downloadComprobante'
);


// CUENTAS CONTABLES

Route::get(
    'fn/cuentas-contables',
    'CuentaContableController@getCuentas'
);

Route::get(
    'fn/cuentas-contables/areas',
    'CuentaContableController@getAreas'
);

Route::get(
    'fn/cuentas-contables/areas/{area}',
    'CuentaContableController@getCuentasByArea'
);

Route::get(
    'fn/cuentas-contables/areas/{area}/cuentas-mayores',
    'CuentaContableController@getCuentasMayoresByArea'
);


// CUENTAS INTERNAS

Route::get(
    'fn/cuentas-internas/areas',
    'CuentaInternaController@getAreas'
);

Route::get(
    'fn/cuentas-internas',
    'CuentaInternaController@getCuentas'
);

Route::get(
    'fn/cuentas-internas/areas/{area}',
    'CuentaInternaController@getCuentasByArea'
);

Route::get(
    'fn/cuentas-internas/areas/{area}/unidades-negocio',
    'CuentaInternaController@getUnidadesNegocioByArea'
);

Route::get(
    'fn/cuentas-internas/nombres/select',
    'CuentaInternaController@getNombres'
);

Route::get(
    'fn/cuentas-internas/unidad-negocio/select',
    'CuentaInternaController@getUnidadesNegocio'
);

Route::post(
    'fn/cuentas-internas/cuenta',
    'CuentaInternaController@store'
);

Route::put(
    'fn/cuentas-internas/cuenta',
    'CuentaInternaController@edit'
);

Route::delete(
    'fn/cuentas-internas/cuenta/{idCuenta}',
    'CuentaInternaController@delete'
);



// CUENTAS USUARIOS

Route::get(
    'fn/cuentas/usuarios/{idUsuario}',
    'CuentasUsuarioController@getCuentasByUsuario'
);

Route::post(
    'fn/cuentas/usuarios/{tipo}',
    'CuentasUsuarioController@save'
);

Route::get(
    'fn/cuentas/usuarios/{idUsuario}/contables/{unidadNegocio}',
    'CuentasUsuarioController@getCuentasContablesByUsuario'
);

Route::get(
    'fn/cuentas/contables/unique',
    'CuentaContableController@getCuentasContablesUnique'
);


Route::get(
    'fn/cuentas/usuarios/{idUsuario}/internas',
    'CuentasUsuarioController@getCuentasInternasByUsuario'
);

// ACEPTACIÓN DE LA ASIGNACIÓN DEL VIÁTICO

Route::put(
    'fn/cg/solicitudes/aceptacion/{solicitud}',
    'CG\SolicitudController@aceptacionSolicitud'
);

// AUTORIZACIÓN

Route::put(
    'fn/cg/solicitudes/autorizacion/{solicitud}',
    'CG\SolicitudController@autorizacionSolicitud'
);

// MOVIMIENTOS DISPERSIÓN

Route::delete(
    'fn/cg/movimientos/{movimiento}',
    'CG\MovimientoController@destroy'
);


// AUXILIAR DE MOVIMIENTOS

Route::get(
    'fn/cg/movimientos/empleado/{idUsuario}',
    'CG\MovimientoController@getMovimientosByEmpleado'
);

Route::get(
    'fn/cg/movimientos/cuenta/{cuenta}',
    'CG\MovimientoController@getMovimientosByCuenta'
);

Route::get(
    'fn/cg/movimientos/empleados',
    'CG\MovimientoController@getEmpleados'
);

Route::get(
    'fn/cg/movimientos/cuentas/{tipoCuenta}',
    'CG\MovimientoController@getCuentas'
);

Route::get(
    'fn/cg/movimientos/{solicitud}',
    'CG\MovimientoController@getMovimientosBySolicitud'
);

Route::post(
    'fn/cg/movimientos/{solicitud}',
    'CG\MovimientoController@save'
);

Route::put(
    'fn/cg/movimientos/{movimiento}',
    'CG\MovimientoController@updateMovDispersion'
);

Route::post(
    'fn/cg/movimientos',
    'CG\MovimientoController@store'
);

Route::put(
    'fn/cg/movimientos/comprobacion/{idComprobacion}',
     'CG\MovimientoController@update'
);

//DISPERSION

Route::post(
    'fn/cg/solicitudes/dispersion/{solicitud}',
    'CG\SolicitudController@dispersarSolicitud'
);

Route::post(
    'fn/cg/solicitudes/dispersion/resumen/{solicitud}',
    'CG\ResumenController@dispersionSolicitud'
);

Route::put(
    'fn/cg/solicitudes/dispersion/resumen/{solicitud}',
    'CG\ResumenController@updateDispersionSolicitud'
);

Route::put(
    'fn/cg/solicitudes/periodo/dispersion/{solicitud}',
    'CG\SolicitudController@setPeriodoDispersion'
);


// COMPROBANTES (DISPERSIÓN Y REEMBOLSOS Y DECREMENTOS)
Route::get(
    'fn/cg/solicitudes/comprobantes/{idSolicitud}',
    'CG\ArchivoController@getArchivos'
);

Route::put(
    'fn/cg/solicitudes/file/comprobante',
    'CG\ArchivoController@update'
);

Route::post(
    'fn/cg/solicitudes/file/comprobante',
    'CG\ArchivoController@store'
);

Route::delete(
    'fn/cg/solicitudes/file/comprobante/{archivo}',
    'CG\ArchivoController@delete'
);

Route::get(
    'fn/cg/solicitudes/comprobante/{file}/download',
    'CG\ArchivoController@download'
);


//COMENTARIOS

Route::post(
    'fn/cg/solicitudes/comentarios',
    'CG\ComentarioController@store'
);

//COMPROBACIONES

// Comprobaciones - indicadores

Route::get(
    'fn/cg/comprobaciones/no-deducibles',
    'CG\ComprobacionController@getNoDeducibles'
);

Route::get(
    'fn/cg/comprobaciones/all',
    'CG\ComprobacionController@getComprobacionesAll'
);

Route::get(
    'fn/cg/comprobaciones/{idSolicitud}',
    'CG\ComprobacionController@getComprobaciones'
);

// Comprobación de gastos
// Route::post(
//     'fn/cg/comprobaciones/resumen/{idSolicitud}',
//     'CG\ResumenController@comprobacionSolicitud'
// );

Route::post(
    'fn/cg/comprobaciones/resumen',
    'CG\ResumenController@comprobacionSolicitud'
);

Route::put(
    'fn/cg/comprobaciones/resumen',
    'CG\ResumenController@updateComprobacionSolicitud'
);

Route::post(
    'fn/cg/comprobaciones',
    'CG\ComprobacionController@store'
);

Route::put(
    'fn/cg/comprobaciones',
    'CG\ComprobacionController@update'
);

Route::post(
    'fn/cg/comprobaciones/files',
    'CG\ComprobacionController@updateFiles'
);

Route::put(
    'fn/cg/comprobaciones/revision',
    'CG\ComprobacionController@setRevision'
);

Route::delete(
    'fn/cg/comprobaciones/{comprobacion}',
    'CG\ComprobacionController@delete'
);

Route::get(
    'fn/cg/comprobaciones/{file}/download',
    'CG\ComprobacionController@downloadFile'
);
// Termina comprobación

Route::put(
    'fn/cg/solicitudes/{solicitud}/fecha-fin-gastos-real',
    'CG\SolicitudController@setFechaFinGastosReal'
);

Route::get(
    'fn/cg/files/{idComprobacion}',
    'CG\ComprobacionController@getFiles'
);


//REMBOLSO Y DECREMENTO

Route::post(
    'fn/cg/solicitudes/{accion}/resumen/{solicitud}',
    'CG\ResumenController@reembolsoDecremento'
);

Route::put(
    'fn/cg/solicitudes/{accion}/resumen/{solicitud}',
    'CG\ResumenController@updateReembolsoDecremento'
);

//ETAPAS

Route::get(
    'fn/cg/etapas',
    'CG\EtapaController@getEtapas'
);

Route::put(
    'fn/cg/solicitudes/{idSolicitud}/etapas/{idEtapa}',
    'CG\SolicitudController@cambioEtapa'
);


//AUTORIZACIONES

Route::get(
    'fn/cg/configuracion/autorizadores',
    'CG\AutorizacionController@getAutorizadores'
);

Route::put(
    'fn/configuracion/cg/autorizadores/{autorizador}/{status}',
    'CG\AutorizacionController@update'
);


Route::get(
    'fn/cg/configuracion/autorizaciones',
    'CG\AutorizacionController@getAutorizaciones'
);

Route::get(
    'fn/cg/autorizaciones/usuarios/{idUsuario}',
    'CG\AutorizacionController@getAutorizacionByUsuario'
);

Route::post(
    'fn/cg/configuracion/autorizaciones',
    'CG\AutorizacionController@store'
);

Route::put(
    'fn/cg/configuracion/autorizaciones/{autorizacion}',
    'CG\AutorizacionController@update'
);


//CENTROS DE COSTOS

Route::get(
    'fn/cc/unidades-negocio',
    'CentroCostoController@getUnidadesNegocio'
);

Route::get(
    'fn/cc/departamentos',
    'CentroCostoController@getDepartamentos'
);

Route::get(
    'fn/cc/categorias',
    'CentroCostoController@getCategorias'
);

Route::get(
    'fn/cc/procesos',
    'CentroCostoController@getProcesos'
);

Route::get(
    'fn/cc/colaboradores',
    'CentroCostoController@getColaboradores'
);

Route::get(
    'fn/cc/no-productivos',
    'CentroCostoController@getNoProductivos'
);

//COMENTARIOS

Route::get(
    'fn/cg/comentarios/{idSolicitud}/{idEtapa}',
    'CG\ComentarioController@getComentario'
);


//BITACORAS

Route::get(
    'fn/solicitudes/{solicitud}/bitacoras',
    'CG\BitacoraController@getBitacorasBySolicitud'
);

//MANO DE OBRA - COSTO OPERATIVO

Route::get(
    'fn/co/puestos',
    'ManoObraController@getPuestos'
);

Route::put(
    'fn/co/puesto/{puesto}', 
    'ManoObraController@updatePuesto'
);

Route::get(
    'fn/co/factores',
    'ManoObraController@getFactores'
);

Route::put(
    'fn/co/factor/{factor}', 
    'ManoObraController@updateFactor'
);

// CONFIGURACIÓN EXPORTAR SOLICITUDES

Route::get(
    'fn/cg/solicitudes/resumen/solicitudes/exportar',
    'CG\ResumenController@exportResumenExcel'
);
