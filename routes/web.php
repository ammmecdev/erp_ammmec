<?php

use App\Usuario;

Route::get('/', 'Auth\LoginController@showLoginForm');
Auth::routes();

Route::group(['middleware' => 'auth'], function () {

	Route::get('/modulos', function () {
        $usuario = Usuario::find(Auth::user()->idUsuario);
        $permisos = collect();
        foreach($usuario->roles as $role) {
            foreach($role->permisos as $permiso)
                $permisos->push($permiso);
        }
        $permisos = $permisos->unique();
		return view('modulos.modules', compact('permisos'));
	})->name('modulos');

	//CERRAR SESION
	Route::get('logout', 'Auth\LoginController@logout')->name('logout');

	//NOTIFICACIONES

	Route::put('api/notification', 'NotificationsController@read');

	Route::get('api/notifications', 'NotificationsController@getNotifications');

	Route::get('/cadmin', function () {
        $module = 'cadmin';
        return view('layouts.app', compact('module'));
    })->name('ad');

	Route::get('/cadmin/usuarios', function () {
        $module = 'cadmin';
        return view('layouts.app', compact('module'));
    })->name('ad.usuarios');

    Route::get('/cadmin/estructura', function () {
        $module = 'cadmin';
        return view('layouts.app', compact('module'));
    })->name('ad.estructura');

    Route::get('/cadmin/roles', function () {
        $module = 'cadmin';
        return view('layouts.app', compact('module'));
    })->name('ad.roles');

    Route::get('/cadmin/configuracion/cuentasExternas', function () {
        $module = 'cadmin';
        return view('layouts.app', compact('module'));
    })->name('ad.cf.cuentas_externas');

    Route::get('/planes', function () {
        $module = 'tickets';
        return view('layouts.app', compact('module'));
    })->name('planes');
});
