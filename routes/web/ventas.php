<?php

use Illuminate\Support\Facades\Route;

//VENTAS

Route::group(['middleware' => 'auth'], function () {

	Route::get('/ventas', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('ventas');

	//NEGOCIOS

	Route::get('/ventas/negocios/seguimiento', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.negocios.seguimiento');

	Route::get('/ventas/negocios/embudos', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.negocios.embudos');

	Route::get('/ventas/negocios/lista', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.negocios.lista');

	Route::get('ventas/negocios/notas', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.negocios.notas');

	Route::get('ventas/negocios/notas/{id}', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.negocios.notas');

	Route::get('ventas/negocios/{idNegocio}', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.negocios.detalles');

	Route::get('ventas/negocios/detalles', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.negocios.detalles');

	//NEGOCIOS DETALLES CONTENIDOS

	Route::post('api/vt/negocios/detalles/notas', 'ContenidosController@store');

	//ACTIVIDADES

	Route::post('/api/vt/actividades', 'ActividadController@store');

	//NOTAS

	Route::post('/api/vt/notas/store', 'NotaLeadController@store');
	Route::post('/api/vt/comentarios/store', 'ComentarioNotaLeadController@store');


	//QUITAR
	Route::get('ventas/servicios', function () {
		return view('layouts.app');
	})->name('vt.servicios');

	Route::get('api/vt/servicios', 'NegocioController@getNegociosServicios');

	Route::post('api/vt/servicios', 'NegocioController@setClaves');

	//NEGOCIOS
	Route::post('api/vt/negocios', 'NegocioController@store');

	//UPDATE EN SEGUIMIENTO

	Route::put(
		'api/vt/seguimiento/negocios',  //UPDATE NEGOCIOS DESDE SEGUIMIENTO
		'NegocioController@update'
	);

	Route::put(
		'api/vt/seguimiento/costo',  //UPDATE COSTO DESDE SEGUIMIENTO
		'CostoController@update'
	);

	Route::put(
		'api/vt/seguimiento/cotizacion',  //UPDATE COTIZACIÓN DESDE SEGUIMIENTO
		'CotizacionController@update'
	);

	Route::put(
		'api/vt/seguimiento/pago',  //UPDATE PAGO DESDE SEGUIMIENTO
		'PagoController@update'
	);

	Route::put(
		'api/vt/seguimiento/factura',  //UPDATE FACTURA DESDE SEGUIMIENTO
		'FacturaController@update'
	);

	Route::put(
		'api/vt/negocios/tipomoneda',
		'NegocioController@update_moneda'
	);


	//UPDATE - EVALUACIÓN DE SERVICIO 
	Route::put(
		'api/vt/indicadores/evaluacion/pregunta',   //VALOR
		'EvaluacionPreguntaController@update'
	);

	Route::put(
		'api/vt/indicadores/evaluacion',   //VALOR
		'EvaluacionServicioController@update'
	);


	//UPDATE - PRESUPUESTO
	Route::put('api/vt/presupuesto/presupuestosMeses', 'Ventas\PresupuestoController@update_presupuestoMes');

	//UPDATE - CLAVE DE NEGOCIO
	Route::put('api/vt/negocios/clave', 'NegocioController@update_clave');

	//UPDATE - CAMBIO DE EMBUDO
	Route::put('api/vt/negocios/{negocio}/embudos', 'NegocioController@update_embudo_negocio');

	//ACTIVIDADES

	Route::get('ventas/actividades/{idNotification}', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.actividades');

	Route::get('ventas/actividades/{actividades}', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.actividades');

	Route::get('ventas/actividades', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.actividades');

	//CLIENTES

	Route::get('ventas/clientes', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.clientes');

	//CONTACTOS

	Route::get('ventas/contactos', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.contactos');

	//PRESUPUESTO

	Route::get('ventas/presupuesto', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.presupuesto');

	//INDICADORES

	//Clientes facturados
	Route::get('ventas/indicadores/clientes-facturados', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.indicadores.cf');

	Route::delete('vt/indicadores/clienteFacturado/{cliente}', 'ClienteFacturadoController@delete');

	//Cartera vencida
	Route::get('ventas/indicadores/cartera-vencida', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.indicadores.cv');

	//crecimiento
	Route::get('ventas/indicadores/porcentaje-crecimiento', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.indicadores.crecimiento');

	//Porcentaje de ventas
	Route::get('ventas/indicadores/porcentaje-ventas', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.indicadores.pv');

	//Evaluación de servicio
	Route::get('ventas/indicadores/evaluacion-servicio', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.indicadores.es');

	//Oportunidades

	Route::get('ventas/indicadores/oportunidades', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.indicadores.oportunidades');

	Route::get('ventas/indicadores/unidades-negocio', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.indicadores.un');

	Route::get('ventas/indicadores/presupuesto', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.indicadores.presupuesto');
	
	//CONFIGURACION

	//EMBUDOS

	Route::get('ventas/configuracion/embudos', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.configuracion.embudos');

	Route::get('ventas/configuracion/unidades_negocio', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.configuracion.unidades_negocio');

	Route::get('ventas/configuracion/proyecciones', function () {
		$module = "ventas";
		return view('layouts.app', compact('module'));
	})->name('vt.configuracion.proyecciones');

});
