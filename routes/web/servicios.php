<?php

use Illuminate\Support\Facades\Route;

//SERVICIOS

Route::group(['middleware' => 'auth'], function () {

    Route::get('/servicios', function () {
        $module = 'servicios';
        return view('layouts.app', compact('module'));
    })->name('servicios');

    //SERVICIOS - ACTIVIDADES

    Route::get('/servicios/ra/actividades', function () {
        $module = 'servicios';
        return view('layouts.app', compact('module'));
    })->name('servicios.actividades');

    Route::get('/servicios/ra/configuracion/semanas', function () {
        $module = 'servicios';
        return view('layouts.app', compact('module'));
    })->name('servicios.configuracion.semanas');

    Route::get('/servicios/ra/reporte/{idReporte}', 'Servicios\RA\ReporteController@showReport');

    Route::get('/servicios/ra/cuentas/{idSemana}', 'Servicios\RA\ReporteController@generateReportCuentasRegistradas');

    Route::get('/servicios/ra/indicadores/actividades', function () {
        $module = 'servicios';
        return view('layouts.app', compact('module'));
    })->name('servicios.indicadores.actividades');

    Route::get('/servicios/costo', function () {
        $module = 'servicios';
        return view('layouts.app', compact('module'));
    })->name('servicios.co.costos');    
});
