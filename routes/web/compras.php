<?php

use Illuminate\Support\Facades\Route;

//COMPRAS

//SS
Route::group(['middleware' => 'auth'], function () {
    Route::get('/compras', function () {
        $module = 'compras';
        return view('layouts.app', compact('module'));
    })->name('compras');

    // COMPRAS - SC
    Route::get('/compras/sc/solicitudes', function () {
        $module = 'compras';
        return view('layouts.app', compact('module'));
    })->name('compras.solicitudes');

    Route::get('/compras/sc/admin-solicitudes', function () {
        $module = 'compras';
        return view('layouts.app', compact('module'));
    })->name('compras.sc.solicitudes.admin_solicitudes');


    Route::get('/compras/configuracion/proveedores', function () {
        $module = 'compras';
        return view('layouts.app', compact('module'));
    })->name('co.configuracion.proveedores');

    Route::get('/compras/configuracion/autorizaciones', function () {
        $module = 'compras';
        return view('layouts.app', compact('module'));
    })->name('co.configuracion.autorizaciones');

    Route::get('/compras/ordencompra/autorizacion/{idOrdenCompra}/{status}', function () {
        $module = 'compras';
        return view('layouts.app', compact('module'));
    })->name('co.ss.autorizacion_ordencompra');

    Route::get('/compras/configuracion/solicitudes/exportar', function () {
        $module = 'compras';
        return view('layouts.app', compact('module'));
    })->name('co.configuracion.exportar-solicitudes');
});
