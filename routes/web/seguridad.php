<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'auth'], function () {

    Route::get('/seguridad', function () {
        $module = 'seguridad';
        return view('layouts.app', compact('module'));
    })->name('seguridad');

    Route::get('/seguridad/salud/historial-clinico', function () {
        $module = 'seguridad';
        return view('layouts.app', compact('module'));
    })->name('seguridad.salud.hc');

    Route::get('/seguridad/accidentes', function () {
        $module = 'seguridad';
        return view('layouts.app', compact('module'));
    })->name('seguridad.accidentes');

});
