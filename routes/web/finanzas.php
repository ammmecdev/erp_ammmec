<?php

use Illuminate\Support\Facades\Route;

//FINANZAS
//CG

Route::group(['middleware' => 'auth'], function () {

    Route::get('/finanzas', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas');

    Route::get('/finanzas/cuentas-contables', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.cuentas_contables');

    Route::get('/finanzas/cuentas-internas', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.cuentas_internas');

    Route::get('/finanzas/cuentas-externas', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('fn.cg.cuentas_externas');

    Route::get('/finanzas/cg/solicitudes', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.solicitudes');

    Route::get('/finanzas/cg/admin-solicitudes', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.solicitudes.admin_solicitudes');

    Route::get('/finanzas/cg/auxiliar-movimientos', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.auxiliar_movimientos');

    Route::get('/finanzas/solicitud-gastos/autorizacion/{idSolicitud}/{status}', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.autorizacion_solicitud');

    Route::get('/finanzas/solicitud-gastos/dispersion/{idSolicitud}', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.dispersion_solicitud');

    Route::get('/finanzas/cg/configuracion/autorizaciones', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.autorizaciones');

    Route::get('/finanzas/configuracion/cuentas-contables', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.cuentas-contables');

    Route::get('/finanzas/configuracion/cuentas-internas', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.cuentas-internas');

    Route::get('/finanzas/configuracion/mano-obra', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.mano-obra');

    Route::get('/finanzas/configuracion/solicitudes/exportar', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.exportar-solicitudes');

    // POR MIENTRAS
    Route::get('/finanzas/configuracion/xml', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('finanzas.xml');

    Route::get('/finanzas/cg/indicadores/no-deducible', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('fn.cg.indicadores.no_deducible');

    Route::get('/finanzas/cg/indicadores/dias-transcurridos', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('fn.cg.indicadores.dias_transcurridos');

    Route::get('/finanzas/cg/indicadores/total-cuenta', function () {
        $module = 'finanzas';
        return view('layouts.app', compact('module'));
    })->name('fn.cg.indicadores.total_cuenta');

});
