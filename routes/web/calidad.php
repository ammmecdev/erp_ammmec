<?php

use Illuminate\Support\Facades\Route;

//NORMATIVIDAD

Route::group(['middleware' => 'auth'], function () {

	Route::get('/calidad', function () {
		$module = "calidad";
		return view('layouts.app', compact('module'));
	})->name('calidad');

	Route::get('/calidad/procesos', function () {
		$module = "calidad";
		return view('layouts.app', compact('module'));
	});

	Route::get('/calidad/procesos/{departamento}', function () {
		$module = "calidad";
		return view('layouts.app', compact('module'));
	});

	Route::get('/calidad/organigrama', function () {
		$module = "calidad";
		return view('layouts.app', compact('module'));
	});

	Route::get('/calidad/organizacion', function () {
		$module = "calidad";
		return view('layouts.app', compact('module'));
	});
});
