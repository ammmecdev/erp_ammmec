<?php

use Illuminate\Support\Facades\Route;

//TALENTO HUMANO

Route::group(['middleware' => 'auth'], function () {

    Route::get('/talentoHumano', function () {
        $module = 'talentoHumano';
        return view('layouts.app', compact('module'));
    })->name('talentoHumano');

    Route::get('/talentoHumano/empleados', function () {
        $module = 'talentoHumano';
        return view('layouts.app', compact('module'));
    })->name('th.empleados');
 
    Route::get('/talentoHumano/vp/solicitudes', function () {
        $module = 'talentoHumano';
        return view('layouts.app', compact('module'));
    })->name('th.vp.solicitudes');

    Route::get('/talentoHumano/configuracion/autorizaciones', function () {
        $module = 'talentoHumano';
        return view('layouts.app', compact('module'));
    })->name('th.config.autorizaciones');

    Route::get('/talentoHumano/configuracion/motivosAusencia', function () {
        $module = 'talentoHumano';
        return view('layouts.app', compact('module'));
    })->name('th.config.motivosAusencia');

    Route::get('/talentoHumano/configuracion/departamentos', function () {
        $module = 'talentoHumano';
        return view('layouts.app', compact('module'));
    })->name('th.config.departamentos');

    Route::get('/talentoHumano/configuracion/puestos', function () {
        $module = 'talentoHumano';
        return view('layouts.app', compact('module'));
    })->name('th.config.puestos');

    Route::get('/talentoHumano/configuracion/diasFestivos', function () {
        $module = 'talentoHumano';
        return view('layouts.app', compact('module'));
    })->name('th.config.diasFestivos');
});
