<?php

use Illuminate\Support\Facades\Route;

//ALMACÉN

//INVENTARIO ACTIVOS
Route::group(['middleware' => 'auth'], function () {

    Route::get('/almacen', function () {
        $module = 'almacen';
        return view('layouts.app', compact('module'));
    })->name('almacen');

    Route::get('/almacen/inventario/activos', function () {
        $module = 'almacen';
        return view('layouts.app', compact('module'));
    })->name('alm.inventario.activos');
    
    Route::get('/almacen/ss/solicitudes', function () {
        $module = 'almacen';
        return view('layouts.app', compact('module'));
    })->name('alm.ss.solicitudes');

     Route::get('/almacen/ss/admin-solicitudes', function () {
        $module = 'almacen';
        return view('layouts.app', compact('module'));
    })->name('alm.ss.admin_solicitudes');

});