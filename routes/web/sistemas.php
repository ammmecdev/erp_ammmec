<?php

use Illuminate\Support\Facades\Route;

//SISTEMAS

Route::group(['middleware' => 'auth'], function () {

    Route::get('/sistemas', function () {
        $module = 'sistemas';
        return view('layouts.app', compact('module'));
    })->name('sistemas');

    Route::get('/paleteria', function () {
        $module = null;
        return view('layouts.app', compact('module'));
    })->name('paleteria');

    Route::get('/sistemas/admin', function () {
        $module = 'sistemas';
        return view('layouts.app', compact('module'));
    })->name('sistemas.admin');

    Route::get('/sistemas/admin/cuentas', function () {
        $module = 'sistemas';
        return view('layouts.app', compact('module'));
    })->name('sistemas.admin.cuentas');

    Route::get('/sistemas/admin/licencias', function () {
        $module = 'sistemas';
        return view('layouts.app', compact('module'));
    })->name('sistemas.admin.licencias');

    Route::get('/sistemas/admin/equipos', function () {
        $module = 'sistemas';
        return view('layouts.app', compact('module'));
    })->name('sistemas.admin.equipos');
});
