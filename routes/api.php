<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//CONSULTAS GENERALES

//MODULOS
Route::get('modulos', 'Cadmin\ModuloController@getModulos');
Route::post('modulos', 'Cadmin\ModuloController@store');

//SUBMODULOS
Route::get('submodulos', 'Cadmin\SubmoduloController@getSubmodulos');
Route::get('submodulos/{modulo}', 'Cadmin\SubmoduloController@getSubmodulosByModulo');
Route::post('submodulos', 'Cadmin\SubmoduloController@store');
Route::put('submodulos/{submodulo}', 'Cadmin\SubmoduloController@update');

//CONFIGURACIÓN
Route::get('cf-cuentasExternas', 'Cadmin\ConfiguracionController@getConfCuentasExternas');
Route::post('cf-cuentasExternas', 'Cadmin\ConfiguracionController@store');
Route::put('cf-cuentasExternas', 'Cadmin\ConfiguracionController@update');

//SECCIONES
Route::get('secciones/{modulo}', 'Cadmin\SeccionController@getSeccionesByModulo');
Route::post('secciones', 'Cadmin\SeccionController@store');
Route::put('secciones/{seccion}', 'Cadmin\SeccionController@update');
Route::delete('secciones/{seccion}', 'Cadmin\SeccionController@delete');

//FUNCIONES
Route::post('funciones', 'Cadmin\FuncionController@store');
Route::put('funciones/{funcion}', 'Cadmin\FuncionController@update');
Route::delete('funciones/{funcion}', 'Cadmin\FuncionController@delete');

//ROLES
Route::get('roles', 'Cadmin\RoleController@getRoles');
Route::get('roles/{modulo}', 'Cadmin\RoleController@getRolesByModulo');
Route::post('roles', 'Cadmin\RoleController@store');
Route::put('roles/{role}', 'Cadmin\RoleController@update');
Route::delete('roles/{role}', 'Cadmin\RoleController@delete');

//ROLES FUNCIONES
Route::post('permisos-roles/{idRole}', 'Cadmin\PermisoRoleController@save');
Route::get('permisos-roles/{idRole}', 'Cadmin\PermisoRoleController@getPermisosByRole');

//USUARIOS ROLES
Route::post('usuarios-roles/{usuario}', 'Cadmin\UsuarioRoleController@save');
Route::get('usuarios/{usuario}/roles', 'UsuarioController@getRolesByUsuario');
Route::get('usuarios/{usuario}/permisos', 'UsuarioController@getPermisosByUsuario');
   
//USUARIOS
Route::put('usuarios', 'UsuarioController@update');
Route::post('usuarios', 'UsuarioController@store');
Route::get('usuarios/all', 'UsuarioController@getUsuariosAll');
Route::get('usuarios/activos', 'UsuarioController@getUsuarios');
Route::delete('usuarios/{idUsuario}', 'UsuarioController@delete');
Route::put('usuarios/usuario-activo', 'UsuarioController@updateActivo');
Route::get('usuario/active/{usuario}', 'UsuarioController@getUsuario');
Route::get('usuario/{usuario}', 'UsuarioController@getUsuarioSimple');
Route::get('usuario/empleado/tarjeta/{usuario}', 'UsuarioController@getTarjetaEmpleado');

//LOGOUT
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

//USUARIOS
Route::get('usuarios/select', 'UsuarioController@selectUsuarios');

//EMAILS
Route::get('send/email', 'ActividadController@sendEmail');

//INSTRUCCIONES
Route::get('/instrucciones/{idInstruccion}/empleados/{noEmpleado}', 'InstruccionController@getInstruccionByEmpleado');

Route::post('/instrucciones/{idInstruccion}/empleados/{noEmpleado}', 'InstruccionController@setInstruccionLeida');

//});