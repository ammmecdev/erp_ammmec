require('./bootstrap');

import Vue from 'vue'
import {BootstrapVue, BootstrapVueIcons} from 'bootstrap-vue'
import router from './router'
import store from './store'
import CKEditor from '@ckeditor/ckeditor5-vue' 
import Vuelidate from 'vuelidate'
import VueGoogleCharts from 'vue-google-charts'
import TRVLPicker from '@trvl/picker'
import InfiniteLoading from 'vue-infinite-loading'
import VCalendar from 'v-calendar'
import VueNumeric from 'vue-numeric'
import VuePaginate from 'vue-paginate'
import VueFormWizard from 'vue-form-wizard'
import Vue2Editor from "vue2-editor";

// import VueTimeline from "@growthbunker/vuetimeline";
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import 'vue-orgchart/dist/style.min.css'
import 'bootstrap-vue/dist/bootstrap-vue-icons.min.css'

window.bus = new Vue();
window.BaseUrl = '';

Vue.use(VCalendar);
Vue.use(BootstrapVue);
Vue.use(BootstrapVueIcons)
Vue.use(CKEditor);
Vue.use(Vuelidate);
Vue.use(VueGoogleCharts);
Vue.use(InfiniteLoading);
Vue.use(VueNumeric);
Vue.use(VuePaginate);
Vue.use(Vue2Editor);
Vue.use(VueFormWizard);

// QUITAR
Vue.component('servicios', require('./components/ventas/negocios/servicios/list.vue'));
Vue.component('negocios', require('./components/ventas/negocios/servicios/negocio.vue'));

// Componentes generales
Vue.component('app', require('./components/AppComponent.vue'));
// Vue.component('infinite-loading', require('vue-infinite-loading'));
Vue.component('system-info-component', require('./components/SystemInfoComponent.vue'));

// Vue.component('navbar-component', require('./components/NavbarComponent.vue'));
Vue.component('header-component', require('./components/HeaderComponent.vue'));
Vue.component('table-component', require('./components/TableComponent.vue'));
Vue.component('ckeditor-component', require('./components/CKEditor.vue'));
Vue.component('pagination-component', require('./components/PaginationComponent.vue'));
Vue.component('modal-filter-fields-component', require('./components/ModalFilterFieldsComponent.vue'));
Vue.component('filters-component', require('./components/FiltersComponent.vue'));
Vue.component('modal-component', require('./components/ModalComponent.vue'));
Vue.component('sidebar-component', require('./components/SidebarComponent.vue'));
Vue.component('form-delete', require('./components/FormDeleteComponent.vue'));
Vue.component('icon-text-component', require('./components/IconTextComponent.vue'));

// COMPONENTES - CENTRO DE COSTOS
Vue.component("tipo-cuenta-component", require("./components/TipoCuentaComponent.vue"));
Vue.component("cuentas-internas-component", require("./components/CuentasInternasComponent.vue"));
Vue.component("cuentas-externas-component", require("./components/CuentasExternasComponent.vue"));

// SELECT EMPLEADOS
Vue.component("empleados-select-component", require("./components/EmpleadosSelectComponent.vue"));

// COMPONENTE SELECT CLIENTES - VT 
Vue.component('vt-clientes-select', require('./components/ventas/clientes/SelectClientesComponent.vue'));
Vue.component('vt-clientes-modal-select', require('./components/ventas/contactos/SelectComponent.vue'));
Vue.component('vt-clientes-actividad-select', require('./components/ventas/actividades/SelectClienteActividadComponent.vue'));

// NOTIFICATIONS ---------------
Vue.component('notification-component', require('./components/notifications/NotificationComponent.vue'))
Vue.component('notifications-list-component', require('./components/notifications/NotificationListComponent.vue'))

// MODULOS INICIALES -----------
Vue.component('modules-list-component', require('./components/ModulesComponent.vue'));

// PLANES DE TRABAJO -----------
Vue.component('planes-form-plan-component', require('./components/planes/FormPlanComponent.vue'));
Vue.component('planes-form-seccion-component', require('./components/planes/FormSeccionComponent.vue'));
Vue.component('planes-form-actividad-component', require('./components/planes/FormActividadComponent.vue'));
Vue.component('planes-tr-seccion-component', require('./components/planes/TrSeccionComponent.vue'));
Vue.component('planes-form-usuarios-component', require('./components/planes/FormUsuariosComponent.vue'));
Vue.component('planes-form-responsables-component', require('./components/planes/FormResponsablesComponent.vue'));
Vue.component('planes-calendario-component', require('./components/planes/CalendarioComponent.vue'));
Vue.component('planes-resumen-component', require('./components/planes/ResumenComponent.vue'));
Vue.component('planes-resumen-tr-seccion', require('./components/planes/TrResumenSeccionComponent.vue'));
Vue.component('planes-form-new-actividad-component', require('./components/planes/FormNewActividadComponent.vue'));
Vue.component('planes-list-planes-component', require('./components/planes/ListPlanesComponent.vue'));

// CADMIN -----------------------
Vue.component("ad-dashboard-component", require("./components/cadmin/DashboardComponent.vue"));
Vue.component("ad-navbar-component", require("./components/cadmin/NavbarComponent.vue"));
Vue.component('ad-configuracion-sidebar', require('./components/cadmin/configuracion/SidebarContentComponent.vue'));
// Usuarios
Vue.component('ad-usuarios-form-usuario-component', require('./components/cadmin/usuarios/FormUsuarioComponent.vue'));
Vue.component('ad-usuarios-check-roles-component', require('./components/cadmin/usuarios/CheckRolesComponent.vue'));
// Estructura
Vue.component('ad-estructura-form-modulo-component', require('./components/cadmin/estructura/FormModuloComponent.vue'));
Vue.component('ad-estructura-form-submodulo-component', require('./components/cadmin/estructura/FormSubmoduloComponent.vue'));
Vue.component('ad-estructura-form-seccion-component', require('./components/cadmin/estructura/FormSeccionComponent.vue'));
Vue.component('ad-estructura-form-funcion-component', require('./components/cadmin/estructura/FormFuncionComponent.vue'));
Vue.component('ad-estructura-seccion-component', require('./components/cadmin/estructura/SeccionComponent.vue'));
Vue.component('ad-estructura-funcion-component', require('./components/cadmin/estructura/FuncionComponent.vue'));
// Roles
Vue.component('ad-roles-form-role-component', require('./components/cadmin/roles/FormRoleComponent.vue'));
Vue.component('ad-roles-check-permisos-component', require('./components/cadmin/roles/CheckPermisosComponent.vue'));
Vue.component('ad-roles-permiso-component', require('./components/cadmin/roles/PermisoComponent.vue'));

//TALENTO HUMANO ------------------
Vue.component('th-navbar-component', require('./components/talentoHumano/NavbarComponent.vue'));
// Empleados
Vue.component('th-form-empleado-component', require('./components/talentoHumano/empleados/FormEmpleadoComponent.vue'));
Vue.component('th-content-sidebar-empleado-component', require('./components/talentoHumano/empleados/ContentSidebarEmpleadoComponent.vue'));
// Configuracion
Vue.component('th-configuracion-sidebar', require('./components/talentoHumano/configuracion/SidebarContentComponent.vue'));
Vue.component('th-configuracion-areas-form-area-component', require('./components/talentoHumano/configuracion/puestos/FormAreaComponent.vue'));
Vue.component('th-configuracion-departamentos-form-departamento-component', require('./components/talentoHumano/configuracion/departamentos/FormDepartamentoComponent.vue'));
Vue.component('th-configuracion-puestos-form-puesto-component', require('./components/talentoHumano/configuracion/puestos/FormPuestoComponent.vue'));
Vue.component('th-configuracion-motivos-ausencia-form-component', require('./components/talentoHumano/configuracion/motivos_ausencia/FormMotivoAusenciaComponent.vue'));
Vue.component('th-vp-configuracion-autorizaciones-form-usuario-component', require('./components/talentoHumano/configuracion/autorizaciones/FormUsuarioComponent.vue'));
Vue.component('th-configuracion-dias-festivos-form-festivos-component', require('./components/talentoHumano/configuracion/dias_festivos/FormDiaFestivoComponent.vue'));
// Vacaciones y permisos
Vue.component('th-vp-solicitudes-tipos-component', require('./components/talentoHumano/vp/solicitudes/TiposSolicitudesComponent.vue'));
Vue.component('th-vp-solicitudes-list-solicitudes-component', require('./components/talentoHumano/vp/solicitudes/ListSolicitudesComponent.vue'));
Vue.component('th-vp-solicitudes-solicitud-component', require('./components/talentoHumano/vp/solicitudes/SolicitudComponent.vue'));
Vue.component('th-vp-solicitudes-calendario-component', require('./components/talentoHumano/vp/solicitudes/CalendarioComponent.vue'));
Vue.component('th-vp-solicitudes-detalles-dias-festivos-component', require('./components/talentoHumano/vp/solicitudes/DetallesDiasFestivosComponent.vue'));
Vue.component('th-vp-solicitudes-detalles-empleado-component', require('./components/talentoHumano/vp/solicitudes/DetallesEmpleadoComponent.vue'));
Vue.component('th-vp-solicitudes-form-solicitud-component', require('./components/talentoHumano/vp/solicitudes/FormSolicitudComponent.vue'));
Vue.component('th-vp-solicitudes-detalles-solicitud-component', require('./components/talentoHumano/vp/solicitudes/DetallesSolicitudComponent.vue'));
Vue.component('th-vp-solicitudes-filters-component', require('./components/talentoHumano/vp/solicitudes/FiltersSolicitudesComponent.vue'));
Vue.component('th-vp-content-sidebar-solicitud-component', require('./components/talentoHumano/vp/solicitudes/ContentSidebarSolicitudComponent.vue'));
Vue.component('th-vp-timeline-solicitud-component', require('./components/talentoHumano/vp/solicitudes/TimelineSolicitudComponent.vue'));
Vue.component('th-vp-autorizacion-solicitud-component', require('./components/talentoHumano/vp/solicitudes/FormAutorizacionComponent.vue'));

// COMPRAS -------------------------
Vue.component('co-component', require('./components/compras/DashboardComponent.vue'));
Vue.component('co-navbar-component', require('./components/compras/NavbarComponent.vue'));

// COMPRAS CONFIGURACIÓN
Vue.component('co-configuracion-sidebar', require('./components/compras/configuracion/SidebarContentComponent.vue'));
Vue.component('co-configuracion-proveedores-form-proveedor-component', require('./components/compras/configuracion/proveedores/FormProveedorComponent.vue'));

//COMPRAS - SC
Vue.component('co-sc-solicitudes-tipos-component', require('./components/compras/sc/solicitudes/TiposSolicitudesComponent.vue'));
Vue.component('co-sc-solicitudes-list-solicitudes-component', require('./components/compras/sc/solicitudes/ListSolicitudesComponent.vue'));
Vue.component('co-sc-solicitudes-solicitud-component', require('./components/compras/sc/solicitudes/SolicitudComponent.vue'));
Vue.component('co-sc-solicitudes-filters-component', require('./components/compras/sc/solicitudes/FiltersSolicitudesComponent.vue'));
Vue.component('co-sc-solicitudes-detalles-component', require('./components/compras/sc/solicitudes/DetallesComponent.vue'));
Vue.component('co-sc-orden-compra-component', require('./components/compras/sc/solicitudes/OrdenCompraComponent.vue'));
Vue.component('co-sc-tr-orden-compra-component', require('./components/compras/sc/solicitudes/TrOrdenCompraComponent.vue'));
Vue.component('co-sc-solicitudes-form-oc-component', require('./components/compras/sc/solicitudes/FormOrdenCompraComponent.vue'));
Vue.component('co-sc-content-sidebar-orden-compra-component', require('./components/compras/sc/solicitudes/ContentSidebarOCComponent.vue'));
Vue.component('co-sc-detalles-oc-component', require('./components/compras/sc/solicitudes/DetallesOrdenCompraComponent.vue'));
Vue.component('co-sc-form-autorizacion-onden-compra-component', require('./components/compras/sc/solicitudes/FormAutorizacionOrdenCompraComponent.vue'));

// COMPRAS - SC - ADMIN
Vue.component('co-sc-solicitudes-admin-card-solicitud-component', require('./components/compras/sc/solicitudes/admin/CardSolicitudComponent.vue'));

//ALMACEN --------------------------
Vue.component('alm-component', require('./components/almacen/DashboardComponent.vue'));
Vue.component('alm-navbar-component', require('./components/almacen/NavbarComponent.vue'));
Vue.component('alm-inventarios-form-activo-component', require('./components/almacen/inventarios/FormActivoComponent.vue'));
Vue.component('alm-activos-modal-select', require('./components/almacen/inventarios/SelectActivosComponent.vue'));
Vue.component('alm-recepcion-mercancia-component', require('./components/almacen/inventarios/RecepcionMercanciaComponent.vue'));
Vue.component('alm-form-recepcion-mercancia-component', require('./components/almacen/inventarios/FormRecepcionMercanciaComponent.vue'));

// ALMACEN - SS
Vue.component('alm-ss-solicitudes-tipos-component', require('./components/almacen/ss/solicitudes/TiposSolicitudesComponent.vue'));
Vue.component('alm-ss-solicitudes-form-solicitud-component', require('./components/almacen/ss/solicitudes/FormSolicitudComponent.vue'));
Vue.component('alm-ss-solicitudes-list-solicitudes-component', require('./components/almacen/ss/solicitudes/ListSolicitudesComponent.vue'));
Vue.component('alm-ss-solicitudes-solicitud-component', require('./components/almacen/ss/solicitudes/SolicitudComponent.vue'));
Vue.component('alm-ss-solicitudes-detalles-component', require('./components/almacen/ss/solicitudes/DetallesComponent.vue'));
Vue.component('alm-ss-solicitudes-articulos-component', require('./components/almacen/ss/solicitudes/ArticulosComponent.vue'));
Vue.component('alm-ss-tr-articulos-component', require('./components/almacen/ss/solicitudes/TrArticulosComponent.vue'));
Vue.component('alm-ss-solicitudes-content-sidebar-component', require('./components/almacen/ss/solicitudes/ContentSidebarComponent.vue'));
Vue.component('alm-ss-solicitudes-filters-component', require('./components/almacen/ss/solicitudes/FiltersSolicitudesComponent.vue'));
Vue.component('alm-ss-form-solicitar-compra-component', require('./components/almacen/ss/solicitudes/FormSolicitarCompraComponent.vue'));
Vue.component('alm-ss-estatus-articulo-component', require('./components/almacen/ss/solicitudes/EstatusArticuloComponent.vue'));
Vue.component('alm-ss-form-archivar-solicitud-component', require('./components/almacen/ss/solicitudes/FormArchivarComponent.vue'));

// ALMACEN - SS - ADMIN
Vue.component('alm-ss-solicitudes-admin-card-solicitud-component', require('./components/almacen/ss/solicitudes/admin/CardSolicitudComponent.vue'));


// VENTAS ----------------------------
Vue.component('vt-component', require('./components/ventas/VentasComponent.vue'));
Vue.component('vt-navbar-component', require('./components/ventas/NavbarComponent.vue'));

//VENTAS - NEGOCIOS

Vue.component('vt-negocios-header', require('./components/ventas/negocios/HeaderComponent.vue'));
Vue.component('vt-negocios-filters', require('./components/ventas/negocios/FiltersComponent.vue'));
Vue.component('vt-negocios-form-negocio', require('./components/ventas/negocios/FormNegocioComponent.vue'));
Vue.component('vt-negocios-form-estimacion', require('./components/ventas/negocios/FormEstimacionComponent.vue'));
Vue.component('vt-negocios-form-set-clave', require('./components/ventas/negocios/FormSetClaveComponent.vue'));
Vue.component('vt-negocios-buttons-menu', require('./components/ventas/negocios/ButtonsMenuComponent.vue'));
Vue.component('vt-negocios-buttons-negocios', require('./components/ventas/negocios/ButtonsNegociosComponent.vue'));
Vue.component('vt-negocios-form-negocio-leads', require('./components/ventas/negocios/FormNegocioLeadsComponent.vue'));

//VENTAS - NEGOCIOS - LISTAS

Vue.component('vt-negocios-lista', require('./components/ventas/negocios/lista/Lista.vue'));

//VENTAS -NEGOCIOS - SEGUIMIENTO

Vue.component('vt-negocios-seguimiento', require('./components/ventas/negocios/seguimiento/Seguimiento.vue'));
Vue.component('vt-negocios-seguimiento-table', require('./components/ventas/negocios/seguimiento/TableComponent.vue'));
Vue.component('vt-negocios-seguimiento-table-body', require('./components/ventas/negocios/seguimiento/NegocioFormComponent.vue'));
Vue.component('vt-form-change-contacto', require('./components/ventas/negocios/seguimiento/FormChangeContactoComponent.vue'));
Vue.component('vt-form-change-embudo', require('./components/ventas/negocios/seguimiento/FormChangeEmbudoComponent.vue'));
Vue.component('vt-form-change-dolar', require('./components/ventas/negocios/seguimiento/FormChangeDolarComponent.vue'));
Vue.component('vt-negocios-form-win-negocio', require('./components/ventas/negocios/seguimiento/FormWinNegocioComponent.vue'));
Vue.component('vt-negocios-form-set-causa', require('./components/ventas/negocios/seguimiento/FormSetCausaComponent.vue'));

//VENTAS - NEGOCIOS - EMBUDOS

Vue.component('vt-negocios-embudos', require('./components/ventas/negocios/embudos/Embudos.vue'));
Vue.component('vt-negocios-embudos-table', require('./components/ventas/negocios/embudos/TableComponent.vue'));
Vue.component('vt-negocios-embudos-card-negocio', require('./components/ventas/negocios/embudos/CardNegocioComponent.vue'));
Vue.component('vt-negocios-embudos-list-actividades', require('./components/ventas/negocios/embudos/ListActividadesPendientes.vue'));
Vue.component('vt-negocios-embudos-actividades-pendientes', require('./components/ventas/negocios/embudos/ActividadPendiente.vue'));
Vue.component('vt-negocios-button-completar-actividad', require('./components/ventas/negocios/embudos/ButtonActividadCompleta.vue'));
Vue.component('vt-negocios-embudos-etapas-head-component', require('./components/ventas/negocios/embudos/EtapasHeadComponent.vue'));

//VENTAS - NEGOCIOS - DETALLES

Vue.component('vt-negocios-detalles', require('./components/ventas/negocios/detalles/Detalles.vue'));
Vue.component('vt-negocios-detalles-header', require('./components/ventas/negocios/detalles/HeaderComponent.vue'));
Vue.component('vt-negocios-detalles-negocio', require('./components/ventas/negocios/detalles/DatosNegocioComponent.vue'));
Vue.component('vt-negocios-detalles-cliente', require('./components/ventas/negocios/detalles/DatosClienteComponent.vue'));
Vue.component('vt-negocios-detalles-contacto', require('./components/ventas/negocios/detalles/DatosContactoComponent.vue'));
Vue.component('vt-negocios-detalles-participantes', require('./components/ventas/negocios/detalles/ParticipantesComponent.vue'));
Vue.component('vt-negocios-detalles-seguidores', require('./components/ventas/negocios/detalles/SeguidoresComponent.vue'));
Vue.component('vt-negocios-detalles-resumen', require('./components/ventas/negocios/detalles/ResumenComponent.vue'));
Vue.component('vt-modal-participantes', require('./components/ventas/negocios/detalles/ModalParticipantesComponent.vue'));
Vue.component('vt-modal-seguidores', require('./components/ventas/negocios/detalles/ModalSeguidoresComponent.vue'));
Vue.component('vt-form-notas', require('./components/ventas/negocios/detalles/FormNotasComponent.vue'));
Vue.component('vt-negocios-detalles-timeline-actividad', require('./components/ventas/negocios/detalles/timeline/ActividadComponent.vue'));
Vue.component('vt-negocios-detalles-timeline-negocio', require('./components/ventas/negocios/detalles/timeline/NegocioComponent.vue'));
Vue.component('vt-negocios-detalles-timeline-cambio', require('./components/ventas/negocios/detalles/timeline/CambioComponent.vue'));
Vue.component('vt-negocios-detalles-timeline-contenido', require('./components/ventas/negocios/detalles/timeline/ContenidoComponent.vue'));
Vue.component('timeline-icon-component', require('./components/ventas/negocios/detalles/timeline/IconComponent.vue'));


//VENTAS - NEGOCIOS - NOTAS

Vue.component('vt-negocios-notas', require('./components/ventas/negocios/notas/Notas.vue'));
Vue.component('vt-negocios-notas-list', require('./components/ventas/negocios/notas/ListComponent.vue'));
Vue.component('vt-negocios-notas-content', require('./components/ventas/negocios/notas/ContentComponent.vue'));
Vue.component('vt-negocios-notas-info', require('./components/ventas/negocios/notas/InfoComponent.vue'));
Vue.component('vt-negocios-notas-lead-form', require('./components/ventas/negocios/notas/LeadFormComponent.vue'));
Vue.component('vt-negocios-notas-nota', require('./components/ventas/negocios/notas/NotaComponent.vue'));
Vue.component('vt-negocios-notas-img', require('./components/ventas/negocios/notas/ImgSelectNotaComponent.vue'));
Vue.component('vt-negocios-notas-form', require('./components/ventas/negocios/notas/FormNotasComponent.vue'));
Vue.component('vt-negocios-comentarios-historial-content', require('./components/ventas/negocios/notas/ContentHistorialComponent.vue'));

//VENTAS - NEGOCIOS - NOTAS

Vue.component('vt-negocios-detalles', require('./components/ventas/negocios/detalles/Detalles.vue'));

//VENTAS - ACTIVIDADES

Vue.component('vt-actividades', require('./components/ventas/actividades/Actividades.vue'));
Vue.component('vt-form-actividad', require('./components/ventas/actividades/FormActividadComponent.vue')); 

//VENTAS - CLIENTES

Vue.component('vt-clientes', require('./components/ventas/clientes/Clientes.vue'));
Vue.component('vt-form-cliente', require('./components/ventas/clientes/FormClienteComponent.vue'));
Vue.component('filter-cliente', require('./components/ventas/clientes/filterClienteComponent.vue'));


//VENTAS - CONTACTOS

Vue.component('vt-contactos', require('./components/ventas/contactos/Contactos.vue'));
Vue.component('vt-form-contacto', require('./components/ventas/contactos/FormContactoComponent.vue'));

//Costo PENDIETE

Vue.component('op-costos', require('./components/ventas/costos/CostosComponent.vue'));
Vue.component('op-costos-modal-costo', require('./components/ventas/costos/ModalCostoComponent.vue'));

//VENTAS - PRESUPUESTO

Vue.component('vt-presupuesto', require('./components/ventas/presupuesto/Presupuesto.vue'));
Vue.component('vt-presupuesto-table', require('./components/ventas/presupuesto/TableComponent.vue'));
Vue.component('vt-presupuesto-tr', require('./components/ventas/presupuesto/TrPresupuestoComponent.vue'));
Vue.component('vt-presupuesto-monto-mes', require('./components/ventas/presupuesto/MontoMesComponent.vue'));
Vue.component('vt-presupuesto-form-negocio', require('./components/ventas/presupuesto/NegocioFormComponent.vue'));
Vue.component('vt-presupuesto-form-total-mes', require('./components/ventas/presupuesto/FormTotalMesComponent.vue'));
Vue.component('vt-presupuesto-detalle-presupuesto', require('./components/ventas/presupuesto/DetallePresupuestoComponent.vue'));
Vue.component('vt-presupuesto-formato-fecha', require('./components/ventas/presupuesto/FormatoFechaComponent.vue'));
Vue.component('vt-presupuesto-formato-importe-mxn', require('./components/ventas/presupuesto/FormatoImporteComponent.vue'));
Vue.component('vt-presupuesto-nombre-mes', require('./components/ventas/presupuesto/NombreMesComponent.vue'));
Vue.component('vt-imagen-presupuesto', require('./components/ventas/presupuesto/ImagenPresupuestoComponent.vue'));
Vue.component('vt-td-total-mensual', require('./components/ventas/presupuesto/TdTotalMensualComponent.vue'));


//VENTAS - INDICADORES

Vue.component('vt-ind-sidebar', require('./components/ventas/indicadores/SidebarContentComponent.vue'));
Vue.component('vt-ind-filter-embudos', require('./components/ventas/indicadores/FilterEmbudosComponent.vue'));

Vue.component('vt-ind-clientes-facturados', require('./components/ventas/indicadores/clientes_facturados/ClientesFacturados.vue'));
Vue.component('vt-ind-clientes-facturados-table', require('./components/ventas/indicadores/clientes_facturados/ContentComponent.vue'));
Vue.component('vt-ind-clientes-facturados-form', require('./components/ventas/indicadores/clientes_facturados/FormCFComponent.vue'));

Vue.component('vt-ind-cartera-vencida', require('./components/ventas/indicadores/cartera_vencida/CarteraVencida.vue'));
Vue.component('vt-ind-cartera-vencida-content', require('./components/ventas/indicadores/cartera_vencida/ContentComponent.vue'));

Vue.component('vt-ind-crecimiento', require('./components/ventas/indicadores/crecimiento/Crecimiento.vue'));
Vue.component('vt-ind-crecimiento-content', require('./components/ventas/indicadores/crecimiento/ContentComponent.vue'));

Vue.component('vt-ind-porcentaje-ventas', require('./components/ventas/indicadores/porcentaje_ventas/PorcentajeVentas.vue'));
Vue.component('vt-ind-porcentaje-ventas-content', require('./components/ventas/indicadores/porcentaje_ventas/ContentComponent.vue'));

Vue.component('vt-ind-es', require('./components/ventas/indicadores/evaluacion_servicio/EvaluacionServicio.vue'));
Vue.component('vt-ind-es-content', require('./components/ventas/indicadores/evaluacion_servicio/ContentComponent.vue'));
Vue.component('vt-ind-es-td-valor', require('./components/ventas/indicadores/evaluacion_servicio/TdValorComponent.vue'));
Vue.component('vt-ind-es-form-cliente', require('./components/ventas/indicadores/evaluacion_servicio/FormClienteComponent.vue'));
Vue.component('vt-ind-es-tr-evaluacion', require('./components/ventas/indicadores/evaluacion_servicio/TrEvaluacionComponent.vue'));
Vue.component('vt-ind-es-tr-programado', require('./components/ventas/indicadores/evaluacion_servicio/TrProgramadoComponent.vue'));
Vue.component('vt-ind-es-td-programado', require('./components/ventas/indicadores/evaluacion_servicio/TdProgramadoComponent.vue'));


Vue.component('vt-indicadores-unidades-negocio-tr-mes-component', require('./components/ventas/indicadores/unidades_negocio/TrMesComponent.vue'));
Vue.component('vt-indicadores-unidades-negocio-table-unidad-component', require('./components/ventas/indicadores/unidades_negocio/TableUnidadComponent.vue'));
Vue.component('vt-indicadores-unidades-negocio-grafica-unidad-component', require('./components/ventas/indicadores/unidades_negocio/GraficaUnidadComponent.vue'));


Vue.component('vt-indicadores-presupuesto-table-resumen', require('./components/ventas/indicadores/presupuesto/TableResumenComponent.vue'));
Vue.component('vt-indicadores-presupuesto-tr-mes-component', require('./components/ventas/indicadores/presupuesto/TrMesComponent.vue'));
Vue.component('vt-indicadores-presupuesto-td-unidad-promedio-component', require('./components/ventas/indicadores/presupuesto/TdUnidadPromedioComponent.vue'));
Vue.component('vt-indicadores-presupuesto-td-unidad-presupuesto-component', require('./components/ventas/indicadores/presupuesto/TdUnidadPresupuestoComponent.vue'));

Vue.component('vt-indicadores-presupuesto-td-totales-component', require('./components/ventas/indicadores/presupuesto/TdTotalesComponent.vue'));
Vue.component('vt-indicadores-presupuesto-td-ingreso-unidad-component', require('./components/ventas/indicadores/presupuesto/TdIngresoUnidadComponent.vue'));
Vue.component('vt-indicadores-presupuesto-td-negocios-facturados2-component', require('./components/ventas/indicadores/presupuesto/TdNegociosFacturados2Component.vue'));
Vue.component('vt-indicadores-presupuesto-td-proyecciones-component', require('./components/ventas/indicadores/presupuesto/TdProyeccionesComponent.vue'));
Vue.component('vt-indicadores-presupuesto-td-crecimiento-component', require('./components/ventas/indicadores/presupuesto/TdCrecimientoComponent.vue'));

//SERVICIOS

// SERVICIOS - RA

Vue.component('op-component', require('./components/servicios/DashboardComponent.vue'));
Vue.component('op-navbar-component', require('./components/servicios/NavbarComponent.vue'));

Vue.component('op-ra-form-actividades-component', require('./components/servicios/ra/actividades/FormActividadComponent.vue'));
Vue.component('op-ra-form-actividades-cuenta-externa-component', require('./components/servicios/ra/actividades/CuentaExternaComponent.vue'));
Vue.component('op-ra-form-actividades-cuenta-interna-component', require('./components/servicios/ra/actividades/CuentaInternaComponent.vue'));
Vue.component('op-ra-actividades-modal-generate-pdf', require('./components/servicios/ra/actividades/GeneratePDFComponent.vue'));
Vue.component('op-ra-actividades-sidebar-reportes', require('./components/servicios/ra/actividades/SidebarReportesComponent.vue'));
Vue.component('op-ra-actividades-modal-export-xlsx', require('./components/servicios/ra/actividades/ExportXLSXComponent.vue'));

Vue.component('op-ra-configuracion-sidebar', require('./components/servicios/ra/configuracion/SidebarContentComponent.vue'));
Vue.component('op-ra-configuracion-semanas-semana', require('./components/servicios/ra/configuracion/semanas/SemanaComponent.vue'));

Vue.component('op-ra-indicadores-actividades-td-horas-reales-component', require('./components/servicios/ra/indicadores/actividades/TdHorasRealesComponent'));
Vue.component('op-ra-indicadores-actividades-td-horas-optimas-component', require('./components/servicios/ra/indicadores/actividades/TdHorasOptimasComponent'));
Vue.component('op-ra-indicadores-actividades-td-horas-diferencia-component', require('./components/servicios/ra/indicadores/actividades/TdHorasDiferenciaComponent'));
Vue.component('op-ra-indicadores-actividades-grafica-horas-component', require('./components/servicios/ra/indicadores/actividades/GraficasHorasComponent'));

// SERVICIOS - CO

Vue.component('op-co-costos-list-costos-component', require('./components/servicios/co/ListCostosComponent.vue'));
Vue.component('op-co-costos-costo-component', require('./components/servicios/co/CostoComponent.vue'));
Vue.component('op-co-costos-filters-component', require('./components/servicios/co/FiltersCostosComponent.vue'));
Vue.component('op-co-form-costo-component', require('./components/servicios/co/FormCostoComponent.vue'));

Vue.component('op-co-cuenta-planeada-component', require('./components/servicios/co/cuenta_planeada/CuentaPlaneadaComponent.vue'));
Vue.component('op-co-cuenta-real-component', require('./components/servicios/co/cuenta_real/CuentaRealComponent.vue'));

Vue.component('op-co-cp-material-insumos-component', require('./components/servicios/co/cuenta_planeada/MaterialInsumosComponent.vue'));
Vue.component('op-co-cp-mano-obra-component', require('./components/servicios/co/cuenta_planeada/ManoObraComponent.vue'));
Vue.component('op-co-cp-herramienta-equipos-component', require('./components/servicios/co/cuenta_planeada/HerramientaEquiposComponent.vue'));
Vue.component('op-co-cp-gastos-especiales-component', require('./components/servicios/co/cuenta_planeada/GastosEspecialesComponent.vue'));
Vue.component('op-co-cp-gastos-operacion-component', require('./components/servicios/co/cuenta_planeada/GastosOperacionComponent.vue'));
Vue.component('op-co-cp-observaciones-component', require('./components/servicios/co/cuenta_planeada/ObservacionesComponent.vue'));
Vue.component('op-co-cp-form-material-insumo-component', require('./components/servicios/co/cuenta_planeada/FormMaterialInsumoComponent.vue'));

Vue.component('op-co-cr-mano-obra-component', require('./components/servicios/co/cuenta_real/ManoObraComponent.vue'));
Vue.component('op-co-cr-gastos-operacion-component', require('./components/servicios/co/cuenta_real/GastosOperacionComponent.vue'));
Vue.component('op-co-cr-ind-cuenta-component', require('./components/servicios/co/cuenta_real/IndCuentaComponent.vue'));
Vue.component('op-co-cr-ind-seguridad-component', require('./components/servicios/co/cuenta_real/IndSeguridadComponent.vue'));


//VENTAS - CONFIGURACION

Vue.component('vt-configuracion-sidebar', require('./components/ventas/configuracion/SidebarContentComponent.vue'));
Vue.component('vt-configuracion-embudos', require('./components/ventas/configuracion/embudos/Embudos.vue'));

Vue.component('vt-form-embudo-component', require('./components/ventas/configuracion/embudos/FormEmbudoComponent.vue'));
Vue.component('vt-form-etapa-component', require('./components/ventas/configuracion/embudos/FormEtapaComponent.vue'));

//PROYECCIONES

Vue.component('vt-configuracion-proyecciones-td-monto-component', require('./components/ventas/configuracion/proyecciones/TdMontoComponent.vue'));
Vue.component('vt-configuracion-proyecciones-tr-unidades-component', require('./components/ventas/configuracion/proyecciones/TrUnidadesComponent.vue'));
Vue.component('vt-configuracion-proyecciones-td-totales-component', require('./components/ventas/configuracion/proyecciones/TdTotalesComponent.vue'));

//UNIDADES NEGOCIO

Vue.component('vt-form-unidad-negocio-component', require('./components/ventas/configuracion/unidades_negocio/FormUnidadNegocioComponent.vue'));

//NORMATIVIDAD

Vue.component('nr-component', require('./components/calidad/DashboardComponent.vue'));
Vue.component('nr-navbar-component', require('./components/calidad/NavbarComponent.vue'));

//PROCESOS

Vue.component('nr-mapeo-organizacional', require('./components/calidad/areas/MapeoOrganizacional.vue'));
Vue.component('nr-procesos-sidebar', require('./components/calidad/areas/SidebarContentComponent.vue'));

Vue.component('nr-mapeo-departamental', require('./components/calidad/areas/MapeoDepartamental.vue'));
Vue.component('nr-form-manual-departamental', require('./components/calidad/areas/FormManualDepartamentalComponent.vue'));

Vue.component('nr-img-no-procesos-component', require('./components/calidad/areas/procesos/ImgNoProcesosComponent.vue'));

Vue.component('nr-proceso-component', require('./components/calidad/areas/procesos/ProcesoComponent.vue'));
Vue.component('nr-form-proceso-component', require('./components/calidad/areas/procesos/FormProcesoComponent.vue'));

Vue.component('nr-procedimiento-component', require('./components/calidad/areas/procedimientos/ProcedimientoComponent.vue'));
Vue.component('nr-form-procedimiento-component', require('./components/calidad/areas/procedimientos/FormProcedimientoComponent.vue'));

Vue.component('nr-list-files-component', require('./components/calidad/areas/archivos/ListFilesComponent.vue'));
Vue.component('nr-file-component', require('./components/calidad/areas/archivos/FileComponent.vue'));
Vue.component('nr-form-file-component', require('./components/calidad/areas/archivos/FormFileComponent.vue'));

Vue.component('nr-organigrama', require('./components/calidad/organigrama/Organigrama.vue'));
Vue.component('nr-form-perfil-puesto-component', require('./components/calidad/organigrama/FormPerfilPuestoComponent.vue'));
Vue.component('nr-organigrama-info-component', require('./components/calidad/organigrama/InfoComponent.vue'));

Vue.component('nr-modal-perfil-component', require('./components/calidad/organigrama/ModalPerfilComponet.vue'));

Vue.component('nr-organizacion-file-component', require('./components/calidad/organizacion/FileComponent.vue'));
Vue.component('nr-organizacion-form-file-component', require('./components/calidad/organizacion/FormFileComponent.vue'));
Vue.component('nr-organizacion-list-files-component', require('./components/calidad/organizacion/ListFilesComponent.vue'));


//SISTEMAS
Vue.component('si-component', require('./components/sistemas/DashboardComponent.vue'));
Vue.component('si-navbar-component', require('./components/sistemas/NavbarComponent.vue'));

//Cuentas
Vue.component('si-form-cuenta-component', require('./components/sistemas/admin/cuentas/FormCuentaComponent.vue'));
Vue.component('si-form-password-servicio-component', require('./components/sistemas/admin/cuentas/FormPasswordServicioComponent.vue'));


//Licencias
Vue.component('si-form-licencia-component', require('./components/sistemas/admin/licencias/FormLicenciaComponent.vue'));
Vue.component('si-licencias-periodos-component', require('./components/sistemas/admin/licencias/PeriodosComponent.vue'));
Vue.component('si-licencias-tr-component', require('./components/sistemas/admin/licencias/TrLicenciaComponent.vue'));

//Equipos
Vue.component('si-form-equipo-component', require('./components/sistemas/admin/equipos/FormEquipoComponent.vue'));

//FINANZAS

Vue.component('fn-component', require('./components/finanzas/DashboardComponent.vue'));
Vue.component('fn-navbar-component', require('./components/finanzas/NavbarComponent.vue'));


//COMPROBACIÓN DE GASTOS

Vue.component('fn-gastos-solicitudes-filters-component', require('./components/finanzas/cg/solicitudes/FiltersSolicitudesComponent.vue'));
Vue.component('fn-gastos-solicitudes-tipos-component', require('./components/finanzas/cg/solicitudes/TiposSolicitudesComponent.vue'));
Vue.component('fn-gastos-solicitudes-timeline-archivos-component', require('./components/finanzas/cg/solicitudes/TimelineArchivosSolicitudComponent.vue'));

Vue.component('fn-gastos-solicitudes-list-solicitudes-component', require('./components/finanzas/cg/solicitudes/ListSolicitudesComponent.vue'));
Vue.component('fn-gastos-solicitudes-solicitud', require('./components/finanzas/cg/solicitudes/SolicitudComponent.vue'));
Vue.component('fn-gastos-solicitudes-detalles-component', require('./components/finanzas/cg/solicitudes/DetallesComponent.vue'));
Vue.component('fn-gastos-solicitudes-resumen-component', require('./components/finanzas/cg/solicitudes/ResumenComponent.vue'));

Vue.component('fn-gastos-solicitudes-comprobaciones-component', require('./components/finanzas/cg/solicitudes/ComprobacionComponent.vue'));
Vue.component('fn-gastos-solicitudes-table-comprobaciones-component', require('./components/finanzas/cg/solicitudes/TableComprobacionesComponent.vue'));
Vue.component('fn-gastos-solicitudes-content-sidebar-comprobaciones-component', require('./components/finanzas/cg/solicitudes/ContentSidebarComprobacionComponent.vue'));

Vue.component('fn-gastos-solicitudes-form-solicitud-component', require('./components/finanzas/cg/solicitudes/FormSolicitudComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-comprobacion-component', require('./components/finanzas/cg/solicitudes/FormComprobacionComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-timbrar-component', require('./components/finanzas/cg/solicitudes/FormTimbrarComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-autorizacion-component', require('./components/finanzas/cg/solicitudes/FormAutorizacionSolicitudComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-aceptacion-asignacion-component', require('./components/finanzas/cg/solicitudes/FormAceptacionAsignacionSolicitudComponent.vue'));

Vue.component('fn-gastos-solicitudes-form-dispersion-component', require('./components/finanzas/cg/solicitudes/FormDispersionSolicitudComponent.vue'));
Vue.component('fn-gastos-solicitudes-dispersion-component', require('./components/finanzas/cg/solicitudes/DispersionComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-periodo-dispersion-component', require('./components/finanzas/cg/solicitudes/FormPeriodoDispersionComponent.vue'));
Vue.component('fn-gastos-solicitudes-fondeo-component', require('./components/finanzas/cg/solicitudes/FondeoComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-archivo-component', require('./components/finanzas/cg/solicitudes/FormArchivoComponent.vue'));

Vue.component('fn-gastos-solicitudes-disposicion-efectivo-component', require('./components/finanzas/cg/solicitudes/DisposicionEfectivoComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-disposicion-component', require('./components/finanzas/cg/solicitudes/FormDisposicionEfectivoComponent.vue'));

Vue.component('fn-gastos-solicitudes-reembolso-component', require('./components/finanzas/cg/solicitudes/ReembolsoComponent.vue'));
Vue.component('fn-gastos-solicitudes-decremento-component', require('./components/finanzas/cg/solicitudes/DecrementoComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-reembolso-component', require('./components/finanzas/cg/solicitudes/FormReembolsoSolicitudComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-decremento-component', require('./components/finanzas/cg/solicitudes/FormDecrementoSolicitudComponent.vue'));
Vue.component('fn-gastos-solicitudes-reembolso-decremento-component', require('./components/finanzas/cg/solicitudes/ReembolsoDecrementoComponent.vue'));
Vue.component('fn-gastos-solicitudes-form-autorizacion-comprobacion-component', require('./components/finanzas/cg/solicitudes/FormAutorizacionComprobacionComponent.vue'));

Vue.component('fn-gastos-solicitudes-resumen-movil-component', require('./components/finanzas/cg/solicitudes/movil/ResumenComponent.vue'));
Vue.component('fn-gastos-solicitudes-detalles-movil-component', require('./components/finanzas/cg/solicitudes/movil/DetallesComponent.vue'));
Vue.component('fn-gastos-solicitudes-comprobaciones-movil-component', require('./components/finanzas/cg/solicitudes/movil/TableComprobacionesComponent.vue'));

Vue.component('fn-gastos-solicitudes-form-enviar-comprobacion-component', require('./components/finanzas/cg/solicitudes/FormEnviarComprobacionComponent.vue'));
Vue.component('fn-gastos-solicitudes-img-seleccionar-solicitud-component', require('./components/finanzas/cg/solicitudes/ImgSeleccionarSolicitudComponent.vue'));
Vue.component('fn-gastos-solicitudes-comprobaciones-opciones-component', require('./components/finanzas/cg/solicitudes/ComprobacionOpcionesComponent.vue'));
Vue.component('fn-gastos-solicitudes-bitacoras-component', require('./components/finanzas/cg/solicitudes/BitacorasComponent.vue'));
Vue.component('fn-gastos-solicitudes-bitacoras-bitacora-component', require('./components/finanzas/cg/solicitudes/BitacoraComponent.vue'));

Vue.component('fn-configuracion-sidebar', require('./components/finanzas/configuracion/SidebarContentComponent.vue'));

Vue.component('fn-configuracion-cuentas-contables-check-component', require('./components/finanzas/configuracion/cuentas_contables/CheckCuentasComponent.vue'));
Vue.component('fn-configuracion-cuentas-internas-check-component', require('./components/finanzas/configuracion/cuentas_internas/CheckCuentasComponent.vue'));
Vue.component('fn-cuentas-internas-form-cuenta-component', require('./components/finanzas/cuentas_internas/FormCuentaInternaComponent.vue'));

Vue.component('fn-cg-files-component', require('./components/finanzas/cg/solicitudes/ArchivoComprobacionComponent.vue'));
Vue.component('fn-cg-configuracion-autorizaciones-form-usuario-component', require('./components/finanzas/cg/configuracion/autorizaciones/FormUsuarioComponent.vue'));

Vue.component('fn-cg-movimientos-empleados-component', require('./components/finanzas/cg/auxiliar_movimientos/MovimientosByEmpleadoComponent.vue'));
Vue.component('fn-cg-movimiento-empleado-component', require('./components/finanzas/cg/auxiliar_movimientos/MovimientoEmpleadoComponent.vue'));
Vue.component('fn-cg-movimientos-cuentas-component', require('./components/finanzas/cg/auxiliar_movimientos/MovimientosByCuentaComponent.vue'));
Vue.component('fn-cg-movimiento-cuenta-component', require('./components/finanzas/cg/auxiliar_movimientos/MovimientoCuentaComponent.vue'));

Vue.component('fn-cg-solicitudes-admin-card-solicitud-component', require('./components/finanzas/cg/solicitudes/admin/CardSolicitudComponent.vue'));

Vue.component('fn-cg-ind-sidebar-content-component', require('./components/finanzas/cg/indicadores/SidebarContentComponent.vue'));
Vue.component('fn-cg-ind-filters-component', require('./components/finanzas/cg/indicadores/FiltersComponent.vue'));
Vue.component('fn-cg-ind-no-deducible-content', require('./components/finanzas/cg/indicadores/no_deducible/ContentComponent.vue'));
Vue.component('fn-cg-ind-no-deducible-empleado', require('./components/finanzas/cg/indicadores/no_deducible/EmpleadoComponent.vue'));
Vue.component('fn-cg-ind-no-deducible-cuenta', require('./components/finanzas/cg/indicadores/no_deducible/CuentaComponent.vue'));
Vue.component('fn-cg-ind-total-cuentas-content', require('./components/finanzas/cg/indicadores/total_cuenta/ContentComponent.vue'));
Vue.component('fn-cg-lista-solicitudes-component', require('./components/finanzas/cg/indicadores/dias_transcurridos/ListaSolicitudesComponent.vue'));


// SEGURIDAD
Vue.component("se-component", require("./components/seguridad/DashboardComponent.vue"));
Vue.component("se-navbar-component", require("./components/seguridad/NavbarComponent.vue"));

// SEGURIDAD - SALUD

Vue.component("se-salud-form-hc-component",	require("./components/seguridad/salud/hc/FormHistorialComponent.vue"));
Vue.component("se-salud-table-hc-component", require("./components/seguridad/salud/hc/TableHistorialComponent.vue"));

// SEGURIDAD - ACCIDENTE

Vue.component("se-accidentes-form-accidente-component", require("./components/seguridad/accidentes/FormAccidenteComponent.vue"));
Vue.component("se-accidentes-table-lista-component", require("./components/seguridad/accidentes/TableListAccidenteComponent.vue"));
Vue.component("se-accidentes-table-seguimiento-component", require("./components/seguridad/accidentes/TableSeguimientoComponent.vue"));



//TICKETS


Vue.component("tk-navbar-component", require("./components/tickets/NavbarComponent.vue"));

// Vue.component('navbar-component', require('./components/tickets/NavbarComponent.vue'));

Vue.component('modal-show-image-component', require('./components/tickets/tickets/ModalShowImageComponent.vue'));
Vue.component('message-image-component', require('./components/tickets/tickets/MessageImageComponent.vue'));

Vue.component('show-ticket', require('./components/tickets/tickets/ShowTicket.vue'));
Vue.component('info-ticket-component', require('./components/tickets/tickets/InfoTicketComponent.vue'));
Vue.component('message-component', require('./components/tickets/tickets/MessageComponent.vue'));
Vue.component('list-tickets-component', require('./components/tickets/tickets/ListTicketsComponent.vue'));
Vue.component('form-message-component', require('./components/tickets/tickets/FormMessageComponent.vue'));
Vue.component('form-report-component', require('./components/tickets/tickets/FormReportComponent.vue'));

Vue.component('no-ticket-active-component', require('./components/tickets/tickets/NoTicketActiveComponent.vue'));
Vue.component('form-add-participant-component', require('./components/tickets/tickets/FormAddParticipantComponent.vue'));
Vue.component('picture-user-for-participant-component', require('./components/tickets/tickets/PictureUserForParticipantComponent.vue'));

Vue.component('ticket-active-component', require('./components/tickets/tickets/TicketActiveComponent.vue'));
Vue.component('ticket-component', require('./components/tickets/tickets/TicketComponent.vue'));
Vue.component('form-new-ticket-component', require('./components/tickets/tickets/FormNewTicketComponent.vue'));
Vue.component('form-edit-tservice-component', require('./components/tickets/tickets/FormEditTServiceComponent.vue'));
Vue.component('form-escalate-ticket-component', require('./components/tickets/tickets/FormEscalateTicketComponent.vue'));
Vue.component('form-programar-ticket-component', require('./components/tickets/tickets/FormProgramarTicketComponent.vue'));
Vue.component('form-status-ticket-component', require('./components/tickets/tickets/FormStatusTicketComponent.vue'));
Vue.component('img-no-messages-component', require('./components/tickets/tickets/ImgNoMessages.vue'));
Vue.component('list-participants-component', require('./components/tickets/tickets/ListParticipantsComponent.vue'));
Vue.component('participant-component', require('./components/tickets/tickets/ParticipantComponent.vue'));
Vue.component('list-files-component', require('./components/tickets/tickets/ListFilesComponent.vue'));
Vue.component('file-component', require('./components/tickets/tickets/FileComponent.vue'));

// // INDICADORES

Vue.component('indicadores-tickets', require('./components/tickets/indicators/Indicadores.vue'));
Vue.component('indicadores-productividad', require('./components/tickets/indicators/Productividad.vue'));
Vue.component('indicadores-tiempo-solucion', require('./components/tickets/indicators/TiempoSolucion.vue'));
Vue.component('indicadores-tickets-por-servicio', require('./components/tickets/indicators/TicketsPorServicio.vue'));
Vue.component('indicadores-tickets-por-asunto', require('./components/tickets/indicators/TicketsPorAsunto.vue'));
Vue.component('indicadores-tickets-por-departamento', require('./components/tickets/indicators/TicketsPorDepartment.vue'));
Vue.component('indicadores-promedio', require('./components/tickets/indicators/PromedioPorDias.vue'));

//CONFIGURACION

Vue.component('tk-configuracion-sidebar', require('./components/tickets/configuracion/SidebarContentComponent.vue'));

//SERVICES
Vue.component('tk-configuracion-form-service-component', require('./components/tickets/configuracion/services/FormServiceComponent.vue'));
Vue.component('tk-configuracion-form-subject-component', require('./components/tickets/configuracion/services/FormSubjectComponent.vue'));
Vue.component('tk-configuracion-form-responsible-component', require('./components/tickets/configuracion/services/FormResponsibleComponent.vue'));

Vue.component('tk-ind-sidebar', require('./components/tickets/indicators/Sidebar.vue'));


//NOTIFICATIONS
Vue.component('notifications', require('./components/tickets/notifications/Notifications.vue'))
Vue.component('notification-component', require('./components/tickets/notifications/NotificationComponent.vue'))
Vue.component('notifications-list-component', require('./components/tickets/notifications/ListNotificationsComponent.vue'))


// Vue.component('notifications-all-component', require('./components/notifications/AllNotificationsComponent.vue'))

const app = new Vue({
    el: '#app',
    store,
    router,
    methods: {
    	logout(){
    		document.getElementById('logout-form').submit();
    	}
    }
});
