<template> 
	<b-container fluid style="height: calc(100vh - 56px); overflow: auto">
		<b-card no-body class="mt-3 pt-2 pb-3">
			<b-row class="text-center">
				<b-col lg="2"></b-col>
				<b-col lg="8">
					<div class="mt-3">
						<h1 class="display-1" 
							style="font-size: 30px;" 
							align="center">			
							Catálogo de Licencias
						</h1>
					</div>
				</b-col>
				<b-col lg="2">
    				<b-button 
    					size="sm" class="float-right mt-3 mr-4" 
    					@click="licencia=null; titleModalLicencia='Agregar Licencia'"
    					v-b-modal.modal-licencia>
    					<i class="fas fa-user-plus mx-1"></i>
    					Agregar una licencia
    				</b-button>
    				<modal-component 
    					:title="titleModalLicencia" 
    					:id="'modal-licencia'"
    					:hideFooter="true">
    					<template slot="content"> 
	    					<si-form-licencia-component :licencia="licencia" />
	    				</template>
	    			</modal-component>
				</b-col>
			</b-row>
		</b-card>
		<b-row class="my-3">
			<b-col lg="2">
				<div class="input-group w-100 bg-white">
	    			<div class="input-group-prepend bg-light">
                        <div class="mr-3 ml-2">
                            <small><strong>Categorias:</strong></small>
                        </div>
                    </div>
                   <b-form-select 
                   		class="border-0"
						size="sm" 
						:options="categoriasSelect"
						v-model="categoriaSelected">	
							<template v-slot:first>
								<option :value="null">
									Todas
								</option>
							</template>				
						</b-form-select>
                 </div>
			</b-col>
			<b-col lg="2">
				<div class="input-group w-100 bg-white">
	    			<div class="input-group-prepend bg-light">
                        <div class="mr-3 ml-2">
                            <small><strong>Ver:</strong></small>
                        </div>
                    </div>
                   <b-form-select 
                   		class="border-0"
						size="sm" 
						:options="status"
						v-model="statusSelected">					
					</b-form-select>
                 </div>
			</b-col>
			<b-col lg="5"></b-col>
			<b-col lg="3">
				<div class="input-group w-100 bg-white">
	    			<div class="input-group-prepend bg-light mr-1 ml-3">
                        <div class="pt-1 bg-white">
                            <i class="fas fa-search"></i>
                        </div>
                    </div>
                    <b-form-input id="input-search" size="sm" class="form-control border-0" placeholder="Escribe tu busqueda aquí" style="" v-model="search"></b-form-input>
                 </div>
			</b-col>
		</b-row>
		<b-card class="mt-3 p-0" no-body>
			<b-table-simple hover caption-top responsive class="mb-0">
				<b-thead>
					<b-tr class="text-center bg-secondary text-white">
						<b-th width="100px">Status</b-th>
						<b-th width="380px" class="text-left pl-4">Licencia</b-th>
						<b-th width="100px"></b-th>
						<b-th>Cuenta</b-th>
						<b-th>Ultimo Págo</b-th>
						<b-th>Fecha Vencimiento</b-th>
						<b-th>Periodo</b-th>
						<b-th>Tipo Pago</b-th>
						<b-th>Monto</b-th>

					</b-tr>
				</b-thead>
				<b-tbody>
					<template v-for="categoria in categoriasByFilter">
						<b-tr>
							<b-th class="bg-light"></b-th>
							<b-th colspan="6" class="bg-light pl-4"><h3 class="mb-0">{{ categoria.nombre }}</h3></b-th>
							<b-th class="bg-light text-right"><h5 class="mt-1 mb-0">Total</h5></b-th>

							<b-th class="bg-light text-center"><h5 class="mt-1 mb-0">$ {{  categoria.total }}</h5></b-th>
						</b-tr>
						<si-licencias-tr-component v-for="(licencia,index) in categoria.licencias" :key="licencia.idLicencia" :licencia="licencia" @openSidebar="openSidebar(categoria, licencia)" :licenciaActiva="licenciaActiva" />
					</template>
				</b-tbody>
			</b-table-simple>
		</b-card>	
		<b-row class="my-3">
			<b-col md="1"></b-col>
             <b-col md="9"></b-col>
             <b-col md="2"></b-col>
		</b-row>
		<div>
			<b-sidebar
			id="sidebar-backdrop"
			backdrop-variant="transparent"
			class="bg-light"
			shadow
			right
			bg-variant="dark"  
			text-variant="white"
			width="630px"
			:visible="showSidebar"
			@hidden="hiddenSidebar"
			>
			<div class="px-5 py-2" v-if="licenciaActiva && categoriaActiva">
				<h2>{{ categoriaActiva.nombre }}</h2>
				<h4>{{ licenciaActiva.nombre }}</h4>
				<h6><strong>Costo:</strong> $ {{ licenciaActiva.monto }} - {{ licenciaActiva.periodoPago }}</h6>
				<h6><strong>Tipo de pago:</strong> {{ licenciaActiva.tipoPago }}</h6>
				<div class="mt-3">
					<b-nav pills>
						<b-nav-item 
							@click="sectionActivate = 'Periodos'"
							:active="sectionActivate == 'Periodos'"
							style="cursor:pointer">
							Periodos
						</b-nav-item>
						<b-nav-item 
							@click="sectionActivate = 'Cuentas'"
							:active="sectionActivate == 'Cuentas'"
							style="cursor:pointer">
							Cuentas
						</b-nav-item>
						<b-nav-item 
							@click="sectionActivate = 'Equipos'"
							:active="sectionActivate == 'Equipos'"
							style="cursor:pointer">
							Equipos
						</b-nav-item>
					</b-nav>
				</div>
				<si-licencias-periodos-component v-if="sectionActivate == 'Periodos'" :idLicencia="licenciaActiva.idLicencia" />
				<si-licencias-cuentas-component v-if="sectionActivate == 'Cuentas'" />
				<si-licencias-equipos-component v-if="sectionActivate == 'Equipos'" />
			</div>
		</b-sidebar>
	</div>
	</b-container>
</template>
<script>
	import {mapActions, mapState} from 'vuex'
    export default {
        data() {
            return {
            	sectionActivate: 'Periodos',
            	licencia: null,
            	titleModalLicencia: 'Agregar Licencia',
		     	categoriaSelected: null,
		     	search: '',
		     	status: [{text:'Licencias Activas', value:1}, {text:'Licencias Inactivas', value:0}, {text:'Todas', value:2}],
		     	statusSelected: 2,
		     	labelCopiar: 'Copiar',
		     	isBusy: true,
		     	selected: [],
		     	filter: null,
		     	licenciaActiva: null,
		     	categoriaActiva: null,
		     	showSidebar: false,
		    };
        },
        mounted()
        {
        	this.getCategoriasSelect()
    		this.getCategoriasWhitLicencias()
        },
        methods: {
        	...mapActions('licencias_software', ['getCategoriasSelect','getCategoriasWhitLicencias']),
        	copiar(valor) {
        		var aux = document.createElement("input");
        		aux.setAttribute("value", valor);
        		document.body.appendChild(aux);
        		aux.select();
        		document.execCommand("copy");
        		document.body.removeChild(aux);
        		this.labelCopiar = 'Copiado!'
        		setTimeout(this.restLabCopiar,1500)
        	},
        	restLabCopiar()
        	{
        		this.labelCopiar='Copiar'
        	},
        	onRowSelected(items) {
        		this.selected = items
        	},
        	openSidebar(categoria, licencia)
        	{
        		this.categoriaActiva = categoria
        		this.licenciaActiva = licencia
        		this.showSidebar = true
        	},
        	hiddenSidebar()
        	{
        		this.licenciaActiva = null
        		this.categoriaActiva = null
        		this.sectionActivate = 'Periodos'
        	}
        },
        computed: {
        	...mapState('licencias_software', ['categorias', 'categoriasSelect']),
        	categoriasByFilter()
        	{
        		if(this.categoriaSelected == null)
        			return this.categorias
        		else{
        			return this.categorias.filter(
                    	categoria => 
                    		categoria.idCategoria == this.categoriaSelected
                    );
        		}
        	},
        	// cuentasByStatus()
        	// {
        	// 	return this.cuentasByEmpresa.filter(
        	// 			cuenta =>
        	// 				cuenta.status == this.statusSelected
        	// 		)
        	// },
        },
        watch: {
        	licenciaActiva()
        	{
        		if(!this.licenciaActiva)
        			this.showSidebar = false        			
        	}
        }
    }
</script>
<style>
	.bg-green-light-2{
		background-color: #e0ffe1;
		color:#28a745;
	}
</style>