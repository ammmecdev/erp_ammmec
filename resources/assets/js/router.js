import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter)
var BaseUrl = '';

export default new VueRouter({
	routes: [
		//TALENTO HUMANO
		{
			path: BaseUrl + "/talentoHumano",
			name: "th",
			component: require("./components/talentoHumano/DashboardComponent.vue")
		},
		{
			path: BaseUrl + "/talentoHumano/empleados",
			name: "th.empleados",
			component: require("./components/talentoHumano/empleados/Empleados.vue")
		},
		{
			path: BaseUrl + "/talentoHumano/vp/solicitudes",
			name: "th.vp.solicitudes",
			component: require("./components/talentoHumano/vp/solicitudes/Solicitudes.vue")
		},
		{
			path: BaseUrl + "/talentoHumano/configuracion/autorizaciones",
			name: "th.config.autorizaciones",
			component: require("./components/talentoHumano/configuracion/autorizaciones/Autorizaciones.vue")
		},
		{
			path: BaseUrl + "/talentoHumano/configuracion/motivosAusencia",
			name: "th.config.motivosAusencia",
			component: require("./components/talentoHumano/configuracion/motivos_ausencia/MotivosAusencia.vue")
		},
		{
			path: BaseUrl + "/talentoHumano/configuracion/departamentos",
			name: "th.config.departamentos",
			component: require("./components/talentoHumano/configuracion/departamentos/Departamentos.vue")
		},
		{
			path: BaseUrl + "/talentoHumano/configuracion/puestos",
			name: "th.config.puestos",
			component: require("./components/talentoHumano/configuracion/puestos/Puestos.vue")
		},
		{
			path: BaseUrl + "/talentoHumano/configuracion/diasFestivos",
			name: "th.config.diasFestivos",
			component: require("./components/talentoHumano/configuracion/dias_festivos/DiasFestivos.vue")
		},


		// ALMACEN
		{
			path: BaseUrl + "/almacen",
			name: "alm",
			component: require("./components/almacen/DashboardComponent.vue")
		},
		// ALMACÉN - SS
		{
			path: BaseUrl + "/almacen/ss/solicitudes",
			name: "alm.ss.solicitudes",
			component: require("./components/almacen/ss/solicitudes/Solicitudes.vue")
		},
		{
			path: BaseUrl + "/almacen/ss/admin-solicitudes",
			name: "alm.ss.admin_solicitudes",
			component: require("./components/almacen/ss/solicitudes/admin/SolicitudesAdmin.vue")
		},
		// ALM - INVENTARIO - ACTIVOS
		{
			path: BaseUrl + "/almacen/inventario/activos",
			name: "alm.inventario.activos",
			component: require("./components/almacen/inventarios/Activos.vue")
		},
		

		// COMPRAS
		{
			path: BaseUrl + "/compras",
			name: "co",
			component: require("./components/compras/DashboardComponent.vue")
		},
		{
			path: BaseUrl + "/compras/configuracion/proveedores",
			name: "co.configuracion.proveedores",
			component: require("./components/compras/configuracion/proveedores/Proveedores.vue")
		},
		{
			path: BaseUrl + "/compras/configuracion/autorizaciones",
			name: "co.configuracion.autorizaciones",
			component: require("./components/compras/configuracion/autorizaciones/Autorizaciones.vue")
		},
		// {
		// 	path: BaseUrl + "/compras/ordencompra/autorizacion/:idOrdenCompra/:status",
		// 	name: "co.ss.autorizacion_ordencompra",
		// 	component: require("./components/compras/ss/solicitudes/FormAutorizacionOrdenCompraComponent.vue")
		// },
		{
			path: BaseUrl + "/compras/configuracion/solicitudes/exportar",
			name: "co.configuracion.solicitudes.exportar",
			component: require("./components/compras/configuracion/exportar_solicitudes/Exportar.vue")
		},
		// COMPRAS - SC
		{
			path: BaseUrl + "/compras/sc/solicitudes",
			name: "co.sc.solicitudes",
			component: require("./components/compras/sc/solicitudes/Solicitudes.vue")
		},
		{
			path: BaseUrl + "/compras/sc/admin-solicitudes",
			name: "co.sc.admin_solicitudes",
			component: require("./components/compras/sc/solicitudes/admin/SolicitudesAdmin.vue")
		},
		

		//PLANES
		{
			path: BaseUrl + "/planes",
			name: "planes",
			component: require("./components/planes/Planes.vue")
		},


		//CADMIN
		{
			path: BaseUrl + "/cadmin",
			name: "ad",
			component: require("./components/cadmin/DashboardComponent.vue")
		},
		{
			path: BaseUrl + "/cadmin/estructura",
			name: "ad.estructura",
			component: require("./components/cadmin/estructura/Estructura.vue")
		},
		{
			path: BaseUrl + "/cadmin/roles",
			name: "ad.roles",
			component: require("./components/cadmin/roles/Roles.vue")
		},
		{
			path: BaseUrl + "/cadmin/usuarios",
			name: "ad.usuarios",
			component: require("./components/cadmin/usuarios/Usuarios.vue")
		},
		{
			path: BaseUrl + "/cadmin/configuracion/cuentasExternas",
			name: "ad.cf.cuentas_externas",
			component: require("./components/cadmin/configuracion/cuentas_externas/CuentasExternas")
		},
		{
			path: BaseUrl + "/ventas",
			name: "ventas",
			component: require("./components/ventas/VentasComponent")
		},
		{
			path: BaseUrl + "/ventas/negocios/seguimiento",
			name: "negocios.seguimiento",
			component: require("./components/ventas/negocios/seguimiento/Seguimiento")
		},
		{
			path: BaseUrl + "/ventas/negocios/embudos",
			name: "negocios.embudos",
			component: require("./components/ventas/negocios/embudos/Embudos")
		},
		{
			path: BaseUrl + "/ventas/negocios/lista",
			name: "negocios.lista",
			component: require("./components/ventas/negocios/lista/Lista")
		},
		{
			path: BaseUrl + "/ventas/negocios/notas",
			name: "negocios.notas",
			component: require("./components/ventas/negocios/notas/Notas")
		},
		{
			path: BaseUrl + "/ventas/negocios/notas/:idNota",
			name: "negocios.notas.idnota",
			component: require("./components/ventas/negocios/notas/Notas")
		},
		{
			path: BaseUrl + "/ventas/negocios/:idNegocio",
			name: "negocios.detalles.idNegocio",
			component: require("./components/ventas/negocios/detalles/Detalles")
		},
		{
			path: BaseUrl + "/ventas/negocios/detalles",
			name: "negocios.detalles",
			component: require("./components/ventas/negocios/detalles/Detalles")
		},
		{
			path: BaseUrl + "/ventas/actividades/:idNotification",
			name: "vt.actividade.notification",
			component: require("./components/ventas/actividades/Actividades")
		},
		{
			path: BaseUrl + "/ventas/actividades",
			name: "vt.actividades",
			component: require("./components/ventas/actividades/Actividades")
		},
		{
			path: BaseUrl + "/ventas/clientes",
			name: "vt.clientes",
			component: require("./components/ventas/clientes/Clientes")
		},
		{
			path: BaseUrl + "/ventas/contactos",
			name: "vt.contactos",
			component: require("./components/ventas/contactos/Contactos")
		},
		{
			path: BaseUrl + "/ventas/presupuesto",
			name: "vt.presupuesto",
			component: require("./components/ventas/presupuesto/Presupuesto")
		},
		{
			path: BaseUrl + "/ventas/indicadores/clientes-facturados",
			name: "vt.indicadores.cf",
			component: require("./components/ventas/indicadores/clientes_facturados/ClientesFacturados")
		},
		{
			path: BaseUrl + "/ventas/indicadores/cartera-vencida",
			name: "vt.indicadores.cv",
			component: require("./components/ventas/indicadores/cartera_vencida/CarteraVencida")
		},
		{
			path: BaseUrl + "/ventas/indicadores/porcentaje-crecimiento",
			name: "vt.indicadores.crecimiento",
			component: require("./components/ventas/indicadores/crecimiento/Crecimiento")
		},
		{
			path: BaseUrl + "/ventas/indicadores/porcentaje-ventas",
			name: "vt.indicadores.pv",
			component: require("./components/ventas/indicadores/porcentaje_ventas/PorcentajeVentas")
		},
		{
			path: BaseUrl + "/ventas/indicadores/evaluacion-servicio",
			name: "vt.indicadores.es",
			component: require("./components/ventas/indicadores/evaluacion_servicio/EvaluacionServicio")
		},
		{
			path: BaseUrl + "/ventas/indicadores/oportunidades",
			name: "vt.indicadores.oportunidades",
			component: require("./components/ventas/indicadores/oportunidades/Oportunidades")
		},
		{
			path: BaseUrl + "/ventas/indicadores/unidades-negocio",
			name: "vt.indicadores.un",
			component: require("./components/ventas/indicadores/unidades_negocio/UnidadesNegocio")
		},
		{
			path: BaseUrl + "/ventas/indicadores/presupuesto",
			name: "vt.indicadores.presupuesto",
			component: require("./components/ventas/indicadores/presupuesto/Presupuesto.vue")
		},
		{
			path: BaseUrl + "/ventas/configuracion/embudos",
			name: "vt.configuracion.embudos",
			component: require("./components/ventas/configuracion/embudos/Embudos")
		},
		{
			path: BaseUrl + "/ventas/configuracion/unidades_negocio",
			name: "vt.configuracion.unidades_negocio",
			component: require("./components/ventas/configuracion/unidades_negocio/UnidadesNegocio.vue")
		},
		{
			path: BaseUrl + "/ventas/configuracion/proyecciones",
			name: "vt.configuracion.proyecciones",
			component: require("./components/ventas/configuracion/proyecciones/Proyecciones.vue")
		},
		{
			//quitar
			path: BaseUrl + "/ventas/servicios",
			name: "vt.servicios",
			component: require("./components/ventas/negocios/servicios/list")
		},
		{
			path: BaseUrl + "/calidad",
			name: "calidad",
			component: require("./components/calidad/DashboardComponent.vue")
		},
		{
			path: BaseUrl + "/calidad/organizacion",
			name: "nr.organizacion",
			component: require("./components/calidad/organizacion/FilosofiaEmpresarial.vue")
		},
		{
			path: BaseUrl + "/calidad/procesos",
			name: "nr.mapeo_organizacional",
			component: require("./components/calidad/areas/Areas.vue")
		},
		{
			path: BaseUrl + "/calidad/procesos/:departamento",
			name: "nr.mapeo_departamental",
			component: require("./components/calidad/areas/Areas.vue")
		},
		{
			path: BaseUrl + "/calidad/organigrama",
			name: "nr.organigrama",
			component: require("./components/calidad/organigrama/Organigrama.vue")
		},


		//SERVICIOS
		//SERVICIOS - RA
		{
			path: BaseUrl + "/servicios",
			name: "servicios",
			component: require("./components/servicios/DashboardComponent.vue")
		},
		{
			path: BaseUrl + "/servicios/ra/actividades",
			name: "op.ra.actividades",
			component: require("./components/servicios/ra/actividades/Actividades.vue")
		},
		{
			path: BaseUrl + "/servicios/ra/indicadores/actividades",
			name: "op.ra.indicadores.actividades",
			component: require("./components/servicios/ra/indicadores/actividades/Actividades.vue")
		},
		{
			path: BaseUrl + "/servicios/ra/configuracion/semanas",
			name: "op.ra.configuracion.semanas",
			component: require("./components/servicios/ra/configuracion/semanas/Semanas.vue")
		},
		//SERVICIOS - CO
		{
			path: BaseUrl + "/servicios/costo",
			name: "op.co.costos",
			component: require("./components/servicios/co/Costos.vue")
		},
		{
			path: BaseUrl + "/sistemas",
			name: "sistemas",
			component: require("./components/sistemas/DashboardComponent.vue")
		},
		{
			path: BaseUrl + "/sistemas/admin",
			name: "si.admin",
			component: require("./components/sistemas/admin/DashboardComponent.vue")
		},
		{
			path: BaseUrl + "/sistemas/admin/cuentas",
			name: "si.admin.cuentas",
			component: require("./components/sistemas/admin/cuentas/Cuentas.vue")
		},
		{
			path: BaseUrl + "/sistemas/admin/licencias",
			name: "si.admin.licencias",
			component: require("./components/sistemas/admin/licencias/Licencias.vue")
		},
		{
			path: BaseUrl + "/sistemas/admin/equipos",
			name: "si.admin.equipos",
			component: require("./components/sistemas/admin/equipos/Equipos.vue")
		},


		//FINANZAS
		{
			path: BaseUrl + "/finanzas",
			name: "fn.cg",
			component: require("./components/finanzas/DashboardComponent.vue")
		},
		{
			path: BaseUrl + "/finanzas/cuentas-contables",
			name: "fn.cuentas_contables",
			component: require("./components/finanzas/cuentas_contables/Cuentas.vue")
		},
		{
			path: BaseUrl + "/finanzas/cuentas-internas",
			name: "fn.cuentas_internas",
			component: require("./components/finanzas/cuentas_internas/CuentasInternas.vue")
		},
		{
			path: BaseUrl + "/finanzas/cuentas-externas",
			name: "fn.cg.cuentas_externas",
			component: require("./components/finanzas/cg/cuentas_externas/CuentasExternas.vue")
		},
		{
			path: BaseUrl + "/finanzas/cg/solicitudes",
			name: "fn.cg.solicitudes",
			component: require("./components/finanzas/cg/solicitudes/Solicitudes.vue")
		},
		{
			path: BaseUrl + "/finanzas/cg/admin-solicitudes",
			name: "fn.cg.admin_solicitudes",
			component: require("./components/finanzas/cg/solicitudes/admin/SolicitudesAdmin.vue")
		},
		{
			path: BaseUrl + "/finanzas/cg/admin/solicitudes/",
			name: "fn.cg.admin_solicitudes2",
			component: require("./components/finanzas/cg/solicitudes/admin/SolicitudesAdmin.vue")
		},
		{
			path: BaseUrl + "/finanzas/cg/auxiliar-movimientos",
			name: "fn.cg.auxiliar_movimientos",
			component: require("./components/finanzas/cg/auxiliar_movimientos/AuxiliarMovimientos.vue")
		},
		{
			path:
				BaseUrl +
				"/finanzas/solicitud-gastos/autorizacion/:idSolicitud/:status",
			name: "fn.cg.autorizacion_solicitud",
			component: require("./components/finanzas/cg/solicitudes/FormAutorizacionSolicitudComponent.vue")
		},
		{
			path: BaseUrl + "/finanzas/solicitud-gastos/dispersion/:idSolicitud",
			name: "fn.cg.dispersion_solicitud",
			component: require("./components/finanzas/cg/solicitudes/FormDispersionSolicitudComponent.vue")
		},
		{
			path: BaseUrl + "/finanzas/cg/configuracion/autorizaciones",
			name: "fn.cg.configuracion_autorizaciones",
			component: require("./components/finanzas/cg/configuracion/autorizaciones/Autorizaciones.vue")
		},
		{
			path: BaseUrl + "/finanzas/configuracion/cuentas-contables",
			name: "fn.cg.cnf.cuentas_contables",
			component: require("./components/finanzas/configuracion/cuentas_contables/AsignacionCuentas.vue")
		},
		{
			path: BaseUrl + "/finanzas/configuracion/cuentas-internas",
			name: "fn.cg.cnf.cuentas_internas",
			component: require("./components/finanzas/configuracion/cuentas_internas/AsignacionCuentas.vue")
		},
		{
			path: BaseUrl + "/finanzas/configuracion/mano-obra",
			name: "fn.cg.cnf.mano_obra",
			component: require("./components/finanzas/configuracion/mano_obra/ManoObra.vue")
		},
		{
			path: BaseUrl + "/finanzas/configuracion/solicitudes/exportar",
			name: "fn.cg.cnf.solicitudes.exportar",
			component: require("./components/finanzas/configuracion/exportar_solicitudes/Exportar.vue")
		},

		// POR MIENTRAS
		{
			path: BaseUrl + "/finanzas/configuracion/xml",
			name: "fn.cg.cnf.solicitudes.xml",
			component: require("./components/finanzas/configuracion/xml/XML.vue")
		},
		
		{
			path: BaseUrl + "/finanzas/cg/indicadores/no-deducible",
			name: "fn.cg.indicadores.no_deducible",
			component: require("./components/finanzas/cg/indicadores/no_deducible/NoDeducible.vue")
		},
		{
			path: BaseUrl + "/finanzas/cg/indicadores/dias-transcurridos",
			name: "fn.cg.indicadores.dias_transcurridos",
			component: require("./components/finanzas/cg/indicadores/dias_transcurridos/DiasTranscurridos.vue")
		},
		{
			path: BaseUrl + "/finanzas/cg/indicadores/total-cuenta",
			name: "fn.cg.indicadores.total_cuenta",
			component: require("./components/finanzas/cg/indicadores/total_cuenta/TotalCuenta.vue")
		},


		//SEGURIDAD
		{
			path: BaseUrl + "/seguridad",
			name: "seguiridad",
			component: require("./components/seguridad/DashboardComponent.vue")
		},
		{
			path: BaseUrl + "/seguridad/salud/historial-clinico",
			name: "se.historial_clinico",
			component: require("./components/seguridad/salud/hc/HistorialClinico.vue")
		},
		{
			path: BaseUrl + "/seguridad/accidentes",
			name: "se.accidentes",
			component: require("./components/seguridad/accidentes/Accidentes.vue")
		},


		//TICKETS
		{
			path: BaseUrl + "/tickets",
			name: "tk.tickets",
			component: require("./components/tickets/tickets/ShowTicket.vue")
		},
		{
			path: BaseUrl + "/tickets/admin",
			name: "tk.tickets.admin",
			component: require("./components/tickets/tickets/ShowTicket.vue")
		},
		{
			path: BaseUrl + "/tickets/estadisticas",
			name: "tk.estadisticas",
			component: require("./components/tickets/indicators/Indicadores.vue")
		},
		{
			path: BaseUrl + "/tickets/servicios",
			name: "tk.configuracion.services",
			component: require("./components/tickets/configuracion/services/Services.vue")
		},

		{
			path: BaseUrl + "/tickets/:idTicket/admin",
			name: "tk.tickets.idTicket.admin",
			component: require("./components/tickets/tickets/ShowTicket.vue")
		},
		
		{
			path: BaseUrl + "/tickets/:idTicket",
			name: "tk.tickets.idTicket",
			component: require("./components/tickets/tickets/ShowTicket.vue")
		},
		// {
		// 	path: BaseUrl + "/tickets/configuracion/servicios",
		// 	name: "tk.configuracion.servicios",
		// 	component: require("./components/tickets/configuracion/servicios/Servicios.vue")
		// },
		// {
		// 	path: BaseUrl + "/tickets/configuracion/usuarios",
		// 	name: "tk.configuracion.usuarios",
		// 	component: require("./components/tickets/configuracion/usuarios/Usuarios.vue")
		// },

	],
	mode: "history"
});