export default{
	namespaced: true,
	state:{
		notifications: [],
		unreadNotifications: [],
		notCount: 0,
		isBusyNot: false,
		isNotificated: false,
	},
	mutations:{
		setNotifications(state, notifications)
        {
            state.notifications = notifications;
        },
		setIdNotifications(state, isNotificated)
        {
			state.isNotificated = isNotificated;
        },
        setNotCount(state, count)
        {
			state.notCount = count;
        },
        changeIsBusyNot(state)
        {
        	state.isBusyNot = !state.isBusyNot
        },
        addNotifications(state, notifications)
        {
            state.notifications.push(...notifications)
        },
        addUnreadNotifications(state, notifications)
        {
            state.unreadNotifications.push(...notifications)
        },
   		addNotification(state, notification)
   		{
   			state.notifications.unshift(notification);
   		},
   		incrementCount(state){
   			state.notCount++;
   		},
   		decrementCount(state){
   			state.notCount--;
   		},
   		makeReadNotification(state, notification)
   		{
   			const notRead = state.notifications.find(function(not){
      			return not.created_at == notification.created_at;
      		});

      		if(notRead){
				notRead.read_at = moment(new Date()).format("YYYY-MM-DD HH:mm:ss");
      		}
   		}
	},
	actions:{
		readNotifications(context, data)
		{
			axios.put(BaseUrl + '/api/notification/', data)
    		.then((response) => {

    			if(data.from != 'ticketActive')
                	context.commit('setNotCount', context.state.notCount - response.data.notifications_read.length);
                response.data.notifications_read.forEach(
                	(notification) => context.commit('makeReadNotification', notification)
                		);
    		});
		}
	},
	getters:{

	}
}