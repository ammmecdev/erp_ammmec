export default{
	namespaced: true,

	state:{
        selectUsers: null,
		services: [],
        subjects: [],
        users: [],
	},
	mutations:{
		setServices(state, services) {
            state.services = services;
        },
        addService(state, service) {
            state.services.push(service);
        },
        updateService(state,  request) {
            const service = state.services.find(
                (service) => 
                service.id == request.id
            );
            service.name = request.name;
            service.description = request.description;
            service.status = request.status;
        },
        setSubjects(state, subjects) {
            state.subjects = subjects;
        },
        addSubject(state, subject) {
            state.subjects.push(subject);
        },
        setUsersByService(state, users) {
            state.users = users;
        },
        setUsersByDepartament(state, users) {
            state.selectUsers = users.map((user) => {
                return {
                    text: user.nombreUsuario,
                    value: user.idUsuario
                }
            });
        },
	},
	actions:{
		getServicesByDepartament(context, departament_id){
            axios.get(BaseUrl + `/api/services/departaments/${departament_id}`)
            .then(response => {
                context.commit('setServices', response.data)
            });
        },
        getSubjectsByService(context, service_id){
            axios.get(BaseUrl + `/api/subjects/service/${service_id}`)
            .then(response => {
                context.commit('setSubjects', response.data)
            });
        },
        getUsersByService(context, service_id) {
            axios.get(BaseUrl + `/api/tk/service/users/${service_id}`)
            .then(response => {
                context.commit('setUsersByService', response.data)
            });
        },
        getUsersByDepartament(context, departament_id) {
            axios.get(BaseUrl + `/api/tk/services/users/departament/${departament_id}`)
            .then(response => {
                context.commit('setUsersByDepartament', response.data)
            });
        },
	},
	getters:{
		
	}
}