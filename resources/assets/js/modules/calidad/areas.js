export default {
    namespaced: true,
    state: {
        areaActiva: null,
        procesos: [],
        listArchivos: [],
        procesoActivo: null,
        procedimientoActivo: null,
        archivoActivo: null,
        searchGeneral: "",
    },
    mutations: {
        setArea(state, area)
        {
            state.areaActiva = area
        },
        setProcesos(state, procesos)
        {
            state.procesos = procesos
        },
        setProceso(state, proceso)
        {
            state.procesoActivo = proceso
        },
        setProcedimiento(state, procedimiento)
        {
            state.procedimientoActivo = procedimiento
        },
        setArchivo(state, archivo)
        {
            state.archivoActivo = archivo 
        },
        setArchivos(state, archivos)
        {
            state.listArchivos = archivos
        },
        addProceso(state, proceso)
        {
            state.procesos.push(proceso)
        },
        updateProceso(state, proceso)
        {
            const proceso_state = state.procesos.find(function(proceso_state){
                    return proceso_state.idProceso == proceso.idProceso
            })
            if(proceso_state){
                proceso_state.nombre = proceso.nombre
                proceso_state.codigo = proceso.codigo
            }
        },
        delElementProceso(state, idProceso)
        {
            const index = state.procesos.findIndex((proceso_state) => {
                return proceso_state.idProceso == idProceso;
            });
            state.procesos.splice(index, 1);
        },
        addProcedimiento(state, proceso)
        {
            const proceso_state = state.procesos.find(function(proceso_state){
                    return proceso_state.idProceso == proceso.idProceso
            })
            if(proceso_state)
                proceso_state.procedimientos.push(proceso.procedimiento)
            
        },
        updateProcedimiento(state, proceso)
        {
            const proceso_state = state.procesos.find(function(proceso_state){
                    return proceso_state.idProceso == proceso.idProceso
            })
            if(proceso_state){
                const procedimiento_state = proceso_state.procedimientos.find(function(procedimiento_state){
                    return procedimiento_state.idProcedimiento == proceso.procedimiento.idProcedimiento
                })
                if(procedimiento_state){
                    procedimiento_state.nombre = proceso.procedimiento.nombre
                    procedimiento_state.procedimiento = proceso.procedimiento.procedimiento
                }
            }
        },
        addArchivo(state, archivo)
        {
            state.listArchivos.push(archivo)
        },
        updateArchivo(state, archivo)
        {
            const archivo_state = state.listArchivos.find(function(archivo_state){
                return archivo_state.idArchivo == archivo.idArchivo
            })
            if(archivo_state){
                archivo_state.nombre = archivo.nombre
                archivo_state.tipo = archivo.tipo
                archivo_state.formato = archivo.formato
                archivo_state.size = archivo.size
                archivo_state.codigo = archivo.codigo
                archivo_state.version = archivo.version
                archivo_state.clave = archivo.clave
                archivo_state.ultima_modificacion = archivo.ultima_modificacion
            }
        },
        delElementProcedimiento(state, idProcedimiento)
        {
            const procedimiento_state = state.procesos.find(function(proceso_state){
                const procedimiento_state = state.proceso_state.procedimientos.findIndex((procedimiento_state) => {
                    return procedimiento_state.idProcedimiento == idProcedimiento
                })
                
                if(procedimiento_state)
                    return procedimiento_state
            })
            if(procedimiento_state)
                proceso_state.predimientos.splice(index, 1);
            
        },
        setSearchGeneral(state, search)
        {
            state.searchGeneral = search
        },
        setManual(state, manual)
        {
            state.areaActiva.manual = manual
        }
    },
    actions: {
        getArea(context, codigo)
        {
            axios.get(BaseUrl + `/api/nr/area/${codigo}`)
                .then(
                (response) => {
                    context.commit('setArea', response.data)
                    context.dispatch('getProcesos', response.data)
                    context.dispatch('getArchivosByArea', response.data)
                }
            );
        },
        getProcesos(context){
            axios.get(BaseUrl + `/api/nr/area/${context.state.areaActiva.idArea}/procesos`,)
                .then(
                (response) => {
                    context.commit('setProcesos', response.data)
                }
            );
        },
        getArchivosByArea(context){
            axios.get(BaseUrl + `/api/nr/areas/${context.state.areaActiva.idArea}/archivos`)
                .then(
                (response) => context.commit('setArchivos', response.data)
            );
        },
        getArchivosByProcedimiento(context, idProcedimiento){
            axios.get(BaseUrl + `/api/nr/procedimientos/${idProcedimiento}/archivos`)
                .then(
                (response) => context.commit('setArchivos', response.data)
            );
        }
    },
    getters: {

    }
}