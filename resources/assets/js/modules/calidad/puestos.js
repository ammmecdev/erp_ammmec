export default {
    namespaced: true,
    state: {
        puestoActivo: null,
        editData: false
    },
    mutations: {
        setPuesto(state, puesto)
        {
            state.puestoActivo = puesto
        },
        changeEditData(state, status)
        {
            state.editData = status
        },
        setPerfil(state, perfil)
        {
            state.puestoActivo.perfil = perfil
        }
    },
    actions: {
        getPuesto(context, idPuesto)
        {
            axios.get(BaseUrl + `/api/nr/puestos/${idPuesto}/perfil`)
                .then(
                (response) => {
                    context.commit('setPuesto', response.data)
                }
            );
        },
    },
    getters: {

    }
}