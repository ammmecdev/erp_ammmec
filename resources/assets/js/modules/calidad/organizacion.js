export default {
    namespaced: true,
    state: {
        tipoActivo: null,
        listArchivos: [],
        archivoActivo: null,
    },
    mutations: {
        setTipo(state, area)
        {
            state.tipoActivo = area
        },
        setArchivo(state, archivo)
        {
            state.archivoActivo = archivo 
        },
        setArchivos(state, archivos)
        {
            state.listArchivos = archivos
            console.log(state.listArchivos)
        },
        addArchivo(state, archivo)
        {
            state.listArchivos.push(archivo)
        },
        updateArchivo(state, archivo)
        {
            const archivo_state = state.listArchivos.find(function(archivo_state){
                return archivo_state.idArchivo == archivo.idArchivo
            })
            if(archivo_state){
                archivo_state.nombre = archivo.nombre
                archivo_state.tipo = archivo.tipo
                archivo_state.formato = archivo.formato
                archivo_state.size = archivo.size
                archivo_state.codigo = archivo.codigo
                archivo_state.version = archivo.version
                archivo_state.clave = archivo.clave
                archivo_state.ultima_modificacion = archivo.ultima_modificacion
            }
        },
        delElementProcedimiento(state, idProcedimiento)
        {
            const procedimiento_state = state.procesos.find(function(proceso_state){
                const procedimiento_state = state.proceso_state.procedimientos.findIndex((procedimiento_state) => {
                    return procedimiento_state.idProcedimiento == idProcedimiento
                })
                
                if(procedimiento_state)
                    return procedimiento_state
            })
            if(procedimiento_state)
                proceso_state.predimientos.splice(index, 1);
            
        },
    },
    actions: {
        getArchivosByTipo(context){
            axios.get(BaseUrl + `/api/nr/organizacion/archivos/${context.state.tipoActivo}`)
                .then(
                (response) => context.commit('setArchivos', response.data)
            );
        },
    },
    getters: {

    }
}