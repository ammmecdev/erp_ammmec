export default {
    namespaced: true,
    state: {
        cuentas: [],
        isBusy: true,
        dominios: [],
        dominioSelected: 'ammmec.com',
        cuentasServicios: [],
        success: false,
    },
    mutations: {
        addCuenta(state, cuenta){
            state.cuentas.push(cuenta)
        },
        setCuentas(state, cuentas) {
            state.cuentas = cuentas
        },
        setIsBusy(state, status){
            state.isBusy = status
        },
        setDominioSelected(state, dominio){
            state.dominioSelected = dominio
        },
        setDominios(state, dominios){
            state.dominios = dominios
        },
        updateCuenta(state, cuenta) {
            const cuenta_state = state.cuentas.find(function(cuenta_state) {
                return cuenta_state.idCuenta == cuenta.idCuenta
            })
            cuenta_state.responsable = cuenta.responsable
            cuenta_state.cuenta = cuenta.cuenta
            cuenta_state.dominio = cuenta.dominio
            cuenta_state.password_cuenta = cuenta.password_cuenta
        },
        delElementCuenta(state, id) {
            const index = state.cuentas.findIndex((cuenta) => {
                  return cuenta.idcuenta == id;
            });
            state.cuenta.splice(index, 1);
        },
        setCuentasServicios(state, data) {
            state.cuentasServicios = data
        },
        setSuccess(state, value) {
            state.success = value
        },
        addCuentaServicio(state, data) {
            state.cuentasServicios.push(data)
        }
    },
    actions: {
        getCuentas(context){
            context.commit('setIsBusy', true)
            axios.get(BaseUrl + `/api/si/admin/cuentas`).then(
                (response) => {
                    context.commit('setCuentas', response.data)
                    context.commit('setIsBusy', false)
                })
        },
        getDominios(context){
            axios.get(BaseUrl + `/api/si/admin/cuentas/dominios`).then(
                (response) => {
                    context.commit('setDominios', response.data)
                })
        },
        getCuentasServicios(context, idCuenta) {
            axios.get(BaseUrl + `/api/si/cuentas-servicios/${idCuenta}`).then(
                (response) => {
                    context.commit('setCuentasServicios', response.data)
                })
        }
    },
    getters: {
    
    }
}