export default {
    namespaced: true,
    state: {
        equipos: [],
        isBusy: true,
    },
    mutations: {
        addEquipo(state, equipo){
            state.equipos.push(equipo)
        },
        setEquipos(state, equipos) {
            state.equipos = equipos
        },
        setIsBusy(state, status){
            state.isBusy = status
        },
        updateEquipo(state, equipo) {
            const equipo_state = state.equipos.find(function(equipo_state) {
                console.log(equipo_state)
                return equipo_state.idEquipo == equipo.idEquipo
            })
            console.log(equipo_state);
           equipo_state.responsable = equipo.responsable
           equipo_state.tipo = equipo.tipo
           equipo_state.clave = equipo.clave
           equipo_state.marca = equipo.marca
           equipo_state.noSerie = equipo.noSerie
           equipo_state.modelo = equipo.modelo
           equipo_state.area = equipo.area
           equipo_state.status  = equipo.status
        },
        delElementEquipo(state, id) {
            const index = state.equipos.findIndex((equipo) => {
                  return equipo.idEquipo == id;
            });
            state.equipo.splice(index, 1);
        }
    },
    actions: {
        getEquipos(context){
            context.commit('setIsBusy', true)
            axios.get(BaseUrl + `/api/si/admin/equipos`).then(
                (response) => {
                    context.commit('setEquipos', response.data)
                    context.commit('setIsBusy', false)
                })
        },
    },
    getters: {
    
    }
}