export default {
    namespaced: true,
    state: {
        licencias: [],
        isBusy: true,
        categorias: [],
        categoriasSelect: [],
    },
    mutations: {
        addLicencia(state, licencia){
            state.licencias.push(licencia)
        },
        setLicencias(state, licencias) {
            state.licencias = licencias
        },
        setIsBusy(state, status){
            state.isBusy = status
        },
        updateLicencia(state, licencia) {
            const licencia_state = state.licencias.find(function(licencia_state) {
                return licencia_state.idLicencia == licencia.idLicencia
            })
            console.log(licencia_state);
           licencia_state.responsable = licencia.responsable
           licencia_state.tipo = licencia.tipo
           licencia_state.clave = licencia.clave
           licencia_state.marca = licencia.marca
           licencia_state.noSerie = licencia.noSerie
           licencia_state.modelo = licencia.modelo
           licencia_state.area = licencia.area
           licencia_state.status  = licencia.status
        },
        delElementLicencia(state, id) {
            const index = state.licencias.findIndex((licencia) => {
                  return licencia.idLicencia == id;
            });
            state.licencia.splice(index, 1);
        },
        setCategorias(state, categorias)
        {
            state.categorias = categorias
        },
        setCategoriasSelect(state, categorias)
        {
            state.categoriasSelect = categorias.map(function(categoria) {
                                return {text:categoria.nombre, value:categoria.idCategoria};
                            })
        }
    },
    actions: { 
        getCategoriasSelect(context){
            context.commit('setIsBusy', true)
            axios.get(BaseUrl + `/api/si/admin/licencias/categorias`).then(
                (response) => {
                    context.commit('setCategoriasSelect', response.data)
                    context.commit('setIsBusy', false)
                })
        },
        getCategoriasWhitLicencias(context){
            context.commit('setIsBusy', true)
            axios.get(BaseUrl + `/api/si/admin/categorias/licencias`).then(
                (response) => {
                    context.commit('setCategorias', response.data)
                    context.commit('setIsBusy', false)
                })
        },
    },
    getters: {
    
    }
}