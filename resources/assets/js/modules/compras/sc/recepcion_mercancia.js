export default {
    namespaced: true,
    state: {
        listRecepcionMercancia: [],
        listRecepcionArticulos: [],
        busyRecepciones: true,
    },
    mutations: {
        setListRecepciones(state, items) {
            state.listRecepcionMercancia = items;
        },
        changeBusyRecepcion(state, value) {
            state.busyRecepciones = value;
        },
        addRecepcionMercancia(state, data) {
            state.listRecepcionMercancia.push(data);
        },
        setListRecepcionArticulos(state, items) {
            state.listRecepcionArticulos = items;
        },
    },
    actions: {
        getRecepcionMercancias(context, idOrdenCompra) {
            context.commit("changeBusyRecepcion", true);
            axios.get(BaseUrl + `/api/co/sc/ordencompra/${idOrdenCompra}/recepciones`).then(response => {
                context.commit("setListRecepciones", response.data);
                context.commit("changeBusyRecepcion", false);
            });
        },
        getRecepcionArticulos(context, idOrdenCompra) {
            axios.get(BaseUrl + `/api/co/sc/ordencompra/${idOrdenCompra}/recepcion/articulos`).then(response => {
                context.commit("setListRecepcionArticulos", response.data);
            });
        },
    },
    getters: {}
};