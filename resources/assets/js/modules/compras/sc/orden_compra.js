export default {
    namespaced: true,
    state: {
        listOrdenesCompra: [],
        busyOrdenesCompra: true,
        ordenCompraActiva: null,
    },
    mutations: {
        setListOrdenesCompra(state, ordenesCompra) {
            state.listOrdenesCompra = ordenesCompra;
        },
        changeBusyOC(state, value) {
            state.busyOrdenesCompra = value;
        },
        updateOrdenCompra(state, orden) {
            const orden_state = state.listOrdenesCompra.find(function (
                orden_state
            ) {
                return orden_state.idOrdenCompra == orden.idOrdenCompra;
            });
            if (orden_state) {
                orden_state.idProveedor = orden.idProveedor;
                orden_state.fechaEntrega = orden.fechaEntrega;
                
                orden_state.iva = orden.iva;                
                orden_state.retencionIVA = orden.retencionIVA;
                orden_state.retencionISR = orden.retencionISR;
                orden_state.otrosImpuestos = orden.otrosImpuestos;

                orden_state.formaPago = orden.formaPago;
                orden_state.metodoPago = orden.metodoPago;
                orden_state.tipoMoneda = orden.tipoMoneda;
                orden_state.observaciones = orden.observaciones;
                orden_state.idAutorizador = orden.idAutorizador;
                orden_state.autorizacion = orden.autorizacion;
                orden_state.fechaAutorizacion = orden.fechaAutorizacion;
                orden_state.nombre_proveedor = orden.nombre_proveedor;
                orden_state.dias_credito = orden.dias_credito;
                orden_state.razon_social_proveedor = orden.razon_social_proveedor;
                orden_state.rfc_proveedor = orden.rfc_proveedor;
                orden_state.descripcion = orden.descripcion;

                orden_state.tipoPedido = orden.tipoPedido;
                orden_state.centroCosto = orden.centroCosto;
                orden_state.referenciaCotizacion = orden.referenciaCotizacion;
                orden_state.condicionesRM = orden.condicionesRM;
                orden_state.cfdi = orden.cfdi;

                orden_state.notificacion = orden.notificacion;
            }
        },
        addOrdenCompra(state, orden) {
            state.listOrdenesCompra.push(orden);   
        },
        setOrdenCompra(state, ordenCompra) {
            state.ordenCompraActiva = ordenCompra
        },
    },
    actions: {
        getOrdenesCompra(context, idSolicitud) {
            context.commit("changeBusyOC", true);
            axios.get(BaseUrl + `/api/co/sc/solicitud/${idSolicitud}/ordenescompra`).then(response => {
                context.commit("setListOrdenesCompra", response.data);
                context.commit("changeBusyOC", false);
            });
        },
        getOrdenCompra(context, idOrdenCompra) {
            axios.get(BaseUrl + `/api/co/sc/ordencompra/${idOrdenCompra}`).then(response => {
                context.commit("setOrdenCompra", response.data);
            });
        },
    },
    getters: {}
};