export default {
    namespaced: true,
    state: {
        listArticulosByOC: [],
        solicitudActiva: null,
        listSolicitudes: [],
        // listArticulos: [],
        listEtapas: [],
        busySolicitudes: false,
        // busyArticulos: false,
        busyArticulosByOC: false,

        tipoSolicitudes: "",
        solicitudesConsultadas: "", //pendiente
        
        filterEtapa: 0,
        filterInicio: null,
        filterFin: null,
        filterSearch: null,
    },
    mutations: {
        setFilterSearch(state, search) {
            state.filterSearch = search;
        },
        setFilterInicio(state, inicio) {
            state.filterInicio = inicio;
        },
        setFilterFin(state, fin) {
            state.filterFin = fin;
        },
        setFilterEtapa(state, etapa) {
            state.filterEtapa = etapa;
        },
        setTipoSolicitudes(state, tipo) {
            state.solicitudActiva = null;
            state.tipoSolicitudes = tipo;
        },
        setSolicitudActiva(state, solicitud) {
            state.solicitudActiva = solicitud;
        },
        setListSolicitudes(state, solicitudes) {
            state.listSolicitudes = solicitudes;
        },
        // setArticulos(state, articulos) {
        //     state.listArticulos = articulos;
        // },
        setArticulosByOC(state, articulos) {
            state.listArticulosByOC = articulos;
        },
        updateArticuloByOC(state, articulo){
            const articulo_state = state.listArticulosByOC.find(function (
                articulo_state
            ) {
                return articulo_state.idArticulo == articulo.idArticulo;
            });
            if (articulo_state) {
                articulo_state.descripcion = articulo.descripcion;
            }
        },
        // addSolicitud(state, solicitud) {
        //     const solicitud_state = state.listSolicitudes.find(
        //         (solicitud_state) => solicitud_state.idSolicitud == solicitud.idSolicitud
        //     )
        //     if (!solicitud_state)
        //         state.listSolicitudes.push(solicitud);
        // },
        setEtapas(state, etapas) {
            state.listEtapas = etapas;
        },
        changeBusySolicitudes(state, value) {
            state.busySolicitudes = value;
        },
        // changeBusyArticulos(state, value) {
        //     state.busyArticulos = value;
        // },
        changeBusyArticulosByOC(state, value) {
            state.busyArticulosByOC = value;
        },
        // updateArticulo(state, articulo) {
        //     const articulo_state = state.listArticulos.find(function (
        //         articulo_state
        //     ) {
        //         return articulo_state.idArticulo == articulo.idArticulo;
        //     });

        //     if (articulo_state) {
        //         articulo_state.idActivo = articulo.idActivo;
        //         articulo_state.tipo = articulo.tipo;
        //         articulo_state.cantidadOut = articulo.cantidadOut;
        //         articulo_state.salida = articulo.salida;
        //         articulo_state.estado = articulo.estado;
        //     }
        // },
        setSolicitudesConsultadas(state, value) {
            state.solicitudesConsultadas = value
        },
        // updateSolicitud(state, solicitud) {
        //     const solicitud_state = state.listSolicitudes.find(function (
        //         solicitud_state
        //     ) {
        //         return solicitud_state.idSolicitud == solicitud.idSolicitud;
        //     });
        //     if (solicitud_state) {
        //         solicitud_state.idEtapa = solicitud.idEtapa;
        //         solicitud_state.idEtapaOld = solicitud.idEtapaOld;
        //         solicitud_state.nombre_etapa = solicitud.nombre_etapa;
        //         solicitud_state.fechaOrdenCompra = solicitud.fechaOrdenCompra;
        //     }
        // },
        updateSolicitudActiva(state, solicitud) {
            state.solicitudActiva.idEtapa = solicitud.idEtapa;
            state.solicitudActiva.nombre_etapa = solicitud.nombre_etapa;
            state.solicitudActiva.fechaAutorizacion = solicitud.fechaAutorizacion;
            state.solicitudActiva.fechaTermino = solicitud.fechaTermino;
        },
    },
    actions: {
        // TODAS
        getSolicitudes(context) {
            context.commit("changeBusySolicitudes", true);
            axios.get(BaseUrl + `/api/co/sc/solicitudes`).then(response => {
                context.commit("setListSolicitudes", response.data);
                context.commit("changeBusySolicitudes", false);
                context.commit("setSolicitudesConsultadas", "todas");

            });
        },
        // POR AUTORIZAR
        getSolicitudesByAutorizador(context, idUsuario) {
            context.commit("changeBusySolicitudes", true);
            axios
                .get(BaseUrl + `/api/co/sc/solicitudes/autorizador/${idUsuario}`)
                .then(response => {
                    context.commit("setListSolicitudes", response.data);
                    context.commit("changeBusySolicitudes", false);
                    context.commit("setSolicitudesConsultadas", "byAutorizador");
                });
        },
        // getSolicitud(context, idSolicitud) {
        //     axios
        //         .get(BaseUrl + `/api/co/ss/solicitud/${idSolicitud}`)
        //         .then(response => {
        //             context.commit("setSolicitudActiva", response.data);
        //         });
        // },
        // getArticulos(context, idSolicitud) {
        //     context.commit("changeBusyArticulos", true);
        //     axios
        //         .get(BaseUrl + `/api/co/ss/solicitud/${idSolicitud}/articulos`)
        //         .then(response => {
        //             context.commit("setArticulos", response.data);
        //         });
        // },
        getEtapas(context) {
            axios.get(BaseUrl + `/api/co/sc/etapas`).then(response => {
                context.commit("setEtapas", response.data);
            });
        }, 
        getArticulosByOrdenCompra(context, idOrdenCompra) {
            context.commit("changeBusyArticulosByOC", true);
            axios
                .get(BaseUrl + `/api/co/sc/ordencompra/${idOrdenCompra}/articulos`)
                .then(response => {
                    context.commit("setArticulosByOC", response.data);
                     context.commit("changeBusyArticulosByOC", false);
                });
        },   
    },
    getters: {
        solicitudesFiltered(state) {
            var solicitudes = [];
            solicitudes = state.listSolicitudes;

            if (state.filterEtapa) {
                solicitudes = state.listSolicitudes.filter(
                    solicitud => solicitud.idEtapa == state.filterEtapa
                );
            }
            if (state.filterInicio && state.filterFin) {
                solicitudes = solicitudes.filter(
                    solicitud =>
                        solicitud.created_at >= state.filterInicio &&
                        state.filterFin >= solicitud.created_at 
                );
            }
            if (state.filterSearch) {
                solicitudes = solicitudes.filter(
                    solicitud => {
                        if (solicitud.cuenta && solicitud.idUsuario) {
                            if (
                                solicitud.idSolicitudCompra == state.filterSearch ||
                                solicitud.nombreUsuario
                                    .toLowerCase()
                                    .indexOf(state.filterSearch.toLowerCase()) > -1 ||
                                solicitud.cuenta
                                    .toLowerCase()
                                    .indexOf(state.filterSearch.toLowerCase()) > -1
                            ) {
                                return solicitud;
                            }
                        }
                    }
                );
            }
            if (state.filterEtapa == 0) {
                solicitudes = solicitudes.filter(
                    solicitud =>
                        solicitud.idEtapa != 4
                );
            }
            return solicitudes.reverse();
        }
    }
};