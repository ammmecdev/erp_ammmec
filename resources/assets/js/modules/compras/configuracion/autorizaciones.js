export default {
    namespaced: true,
    state: {
        listAutorizaciones: [],
        busyAutorizaciones: true,
    },
    mutations: {
        setListAutorizaciones(state, autorizaciones) {
            state.listAutorizaciones = autorizaciones;
        },
        changeBusyAutorizaciones(state, value) {
            state.busyAutorizaciones = value;
        },
        updateAutorizacion(state, autorizacion) {
            const autorizacion_state = state.listAutorizaciones.find(function (
                autorizacion_state
            ) {
                return autorizacion_state.idAutorizacion == autorizacion.idAutorizacion;
            });
            if (autorizacion_state) {
                autorizacion_state.montoInicial = autorizacion.montoInicial;
                autorizacion_state.montoFin = autorizacion.montoFin;
            }
        },
    },
    actions: {
        getAutorizaciones(context) {
            context.commit("changeBusyAutorizaciones", true);
            axios.get(BaseUrl + `/api/co/sc/solicitudes/autorizaciones`).then(response => {
                context.commit("setListAutorizaciones", response.data);
                context.commit("changeBusyAutorizaciones", false);
            });
        },
    },
    getters: {}
};