export default {
    namespaced: true,
    state: {
        filter: "",
        listProveedores: [],
        selectProveedores: [],
        busyProveedores: false,
    },
    mutations: {
        setListProveedores(state, proveedores) {
            state.listProveedores = proveedores;
            state.selectProveedores = state.listProveedores.map((proveedor) => {
                return { text: proveedor.nombre, value: proveedor.idProveedor }
            });
        },
        addProveedor(state, proveedor) {
            const proveedor_state = state.listProveedores.find(
                (proveedor_state) => proveedor_state.idProveedor == proveedor.idProveedor
            )
            if (!proveedor_state)
                state.listProveedores.push(proveedor);
        },
        changeBusyProveedores(state, value) {
            state.busyProveedores = value;
        },
        updateProveedor(state, proveedor) {
            const proveedor_state = state.listProveedores.find(function (
                proveedor_state
            ) {
                return proveedor_state.idProveedor == proveedor.idProveedor;
            });
            if (proveedor_state) {
                proveedor_state.nombre = proveedor.nombre;
                proveedor_state.razonSocial = proveedor.razonSocial;
                proveedor_state.rfc = proveedor.rfc;
                proveedor_state.email = proveedor.email;
                proveedor_state.pagoCredito = proveedor.pagoCredito;
                proveedor_state.diasCredito = proveedor.diasCredito;
                proveedor_state.anticipo = proveedor.anticipo;
                proveedor_state.contacto = proveedor.contacto;
                proveedor_state.telefono = proveedor.telefono;
                proveedor_state.direccion = proveedor.direccion;
            }
        },
        removeProveedor(state, id) {
            const index = state.listProveedores.findIndex((item) => {
                return item.idProveedor == id;
            });
            state.listProveedores.splice(index, 1);
        },
        setFilterSearch(state, value) {
            state.filter = value;
        },
    },
    actions: {
        getProveedores(context) {
            context.commit("changeBusyProveedores", true);
            axios.get(BaseUrl + `/api/co/proveedores`).then(response => {
                context.commit("setListProveedores", response.data);
                context.commit("changeBusyProveedores", false);
            });
        },
    },
    getters: {
        proveedoresFiltered(state) {
            var proveedores = state.listProveedores;
            if (state.filter) {
                proveedores = proveedores.filter(
                    proveedor =>
                        proveedor.nombre
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1 ||
                        proveedor.rfc
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1 ||
                        proveedor.contacto
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1 ||
                        proveedor.direccion
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1
                );
            }
            return proveedores.reverse();
        }
    }
};