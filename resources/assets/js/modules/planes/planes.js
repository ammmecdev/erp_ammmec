import store from "../../store";

export default {
    namespaced: true,
    state: {
        planes: [],
        planActivo: null,
        usuariosCompartidos: [],
        usuarios: [],
        responsables: [],
        secciones: [],
        actividades: [],
        resumen: {
            diaCalculable: "2021-06-20" 
        },
    },
    mutations: {
        setPlanes(state, data) {
            state.planes = data;
        },
        setPlanActivo(state, data) {
            state.planActivo = data;
        },
        addPlan(state, data) {
            state.planes.push(data);
        },
        updatePlan(state, data) {
            const plan_state = state.planes.find(function(plan_state) {
                return plan_state.idPlan == data.idPlan;
            });
            if (plan_state) {
                plan_state.nombre = data.nombre;
            }
        },
        removePlan(state, idPlan) {
            state.planActivo = null
            const index = state.planes.findIndex(plan_state => {
                return plan_state.idPlan == idPlan;
            });
            state.planes.splice(index, 1);
        },
        setSecciones(state, data) {
            state.secciones = data;
        },
        addSeccion(state, data) {
            state.secciones.push(data);
        },
        removeSeccion(state, idSeccion) {
            const index = state.secciones.findIndex(seccion_state => {
                return seccion_state.idSeccion == idSeccion;
            });
            state.secciones.splice(index, 1);
        },
        setActividades(state, data) {
            state.actividades = data;
        },
        addActividad(state, data) {
            state.actividades.push(data);
        },
        updateActividad(state, data){
            const actividad_state = state.actividades.find(function(actividad_state) {
                return actividad_state.idActividad == data.idActividad;
            });

            if (actividad_state) {
                actividad_state.actividad = data.actividad;
                actividad_state.responsable = data.responsable;
                actividad_state.inicioP = data.inicioP;
                actividad_state.finP = data.finP;
                actividad_state.inicioR = data.inicioR;
                actividad_state.finR = data.finR;
                actividad_state.avance = data.avance;
                actividad_state.observaciones = data.observaciones;
            }
        },
        removeActividad(state, idActividad) {
            const index = state.actividades.findIndex(actividad_state => {
                return actividad_state.idActividad == idActividad;
            });
            state.actividades.splice(index, 1);
        },
        setResponables(state, data) {
            state.responsables = data;
        },
        addResponsable(state, data) {
            state.responsables.push(data);
        },
        setUsuariosCompartidos(state, data) {
            state.usuariosCompartidos = data;
        },
        addUsuarioCompartido(state, data) {
            state.usuariosCompartidos.push(data);
        },
        removeUsuarioCompartido(state, id) {
            const index = state.usuariosCompartidos.findIndex(usuarioCompartido_state => {
                return usuarioCompartido_state.id == id;
            });
            state.usuariosCompartidos.splice(index, 1);
        },
    },
    actions: {
        getPlanes(context, idUsuario) {
            axios
                .get(BaseUrl + `/api/planes/${idUsuario}`)
                .then(response => {
                    context.commit("setPlanes", response.data);
                });
        },
        getSecciones(context) {
            axios
                .get(BaseUrl + `/api/plan/${context.state.planActivo.idPlan}/secciones`)
                .then(response => {
                    context.commit("setSecciones", response.data);
                });
        },
        getActividades(context) {
            axios
                .get(BaseUrl + `/api/plan/${context.state.planActivo.idPlan}/actividades`)
                .then(response => {
                    context.commit("setActividades", response.data);
                });
        },
        getResponsables(context) {
            axios
                .get(BaseUrl + `/api/plan/${context.state.planActivo.idPlan}/responsables`)
                .then(response => {
                    context.commit("setResponables", response.data);
                });
        },  
        getUsuariosCompartidos(context) {
            axios
                .get(BaseUrl + `/api/plan/${context.state.planActivo.idPlan}/usuarios`)
                .then(response => {
                    context.commit("setUsuariosCompartidos", response.data);
                });
        },
    },
};
