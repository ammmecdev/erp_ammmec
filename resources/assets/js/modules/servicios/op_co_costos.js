export default {
    namespaced: true,
    state: {        
        //COSTO OPERATIVO
        loadingRemove: false,
        busyCostos: false,
        filterInicio: null,
        filterFin: null,
        filterSearch: null,
        filterStatus: null,
        costoActivo: null,
        listCostos: [],
        costoFactores: null,
        costoSeguridad: null,
        loadCosto: false,
        confCostoEtapa: null,

        //MANO DE OBRA
        manoObra: null,
        dataManoObra: { 
            items: [], 
            total: 0 
        },

        //HERRAMIENTA Y EQUIPOS
        herramientaEquipo: null,
        herramientasEquipos: { 
            items: [], 
            total: 0 
        },

        //GASTOS ESPECIALES
        gastoEspecial: {},
        gastosEspeciales: { 
            items: [], 
            total: 0 
        },

        //GASTOS DE OPERACIÓN
        gastoOperacion: {},
        gastosOperacion: { 
            items: [], 
            total: 0 
        },

        //OBSERVACIONES
        observaciones: [],

        //INVENTARIO ACTIVOS
        // activos: [],
        // categorias: [],

        //MATERIALES E INSUMOS
        materialInsumo: {},
        materialesInsumos: { 
            items: [], 
            total: 0 
        },

        //TOTALES
        cuentaPlaneada: {
            insumo: 0,
            manoObra: 0,
            herramientaEquipo: 0,
            gastoEspecial: 0,
            gastoOperacion: 0
        },
    },
    mutations: {
        //COSTO OPERATIVO
        setFilterSearch(state, search) {
            state.filterSearch = search;
        },
        setFilterInicio(state, inicio) {
            state.filterInicio = inicio;
        },
        setFilterFin(state, fin) {
            state.filterFin = fin;
        },
        setFilterStatus(state, value) {
            state.filterStatus = value;
        },
        setCostoActivo(state, costo) {
            state.costoActivo = costo;
        },
        setListCostos(state, costos) {
            state.listCostos = costos;
        },
        addCosto(state, costo) {
            const costo_state = state.listCostos.find(
                (costo_state) => costo_state.idCosto == costo.idCosto
            )
            if (!costo_state)
                state.listCostos.push(costo);
        },
        updateCosto(state, costo) {
            const costo_state = state.listCostos.find(function(costo_state) {
                return costo_state.idCosto == costo.idCosto;
            });
            if (costo_state) {
                costo_state.duracion = costo.duracion;
                costo_state.jornadaLaboral = costo.jornadaLaboral;
                costo_state.jornadaInterna = costo.jornadaInterna;
                costo_state.jornadaDominical = costo.jornadaDominical;
            }
        },
        setCostoFactores(state, data) {
            state.costoFactores = data;
        },
        setCostoSeguridad(state, data) {
            state.costoSeguridad = data;
        },
        updateCostoSeguridad(state, data) {
            state.costoSeguridad.contacto = data.contacto;
            state.costoSeguridad.incidente = data.incidente;
            state.costoSeguridad.accidente = data.accidente;
        },
        setLoadCosto(state, value) {
            state.loadCosto = value
        },
        setLoadingRemove(state, value) {
            state.loadingRemove = value;
        },
        removeCosto(state, idCosto) {
            const index = state.listCostos.findIndex(costo_state => {
                return costo_state.idCosto == idCosto;
            });
            state.listCostos.splice(index, 1);
        },

        //MANO DE OBRA
        setManoObra(state, data) {
            state.manoObra = data;
        },
        updateManoObra(state, data) {
            if (data.manoObra) {
                state.manoObra.manoObra = data.manoObra;
                const manoObra = JSON.parse(data.manoObra);
                const items = manoObra.map((puesto) => {
                    var costoUnitario = 0;
                    if (state.costoActivo.jornadaLaboral == 8) {
                        costoUnitario =
                            puesto.costoUnitario *
                            state.costoFactores.jornadaOcho *
                            puesto.factorUtilizacion;
                    } else if (state.costoActivo.jornadaLaboral == 12) {
                        costoUnitario =
                            puesto.costoUnitario *
                            state.costoFactores.jornadaDoce *
                            puesto.factorUtilizacion;
                    }
                    return {
                        cantidad: puesto.cantidad,
                        puesto: puesto.puesto,
                        costoUnitario: costoUnitario,
                        total:
                            costoUnitario *
                            puesto.cantidad *
                            state.costoActivo.jornadaInterna,
                    };
                });
                const total = items.reduce(
                    (suma, item) => suma + parseFloat(item.total),
                    0
                );
                state.dataManoObra = {
                    items: items,
                    total: total * state.costoFactores.moDisponible,
                };
                state.cuentaPlaneada.manoObra = state.dataManoObra.total;
            } else  {
                state.dataManoObra = { items: [], total: 0 };
                state.cuentaPlaneada.manoObra = 0;
            }
        },

        //HERRAMIENTA Y EQUIPOS
        setHerramientaEquipo(state, data) {
            state.herramientaEquipo = data;
        },
        updateHerramientaEquipo(state, data) {
            if (data.herramientasEquipos) {
                state.herramientaEquipo.herramientasEquipos = data.herramientasEquipos;
                const herramientaEquipos = JSON.parse(
                    data.herramientasEquipos
                );
                const items = herramientaEquipos.map((articulo) => {
                    return {
                        cantidad: articulo.cantidad,
                        unidad: articulo.unidad,
                        descripcion: articulo.descripcion,
                        costoUnitario: articulo.costoUnitario,
                        total: articulo.cantidad * articulo.costoUnitario,
                    };
                });
                const total = items.reduce(
                    (suma, item) => suma + parseFloat(item.total),
                    0
                );
                state.cuentaPlaneada.herramientaEquipo = total;
                state.herramientasEquipos = { items: items, total: total };
            }else {
                state.herramientasEquipos = { items: [], total: 0 };
                state.cuentaPlaneada.herramientaEquipo = 0;
            }
        },

        //GASTOS ESPECIALES
        setGastoEspecial(state, data) {
             state.gastoEspecial = data;
        },
        updateGastoEspecial(state, data) {
            if (data.gastosEspeciales) {
                state.gastoEspecial.gastosEspeciales = data.gastosEspeciales;
                const gastosEspeciales = JSON.parse(
                    data.gastosEspeciales
                );
                const items = gastosEspeciales.map((gasto) => {
                    return {
                        cantidad: gasto.cantidad,
                        unidad: gasto.unidad,
                        descripcion: gasto.descripcion,
                        costoUnitario: gasto.costoUnitario,
                        total: gasto.cantidad * gasto.costoUnitario,
                    };
                });
                const total = items.reduce(
                    (suma, item) => suma + parseFloat(item.total),
                    0
                );
                state.cuentaPlaneada.gastoEspecial = total;
                state.gastosEspeciales = { items: items, total: total };
            }else {
                state.gastosEspeciales = { items: [], total: 0 };
                 state.cuentaPlaneada.gastoEspecial = 0;
            }
        },

        //GASTOS DE OPERACIÓN
        setGastoOperacion(state, data) {
             state.gastoOperacion = data;
        },
        updateGastoOperacion(state, data) {
            if (data.gastosOperacion) {
                state.gastoOperacion.gastosOperacion = data.gastosOperacion;
                const gastosOperacion = JSON.parse(
                    data.gastosOperacion
                );
                const items = gastosOperacion.map((gasto) => {
                    return {
                        cantidad: gasto.cantidad,
                        unidad: gasto.unidad,
                        descripcion: gasto.descripcion,
                        costoUnitario: gasto.costoUnitario,
                        total: gasto.cantidad * gasto.costoUnitario,
                    };
                });
                const total = items.reduce(
                    (suma, item) => suma + parseFloat(item.total),
                    0
                );
                state.cuentaPlaneada.gastoOperacion = total;
                state.gastosOperacion = { items: items, total: total };
            }else {
                state.gastosOperacion = { items: [], total: 0 };
                state.cuentaPlaneada.gastoOperacion = 0;
            }
        },

        //OBSERVACIONES
        setObservaciones(state, data) {
            state.observaciones = data;
        },
        addObservacion(state, data) {
            state.observaciones.push(data);
        },
        removeObservacion(state, id) {
            const index = state.observaciones.findIndex(observacion_state => {
                return observacion_state.idObservacion == id;
            });
            state.observaciones.splice(index, 1);
        },
        updateObservacion(state, data) {
            const observacion_state = state.observaciones.find(function(observacion_state) {
                return observacion_state.idObservacion == data.idObservacion;
            });
            if (observacion_state) {
                observacion_state.observacion = data.observacion;
            }
        },

        //INVENTARIO ACTIVOS
        // setActivos(state, data) {
        //     state.activos = data;
        // },
        // setCategorias(state, data) {
        //     state.categorias = data;
        // },

        //MATERIALES E INSUMOS
        setMaterialInsumo(state, data) {
            state.materialInsumo = data;
        },
        updateMaterialInsumo(state, data) {
             if (data.materialesInsumos) {
                state.materialInsumo.materialesInsumos = data.materialesInsumos;
                const materialesInsumos = JSON.parse(
                    data.materialesInsumos
                );
                const items = materialesInsumos.map((item) => {
                    return {
                        id: item.idMaterial,
                        cantidad: item.cantidad,
                        unidad: item.unidad,
                        descripcion: item.descripcion,
                        costoUnitario: item.costoUnitario,
                        categoria: item.categoria,
                        total: item.cantidad * item.costoUnitario,
                    };
                });
                var total = items.reduce(
                    (suma, item) => suma + parseFloat(item.total),
                    0
                );
                state.cuentaPlaneada.insumo = total;
                state.materialesInsumos = { items: items, total: total };
            }else {
                state.materialesInsumos = { items: [], total: 0 };
                state.cuentaPlaneada.insumo = 0;
            }
        },
        setConfCostoEtapa(state, data) {
            state.confCostoEtapa = data;
        },
    },
    actions: {
        // COSTO OPERATIVO
        getCostos(context) {
            axios.get(BaseUrl + `/api/op/co/costos`).then(response => {
                context.commit("setListCostos", response.data);
            });
        },
        getCostoFactores(context) {
            axios
                .get(BaseUrl + `/api/op/co/costo/${context.state.costoActivo.idCosto}/factores`)
                .then(response => {
                    context.commit("setCostoFactores", response.data);
                });
        },
        getCostoSeguridad(context) {
            axios
                .get(BaseUrl + `/api/op/co/costo/${context.state.costoActivo.idCosto}/seguridad`)
                .then(response => {
                    context.commit("setCostoSeguridad", response.data);
                });
        },
        //MANO DE OBRA
        getManoObra(context) {
           axios
                .get(BaseUrl + `/api/op/co/costo/${context.state.costoActivo.idCosto}/manoObra`)
                .then(response => {
                    context.commit("setManoObra", response.data);
                    context.commit("updateManoObra", response.data);

                }); 
        },
        //HERRAMIENTA Y EQUIPOS
        getHerramientaEquipo(context) {
           axios
                .get(BaseUrl + `/api/op/co/costo/${context.state.costoActivo.idCosto}/herramientaEquipo`)
                .then(response => {
                    context.commit("setHerramientaEquipo", response.data);
                    context.commit("updateHerramientaEquipo", response.data);
                }); 
        },
        //GASTOS ESPECIALES
        getGastoEspecial(context) {
           axios
                .get(BaseUrl + `/api/op/co/costo/${context.state.costoActivo.idCosto}/gastoEspecial`)
                .then(response => {
                    context.commit("setGastoEspecial", response.data);
                    context.commit("updateGastoEspecial", response.data);
                }); 
        },
        //GASTOS DE OPERACIÓN 
        getGastoOperacion(context) {
            axios
                .get(BaseUrl + `/api/op/co/costo/${context.state.costoActivo.idCosto}/gastoOperacion`)
                .then(response => {
                    context.commit("setGastoOperacion", response.data);
                    context.commit("updateGastoOperacion", response.data);
                });
        },
        //OBSERVACIONES
        getObservaciones(context) {
            axios
                .get(BaseUrl + `/api/op/co/costo/${context.state.costoActivo.idCosto}/observaciones`)
                .then(response => {
                    context.commit("setObservaciones", response.data);
                });
        },
        //INVENTARIO ACTIVOS
        // getActivos(context) {
        //     axios
        //         .get(BaseUrl + `/api/op/co/activos`)
        //         .then(response => {
        //             context.commit("setActivos", response.data);
        //         });
        // },
        // getActivoCategorias(context) {
        //     axios
        //         .get(BaseUrl + `/api/op/co/activo/categorias/select`)
        //         .then((response) => {
        //             if (response.data){
        //                 const categorias = response.data.map((item) => item.categoria);
        //                 context.commit("setCategorias", categorias);
        //             }
        //         });

        // },
        //MATERIALES E INSUMOS
        getMaterialInsumo(context) {
            axios
                .get(BaseUrl + `/api/op/co/costo/${context.state.costoActivo.idCosto}/materialInsumo`)
                .then(response => {
                    context.commit("setMaterialInsumo", response.data);
                    context.commit("updateMaterialInsumo", response.data);
                });
        },
        //CONF. COSTO ETAPAS
        getConfCostoEtapa(context) {
            axios
                .get(BaseUrl + `/api/op/co/config/costo/etapa`)
                .then(response => {
                    context.commit("setConfCostoEtapa", response.data);
                });
        },
    },
    getters: {
        costosFiltered(state) {
            var costos = [];
            costos = state.listCostos;
            if (state.filterInicio && state.filterFin) {
                costos = costos.filter(
                    costo =>
                        costo.created_at > state.filterInicio &&
                        costo.created_at < state.filterFin
                );
            }
            if (state.filterSearch) {
                costos = costos.filter(
                    costo =>
                        costo.cuenta
                            .toLowerCase()
                            .indexOf(state.filterSearch.toLowerCase()) > -1 ||
                        costo.proyecto
                            .toLowerCase()
                            .indexOf(state.filterSearch.toLowerCase()) > -1
                );
            } 
            if (state.filterStatus) {
                if (state.filterStatus === "Facturadas") {
                    costos = costos.filter(
                        costo =>
                            costo.cuentaReal === "Activo"
                    );
                }else if (state.filterStatus === "Perdidas") {
                    costos = costos.filter(
                        costo => {
                            const config = state.confCostoEtapa.find(
                                (item) => item.idEmbudo == costo.idEmbudo
                            );
                            if (costo.etapa_negocio == config.etapaPerdido) {
                                return costo;
                            }
                        }
                    );
                }
            }
            return costos
                .sort(function(a, b) {
                    return a.created_at - b.created_at;
                }).reverse();
        }
    }
};
