export default {
    namespaced: true,
    state: {
        actividades: [],
        actividad: null,
        clienteActividadSelected: null,
        cambiosActividades: 0,
        fecha: '',
        semana: ''
    },
    mutations: {
        addActividad(state, actividad){
            state.actividades.push(actividad)
        },
        setActividades(state, data) {
            state.actividades = data.actividades
            state.semana = data.semana
        },
        updateActividad(state, actividad) {
            const actividad_state = state.actividades.find(function(actividad_state) {
                return actividad_state.idActividad == actividad.idActividad
            })

            actividad_state.turno = actividad.turno
            actividad_state.semana = actividad.semana
            actividad_state.fecha = actividad.fecha
            actividad_state.equipo = actividad.equipo
            actividad_state.claveCliente = actividad.claveCliente
            actividad_state.consecutivo = actividad.consecutivo
            actividad_state.claveEquipo = actividad.claveEquipo
            actividad_state.claveUnidad = actividad.claveUnidad
            actividad_state.claveServicio = actividad.claveServicio
            actividad_state.horas = actividad.horas
            actividad_state.descripcion = actividad.descripcion
            actividad_state.inicio = actividad.inicio
            actividad_state.fin = actividad.fin
            actividad_state.cuenta = actividad.cuenta
            actividad_state.tipoCuenta = actividad.tipoCuenta
            actividad_state.tipoTiempo = actividad.tipoTiempo
        },
        deleteActividad(state, idActividad) {
            const index = state.actividades.findIndex((actividad) => {
                  return actividad.idActividad == idActividad;
            });

            state.actividades.splice(index, 1);
        }
    },
    actions: {
        getActividades(context, data){
            axios.get(BaseUrl + `/api/op/ra/actividades/${data.noEmpleado}/${data.fecha}`)
                .then(
                    (response) => context.commit('setActividades', response.data)
                );
        },
    },
    getters: {
        filteredActividadesByCliente(state)
        {
            if(state.clienteActividadSelected!=null){
                return state.actividades.filter(
                    actividad => actividad.idCliente == state.clienteActividadSelected
                    );
            }
            return state.actividades;
        },

    }
}