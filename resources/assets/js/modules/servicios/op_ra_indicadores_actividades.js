export default {
    namespaced: true,
    state: {
        actividades: [],
        horasOptimas: [],
        periodo: 2023,
        loadingActividades: false,
        loadingHorasOptimas: false,
        fechaInicio: null,
        fechaFin: null,
        empleados: [
            "015-030211", // Martin           
            "047-270611", // Cande           
            "073-231211", // Wili         
            "102-111115", // Ismael
            "142-200917", // Parachico
            "173-050919", // Cesar ricardo    
            "184-290920", // Guadalupe
            "004-260422", // Jose Guadalupe   
            "185-291020", // Gonzalo   
            "192-110221", // Fabian
            "193-240221", // Juana              
            "168-280319", // Ricardo
            "203-120821", // Victor Hugo       
            "204-120821", // Angel Manuel      
            "212-271021", // Juan José         
            "268-050222", // Jesus Amador    
            "279-170322", // Hector mendez   
            "284-040622", // Michelle
            "290-241122", // Eduardo Daniel   
            "295-270223", // Anahí
            "296-090323", // Alejandro Moreno
            "140-030817", // Margarita
            "297-090323", // Jesus alonso
            "293-230223", // Citlalli
            "294-230223", // Ana getsemani
            "288-200922", // Erik
            "287-250822", // Francisco
            "289-241122", // karen L. Vázquez
            "236-031121", // Raymundo Hinojosa
            "131-051116", // Oscar Martinez
            "083-120813", // Guillermo Murillo
            "300-290623", // Luis Antonio Alcalá
            "301-050723", // José Ricardo González
            "302-050723", // Juan González
            "303-280823", // Jose Ascencion Martinez
            "305-091023", // Rosa Yaneth Santillán Félix
        ],
        empleadosLineaNegocio: {  // NOTA: SI SE ACTUALIZA ESTA LISTA SE TIENE QUE ACTUALIZAR EN EL CONTROLADOR
            "TM": [
                "279-170322", // Hector mendez
                "295-270223", // Anahí
                "289-241122", // karen L. Vázquez
                "073-231211", // Wilibaldo (Supervisor)
            ],
            "CM": [
                "204-120821", // Angel Manuel
                "015-030211", // Martin
                "290-241122", // Eduardo Daniel
                "288-200922", // Erik
                "284-040622", // Michelle
                "287-250822", // Francisco
                "203-120821", // Victor Hugo
                "047-270611", // Cande
                "212-271021", // Juan José
                "296-090323", // Alejandro Moreno
                "004-260422", // Jose Guadalupe
                "168-280319", // Ricardo
                "140-030817", // Margarita
                "297-090323", // Jesus alonso
                "293-230223", // Citlalli
                "294-230223", // Ana getsemani
                "300-290623", // Luis Antonio Alcalá
                "301-050723", // José Ricardo González
                "302-050723", // Juan González
                "303-280823", // Jose Ascencion Martinez
                "305-091023", // Rosa Yaneth Santillán Félix
                "268-050222", // Jesus Amador (Supervisor)
            ],
            "RD": [
                "193-240221", // Juana
                "173-050919", // Cesar ricardo (Supervisor)
            ]
        },
        lineaNegocio: "Todas",
    },
    mutations: {
        setActividades(state, actividades) {
            state.actividades = actividades.filter((act) => {
                if (state.empleados.includes(act.noEmpleado)) {
                    return act;
                }
            });
            // state.actividades = actividades;
        },
        setHorasOptimas(state, data) {
            state.horasOptimas = data
        },
        updateHoraOptima(state, data) {
            let horaOptima = state.horasOptimas.find((horaOptima) => horaOptima.id == data.id)
            if(horaOptima)
                horaOptima.horas = data.horas;
            else 
                state.horasOptimas.push(data)
        },
        setPeriodo(state, periodo) {
            state.periodo = periodo
        },
        setFechaInicio(state, fecha) {
            state.fechaInicio = fecha;
        },
        setFechaFin(state, fecha) {
            state.fechaFin = fecha;
        },
        setLoadingActividades(state, value) {
            state.loadingActividades = value
        },
        setLoadingHorasOptimas(state, value) {
            state.loadingHorasOptimas = value
        },
        setLineaNegocio(state, data) {
            state.lineaNegocio = data;
        },
    },
    actions: {
        getActividades(context) {
            context.commit('setLoadingActividades', true)
            axios.get(BaseUrl + `/api/op/ra/actividades`, {
                params: {
                  periodo: context.state.periodo,
                  fechaInicio: context.state.fechaInicio,
                  fechaFin: context.state.fechaFin
                }
            })
                .then(
                    (response) => {
                        context.commit('setActividades', response.data)
                        context.commit('setLoadingActividades', false)
                    }
                );
        },
        getHorasOptimas(context) {
            context.commit('setLoadingHorasOptimas', true)
            axios.get(BaseUrl + `/api/op/ra/indicadores/horas/optimas/${context.state.periodo}`)
                .then(
                    (response) => {
                        context.commit('setHorasOptimas', response.data)
                        context.commit('setLoadingHorasOptimas', false)
                    }
                );
        },
    },
    getters: {
        actividadesFiltered(state) {
            if (state.lineaNegocio && state.actividades) {
                if (state.lineaNegocio == "Todas") {
                    return state.actividades.filter((act) => {
                        if (act.tipoCuenta == "Externa") {
                            var arrCuenta = act.cuenta.split("-");
                            if (arrCuenta[2] == "TM" || arrCuenta[2] == "CM" || arrCuenta[2] == "RD") {
                                return act;
                            }
                        }else if (act.tipoCuenta == "Interna") {
                            if (moment(moment(act.fecha)).isBefore('2023-08-15')) {
                                if (state.empleadosLineaNegocio["TM"].includes(act.noEmpleado)) {
                                    return act;
                                } else if (state.empleadosLineaNegocio["CM"].includes(act.noEmpleado)){
                                    return act;
                                } else if (state.empleadosLineaNegocio["RD"].includes(act.noEmpleado)){
                                    return act;
                                } 
                            }else {
                                var arrCuenta = act.cuenta.split("-");
                                if (arrCuenta[0] == "TM" || arrCuenta[0] == "CM" || arrCuenta[0] == "RD") {
                                    return act;
                                }
                            }
                        }else if (act.tipoCuenta == "Capacitacion") {
                            if (state.empleadosLineaNegocio["TM"].includes(act.noEmpleado)) {
                                return act;
                            }else if (state.empleadosLineaNegocio["CM"].includes(act.noEmpleado)) {
                                return act;
                            }else if (state.empleadosLineaNegocio["RD"].includes(act.noEmpleado)) {
                                return act;
                            }
                        }
                    });
                }else if (state.lineaNegocio == "MM" || state.lineaNegocio == "SC") {
                    return state.actividades.filter((act) => {
                        if (act.tipoCuenta == "Externa") {
                            var arrCuenta = act.cuenta.split("-");
                            if (arrCuenta[2] == state.lineaNegocio) {
                                return act;
                            }
                        }
                    });
                }else {
                     return state.actividades.filter((act) => {
                        if (act.tipoCuenta == "Externa") {
                            var arrCuenta = act.cuenta.split("-");
                            if (arrCuenta[2] == state.lineaNegocio) {
                                return act;
                            }
                        }else if (act.tipoCuenta == "Interna") {
                            if (moment(moment(act.fecha)).isBefore('2023-08-15')) {
                                if (state.empleadosLineaNegocio[state.lineaNegocio].includes(act.noEmpleado)) {
                                    return act;
                                }  
                            }else {
                                var arrCuenta = act.cuenta.split("-");
                                if (arrCuenta[0] == state.lineaNegocio) {
                                    return act;
                                }
                            }
                        }else if (act.tipoCuenta == "Capacitacion") {
                            if (state.empleadosLineaNegocio[state.lineaNegocio].includes(act.noEmpleado)) {
                                return act;
                            }
                        }
                    });

                }
            } else return state.actividades;
        }
    },
}