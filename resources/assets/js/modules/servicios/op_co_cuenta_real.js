export default {
    namespaced: true,
    state: {        
        //MANO DE OBRA
        manoObra: { 
            items: [], 
            total: 0 
        },
        
        //GASTOS DE OPERACIÓN
        gastosOperacion: { 
            items: [], 
            total: 0 
        },

        //MATERIALES E INSUMOS
        materialInsumo: {},
        materialesInsumos: { 
            items: [], 
            total: 0 
        },

        //GASTOS ESPECIALES
        gastoEspecial: {},
        gastosEspeciales: { 
            items: [], 
            total: 0 
        },

        //HERRAMIENTA Y EQUIPOS
        herramientaEquipo: null,
        herramientasEquipos: { 
            items: [], 
            total: 0 
        },

        //SUELDOS RH 
        sueldos: [],

        //TOTALES
        cuentaReal: {
            insumo: 0,
            manoObra: 0,
            gastoEspecial: 0,
            gastoOperacion: 0,
            herramientaEquipo: 0,
        },
    },
    mutations: {
        //MANO DE OBRA
        setManoObra(state, manoObra) {
            if (manoObra.length) {
                var items = [];
                let uniqueSemana = [
                    ...new Set(manoObra.map((item) => item.semana)),
                ];
                let uniqueNoEmpleado = [
                    ...new Set(manoObra.map((item) => item.noEmpleado)),
                ];
                uniqueSemana.forEach((semana) => {
                    uniqueNoEmpleado.forEach((noEmpleado) => {
                        let hrsDobles = 0;
                        const data = manoObra.filter(
                            (item) =>
                                item.semana == semana &&
                                item.noEmpleado == noEmpleado
                        );
                        const sueldoEmpleado = state.sueldos.find(
                            (item) => item.noEmpleado === noEmpleado
                        );

                        if (sueldoEmpleado) {
                            data.forEach((item) => {
                                let costo = 0;
                                if (item.dia != "Domingo") {
                                    if (hrsDobles < 9) {
                                        if (item.total_horas <= 8) {
                                            costo =
                                                item.total_horas *
                                                sueldoEmpleado.normal;
                                        } else if (
                                            item.total_horas > 8 &&
                                            item.total_horas <= 11
                                        ) {
                                            costo =
                                                sueldoEmpleado.normal * 8 +
                                                (item.total_horas - 8) *
                                                    sueldoEmpleado.doble;
                                            hrsDobles += item.total_horas - 8;
                                        } else {
                                            costo =
                                                sueldoEmpleado.normal * 8 +
                                                sueldoEmpleado.doble * 3 +
                                                sueldoEmpleado.triple *
                                                    (item.total_horas - 11);
                                            hrsDobles += 3;
                                        }
                                    } else {
                                        costo =
                                            item.total_horas *
                                            sueldoEmpleado.triple;
                                    }
                                } else {
                                    const sueldoDia = sueldoEmpleado.normal * 8;
                                    const prima = (25 * sueldoDia) / 100;
                                    costo = (sueldoDia * 2) + prima;
                                }
                                items.push({
                                    fecha: moment(item.fecha).format(
                                        "dddd, LL"
                                    ),
                                    responsable: item.responsable,
                                    total_horas: item.total_horas,
                                    costo: costo,
                                });

                            });

                        }
                    }, this);
                }, this);
                const total = items.reduce(
                    (suma, item) => suma + parseFloat(item.costo),
                    0
                );
                state.cuentaReal.manoObra = total;
                state.manoObra = { items: items, total: total };
            }else {
                state.manoObra = { items: [], total: 0 };
                state.cuentaReal.manoObra = 0;
            }
        },
        //GASTOS DE OPERACIÓN
        setGastosOperacion(state, data) {
            if (data.length) {
                const total = data.reduce(
                    (suma, item) => suma + parseFloat(item.total),
                    0
                );
                state.cuentaReal.gastoOperacion = total;
                state.gastosOperacion = { items: data, total: total };
            }else {
                state.gastosOperacion = { items: [], total: 0 };
                state.cuentaReal.gastoOperacion = 0;
            }
        },
        //MATERIALES E INSUMOS
        setMaterialInsumo(state, data) {
            state.materialInsumo = data;
        },
        updateMaterialInsumo(state, data) {
            if (data.materialesInsumos) {
                state.materialInsumo.materialesInsumos = data.materialesInsumos;
                const materialesInsumos = JSON.parse(
                    data.materialesInsumos
                );
                const items = materialesInsumos.map((item) => {
                    return {
                        id: item.idMaterial,
                        cantidad: item.cantidad,
                        unidad: item.unidad,
                        descripcion: item.descripcion,
                        costoUnitario: item.costoUnitario,
                        categoria: item.categoria,
                        total: item.cantidad * item.costoUnitario,
                    };
                });
                const total = items.reduce(
                    (suma, item) => suma + parseFloat(item.total),
                    0
                );
                state.cuentaReal.insumo = total;
                state.materialesInsumos = { items: items, total: total };
            }else {
                state.materialesInsumos = { items: [], total: 0 };
                state.cuentaReal.insumo = 0;
            }
        },
        //GASTOS ESPECIALES
        setGastoEspecial(state, data) {
            state.gastoEspecial = data;
        },
        updateGastoEspecial(state, data) {
            if (data.gastosEspeciales) {
                state.gastoEspecial.gastosEspeciales = data.gastosEspeciales;
                const gastosEspeciales = JSON.parse(
                    data.gastosEspeciales
                );
                const items = gastosEspeciales.map((gasto) => {
                    return {
                        cantidad: gasto.cantidad,
                        unidad: gasto.unidad,
                        descripcion: gasto.descripcion,
                        costoUnitario: gasto.costoUnitario,
                        total: gasto.cantidad * gasto.costoUnitario,
                    };
                });
                const total = items.reduce(
                    (suma, item) => suma + parseFloat(item.total),
                    0
                );
                state.cuentaReal.gastoEspecial = total;
                state.gastosEspeciales = { items: items, total: total };
            }else {
                state.gastosEspeciales = { items: [], total: 0 };
                state.cuentaReal.gastoEspecial = 0;
            }
        },
        //HERRAMIENTA Y EQUIPOS
        setHerramientaEquipo(state, data) {
            state.herramientaEquipo = data;
        },
        updateHerramientaEquipo(state, data) {
            if (data.herramientasEquipos) {
                state.herramientaEquipo.herramientasEquipos = data.herramientasEquipos;
                const herramientaEquipos = JSON.parse(
                    data.herramientasEquipos
                );
                const items = herramientaEquipos.map((articulo) => {
                    return {
                        cantidad: articulo.cantidad,
                        unidad: articulo.unidad,
                        descripcion: articulo.descripcion,
                        costoUnitario: articulo.costoUnitario,
                        total: articulo.cantidad * articulo.costoUnitario,
                    };
                });
                const total = items.reduce(
                    (suma, item) => suma + parseFloat(item.total),
                    0
                );
                state.cuentaReal.herramientaEquipo = total;
                state.herramientasEquipos = { items: items, total: total };
            }else {
                state.herramientasEquipos = { items: [], total: 0 };
                state.cuentaReal.herramientaEquipo = 0;
            }
        },

        //RH SUELDOS
        setSueldos(state, data) {
            state.sueldos = data;
        },
    },
    actions: { 
        //MANO DE OBRA
        getManoObra(context, cuenta) {
           axios
                .get(BaseUrl + `/api/op/co/cr/manoObra/${cuenta}`)
                .then(response => {
                    context.commit("setManoObra", response.data);
                }); 
        },
        //GASTOS DE OPERACIÓN
        getGastosOperacion(context, cuenta) {
            axios
                .get(BaseUrl + `/api/op/co/cr/gastosOperacion/${cuenta}`)
                .then(response => {
                    context.commit("setGastosOperacion", response.data);
                });
        },
        //MATERIALES E INSUMOS
        getMaterialInsumo(context, idCosto) {
            axios
                .get(BaseUrl + `/api/op/co/${idCosto}/cr/materialInsumo`)
                .then(response => {
                    context.commit("setMaterialInsumo", response.data);
                    context.commit("updateMaterialInsumo", response.data);
                });
        },
        //GASTOS ESPECIALES
        getGastoEspecial(context, idCosto) {
           axios
                .get(BaseUrl + `/api/op/co/${idCosto}/cr/gastoEspecial`)
                .then(response => {
                    context.commit("setGastoEspecial", response.data);
                    context.commit("updateGastoEspecial", response.data);
                }); 
        },
        //SUELDOS RH
        getSueldos(context) {
           axios
                .get(BaseUrl + `/api/op/co/rh/sueldos`)
                .then(response => {
                    context.commit("setSueldos", response.data);
                }); 
        },
        //HERRAMIENTA Y EQUIPOS
        getHerramientaEquipo(context, idCosto) {
           axios
                .get(BaseUrl + `/api/op/co/${idCosto}/cr/herramientaEquipo`)
                .then(response => {
                    context.commit("setHerramientaEquipo", response.data);
                    context.commit("updateHerramientaEquipo", response.data);
                }); 
        },
    },
    getters: {}
};
