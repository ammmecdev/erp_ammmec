export default {
    namespaced: true,
    state: {
        cuentasContables: [],
        arraySelected: [],
        fields: [
            {
                key: "area",
                label: "Área",
                sortable: true,
                class: "text-center",
                thStyle: { width: "180px" },
            },
            {
                key: "cuentaMayor",
                label: "Cuenta Mayor",
                sortable: true,
                class: "text-center",
                thStyle: { width: "280px" },
            },
            {
                key: "subcuenta",
                label: "Subcuenta",
                sortable: true,
                class: "text-center",
                thStyle: { width: "280px" },
            },
            {
                key: "noCuenta",
                label: "Número de cuenta",
                sortable: true,
                class: "text-center",
                thStyle: { width: "180px" },
            },
        ],
    },
    mutations: {
        setCuentasContables(state, cuentasContables) {
            state.cuentasContables = cuentasContables
        },
        setArraySelected(state, data) {
            if (data.cuentas_contables) state.arraySelected = JSON.parse(data.cuentas_contables)
            else state.arraySelected = []
        },
        addArea(state, data) {
            const area_state = state.arraySelected.find(
                (area_state) =>
                    area_state.area === data.area
            );
            if(!area_state){
                const areaSelected = {
                    area: data.area,
                    cuentas: []
                }
                state.arraySelected.push(areaSelected)
            }      
        },
        addCuenta(state, data) {
            const area_state = state.arraySelected.find(
                (area_state) =>
                    area_state.area === data.area
            );
            if(area_state) {
                const cuenta_state = area_state.cuentas.find(
                    (cuenta_state) =>
                        cuenta_state.cuenta === data.cuenta
                );
                if (!cuenta_state) {
                    const cuentaSelected = {
                        cuenta: data.cuenta,
                        subcuentas: []
                    }
                    area_state.cuentas.push(cuentaSelected)
                }      
            }
        },
        addSubcuentas(state, data) {
            const area_state = state.arraySelected.find(
                (area_state) =>
                    area_state.area === data.area
            );
            if (area_state) {
                const cuenta_state = area_state.cuentas.find(
                    (cuenta_state) =>
                        cuenta_state.cuenta === data.cuenta
                );
                if(cuenta_state) 
                    cuenta_state.subcuentas = data.subcuentas
            }
        },
        delete(state, data) {
            const area_state = state.arraySelected.find(
                (area_state) =>
                    area_state.area === data.area
            );
            if(area_state) {
                if (area_state.cuentas) {
                    const cuenta_index = area_state.cuentas.findIndex(
                        (cuenta_state) =>
                            cuenta_state.cuenta == data.cuenta
                    );
                    area_state.cuentas.splice(cuenta_index, 1);
                    if (!area_state.cuentas.length) {
                        const area_index = state.arraySelected.findIndex(
                            (area_state) =>
                                area_state.area == data.area
                        );
                        state.arraySelected.splice(area_index, 1);
                    }
                } 
            }
        },
    },
    actions: {
        getCuentasContables(context) {
            axios
                .get(
                    BaseUrl + 
                        `/api/fn/cuentas-contables`
                )
                .then((response) => {
                    context.commit("setCuentasContables", response.data);
                });
        },
        getCuentasByUsuario(context, idUsuario) {
            axios
                .get(
                    BaseUrl +
                        `/api/fn/cuentas/usuarios/${idUsuario}`
                )
                .then((response) => {
                    context.commit("setArraySelected", response.data);
                });
        },
        getCuentasContablesByArea(context, area) {
            axios
                .get(
                    BaseUrl + 
                        `/api/fn/cuentas-contables/areas/${area}`
                )
                .then((response) => {
                    context.commit("setCuentasContables", response.data);
                });
        },
    },
    getters: {},
};
