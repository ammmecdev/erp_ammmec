export default {
    namespaced: true,
    state: {
        //CONFIGURACIÓN
        arraySelected: [],

        //VISTA CUENTAS INTERNAS
        cuentasInternas: [],
        unidadesNegocio: [],
        nombres: [],
        fields: [
            {
                key: "unidadNegocio",
                label: "Unidad de Negocio",
                sortable: true,
                class: "text-center",
                thStyle: { width: "280px" },
            },
            {
                key: "area",
                label: "Área",
                sortable: true,
                class: "text-center",
                thStyle: { width: "180px" },
            },
            {
                key: "tipo",
                label: "Tipo",
                sortable: true,
                class: "text-center",
                thStyle: { width: "180px" },
            },
            {
                key: "nombre",
                label: "Nombre",
                sortable: true,
                class: "text-center",
                thStyle: { width: "250px" },
            },
            {
                key: "cuenta",
                label: "Cuenta",
                sortable: true,
                class: "text-center",
                thStyle: { width: "180px" },
            },
            {
                key: "actions",
                label: "Acción",
                class: "text-center",
                thStyle: { width: "80px" },
            },
        ],
    },
    mutations: {

        //VISTA CUENTAS INTERNAS
        setCuentasInternas(state, cuentasInternas) {
            state.cuentasInternas = cuentasInternas
        },
        setNombres(state, nombres) {
            state.nombres = nombres
        },
        setUnidadesNegocio(state, unidadesNegocio) {
            state.unidadesNegocio = unidadesNegocio
        },
        addCuentaInterna(state, cuenta) {
            state.cuentasInternas.push(cuenta);
        },
        updateCuentaInterna(state, cuenta) {
            const cuentaInterna = state.cuentasInternas.find((cuentaInterna) => {
                return cuentaInterna.idCuenta == cuenta.idCuenta;
            });
            cuentaInterna.area = cuenta.area;
            cuentaInterna.tipo = cuenta.tipo;
            cuentaInterna.cuenta = cuenta.cuenta;
            cuentaInterna.unidadNegocio = cuenta.unidadNegocio;
            cuentaInterna.nombre = cuenta.nombre;
        },
        removeCuentaInterna(state, idCuenta) {
            const index = state.cuentasInternas.findIndex((cuentaInterna) => {
                return cuentaInterna.idCuenta == idCuenta;
            });
            state.cuentasInternas.splice(index, 1);
        },



        //CONFIGURACIÓN
        setArraySelected(state, data) {
            if (data.cuentas_internas) state.arraySelected = JSON.parse(data.cuentas_internas)
            else state.arraySelected = []
        },
        addArea(state, data) {
            const area_state = state.arraySelected.find(
                (area_state) =>
                    area_state.area === data.area
            );
            if(!area_state){
                const areaSelected = {
                    area: data.area,
                    unidades_negocio: []
                }
                state.arraySelected.push(areaSelected)
            }      
        },
        addUnidadNegocio(state, data) {
            const area_state = state.arraySelected.find(
                (area_state) =>
                    area_state.area === data.area
            );
            if(area_state) {
                const unidad_negocio_state = area_state.unidades_negocio.find(
                    (unidad_negocio_state) =>
                        unidad_negocio_state.unidad_negocio === data.unidad_negocio
                );
                if (!unidad_negocio_state) {
                    const unidadNegocioSelected = {
                        unidad_negocio: data.unidad_negocio,
                        cuentas: []
                    }
                    area_state.unidades_negocio.push(unidadNegocioSelected)
                }      
            }
        },
        addCuentas(state, data) {
            const area_state = state.arraySelected.find(
                (area_state) =>
                    area_state.area === data.area
            );
            if (area_state) {
                const unidad_negocio = area_state.unidades_negocio.find(
                    (unidad_negocio) =>
                        unidad_negocio.unidad_negocio === data.unidad_negocio
                );
                if(unidad_negocio)
                    unidad_negocio.cuentas = data.cuentas
            }
        },
        delete(state, data) {
            const area_state = state.arraySelected.find(
                (area_state) =>
                    area_state.area === data.area
            );
            if(area_state) {
                if (area_state.unidades_negocio) {
                    const cuenta_index = area_state.unidades_negocio.findIndex(
                        (unidad_negocio_state) =>
                            unidad_negocio_state.unidad_negocio == data.unidad_negocio
                    );
                    area_state.unidades_negocio.splice(cuenta_index, 1);
                    if (!area_state.unidades_negocio.length) {
                        const area_index = state.arraySelected.findIndex(
                            (area_state) =>
                                area_state.area == data.area
                        );
                        state.arraySelected.splice(area_index, 1);
                    }
                } 
            }
        },
    },
    actions: {
        getCuentasInternas(context) {
            axios
                .get(
                    BaseUrl + 
                        `/api/fn/cuentas-internas`
                )
                .then((response) => {
                    context.commit("setCuentasInternas", response.data);
                });
        },
        getCuentasByUsuario(context, idUsuario) {
            axios
                .get(
                    BaseUrl + 
                        `/api/fn/cuentas/usuarios/${idUsuario}`
                )
                .then((response) => {
                    context.commit("setArraySelected", response.data);
                });
        },
        getCuentasInternasByArea(context, area) {
            axios
                .get(
                    BaseUrl + 
                        `/api/fn/cuentas-internas/areas/${area}`
                )
                .then((response) => {
                    context.commit("setCuentasInternas", response.data);
                });
        },
        getNombres(context) {
            axios
                .get(
                    BaseUrl +
                        `/api/fn/cuentas-internas/nombres/select`
                )
                .then((response) => {
                    if (response.data){
                        const nombres = response.data.map((nombre) => nombre.nombre);
                        context.commit("setNombres", nombres)
                    }
                });
        },
        getUnidadesNegocio(context) {
            axios
                .get(
                    BaseUrl +
                        `/api/fn/cuentas-internas/unidad-negocio/select`
                )
                .then((response) => {
                    if (response.data){
                        const unidadesNegocio = response.data.map((un) => un.unidadNegocio);
                        context.commit("setUnidadesNegocio", unidadesNegocio)
                    }
                });
        },
    },
    getters: {},
};
