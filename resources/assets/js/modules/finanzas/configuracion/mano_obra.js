export default {
    namespaced: true,
    state: { 
        factores: [],
        puestos: [],
    },
    mutations: {
        setPuestos(state, data) {
            state.puestos = data;
        },
        setFactores(state, data) {
            state.factores = data;
        },
        updatePuesto(state, data) {
            const puesto_state = state.puestos.find(function(puesto_state) {
                return puesto_state.idPuesto == data.idPuesto;
            });
            if (puesto_state) {
                puesto_state.sueldo = data.sueldo;
                puesto_state.factorUtilizacion = data.factorUtilizacion;
            }
        }, 
    },
    actions: {
        getPuestos(context) {
            axios
                .get(BaseUrl + `/api/fn/co/puestos`)
                .then(response => {
                    context.commit("setPuestos", response.data);
                });
        },
        getFactores(context) {
            axios
                .get(BaseUrl + `/api/fn/co/factores`)
                .then(response => {
                    context.commit("setFactores", response.data);
                });
        },
    },
    getters: {}
};
