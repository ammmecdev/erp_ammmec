export default {
    namespaced: true,
    state: {
        saldoAux: 0.00,
        sumaCargos: 0.00,
        sumaDepositos: 0.00,
    },
    mutations: {
        setSaldoInicial(state, saldoInicial){
            state.saldoAux = saldoInicial
            state.sumaCargos = 0.00
            state.sumaDepositos = 0.00

        },
        updateSaldoAux(state, movimiento) {
            if(movimiento.accion == 'dispersion') {
                state.saldoAux = Number(state.saldoAux) + Number(movimiento.importe)
                state.sumaCargos = Number(state.sumaCargos) + Number(movimiento.importe)
            }else if(movimiento.accion == 'gasto') {
                state.saldoAux = Number(state.saldoAux) - Number(movimiento.importe)
                state.sumaDepositos = Number(state.sumaDepositos) + Number(movimiento.importe)
            }
            else if (movimiento.accion == 'reembolso') {
                state.saldoAux = Number(state.saldoAux) + Number(movimiento.importe)
                state.sumaDepositos = Number(state.sumaDepositos) + Number(movimiento.importe)
            }
            else if (movimiento.accion == 'decremento') {
                state.saldoAux = Number(state.saldoAux) - Number(movimiento.importe)
                state.sumaDepositos = Number(state.sumaDepositos) + Number(movimiento.importe)
            }
            state.saldoAux = parseFloat(state.saldoAux).toFixed(2);
            state.sumaCargos = parseFloat(state.sumaCargos).toFixed(2);
            state.sumaDepositos = parseFloat(state.sumaDepositos).toFixed(2);
        },
    },
    actions: {
        
    },
    getters: {},
};
