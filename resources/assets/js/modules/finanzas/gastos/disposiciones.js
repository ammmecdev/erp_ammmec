export default {
	namespaced: true,
	state: {
		listDisposiciones: [],
		busyDisposiciones: true,
	},
	mutations: {
		setDisposiciones(state, data) {
			state.listDisposiciones = data;
		},
		addDisposicion(state, data) {
			state.listDisposiciones.push(data);
		},
		updateDisposicion(state, disposicion) {
			const disposicion_state = state.listDisposiciones.find(function(
				disposicion_state
			) {
				return disposicion_state.idDisposicion == disposicion.idDisposicion;
			});
			if (disposicion_state) {
				disposicion_state.fecha = disposicion.fecha;
				disposicion_state.formaPago = disposicion.formaPago;
				disposicion_state.importe = disposicion.importe;
				disposicion_state.comision = disposicion.comision;
				disposicion_state.file = disposicion.file;
				disposicion_state.ruta = disposicion.ruta;
			}
		},
		removeDisposicion(state, idDisposicion) {
			const index = state.listDisposiciones.findIndex((item) => {
				return item.idDisposicion == idDisposicion;
			});
			state.listDisposiciones.splice(index, 1);
		},
		changeBusyDisposiciones(state, value) {
			state.busyDisposiciones = value;
		}
	},
	actions: {
		getDisposicionesEfectivo(context, idSolicitud) {
			context.commit("changeBusyDisposiciones", true);
			axios.get(`/api/fn/cg/solicitudes/disposiciones/${idSolicitud}`).then((response) => { 
				context.commit("setDisposiciones", response.data);
				context.commit("changeBusyDisposiciones", false);

			});
		},
	},
};