export default {
    namespaced: true,
    state: {
        dataFilter: {
            inicio: "",
            fin: "",
        },
    },
    mutations: {
        setDataFilter(state, dataFilter) {
            state.dataFilter = dataFilter;
        },
    },
    actions: {},
    getters: {},
};
