export default {
	namespaced: true,
	state: {
		solicitudActiva: null,
		listSolicitudes: [],
		resumenSolicitudActiva: [],
		planeacionSolicitudActiva: [],
		movimientos: [],
		listComprobaciones: [],
		busyResumen: true,
		busyMovimientos: true,
		busyPlaneacion: true,
		busyComprobaciones: true,
		listEtapas: [],
		busySolicitudes: false,
		tipoSolicitudes: "",
		filterEtapa: 0,
		filterInicio: null,
		filterFin: null,
		filterSearch: null,
		filterUnidadNegocio: null,
		formasPago: [
			{ value: "Efectivo", text: "Efectivo" },
			{ value: "Efectivo Caja Chica", text: "Efectivo Caja Chica" },
			{ value: "TE Proveedor", text: "Transferencia Electrónica a Proveedor"},
			{ value: "TC Empresarial", text: "Tarjeta de Crédito Empresarial" },
			{ value: "Tarjeta Banbajio", text: "Tarjeta Banbajio" },
			{ value: "Banbajio Efectivo", text: "Banbajio Efectivo" },
			{ value: "Tarjeta Personal", text: "Tarjeta Personal" },
			{ value: "TP Efectivo", text: "TP Efectivo" },
		],

		// CAMBIO 07/24 - Clasificación del gasto, asignación de tarjeta, 
		// número de personas, tipo de moneda, periodo del gasto, etc.
		idLimiteCambio: 1291, // LIMITE DE ID PARA VISUALIZAR CAMBIOS 07/24
		limiteCambio: false,

		arrClasifGasto: {
			alimentacion: {
				cuentas: ["1203"],
				descripcion: "GASTOS DE ALIMENTACIÓN",
				clasificacion: "Alimentación",
			},
			hospedaje: {
				cuentas: ["1201"],
				descripcion: "GASTOS DE HOSPEDAJE",
				clasificacion: "Hospedaje",
			},
			transportes: {
				cuentas: ["1202"],
				descripcion: "GASTOS DE TRANSPORTE",
				clasificacion: "Transportes",
			},
			combustibles: {
				cuentas: ["0701", "0702"],
				descripcion: "GASTOS DE COMBUSTIBLES",
				clasificacion: "Combustibles",
			},
			peajes: {
				cuentas: ["1205", "1204"],
				descripcion: "GASTOS DE PEAJES",
				clasificacion: "Peajes",
			},
		},
		// Se utilizán para la asignación de viáticos
		puestosAsignacion: [
			"DRN", // Dirección 
			"CDT", // Tesoresía
			"GDS", // Gerente servicio
			"CSC", // Coordinador de servicio en campo
			"CCS", // Coordinador de centro de servicio
			"CRD", // Coordinador de RedData
			"TSA", // Técnico de servicio A
			"TSB", // Técnico de servicio B
			"TSC", // Técnico de servicio C
			"ASR", // Asistente de servicio
			"CSS", // Coordinador de SSMA Provisional Memo
		],
	},
	mutations: {
		setFilterSearch(state, search) {
			state.filterSearch = search;
		},
		setFilterInicio(state, inicio) {
			state.filterInicio = inicio;
		},
		setFilterFin(state, fin) {
			state.filterFin = fin;
		},
		setFilterUN(state, unidadNegocio) {
			state.filterUnidadNegocio = unidadNegocio;
		},
		setFilterEtapa(state, etapa) {
			state.filterEtapa = etapa;
		},
		setTipoSolicitudes(state, tipo) {
			state.solicitudActiva = null;
			state.tipoSolicitudes = tipo;
		},
		setSolicitudActiva(state, solicitud) {
			state.solicitudActiva = solicitud;
			if (state.solicitudActiva.idSolicitud > state.idLimiteCambio) {
				state.limiteCambio = true;
			}else state.limiteCambio = false;
		},
		setListSolicitudesAll(state, solicitudes) {
			state.listSolicitudes = solicitudes;
		},
		setListSolicitudes(state, solicitudes) {
			state.listSolicitudes = solicitudes;
		},
		addSolicitud(state, solicitud) {
			const solicitud_state = state.listSolicitudes.find(
				(solicitud_state) => solicitud_state.idSolicitud == solicitud.idSolicitud
			)
			if(!solicitud_state)
				state.listSolicitudes.push(solicitud);
		},
		updateSolicitud(state, solicitud) {
			const solicitud_state = state.listSolicitudes.find(function(
				solicitud_state
			) {
				return solicitud_state.idSolicitud == solicitud.idSolicitud;
			});
			if (solicitud_state) {
				if (solicitud_state.idSolicitud > state.idLimiteCambio) {
					solicitud_state.fechaInicioGasto = solicitud.fechaInicioGasto;
					solicitud_state.fechaFinGasto = solicitud.fechaFinGasto;
					solicitud_state.tipoMoneda = solicitud.tipoMoneda;
					solicitud_state.numeroPersonas = solicitud.numeroPersonas;
					solicitud_state.tarjeta = solicitud.tarjeta;
				}else {
					solicitud_state.unidad_negocio = solicitud.unidad_negocio;
					solicitud_state.fechaFinGastoEstimada = solicitud.fechaFinGastoEstimada;
					solicitud_state.fechaFinGastoReal = solicitud.fechaFinGastoReal;
				}
				solicitud_state.periodoInicioDispersion = solicitud.periodoInicioDispersion;
				solicitud_state.periodoFinDispersion = solicitud.periodoFinDispersion;
				solicitud_state.timbrar = solicitud.timbrar;
				solicitud_state.noNominaTimbrada = solicitud.noNominaTimbrada;
				solicitud_state.importeTimbrado = solicitud.importeTimbrado;
				solicitud_state.destinoGasto = solicitud.destinoGasto;
				solicitud_state.objetivoGasto = solicitud.objetivoGasto;
				solicitud_state.importe_total = solicitud.importe_total;
				solicitud_state.idEtapa = Number(solicitud.idEtapa);
				solicitud_state.idEtapaOld = Number(solicitud.idEtapaOld);
				solicitud_state.fechaCambioEtapa = solicitud.fechaCambioEtapa;
				solicitud_state.ruta = solicitud.ruta;
				solicitud_state.nombre_etapa = solicitud.nombre_etapa;
				solicitud_state.tipoCuenta = solicitud.tipoCuenta;
				solicitud_state.cuenta = solicitud.cuenta;
				solicitud_state.fileDispersion = solicitud.fileDispersion;
				solicitud_state.rutaFileDispersion = solicitud.rutaFileDispersion;
				solicitud_state.aceptacion = solicitud.aceptacion;
			}
		},
		updateSolicitudActiva(state, solicitud) {
			state.solicitudActiva.unidad_negocio = solicitud.unidad_negocio;
			state.solicitudActiva.fechaFinGastoEstimada = solicitud.fechaFinGastoEstimada;
			state.solicitudActiva.destinoGasto = solicitud.destinoGasto;
			state.solicitudActiva.objetivoGasto = solicitud.objetivoGasto;
			state.solicitudActiva.importe_total = solicitud.importe_total;
			state.solicitudActiva.idEtapa = Number(solicitud.idEtapa);
			state.solicitudActiva.idEtapaOld = Number(solicitud.idEtapaOld);
			state.solicitudActiva.fechaCambioEtapa = solicitud.fechaCambioEtapa;
			state.solicitudActiva.timbrar = solicitud.timbrar;
			state.solicitudActiva.noNominaTimbrada = solicitud.noNominaTimbrada;
			state.solicitudActiva.importeTimbrado = solicitud.importeTimbrado;
			state.solicitudActiva.updated_at = solicitud.updated_at
			state.solicitudActiva.nombre_etapa = solicitud.nombre_etapa;
			state.solicitudActiva.fechaFinGastoReal = solicitud.fechaFinGastoReal;
			state.solicitudActiva.tipoCuenta = solicitud.tipoCuenta;
			state.solicitudActiva.cuenta = solicitud.cuenta;
			state.solicitudActiva.fileDispersion = solicitud.fileDispersion;
			state.solicitudActiva.rutaFileDispersion = solicitud.rutaFileDispersion;
			state.solicitudActiva.periodoInicioDispersion = solicitud.periodoInicioDispersion;
			state.solicitudActiva.periodoFinDispersion = solicitud.periodoFinDispersion;
			state.solicitudActiva.fechaAutorizacionSolicitud = solicitud.fechaAutorizacionSolicitud;
			state.solicitudActiva.aceptacion = solicitud.aceptacion;
			
			if (state.solicitudActiva.idSolicitud > state.idLimiteCambio) {
				state.solicitudActiva.fechaInicioGasto = solicitud.fechaInicioGasto;
				state.solicitudActiva.fechaFinGasto = solicitud.fechaFinGasto;
				state.solicitudActiva.tipoMoneda = solicitud.tipoMoneda;
				state.solicitudActiva.numeroPersonas = solicitud.numeroPersonas;
				state.solicitudActiva.tarjeta = solicitud.tarjeta;
			}
		},
		deleteSolicitud(state, idSolicitud) {
			if(state.solicitudActiva == idSolicitud)
				state.solicitudActiva = null
			const index = state.listSolicitudes.findIndex(solicitud_state => {
				return solicitud_state.idSolicitud == idSolicitud;
			});
			state.listSolicitudes.splice(index, 1);
		},
		setResumenSolicitudActiva(state, resumen) {
			state.resumenSolicitudActiva = resumen;
		},
		setPlaneacionSolicitudActiva(state, planeacion) {
			state.planeacionSolicitudActiva = planeacion;
		},
		setMovimientos(state, movimientos) {
			state.movimientos = movimientos;
		},
		setComprobaciones(state, comprobaciones) {
			state.listComprobaciones = comprobaciones;
		},
		addComprobacion(state, comprobacion) {
			state.listComprobaciones.push(comprobacion);
		},
		updateComprobacion(state, comprobacion) {
			const comprobacion_state = state.listComprobaciones.find(function(
				comprobacion_state
			) {
				return comprobacion_state.idComprobacion == comprobacion.idComprobacion;
			});
			if (comprobacion_state) {
				comprobacion_state.fechaGasto = comprobacion.fechaGasto;
				comprobacion_state.tipoComprobante = comprobacion.tipoComprobante;
				comprobacion_state.folioComprobante = comprobacion.folioComprobante;
				comprobacion_state.proveedor = comprobacion.proveedor;
				comprobacion_state.descripcion = comprobacion.descripcion;
				comprobacion_state.noCuenta = comprobacion.noCuenta;
				comprobacion_state.tipoCuenta = comprobacion.tipoCuenta;
				comprobacion_state.cuenta = comprobacion.cuenta;
				comprobacion_state.tipoGasto = comprobacion.tipoGasto;
				comprobacion_state.subtotal = comprobacion.subtotal;
				comprobacion_state.iva = comprobacion.iva;
				comprobacion_state.otros = comprobacion.otros;
				comprobacion_state.importe = comprobacion.importe;
				comprobacion_state.file = comprobacion.file;
				comprobacion_state.ruta = comprobacion.ruta;
				comprobacion_state.fileXML = comprobacion.fileXML;
				comprobacion_state.rutaXML = comprobacion.rutaXML;
				comprobacion_state.revision = comprobacion.revision;
			}
		},
		changeBusyResumen(state, value) {
			state.busyResumen = value;
		},
		changeBusyMovimientos(state, value) {
			state.busyMovimientos = value;
		},
		changeBusyPlaneacion(state, value) {
			state.busyPlaneacion = value;
		},
		changeBusyComprobaciones(state, value) {
			state.busyComprobaciones = value;
		},
		deleteComprobacion(state, idComprobacion) {
			const index = state.listComprobaciones.findIndex(comprobacion_state => {
				return comprobacion_state.idComprobacion == idComprobacion;
			});
			state.listComprobaciones.splice(index, 1);
		},
		setEtapas(state, etapas) {
			state.listEtapas = etapas;
		},
		changeBusySolicitudes(state, value) {
			state.busySolicitudes = value;
		},
		setSolicitudesConsultadas(state, value) {
			state.solicitudesConsultadas = value
		},
	},
	actions: {
		getSolicitudes(context) {
			context.commit("changeBusySolicitudes", true);
			axios.get(BaseUrl + `/api/fn/cg/solicitudes`).then(response => {
				context.commit("setListSolicitudes", response.data);
				context.commit("changeBusySolicitudes", false);
				context.commit("setSolicitudesConsultadas", "todas");

			});
		},
		getSolicitudesByUsuario(context, idUsuario) {
			context.commit("changeBusySolicitudes", true);
			axios
				.get(BaseUrl + `/api/fn/cg/solicitudes/usuarios/${idUsuario}`)
				.then(response => {
					context.commit("setListSolicitudes", response.data);
					context.commit("changeBusySolicitudes", false);
					context.commit("setSolicitudesConsultadas", "byUsuario");

				});
		},
		getSolicitudesByAutorizador(context, idUsuario) {
			context.commit("changeBusySolicitudes", true);
			axios
				.get(BaseUrl + `/api/fn/cg/solicitudes/autorizador/${idUsuario}`)
				.then(response => {
					context.commit("setListSolicitudes", response.data);
					context.commit("changeBusySolicitudes", false);
					context.commit("setSolicitudesConsultadas", "byAutorizador");
				});
		},
		getResumenSolicitudActiva(context, idSolicitud) {
			context.commit("changeBusyResumen", true);
			axios
				.get(BaseUrl + `/api/fn/cg/solicitudes/resumen/${idSolicitud}`)
				.then(response => {
					context.commit("setResumenSolicitudActiva", response.data);
					context.commit("changeBusyResumen", false);
				});
		},
		getPlaneacionSolicitudActiva(context, idSolicitud) {
			context.commit("changeBusyPlaneacion", true);
			axios
				.get(BaseUrl + `/api/fn/cg/solicitudes/planeacion/gasto/${idSolicitud}`)
				.then(response => {
					context.commit("setPlaneacionSolicitudActiva", response.data);
					context.commit("changeBusyPlaneacion", false);
				});
		},
		getSolicitud(context, idSolicitud) {
			axios
				.get(BaseUrl + `/api/fn/cg/solicitud/${idSolicitud}`)
				.then(response => {
					context.commit("setSolicitudActiva", response.data);
				});
		},
		getMovimientosBySolicitud(context, idSolicitud) {
			context.commit("changeBusyMovimientos", true);
			axios
				.get(BaseUrl + `/api/fn/cg/movimientos/${idSolicitud}`)
				.then(response => {
					context.commit("setMovimientos", response.data);
					context.commit("changeBusyMovimientos", false);
				});
		},
		getComprobaciones(context) {
			context.commit("changeBusyComprobaciones", true);
			axios
				.get(
					BaseUrl +
						`/api/fn/cg/comprobaciones/${context.state.solicitudActiva.idSolicitud}`
				)
				.then(response => {
					context.commit("setComprobaciones", response.data);
					context.commit("changeBusyComprobaciones", false);
				});
		},
		getEtapas(context) {
			axios.get(BaseUrl + `/api/fn/cg/etapas`).then(response => {
				context.commit("setEtapas", response.data);
			});
		}
	},
	getters: {
		solicitudesFiltered(state) {
			var solicitudes = [];
			solicitudes = state.listSolicitudes;
			if (state.filterEtapa) {
				solicitudes = state.listSolicitudes.filter(
					solicitud => solicitud.idEtapa == state.filterEtapa
				);
			} 
			if (state.filterInicio && state.filterFin) {
				solicitudes = solicitudes.filter(
					solicitud =>
						solicitud.created_at > state.filterInicio &&
						solicitud.created_at < state.filterFin
				);
			}
			if (state.filterSearch) {
				solicitudes = solicitudes.filter(
					solicitud =>
						solicitud.usuario.nombreUsuario
							.toLowerCase()
							.indexOf(state.filterSearch.toLowerCase()) > -1 ||
						solicitud.idSolicitud == state.filterSearch
				);
			} 
			if(state.filterEtapa == 0) {
				solicitudes = solicitudes.filter(
					solicitud =>
						solicitud.idEtapa != 9
				);
			}
			return solicitudes.reverse();
		}
	}
};
