export default {
	namespaced: true,
	state: {
		listArchivos: [],
		busyArchivos: true,
	},
	mutations: {
		setArchivos(state, data) {
			state.listArchivos = data;
		},
		addArchivo(state, data) {
			state.listArchivos.push(data);
		},
		updateArchivo(state, archivo) {
			const archivo_state = state.listArchivos.find(function(
				archivo_state
			) {
				return archivo_state.idArchivo == archivo.idArchivo;
			});
			if (archivo_state) {
				archivo_state.etiquetas = archivo.etiquetas;
				archivo_state.file = archivo.file;
				archivo_state.ruta = archivo.ruta;
				archivo_state.color = archivo.color;
			}
		},
		removeArchivo(state, idArchivo) {
			const index = state.listArchivos.findIndex((item) => {
				return item.idArchivo == idArchivo;
			});
			state.listArchivos.splice(index, 1);
		},
		changeBusyArchivos(state, value) {
			state.busyArchivos = value;
		}
	},
	actions: {
		getArchivos(context, idSolicitud) {
			context.commit("changeBusyArchivos", true);
			axios.get(`/api/fn/cg/solicitudes/comprobantes/${idSolicitud}`).then((response) => { 
				context.commit("setArchivos", response.data);
				context.commit("changeBusyArchivos", false);

			});
		},
	},
};