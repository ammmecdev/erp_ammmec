export default {
    namespaced: true,
    state: {
        cfCuentasExternas: [],
    },
    mutations: {
        setConfCuentasExternas(state, cfCuentasExternas) {
             state.cfCuentasExternas = cfCuentasExternas
        },
        addConfCuentaExterna(state, cfCuentaExterna) {
            state.cfCuentasExternas.push(cfCuentaExterna)
        },
        updateConfCuentaExterna(state, cfCuentaExterna) {
            const cuentaExterna = state.cfCuentasExternas.find((cuentaExterna) => {
                return cuentaExterna.id == cfCuentaExterna.id;
            });
            cuentaExterna.fn_cg_etapas = cfCuentaExterna.fn_cg_etapas;
            cuentaExterna.op_ra_etapas = cfCuentaExterna.op_ra_etapas;
            cuentaExterna.se_sa_etapas = cfCuentaExterna.se_sa_etapas;
            cuentaExterna.co_ss_etapas = cfCuentaExterna.co_ss_etapas;
            cuentaExterna.co_ss_etapas = cfCuentaExterna.fn_cg_sv_etapas;
        },
    },
    actions: {
        getConfCuentasExternas(context, idEmbudo) {
            axios.get(BaseUrl + `/api/cf-cuentasExternas`).then((response) => {
                context.commit('setConfCuentasExternas', response.data)
            });
        },
    },
    getters: {

    }
}