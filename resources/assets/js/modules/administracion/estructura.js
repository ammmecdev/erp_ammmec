export default {
    namespaced: true,
    state: {
        cuentas: [],
        isBusy: true,
        modulos: [],
        secciones: [],
        submodulos: [],
        load: false,
        success: false,
        moduloActive: null,
    },
    mutations: {
        setModuloActive(state, modulo) {
            state.moduloActive = modulo
        },
        setModulos(state, modulos) {
            state.modulos = modulos
        },
        addModulo(state, modulo) {
            state.modulos.push(modulo)
        },
        setSecciones(state, secciones) {
            state.secciones = secciones
        },
        setSubmodulos(state, submodulos) {
            state.submodulos = submodulos
        },
        addSubmodulo(state, submodulo) {
            state.submodulos.push(submodulo)
        },
        updateSubmodulo(state, submodulo) {
            const submodulo_state = state.submodulos.find(
                (submodulo_state) => submodulo_state.idSubmodulo == submodulo.idSubmodulo
            )
            if(submodulo_state) {
                submodulo_state.nombre = submodulo.nombre
                submodulo_state.idSubmodulo = submodulo.idSubmodulo
            }
        },
        addSeccion(state, seccion) {
            state.secciones.push(seccion)
        },
        updateSeccion(state, seccion) {
            const seccion_state = state.secciones.find(
                (seccion_state) => seccion_state.idSeccion == seccion.idSeccionOld
            )
            seccion_state.nombre = seccion.nombre
            seccion_state.idSeccion = seccion.idSeccion 
        },
        deleteSeccion(state, seccion) {
            const index = state.secciones.findIndex(
                (seccion_state) => seccion_state.idSeccion == seccion.idSeccion
            )
            state.secciones.splice(index, 1);
        },
        addFuncion(state, funcion) {
            const seccion_state = state.secciones.find(
                (seccion_state) => seccion_state.idSeccion == funcion.idSeccion
            )
            if(seccion_state){
                seccion_state.funciones.push(funcion)
            }
        },
        updateFuncion(state, funcion) {
            const seccion_state = state.secciones.find(
                (seccion_state) => seccion_state.idSeccion == funcion.idSeccion
            )
            if (seccion_state) {
                const funcion_state = seccion_state.funciones.find(
                    (funcion_state) => funcion_state.idFuncion == funcion.idFuncionOld
                )
                if(funcion_state){
                    funcion_state.idFuncion = funcion.idFuncion
                    funcion_state.nombre = funcion.nombre
                    funcion_state.descripcion = funcion.descripcion
                }
            }
        },
        deleteFuncion(state, funcion) {
            const seccion_state = state.secciones.find(
                (seccion_state) => seccion_state.idSeccion == funcion.idSeccion
            )
            if (seccion_state) {
                const index = seccion_state.funciones.findIndex((funcion_state) => {
                    return funcion_state.idFuncion == funcion.idFuncion;
                });
                seccion_state.funciones.splice(index, 1);
            }
        },
        setIsBusy(state, value) {
            state.isBusy = value
        },
        setLoad(state, value) {
            state.load = value
        },
        setSuccess(state, value) {
            state.success = value
        },
    },
    actions: {
        getModulos(context) {
            context.commit('setIsBusy', true)
            axios.get(BaseUrl + `/api/modulos`).then((response) => {
                context.commit('setModulos', response.data)
                context.commit('setIsBusy', false)
            });
        },
        getSecciones(context, idModulo) {
            context.commit('setIsBusy', true)
            axios.get(BaseUrl + `/api/secciones/${idModulo}`).then(
                (response) => {
                    context.commit('setSecciones', response.data)
                    context.commit('setIsBusy', false)
                })
        },
        getSubmodulos(context, idModulo) {
            context.commit('setIsBusy', true)
            axios.get(BaseUrl + `/api/submodulos/${idModulo}`).then(
                (response) => {
                    context.commit('setSubmodulos', response.data)
                    context.commit('setIsBusy', false)
                })
        },
    },
    getters: {

    }
}