export default {
    namespaced: true,
    state: {
        isBusy: true,
        roles: [],
        load: false,
        success: false,
        permisos: []
     },
    mutations: {
        setPermisos(state, permisos) {
            state.permisos = permisos
        },
        setRoles(state, roles) {
            state.roles = roles
        },
        addRole(state, role) {
            state.roles.push(role)
        },
        updateRole(state, role) {
            const role_state = state.roles.find(
                (role_state) => rol_state.idRole == rol.idRoleOld
            )
            role_state.nombre = role.nombre
            role_state.idRole = role.idRole
        },
        deleteRole(state, role) {
            const index = state.roles.findIndex(
                (role_state) => role_state.idRole == role.idRole
            )
            state.roles.splice(index, 1);
        },
        addPermiso(state, data) {  
            state.permisos.push(data)
        },
        deletePermiso(state, data) {
            const index = state.permisos.findIndex(
                 (permiso) => permiso.idFuncion == data.idFuncion && permiso.idRole == data.idRole 
                )
            state.permisos.splice(index, 1); 
        },
        setIsBusy(state, value) {
            state.isBusy = value
        },
        setLoad(state, value) {
            state.load = value
        },
        setSuccess(state, value) {
            state.success = value
        },
        addPermisosOfSeccion(state, data) {
            for (let i = 0; i < data.seccion.funciones.length; i++) {
                const funcion = state.permisos.find(
                    (permiso) => permiso.idFuncion == data.seccion.funciones[i].idFuncion
                    )
                if(!funcion) {
                    const fun = {
                        idFuncion: data.seccion.funciones[i].idFuncion,
                        idRole: data.idRole,
                        idSeccion: data.seccion.idSeccion,
                    }
                    state.permisos.push(fun)
                }
            }
        },
        deletePermisosOfSeccion(state, data) {
            for (let i = 0; i < data.seccion.funciones.length; i++) {
                const index = state.permisos.findIndex(
                    (permiso) => permiso.idFuncion == data.seccion.funciones[i].idFuncion
                )
                state.permisos.splice(index, 1); 
            }
        },
    },
    actions: {
        getRoles(context, idModulo) {
            context.commit('setIsBusy', true)
            axios.get(BaseUrl + `/api/roles/${idModulo}`).then(
                (response) => {
                    context.commit('setRoles', response.data)
                    context.commit('setIsBusy', false)
                })
        },
        getPermisosByRole(context, idRole) {
            context.commit('setIsBusy', true)
            axios.get(BaseUrl + `/api/permisos-roles/${idRole}`).then(
                (response) => {
                    context.commit('setPermisos', response.data)
                    context.commit('setIsBusy', false)
                })
        }
    },
    getters: {

    }
}