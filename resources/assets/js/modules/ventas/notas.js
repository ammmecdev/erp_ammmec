export default {
	namespaced: true,
	state: {
		notas: [],
		infoNota: [],
		comentarios: [],
		actualizaciones: [],
		filter: "",
		idNota: null,
		value: false,
		dataNota: {
			idNota: null,
			titulo: "",
			idCliente: null,
			idContacto: null,
			idUsuario: null,
		},
		loading: null,
	},
	mutations: {
		addNota(state, nota) {
			state.notas.push(nota);
		},
		setDataNota(state, data) {
			state.dataNota.idNota = data.idNota;
			state.dataNota.titulo = data.titulo;
			state.dataNota.idCliente = data.idCliente;
			state.dataNota.idContacto = data.idContacto;
			state.dataNota.idUsuario = data.idUsuario;
		},
		addComentarios(state, comentario) {
			state.comentarios.push(comentario);
		},
		setNotasLead(state, nota) {
			state.notas = nota;
		},
		setActualizaciones(state, actualizacion) {
			state.actualizaciones = actualizacion;
		},
		setInfoNota(state, infoNota) {
			state.infoNota = infoNota;
		},
		setIdNota(state, idNota) {
			state.idNota = idNota;
		},
		setFilter(state, filter) {
			state.filter = filter;
		},
		setValue(state, value) {
			state.value = value;
		},
		setComentarios(state, comentario) {
			state.comentarios = comentario;
		},
		updateComentario(state, comentario) {
			const comentario_state = state.comentarios.find(function(
				comentario_state
			) {
				return comentario_state.idComentario == comentario.idComentario;
			});
			comentario_state.contenido = comentario.contenido;
			comentario_state.tituloNegocio = comentario.tituloNegocio;
			comentario_state.responsable = comentario.responsable;
			comentario_state.lead = comentario.lead;
			comentario_state.idUsuario = comentario.idUsuario;
			comentario_state.idNota = comentario.idNota;
			comentario_state.negocio = comentario.negocio;
			comentario_state.fechaActualizado = comentario.fechaActualizado;
		},
		updateNota(state, nota) {
			const nota_state = state.notas.find(function(nota_state) {
				return nota_state.idNota == nota.idNota;
			});
			nota_state.titulo = nota.titulo;
			nota_state.idCliente = nota.idCliente;
			nota_state.idContacto = nota.idContacto;
			nota_state.idUsuario = nota.idUsuario;
			nota_state.fechaActualizado = nota.fechaActualizado;
		},
		delElementComentario(state, id) {
			const index = state.comentarios.findIndex((comentario) => {
				return comentario.idComentario == id;
			});
			state.comentarios.splice(index, 1);
		},
		delElementNota(state, id) {
			const index = state.notas.findIndex((nota) => {
				return nota.idNota == id;
			});
			state.notas.splice(index, 1);
		},
		setLoading(state, val) {
			state.loading = val;
		},
	},
	actions: {
		getNotasLead(context) {
			axios
				.get(BaseUrl + "/api/vt/notas")
				.then((response) =>
					context.commit("setNotasLead", response.data)
				);
		},

		getInfoNota(context) {
			axios
				.get(BaseUrl + `/api/vt/notas/${context.state.idNota}`)
				.then((response) => {
					context.commit("setInfoNota", response.data);
				});
		},

		getComentarios(context) {
			context.commit("setLoading", true);
			axios
				.get(BaseUrl + `/api/vt/comentarios/${context.state.idNota}`)
				.then((response) => {
					context.commit("setLoading", false);
					context.commit("setComentarios", response.data);
				});
		},
	},
	getters: {
		filtrarNotas(state) {
			return state.notas.filter((oportunidad) =>
				notas.titulo.includes(state.filter)
			);
		},

		getIdNota(state) {
			return state.idNota;
		},
	},
};
