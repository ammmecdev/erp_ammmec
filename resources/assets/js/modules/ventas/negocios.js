import store from "../../store";
export default {
	namespaced: true,
	state: {
		negocios: [],
		negociosFacturados: [],
		negociosFacturados2: [],
		selectNegocios: [],
		selectEstimaciones: [],
		negociosNotLeads: [],
		negocios_actividades: [],
		embudos: [],
		etapas: [],
		etapaSelected: null,
		unidadSelected: null,
		selectEtapas: [],
		statusFilter: 0,
		embudoSelected: {
			idEmbudo: 8,
			nombre: 'AMMMEC 2024',
			periodo: 2024
		},
		causas: [],
		negocioSelected: null,
		negocio: null,
		setedClave: {
			seted: null,
			idNegocio: null,
		},
		detectarCambio: 0,
		dataNegocioLeads: {
			tituloNegocio: null,
			idCliente: 0,
			cliente: null,
			claveCliente: null,
			idContacto: 0,
			contacto: null,
			idComentario: 0,
			idUsuarioComentario: null,
		},
		filterEmbudos: "",
		bitacoras: [],
		cambiosNegocios: 0,
	},
	mutations: {
		setNegocioSelected(state, idNegocio) {
			state.negocioSelected = idNegocio;
		},
		setNegocio(state, negocio) {
			state.negocio = negocio;
		},
		setFilterEmbudos(state, filter) {
			state.filterEmbudos = filter;
		},
		setNegocioLeads(state, data) {
			state.dataNegocioLeads.tituloNegocio = data.tituloNegocio;
			state.dataNegocioLeads.idCliente = data.idCliente;
			state.dataNegocioLeads.cliente = data.cliente;
			state.dataNegocioLeads.claveCliente = data.claveCliente;
			state.dataNegocioLeads.idContacto = data.idContacto;
			state.dataNegocioLeads.contacto = data.contacto;
			state.dataNegocioLeads.idComentario = data.idComentario;
			state.dataNegocioLeads.idUsuarioComentario =
				data.idUsuarioComentario;
		},
		addNegocio(state, negocio) {
			state.negocios.push(negocio);
			if (state.negocios_actividades.length > 0)
				state.negocios_actividades.push(negocio);
			state.totalRows = state.negocios.length;
		},
		updateClaveNegocio(state, negocio) {
			const negocio_state = state.negocios.find(function(negocio_state) {
				return negocio_state.idNegocio == negocio.idNegocio;
			});

			const neg_act_state = state.negocios_actividades.find(function(
				neg_act_state
			) {
				return neg_act_state.idNegocio == negocio.idNegocio;
			});

			if (negocio_state) {
				negocio_state.claveConsecutivo = negocio.claveConsecutivo;
				negocio_state.claveServicio = negocio.claveServicio;
				negocio_state.idUnidadNegocio = negocio.idUnidadNegocio;
				negocio_state.idEquipo = negocio.idEquipo;
				negocio_state.claveNegocio = negocio.claveNegocio;
				negocio_state.claveNegocioCompleta =
					negocio.claveNegocioCompleta;
				negocio_state.idServicio = negocio.idServicio;
				negocio_state.claveCliente = negocio.claveCliente;
				negocio_state.idCliente = negocio.idCliente;
				negocio_state.idContacto = negocio.idContacto;
				negocio_state.nombre_cliente = negocio.nombre_cliente;
				negocio_state.nombre_contacto = negocio.nombre_contacto;
				negocio_state.clave_unidad_negocio =
					negocio.clave_unidad_negocio;
				negocio_state.nombre_equipo = negocio.nombre_equipo;
			}

			if (neg_act_state) {
				neg_act_state.claveConsecutivo = negocio.claveConsecutivo;
				neg_act_state.claveServicio = negocio.claveServicio;
				neg_act_state.idUnidadNegocio = negocio.idUnidadNegocio;
				neg_act_state.idEquipo = negocio.idEquipo;
				neg_act_state.claveNegocio = negocio.claveNegocio;
				neg_act_state.claveNegocioCompleta =
					negocio.claveNegocioCompleta;
				neg_act_state.idServicio = negocio.idServicio;
				neg_act_state.claveCliente = negocio.claveCliente;
				neg_act_state.idCliente = negocio.idCliente;
				neg_act_state.idContacto = negocio.idContacto;
				neg_act_state.nombre_cliente = negocio.nombre_cliente;
				neg_act_state.nombre_contacto = negocio.nombre_contacto;
				neg_act_state.clave_unidad_negocio =
					negocio.clave_unidad_negocio;
				neg_act_state.nombre_equipo = negocio.nombre_equipo;
			}
		},
		setNegocios(state, negocios) {
			state.negocios = negocios;
			state.selectNegocios = negocios.map((negocio) => ({
				text: negocio.claveNegocio + " - " + negocio.tituloNegocio,
				value: negocio.idNegocio,
			}));

			const estimaciones = negocios.filter(
				(estimacion) => estimacion.estimacion != 0
			);

			state.selectEstimaciones = estimaciones.map((negocio) => ({
				text: negocio.claveNegocio + " - " + negocio.tituloNegocio,
				value: negocio.idNegocio,
			}));
		},
		setNegociosLeads(state, negocios) {
			state.negociosNotLeads = negocios.filter(
				(negocio) => negocio.claveNegocio != "Leads"
			).map((negocio) => {
				return {
					idNegocio: negocio.idNegocio,
					claveNegocioCompleta: negocio.claveNegocioCompleta,
					tituloNegocio: negocio.tituloNegocio
				}
			});
		},
		setNegociosActividades(state, negocios) {
			state.negocios_actividades = negocios;
		},
		setEtapas(state, etapas) {
			state.etapas = etapas;
			state.selectEtapas = etapas.map((etapa) => ({
				text: etapa.nombreEtapa,
				value: {
					idEtapa: etapa.idEtapa,
					nombreEtapa: etapa.nombreEtapa,
				},
			}));
		},
		setEtapaSelected(state, etapaSelected) {
			state.etapaSelected = etapaSelected;
		},
		setUnidadSelected(state, unidadSelected) {
			state.unidadSelected = unidadSelected;
		},
		setEmbudos(state, embudos) {
			state.embudos = embudos;
		},
		setStatusFilter(state, statusFilter) {
			state.statusFilter = statusFilter;
		},
		setEmbudoSelected(state, embudo) {
			state.embudoSelected = embudo;
		},
		setCausas(state, causas) {
			state.causas = causas;
		},
		updateNegocio(state, negocio) {
			const negocio_state = state.negocios.find(function(negocio_state) {
				return negocio_state.idNegocio == negocio.idNegocio;
			});

			if (state.negocios_actividades.length > 0) {
				const neg_act_state = state.negocios_actividades.find(function(
					neg_act_state
				) {
					return neg_act_state.idNegocio == negocio.idNegocio;
				});

				neg_act_state.valorNegocio = negocio.valorNegocio;
				neg_act_state.tituloNegocio = negocio.tituloNegocio;
				neg_act_state.idEtapa = negocio.idEtapa;
				neg_act_state.xCambio = negocio.xCambio;
				neg_act_state.status = negocio.status;
				neg_act_state.fechaGanadoPerdido = negocio.fechaGanadoPerdido;
				neg_act_state.idCausa = negocio.idCausa;
				neg_act_state.idEmbudo = negocio.idEmbudo;
			}

			if (negocio_state) {
				negocio_state.fechaEntrega = negocio.fechaEntrega;
				negocio_state.fechaReporte = negocio.fechaReporte;
				negocio_state.valorNegocio = negocio.valorNegocio;
				negocio_state.ponderacion = negocio.ponderacion;
				negocio_state.fechaGanadoPerdido = negocio.fechaGanadoPerdido;
				negocio_state.status = negocio.status;
				negocio_state.rfq = negocio.rfq;
				negocio_state.fechaRfq = negocio.fechaRfq;
				negocio_state.tituloNegocio = negocio.tituloNegocio;
				negocio_state.idEtapa = negocio.idEtapa;
				negocio_state.idEmbudo = negocio.idEmbudo;
				negocio_state.idCausa = negocio.idCausa;
				negocio_state.pedido = negocio.pedido;
				negocio_state.fechaVencimientoPedido =
					negocio.fechaVencimientoPedido;
				negocio_state.idContacto = negocio.idContacto;
				negocio_state.nombre_contacto = negocio.nombre_contacto;
				negocio_state.nombre_etapa = negocio.nombre_etapa;
				negocio_state.xCambio = negocio.xCambio;
				negocio_state.tipoMoneda = negocio.tipoMoneda;
			}
			if (state.negocio) {
				if (state.negocio.idNegocio === negocio.idNegocio)
					state.negocio.status = negocio.status;
			}
			state.cambiosNegocios++;
		},
		removeNegocio(state, index) {
			state.negocios.splice(index, 1);
		},
		updateCosto(state, costo) {
			const negocio_state = state.negocios.find(function(negocio_state) {
				return negocio_state.idNegocio == costo.idNegocio;
			});
			negocio_state.fecha_solicitud = costo.fechaSolicitud;
			negocio_state.fecha_entrega = costo.fechaEntrega;
			negocio_state.monto_total = costo.montoTotal;
		},
		updateCotizacion(state, cotizacion) {
			const negocio_state = state.negocios.find(function(negocio_state) {
				return negocio_state.idNegocio == cotizacion.idNegocio;
			});
			negocio_state.fecha_cotizacion = cotizacion.fechaCotizacion;
			negocio_state.clave_cotizacion = cotizacion.claveCotizacion;
		},
		updateFactura(state, factura) {
			const negocio_state = state.negocios.find(function(negocio_state) {
				return negocio_state.idNegocio == factura.idNegocio;
			});
			negocio_state.fecha_factura = factura.fechaFactura;
			negocio_state.no_factura = factura.noFactura;
		},
		updatePago(state, pago) {
			const negocio_state = state.negocios.find(function(negocio_state) {
				return negocio_state.idNegocio == pago.idNegocio;
			});
			negocio_state.fecha_pago = pago.fechaPago;
			negocio_state.pago_mxn_iva = pago.pagoMXNiva;
			negocio_state.pago_usd_iva = pago.pagoUSDiva;
			negocio_state.no_pago = pago.noPago;
			negocio_state.fecha_recibo = pago.fechaRecibo;
		},
		removeActividadCompletada(state, data) {
			const negocio = state.negocios_actividades.find(function(negocio) {
				return negocio.idNegocio == data.idNegocio;
			});

			if (negocio) {
				const index = negocio.actividades.findIndex((actividad) => {
					return actividad.idActividad == data.idActividad;
				});

				negocio.actividades.splice(index, 1);
			}
			state.cambiosNegocios++;
		},
		addActividad(state, data) {
			const negocio_state = state.negocios_actividades.find(function(
				negocio_state
			) {
				return negocio_state.idNegocio == data.idNegocio;
			});
			if (negocio_state) negocio_state.actividades.push(data.actividad);

			state.cambiosNegocios++;
		},
		setSetedClave(state, value) {
			state.setedClave.seted = value.seted;
			state.setedClave.idNegocio = value.idNegocio;
		},
		setNegociosFacturados(state, negocios) {
			state.negociosFacturados = negocios;
		},
		setNegociosFacturados2(state, negocios) {
			state.negociosFacturados2 = negocios;
		},
		setBitacoras(state, bitacoras) {
			state.bitacoras = bitacoras;
		},
		incrementCambiosNegocios(state) {
			state.cambiosNegocios++;
		},
		sumaImporteMXN() {

		},
		sumaImporteUSD() {

		},
		sumaPrecioNeto() {

		}
	},
	actions: {
		getNegocio(context, idNegocio) {
			axios
				.get(BaseUrl + `/api/vt/negocio/${idNegocio}`)
				.then((response) => {
					context.commit("setNegocio", response.data);
				});
		},
		getNegocios(context) {
			axios
				.get(
					BaseUrl +
						`/api/vt/negocios/join?idEmbudo=${context.state.embudoSelected.idEmbudo}`
				)
				.then((response) => {
					context.commit("setNegocios", response.data);
					context.commit("setNegociosLeads", response.data);
				});
		},
		getNegociosActividades(context) {
			axios
				.get(
					BaseUrl +
						`/api/vt/negocios/actividades?idEmbudo=${context.state.embudoSelected.idEmbudo}`
				)
				.then((response) => {
					context.commit("setNegociosActividades", response.data);
				});
		},
		getEmbudos(context) {
			axios
				.get(BaseUrl + "/api/vt/embudos/select")
				.then((response) =>
					context.commit("setEmbudos", response.data)
				);
		},
		getEtapas(context) {
			axios
				.get(
					BaseUrl +
						`/api/vt/embudos/${context.state.embudoSelected.idEmbudo}/etapas`
				)
				.then((response) => {
					context.commit("setEtapas", response.data);
				});
		},
		getCausas(context) {
			axios
				.get(BaseUrl + "/api/vt/causas/select")
				.then((response) => context.commit("setCausas", response.data));
		},
		getNegociosFacturados(context) {
			axios
				.get(
					BaseUrl +
						`/api/vt/negocios/facturados/negocios?idEmbudo=${context.state.embudoSelected.idEmbudo}`
				)
				.then((response) => {
					context.commit("setNegociosFacturados", response.data);
				});
		},
		//Este metodo es igual que el de arriba, pero se utiliza para comparar estos negocios
		getNegociosFacturados2(context, idEmbudo) {
			axios
				.get(
					BaseUrl +
						`/api/vt/negocios/facturados/negocios?idEmbudo=${idEmbudo}`
				)
				.then((response) => {
					context.commit("setNegociosFacturados2", response.data);
				});
		},
		getBitacoras(context, idNegocio) {
			axios
				.get(BaseUrl + `/api/vt/negocio/${idNegocio}/historial`)
				.then((response) => {
					context.commit("setBitacoras", response.data);
				});
		},
	},
	getters: {
		filteredNegociosByStatus(state) {
			var negocios = state.negocios;

			if (state.unidadSelected) {
				negocios = negocios.filter(negocio =>
					negocio.idUnidadNegocio == state.unidadSelected.idUnidadNegocio
				)
			}

			if (state.statusFilter != 3) {
				negocios = negocios.filter(
					(negocio) => negocio.status == state.statusFilter
				);
			}

			return negocios;
		},
		filteredNegociosEmbudos(state) {
			var negocios = [];
			if (state.statusFilter != 3) {
				negocios = state.negocios_actividades.filter(
					(negocio) => negocio.status == state.statusFilter
				);
			} else {
				negocios = state.negocios_actividades;
			}
			if (state.unidadSelected) {
				negocios = negocios.filter(negocio =>
					negocio.idUnidadNegocio == state.unidadSelected.idUnidadNegocio
				)
			}
			if (state.filterEmbudos) {
				return negocios.filter(
					(negocio) =>
						negocio.claveNegocio
							.toLowerCase()
							.indexOf(state.filterEmbudos.toLowerCase()) > -1 ||
						negocio.valorNegocio.includes(state.filterEmbudos) ||
						negocio.nombre_cliente
							.toLowerCase()
							.indexOf(state.filterEmbudos.toLowerCase()) > -1 ||
						negocio.tituloNegocio
							.toLowerCase()
							.indexOf(state.filterEmbudos.toLowerCase()) > -1
				);
			} else {
				return negocios;
			}
		},
		bitacorasReverse(state) {
			return state.bitacoras.reverse();
		},
	},
};
