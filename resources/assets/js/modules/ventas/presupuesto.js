export default{
	namespaced: true,
	state:{
        meses: [
            {id: 1, nombre: "Enero"},
            {id: 2, nombre: "Febrero"},
            {id: 3, nombre: "Marzo"},
            {id: 4, nombre: "Abril"},
            {id: 5, nombre: "Mayo"},
            {id: 6, nombre: "Junio"},
            {id: 7, nombre: "Julio"},
            {id: 8, nombre: "Agosto"},
            {id: 9, nombre: "Septiembre"},
            {id: 10, nombre: "Octubre"},
            {id: 11, nombre: "Noviembre"},
            {id: 12, nombre: "Diciembre"},
        ],
        presupuestosMeses: [],
        presupuestos: [],
        estimaciones:[],
		dataTotalMes:{
			idPresupuesto: null,
            idNegocio: null,
			claveNegocio: '',
			totalMensual: null,
			idMes: null,
			idPreMes: null,
            idNegocioMensual: null,
            facturado: null,
            foundNegocio: null
		},
        dataDetailsPresupuesto: {
            idPresupuesto: 0,
            totalAnual: 0,
            idNegocio: 0,
            claveNegocio: null,
            tituloNegocio: null,
            facturado: null,
            foundNegocio: null,
        },
        // presupuestosMesesByEmbudo: [],
        // monto_meses: [],
	},
	mutations:{
        setPresupuestoEdit(state, data) {
            state.dataTotalMes.idPresupuesto = data.idPresupuesto
            state.dataTotalMes.idNegocio = data.idNegocio
			state.dataTotalMes.claveNegocio = data.claveNegocio
			state.dataTotalMes.totalMensual = data.totalMensual
			state.dataTotalMes.idMes = data.idMes
			state.dataTotalMes.idPreMes = data.idPreMes
            state.dataTotalMes.idNegocioMensual = data.idNegocioMensual
            state.dataTotalMes.fecha_factura = data.fecha_factura
            state.dataTotalMes.facturado = data.facturado
            state.dataTotalMes.foundNegocio = data.foundNegocio
        },
        setDetailsPresupuesto(state, data) {
            state.dataDetailsPresupuesto.idPresupuesto = data.idPresupuesto
            state.dataDetailsPresupuesto.totalAnual = data.totalAnual
            state.dataDetailsPresupuesto.idNegocio = data.idNegocio
            state.dataDetailsPresupuesto.claveNegocio = data.claveNegocio
            state.dataDetailsPresupuesto.tituloNegocio = data.tituloNegocio
            state.dataDetailsPresupuesto.facturado = data.facturado
            state.dataDetailsPresupuesto.foundNegocio = data.foundNegocio
        },
        setPresupuestos(state, presupuestos) {
            state.presupuestos = presupuestos
        },
        setPresupuestosMeses(state, presupuestosMeses) {
            state.presupuestosMeses = presupuestosMeses
        },
        setEstimacion(state, estimaciones) {
            state.estimaciones = []
            state.estimaciones = estimaciones
        },
        addPresupuestosMeses(state, presupuestoMes) {
            state.presupuestosMeses.push(presupuestoMes)
        },
        addPresupuesto(state, presupuesto) {
            state.presupuestos.push(presupuesto)
        },
        updatePresupuestosMeses(state, presupuestoMes) {
            const presupuestoMes_state = state.presupuestosMeses.find(function(presupuestoMes_state){
                    return presupuestoMes_state.idPreMes == presupuestoMes.idPreMes
            })
            presupuestoMes_state.idNegocioMensual = presupuestoMes.idNegocioMensual
            presupuestoMes_state.totalMensual = presupuestoMes.totalMensual
        },
        removePresupuestoMes(state, id) {
            const index = state.presupuestosMeses.findIndex((pre_mes) => {
                    return pre_mes.idPreMes == id;
            });
            state.presupuestosMeses.splice(index, 1);
        },
        removePresupuestosMeses(state, id) {
            state.presupuestosMeses.filter(function(pre_mes, index) {
                if (pre_mes.idPresupuesto == id) {
                    state.presupuestosMeses.splice(index, 1);
                }
            });           
        },
        removePresupuesto(state, id) {
            const index = state.presupuestos.findIndex((presupuesto) => {
                    return presupuesto.idPresupuesto == id;
            });
            state.presupuestos.splice(index, 1);
        },


        // setMontoMeses(state, montoMeses)
        // {
        // 	state.monto_meses = montoMeses
        // },
        // setPresupuestosMesesByEmbudo(state, data)
        // {
        // 	state.presupuestosMesesByEmbudo = data
        // },
        // updateTotalAnual(state, data) {
        //     const pres_mes = data.presupuestos_meses
        //     const presupuesto_state = state.presupuestos.find(function(presupuesto_state){
        //             return presupuesto_state.idPresupuesto == pres_mes.idPresupuesto
        //     })
        //     var totalAnual = 0
        //     if (isNaN(data.old_totalMensual)){
        //         totalAnual = parseFloat(presupuesto_state.totalAnual) + parseFloat(pres_mes.totalMensual)
        //         presupuesto_state.totalAnual = totalAnual.toFixed(2)
        //     }else{
        //         totalAnual = (parseFloat(presupuesto_state.totalAnual) - parseFloat(data.old_totalMensual)) + parseFloat(pres_mes.totalMensual)
        //         presupuesto_state.totalAnual = totalAnual.toFixed(2)
        //     }
        // },
        // updateTotalMensual(state, data) {
        //     const pres_mes = data.presupuestos_meses
        //     const presupuesto_state = state.presupuestos.find(function(presupuesto_state){
        //             return presupuesto_state.idPresupuesto == pres_mes.idPresupuesto
        //     })
        //     const montoMeses_state = state.monto_meses.find(function(montoMeses_state){
        //         return montoMeses_state.idEmbudo == presupuesto_state.idEmbudo && montoMeses_state.idMes == pres_mes.idMes
        //     })
        //     var totalAnual = 0
        //     if (isNaN(data.old_totalMensual)){
        //         totalAnual = parseFloat(montoMeses_state.monto) + parseFloat(pres_mes.totalMensual)
        //         montoMeses_state.monto = totalAnual.toFixed(2)
        //     }else{
        //         totalAnual = (parseFloat(montoMeses_state.monto) - parseFloat(data.old_totalMensual)) + parseFloat(pres_mes.totalMensual)
        //         montoMeses_state.monto = totalAnual.toFixed(2)
        //     }
        // },
        // subtractTotalAnualMensual(state, pre_mes) {
        //     const presupuesto_state = state.presupuestos.find(function(presupuesto_state){
        //             return presupuesto_state.idPresupuesto == pre_mes.idPresupuesto
        //     })

        //     presupuesto_state.totalAnual = presupuesto_state.totalAnual - pre_mes.totalMensual

        //     const montoMeses_state = state.monto_meses.find(function(montoMeses_state){
        //         return montoMeses_state.idEmbudo == presupuesto_state.idEmbudo && montoMeses_state.idMes == pre_mes.idMes
        //     })

        //     montoMeses_state.monto = montoMeses_state.monto - pre_mes.totalMensual
        // }
	},
	actions:{
        getPresupuestosMeses(context, idEmbudo)
        {
        	axios.get(BaseUrl + `/api/vt/presupuesto/embudo/${idEmbudo}/meses`)
            .then(response => {
                context.commit('setPresupuestosMeses', response.data) 
            });
        },
        getPresupuestos(context, idEmbudo)
        {
            axios.get(BaseUrl + `/api/vt/presupuesto/join?idEmbudo=${idEmbudo}`)
            .then(response => {
                context.commit('setPresupuestos', response.data)
            });
        },
        getEstimaciones(context, idNegocio, idEmbudo)
        {
            axios.get(BaseUrl + `/api/vt/negocios/select/estimaciones?idNegocio=${idNegocio}`)
            .then(response => {
                context.commit('setEstimacion', response.data)
            });
        },

        // getPresupuestosMesesByEmbudo(context, idEmbudo)
        // {
        // 	axios.get(BaseUrl + `/api/vt/presupuesto/embudo/${idEmbudo}/meses`)
        //     .then(response => {
        //         context.commit('setPresupuestosMesesByEmbudo', response.data) 
        //     });
        // },
        // getMontoMeses(context, idEmbudo)
        // {
        // 	axios.get(BaseUrl + `/api/vt/presupuesto/montoMeses?idEmbudo=${idEmbudo}`)
        //     .then(response => {
        //         context.commit('setMontoMeses', response.data)
        //     });
        // },
	},
	getters:{
		
	}
}