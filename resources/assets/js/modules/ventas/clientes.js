export default {
	namespaced: true,
	state: {
		clientes: [],
		cliente: null,
		clienteSelected: null,
		selectContactos: [],
	},
	mutations: {
		setClientes(state, clientes) {
			state.clientes = clientes;
		},
		setClienteSelected(state, value) {
			state.clienteSelected = value;
		},
		setSelectContactos(state, value) {
			state.selectContactos = value;
		},
		addCliente(state, cliente) {
			state.clientes.push(cliente);
			state.totalRows = state.clientes.length;
		},
		updateCliente(state, cliente) {
			const cliente_state = state.clientes.find(function(cliente_state) {
				return cliente_state.idCliente == cliente.idCliente;
			});
			if (cliente_state) {
				cliente_state.nombre = cliente.nombre;
				cliente_state.idUsuario = cliente.idUsuario;
				cliente_state.direccion = cliente.direccion;
				cliente_state.paginaWeb = cliente.paginaWeb;
				cliente_state.telfono = cliente.telfono;
				cliente_state.clave = cliente.clave;
				cliente_state.sector = cliente.sector;
				cliente_state.localidadEstado = cliente.localidadEstado;
				cliente_state.extensionTelefonica = cliente.extensionTelefonica;
				cliente_state.tipoTelefono = cliente.tipoTelefono;
				cliente_state.rfc = cliente.rfc;
			}
		},
		delElementCliente(state, id) {
			const index = state.clientes.findIndex((cliente) => {
				return cliente.idCliente == id;
			});
			state.clientes.splice(index, 1);
		},
	},
	actions: {
		getClientes(context) {
			axios
				.get(BaseUrl + `/api/vt/clientes`)
				.then((response) =>
					context.commit("setClientes", response.data)
				);
		},
		getContactosByIdCliente(context) {
			axios
				.get(
					BaseUrl +
						`/api/vt/clientes/${context.state.clienteSelected}/contactos/select`
				)
				.then((response) =>
					context.commit("setSelectContactos", response.data)
				);
		},
	},
};
