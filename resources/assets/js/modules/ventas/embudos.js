export default {
	namespaced: true,
	state: {
		embudos: [],
		selectEmbudos: [],
	},
	mutations: {
		setEmbudos(state, embudos) {
			state.embudos = embudos;
		},
		setSelectEmbudos(state, embudos) {
			state.selectEmbudos = embudos;
		},
	},
	actions: {
		getEmbudos(context) {
			axios
				.get(BaseUrl + `/api/vt/embudos`)
				.then((response) =>
					context.commit("setEmbudos", response.data)
				);
		},
		getSelectEmbudos(context) {
			axios
				.get(BaseUrl + "/api/vt/embudos/select")
				.then((response) =>
					context.commit("setSelectEmbudos", response.data)
				);
		},
	},
	getters: {},
};
