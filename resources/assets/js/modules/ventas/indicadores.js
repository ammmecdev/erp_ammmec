export default {
	namespaced: true,
	state: {
		clientesFacturados: [],
		selectClientes: [],
		evaluaciones: [],
		preguntas: [],
		evaluaciones_preguntas: [],
		dataGraph: [],
		cuatrimestre: 3,
		evaluacion_preguntaLength: true,
	},
	mutations: {
		setEvaluacionPreguntaBoolean(state, estado) {
			state.evaluacion_preguntaLength = estado;
		},
		setPeriodoSelected(state, cuatrimestre) {
			state.cuatrimestre = cuatrimestre;
		},
		setClientesFacturados(state, clientesFacturados) {
			state.clientesFacturados = clientesFacturados;
		},
		addClienteFacturado(state, clienteFacturado) {
			state.clientesFacturados.push(clienteFacturado);
		},
		removeClienteFacturado(state, index) {
			state.clientesFacturados.splice(index, 1);
		},
		selectClientesFacturados(state, clientesFacturados) {
			state.selectClientes = [];
			const filterClientes = clientesFacturados.filter(
				(cliente) => cliente.facturado == true
			);
			filterClientes.forEach(function(cliente) {
				const cadena = {
					text: cliente.clave + ' - ' +cliente.nombre,
					value: { id: cliente.id, idCliente: cliente.idCliente },
				};
				state.selectClientes.push(cadena);
			});
		},
		setEvaluacion(state, evaluaciones) {
			state.evaluaciones = evaluaciones;
		},
		addEvaluacion(state, evaluacion) {
			state.evaluaciones.push(evaluacion);
		},
		updateEvaluacion(state, evaluacion) {
			const evaluacion_state = state.evaluaciones.find(function(
				evaluacion_state
			) {
				return evaluacion_state.idEvaluacion == evaluacion.idEvaluacion;
			});
			evaluacion_state.observaciones = evaluacion.observaciones;
		},
		setPregunta(state, preguntas) {
			state.preguntas = preguntas;
		},
		setEvaluacionPregunta(state, evaluacionesPreguntas) {
			state.evaluaciones_preguntas = evaluacionesPreguntas;
		},
		updateEvaluacionPregunta(state, evaluacionPregunta) {
			const evaluacionPregunta_state = state.evaluaciones_preguntas.find(
				function(evaluacionPregunta_state) {
					return evaluacionPregunta_state.id == evaluacionPregunta.id;
				}
			);
			evaluacionPregunta_state.valor = evaluacionPregunta.valor;
		},
		addEvaluacionPregunta(state, evaluacionPregunta) {
			state.evaluaciones_preguntas.push(evaluacionPregunta);
		},
		setDataGraph(state, data) {
			state.dataGraph = data;
		},
	},
	actions: {
		getClientesFacturados(context, idEmbudo) {
			axios
				.get(
					BaseUrl +
						`/api/vt/indicadores/clientesFacturados?idEmbudo=${idEmbudo}`
				)
				.then((response) => {
					context.commit("setClientesFacturados", response.data);
					context.commit("selectClientesFacturados", response.data);
				});
		},
		getEvaluaciones(context, idEmbudo) {
			axios
				.get(
					BaseUrl +
						`/api/vt/indicadores/evaluaciones?idEmbudo=${idEmbudo}&cuatrimestre=${context.state.cuatrimestre}`
				)
				.then((response) => {
					context.commit("setEvaluacion", response.data);
				});
		},
		getPreguntas(context) {
			axios
				.get(BaseUrl + `/api/vt/indicadores/preguntas`)
				.then((response) => {
					context.commit("setPregunta", response.data);
				});
		},
		getEvaluacionesPreguntas(context) {
			axios
				.get(BaseUrl + `/api/vt/indicadores/evaluaciones/preguntas`)
				.then((response) => {
					context.commit("setEvaluacionPregunta", response.data);
				});
		},
	},
	getters: {},
};
