import { mapState } from "vuex";

export default {
	namespaced: true,
	state: {
		contactos: [],
		contacto: null,
		clienteModalSelected: null,
	},
	mutations: {
		setContactos(state, contactos) {
			state.contactos = contactos;
		},
		setClienteModalSelected(state, clienteModalSelected) {
			state.clienteModalSelected = clienteModalSelected;
		},
		addContacto(state, contacto) {
			state.contactos.push(contacto);
		},
		updateContacto(state, contacto) {
			const contacto_state = state.contactos.find(function(
				contacto_state
			) {
				return contacto_state.idContacto == contacto.idContacto;
			});
			if (contacto_state) {
				contacto_state.nombre = contacto.nombre;
				contacto_state.telefono = contacto.telefono;
				contacto_state.extension = contacto.extension;
				contacto_state.tipoTelefono = contacto.tipoTelefono;
				contacto_state.email = contacto.email;
				contacto_state.honorifico = contacto.honorifico;
				contacto_state.puesto = contacto.puesto;
			}
		},
		delElementContacto(state, id) {
			const index = state.contactos.findIndex((contacto) => {
				return contacto.idContacto == id;
			});
			state.contactos.splice(index, 1);
		},
	},
	actions: {
		getContactos(context) {
			axios
				.get(BaseUrl + "/api/vt/contactos")
				.then((response) =>
					context.commit("setContactos", response.data)
				);
		},
	},
	computed: {
		...mapState(["formDelete"]),
	},
	getters: {
		filteredContactosByCliente(state) {
			if (state.clienteSelected != null) {
				return state.contactos.filter(
					(contacto) => contacto.idCliente == state.clienteSelected
				);
			}
			return state.contactos;
		},
	},
};
