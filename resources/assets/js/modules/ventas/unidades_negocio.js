export default{
	namespaced: true,
	state:{
		listUnidadesNegocio: [],
	},
	mutations:{
        addUnidad(state, data) {
            state.listUnidadesNegocio.push(data);
        },
        updateUnidad(state, data) {
            const unidad_state = state.listUnidadesNegocio.find(
                (unidad) => unidad.idUnidadNegocio == data.idUnidadNegocio
            );
            if(unidad_state) {
                unidad_state.claveUnidadNegocio = data.claveUnidadNegocio;
                unidad_state.unidadNegocio = data.unidadNegocio;
            }
        },
        setUnidadesNegocio(state, data) {
            state.listUnidadesNegocio = data
        },
	},
	actions:{
         getUnidadesNegocio(context) {
             axios.get(BaseUrl + `/api/vt/unidades-negocio`)
                .then(response => {
                    context.commit('setUnidadesNegocio', response.data)
            });
        }
	},
}