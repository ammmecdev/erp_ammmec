export default{
	namespaced: true,
	state:{
        listProyecciones: [],
        load: false,
        success: false,
	},
	mutations:{
        setProyecciones(state, data) {
            state.listProyecciones = data;
        },
        setLoad(state, value) {
            state.load = value
        },
        setSuccess(state, value) {
            state.success = value
        },
        saveProyeccion(state, proyeccion)
        {
            const proyeccion_state = state.listProyecciones.find(
                (proyeccion_state) => proyeccion_state.idProyeccion == proyeccion.idProyeccion
            )

            if(proyeccion_state)
                proyeccion_state.monto = proyeccion.monto
            else
                state.listProyecciones.push(proyeccion)

            state.load = false
            state.success = true
        },
	},
	actions:{
        getProyecciones(context)
        {
			axios.get(BaseUrl + `/api/vt/proyecciones`)
            .then(response => {
                context.commit('setProyecciones', response.data) 
            });
        },
	},
	getters:{
		
	}
}
