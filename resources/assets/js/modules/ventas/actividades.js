export default {
	namespaced: true,
	state: {
		actividades: [],
		actividad: null,
		clienteActividadSelected: null,
		cambiosActividades: 0,
	},
	mutations: {
		setActividades(state, actividades) {
			state.actividades = actividades;
		},
		setClienteActividadSelected(state, clienteActividadSelected) {
			state.clienteActividadSelected = clienteActividadSelected;
		},
		updateFechaCompletado(state, actividad) {
			const actividad_state = state.actividades.find(function(
				actividad_state
			) {
				return actividad_state.idActividad == actividad.idActividad;
			});
			actividad_state.fechaCompletado = actividad.fechaCompletado;
		},
		updateActividad(state, actividad, rootState, rootGetters) {
			const actividad_state = state.actividades.find(function(
				actividad_state
			) {
				return actividad_state.idActividad == actividad.idActividad;
			});

			actividad_state.tipo = actividad.tipo;
			actividad_state.notas = actividad.notas;
			//actividad_state.duracion = actividad.tipoDuracion
			actividad_state.fechaActividad = actividad.fechaActividad;
			actividad_state.horaInicio = actividad.horaInicio;
			actividad_state.idUsuario = actividad.idUsuario;
			// actividad_state.horaFin = actividad.horaFin
			actividad_state.nombrePersona = actividad.nombrePersona;
			state.cambiosActividades++;
		},
		delElementActividad(state, id) {
			const index = state.actividades.findIndex((actividad) => {
				return actividad.idActividad == id;
			});

			state.actividades.splice(index, 1);
		},
	},
	actions: {
		getActividades(context) {
			axios
				.get(BaseUrl + "/api/vt/actividades")
				.then((response) =>
					context.commit("setActividades", response.data)
				);
		},
	},
	getters: {
		filteredActividadesByCliente(state) {
			if (state.clienteActividadSelected != null) {
				return state.actividades.filter(
					(actividad) =>
						actividad.idCliente == state.clienteActividadSelected
				);
			}
			return state.actividades;
		},
	},
};
