export default {
    namespaced: true,
    state: {
        usuarios: [],
        usuario: null,
        modulos: [],
        submodulos: [],
        roles: [],
        load: false,
        success: false,
        rolesUsuario: []
    },
    mutations: {
        setRolesUsuario(state, rolesUsuario) {
            state.rolesUsuario = rolesUsuario
        },
        setUsuarios(state, usuarios) {
            state.usuarios = usuarios
        },
        setModulos(state, modulos) {
            state.modulos = modulos
        },
        setSubmodulos(state, submodulos) {
            state.submodulos = submodulos
        },
        setRoles(state, roles) {
            state.roles = roles
        },
        addRoleUsuario(state, data) {
            state.rolesUsuario.push(data)
        },
        deleteRoleUsuario(state, data) {
            const index = state.rolesUsuario.findIndex(
                (role) => role.idUsuario == data.idUsuario && role.idRole == data.idRole
            )
            state.rolesUsuario.splice(index, 1);
        },
        setLoad(state, value) {
            state.load = value
        },
        setSuccess(state, value) {
            state.success = value
        },
        addUsuario(state, usuario) {
            state.usuarios.push(usuario)
        },
        updateUsuario(state, request) {
            const usuario = state.usuarios.find((user) => {
                return user.idUsuario == request.idUsuario;
            });
            usuario.nombreUsuario = request.nombreUsuario;
            usuario.usuario = request.usuario;
            usuario.email = request.email;
            usuario.pass = request.pass;
            usuario.activo = request.activo;
            usuario.noEmpleado = request.noEmpleado;
            usuario.foto = request.foto;
            usuario.ruta = request.ruta;
            usuario.telefono = request.telefono;
        },
        removeUsuario(state, idUsuario) {
            const index = state.usuarios.findIndex((usuario) => {
                return usuario.idUsuario == idUsuario;
            });
            state.usuarios.splice(index, 1);
        },
    },
    actions: {    
        getUsuariosAll(context) {
            axios.get(BaseUrl + `/api/usuarios/all`).then(
                (response) => context.commit('setUsuarios', response.data)
            );
        },    
        selectUsuarios(context){
            axios.get(BaseUrl + '/api/usuarios/select')
                .then(
                    (response) => context.commit('setUsuarios', response.data)
                );
        },
        getModulos(context) {
            axios.get(BaseUrl + `/api/modulos`).then((response) => {
                context.commit('setModulos', response.data)
            });
        },
        getSubmodulos(context) {
            axios.get(BaseUrl + `/api/submodulos`).then((response) => {
                context.commit('setSubmodulos', response.data)
            });
        },
        getRoles(context) {
            axios.get(BaseUrl + `/api/roles`).then((response) => {
                context.commit('setRoles', response.data)
            });
        },
        getRolesByUsuario(context, idUsuario) {
            axios.get(BaseUrl + `/api/usuarios/${idUsuario}/roles`).then((response) => {
                context.commit('setRolesUsuario', response.data)
            });
        },
    },
    getters: {

    }
}