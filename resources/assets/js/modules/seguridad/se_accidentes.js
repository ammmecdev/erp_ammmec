export default {
    namespaced: true,
    state: {
        accidentes: [
            {
                titulo: "Titulo",
                involucrado: "Guillermo Murillo Ramirez",
                fecha: "2020-06-06",
                hora: "15:26:00",
                lugar: "Fresnillo",
                cuenta: "FRE-123-AB-SD-01",
                clasificacion: "Accidente A",
                metodo: "Accidente",
                hallazgo: "Cortadura de memo",
                responsable: "Guillermo Murillo Ramirez",
                accionesSeguimiento: "Comprar guantes",
                costo: 10,
                evidencia: "Archivo evidencia.jpg",
                fechaReal: "2020-10-10",
                verificacion: "NO",
                observacion: "Nada"
            },
            {
                titulo: "Titulo",
                involucrado: "Guillermo Murillo Ramirez",
                fecha: "2020-06-06",
                hora: "15:26:00",
                lugar: "Fresnillo",
                cuenta: "FRE-123-AB-SD-01",
                clasificacion: "Accidente A",
                metodo: "Accidente",
                hallazgo: "Cortadura de memo",
                responsable: "Guillermo Murillo Ramirez",
                accionesSeguimiento: "Comprar guantes",
                costo: 10,
                evidencia: "Archivo evidencia.jpg",
                fechaReal: "2020-10-10",
                verificacion: "NO",
                observacion: "Nada"
            },
            {
                titulo: "Titulo",
                involucrado: "Guillermo Murillo Ramirez",
                fecha: "2020-06-06",
                hora: "15:26:00",
                lugar: "Fresnillo",
                cuenta: "FRE-123-AB-SD-01",
                clasificacion: "Accidente A",
                metodo: "Accidente",
                hallazgo: "Cortadura de memo",
                responsable: "Guillermo Murillo Ramirez",
                accionesSeguimiento: "Comprar guantes",
                costo: 10,
                evidencia: "Archivo evidencia.jpg",
                fechaReal: "2020-10-10",
                verificacion: "NO",
                observacion: "Nada"
            },
            {
                titulo: "Titulo",
                involucrado: "Guillermo Murillo Ramirez",
                fecha: "2020-06-06",
                hora: "15:26:00",
                lugar: "Fresnillo",
                cuenta: "FRE-123-AB-SD-01",
                clasificacion: "Accidente A",
                metodo: "Accidente",
                hallazgo: "Cortadura de memo",
                responsable: "Guillermo Murillo Ramirez",
                accionesSeguimiento: "Comprar guantes",
                costo: 10,
                evidencia: "Archivo evidencia.jpg",
                fechaReal: "2020-10-10",
                verificacion: "NO",
                observacion: "Nada"
            },
            {
                titulo: "Titulo",
                involucrado: "Guillermo Murillo Ramirez",
                fecha: "2020-06-06",
                hora: "15:26:00",
                lugar: "Fresnillo",
                cuenta: "FRE-123-AB-SD-01",
                clasificacion: "Accidente A",
                metodo: "Accidente",
                hallazgo: "Cortadura de memo",
                responsable: "Guillermo Murillo Ramirez",
                accionesSeguimiento: "Comprar guantes",
                costo: 10,
                evidencia: "Archivo evidencia.jpg",
                fechaReal: "2020-10-10",
                verificacion: "NO",
                observacion: "Nada"
            },
        ],
        isBusy: false,
        search: "",
        colaboradores: ["Guillermo Murillo Ramirez"],
        colaboradorSelected: null,
        clasificacionSelected: null
    },
    mutations: {
        addAccidente(state, accidente) {
            state.accidentes.push(accidente);
        },
        updateAccidente(state, accidente) {
            const accidente_state = state.historiales.find(function (accidente) {
                return accidente.idAccidente == accidente.idAccidente;
            });
            if (accidente_state) {
                accidente_state.titulo = accidente.titulo;
               //ASIGNAR LOS DATOS A ACTUALIZAR DEL ACCIDENTE
            }
        },
        deleteAccidente(state, id) {
            const index = state.accidentes.findIndex((accidente) => {
                return accidente.idAccidente == id;
            });
            state.accidentes.splice(index, 1);
        },
        changeIsBusy(state, status) {
            state.isBusy = status;
        },
        setSearch(state, value) {
            state.search = value;
        },
    },
    actions: {
        getAccidentes(context) {
            context.commit("changeIsBusy", true);
            axios
                .get(BaseUrl + `/api/se/accidentes`)
                .then((response) => {
                    context.commit("setAccidentes", response.data);
                    context.commit("changeIsBusy", false);
                });
        },
        getColaboradores(context) {
            axios
                .get(
                    BaseUrl + `/api/se/accidentes/colaboradores`
                )
                .then((response) => {
                    if (response.data) {
                        const colaboradores = response.data;
                        context.commit("setColaboradores", colaboradores);
                    }
                });
        },
    },
    getters: {
        accidentesFiltered(state) {
            //INCLUIR CODIGO PARA FILTRO DE ACCIDENTES POR COLABORADOR Y CLASIFICACION
            return state.accidentes
        }
    },
};
