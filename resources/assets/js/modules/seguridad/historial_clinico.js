export default {
	namespaced: true,
	state: {
		historiales: [],
		isBusy: false,
        search: "",
        clinicas: [],
        tiposExamen: [],
	},
	mutations: {
		addHC(state, hc) {
			state.historiales.push(hc);
		},
		updateHC(state, hc) {
			const hc_state = state.historiales.find(function(hc_state) {
				return hc_state.id == hc.id;
			});
			if (hc_state) {
				hc_state.tipo_examen = hc.tipo_examen;
				hc_state.fecha = hc.fecha;
				hc_state.persona = hc.persona;
				hc_state.clinica = hc.clinica;
				hc_state.costo = hc.costo;
				hc_state.resultado = hc.resultado;
				hc_state.cuenta = hc.cuenta;
				hc_state.observaciones = hc.observaciones;
				hc_state.file = hc.file;
				hc_state.nombreFile = hc.nombreFile;
				hc_state.visible = hc.visible;
			}
		},
		deleteHC(state, id) {
			const index = state.historiales.findIndex((hc) => {
				return hc.id == id;
			});
			state.historiales.splice(index, 1);
		},
		setHC(state, historiales) {
			state.historiales = historiales;
		},
		changeIsBusy(state, status) {
			state.isBusy = status;
		},
		setSearch(state, value) {
			state.search = value;
        },
        setClinicas(state, clinicas) {
            state.clinicas = clinicas
        },
        setTiposExamen(state, tiposExamen) {
            state.tiposExamen = tiposExamen
        },
        addClinica(state, clinica) {
            state.clinicas.push(clinica)
        },
        addTipoExamen(state, tipoExamen) {
            state.tiposExamen.push(tipoExamen);
        }
	},
	actions: {
		getHistoriales(context) {
			context.commit("changeIsBusy", true);
			axios
				.get(BaseUrl + `/api/se/salud/historial-clinico`)
				.then((response) => {
					context.commit("setHC", response.data);
					context.commit("changeIsBusy", false);
				});
		},
		getClinicas(context) {
			axios
				.get(
					BaseUrl + `/api/se/salud/historial-clinico/clinicas/select`
				)
				.then((response) => {
					if (response.data){
                        const clinicas = response.data.map((clinica) => clinica.clinica);
                        context.commit("setClinicas", clinicas);
                    }
				});
		},
		getTiposExamen(context) {
			axios
				.get(
					BaseUrl +
						`/api/se/salud/historial-clinico/tipos-examenes/select`
				)
				.then((response) => {
					if (response.data){
                        const tiposExamen = response.data.map((tipo) => tipo.tipo);
                        context.commit("setTiposExamen", tiposExamen)
                    }
     
				});
        },
	},
	getters: {},
};
