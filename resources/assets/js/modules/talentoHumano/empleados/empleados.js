export default {
    namespaced: true,
    state: {
        filter: "",
        listEmpleados: [],
        busyEmpleados: false,
        busyEmpleado: false,
        empleado: {},

        diasDisponibles: 0,
        tiempoServicio: 0,
        horasDisponibles: 0,
        diasPendientes: 0,
        diasAcumulados: 0,

        fields: [
            {
                key: "noEmpleado",
                class: "text-center",
                label: "Empleado",
                sortable: true,
                thStyle: {
                    minWidth: "170px",
                },
            },
            {
                key: "nombre",
                sortable: true,
                thStyle: {
                    minWidth: "225px",
                },
            },
            {
                key: "curp",
                label: "CURP",
                class: "text-center",
                sortable: true,
                thStyle: {
                    minWidth: "170px",
                },
            },
            {
                key: "rfc",
                label: "RFC",
                sortable: true,
                class: "text-center",
                thStyle: {
                    minWidth: "170px",
                },
            },
            {
                key: "nss",
                class: "text-center",
                label: "NSS",
                sortable: true,
                thStyle: {
                    minWidth: "170px",
                },
            },
            {
                key: "nombre_puesto",
                label: "Puesto",
                sortable: true,
                class: "text-center",
                thStyle: {
                    minWidth: "225px",
                },
            },
            {
                key: "nombre_departamento",
                label: "Departamento",
                sortable: true,
                class: "text-center",
                thStyle: {
                    minWidth: "225px",
                },
            },
            {
                key: "nombre_area",
                label: "Área",
                class: "text-center",
                sortable: true,
                thStyle: {
                    minWidth: "225px",
                },
            },
            {
                key: "fechaIngreso",
                class: "text-center",
                sortable: true,
                label: "Fecha de ingreso",
                thStyle: {
                    minWidth: "225px",
                },
            },
            {
                key: "fechaTermino",
                class: "text-center",
                sortable: true,
                label: "Fecha de termino",
                thStyle: {
                    minWidth: "275px",
                },
            },
            {
                key: "actions",
                class: "text-center",
                label: "Acción",
                thStyle: {
                    minWidth: "80px",
                },
            },
        ],
    },
    mutations: {
        setEmpleados(state, data) {
            state.listEmpleados = data;
        },
        setEmpleado(state, data) {
            state.empleado = data;
        },
        addEmpleado(state, data) {
            state.listEmpleados.push(data);
        },
        updateEmpleado(state, empleado) {
            const empleado_state = state.listEmpleados.find(function(empleado_state) {
                return empleado_state.noEmpleado == empleado.noEmpleado;
            });
            if (empleado_state) {
                empleado_state.noEmpleado = empleado.noEmpleado;
                empleado_state.idPuesto = empleado.idPuesto;

                empleado_state.nombre_puesto = empleado.nombre_puesto;
                empleado_state.nombre_area = empleado.nombre_area;
                empleado_state.nombre_departamento = empleado.nombre_departamento;

                empleado_state.nombre = empleado.nombre;
                empleado_state.nacionalidad = empleado.nacionalidad;
                empleado_state.sexo = empleado.sexo;
                empleado_state.edad = empleado.edad;
                empleado_state.estadoCivil = empleado.estadoCivil;
                empleado_state.domicilio = empleado.domicilio;
                empleado_state.cp = empleado.cp;
                empleado_state.curp = empleado.curp;
                empleado_state.rfc = empleado.rfc;
                empleado_state.fechaIngreso = empleado.fechaIngreso;
                empleado_state.tipoContrato = empleado.tipoContrato;
                empleado_state.tipoPuesto = empleado.tipoPuesto;
                empleado_state.fechaTermino = empleado.fechaTermino;
                empleado_state.telefono = empleado.telefono;
                empleado_state.email = empleado.email;
                empleado_state.emailPersonal = empleado.telefono;
                empleado_state.nss = empleado.nss;
                empleado_state.unidadNegocio = empleado.unidadNegocio;
                empleado_state.salario = empleado.salario;
                empleado_state.tarjetaBanbajio = empleado.tarjetaBanbajio;
                empleado_state.fechaBaja = empleado.fechaBaja;
                empleado_state.status = empleado.status;
            }
        },
        removeEmpleado(state, noEmpleado) {
            const index = state.listEmpleados.findIndex(empleado_state => {
                return empleado_state.noEmpleado == noEmpleado;
            });
            state.listEmpleados.splice(index, 1);
        },
        setFilterSearch(state, value) {
            state.filter = value;
        },
        changeBusyEmpleados(state, value) {
            state.busyEmpleados = value;
        },
        changeBusyEmpleado(state, value) {
            state.busyEmpleado = value;
        },
        setDiasDisponibles(state, dias) {
            state.diasDisponibles = dias;
        },
        setTiempoServicio(state, tiempoServicio) {
            state.tiempoServicio = tiempoServicio;
        },
        setHorasDisponibles(state, horas) {
            state.horasDisponibles = horas;
        },
        setDiasPendientes(state, dias) {
            state.diasPendientes = dias;
        },
        setDiasAcumulados(state, dias) {
            state.diasAcumulados = dias;
        },
    },
    actions: {
        getEmpleados(context) {
            context.commit("changeBusyEmpleados", true);
            axios
                .get(BaseUrl + `/api/th/empleados`)
                .then(response => {
                    context.commit("setEmpleados", response.data);
                    context.commit("changeBusyEmpleados", false);
                });
        },
        getEmpleado(context, noEmpleado) {
            context.commit("changeBusyEmpleado", true);
            axios
                .get(BaseUrl + `/api/th/empleado/${noEmpleado}`)
                .then(response => {
                    context.commit("setEmpleado", response.data);
                    context.commit("changeBusyEmpleado", false);
                });
        },
    },
    getters: {
        // empleadosFiltered(state) {
        //     var empleados = state.listEmpleados;
        //     if (state.filter) {
        //         empleados = empleados.filter(
        //             item =>
        //                 item.nombre
        //                     .toLowerCase()
        //                     .indexOf(state.filter.toLowerCase()) > -1 ||
        //                 item.noEmpleado
        //                     .toLowerCase()
        //                     .indexOf(state.filter.toLowerCase()) > -1
        //         );
        //     }
        //     return empleados.reverse();
        // }
    },
};
