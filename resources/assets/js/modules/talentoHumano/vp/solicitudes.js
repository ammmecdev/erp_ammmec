export default {
    namespaced: true,
    state: {
        solicitudActiva: null,
        tipoSolicitudes: "misSolicitudes",
        busySolicitudes: false,
        solicitudesConsultadas: "",
        listSolicitudes: [],
        listSolicitudesByEmpleado: [],
        periodo: {
            fechaInicio: null,
            fechaTermino: null,
        },
        filterEtapa: null,
        filterInicio: null,
        filterFin: null,
        filterSearch: null,
        vacacionesPendientes: null,
    },
    mutations: {
        setFilterSearch(state, search) {
            state.filterSearch = search;
        },
        setFilterInicio(state, inicio) {
            state.filterInicio = inicio;
        },
        setFilterFin(state, fin) {
            state.filterFin = fin;
        },
        setFilterEtapa(state, etapa) {
            state.filterEtapa = etapa;
        },
        setTipoSolicitudes(state, tipo) {
            state.solicitudActiva = null;
            state.tipoSolicitudes = tipo;
        },
        setSolicitudActiva(state, solicitud) {
            state.solicitudActiva = solicitud;
        },
        setListSolicitudes(state, solicitudes) {
            state.listSolicitudes = solicitudes;
        },
        addSolicitud(state, solicitud){
            state.listSolicitudes.push(solicitud);
        },
        addSolicitudByEmpleado(state, solicitud){
            state.listSolicitudesByEmpleado.push(solicitud);
        },
        changeBusySolicitudes(state, value) {
            state.busySolicitudes = value;
        },
        setSolicitudesConsultadas(state, value) {
            state.solicitudesConsultadas = value;
        },
        setPeriodoLaboral(state, periodo) {
            state.periodo = periodo;
        },
        setListSolicitudesByEmpleado(state, solicitudes) {
            state.listSolicitudesByEmpleado = solicitudes;
        },
        updateSolicitud(state, solicitud) {
            const solicitud_state = state.listSolicitudes.find(function (
                solicitud_state
            ) {
                return solicitud_state.idSolicitud == solicitud.idSolicitud;
            });
            if (solicitud_state) {
                solicitud_state.idEtapa = solicitud.idEtapa;
                solicitud_state.nombre_etapa = solicitud.nombre_etapa;
                solicitud_state.fechaAutorizacion = solicitud.fechaAutorizacion;
                solicitud_state.autorizacion = solicitud.autorizacion;
            }
        },
        updateSolicitudByEmpleado(state, solicitud) {
            const solicitud_state = state.listSolicitudesByEmpleado.find(function (
                solicitud_state
            ) {
                return solicitud_state.idSolicitud == solicitud.idSolicitud;
            });
            if (solicitud_state) {
                solicitud_state.idEtapa = solicitud.idEtapa;
                solicitud_state.nombre_etapa = solicitud.nombre_etapa;
                solicitud_state.fechaAutorizacion = solicitud.fechaAutorizacion;
                solicitud_state.autorizacion = solicitud.autorizacion;
            }
        },
        updateSolicitudActiva(state, solicitud) {
            state.solicitudActiva.idEtapa = solicitud.idEtapa;
            state.solicitudActiva.nombre_etapa = solicitud.nombre_etapa;
            state.solicitudActiva.fechaAutorizacion = solicitud.fechaAutorizacion;
            state.solicitudActiva.autorizacion = solicitud.autorizacion;
            state.solicitudActiva.semanasCapturacion = solicitud.semanasCapturacion;
        },
        setVacacionesPendientes(state, data) {
            state.vacacionesPendientes = data;
        }
    },
    actions: {
        // TODAS
        getSolicitudes(context) {
            context.commit("changeBusySolicitudes", true);
            axios.get(BaseUrl + `/api/th/vp/solicitudes`).then(response => {
                context.commit("setListSolicitudes", response.data);
                context.commit("changeBusySolicitudes", false);
                context.commit("setSolicitudesConsultadas", "todas");

            });
        },
        // POR AUTORIZAR
        getSolicitudesByAutorizador(context, idUsuario) {
            context.commit("changeBusySolicitudes", true);
            axios
                .get(BaseUrl + `/api/th/vp/solicitudes/autorizador/${idUsuario}`)
                .then(response => {
                    context.commit("setListSolicitudes", response.data);
                    context.commit("changeBusySolicitudes", false);
                    context.commit("setSolicitudesConsultadas", "byAutorizador");
                });
        },
        // POR USUARIO
        getSolicitudesByUsuario(context, idUsuario) {
            context.commit("changeBusySolicitudes", true);
            axios
                .get(BaseUrl + `/api/th/vp/solicitudes/usuarios/${idUsuario}`)
                .then(response => {
                    context.commit("setListSolicitudes", response.data);
                    context.commit("changeBusySolicitudes", false);
                    context.commit("setSolicitudesConsultadas", "byUsuario");

                });
        },
        // POR PERIODO Y USUARIO
        getSolicitudesByEmpleado(context, idUsuario) {
            axios
                // .get(BaseUrl + `/api/th/vp/solicitudes/empleado/${idUsuario}?inicio=${context.state.periodo.fechaInicio}&termino=${context.state.periodo.fechaTermino}`)
                .get(BaseUrl + `/api/th/vp/solicitudes/empleado/${idUsuario}`)
                .then(response => {
                    context.commit("setListSolicitudesByEmpleado", response.data);
                });
        },
        getEtapas(context) {
            axios.get(BaseUrl + `/api/th/vp/etapas`).then(response => {
                context.commit("setEtapas", response.data);
            });
        }, 
        // DÍAS PENDIENTES POR DISFRUTAR ACUMULADO DE AÑOS ATERIORES
        getVacacionesPendientes(context, noEmpleado) {
            axios.get(BaseUrl + `/api/th/vp/pendientes/${noEmpleado}`).then(response => {
                context.commit("setVacacionesPendientes", response.data);
            });
        }
    },
    getters: {
        solicitudesFiltered(state) {
            var solicitudes = [];
            solicitudes = state.listSolicitudes;
            if (state.filterEtapa) {
                solicitudes = state.listSolicitudes.filter(
                    solicitud => solicitud.idEtapa == state.filterEtapa
                );
            }
            if (state.filterInicio && state.filterFin) {
                solicitudes = solicitudes.filter(
                    solicitud =>
                        solicitud.fechaInicio >= state.filterInicio &&
                        state.filterFin >= solicitud.fechaInicio 
                );
            }
            if (state.filterSearch) {
                solicitudes = solicitudes.filter(
                    solicitud => {
                        if (solicitud.idUsuario) {
                            if (solicitud.usuario.nombreUsuario
                                    .toLowerCase()
                                    .indexOf(state.filterSearch.toLowerCase()) > -1 ||
                                solicitud.motivoAusencia
                                    .toLowerCase()
                                    .indexOf(state.filterSearch.toLowerCase()) > -1

                            ) {
                                return solicitud;
                            }
                        }
                    }
                );
            }
            return solicitudes.reverse();
        }
    }
};