export default {
    namespaced: true,
    state: {
        filter: "",
        listPuestos: [],
        busyPuestos: false,
    },
    mutations: {
        setPuestos(state, data) {
            state.listPuestos = data;
        },
        addPuesto(state, data) {
            state.listPuestos.push(data);
        },
        updatePuesto(state, puesto) {
            const puesto_state = state.listPuestos.find(function(puesto_state) {
                return puesto_state.idPuesto == puesto.idPuesto;
            });
            if (puesto_state) {
                puesto_state.nombre = puesto.nombre;
                puesto_state.codigo = puesto.codigo;
                puesto_state.idArea = puesto.idArea;
                puesto_state.nombre_area = puesto.nombre_area;
                puesto_state.nombre_departamento = puesto.nombre_departamento;
            }
        },
        removePuesto(state, idPuesto) {
            const index = state.listPuestos.findIndex(puesto_state => {
                return puesto_state.idPuesto == idPuesto;
            });
            state.listPuestos.splice(index, 1);
        },
        setFilterSearch(state, value) {
            state.filter = value;
        },
        changeBusyPuestos(state, value) {
            state.busyPuestos = value;
        },
    },
    actions: {
        getPuestos(context) {
            context.commit("changeBusyPuestos", true);
            axios
                .get(BaseUrl + `/api/th/puestos`)
                .then(response => {
                    context.commit("setPuestos", response.data);
                    context.commit("changeBusyPuestos", false);
                });
        },
    },
    getters: {
        puestosFiltered(state) {
            var puestos = state.listPuestos;
            if (state.filter) {
                puestos = puestos.filter(
                    puesto =>
                        puesto.nombre
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1 ||
                        puesto.codigo
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1
                );
            }
            return puestos.reverse();
        }
    },
};
