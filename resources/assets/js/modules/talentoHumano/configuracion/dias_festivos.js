export default {
    namespaced: true,
    state: {
        filter: "",
        listDiasFestivos: [],
        busyDiasFestivos: false,
    },
    mutations: {
        setDiasFestivos(state, data) {
            state.listDiasFestivos = data;
        },
        addDiaFestivo(state, data) {
            state.listDiasFestivos.push(data);
        },
        updateDiaFestivo(state, diaFestivo) {
            const diaFestivo_state = state.listDiasFestivos.find(function(diaFestivo_state) {
                return diaFestivo_state.idDiaFestivo == diaFestivo.idDiaFestivo;
            });
            if (diaFestivo_state) {
                diaFestivo_state.descripcion = diaFestivo.descripcion;
                diaFestivo_state.fecha = diaFestivo.fecha;
                diaFestivo_state.fechaFeriado = diaFestivo.fechaFeriado;
            }
        },
        removeDiaFestivo(state, idDiaFestivo) {
            const index = state.listDiasFestivos.findIndex(diaFestivo_state => {
                return diaFestivo_state.idDiaFestivo == idDiaFestivo;
            });
            state.listDiasFestivos.splice(index, 1);
        },
        setFilterSearch(state, value) {
            state.filter = value;
        },
        changeBusyDiasFestivos(state, value) {
            state.busyDiasFestivos = value;
        },
    },
    actions: {
        getDiasFestivos(context) {
            context.commit("changeBusyDiasFestivos", true);
            axios
                .get(BaseUrl + `/api/th/vp/diasFestivos`)
                .then(response => {
                    context.commit("setDiasFestivos", response.data);
                    context.commit("changeBusyDiasFestivos", false);
                });
        },
    },
    getters: {
        diasFestivosFiltered(state) {
            var diasFestivos = state.listDiasFestivos;
            if (state.filter) {
                diasFestivos = diasFestivos.filter(
                    item =>
                        item.descripcion
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1
                );
            }
            return diasFestivos.reverse();
        }
    },
};
