export default {
    namespaced: true,
    state: {
        filter: "",
        listDepartamentos: [],
        busyDepartamentos: false,
    },
    mutations: {
        setDepartamentos(state, data) {
            state.listDepartamentos = data;
        },
        addDepartamento(state, data) {
            state.listDepartamentos.push(data);
        },
        updateDepartamento(state, area) {
            const area_state = state.listDepartamentos.find(function(area_state) {
                return area_state.idDepartamento == area.idDepartamento;
            });
            if (area_state) {
                area_state.nombre = area.nombre;
                area_state.codigo = area.codigo;
            }
        },
        removeDepartamento(state, idDepartamento) {
            const index = state.listDepartamentos.findIndex(area_state => {
                return area_state.idDepartamento == idDepartamento;
            });
            state.listDepartamentos.splice(index, 1);
        },
        setFilterSearch(state, value) {
            state.filter = value;
        },
        changeBusyDepartamentos(state, value) {
            state.busyDepartamentos = value;
        },
    },
    actions: {
        getDepartamentos(context) {
            context.commit("changeBusyDepartamentos", true);
            axios
                .get(BaseUrl + `/api/th/departamentos`)
                .then(response => {
                    context.commit("setDepartamentos", response.data);
                    context.commit("changeBusyDepartamentos", false);
                });
        },
    },
    getters: {
        departamentosFiltered(state) {
            var departamentos = state.listDepartamentos;
            if (state.filter) {
                departamentos = departamentos.filter(
                    departamento =>
                        departamento.nombre
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1 ||
                        departamento.codigo
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1
                );
            }
            return departamentos;
        }
    },
};
