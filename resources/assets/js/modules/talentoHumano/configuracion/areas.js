export default {
    namespaced: true,
    state: {
        filter: "",
        listAreas: [],
        busyAreas: false,
    },
    mutations: {
        setAreas(state, data) {
            state.listAreas = data;
        },
        addArea(state, data) {
            state.listAreas.push(data);
        },
        updateArea(state, area) {
            const area_state = state.listAreas.find(function(area_state) {
                return area_state.idArea == area.idArea;
            });
            if (area_state) {
                area_state.nombre = area.nombre;
                area_state.codigo = area.codigo;
            }
        },
        removeArea(state, idArea) {
            const index = state.listAreas.findIndex(area_state => {
                return area_state.idArea == idArea;
            });
            state.listAreas.splice(index, 1);
        },
        setFilterSearch(state, value) {
            state.filter = value;
        },
        changeBusyAreas(state, value) {
            state.busyAreas = value;
        },
    },
    actions: {
        getAreas(context) {
            context.commit("changeBusyAreas", true);
            axios
                .get(BaseUrl + `/api/th/areas`)
                .then(response => {
                    context.commit("setAreas", response.data);
                    context.commit("changeBusyAreas", false);
                });
        },
    },
    getters: {
        areasFiltered(state) {
            var areas = state.listAreas;
            if (state.filter) {
                areas = areas.filter(
                    area =>
                        area.nombre
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1 ||
                        area.codigo
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1
                );
            }
            return areas.reverse();
        }
    },
};
