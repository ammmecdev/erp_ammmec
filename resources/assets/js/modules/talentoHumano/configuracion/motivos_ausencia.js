export default {
    namespaced: true,
    state: {
        filter: "",
        listMotivosAusencia: [],
        busyMotivosAusencia: false,
    },
    mutations: {
        setMotivosAusencia(state, data) {
            state.listMotivosAusencia = data;
        },
        addMotivoAusencia(state, data) {
            state.listMotivosAusencia.push(data);
        },
        updateMotivoAusencia(state, motivoAusencia) {
            const motivoAusencia_state = state.listMotivosAusencia.find(function(motivoAusencia_state) {
                return motivoAusencia_state.idMotivoAusencia == motivoAusencia.idMotivoAusencia;
            });
            if (motivoAusencia_state) {
                motivoAusencia_state.motivo = motivoAusencia.motivo;
                motivoAusencia_state.diasAnticipacion = motivoAusencia.diasAnticipacion;
            }
        },
        removeMotivoAusencia(state, idMotivoAusencia) {
            const index = state.listMotivosAusencia.findIndex(motivoAusencia_state => {
                return motivoAusencia_state.idMotivoAusencia == idMotivoAusencia;
            });
            state.listMotivosAusencia.splice(index, 1);
        },
        setFilterSearch(state, value) {
            state.filter = value;
        },
        changeBusyMotivosAusencia(state, value) {
            state.busyMotivosAusencia = value;
        },
    },
    actions: {
        getMotivosAusencia(context) {
            context.commit("changeBusyMotivosAusencia", true);
            axios
                .get(BaseUrl + `/api/th/motivosAusencia`)
                .then(response => {
                    context.commit("setMotivosAusencia", response.data);
                    context.commit("changeBusyMotivosAusencia", false);
                });
        },
    },
    getters: {
        motivosAusenciaFiltered(state) {
            var motivosAusencia = state.listMotivosAusencia;
            if (state.filter) {
                motivosAusencia = motivosAusencia.filter(
                    motivo =>
                        motivo.motivo
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1
                );
            }
            return motivosAusencia.reverse();
        }
    },
};
