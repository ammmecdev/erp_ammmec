export default{
	namespaced: true,
	state:{
		notifications: [],
		notCount: 0,
		isBusyNot: false,
		isNotificated: false,
	},
	mutations:{
		setNotifications(state, notifications)
        {
            state.notifications = notifications;
        },
		setIdNotifications(state, isNotificated)
        {
			state.isNotificated = isNotificated;
        },
        setNotCount(state, count)
        {
			state.notCount = count;
        },
        changeIsBusyNot(state)
        {
        	state.isBusyNot = !state.isBusyNot
        }
	},
	actions:{
		getNotifications(context)
		{
				context.commit('changeIsBusyNot')
				axios.get(BaseUrl + '/api/notifications/')
				.then(
					response => {
						context.commit('setNotifications', response.data)
						if(context.state.isBusyNot)
							context.commit('changeIsBusyNot')
					})
		}
	},
	getters:{

	}
}