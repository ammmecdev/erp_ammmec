export default {
    namespaced: true,
    state: {
        listActivos: [],
        busyActivos: false,
        selectedCategoria: "TODAS",
        listCategorias: [],
        filter: "",
        activoSelected: null,
        activoTreeselect: null,
    },
    mutations: {
        changeBusyActivos(state, value) {
            state.busyActivos = value;
        },
        setListActivos(state, activos) {
            state.listActivos = activos;
        },
        setListCategorias(state, categorias) {         
            state.listCategorias = categorias;
            state.listCategorias.unshift("TODAS");
        },
        setSelectedCategoria(state, categoria) {
            state.selectedCategoria = categoria;
        },
        setFilterSearch(state, value) {
            state.filter = value;
        },
        addActivo(state, activo) {
            state.listActivos.push(activo);
        },
        updateActivo(state, data) {
            const activo = state.listActivos.find((activo) => {
                return activo.idActivo == data.idActivo;
            });
            activo.articulo = data.articulo;
            activo.unidad = data.unidad;
            activo.existencia = data.existencia;
            activo.categoria = data.categoria;
            activo.costoUnitario = data.costoUnitario;
        },
        removeActivo(state, id) {
            const index = state.listActivos.findIndex((item) => {
                return item.idActivo == id;
            });
            state.listActivos.splice(index, 1);
        },
        setActivoSelected(state, activoSelected) {
            state.activoSelected = activoSelected;
        },
        setTreeselectActivo(state, activoTreeselect) {
            state.activoTreeselect = activoTreeselect;
        },
    },
    actions: {
        getActivos(context) {
            context.commit("changeBusyActivos", true);
            axios.get(BaseUrl + `/api/alm/inventario/activos`).then(response => {
                context.commit("setListActivos", response.data);
                context.commit("changeBusyActivos", false);
            });
        },
        getActivoCategorias(context) {
            axios.get(BaseUrl + `/api/alm/activo/categorias/select`).then((response) => {
                if (response.data){
                    const categorias = response.data.map((item) => item.categoria);
                    context.commit("setListCategorias", categorias);
                }
            });
        },
    },
    getters: {
        activosFiltered(state) {
            var activos = [];
            if (state.selectedCategoria != "TODAS") {
                activos = state.listActivos.filter(
                    activo => activo.categoria == state.selectedCategoria
                );
            }else activos = state.listActivos;

            if (state.filter) {
                activos = activos.filter(
                    activo =>
                        activo.articulo
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1 ||
                        activo.unidad
                            .toLowerCase()
                            .indexOf(state.filter.toLowerCase()) > -1
                );
            }
            return activos.reverse();
        }
    }
};