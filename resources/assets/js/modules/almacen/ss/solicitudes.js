export default {
    namespaced: true,
    state: {
        //SOLICITUDES
        solicitudActiva: null,
        solicitudSuministroActiva: null,
        listSolicitudes: [],
        busySolicitudes: false,
        busySolicitudActiva: false,
        tipoSolicitudes: "",
        solicitudesConsultadas: "", //pendiente


        //ARTICULOS
        listArticulos: [],
        busyArticulos: false,
        // busyArticulosByOC: false,
        // listArticulosByOC: [],


        //ETAPAS
        listEtapas: [],

        
        //FILTROS
        filterEtapa: 6,
        filterInicio: null,
        filterFin: null,
        filterSearch: null,
    },
    mutations: {

        //SOLICITUDES
        setListSolicitudes(state, solicitudes) {
            state.listSolicitudes = solicitudes;
        },
        setSolicitudActiva(state, solicitud) {
            state.solicitudActiva = solicitud;
            state.solicitudSuministroActiva = solicitud;
        }, 
        addSolicitud(state, solicitud) {
            const solicitud_state = state.listSolicitudes.find(
                (solicitud_state) => solicitud_state.idSolicitud == solicitud.idSolicitud
            )
            if (!solicitud_state)
                state.listSolicitudes.push(solicitud);
        },
        changeBusySolicitudes(state, value) {
            state.busySolicitudes = value;
        },
        changeBusySolicitudActiva(state, value) {
            state.busySolicitudActiva = value;
        },
        updateSolicitud(state, solicitud) {
            const solicitud_state = state.listSolicitudes.find(function (
                solicitud_state
            ) {
                return solicitud_state.idSolicitud == solicitud.idSolicitud;
            });
            if (solicitud_state) {
                solicitud_state.idEtapa = solicitud.idEtapa;
                solicitud_state.nombre_etapa = solicitud.nombre_etapa;
                solicitud_state.fechaSolicitudCompra = solicitud.fechaSolicitudCompra;  
            }
        },
        updateSolicitudActiva(state, solicitud) {
            state.solicitudActiva.idEtapa = solicitud.idEtapa;
            state.solicitudActiva.nombre_etapa = solicitud.nombre_etapa;
            solicitud_state.fechaSolicitudCompra = solicitud.fechaSolicitudCompra;
            solicitud_state.fechaTermino = solicitud.fechaTermino;
        },
        setTipoSolicitudes(state, tipo) {
            state.solicitudActiva = null;
            state.tipoSolicitudes = tipo;
        },
        setSolicitudesConsultadas(state, value) {
            state.solicitudesConsultadas = value
        },
        // FILTROS
        setFilterSearch(state, search) {
            state.filterSearch = search;
        },
        setFilterInicio(state, inicio) {
            state.filterInicio = inicio;
        },
        setFilterFin(state, fin) {
            state.filterFin = fin;
        },
        setFilterEtapa(state, etapa) {
            state.filterEtapa = etapa;
        },
        //ARTICULOS 
        setArticulos(state, articulos) {
            state.listArticulos = articulos;
        },
        changeBusyArticulos(state, value) {
            state.busyArticulos = value;
        },
        updateArticulo(state, articulo) {
            const articulo_state = state.listArticulos.find(function (
                articulo_state
            ) {
                return articulo_state.idArticulo == articulo.idArticulo;
            });

            if (articulo_state) {
                articulo_state.idActivo = articulo.idActivo;
                articulo_state.tipo = articulo.tipo;
                articulo_state.salida = articulo.salida;
                articulo_state.descripcion = articulo.descripcion;

            }
        },
        //ETAPAS 
        setEtapas(state, etapas) {
            state.listEtapas = etapas;
        },


        // ART - ORDEN DE COMPRA
        // setArticulosByOC(state, articulos) {
        //     state.listArticulosByOC = articulos;
        // },
        // changeBusyArticulosByOC(state, value) {
        //     state.busyArticulosByOC = value;
        // },



        // setListSolicitudesAll(state, solicitudes) {
        //     state.listSolicitudes = solicitudes;
        // },
        // deleteSolicitud(state, idSolicitud) {
        //     if (state.solicitudActiva == idSolicitud)
        //         state.solicitudActiva = null
        //     const index = state.listSolicitudes.findIndex(solicitud_state => {
        //         return solicitud_state.idSolicitud == idSolicitud;
        //     });
        //     state.listSolicitudes.splice(index, 1);
        // },
        // deleteArticulo(state, idComprobacion) {
        //     const index = state.listComprobaciones.findIndex(comprobacion_state => {
        //         return comprobacion_state.idComprobacion == idComprobacion;
        //     });
        //     state.listComprobaciones.splice(index, 1);
        // },
    },
    actions: {
        // TODAS
        getSolicitudes(context) {
            context.commit("changeBusySolicitudes", true);
            axios.get(BaseUrl + `/api/alm/ss/solicitudes`).then(response => {
                context.commit("setListSolicitudes", response.data);
                context.commit("changeBusySolicitudes", false);
                context.commit("setSolicitudesConsultadas", "todas");

            });
        },
        // POR USUARIO
        getSolicitudesByUsuario(context, idUsuario) {
            context.commit("changeBusySolicitudes", true);
            axios
                .get(BaseUrl + `/api/alm/ss/solicitudes/usuarios/${idUsuario}`)
                .then(response => {
                    context.commit("setListSolicitudes", response.data);
                    context.commit("changeBusySolicitudes", false);
                    context.commit("setSolicitudesConsultadas", "byUsuario");

                });
        },
        //CONSULTAS
        getSolicitud(context, idSolicitud) {
            context.commit("changeBusySolicitudActiva", true);
            axios
                .get(BaseUrl + `/api/alm/ss/solicitud/${idSolicitud}`)
                .then(response => {
                    context.commit("setSolicitudActiva", response.data);
                    context.commit("changeBusySolicitudActiva", false);
                });
        },
        getArticulos(context, idSolicitud) {
            context.commit("changeBusyArticulos", true);
            axios
                .get(BaseUrl + `/api/alm/ss/solicitud/${idSolicitud}/articulos`)
                .then(response => {
                    context.commit("setArticulos", response.data);
                });
        },
        getEtapas(context) {
            axios.get(BaseUrl + `/api/alm/ss/etapas`).then(response => {
                context.commit("setEtapas", response.data);
            });
        }, 
        // getArticulosByOrdenCompra(context, idOrdenCompra) {
        //     context.commit("changeBusyArticulosByOC", true);
        //     axios
        //         .get(BaseUrl + `/api/alm/ss/ordencompra/${idOrdenCompra}/articulos`)
        //         .then(response => {
        //             context.commit("setArticulosByOC", response.data);
        //              context.commit("changeBusyArticulosByOC", false);
        //         });
        // },   
    },
    getters: {
        solicitudesFiltered(state) {
            var solicitudes = [];
            solicitudes = state.listSolicitudes;

            if (state.filterInicio && state.filterFin) {
                solicitudes = solicitudes.filter(
                    solicitud =>
                        solicitud.created_at >= state.filterInicio &&
                        state.filterFin >= solicitud.created_at 
                );
            }
            if (state.filterSearch) {
                solicitudes = solicitudes.filter(
                    solicitud =>
                        solicitud.idSolicitud == state.filterSearch ||
                        solicitud.usuario.nombreUsuario
                            .toLowerCase()
                            .indexOf(state.filterSearch.toLowerCase()) > -1 ||
                        solicitud.cuenta
                            .toLowerCase()
                            .indexOf(state.filterSearch.toLowerCase()) > -1
                );
            }
           
            if (state.filterEtapa) { 
                if (state.filterEtapa == 6) {
                    solicitudes = solicitudes.filter(
                        solicitud => solicitud.idEtapa != 1
                    );
                    solicitudes = solicitudes.filter(
                        solicitud => solicitud.idEtapa != 5
                    );
                }else{
                    solicitudes = solicitudes.filter(
                        solicitud => solicitud.idEtapa == state.filterEtapa
                    );   
                }   
            }
            return solicitudes.reverse();
        }
    }
};