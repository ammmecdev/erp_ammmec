import Vue from 'vue';
import Vuex from 'vuex';
import notifications from './modules/notifications'

//ADMINISTRACION
import ad_estructura from './modules/administracion/estructura'
import ad_roles from './modules/administracion/roles'
import ad_cf_cuentas_externas from './modules/administracion/configuracion/cf_cuentas_externas'

//COMPRAS
import co_sc_solicitudes from './modules/compras/sc/solicitudes'
import co_sc_orden_compra from './modules/compras/sc/orden_compra'
import co_sc_recepcion_mercancia from './modules/compras/sc/recepcion_mercancia'
import co_conf_proveedores from './modules/compras/configuracion/proveedores'
import co_conf_autorizaciones from './modules/compras/configuracion/autorizaciones'

//ALMACEN
import alm_inventario_activos from './modules/almacen/inventarios/activos'
import alm_ss_solicitudes from './modules/almacen/ss/solicitudes'

//VENTAS
import actividades from './modules/ventas/actividades'
import negocios from './modules/ventas/negocios'
import notas from './modules/ventas/notas'
import clientes from './modules/ventas/clientes'
import contactos from './modules/ventas/contactos'
import usuarios from './modules/usuarios'
import indicadores from './modules/ventas/indicadores'
import presupuesto from './modules/ventas/presupuesto'
import embudos from './modules/ventas/embudos'
import vt_unidades_negocio from './modules/ventas/unidades_negocio'
import vt_proyecciones from './modules/ventas/proyecciones'

//NORMATIVIDAD
import areas from './modules/calidad/areas'
import puestos from './modules/calidad/puestos'
import organizacion from './modules/calidad/organizacion'

//SERVICIOS
import reporte_actividades from './modules/servicios/reporte_actividades'
import op_ra_indicadores_actividades from './modules/servicios/op_ra_indicadores_actividades'
import op_co_costos from './modules/servicios/op_co_costos'
import op_co_cuenta_real from './modules/servicios/op_co_cuenta_real'

//SISTEMAS
import cuentas_electronicas from './modules/sistemas/admin/cuentas_electronicas'
import equipos_electronicos from './modules/sistemas/admin/equipos_electronicos'
import licencias_software from './modules/sistemas/admin/licencias_software'

//FINANZAS
//Gastos
import solicitudes_gastos from './modules/finanzas/gastos/solicitudes'
import fn_cg_movimientos from './modules/finanzas/gastos/movimientos'
import fn_cg_comprobaciones from './modules/finanzas/gastos/comprobaciones'
import fn_cg_disposicion_efectivo from './modules/finanzas/gastos/disposiciones'
import fn_cg_archivos from './modules/finanzas/gastos/archivos'

//Configuración
import cnf_cuentas_contables from './modules/finanzas/configuracion/cuentas_contables'
import cnf_cuentas_internas from "./modules/finanzas/configuracion/cuentas_internas";
import cnf_mano_obra from "./modules/finanzas/configuracion/mano_obra";

//SEGURIDAD
import historial_clinico from "./modules/seguridad/historial_clinico";
import se_accidentes from "./modules/seguridad/se_accidentes";

//TICKETS
import tickets from "./modules/tickets/tickets";
import indicators from "./modules/tickets/indicators";
import services from "./modules/tickets/services";
import tk_notifications from "./modules/tickets/notifications";

//PLANES
import planes from "./modules/planes/planes";

//TALENTO HUMANO
import th_areas from "./modules/talentoHumano/configuracion/areas";
import th_departamentos from "./modules/talentoHumano/configuracion/departamentos";
import th_puestos from "./modules/talentoHumano/configuracion/puestos";
import th_empleados from "./modules/talentoHumano/empleados/empleados";
//Vacaciones y permisos
import th_motivos_ausencia from "./modules/talentoHumano/configuracion/motivos_ausencia";
import th_dias_festivos from "./modules/talentoHumano/configuracion/dias_festivos";
import th_vp_solicitudes from "./modules/talentoHumano/vp/solicitudes";


Vue.use(Vuex);

export default new Vuex.Store({
	state: {
        usuario: null,
        moduleActive: null,
        permissions: [],
		nombreUsuario: null,
        oportunidades: [],
        causas:[],
		isBusy: false,
		totalRows: 0,
		filterFields: [],
		currentPage: 1, //posicion del paginado
		perPage: 15, //registros por pagina
		pageOptions: [10, 15, 20, 25, 50, 100], //opc registros por pagina
		filter: '',
		filterOn: [],
        filterOnNeg: [],
		routeName: null,
        okModal: false,
        hiddenModal: false,
        submitModal: false,
        loadData: true,
        dateFilter1: '',
        dateFilter2: '',
        cantidadFiter: null,
        conditionCantidadFilter: '',
        saveProcess: {
            active: false,
            textBadge: '',
            variantBadge: '',
            iconClass: ''
        },
        meses: [
            {idMes: 1, mes: 'Enero'},
            {idMes: 2, mes: 'Febrero'},
            {idMes: 3, mes: 'Marzo'},
            {idMes: 4, mes: 'Abril'},
            {idMes: 5, mes: 'Mayo'},
            {idMes: 6, mes: 'Junio'},
            {idMes: 7, mes: 'Julio'},
            {idMes: 8, mes: 'Agosto'},
            {idMes: 9, mes: 'Septiembre'},
            {idMes: 10, mes: 'Octubre'},
            {idMes: 11, mes: 'Noviembre'},
            {idMes: 12, mes: 'Diciembre'},
        ],
        editorComentario: null,
        formDelete: {
            dpto: null,
            section: null,
            id: null,
            registro: '',
            tipo: ''
        },
    },
    mutations: {
        setPermisosUsuario(state, permisos) {
            state.permissions = permisos
        },
        setStatusBusy(state) {
            state.isBusy = !state.isBusy
        },
        setEditor(state,editorComentario){
            state.editorComentario = editorComentario
        },
        setTotalRows(state, totalRows) { 
            state.totalRows = totalRows
        },
        newFilterOn(state, filterOn) {
            state.filterOn = filterOn
        },
        setFilter(state, filter) {
            state.filter = filter
        },
        setFilterOn(state, filterOn) {
            state.filterOn = filterOn
        },
        setFilterOnNegocios(state, filterOn) {
            state.filterOnNeg = filterOn
        },
        setCurrentPage(state, currentPage) {
            state.currentPage = currentPage
        },
        setIsBusy(state) {
            state.isBusy = !state.isBusy
        },
        setPerPage(state, perPage) {
            state.perPage = perPage
        },
        setFilterFields(state, filterFields) {
            state.filterFields = filterFields
        },
        resetFilterOn(state) {
            state.filterOn = []
        },
        setRouteName(state, name) {
            state.routeName = name
        },
        setUsuario(state, usuario) {
            state.usuario = usuario;
        },
        setNombreUsuario(state, nombre) {
            state.nombreUsuario = nombre;
        },
        modalOk(state, value) {
            state.okModal = value
        },
        modalHidden(state, value) {
            state.hiddenModal = value
        },
        modalSubmit(state, value) {
            state.submitModal = value
        },
        setFormDelete(state,value) {
            state.formDelete.dpto = value.dpto
            state.formDelete.section = value.section
            state.formDelete.id = value.id
            state.formDelete.registro = value.registro
            state.formDelete.tipo = value.tipo
        },
        setSaveProcess(state, value) {
            state.saveProcess.active = true
            state.saveProcess.textBadge = value.text
            state.saveProcess.iconClass = value.icon
            state.saveProcess.variantBadge = value.variant
        },
        setDateFilter1(state, value) {
            state.dateFilter1 = value
        },
        setDateFilter2(state, value) {
            state.dateFilter2 = value
        },
        setCantidadFilter(state, value) {
            state.cantidadFiter = value
        },
        setConditionCantidadFilter(state, value) {
            state.conditionCantidadFilter = value
        },
        setModuleActive(state, value) {
            state.moduleActive = value;
        }
    },
    actions: {},
    getters: {
        testFilterNumber(state)
        {
           return state.filterOnNeg.filter(
                filter => filter.type == 'number'
            );
        },
        testFilterDate(state)
        {
            return state.filterOnNeg.filter(
                filter => filter.type == 'date'
            );
        },
    },
    modules:{
        //ADMINISTRACION

        ad_cf_cuentas_externas,
        ad_estructura,
        ad_roles,

        //COMPRAS

        co_sc_solicitudes,
        co_sc_orden_compra,
        co_sc_recepcion_mercancia,
        co_conf_proveedores,
        co_conf_autorizaciones,

        //ALMACEN 

        alm_inventario_activos,
        alm_ss_solicitudes,

        //VENTAS

    	notifications,
        actividades,
        negocios,
        notas,
        clientes,
        contactos,
        usuarios,
        indicadores,
        presupuesto,
        embudos,
        vt_unidades_negocio,
        vt_proyecciones,

        //NORMATIVIDAD

        areas,
        puestos,
        organizacion,

        //SERVICIOS
        
        reporte_actividades,
        op_ra_indicadores_actividades,
        op_co_costos,
        op_co_cuenta_real,

        //SISTEMAS
        
        cuentas_electronicas,
        equipos_electronicos,
        licencias_software,

        //FINANZAS

        fn_cg_disposicion_efectivo,
        fn_cg_archivos,
        solicitudes_gastos,
        fn_cg_movimientos,
        fn_cg_comprobaciones,
        cnf_cuentas_contables,
        cnf_cuentas_internas,
        cnf_mano_obra,

        //SEGURIDAD

        historial_clinico,
        se_accidentes,

        //TICKETS

        tickets,
        indicators,
        services,
        tk_notifications,

        //PLANES

        planes,

        //VACACIONES Y PERMISOS

        th_areas,
        th_puestos,
        th_departamentos,
        th_motivos_ausencia,
        th_dias_festivos,
        th_empleados,
        th_vp_solicitudes,
    }
});