import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
// import '@fortawesome/fontawesome-free/css/all.css' 
// import '@fortawesome/fontawesome-free/js/all.js'

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Next we will register the CSRF Token as a common header with Axios so that
 * all outgoing HTTP requests automatically have it attached. This is just
 * a simple convenience so we don't have to attach every token manually.
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

import Echo from 'laravel-echo'

window.Pusher = require('pusher-js');

window.Echo = new Echo({
    // authEndpoint : 'http://smartconsulting.mx/sgc/public/broadcasting/auth',
    broadcaster: 'pusher',
    key: 'f27295ebfd8917a7fa4e',
    cluster: 'us2',
    encrypted: true 
});

//EHO COMPROBACIÓN DE GASTOS

window.EchoCG = new Echo({
    // authEndpoint : 'http://smartconsulting.mx/sgc/public/broadcasting/auth',
    broadcaster: 'pusher',
    key: 'f27295ebfd8917a7fa4e',
    cluster: 'us2',
    encrypted: true,
    namespace: 'App.Events.Finanzas.CG'
});

// ECHO SOLICITUD DE SUMINISTROS

window.EchoSS = new Echo({
    // authEndpoint : 'http://smartconsulting.mx/sgc/public/broadcasting/auth',
    broadcaster: 'pusher',
    key: 'f27295ebfd8917a7fa4e',
    cluster: 'us2',
    encrypted: true,
    namespace: 'App.Events.Compras.SS'
});

window.EchoTK = new Echo({
    // authEndpoint : 'http://smartconsulting.mx/sgc/public/broadcasting/auth',
    broadcaster: 'pusher',
    key: 'f27295ebfd8917a7fa4e',
    cluster: 'us2',
    encrypted: true,
    namespace: 'App.Events.Tickets'
});

// 
//Date & time format
window.moment = require('moment');

