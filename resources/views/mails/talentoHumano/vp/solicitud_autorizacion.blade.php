@component('mail::message')
# Hola {{ $user[0] . ',' }}

{{ $solicitante }} ha realizado una solicitud de {{ strtolower($solicitud->tipo) }}.

Requerimos de tu autorización para poder dar seguimiento a esta solicitud.

**Aquí tienes los detalles:**

@component('mail::panel')

<h2 style="margin-left: 15px; margin-top: 10px; color: #737373"> Solicitud #{{ $solicitud->idSolicitud }} </h2>

@if($solicitud->tipo == "Vacaciones")
- **Días que inician sus vacaciones: **
 - **Del: ** {{ $solicitud->fechaInicio }}
 - **al: ** {{ $solicitud->fechaFin }}
- **Días a disfrutar:** {{ $solicitud->dias }}
@endif

@if($solicitud->tipo == "Permiso")
- **Fecha: ** {{ $solicitud->fechaInicio }}
- **Horas:** {{ $solicitud->horas }}
- **Motivo de ausencia: ** {{ $solicitud->motivoAusencia }}
@endif

@if($solicitud->descripcion)
- **Descripción: ** {{ $solicitud->descripcion }}
@endif

@endcomponent

<center>
    <a class="button button-green mr-4 mt-2 mb-3" href="{{ config('app.url') }}/talentoHumano/solicitud/autorizacion/{{$solicitud->idSolicitud}}/autorizado">Autorizar</a>
    <a class="button button-red mb-4" href=" {{config('app.url') }}/talentoHumano/solicitud/autorizacion/{{$solicitud->idSolicitud}}/rechazado">Rechazar</a>
</center>

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent