@component('mail::message')
# Hola {{ $user[0] . ',' }}

Este correo es para notificarte que la solicitud ha sido autorizada por {{ $solicitud->autorizador }}.

**Detalles de la solicitud:**

@component('mail::panel')

@if($solicitud->tipo == "Vacaciones")
- **Días que inician sus vacaciones: **
 - **Del: ** {{ $solicitud->fechaInicio }}
 - **al: ** {{ $solicitud->fechaFin }}
- **Días a disfrutar:** {{ $solicitud->dias }}
@endif

@if($solicitud->tipo == "Permiso")
- **Fecha: ** {{ $solicitud->fechaInicio }}
- **Horas:** {{ $solicitud->horas }} hrs
- **Motivo de ausencia: ** {{ $solicitud->motivoAusencia }}
@endif

@endcomponent

@if($solicitud->comentarios)
**Comentarios: **

@component('mail::panel')
{{ $solicitud->comentarios->comentario }}
@endcomponent

@endif

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent