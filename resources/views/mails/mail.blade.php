@component('mail::message')

    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            {{ config('app.name') }}
        @endcomponent
    @endslot


    {{-- Body --}}
    <h1>Hola {{ $user[0] . ',' }} </h1> <br>
    <h2>Tienes las siguientes actividades para hoy: </h2>
    <ul>
        @foreach ($actividades as $actividad)
            <li>
                <div style="font-family: Arial; text-align: justify">
                    <span style="color: #000">
                        {{ $actividad->tipo }} - {{ $actividad->nombre_cliente }}
                        <span style="font-size:12px; ">{{ 'a las ' . $actividad->horaInicio }}</span><br>
                    </span>
                    {{ ' "' . $actividad->notas . '"' }}
                </div> <br>
            </li>
        @endforeach
    </ul>

    {{-- Action Button --}}
    @isset($actionText)
        {{-- Subcopy --}}
        @isset($actionText)
            @component('mail::subcopy')
                Si tienes problemas con con el botón "{{ $actionText }}"
                copia y pega la siguiente URL en tu navegador: [{{ $actionUrl }}]({!! $actionUrl !!})
            @endcomponent
        @endisset


        {{-- Footer --}}
        @slot('footer')
            @component('mail::footer')
                © {{ date('Y') }} {{ config('app.name') }}. Todos los derechos reservados.
            @endcomponent
        @endslot
    @endcomponent
