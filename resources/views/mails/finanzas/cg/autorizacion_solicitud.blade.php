@component('mail::message')
# Hola {{ $user[0] . ',' }}

{{ $solicitante }} ha realizado una solicitud de viático.

Requerimos de tu autorización para proceder con el seguimiento de esta solicitud.

**Aquí tienes los detalles:**

@component('mail::panel')

<h2 style="margin-left: 15px; margin-top: 10px; color: #737373"> Solicitud #{{ $solicitud->idSolicitud }} </h2>

- **Cuenta:** {{ $solicitud->cuenta }}
- **Destino del viático:** {{ $solicitud->destinoGasto }}
- **Objetivo del viático:** {{ $solicitud->objetivoGasto }}
- **Importe de la solicitud:** $ {{ $solicitud->importe_total }}

@endcomponent

<center>
    <a class="button button-green mr-4 mt-2 mb-3" href="{{ config('app.url') }}/finanzas/solicitud-gastos/autorizacion/{{$solicitud->idSolicitud}}/autorizado">Autorizar</a>
    <a class="button button-red mb-4" href=" {{config('app.url') }}/finanzas/solicitud-gastos/autorizacion/{{$solicitud->idSolicitud}}/rechazado">Rechazar</a>
</center>

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent
