@component('mail::message')
# Hola {{ $user[0] . ',' }}

@if($solicitud->idEtapa == 1)
Este correo es para notificarte que tu solicitud ha sido rechazada por {{$solicitud->autorizador}}.
@endif

@if($solicitud->idEtapa == 4)
Este correo es para notificarte que la comprobación de la solicitud #{{$solicitud->idSolicitud}} ha sido rechazada por {{$solicitud->autorizador}}.
@endif

@if($solicitud->comentarios)
**Comentarios: **

{{ $solicitud->comentarios->comentario }}
@endif

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent