@component('mail::message')
# Hola {{ $user[0] . ',' }}

{{ $asignador }} te ha asignado una solicitud de viático.

Requerimos de tu autorización para proceder con el seguimiento de la solicitud.

**Aquí tienes los detalles:**

@component('mail::panel')

<h2 style="margin-left: 15px; margin-top: 10px; color: #737373"> Solicitud #{{ $solicitud->idSolicitud }} </h2>

- **Cuenta:** {{ $solicitud->cuenta }}
- **Destino del viático:** {{ $solicitud->destinoGasto }}
- **Objetivo del viático:** {{ $solicitud->objetivoGasto }}
@if($solicitud->numeroPersonas == 1)
- **Viático para:** {{ $solicitud->numeroPersonas }} Persona 
@endif
@if($solicitud->numeroPersonas != 1)
- **Viático para:** {{ $solicitud->numeroPersonas }} Personas 
@endif
- **Periodo del viático:** {{ $solicitud->fechaInicioGasto }} al {{ $solicitud->fechaFinGasto }}
- **Importe solicitado:** $ {{ $solicitud->importe_total }}

@endcomponent

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent