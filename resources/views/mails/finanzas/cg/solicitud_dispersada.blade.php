@component('mail::message')
# Hola {{ $user[0] . ',' }}

Este correo es para notificarte que tu solicitud ha sido dispersada.

**Aquí tienes los detalles:**

@component('mail::panel')

<h2 style="color: #737373" class="ml-3 mt-4 mb-4"> Solicitud #{{ $solicitud->idSolicitud }} </h2>
- **Cuenta:** {{ $solicitud->cuenta }}
- **Destino del viático:** {{ $solicitud->destinoGasto }}
- **Objetivo del viático:** {{ $solicitud->objetivoGasto }}
- **Importe de la solicitud:** $ {{ $solicitud->importe_total }}

<h2 style="text-align: center; color: #737373" class="ml-3 mt-3 mb-1"> Resumen: </h2>
<hr style="margin-bottom: -16px; border-color: #ffffff57;">
@component('mail::table')
| Medio de dispersión | Solicitado     | Dispersado     |
|:-------------------:|:--------------:|:--------------:|
@foreach ($solicitud->resumen as $importe)
| {{ $importe->formaPago }} | $ {{ $importe->solicitado }} | $ {{ $importe->dispersado }}
@endforeach
@endcomponent

@endcomponent

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent
