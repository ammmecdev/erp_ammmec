@component('mail::message')
# Hola {{ $user[0] . ',' }}

{{ $solicitante }} ha realizado una solicitud de viático la cual fue autorizada por {{ $solicitud->autorizador }}.

Requerimos de tu tiempo para darle seguimiento a esta solicitud.

**Aquí tienes los detalles:**

@component('mail::panel')

<h2 style="color: #737373" class="ml-3 mt-4 mb-4"> Solicitud #{{ $solicitud->idSolicitud }} </h2>
- **Cuenta:** {{ $solicitud->cuenta }}
- **Destino del viático:** {{ $solicitud->destinoGasto }}
- **Objetivo del viático:** {{ $solicitud->objetivoGasto }}
- **Importe de la solicitud:** $ {{ $solicitud->importe_total }}

<h2 style="text-align: center; color: #737373" class="ml-3 mt-3 mb-1"> Resumen: </h2>
<hr style="margin-bottom: -16px; border-color: #ffffff57;">
@component('mail::table')
| Medio de dispersión | Importe         |
|:-------------------:|:---------------:|
@foreach ($solicitud->resumen as $importe)
| {{ $importe->formaPago }} | $ {{ $importe->solicitado }} |
@endforeach
@endcomponent

@endcomponent

@if($solicitud->comentarios)
**Comentarios adicionales: **

{{ $solicitud->comentarios->comentario }}

@endif


<center>
    <a class="button button-green mr-4 mt-2 mb-4" href="{{ config('app.url') }}/finanzas/solicitud-gastos/dispersion/{{$solicitud->idSolicitud}}">Dispersar solicitud</a>
</center>

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent
