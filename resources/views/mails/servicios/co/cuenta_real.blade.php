@component('mail::message')
# Hola {{ $user[0] . ',' }}

Este correo es para notificarte que cuentas con 5 días para cargar los consumibles correspondientes al costo operativo de la cuenta **{{ $data["cuenta"] }}**.

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent