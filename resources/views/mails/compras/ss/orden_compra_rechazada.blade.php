@component('mail::message')
# Hola {{ $user[0] . ',' }}

Este correo es para notificarte que la orden de compra #{{$orden->idOrdenCompra}} de la solicitud #{{$orden->idSolicitud}} ha sido rechazada por {{$orden->autorizador->nombreUsuario}}.

@if($orden->comentarios)
**Comentarios: **

{{ $orden->comentarios->comentario }}
@endif

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent