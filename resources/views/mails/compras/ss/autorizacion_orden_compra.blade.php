@component('mail::message')
# Hola {{ $user[0] . ',' }}

El departamento de compras ha realizado una orden de compra.

Requerimos de tu autorización para proceder con el seguimiento de esta orden de compra.

**Aquí tienes los detalles:**

@component('mail::panel')

<h2 style="margin-left: 15px; margin-top: 10px; color: #737373"> OC #{{ $orden->idOrdenCompra }} </h2>

- **Proveedor:** {{ $orden->nombre_proveedor }}
- **Fecha de entrega:** {{ $orden->fechaEntrega }}
- **Forma de pago:** {{ $orden->formaPago }}
- **Observaciones:** {{ $orden->observaciones }}

@endcomponent

<center>
    <a class="button button-green mr-4 mt-2 mb-3" href="{{ config('app.url') }}/compras/ordencompra/autorizacion/{{$orden->idOrdenCompra}}/autorizado">Autorizar</a>

    <a class="button button-red mb-4" href="{{ config('app.url') }}/compras/ordencompra/autorizacion/{{$orden->idOrdenCompra}}/rechazado">Rechazar</a>
</center>

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent