@component('mail::message')
# Hola {{ $user[0] . ',' }}

@component('mail::panel')

<h2 style="margin-left: 15px; margin-top: 10px; color: #737373"> Orden de compra #{{ $data['idOrdenCompra'] }} </h2>

- **Proveedor:** {{ $data['nombre_proveedor'] }}
- **Cuenta:** {{ $data['cuenta'] }}
- **Monto:** {{ '$'. $data['total'] }}
- **Método de pago:** **<label style="color: #219351; font-size: 16px">{{ $data['metodoPago'] }}</label>**

@if($data["metodoPago"] == "Crédito" || $data["metodoPago"] == "PPD")
- **Días de crédito:** {{ $data['diasCredito'] }}
- **Fecha estimada de pago:** **<label style="color: #dc3545; font-size: 16px"> 12 de Julio del 2022</label>**
@endif

@endcomponent

Agradecemos tu colaboración,<br>
{{ config('app.name') }}
@endcomponent