<table>
	<thead>
		<tr >
			<th style="width: 50px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Nombre</strong>
			</th>
			<th style="width: 45px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Razon social</strong>
			</th>
			<th style="width: 45px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Nombre comercial</strong>
			</th>
			<th style="width: 28px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>RFC</strong>
			</th>
			<th style="width: 75px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Dirección</strong>
			</th>
			<th style="width: 40px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Contacto</strong>
			</th>
			<th style="width: 22px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Teléfono</strong>
			</th>
			<th style="width: 22px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Días de crédito</strong>
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($proveedores as $proveedor)
			<tr>
				<td style="text-align: left;">{{ $proveedor->nombre }}</td>
				<td style="text-align: left;">{{ $proveedor->razonSocial }}</td>
				<td style="text-align: left;">{{ $proveedor->nombreComercial }}</td>
				<td style="text-align: center;">{{ $proveedor->rfc }}</td>
				<td style="text-align: left;">{{ $proveedor->direccion }}</td>
				<td style="text-align: left;">{{ $proveedor->contacto }}</td>
				<td style="text-align: center;">{{ $proveedor->telefono }}</td>
				<td style="text-align: center;">{{ $proveedor->diasCredito }}</td>
			</tr>
		@endforeach
	</tbody>
</table>