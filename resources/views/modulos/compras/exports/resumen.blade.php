<table>
	<thead>
		<tr >
			<th style="width: 28px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>N° de Solicitud de Suministro</strong>
			</th>
			<th style="width: 13px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Fecha de S.S.</strong>
			</th>
			<th style="width: 30px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>N° de Cuenta / Centro de Costo</strong>
			</th>
			<th style="width: 13px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>OC</strong>
			</th>
			<th style="width: 17px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Fecha de OC</strong>
			</th>
			<th style="width: 40px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Razón Social</strong>
			</th>
			<th style="width: 50px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Nombre Comercial</strong>
			</th>
			<th style="width: 180px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Descripción de Materiales</strong>
			</th>
			<th style="width: 22px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Unidad de Negocio</strong>
			</th>
			<th style="width: 16px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Autorización ERP</strong>
			</th>
			<th style="width: 39px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Fecha de Entrega Programada de Material</strong>
			</th>
			<th style="width: 24px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>N° de Factura / Remisión</strong>
			</th>
			<th style="width: 25px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Fecha de Factura</strong>
			</th>
			<th style="width: 22px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Monto Total de Factura</strong>
			</th>
			<th style="width: 15px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Tipo de Moneda</strong>
			</th>
			<th style="width: 18px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Forma de Pago</strong>
			</th>
			<th style="width: 14px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Días de Crédito</strong>
			</th>
			<th style="width: 15px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
				<strong>Método de Pago</strong>
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($resumen as $item)
			<tr>
				<td style="text-align: center;">{{ $item["idSolicitud"] }}</td>
				<td style="text-align: center;">{{ $item["fechaSolicitud"] }}</td>
				<td style="text-align: center;">{{ $item["cuenta"] }}</td>
				<td style="text-align: center;">{{ $item["idOrdenCompra"] }}</td>
				<td style="text-align: center;">{{ $item["fechaOrdenCompra"] }}</td>
				<td style="text-align: left;">{{ $item["razonSocial"] }}</td>
				<td style="text-align: left;">{{ $item["nombreComercial"] }}</td>
				<td style="text-align: left;">{{ $item["descripcionMateriales"] }}</td>
				<td style="text-align: center;">{{ $item["unidadNegocio"] }}</td>
				<td style="text-align: center;">{{ $item["autorizacion"] }}</td>
				<td style="text-align: center;">{{ $item["fechaEntrega"] }}</td>
				<td style="text-align: center;">{{ $item["folio"] }}</td>
				<td style="text-align: center;">{{ $item["fechaFactura"] }}</td>
				<td style="text-align: center;">{{ $item["totalFactura"] }}</td>
				<td style="text-align: center;">{{ $item["tipoMoneda"] }}</td>
				<td style="text-align: center;">{{ $item["formaPago"] }}</td>
				<td style="text-align: center;">{{ $item["diasCredito"] }}</td>
				<td style="text-align: center;">{{ $item["metodoPago"] }}</td>
			</tr>
		@endforeach
	</tbody>
</table>