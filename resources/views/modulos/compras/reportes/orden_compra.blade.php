<!DOCTYPE html>
<html>
<head>
	<title>Orden de compra</title>
	<style>
		body {
			margin: 0;
			font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
			font-size: 1rem;
			font-weight: 400;
			line-height: 1.5;
			color: rgb(69, 85, 95) !important;
			text-align: left;
			background-color: #fff;
		}
		.bg-light {
			background-color: rgb(159, 161, 164, .1) !important;
        	color: rgb(69, 85, 95) !important;
		}
		.bg-danger {
			background-color: rgb(192, 0 ,0) !important;
        	color: white !important;
		}
		.bg-secondary {
			background-color: #6c757d !important;
        	color: white !important;
		}
		.border-bottom {
			border-bottom: 1px solid rgb(192, 0 ,0) !important;
		}
		.text-danger {
			color: rgb(192, 0 ,0) !important;
		}
		.table {
			border-collapse: collapse !important;
/*			border: 1px solid #dee2e6 !important;*/
		}
		.table-bordered th,
		.table-bordered td {
			border: 1px solid #dee2e6;
		}
		small{
			font-size: 10px;
		}
		small.small{
			font-size: 9px !important;
		}
		small.small2{
			font-size: 8px !important;
		}
		td{
			padding: 5px 5px 5px 5px;
			margin: 0px;
		}
		.text-left {
			text-align: left !important;
		}
		.text-right {
			text-align: right !important;
		}
		.text-center {
			text-align: center !important;
		}
		.d-flex {
			display: -ms-flexbox !important;
			display: flex !important;
		}
		.justify-content-center {
			-ms-flex-pack: center !important;
			justify-content: center !important;
		}
		.row {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			margin-right: -15px;
			margin-left: -15px;
		}
		.p-0 {
			padding: 0 !important;
		}
		.pt-0,
		.py-0 {
			padding-top: 0 !important;
		}
		.pr-0,
		.px-0 {
			padding-right: 0 !important;
		}
		.pb-0,
		.py-0 {
			padding-bottom: 0 !important;
		}
		.pl-0,
		.px-0 {
			padding-left: 0 !important;
		}
		.p-1 {
			padding: 0.25rem !important;
		}
		.pt-1,
		.py-1 {
			padding-top: 0.25rem !important;
		}
		.pr-1,
		.px-1 {
			padding-right: 0.25rem !important;
		}
		.pb-1,
		.py-1 {
			padding-bottom: 0.25rem !important;
		}
		.pl-1,
		.px-1 {
			padding-left: 0.25rem !important;
		}
		.p-2 {
			padding: 0.5rem !important;
		}
		.pt-2,
		.py-2 {
			padding-top: 0.5rem !important;
		}
		.pr-2,
		.px-2 {
			padding-right: 0.5rem !important;
		}
		.pb-2,
		.py-2 {
			padding-bottom: 0.5rem !important;
		}
		.pl-2,
		.px-2 {
			padding-left: 0.5rem !important;
		}
		.p-3 {
			padding: 1rem !important;
		}
		.pt-3,
		.py-3 {
			padding-top: 1rem !important;
		}
		.pr-3,
		.px-3 {
			padding-right: 1rem !important;
		}
		.pb-3,
		.py-3 {
			padding-bottom: 1rem !important;
		}
		.pl-3,
		.px-3 {
			padding-left: 1rem !important;
		}
		.p-4 {
			padding: 1.5rem !important;
		}
		.pt-4,
		.py-4 {
			padding-top: 1.5rem !important;
		}
		.pr-4,
		.px-4 {
			padding-right: 1.5rem !important;
		}
		.pb-4,
		.py-4 {
			padding-bottom: 1.5rem !important;
		}
		.pl-4,
		.px-4 {
			padding-left: 1.5rem !important;
		}
		.p-5 {
			padding: 3rem !important;
		}
		.pt-5,
		.py-5 {
			padding-top: 3rem !important;
		}
		.pr-5,
		.px-5 {
			padding-right: 3rem !important;
		}
		.pb-5,
		.py-5 {
			padding-bottom: 3rem !important;
		}
		.pl-5,
		.px-5 {
			padding-left: 3rem !important;
		}
		table, th, td {
			border: 0.25rem solid #dee2e6;
			border-collapse: collapse;
			padding: 0.15rem;
		}
	</style>
</head>
<body>
	<div>
		<table class="table text-center" style="width: 100%">
			<tr>
				<td rowspan="2" colspan="2" class="text-left py-0">
					<img src="{{ public_path('img/logo/logoammmec.png')}}" style="width: 85px;">
				</td>
				<td rowspan="2" colspan="3" class="text-center" style="font-size: 12px;">
					<strong>ORDEN DE COMPRA</strong>
				</td>
				<td class="text-right" style="font-size: 8px;">
					<strong>FOLIO O.C.:</strong>
				</td>
				<td class="text-center" style="font-size: 8px;">
					{{ $orden['idOrdenCompra'] }}
				</td>
				<td class="text-right" style="font-size: 8px;">
					<strong>TIPO DE PEDIDO:</strong>
				</td>
				<td class="text-center" style="font-size: 8px;">
					{{ $orden['tipoPedido'] }}
				</td>
			</tr>
			<tr>
				<td class="text-right" style="font-size: 8px;"><strong>FECHA DOC.:</strong></td>
				<td class="text-center" colspan="2" style="font-size: 8px;">
					{{ $created_at }}
				</td>
				<td></td>
			</tr>
			<tr>
				<td class="text-left" colspan="5" style="font-size: 8px;">
					<strong>SOLICITA: </strong>{{ $solicitante }}
				</td>
				<td class="text-right" style="font-size: 8px;"><strong>CENTRO DE COSTO:</strong></td>
				<td class="text-center" style="font-size: 8px;">
					{{ $orden['centroCosto'] }}
				</td>
				<td class="text-right" style="font-size: 8px;"><strong>REF. COTIZACIÓN:</strong></td>
				<td class="text-center" style="font-size: 8px;">
					{{ $orden['referenciaCotizacion'] }}
				</td>
			</tr>

			<tr class="bg-danger" style="font-size: 8px;">
				<td colspan="9" style="width: 100%" class="text-center">
					DATOS PARA FACTURACIÓN Y ENVIÓ
				</td>
			</tr> 

			<tbody style="font-size: 8px;">
				<tr>
					<td class="text-left"><strong>Facturar a:</strong></td>
					<td colspan="2" class="text-left">AMMMEC</td>
					<td></td>
					<td class="text-left"><strong>Enviar CFDI a:</strong></td>
					<td colspan="3" class="text-left">compras_fresnillo@ammmec.com</td>
					<td></td>
				</tr>

				<tr>
					<td class="text-left"><strong>Domicilio:</strong></td>
					<td colspan="2" class="text-left">Industria Artesanal #12 Colonia Industrial, Fresnillo Zacatecas</td>
					<td></td>
					<td class="text-left"><strong>Uso de CFDI:</strong></td>
					<td colspan="3" class="text-left">
						{{ $orden['cfdi'] }}
					</td>
					<td></td>
				</tr>

				<tr>
					<td class="text-left"><strong>C.P.:</strong></td>
					<td colspan="2" class="text-left">99030</td>
					<td></td>
					<td class="text-left"><strong>Método de pago:</strong></td>
					<td colspan="3" class="text-left">
						{{ $orden['metodoPago'] }}
					</td>
					<td></td>
				</tr>

				<tr>
					<td class="text-left"><strong>RFC:</strong></td>
					<td colspan="2" class="text-left">AMM101126LJ1</td>
					<td></td>
					<td class="text-left"><strong>Forma de pago:</strong></td>
					<td colspan="3" class="text-left">
						{{ $orden['formaPago'] }}
					</td>
					<td></td>
				</tr>

				<tr>
					<td class="text-left"><strong>Régimen fiscal:</strong></td>
					<td colspan="2" class="text-left">601 General de Ley Personas Morales</td>
					<td></td>
					<td class="text-left"><strong>Horario recepción días hábiles</strong></td>
					<td colspan="3" class="text-left">Lunes a Sábado, 7:00 am - 2:30 pm</td>
					<td></td>
				</tr>

				<tr>
					<td colspan="2" class="text-left"><strong> Condiciones de recepción de mercancía: </strong></td>
					<td colspan="7" class="text-left">
						{{ $orden['condicionesRM'] }}
					</td>
				</tr>

				<tr class="bg-danger" style="font-size: 9px;">
					<td colspan="9" style="width: 100%" class="text-center">
						DATOS PROVEEDOR
					</td>
				</tr> 

				<tr>
					<td class="text-left"><strong>Nombre comercial:</strong></td>
					<td colspan="2" class="text-left">{{ $proveedor['nombre'] }}</td>
					<td></td>
					<td class="text-left"><strong>Nombre de contacto:</strong></td>
					<td colspan="3" class="text-left">
						{{ $proveedor['contacto'] }}
					</td>
					<td></td>
				</tr>

				<tr>
					<td class="text-left"><strong>Razón social:</strong></td>
					<td colspan="2" class="text-left">
						{{ $proveedor['razonSocial'] }}
					</td>
					<td></td>
					<td class="text-left"><strong>Teléfono de contacto:</strong></td>
					<td colspan="3" class="text-left">
						{{ $proveedor['telefono'] }}
					</td>
					<td></td>
				</tr>

				<tr>
					<td class="text-left"><strong>RFC:</strong></td>
					<td colspan="2" class="text-left">{{ $proveedor['rfc'] }}</td>
					<td></td>
					<td class="text-left"><strong>Correo electrónico:</strong></td>
					<td colspan="3" class="text-left">{{ $proveedor['email'] }}</td>
					<td></td>
				</tr>

				<tr class="bg-danger" style="font-size: 9px;">
					<td colspan="9" style="width: 100%" class="text-center">
						ARTÍCULOS SOLICITADOS
					</td>
				</tr>

				<tr class="bg-secondary" style="font-size: 9px;">
					<td class="text-left">ÍTEM</td>
					<td class="text-left">CÓDIGO DE MATERIAL</td>
					<td class="text-left">CANTIDAD</td>
					<td class="text-left">UNIDAD DE MEDIDA</td>
					<td colspan="2" class="text-left">DESCRIPCIÓN</td>
					<td class="text-left">PRECIO UNITARIO</td>
					<td class="text-left">DESCUENTO</td>
					<td class="text-left">SUBTOTAL</td>
				</tr> 

				<?php $pag = 1; $i = 0; ?>
				@foreach ($articulos as $articulo)
					<?php
						$i++; 
						$costoUnitario = number_format(
							floatval($articulo['costoUnitario']),2,".",","
						);
						$totalArt = floatval($articulo['cantidadOC']) * floatval($articulo['costoUnitario']);
						$importeDescuento = (floatval($articulo['descuento']) / 100) * $totalArt;
						$totalArt = $totalArt - $importeDescuento;
						$totalArt = number_format($totalArt,2,".",",");
					?>

					<tr style="font-size: 8px;">
						<td><strong>{{ $i }}</strong></td>
						<td>{{ $articulo['codigo'] }}</td>
						<td>{{ $articulo['cantidadOC'] }}</td>
						<td>{{ $articulo['unidad'] }}</td>
						<td colspan="2">{{ $articulo['descripcion'] }}</td>
						<td>$ {{ $costoUnitario }}</td>
						<td>{{ $articulo['descuento'] }} %</td>
						<td>$ {{ $totalArt }}</td>
					</tr>

					{{-- @if(($pag == 1 && $i == 10) || ($pag > 1 && $i==18))
						<tr>
							<td colspan="2">
								<div style="page-break-after:always;"></div>
								<?php $pag++; $i=0; ?>
							</td>
						</tr>		
					@endif  --}}
				@endforeach

				<tr>
					<td class="text-left"><strong>OBSERVACIONES</strong></td>
					<td colspan="8"></td>
				</tr> 

				<tr>
					<td rowspan="3" colspan="6" class="text-left bg-light">
						{{ $orden['observaciones'] }}
					</td>
					<td class="text-left"><strong>MONEDA:</strong> {{ $orden['tipoMoneda'] }}</td>
					<td colspan="2"></td>
				</tr> 

				<tr>
					<td></td>
					<td class="text-left"><strong>SUBTOTAL ({{ $orden['tipoMoneda'] }}):</strong></td>
					<td class="text-center"> $ {{ $subTotal }} </td>
				</tr> 

				<tr>
					<td></td>
					<td class="text-left"><strong>DESCUENTO:</strong></td>
					<td class="text-center"> $ {{ $montoDescuento }} </td>
				</tr> 

				<tr>
					<td></td>
					<td colspan="2" class="text-center"><strong>COORDINADOR DE COMPRAS</strong></td>
					<td></td>
					<td colspan="2" class="text-center"><strong>AUTORIZADOR</strong></td>
					<td></td>
					<td class="text-left"><strong>RETENCIÓN I.V.A.:</strong></td>
					<td> $ {{ $retencionIVA }} </td>
				</tr>

				<tr>
					<td></td>
					<td rowspan="2" colspan="2" class="text-center" style="padding-bottom: 0px !important; vertical-align: bottom !important; border-bottom: 2px solid #fff !important;">
						{{ $coordinadorCompras }}
					</td>
					<td></td>
					<td rowspan="2" colspan="2" class="text-center" style="padding-bottom: 0px !important; vertical-align: bottom !important; border-bottom: 2px solid #fff !important;">
						{{ $autorizador }}
					</td>
					<td></td>
					<td class="text-left"><strong>I.V.A.:</strong></td>
					<td class="text-center">$ {{ $iva }}</td>
				</tr>

				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td class="text-left"><strong>RETENCIÓN I.S.R.:</strong></td>
					<td> $ {{ $retencionISR }} </td>
				</tr>

				<tr>
					<td></td>
					<td colspan="2" class="text-center">
						<hr style="margin-top: 0px;"><strong>NOMBRE Y FIRMA</strong>
					</td>
					<td></td>
					<td colspan="2" class="text-center">
						<hr style="margin-top: 0px;"><strong>NOMBRE Y FIRMA </strong>
					</td>
					<td></td>
					<td class="text-left"><strong>OTROS IMPUESTOS:</strong></td>
					<td> $ {{ $otrosImpuestos }} </td>
				</tr>

				<tr>
					<td></td>
					<td colspan="2"></td>
					<td></td>
					<td colspan="2"></td>
					<td></td>
					<td class="text-left"><strong>TOTAL ({{ $orden['tipoMoneda'] }}):</strong></td>
					<td class="text-center">$ {{ $total }}</td>
				</tr>

				<tr>
					<td colspan="9" class="text-left">
						<strong>NOTAS: </strong><br>
						La empresa no se responsabiliza del pago de las ordenes de compra que no estén firmadas o autorizadas  y/o sin confirmación de recepción del responsable de nuestro almacén. <br>

						{{-- Se permiten entregas parciales o no. <br>
						Datos del usuario para aclaraciones. <br>	 --}}						
						Condiciones de entrega: Favor de entregar factura y orden de compra junto con la mercancía, en días hábiles de lunes a sábado de  7:00 a.m. a 2:30 p.m. en nuestro almacén con original y copia y enviar documentos con firmas autorizadas al correo compras_fresnillo@ammmec.com. <br>

						<strong>AVISO DE PRIVACIDAD</strong>								
					</td>
				</tr>
			</tbody>

		</table>
	</div>
</body>
</html>