 @extends('layout')
 @section('content')
 <link rel="stylesheet" href="{{ asset('css/sidebar/style5.css') }}">

 <div class="wrapper">
 	<!-- Sidebar Holder -->
 	
 	@include('modules.services.costs.sidebar')

 	<!-- Page Content Holder -->
 	<div id="content">

 		<nav class="navbar navbar-expand-lg navbar-light mb-3">
 			<div class="container-fluid">

 				<button type="button" id="sidebarCollapse" class="navbar-btn bg-white">
 					<span></span>
 					<span></span>
 					<span></span>
 				</button>
 				<button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 					<i class="fas fa-align-justify"></i>
 				</button>

 				<h1 class="display-1 pt-2" style="font-size: 35px;">Cotización</h1>

 				<div class="collapse navbar-collapse" id="navbarSupportedContent">
 					
 				</div>
 			</div>
 			<div align="right" class="col-4">
 				<button type="button" class="btn btn-outline-secondary btn-xs" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-check-circle"></i> Terminar</button>
 				<button type="button" class="btn btn-outline-secondary btn-xs" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-print"></i> Imprimir</button>
				<button type="button" class="btn btn-outline-secondary btn-xs" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-share"></i> Enviar</button>


 			</div>
 		</nav>

 		<div class="card">
 			
 			<div class="card-body">
 				<div class="ml-0 mr-0">
 					<div class="row">
 						<div class="col-lg-2">
 							<p class="h6 pt-1" align="right"><strong>Proyecto:</strong></p>
 						</div>
 						<div class="col-lg-8 mt-0 bg-light pt-2">
 							<p class="h5" align="center">Servicio anual Molino B</p>

 							<p class="h4" align="center">FRE-0001-FTA</p>
 						</div>
 					</div>

 					<div class="row">
 						<div class="col-lg-6">
 							<div class="row pt-3">
 								<div class="col-xs-6 col-lg-4" align="right">
 									<p class="h6"><strong>Cliente:</strong></p>
 								</div>
 								<div class="col-xs-6 col-lg-8" align="left">
 									<p class="h6">Minera fresnillo S.A de C.V</p>
 								</div>
 							</div>
 							<div class="row">
 								<div class="col-xs-6 col-lg-4" align="right">
 									<p class="h6"><strong>Fecha de Inicio:</strong></p>
 								</div>
 								<div class="col-xs-6 col-lg-8" align="left">
 									<p class="h6">15 de julio del 2019</p>
 								</div>
 							</div>
 							<div class="row">
 								<div class="col-xs-6 col-lg-4" align="right">
 									<p class="h6"><strong>Fecha de Término:</strong></p>
 								</div>
 								<div class="col-xs-6 col-lg-8" align="left">
 									<p class="h6">25 de julio del 2019</p>
 								</div>
 							</div>

 						</div>
 						<div class="col-lg-6 mt-3">
 							<table class="table table-bordered" style="margin-bottom: 0px;">
 								<tfoot>
 									<tr>
 										<td align="center"><strong>Duración del Proyecto</strong></td>
 										<td align="center" class="bg-light"><strong>4</strong></td>
 									</tr>
 									<tr>
 										<td align="center"><strong>Jornada Laboral (Hrs)</strong></td>
 										<td align="center" class="bg-light"><strong>12</strong></td>
 									</tr>
 								</tfoot>
 							</table>
 						</div>
 					</div>
 					<div class="row">
 						<div class="col-lg-8">
 							<div class="row">
 								<div class="col-lg-4 pt-2" align="center" style="color: white;">
 									<h1 style="font-size: 25px" class="display-5 text-danger pt-2">TOTAL PLANEADO</h1>
 								</div>
 								<div class="col-lg-4 pt-2 bg-light " align="center">
 									<div class="row">
 										<div class="col-xs-1 col-lg-1">
 											<h4 class="pt-2"><strong>$</strong></h4>
 										</div>
 										<div class="col-xs-10 col-lg-10" align="right">
 											<h4 class="h2"><strong>71,707.00</strong></h4>
 										</div>
 									</div>
 								</div>
 							</div>
 						</div>
 					</div>
 				</div>
 			</div>
 		</div>

 		<div class="card mt-3">
 			<div class="card-header">
 				<ul class="nav nav-pills nav-fill mb-0" id="pills-tab" role="tablist">
 					<li class="nav-item">
 						<a class="nav-link active" id="pills-mat-ins-tab" data-toggle="pill" href="#pills-mat-ins" role="tab" aria-controls="pills-mat-ins" aria-selected="true">Material e insumos</a>
 					</li>
 					<li class="nav-item">
 						<a class="nav-link" id="pills-mano-obra-tab" data-toggle="pill" href="#pills-mano-obra" role="tab" aria-controls="pills-mano-obra" aria-selected="false">Mano de obra</a>
 					</li>
 					<li class="nav-item">
 						<a class="nav-link" id="pills-herr-equi-tab" data-toggle="pill" href="#pills-herr-equi" role="tab" aria-controls="pills-herr-equi" aria-selected="false">Herramienta y equipo</a>
 					</li>
 					<li class="nav-item">
 						<a class="nav-link" id="pills-viaticos-tab" data-toggle="pill" href="#pills-viaticos" role="tab" aria-controls="pills-viaticos" aria-selected="false">Gastos de operación</a>
 					</li>
 				</ul>
 			</div>
 			<div class="card-body pt-0">
 				<div class="tab-content" id="pills-tabContent">
 					<div class="tab-pane fade show active" id="pills-mat-ins" role="tabpanel" aria-labelledby="pills-mat-ins-tab">
 						<div class="row">
 							<div class="col-lg-12 mt-2 pt-2">
 								<h1 class="display-1" style="font-size: 35px;" align="center">Materiales e insumos</h1>
 								<div align="right" class="mb-2" style="margin-top: -25px;">
 									<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#materialesInsumosModal" >Añadir <i class="fas fa-plus"></i></button>
 								</div>
 							</div>
 						</div>
 						<div class="table-responsive">
 							<table class="table table-hover table-bordered">
 								<thead>
 									<tr class="bg-secondary text-white">
 										<td align="center"><strong>Item</strong></td>
 										<td align="center"><strong>Cantidad</strong></td>
 										<td align="center"><strong>Unidad</strong></td>
 										<td align="center"><strong>Descripción</strong></td>
 										<td align="center"><strong>Costo Unitario</strong></td>
 										<td align="center"><strong>Costo Total</strong></td>
 									</tr>
 								</thead>
 								<tr>
 									<td align="center"><strong>1</strong></td>
 									<td align="center">4</td>
 									<td align="center">pza</td>
 									<td align="center">Material</td>
 									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
 									<td class="pt-0 pb-0">
 										<table class="table table-borderless" style="margin-bottom: 0px;">
 											<thead>
 												<tr>
 													<td><strong>$</strong></td>
 													<td></td>
 													<td align="right"><strong>1,800.00</strong></td>
 												</tr>
 											</thead>
 										</table>
 									</td>
 								</tr>	
 								<tr>
 									<td align="center"><strong>2</strong></td>
 									<td align="center">4</td>
 									<td align="center">pza</td>
 									<td align="center">Insumo</td>
 									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
 									<td class="pt-0 pb-0">
 										<table class="table table-borderless" style="margin-bottom: 0px;">
 											<thead>
 												<tr>
 													<td><strong>$</strong></td>
 													<td></td>
 													<td align="right"><strong>1,800.00</strong></td>
 												</tr>
 											</thead>
 										</table>
 									</td>
 								</tr>	
 								<tr>
 									<td align="center"><strong>3</strong></td>
 									<td align="center">4</td>
 									<td align="center">pza</td>
 									<td align="center">Material</td>
 									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
 									<td class="pt-0 pb-0">
 										<table class="table table-borderless" style="margin-bottom: 0px;">
 											<thead>
 												<tr>
 													<td><strong>$</strong></td>
 													<td></td>
 													<td align="right"><strong>1,800.00</strong></td>
 												</tr>
 											</thead>
 										</table>
 									</td>
 								</tr>	
 								<tr>
 									<td align="center"><strong>4</strong></td>
 									<td align="center">4</td>
 									<td align="center">pza</td>
 									<td align="center">Insumo</td>
 									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
 									<td class="pt-0 pb-0">
 										<table class="table table-borderless" style="margin-bottom: 0px;">
 											<thead>
 												<tr>
 													<td><strong>$</strong></td>
 													<td></td>
 													<td align="right"><strong>1,800.00</strong></td>
 												</tr>
 											</thead>
 										</table>
 									</td>
 								</tr>	
 							</tbody>
 							<tfoot>
 								<tr>
 									<td colspan="4"></td>
 									<td class="bg-secondary text-white" align="right" style="padding-top: 15px;"><strong>Total</strong></td>
 									<td class="bg-secondary text-white pt-0 pb-0">
 										<table class="table table-borderless  text-white" style="margin-bottom: 0px;">
 											<thead>
 												<tr class="bg-secondary">
 													<td><strong>$</strong></td>
 													<td></td>
 													<td align="right" ><strong>1,800.00</strong></td>
 												</tr>
 											</thead>
 										</table>
 									</td>
 								</tr>
 							</tfoot>
 						</table>
 					</div>
 				</div>
 				<div class="tab-pane fade" id="pills-mano-obra" role="tabpanel" aria-labelledby="pills-mano-obra-tab">
 					<div class="row">
 						<div class="col-lg-12 mt-2 pt-2">
 							<h1 class="display-1" style="font-size: 35px;" align="center">Mano de obra</h1>
 							<div align="right" class="mb-2" style="margin-top: -25px;">
 								<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#manoObraModal" >Añadir <i class="fas fa-plus"></i></button>
 							</div>
 						</div>
 					</div>
 					<div class="table-responsive">
 						<table class="table table-hover table-bordered">
 							<thead>
 								<tr class="bg-secondary text-white">
 									<td align="center"><strong>Item</strong></td>
 									<td align="center"><strong>Cantidad</strong></td>
 									<td><strong>Puesto</strong></td>
 									<td align="center"><strong>Costo Unitario</strong></td>
 									<td align="center"><strong>Costo Total</strong></td>
 								</tr>
 							</thead>
 							<tr>
 								<td align="center"><strong>1</strong></td>
 								<td align="center">1</td>
 								<td>Subgerente de Mantenimiento Predictivo</td>
 								<td class="pt-0 pb-0">
 									<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 										<tr>
 											<td>$</td>
 											<td></td>
 											<td align="right">1,800.00</td>
 										</tr>

 									</table>
 								</td>
 								<td class="pt-0 pb-0">
 									<table class="table table-borderless" style="margin-bottom: 0px;">
 										<thead>
 											<tr>
 												<td><strong>$</strong></td>
 												<td></td>
 												<td align="right"><strong>1,800.00</strong></td>
 											</tr>
 										</thead>
 									</table>
 								</td>
 							</tr>	
 							<tr>
 								<td align="center"><strong>2</strong></td>
 								<td align="center">1</td>
 								<td>Auxiliar Técnico Mtto. Predictivo</td>
 								<td class="pt-0 pb-0">
 									<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 										<tr>
 											<td>$</td>
 											<td></td>
 											<td align="right">1,800.00</td>
 										</tr>

 									</table>
 								</td>
 								<td class="pt-0 pb-0">
 									<table class="table table-borderless" style="margin-bottom: 0px;">
 										<thead>
 											<tr>
 												<td><strong>$</strong></td>
 												<td></td>
 												<td align="right"><strong>1,800.00</strong></td>
 											</tr>
 										</thead>
 									</table>
 								</td>
 							</tr>	
 							
 						</tbody>
 						<tfoot>
 							<tr>
 								<td colspan="3"></td>
 								<td class="bg-secondary text-white" align="right" style="padding-top: 15px;"><strong>Total</strong></td>
 								<td class="bg-secondary text-white pt-0 pb-0">
 									<table class="table table-borderless  text-white" style="margin-bottom: 0px;">
 										<thead>
 											<tr class="bg-secondary">
 												<td><strong>$</strong></td>
 												<td></td>
 												<td align="right" ><strong>1,800.00</strong></td>
 											</tr>
 										</thead>
 									</table>
 								</td>
 							</tr>
 						</tfoot>
 					</table>
 				</div>
 			</div>
 			<div class="tab-pane fade" id="pills-herr-equi" role="tabpanel" aria-labelledby="pills-herr-equi-tab">
 				<div class="row">
 					<div class="col-lg-12 mt-2 pt-2">
 						<h1 class="display-1" style="font-size: 35px;" align="center">Herramienta y equipo</h1>
 						<div align="right" class="mb-2" style="margin-top: -25px;">
 							<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#herramientaEquipoModal" >Añadir <i class="fas fa-plus"></i></button>
 						</div>
 					</div>
 				</div>
 				<div class="table-responsive">
 					<table class="table table-hover table-bordered">
 						<thead>
 							<tr class="bg-secondary text-white">
 								<td align="center"><strong>Item</strong></td>
 								<td align="center"><strong>Cantidad</strong></td>
 								<td align="center"><strong>Unidad</strong></td>
 								<td align="center"><strong>Descripción</strong></td>
 								<td align="center"><strong>Costo Unitario</strong></td>
 								<td align="center"><strong>Costo Total</strong></td>
 							</tr>
 						</thead>
 						<tr>
 							<td align="center"><strong>1</strong></td>
 							<td align="center">1</td>
 							<td align="center">Hr</td>
 							<td align="center">Torno B</td>
 							<td class="pt-0 pb-0">
 								<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 									<tr>
 										<td>$</td>
 										<td></td>
 										<td align="right">1,800.00</td>
 									</tr>

 								</table>
 							</td>
 							<td class="pt-0 pb-0">
 								<table class="table table-borderless" style="margin-bottom: 0px;">
 									<thead>
 										<tr>
 											<td><strong>$</strong></td>
 											<td></td>
 											<td align="right"><strong>1,800.00</strong></td>
 										</tr>
 									</thead>
 								</table>
 							</td>
 						</tr>	
 						<tr>
 							<td align="center"><strong>2</strong></td>
 							<td align="center">1</td>
 							<td align="center">Hr</td>
 							<td align="center">Toyota Hilux</td>
 							<td class="pt-0 pb-0">
 								<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 									<tr>
 										<td>$</td>
 										<td></td>
 										<td align="right">1,800.00</td>
 									</tr>

 								</table>
 							</td>
 							<td class="pt-0 pb-0">
 								<table class="table table-borderless" style="margin-bottom: 0px;">
 									<thead>
 										<tr>
 											<td><strong>$</strong></td>
 											<td></td>
 											<td align="right"><strong>1,800.00</strong></td>
 										</tr>
 									</thead>
 								</table>
 							</td>
 						</tr>	

 					</tbody>
 					<tfoot>
 						<tr>
 							<td colspan="4"></td>
 							<td class="bg-secondary text-white" align="right" style="padding-top: 15px;"><strong>Total</strong></td>
 							<td class="bg-secondary text-white pt-0 pb-0">
 								<table class="table table-borderless  text-white" style="margin-bottom: 0px;">
 									<thead>
 										<tr class="bg-secondary">
 											<td><strong>$</strong></td>
 											<td></td>
 											<td align="right" ><strong>1,800.00</strong></td>
 										</tr>
 									</thead>
 								</table>
 							</td>
 						</tr>
 					</tfoot>
 				</table>
 			</div>
 		</div>
 		<div class="tab-pane fade" id="pills-viaticos" role="tabpanel" aria-labelledby="pills-viaticos-tab">
 			<div class="row">
 				<div class="col-lg-12 mt-2 pt-2">
 					<h1 class="display-1" style="font-size: 35px;" align="center">Gastos de operación</h1>
 					<div align="right" class="mb-2" style="margin-top: -25px;">
 						<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#gastosOperacionModal" >Añadir <i class="fas fa-plus"></i></button>
 					</div>
 				</div>
 			</div>
 			<div class="table-responsive">
 				<table class="table table-hover table-bordered">
 					<thead>
 						<tr class="bg-secondary text-white">
 							<td align="center"><strong>Item</strong></td>
 							<td align="center"><strong>Cantidad</strong></td>
 							<td align="center"><strong>Unidad</strong></td>
 							<td align="center"><strong>Descripción</strong></td>
 							<td align="center"><strong>Costo Unitario</strong></td>
 							<td align="center"><strong>Costo Total</strong></td>
 						</tr>
 					</thead>
 					<tr>
 						<td align="center"><strong>1</strong></td>
 						<td align="center">1</td>
 						<td align="center">Servicio</td>
 						<td align="center">Alimentos</td>
 						<td class="pt-0 pb-0">
 							<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 								<tr>
 									<td>$</td>
 									<td></td>
 									<td align="right">1,800.00</td>
 								</tr>

 							</table>
 						</td>
 						<td class="pt-0 pb-0">
 							<table class="table table-borderless" style="margin-bottom: 0px;">
 								<thead>
 									<tr>
 										<td><strong>$</strong></td>
 										<td></td>
 										<td align="right"><strong>1,800.00</strong></td>
 									</tr>
 								</thead>
 							</table>
 						</td>
 					</tr>	
 					<tr>
 						<td align="center"><strong>2</strong></td>
 						<td align="center">1</td>
 						<td align="center">Servicio</td>
 						<td align="center">Hospedajes</td>
 						<td class="pt-0 pb-0">
 							<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 								<tr>
 									<td>$</td>
 									<td></td>
 									<td align="right">1,800.00</td>
 								</tr>

 							</table>
 						</td>
 						<td class="pt-0 pb-0">
 							<table class="table table-borderless" style="margin-bottom: 0px;">
 								<thead>
 									<tr>
 										<td><strong>$</strong></td>
 										<td></td>
 										<td align="right"><strong>1,800.00</strong></td>
 									</tr>
 								</thead>
 							</table>
 						</td>
 					</tr>	

 				</tbody>
 				<tfoot>
 					<tr>
 						<td colspan="4"></td>
 						<td class="bg-secondary text-white" align="right" style="padding-top: 15px;"><strong>Total</strong></td>
 						<td class="bg-secondary text-white pt-0 pb-0">
 							<table class="table table-borderless  text-white" style="margin-bottom: 0px;">
 								<thead>
 									<tr class="bg-secondary">
 										<td><strong>$</strong></td>
 										<td></td>
 										<td align="right" ><strong>1,800.00</strong></td>
 									</tr>
 								</thead>
 							</table>
 						</td>
 					</tr>
 				</tfoot>
 			</table>
 		</div>
 	</div>
 </div>

</div>


</div>

<div class="card mt-3">
	<div class="card-header pb-1">
		<p class="h4 pt-2">Observaciones</p>
	</div>
	<div class="card-body">
		<table class="table table-hover table-bordered">

			<tr class="bg-secondary text-white">
				<td align="center"><strong>Item</strong></td>
				<td align="center"><strong>Observación</strong></td>
			</tr>

			<tr>
				<td align="center">1</td>
				<td>Esta es una observación para explicar algo</td>
			</tr>
			<tr>
				<td align="center">2</td>
				<td>Esto es otro tipo de observación para explicar otro tipo de cosas que se enlistarán en la cotización</td>
			</tr>
		</div>
	</div>
</div>

</div>
</div>

<!-- Material Insumos Modal -->
<div class="modal fade" id="materialesInsumosModal" tabindex="-1" role="dialog" aria-labelledby="materialesInsumosModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="materialesInsumosModalCenterTitle"><strong>Materiales e insumos</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label><strong>Cantidad</strong></label>
							<input class="form-control" type="text" placeholder="0">
						</div>
						<div class="col-sm-6 form-group">
							<label><strong>Unidad de medida</strong></label>
							<select class="form-control">
								<option value="">Seleccione</option>

								<option value="">PZA</option>
								<option value="">JUEGO</option>
								<option value="">LTS</option>
								<option value="">KG</option>

							</select>
						</div>
					</div>
					<div class="form-group">
						<label><strong>Material o insumo</strong></label>
						<select class="form-control">
							<option value="">Seleccione</option>

							<option value="">PZA</option>
							<option value="">JUEGO</option>
							<option value="">LTS</option>
							<option value="">KG</option>

						</select>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group"> 
							<label><strong>Costo unitario</strong></label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
								</div>
								<input type="text" class="form-control text-right" placeholder="0.00" aria-label="Username" aria-describedby="basic-addon1" align="">
							</div>
						</div>
						<div class="col-sm-6 form-group">
							<label><strong>Costo total</strong></label>
							
							<div class="bg-light pt-1 pb-1">
								<div class="row">
									<div class="col-2">
										<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
									</div>
									<div class="col-10">
										<p class="h5 pt-1 pr-0 col-10" align="right" ><strong>1,800.00</strong></p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-danger">Guardar</button>
			</div>
		</div>
	</div>
</div>

<!-- Mano Obra Modal -->
<div class="modal fade" id="manoObraModal" tabindex="-1" role="dialog" aria-labelledby="manoObraModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="manoObraModalCenterTitle"><strong>Mano de obra</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label><strong>Item</strong></label>
							<input class="form-control" type="text" placeholder="0">
						</div>
						<div class="col-sm-6 form-group">
							<label><strong>Cantidad</strong></label>
							<input class="form-control" id="txtCantidad" type="number" placeholder="0" name="cantidad">
						</div>
					</div>
					<div class="form-group">
						<label><strong>Puesto</strong></label>
						<select class="form-control">
							<option value="">Seleccione</option>
							<option value="">Gerente de operaciones</option>
							<option value="">Tec. Esp. C</option>
						</select>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group"> 
							<label><strong>Costo unitario</strong></label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
								</div>
								<input type="text" class="form-control text-right" placeholder="0.00" aria-label="Username" aria-describedby="basic-addon1" align="">
							</div>
						</div>
						<div class="col-sm-6 form-group">
							<label><strong>Costo total</strong></label>
							
							<div class="bg-light pt-1 pb-1">
								<div class="row">
									<div class="col-2">
										<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
									</div>
									<div class="col-10">
										<p class="h5 pt-1 pr-0 col-10" align="right" ><strong>1,800.00</strong></p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-danger">Guardar</button>
			</div>
		</div>
	</div>
</div>

<!-- Herramienta Equipo Modal -->
<div class="modal fade" id="herramientaEquipoModal" tabindex="-1" role="dialog" aria-labelledby="herramientaEquipoModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="herramientaEquipoModalCenterTitle"><strong>Herramienta y equipo</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label><strong>Cantidad</strong></label>
							<input class="form-control" id="txtCantidad" type="number" placeholder="0">
						</div>
						<div class="col-sm-6 form-group">
							<label><strong>Unidad</strong></label>
							<select class="form-control" id="txtUnidad" type="text" name="unidad">
								<option value="">Hora</option>
								<option value="">Turno</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label><strong>Descripción</strong></label>
						<select class="form-control">
							<option value="">Seleccione</option>
							<option value="">Torno B</option>
							<option value="">Toyota Hilux</option>
						</select>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group"> 
							<label><strong>Costo unitario</strong></label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
								</div>
								<input type="text" class="form-control text-right" placeholder="0.00" aria-label="Username" aria-describedby="basic-addon1" align="">
							</div>
						</div>
						<div class="col-sm-6 form-group">
							<label><strong>Costo total</strong></label>
							
							<div class="bg-light pt-1 pb-1">
								<div class="row">
									<div class="col-2">
										<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
									</div>
									<div class="col-10">
										<p class="h5 pt-1 pr-0 col-10" align="right" ><strong>1,800.00</strong></p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-danger">Guardar</button>
			</div>
		</div>
	</div>
</div>

<!-- Gastos de operación Modal -->
<div class="modal fade" id="gastosOperacionModal" tabindex="-1" role="dialog" aria-labelledby="gastosOperacionModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="gastosOperacionModalCenterTitle"><strong>Gastos de operación</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label><strong>Cantidad</strong></label>
							<input class="form-control" id="txtCantidad" type="number" placeholder="0">
						</div>
						<div class="col-sm-6 form-group">
							<label><strong>Unidad</strong></label>
							<select class="form-control" id="txtUnidad" type="text" name="unidad">
								<option value="">Servicio</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label><strong>Descripción</strong></label>
						<select class="form-control">
							<option value="">Alimentos</option>
							<option value="">Hospedajes</option>
							<option value="">Casetas</option>
							<option value="">Combustibles</option>
							<option value="">Fletes</option>
							<option value="">Capacitaciones</option>
						</select>
					</div>
					<div><p class="text-danger">Falta por definir la seleccion de la base de datos de precios despues de elegir el tipo de gasto</p></div>
					<div class="row">
						<div class="col-sm-6 form-group"> 
							<label><strong>Costo unitario</strong></label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
								</div>
								<input type="text" class="form-control text-right" placeholder="0.00" aria-label="Username" aria-describedby="basic-addon1" align="">
							</div>
						</div>
						<div class="col-sm-6 form-group">
							<label><strong>Costo total</strong></label>
							<div class="bg-light pt-1 pb-1">
								<div class="row">
									<div class="col-2">
										<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
									</div>
									<div class="col-10">
										<p class="h5 pt-1 pr-0 col-10" align="right" ><strong>1,800.00</strong></p>
									</div>
								</div>
							</div>

						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-danger">Guardar</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function () {
		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').toggleClass('active');
			$(this).toggleClass('active');
		});
	});
</script>
@endsection