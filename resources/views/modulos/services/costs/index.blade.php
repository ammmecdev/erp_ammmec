@extends('layout')
@section('content')
<style>
td, th{
	padding-top:0.45rem !important;
	padding-bottom:0.45rem !important;

}
</style>
<!--Encabezado-->
<div class="card mt-3">
	<div class="card-body">
		<h1 class="display-1 my-3" style="font-size: 40px;" align="center">Lista de Cotizaciones</h1>
						{{-- <div align="right" style="margin-top: -20px;" class="mb-2">
							<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#exampleModal">Añadir <i class="fas fa-plus"></i></button>
						</div> --}}
	</div>
</div>
<!--Encabezado-->

<div class="card mt-3">
	<div class="card-body">
		<div class="ml-0 mr-0">
			<div class="row">
				{{-- <div class="col-md-2 col-lg-2">
					<ul class="list-group2 buttons pl-0">
						<li class="list-group-item bg-light">
							<h1 class="display-1 mt-2" style="font-size: 25px">Ver</h1>
						</li>
						<li class="list-group-item">
							<input id="radiobtn_1" class="radiobtn" name="filter" type="radio" value="All" tabindex="1" checked>
							<span></span>
							<label for="radiobtn_1">Todas</label>
						</li>

						<li class="list-group-item">
							<input id="radiobtn_2" class="radiobtn" name="filter" type="radio" value="Abierto" tabindex="1">
							<span></span>
							<label for="radiobtn_2">Pendientes</label>
						</li>

						<li class="list-group-item">
							<input id="radiobtn_3" class="radiobtn" name="filter" type="radio" value="Contestado" tabindex="1">
							<span></span>
							<label for="radiobtn_3">Aprobadas</label>
						</li>
						<li class="list-group-item">
							<input id="radiobtn_4" class="radiobtn" name="filter" type="radio" value="Cerrado" tabindex="1">
							<span></span>
							<label for="radiobtn_4">Rechazadas</label>
						</li>
					</ul>
				</div> --}}
				<div role="main" class="col-lg-12 px-2 {{-- toast fade show pt-2 pb-4 pr-3 pl-3 --}}">
					<div id="example-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="example-table_length"><label>Mostrar <select name="example-table_length" aria-controls="example-table" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> registros</label></div></div><div class="col-sm-12 col-md-6"><div id="example-table_filter" class="dataTables_filter"><label>Buscar:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example-table"></label></div></div></div><div class="row"><div class="col-sm-12">


						<div class="table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
								<tr class="bg-secondary text-white">
									<td scope="col" align="center"><strong>#</strong></td>
									<td scope="col" align="center" width="5%"><strong>Status</strong></td>
									<td scope="col" align="center"><strong>Cuenta</strong></td>
									<td scope="col" align="center"><strong>Nombre del proyecto</strong></td>

									<td scope="col" align="center"><strong>Monto</strong></td>
									<td scope="col" align="center"><strong>Ultima actualización</strong></td>
									<td scope="col" align="center"><strong>Fecha de solicitud</strong></td>
									<td scope="col" align="center"><strong>Días pasantes</strong></td>
									<td align="center"></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td scope="col" align="center"><strong>1</strong></td>
									<td scope="col" align="center"><span class='badge badge-secondary d-block'>Terminada</span></td>
									<td scope="col" align="center"><a href="{{ route('costs.plannedAccount') }}">FRE-0001-FTA</a></td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									<td scope="col" align="center"><strong>2</strong></td>
									<td scope="col" align="center"><span class='badge badge-danger d-block'>Pendiente</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									<td scope="col" align="center"><strong>3</strong></td>
									<td scope="col" align="center"><span class='badge badge-success d-block'>Aprobada</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									
									<td scope="col" align="center"><strong>4</strong></td>
									<td scope="col" align="center"><span class='badge badge-danger d-block'>Pendiente</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									<td scope="col" align="center"><strong>5</strong></td>
									<td scope="col" align="center"><span class='badge badge-secondary d-block'>Terminada</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									<td scope="col" align="center"><strong>6</strong></td>
									<td scope="col" align="center"><span class='badge badge-secondary d-block'>Terminada</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									<td scope="col" align="center"><strong>7</strong></td>
									<td scope="col" align="center"><span class='badge badge-secondary d-block'>Terminada</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									<td scope="col" align="center"><strong>8</strong></td>
									<td scope="col" align="center"><span class='badge badge-success d-block'>Aprobada</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									<td scope="col" align="center"><strong>9</strong></td>
									<td scope="col" align="center"><span class='badge badge-secondary d-block'>Enviada</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									
									<td scope="col" align="center"><strong>10</strong></td>
									<td scope="col" align="center"><span class='badge badge-secondary d-block'>Enviada</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									<td scope="col" align="center"><strong>11</strong></td>
									<td scope="col" align="center"><span class='badge badge-success d-block'>Aprobada</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
								<tr>
									<td scope="col" align="center"><strong>12</strong></td>
									<td scope="col" align="center"><span class='badge badge-success d-block'>Aprobada</span></td>
									<td scope="col" align="center">FRE-0001-FTA</td>
									<td scope="col">Alineación de motor en molino B</td>
									<td class="pt-0 pb-0">
 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 											<tr>
 												<td>$</td>
 												<td></td>
 												<td align="right">1,800.00</td>
 											</tr>

 										</table>
 									</td>
									<td scope="col">15 de julio del 2019</td>
									<td scope="col">10 de julio del 2019</td>
									<td scope="col" align="center">5 días</td>
									<td align="center"><button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-edit"></button></td>
								</tr>
							</tbody>
						<tfoot>
							<tr class="bg-light">
								<td scope="col" align="center"><strong>#</strong></td>
								<td scope="col" align="center"><strong>Cuenta</strong></td>
								<td scope="col" align="center"><strong>Nombre del proyecto</strong></td>
								<td scope="col" align="center"><strong>Monto</strong></td>
								<td scope="col" align="center"><strong>Estado</strong></td>
								<td scope="col" align="center"><strong>Última Actualización</strong></td>
								<td scope="col" align="center"><strong>Fecha de solicitud</strong></td>
									<td scope="col" align="center"><strong>Días pasantes</strong></td>
								<td align="center"></td>
							</tr>
						</tfoot>
					</table>
				</div>

						</div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="example-table_info" role="status" aria-live="polite">Mostrando 1 a 10 de 36 registros</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="example-table_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="example-table_previous"><a href="#" aria-controls="example-table" data-dt-idx="0" tabindex="0" class="page-link">Anterior</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="example-table" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example-table" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example-table" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example-table" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item next" id="example-table_next"><a href="#" aria-controls="example-table" data-dt-idx="5" tabindex="0" class="page-link">Siguiente</a></li></ul></div></div></div></div>
					
				
			</div>
		</div>
	</div>
</div>
</div>



<!-- Modal -->
<div class="modal fade" id="mEliminar" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-eliminar" class="frmEliminar" action="?c=clientes&a=Baja" method="POST" enctype="multipart/form-data" name="frmaltaClientes" id="frmEliminar">
				<div class="modal-header">
					<h4 class="modal-title text-danger" id="modalLabel">Eliminar cliente</h4>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h6>¿Esta seguro que desea eliminar el cliente?</h6>
					<input type="" id="txtIdClienteE" name="idCliente">
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Eliminar</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('script')

@endsection