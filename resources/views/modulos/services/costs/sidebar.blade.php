 <!-- Sidebar Holder -->
 <nav id="sidebar">
 	<div class="sidebar-header">
 		<h3>FRE-0001-FTA</h3>
 	</div>
 	<ul class="list-unstyled components">
 		<li class=" @if($side_link=='service') {{ 'active' }} @endif ">
 			<a href="{{ route('costs.service') }}">Descripción del servicio</a>
 		</li>
 		<li class=" @if($side_link=='plannedAccount') {{ 'active' }} @endif ">
 			<a href="{{ route('costs.plannedAccount') }}">Cuenta planeada</a>
 		</li>
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a href="{{ route('costs.realAccount') }}">Cuenta real</a>
 		</li>
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a href="{{ route('costs.productivity') }}">Indicador de productividad</a>
 		</li>	
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a class="nav-link disabled" href="#">Instrucciones de trabajo</a>
 		</li>
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a class="nav-link disabled" href="#">Cuenta real</a>
 		</li>
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a class="nav-link disabled" href="#">Indicador de seguridad</a>
 		</li>
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a class="nav-link disabled" href="#">Indicador tiempo de entrega</a>
 		</li>
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a class="nav-link disabled" href="#">Indicador TPEF</a>
 		</li>
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a class="nav-link disabled" href="#">Balance general</a>
 		</li>
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a class="nav-link disabled" href="#">Autoevaluación</a>
 		</li>
 		<li class=" @if($side_link=='') {{ 'active' }} @endif ">
 			<a class="nav-link disabled" href="#">Costos planeados</a>
 		</li>
 	</ul>
 </nav>

  @section('script')
 <script type="text/javascript">
 	$(document).ready(function () {
 		$('#sidebarCollapse').on('click', function () {
 			$('#sidebar').toggleClass('active');
 			$(this).toggleClass('active');
 		});
 	});
 </script>
 @endsection

