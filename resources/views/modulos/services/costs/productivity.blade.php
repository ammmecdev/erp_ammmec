 @extends('layout')
 @section('content')
 <link rel="stylesheet" href="{{ asset('css/sidebar/style5.css') }}">

 <div class="wrapper">
 	<!-- Sidebar Holder -->
 	<nav id="sidebar">
 		<div class="sidebar-header">
 			<h3>FRE-0001-FTA</h3>
 		</div>

 		<ul class="list-unstyled components">
			
 			<li>
 				<a href="#" class="nav-link disabled">Plan de trabajo</a>
 			</li>
 			<li>
 				<a href="#" class="nav-link disabled">Instrucciones de trabajo</a>
 			</li>
 			<li>
 				<a href="#" class="nav-link disabled">Indicador de seguridad</a>
 			</li>
 			<li>
 				<a href="#" class="nav-link disabled">Indicador tiempo de entrega</a>
 			</li>
 			<li>
 				<a href="#" class="nav-link disabled">Indicador TPEF</a>
 			</li>
 			<li>
 				<a href="#" class="nav-link disabled">Balance general</a>
 			</li>
 			<li>
 				<a href="#" class="nav-link disabled">Autoevaluación</a>
 			</li>
 			<li>
 				<a href="#" class="nav-link disabled">Costos planeados</a>
 			</li>
 		</ul>

 	</nav>

 	<!-- Page Content Holder -->
 	<div id="content">

 		<nav class="navbar navbar-expand-lg navbar-light mb-3">
 			<div class="container-fluid">

 				<button type="button" id="sidebarCollapse" class="navbar-btn bg-white">
 					<span></span>
 					<span></span>
 					<span></span>
 				</button>
 				<button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 					<i class="fas fa-align-justify"></i>
 				</button>

 				<h1 class="display-1 pt-2" style="font-size: 35px;">Productividad</h1>

 				<div class="collapse navbar-collapse" id="navbarSupportedContent">
 					
 				</div>
 			</div>
 			<div align="right" class="col-4">
 				<button type="button" class="btn btn-outline-secondary btn-xs" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-print"></i> Imprimir</button>

 			</div>
 		</nav>

 		<div class="card">
 			
 			<div class="card-body">
 				<div class="ml-0 mr-0">
 					<div class="row">
 						<div class="col-lg-2">
 							<p class="h6 pt-1" align="right"><strong>Proyecto:</strong></p>
 						</div>
 						<div class="col-lg-8 mt-0 bg-light pt-2">
 							<p class="h5" align="center">Servicio anual Molino B</p>

 							<p class="h4" align="center">FRE-0001-FTA</p>
 						</div>
 					</div>

 				</div>
 			</div>
 		</div>

 		<div class="card mt-3">
 			<div class="card-header pb-1">
 				<p class="h4 pt-2">Comparativa Planeado vs Real</p>
 			</div>
 			<div class="card-body">
 				<div class="row">
 					<div class="col-lg-6">
 						<table class="table table-hover table-bordered">

 							<tr class="bg-secondary text-white">
 								<td align="center" colspan="2" class="h5"><strong>Cuenta planeada</strong></td>
 							</tr>
 							<tr>
 								<td align="center" class="bg-light h6 pt-3">Material e insumos</td>
 								<td>
 									<div class="row">
 										<div class="col-2">
 											<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
 										</div>
 										<div class="col-10">
 											<p class="h5 pt-1 pr-2" align="right" ><strong>1,800.00</strong></p>
 										</div>
 									</div>
 								</td>
 							</tr>
 							<tr>
 								<td align="center" class="bg-light h6 pt-3">Mano de obra</td>
 								<td>
 									<div class="row">
 										<div class="col-2">
 											<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
 										</div>
 										<div class="col-10">
 											<p class="h5 pt-1 pr-2" align="right" ><strong>1,800.00</strong></p>
 										</div>
 									</div>
 								</td>
 							</tr>
 							<tr>
 								<td align="center" class="bg-light h6 pt-3">Herramienta y equipo</td>
 								<td>
 									<div class="row">
 										<div class="col-2">
 											<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
 										</div>
 										<div class="col-10">
 											<p class="h5 pt-1 pr-2" align="right" ><strong>1,800.00</strong></p>
 										</div>
 									</div>
 								</td>
 							</tr>
 							<tr>
 								<td align="center" class="bg-light h6 pt-3">Gastos de operación</td>
 								<td>
 									<div class="row">
 										<div class="col-2">
 											<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
 										</div>
 										<div class="col-10">
 											<p class="h5 pt-1 pr-2" align="right" ><strong>1,800.00</strong></p>
 										</div>
 									</div>
 								</td>
 							</tr>
 							<tr class="bg-light">
 								<td align="center">
 									<p class="h4 text-danger pt-2">TOTAL PLANEADO</p>
 									
 								</td>
 								<td>
 									<div class="row">
 										<div class="col-xs-1 col-lg-2">
 											<h4 class="pt-2 pl-3 text-danger"><strong>$</strong></h4>
 										</div>
 										<div class="col-xs-10 col-lg-10" align="right">
 											<h4 class="h2 text-danger"><strong>71,707.00</strong></h4>
 										</div>
 									</div>
 								</td>
 							</tr>
 						</table>
 					</div>
 					<div class="col-lg-6">
 						<table class="table table-hover table-bordered">

 							<tr class="bg-secondary text-white">
 								<td align="center" colspan="2" class="h5"><strong>Cuenta real</strong></td>
 							</tr>

 							<tr>
 								<td align="center" class="bg-light h6 pt-3">Material e insumos</td>
 								<td>
 									<div class="row">
 										<div class="col-2">
 											<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
 										</div>
 										<div class="col-10">
 											<p class="h5 pt-1 pr-2" align="right" ><strong>1,800.00</strong></p>
 										</div>
 									</div>
 								</td>
 							</tr>
 							<tr>
 								<td align="center" class="bg-light h6 pt-3">Mano de obra</td>
 								<td>
 									<div class="row">
 										<div class="col-2">
 											<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
 										</div>
 										<div class="col-10">
 											<p class="h5 pt-1 pr-2" align="right" ><strong>1,800.00</strong></p>
 										</div>
 									</div>
 								</td>
 							</tr>
 							<tr>
 								<td align="center" class="bg-light h6 pt-3">Herramienta y equipo</td>
 								<td>
 									<div class="row">
 										<div class="col-2">
 											<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
 										</div>
 										<div class="col-10">
 											<p class="h5 pt-1 pr-2" align="right" ><strong>1,800.00</strong></p>
 										</div>
 									</div>
 								</td>
 							</tr>
 							<tr>
 								<td align="center" class="bg-light h6 pt-3">Gastos de operación</td>
 								<td>
 									<div class="row">
 										<div class="col-2">
 											<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
 										</div>
 										<div class="col-10">
 											<p class="h5 pt-1 pr-2" align="right" ><strong>1,800.00</strong></p>
 										</div>
 									</div>
 								</td>
 							</tr>
 							<tr class="bg-light">
 								<td align="center">
 									<p class="h4 text-danger pt-2">TOTAL PLANEADO</p>
 								</td>
 								<td>
 									<div class="row">
 										<div class="col-xs-1 col-lg-2">
 											<h4 class="pt-2 pl-3 text-danger"><strong>$</strong></h4>
 										</div>
 										<div class="col-xs-10 col-lg-10" align="right">
 											<h4 class="h2 text-danger"><strong>71,707.00</strong></h4>
 										</div>
 									</div>
 								</td>
 							</tr>
 						</table>
 					</div>
 				</div> 	
 
 			</div>

 		</div>

 		<div class="card mt-3">
 			<div class="card-header pb-1">
 				<p class="h4 pt-2">Indicador de productividad</p>
 			</div>
 			<div class="card-body"> 	
 				<div class="row">
 					<div class="col-lg-6">
 						<table class="table table-hover table-bordered">
 							<tr class="bg-secondary text-white">
 								<td align="center" class="h5"><strong>P</strong></td>
 							</tr>
 							<tr>
 								<td align="center" class="h2 pt-3">1.04</td>
 							</tr>		
 						</table>
 					</div>
 					<div class="col-lg-6">
 						<table class="table table-hover table-bordered">
 							<tr class="bg-secondary text-white">
 								<td align="center" class="h5"><strong>PMO</strong></td>
 							</tr>
 							<tr>
 								<td align="center" class="h2 pt-3">0.70</td>
 							</tr>
 						</table>
 					</div>
 				</div> 			
 			</div>

 		</div>

 		<div class="card mt-3">
 			<div class="card-header pb-1">
 				<p class="h4 pt-2">Observaciones</p>
 			</div>
 			<div class="card-body">
 				<table class="table table-hover table-bordered">

 					<tr class="bg-secondary text-white">
 						<td align="center"><strong>Item</strong></td>
 						<td align="center"><strong>Observación</strong></td>
 					</tr>

 					<tr>
 						<td align="center">1</td>
 						<td>Esta es una observación para explicar algo</td>
 					</tr>
 					<tr>
 						<td align="center">2</td>
 						<td>Esto es otro tipo de observación para explicar otro tipo de cosas que se enlistarán en la cotización</td>
 					</tr>
 				</table>
 			</div>
 		</div>
 	</div>
 </div>

 <!-- Modal -->
 <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
 	<div class="modal-dialog modal-dialog-centered" role="document">
 		<div class="modal-content">
 			<div class="modal-header pb-3 pt-4 bg-light">
 				<h5 class="modal-title" id="exampleModalCenterTitle"><strong>Materiales e insumos</strong></h5>
 				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
 					<i class="fas fa-times"></i>
 				</button>
 			</div>
 			<div class="modal-body">
 				<form>
 					<div class="row">
 						<div class="col-sm-6 form-group">
 							<label>Cantidad</label>
 							<input class="form-control" type="text" placeholder="0">
 						</div>
 						<div class="col-sm-6 form-group">
 							<label>Unidad de medida</label>
 							<select class="form-control">
 								<option value="">Seleccione</option>

 								<option value="">PZA</option>
 								<option value="">JUEGO</option>
 								<option value="">LTS</option>
 								<option value="">KG</option>

 							</select>
 						</div>
 					</div>
 					<div class="form-group">
 						<label>Material o insumo</label>
 						<select class="form-control">
 							<option value="">Seleccione</option>

 							<option value="">PZA</option>
 							<option value="">JUEGO</option>
 							<option value="">LTS</option>
 							<option value="">KG</option>

 						</select>
 					</div>
 					<div class="row">
 						<div class="col-sm-6 form-group"> 
 							<label>Costo unitario</label>
 							<div class="input-group mb-3">
 								<div class="input-group-prepend">
 									<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
 								</div>
 								<input type="text" class="form-control text-right" placeholder="0.00" aria-label="Username" aria-describedby="basic-addon1" align="">
 							</div>
 						</div>
 						<div class="col-sm-6 form-group">
 							<label>Costo total</label>

 							<div class="bg-light pt-1 pb-1">
 								<div class="row">
 									<div class="col-2">
 										<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
 									</div>
 									<div class="col-10">
 										<p class="h5 pt-1 pr-0 col-10" align="right" ><strong>1,800.00</strong></p>
 									</div>
 								</div>
 							</div>

 						</div>
 					</form>
 				</div>
 				<div class="modal-footer pt-4">
 					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
 					<button type="button" class="btn btn-danger">Guardar</button>
 				</div>
 			</div>
 		</div>
 	</div>

 	@endsection
 	@section('script')
 	<script type="text/javascript">
 		$(document).ready(function () {
 			$('#sidebarCollapse').on('click', function () {
 				$('#sidebar').toggleClass('active');
 				$(this).toggleClass('active');
 			});
 		});
 	</script>
 	@endsection