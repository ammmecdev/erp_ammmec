 @extends('layout')
 @section('content')
 <link rel="stylesheet" href="{{ asset('css/sidebar/style5.css') }}">

 <style>
 	.table > tbody > tr > th, .table > tbody > tr > td {
 			border-top: 1px solid #e8e8e8;
 		}
 </style>

 <div class="wrapper">
 	<!-- Sidebar Holder -->
 	@include('modules.services.costs.sidebar')
 	<!-- Page Content Holder -->
 	<div id="content">

 		<nav class="navbar navbar-expand-lg navbar-light mb-3">
 			<div class="container-fluid">

 				<button type="button" id="sidebarCollapse" class="navbar-btn bg-white">
 					<span></span>
 					<span></span>
 					<span></span>
 				</button>
 				<button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
 					<i class="fas fa-align-justify"></i>
 				</button>

 				<h1 class="display-1 pt-2" style="font-size: 35px;">Solicitud de costo operativo</h1>

 				<div class="collapse navbar-collapse" id="navbarSupportedContent">
 					
 				</div>
 			</div>
 			<div align="right" class="col-4">
 				<button type="button" class="btn btn-outline-secondary btn-xs" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-print"></i> Imprimir</button>

 			</div>
 		</nav>

 		<div class="card">
 			<div class="card-body">
				<fieldset>
					<legend class="pb-2"><font size="4"> Datos generales </font></legend>
						<div class="table-responsive">
							<table class="table table-bordered">
								<tr align="center">
									<td>
										<label>Fecha de emisión</label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center" ><strong>30 de agosto del 2019</strong></p>
												</div>
											</div>
										</div>
									</td>
									<td>
										<label>Entrega Requerida</label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center" ><strong>30 de agosto del 2019</strong></p>
												</div>
											</div>
										</div>
									</td>
									<td>
										<label>Requisicón No.</label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center" ><strong>1</strong></p>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr align="center">
									<td>
										<label>Unidad de negocio</label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center" ><strong>Taller mantenimiento</strong></p>
												</div>
											</div>
										</div>
									</td>
									<td>
										<label>Cliente</label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center" ><strong>Minera Fresnillo S.A. de C.V.</strong></p>
												</div>
											</div>
										</div>
									</td>
									<td>
										<label>Contacto</label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center" ><strong>Joaquin Guzman Loera</strong></p>
												</div>
											</div>
										</div>
									</td>
								</tr>
								<tr align="center">
									<td align="center">
										<label>Cuenta</label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center" ><strong>FRE-001-FTA</strong></p>
												</div>
											</div>
										</div>
									</td>
									<td colspan="2">
										<label>Proyecto</label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center" ><strong>Reparación de molino B</strong></p>
												</div>
											</div>
										</div>
									</td>
								</tr>
							</table>
						</div>
					</fieldset>

					<div id="divCarrito" class="mt-2">
						<fieldset>
							<legend class="pb-2"><font size="4"> Carrito de servicios </font></legend>
							<div class="row">
								<div class="col-lg-12">
									<div class="table-responsive" id="tabla_carrito">
										<table class="table table-bordered">
											<thead>
												<tr class="bg-secondary text-white" align="center">
													<td><strong>Clave</strong></td>
													<td style="width: 100px;"><strong>Cantidad</strong></td>
													<td style="width: 100px;"><strong>Unidad</strong></td>
													<td align="left"><strong>Descripción</strong></td>
												</tr>
												
											</thead>
											<tbody>
												<tr>
													<td align="center">1</td>
													<td align="center">1</td>
													<td align="center">Servicio</td>
													<td>Reparación de chumasera en el molino B</td>
												</tr>
												<tr>
													<td align="center">2</td>
													<td align="center">1</td>
													<td align="center">Servicio</td>
													<td>Reparación de chumasera en el molino B</td>
												</tr>
												<tr>
													<td align="center">3</td>
													<td align="center">1</td>
													<td align="center">Servicio</td>
													<td>Reparación de chumasera en el molino B</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
			</div>
 		</div>
 	</div>
 </div>


<div class="card mt-3">
	<div class="card-header pb-1">
		<p class="h4 pt-2">Observaciones</p>
	</div>
	<div class="card-body">
		<table class="table table-hover table-bordered">

			<tr class="bg-secondary text-white">
				<td align="center"><strong>Item</strong></td>
				<td align="center"><strong>Observación</strong></td>
			</tr>

			<tr>
				<td align="center">1</td>
				<td>Esta es una observación para explicar algo</td>
			</tr>
			<tr>
				<td align="center">2</td>
				<td>Esto es otro tipo de observación para explicar otro tipo de cosas que se enlistarán en la cotización</td>
			</tr>
		</div>
	</div>
</div>

</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="exampleModalCenterTitle"><strong>Materiales e insumos</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label>Cantidad</label>
							<input class="form-control" type="text" placeholder="0">
						</div>
						<div class="col-sm-6 form-group">
							<label>Unidad de medida</label>
							<select class="form-control">
								<option value="">Seleccione</option>

								<option value="">PZA</option>
								<option value="">JUEGO</option>
								<option value="">LTS</option>
								<option value="">KG</option>

							</select>
						</div>
					</div>
					<div class="form-group">
						<label>Material o insumo</label>
						<select class="form-control">
							<option value="">Seleccione</option>

							<option value="">PZA</option>
							<option value="">JUEGO</option>
							<option value="">LTS</option>
							<option value="">KG</option>

						</select>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group"> 
							<label>Costo unitario</label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text" id="basic-addon1"><i class="fas fa-dollar-sign"></i></span>
								</div>
								<input type="text" class="form-control text-right" placeholder="0.00" aria-label="Username" aria-describedby="basic-addon1" align="">
							</div>
						</div>
						<div class="col-sm-6 form-group">
							<label>Costo total</label>
							
							<div class="bg-light pt-1 pb-1">
								<div class="row">
									<div class="col-2">
										<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
									</div>
									<div class="col-10">
										<p class="h5 pt-1 pr-0 col-10" align="right" ><strong>1,800.00</strong></p>
									</div>
								</div>
							</div>

						</div>
					</form>
				</div>
				<div class="modal-footer pt-4">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="button" class="btn btn-danger">Guardar</button>
				</div>
			</div>
		</div>
	</div>

	@endsection
	@section('script')
	<script type="text/javascript">
		$(document).ready(function () {
			$('#sidebarCollapse').on('click', function () {
				$('#sidebar').toggleClass('active');
				$(this).toggleClass('active');
			});
		});
	</script>
	@endsection