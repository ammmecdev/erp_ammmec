 @extends('layout')
 @section('content')
 <style>
 h1 {
 	width: 100%;
 	opacity: 0.4;
 }
 .imag {
 	width: 400px;
 	opacity: 0.6;
 }

 @media (min-width: 1367px) and (max-width: 2500px) { 

 	.imag{
 		padding-top: 170px;
 		width: 400px;
 		opacity: 0.6;
 	}

 }
 @media (max-width: 767px) {

 	.imag{
 		padding-top: 53px;
 		width: 400px;
 		opacity: 0.6;
 	}

 }
</style>
<div align="center">
	<img src="{{ asset('img/logo/logoammmec.png') }}" alt="AMMMEC" class="ml-3 imag">
	<h1 class="display-4" align="center">SERVICIOS</h2>
</div>
@endsection
