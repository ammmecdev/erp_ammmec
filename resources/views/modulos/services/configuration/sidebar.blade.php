 <link rel="stylesheet" href="{{ asset('css/sidebar/style5.css') }}">

<nav id="sidebar">
	<div class="sidebar-header">
		<h3>CONFIGURACIÓN</h3>
	</div>

	<ul class="list-unstyled components">
		<li>
			<a href="#">Cuenta de usuario</a>
		</li>
		<li class="active">
			<a href="{{ route('configuration.prices') }}">Configuración de precios</a>
		</li>
	</ul>
</nav>