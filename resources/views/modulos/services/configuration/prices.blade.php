 @extends('layout')
 @section('content')
 
<div class="wrapper">
	<!-- Sidebar Holder -->
	@include('modules.services.configuration.sidebar')	

	<!-- Page Content Holder -->
	<div id="content">

		<nav class="navbar navbar-expand-lg navbar-light mb-3">
			<div class="container-fluid">
				<button type="button" id="sidebarCollapse" class="navbar-btn bg-white">
					<span></span>
					<span></span>
					<span></span>
				</button>
				<button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<i class="fas fa-align-justify"></i>
				</button>

				<h1 class="display-1 pt-2" style="font-size: 35px;">Configuración de precios</h1>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">

				</div>
			</div>
			<div align="right" class="col-4">
				<button type="button" class="btn btn-outline-secondary btn-xs" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-print"></i> Imprimir</button>

			</div>
		</nav>


		<div class="card mt-3">
			<div class="card-header">
				<ul class="nav nav-pills nav-fill mb-0" id="pills-tab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="pills-mano-obra-tab" data-toggle="pill" href="#pills-mano-obra" role="tab" aria-controls="pills-mano-obra" aria-selected="false">Mano de obra</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pills-herr-equi-tab" data-toggle="pill" href="#pills-herr-equi" role="tab" aria-controls="pills-herr-equi" aria-selected="false">Herramienta y equipo</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="pills-viaticos-tab" data-toggle="pill" href="#pills-viaticos" role="tab" aria-controls="pills-viaticos" aria-selected="false">Gastos de operación</a>
					</li>
				</ul>
			</div>
			<div class="card-body pt-0">
				<div class="tab-content" id="pills-tabContent">

					<div class="tab-pane fade show active" id="pills-mano-obra" role="tabpanel" aria-labelledby="pills-mano-obra-tab">
						<h1 class="display-1 mt-3" style="font-size: 40px;" align="center">Mano de obra</h1>
						<div align="right" style="margin-top: -20px;" class="mb-2">
							<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#manoObraModal">Añadir <i class="fas fa-plus"></i></button>
						</div>
						<div class="table-responsive">
							<table class="table table-hover table-bordered">
								<thead>
									<tr class="bg-secondary text-white">
										<td align="center"><strong>Item</strong></td>
										<td align="center"><strong>Puesto</strong></td>
										<td align="center"><strong>Costo Unitario</strong></td>
										<td align="center"><strong>Factor Utilización</strong></td>
										<td align="center"><strong>Jornada 8 horas</strong></td>
										<td align="center"><strong>Jornada 12 horas</strong></td>
										<td></td>
									</tr>
								</thead>
								<tr>
									
									<td align="center"><strong>1</strong></td>
									<td>Director General</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>2</strong></td>
									<td>Gerente de operaciones</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>3</strong></td>
									<td>Gerente de Ventas y Asesoría Técnica				</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>4</strong></td>
									<td>Coordinador de Campo</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>5</strong></td>
									<td>Subgerente de Mantenimiento Predictivo				</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>6</strong></td>
									<td>Coordinador de Seguridad</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>7</strong></td>
									<td>Coordinador de Taller</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>8</strong></td>
									<td>Coordinador de Compras y Almacen				</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>9</strong></td>
									<td>Planeador</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>10</strong></td>
									<td>Auxiliar Técnico Mtto. Predictivo				</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>11</strong></td>
									<td>Técnico Mantenimiento Predictivo</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>12</strong></td>
									<td>Almacenista</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>13</strong></td>
									<td>Asistente A				</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>14</strong></td>
									<td>Asistente A+				</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>15</strong></td>
									<td>Tec. Esp. C</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>16</strong></td>
									<td>Tec. Esp. C+</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>17</strong></td>
									<td>Tec. Esp. B</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>18</strong></td>
									<td>Tec. Esp. B+1</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>19</strong></td>
									<td>Tec. Esp. B+2</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>20</strong></td>
									<td>Tec. Esp. A</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>
								<tr>
									
									<td align="center"><strong>21</strong></td>
									<td>Tec. Esp. A+1</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>22</strong></td>
									<td>Tec. Esp. A+2</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									
									<td align="center"><strong>23</strong></td>
									<td>Tec. Esp. A+3</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">700.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td></td>
												<td></td>
												<td align="right">1.00</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,304.48</td>
											</tr>

										</table>
									</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,985.40</td>
											</tr>

										</table>
									</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>		
							</tbody>
						</table>
					</div>
				</div>
				<div class="tab-pane fade" id="pills-herr-equi" role="tabpanel" aria-labelledby="pills-herr-equi-tab">
					
						<h1 class="display-1 mt-3" style="font-size: 40px;" align="center">Herramienta y equipo</h1>
						<div align="right" style="margin-top: -20px;" class="mb-2">
							<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#herramientaEquipoModal">Añadir <i class="fas fa-plus"></i></button>
						</div>
					<div class="table-responsive">
						<table class="table table-hover table-bordered">
							<thead>
								<tr class="bg-secondary text-white">
									<td align="center"><strong>Item</strong></td>
									<td align="center"><strong>Descripción</strong></td>
									<td align="center"><strong>Costo Unitario</strong></td>
									<td align="center"><strong>Unidad</strong></td>
									<td></td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td align="center"><strong>1</strong></td>
									<td >Torno D</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1,500.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Hora</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									<td align="center"><strong>2</strong></td>
									<td >Torno B</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr> 
												<td>$</td>
												<td></td>
												<td align="right">500.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Turno</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>
								<tr>
									<td align="center"><strong>3</strong></td>
									<td >Máquinas de Soldar</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">100.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Turno</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>
								<tr>
									<td align="center"><strong>4</strong></td>
									<td >Taladro o Banco/Magneticos</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">300.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Turno</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>
								<tr>
									<td align="center"><strong>5</strong></td>
									<td >Pensa</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">1000.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Turno</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>
								<tr>
									<td align="center"><strong>6</strong></td>
									<td >Plasma</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">500.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Hora</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>
								<tr>
									<td align="center"><strong>7</strong></td>
									<td >Compresores</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">25.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Hora</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>
								<tr>
									<td align="center"><strong>8</strong></td>
									<td >Metalizadora</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">2000.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Turno</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>
								<tr>
									<td align="center"><strong>9</strong></td>
									<td >Herramienta Métalica</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">19.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Turno</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									<td align="center"><strong>10</strong></td>
									<td >Toyota Hilux</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">112.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Hora</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									<td align="center"><strong>11</strong></td>
									<td >Mitsubichi</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">170.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Hora</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									<td align="center"><strong>12</strong></td>
									<td >Vehículo A09</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">12.48</td>
											</tr>

										</table>
									</td>
									<td align="center">Hora</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
								<tr>
									<td align="center"><strong>13</strong></td>
									<td >RAM 2500 A08</td>
									<td class="pt-0 pb-0">
										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
											<tr>
												<td>$</td>
												<td></td>
												<td align="right">16.00</td>
											</tr>

										</table>
									</td>
									<td align="center">Hora</td>
									<td align="center">
										<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#trashModal" style="font-size: 10px;"><i class="fas fa-trash"></i></button>
									</td>
								</tr>	
							</tbody>
						</table>
					</div>
			</div>
			<div class="tab-pane fade" id="pills-viaticos" role="tabpanel" aria-labelledby="pills-viaticos-tab">	
				<div class="row">
					<div class="col-lg-12 mt-3 mb-0">
						<h1 class="display-1" style="font-size: 40px;" align="center">Gastos de operación</h1>
						<nav style="margin-top:-25px;">
							<div class="nav nav-tabs pb-0 mt-0" id="nav-tab" role="tablist" >
								<a class="nav-item nav-link active" id="nav-transporte-tab" data-toggle="tab" href="#nav-transporte" role="tab" aria-controls="nav-transporte" aria-selected="true">Transporte</a>
								<a class="nav-item nav-link" id="nav-casetas-tab" data-toggle="tab" href="#nav-casetas" role="tab" aria-controls="nav-casetas" aria-selected="false">Casetas</a>
								<a class="nav-item nav-link" id="nav-alimentacion-tab" data-toggle="tab" href="#nav-alimentacion" role="tab" aria-controls="nav-alimentacion" aria-selected="false">Alimentación</a>
								<a class="nav-item nav-link" id="nav-hospedaje-tab" data-toggle="tab" href="#nav-hospedaje" role="tab" aria-controls="nav-hospedaje" aria-selected="false">Hospedaje</a>
							</div>
						</nav>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="tab-content" id="nav-tabContent">
							<div class="tab-pane fade show active" id="nav-transporte" role="tabpanel" aria-labelledby="nav-transporte-tab">
								<h1 class="display-1" style="font-size: 35px;" align="center">Transporte</h1>
								<div align="right" class="mb-2" style="margin-top: -15px;">
									<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#exampleModal" >Añadir <i class="fas fa-plus"></i></button>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-bordered">
										<thead>
											<tr class="bg-secondary text-white">
												<td align="center"><strong>Item</strong></td>
												<td align="center"><strong>Cliente</strong></td>
												<td align="center"><strong>Estado</strong></td>
												<td align="center"><strong>KM</strong></td>
												<td align="center"><strong>#</strong></td>
												<td align="center"><strong>Vehículo</strong></td>
												<td align="center"><strong>Combustible</strong></td>
												<td align="center"><strong>Tanque lleno</strong></td>
												<td align="center"><strong>Diferiencia</strong></td>
												<td align="center"><strong>Horas</strong></td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td align="center"><strong>1</strong></td>
												<td>Mina Bolañitos</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>	
											<tr>
												<td align="center"><strong>2</strong></td>
												<td>Mina Cosala</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>	
											<tr>
												<td align="center"><strong>3</strong></td>
												<td>Mina El Bolivar</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>	
											<tr>
												<td align="center"><strong>4</strong></td>
												<td>Mina La Colorada</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>
											<tr>
												<td align="center"><strong>5</strong></td>
												<td>Mina Palmarejo</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>
											<tr>
												<td align="center"><strong>6</strong></td>
												<td>Mina Primero Mining</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>	
											<tr>
												<td align="center"><strong>7</strong></td>
												<td>Mina San Martín</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>
											<tr>
												<td align="center"><strong>8</strong></td>
												<td>Mina Velardeña</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>	
											<tr>
												<td align="center"><strong>9</strong></td>
												<td>Mina Oro Silver</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>	
											<tr>
												<td align="center"><strong>10</strong></td>
												<td>Mina Sabinas</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>	
											<tr>
												<td align="center"><strong>11</strong></td>
												<td>Mina Saucito</td>
												<td>Guanajuato</td>
												<td align="center">349</td>
												<td align="center">8</td>
												<td align="center">Vagoneta Express(*)</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>
													</table>
												</td>
												<td align="right">51.11</td>
												<td align="right">4 hrs 38 min</td>
											</tr>				
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="nav-casetas" role="tabpanel" aria-labelledby="nav-casetas-tab">
								<h1 class="display-1" style="font-size: 35px;" align="center">Casetas</h1>
								<div align="right" class="mb-2" style="margin-top: -15px;">
									<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#exampleModal" >Añadir <i class="fas fa-plus"></i></button>
								</div>
								<div class="table-responsive">
									<table class="table table-hover table-bordered">
										<thead>
											<tr class="bg-secondary text-white">
												<td align="center"><strong>Item</strong></td>
												<td align="center"><strong>Cliente</strong></td>
												<td align="center"><strong>Estado</strong></td>
												<td align="center"><strong>#</strong></td>
												<td align="center"><strong>Precio</strong></td>
												<td align="center"><strong>Total</strong></td>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td align="center"><strong>1</strong></td>
												<td>Mina Bolañitos</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>	
											<tr>
												<td align="center"><strong>2</strong></td>
												<td>Mina Cosala</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>	
											<tr>
												<td align="center"><strong>3</strong></td>
												<td>Mina El Bolivar</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>	
											<tr>
												<td align="center"><strong>4</strong></td>
												<td>Mina La Colorada</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>
											<tr>
												<td align="center"><strong>5</strong></td>
												<td>Mina Palmarejo</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>
											<tr>
												<td align="center"><strong>6</strong></td>
												<td>Mina Primero Mining</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>	
											<tr>
												<td align="center"><strong>7</strong></td>
												<td>Mina San Martín</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>
											<tr>
												<td align="center"><strong>8</strong></td>
												<td>Mina Velardeña</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>	
											<tr>
												<td align="center"><strong>9</strong></td>
												<td>Mina Oro Silver</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>	
											<tr>
												<td align="center"><strong>10</strong></td>
												<td>Mina Sabinas</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>

													</table>
												</td>
											</tr>	
											<tr>
												<td align="center"><strong>11</strong></td>
												<td>Mina Saucito</td>
												<td>Guanajuato</td>
												<td align="center">8</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,551.11</td>
														</tr>

													</table>
												</td>
												<td class="pt-0 pb-0">
													<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">
														<tr>
															<td>$</td>
															<td></td>
															<td align="right">1,500.00</td>
														</tr>
													</table>
												</td>
											</tr>				
										</tbody>
									</table>
								</div>
							</div>
							<div class="tab-pane fade" id="nav-alimentacion" role="tabpanel" aria-labelledby="nav-alimentacion-tab">
								
							</div>
							<div class="tab-pane fade" id="nav-hospedaje" role="tabpanel" aria-labelledby="nav-hospedaje-tab">

							</div>
					</div>	
				</div>
						</div>
					</div>		
				</div>
						
		</div>
	</div>
</div>

</div>

</div>
</div>

<!-- Modal Herramienta y Equipo -->
<!-- Modal Mano de obra-->
<div class="modal fade" id="herramientaEquipoModal" tabindex="-1" role="dialog" aria-labelledby="herramientaEquipoModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="herramientaEquipoModalCenterTitle"><strong>Herramienta y equipo</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-lg-12 form-group">
							<label for="selectPuesto">Descripción:</label>
							<input type="text" class="form-control" name="descripcion">
						</div>
						<div class="col-lg-12 form-group">
							<label for="txtTipoPuesto">Tipo:</label>
							<input type="text" class="form-control" name="tipo" id="txtTipo">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label for="txtCostoUnitario">Costo Unitario:</label>
							<input class="form-control" type="text" placeholder="0" id="txtCostoUnitario" name="costoUnitario">
						</div>
						<div class="col-sm-6 form-group">
							<label for="txtFactorUtilizacion">Unidad</label>
							<select class="form-control" name="unidad" id="txtUnidad">
								<option value="">Hora</option>
								<option value="">Día</option>
								<option value="">Turno</option>
							</select>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-danger">Guardar</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Mano de obra-->
<div class="modal fade" id="manoObraModal" tabindex="-1" role="dialog" aria-labelledby="manoObraModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="manoObraModalCenterTitle"><strong>Mano de obra</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row">
						<div class="col-lg-12 form-group">
							<label for="selectPuesto">Puesto:</label>
							<div class="col-lg-12 px-0">
								<select class="select2" id="selectPuesto" name="idPuesto" style="width: 100%" multiple="multiple" placeholder="Seleccione el puesto q">
									<option value="" select>Seleccione el puesto que decea agregar</option>
									<option value="1">Dirección general</option>
									<option value="2">Gerente de operaciones</option>
									<option value="3">Gerente de Ventas y Asesoría Técnica</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label for="txtCostoUnitario">Costo Unitario:</label>
							<input class="form-control" type="text" placeholder="0" id="txtCostoUnitario" name="costoUnitario">
						</div>
						<div class="col-sm-6 form-group">
							<label for="txtFactorUtilizacion">Factor Utilización</label>
							<input class="form-control" type="text" placeholder="0" id="txtFactorUtilizacion" name="factorUtilizacion">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group"> 
							<label>Jornada de 8 horas</label>
							<div class="bg-light pt-1 pb-1">
								<div class="row">
									<div class="col-2">
										<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
									</div>
									<div class="col-10">
										<p class="h5 pt-1 pr-0 col-10" align="right" ><strong>1,000.00</strong></p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 form-group">
							<label>Jornada de 12 horas</label>
							
							<div class="bg-light pt-1 pb-1">
								<div class="row">
									<div class="col-2">
										<p class="h5 pt-1 pl-3" align="left" ><strong>$</strong></p>
									</div>
									<div class="col-10">
										<p class="h5 pt-1 pr-0 col-10" align="right" ><strong>1,800.00</strong></p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-danger">Guardar</button>
			</div>
		</div>
	</div>
</div>

<!-- Modal Eliminar-->
<div class="modal fade" id="trashModal" tabindex="-1" role="dialog" aria-labelledby="trashModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="trashModalCenterTitle"><strong>Confirmación de eliminación</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<form>
					<div class="row pt-2">
						<div class="col-lg-12">
							<h1 class="display-1" style="font-size: 20px;">¿Esta seguro que decea eliminar este registro? <br>Una vez eliminado no podrá ser recuperado</h1>
						</div>
					</div>
				</form>
			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-danger">Eliminar</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
	$(document).ready(function () {
		$('#sidebarCollapse').on('click', function () {
			$('#sidebar').toggleClass('active');
			$(this).toggleClass('active');
		});
		$('.select2').select2();
	});
</script>
@endsection