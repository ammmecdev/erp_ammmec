@extends('layout')
@section('content')

<style>

.sidebar-outer {
	position:relative;
}
.fixed {
	position: fixed;
}

#sidebar {
	overflow-y: scroll;
	color: #fff;
	background: #fff !important;
	
	top:0;
	bottom: 0;
}

.wrapper {
	margin-top:-25px;
	/*display: flex;*/
	width: 100%;
	align-items: stretch;
}
.navbar {
	padding: 15px 10px;
	background: #fff;
	border: none;
	border-radius: 0;
	margin-bottom: 40px;
	box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
}


#sidebar.active {
	margin-left: -250px;
	transform: rotateY(100deg);
}

#sidebar .sidebar-header {
	background: #fff;
}

#sidebar ul.components {
	padding: 0px 3px;
	border-bottom:1px solid #adb5bd;;
}

#sidebar ul p {
	color: #fff;
	padding: 10px;
}

#sidebar ul li a {
	padding: 10px;
	font-size: 1.1em;
	display: block;
}
#sidebar ul li a:hover {
	color: #fff;
	background: #dc3545 !important;
}

#sidebar ul li.active > a, a[aria-expanded="true"] {
	color: #fff;
	background: rgb(220, 53, 69);;
}


a[data-toggle="collapse"] {
	position: relative;
}

.dropdown-toggle::after {
	display: block;
	position: absolute;
	top: 50%;
	right: 20px;
	transform: translateY(-50%);
}

ul ul a {
	font-size: 0.9em !important;
	padding-left: 30px !important;
	background: #6d7fcc;
}

ul.CTAs {
	padding: 20px;
}

ul.CTAs a {
	text-align: center;
	font-size: 0.9em !important;
	display: block;
	border-radius: 5px;
	margin-bottom: 5px;
}

a.download {
	background: #fff;
	color: #fff;
}

a.article, a.article:hover {
	background: #6d7fcc !important;
	color: #fff !important;
}



/* ---------------------------------------------------
    CONTENT STYLE
    ----------------------------------------------------- */
    #content {
    	width: 100%;
    	padding: 20px;
    	min-height: 100vh;
    	transition: all 0.3s;
    }

    #sidebarCollapse {
    	width: 40px;
    	height: 40px;
    	background: #f5f5f5;
    	cursor: pointer;
    }

    #sidebarCollapse span {
    	width: 80%;
    	height: 2px;
    	margin: 0 auto;
    	display: block;
    	background: #555;
    	transition: all 0.8s cubic-bezier(0.810, -0.330, 0.345, 1.375);
    	transition-delay: 0.2s;
    }

    #sidebarCollapse span:first-of-type {
    	transform: rotate(45deg) translate(2px, 2px);
    }
    #sidebarCollapse span:nth-of-type(2) {
    	opacity: 0;
    }
    #sidebarCollapse span:last-of-type {
    	transform: rotate(-45deg) translate(1px, -1px);
    }


    #sidebarCollapse.active span {
    	transform: none;
    	opacity: 1;
    	margin: 5px auto;
    }


/* ---------------------------------------------------
    MEDIAQUERIES
    ----------------------------------------------------- */
    @media (max-width: 768px) {
    	#sidebar {
    		margin-left: -250px;
    		transform: rotateY(90deg);
    	}
    	#sidebar.active {
    		margin-left: 0;
    		transform: none;
    	}
    	#sidebarCollapse span:first-of-type,
    	#sidebarCollapse span:nth-of-type(2),
    	#sidebarCollapse span:last-of-type {
    		transform: none;
    		opacity: 1;
    		margin: 5px auto;
    	}
    	#sidebarCollapse.active span {
    		margin: 0 auto;
    	}
    	#sidebarCollapse.active span:first-of-type {
    		transform: rotate(45deg) translate(2px, 2px);
    	}
    	#sidebarCollapse.active span:nth-of-type(2) {
    		opacity: 0;
    	}
    	#sidebarCollapse.active span:last-of-type {
    		transform: rotate(-45deg) translate(1px, -1px);
    	}

    }
    .list-group-item.active {
    	z-index: 2;
    	color: #fff;
    	background-color: #6c757d99;
    	border-color: #6c757d00;
    }

</style>

<div class="wrapper">
	<div class="row">
		<div class="col-lg-3 sidebar-outer pr-0">
			<nav class="fixed col-lg-3" style="margin-top:55px;" id="sidebar">
				<div class="sidebar-header">
					<h3 class="display-1 text-dark pt-4 px-2" style="font-size:25px;">Lista de solicitudes</h3>
				</div><hr>
				<input type="search" class="form-control ds-input" id="search-input" placeholder="Buscar..." aria-label="Search for..." autocomplete="off" data-siteurl="https://getbootstrap.com" data-docs-version="4.1" spellcheck="false" role="combobox" aria-autocomplete="list" aria-expanded="false" aria-owns="algolia-autocomplete-listbox-0" dir="auto" style="position: relative; vertical-align: top;">
				<hr>
				<div class="px-0">
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small>3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 1</p>
						<small>15 de septiembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>
					<a href="#" class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">SAU-001-CM-MO-05</h5>
							<small class="text-muted">3 days ago</small>
						</div>
						<p class="mb-1">Alineación de motores para molino 2</p>
						<small class="text-muted">15 de noviembre 2019</small>
					</a>

				</div>
			</nav>
		</div>
		<div class="col-lg-9">
			<div id="content">
				<!--Encabezado-->

				<div class="card">
					<div class="card-body">
						<div class="ml-0 mr-0">
							<div class="row">
								<div class="col-lg-2">
									<p class="h6 pt-1" align="right"><strong>Proyecto:</strong></p>
								</div>
								<div class="col-lg-8 mt-0 bg-light pt-2">
									<p class="h5" align="center">Servicio anual Molino B</p>

									<p class="h4" align="center">FRE-0001-FTA</p>
								</div>
							</div>

							<div class="row">
								<div class="col-lg-6">
									<div class="row pt-3">
										<div class="col-xs-6 col-lg-4" align="right">
											<p class="h6"><strong>Cliente:</strong></p>
										</div>
										<div class="col-xs-6 col-lg-8" align="left">
											<p class="h6">Minera fresnillo S.A de C.V</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6 col-lg-4" align="right">
											<p class="h6"><strong>Fecha de Inicio:</strong></p>
										</div>
										<div class="col-xs-6 col-lg-8" align="left">
											<p class="h6">15 de julio del 2019</p>
										</div>
									</div>
									<div class="row">
										<div class="col-xs-6 col-lg-4" align="right">
											<p class="h6"><strong>Fecha de Término:</strong></p>
										</div>
										<div class="col-xs-6 col-lg-8" align="left">
											<p class="h6">25 de julio del 2019</p>
										</div>
									</div>

								</div>

							</div>

						</div>
					</div>
				</div>
				<!--Encabezado--> 
				<div class="card mt-3">
 			<div class="card-header">
 				<ul class="nav nav-pills nav-fill mb-0" id="pills-tab" role="tablist">
 					<li class="nav-item">
 						<a class="nav-link active" id="pills-mat-ins-tab" data-toggle="pill" href="#pills-mat-ins" role="tab" aria-controls="pills-mat-ins" aria-selected="true">Material e insumos</a>
 					</li>
 					<li class="nav-item">
 						<a class="nav-link" id="pills-herr-equi-tab" data-toggle="pill" href="#pills-herr-equi" role="tab" aria-controls="pills-herr-equi" aria-selected="false">Herramienta y equipo</a>
 					</li>

 				</ul>
 			</div>
 			<div class="card-body pt-0">
 				<div class="tab-content" id="pills-tabContent">
 					<div class="tab-pane fade show active" id="pills-mat-ins" role="tabpanel" aria-labelledby="pills-mat-ins-tab">
 						<div class="row">
 							<div class="col-lg-12 mt-2 pt-2">
 								<h1 class="display-1" style="font-size: 35px;" align="center">Materiales e insumos</h1>
 								<div align="right" class="mb-2" style="margin-top: -25px;">
 									<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#materialesInsumosModal" >Dar salida <i class="fas fa-plus"></i></button>
 								</div>
 							</div>
 						</div>
 						<div class="table-responsive">
 							<table class="table table-hover table-bordered">
 								<thead>
 									<tr class="bg-secondary text-white">
 										<td align="center"><strong>Item</strong></td>
 										<td align="center"><strong>Cantidad</strong></td>
 										<td align="center"><strong>Unidad</strong></td>
 										<td align="center"><strong>Descripción</strong></td>
 										<td align="center"><strong>Costo Unitario</strong></td>
 										<td align="center"><strong>Costo Total</strong></td>
 									</tr>
 								</thead>
 								<tbody>
	 								<tr>
	 									<td align="center"><strong>1</strong></td>
	 									<td align="center">4</td>
	 									<td align="center">pza</td>
	 									<td align="center">Material</td>
	 									<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
	 									<td class="pt-0 pb-0">
	 										<table class="table table-borderless" style="margin-bottom: 0px;">
	 											<thead>
	 												<tr>
	 													<td><strong>$</strong></td>
	 													<td></td>
	 													<td align="right"><strong>1,800.00</strong></td>
	 												</tr>
	 											</thead>
	 										</table>
	 									</td>
	 								</tr>	
	 								<tr>
	 									<td align="center"><strong>2</strong></td>
	 									<td align="center">4</td>
	 									<td align="center">pza</td>
	 									<td align="center">Insumo</td>
	 									<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
	 									<td class="pt-0 pb-0">
	 										<table class="table table-borderless" style="margin-bottom: 0px;">
	 											<thead>
	 												<tr>
	 													<td><strong>$</strong></td>
	 													<td></td>
	 													<td align="right"><strong>1,800.00</strong></td>
	 												</tr>
	 											</thead>
	 										</table>
	 									</td>
	 								</tr>	
	 								<tr>
	 									<td align="center"><strong>3</strong></td>
	 									<td align="center">4</td>
	 									<td align="center">pza</td>
	 									<td align="center">Material</td>
	 									<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
	 									<td class="pt-0 pb-0">
	 										<table class="table table-borderless" style="margin-bottom: 0px;">
	 											<thead>
	 												<tr>
	 													<td><strong>$</strong></td>
	 													<td></td>
	 													<td align="right"><strong>1,800.00</strong></td>
	 												</tr>
	 											</thead>
	 										</table>
	 									</td>
	 								</tr>	
	 								<tr>
	 									<td align="center"><strong>4</strong></td>
	 									<td align="center">4</td>
	 									<td align="center">pza</td>
	 									<td align="center">Insumo</td>
	 									<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
	 									<td class="pt-0 pb-0">
	 										<table class="table table-borderless" style="margin-bottom: 0px;">
	 											<thead>
	 												<tr>
	 													<td><strong>$</strong></td>
	 													<td></td>
	 													<td align="right"><strong>1,800.00</strong></td>
	 												</tr>
	 											</thead>
	 										</table>
	 									</td>
	 								</tr>	
 								</tbody>
 							</table>
 						</div>
 					</div>
 				<div class="tab-pane fade" id="pills-herr-equi" role="tabpanel" aria-labelledby="pills-herr-equi-tab">
 				<div class="row">
 					<div class="col-lg-12 mt-2 pt-2">
 						<h1 class="display-1" style="font-size: 35px;" align="center">Herramienta y equipo</h1>
 						<div align="right" class="mb-2" style="margin-top: -25px;">
 							<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#herramientaEquipoModal" >Dar salida <i class="fas fa-plus"></i></button>
 						</div>
 					</div>
 				</div>
 				<div class="table-responsive">
 					<table class="table table-hover table-bordered">
 						<thead>
 							<tr class="bg-secondary text-white">
 								<td align="center"><strong>Item</strong></td>
 								<td align="center"><strong>Cantidad</strong></td>
 								<td align="center"><strong>Unidad</strong></td>
 								<td align="center"><strong>Descripción</strong></td>
 								<td align="center"><strong>Costo Unitario</strong></td>
 								<td align="center"><strong>Costo Total</strong></td>
 							</tr>
 						</thead>
 						<tr>
 							<td align="center"><strong>1</strong></td>
 							<td align="center">1</td>
 							<td align="center">Hr</td>
 							<td align="center">Torno B</td>
 							<td class="pt-0 pb-0">
 								<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 									<tr>
 										<td>$</td>
 										<td></td>
 										<td align="right">1,800.00</td>
 									</tr>

 								</table>
 							</td>
 							<td class="pt-0 pb-0">
 								<table class="table table-borderless" style="margin-bottom: 0px;">
 									<thead>
 										<tr>
 											<td><strong>$</strong></td>
 											<td></td>
 											<td align="right"><strong>1,800.00</strong></td>
 										</tr>
 									</thead>
 								</table>
 							</td>
 						</tr>	
 						<tr>
 							<td align="center"><strong>2</strong></td>
 							<td align="center">1</td>
 							<td align="center">Hr</td>
 							<td align="center">Toyota Hilux</td>
 							<td class="pt-0 pb-0">
 								<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

 									<tr>
 										<td>$</td>
 										<td></td>
 										<td align="right">1,800.00</td>
 									</tr>

 								</table>
 							</td>
 							<td class="pt-0 pb-0">
 								<table class="table table-borderless" style="margin-bottom: 0px;">
 									<thead>
 										<tr>
 											<td><strong>$</strong></td>
 											<td></td>
 											<td align="right"><strong>1,800.00</strong></td>
 										</tr>
 									</thead>
 								</table>
 							</td>
 						</tr>	

 					</tbody>
 					<tfoot>
 						<tr>
 							<td colspan="4"></td>
 							<td class="bg-secondary text-white" align="right" style="padding-top: 15px;"><strong>Total</strong></td>
 							<td class="bg-secondary text-white pt-0 pb-0">
 								<table class="table table-borderless  text-white" style="margin-bottom: 0px;">
 									<thead>
 										<tr class="bg-secondary">
 											<td><strong>$</strong></td>
 											<td></td>
 											<td align="right" ><strong>1,800.00</strong></td>
 										</tr>
 									</thead>
 								</table>
 							</td>
 						</tr>
 					</tfoot>
 				</table>
 				</div>
 				</div>
			</div>
		</div>
	</div>
</div>

<!-- Crear solicitud de cotización Modal -->
<div class="modal fade" id="crearSolicitudModal" tabindex="-1" role="dialog" aria-labelledby="crearSolicitudModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="crearSolicitudModalCenterTitle"><strong>Solicitud de cotización</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<div id="divGeneral">
					<fieldset>
						<legend><font size="4"> Datos generales </font></legend>
						<br>
						<div class="row">
							<input name="idCostoOperativo" id="txtIdCostoOperativo" type="hidden" class="form-control">

							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Fecha de Emisión:</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center"><strong>1 de agosto del 2019</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Entrega Requerida:</strong></label>
									<input name="fechaEntrega" id="txtFechaEntrega" type="date" class="form-control" required>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Requisición No.</strong></label>
									<input name="requisicion" id="txtRequisicion" type="text" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-5">
								<div class="form-group">
									<label><strong>Cuenta:</strong></label>
									<select name="idNegocio" id="selectCuenta" class="form-control selectpicker" data-live-search="true" onchange="consultarPorCuenta()" required>
										<option value="">Seleccione la cuenta</option>
									</select>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<label><strong>Contacto:</strong></label>
									<select name="idContacto" id="selectContacto" class="form-control selectpicker" data-live-search="true" required>
										<option value="">Seleccione la persona de contacto</option>
									</select>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="form-group">
									<label><strong>Cliente:</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center"><strong>Minera fresnillo S.A. de C.V.</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<label><strong>Proyecto</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center"><strong>Reparación de flecha en Molino B</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row" id="divCrear">

							<div class="col-lg-12">
								<hr style="margin-bottom: 10px;">
							</div>
						</div>
					</fieldset>
				</div>
				<div id="divCarrito" class="mt-2">
					<fieldset>
						<legend><font size="4"> Datos del servicio </font></legend>
						<input name="idSerCO" id="txtIdSerCO" type="hidden" class="form-control">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<p id="verificador" class="text-danger"></p>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Clave:</strong></label>
									<input name="clave" id="txtClave" type="text" class="form-control">
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Cantidad:</strong></label>
									<input name="cantidad" id="txtCantidad" type="number" class="form-control">
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Unidad:</strong></label>
									<select name="unidad" id="txtUnidad" class="form-control">
										<option value="">Seleccione...</option>
										<option value="Pieza">Pieza</option>
									</select>
								</div>
							</div>
							<div class="col-lg-10 pr-0">
								<div class="form-group">
									<label><strong>Descripción:</strong></label>
									<textarea name="descripcion" id="txtNotas" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="col-xs-12 col-lg-12 pt-4">
									<div class="form-group" style="max-width: 60%">
										<button onclick="limpiarInputs()" type="button" class="btn btn-sm btn-light btn-block"><i class="fas fa-eraser"></i> Limpiar </button>
										<button onclick="agregarServicio()" type="button" class="btn btn-sm btn-danger btn-block"><i class="fas fa-cart-arrow-down"></i> Dar salida </button>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<legend><font size="4"> Carrito de servicios </font></legend>
							</div>
						</div>
						<div class="row">
							<div class="table-responsive" id="tabla_carrito">
								<table class="table table-bordered">
									<thead>
										<tr class="bg-light" align="center">
											<td><strong>Clave</strong></td>
											<td style="width: 100px;"><strong>Cantidad</strong></td>
											<td style="width: 100px;"><strong>Unidad</strong></td>
											<td><strong>Descripción</strong></td>
										</tr>

									</thead>
									<tbody>
										<tr>
											<td align="center">1</td>
											<td align="center">1</td>
											<td align="center">Servicio</td>
											<td>Reparación de chumasera en el molino B</td>
										</tr>
									</tbody>
								</table>
							</div>
							
						</div>
					</fieldset>
				</div>
			</div> {{-- modal-body --}}
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Terminar sin enviar</button>
				<button type="button btn" class="btn btn-danger btn-sm">Guardar y enviar</button>
			</div> {{-- modal-footer --}}
		</div> {{-- modal-content --}}
	</div> {{-- modal-dialog --}}
</div> {{-- modal --}}

@endsection

@section('script')
<script>
	var min = 300;
	var max = 3600;
	var mainmin = 200;

	$('#split-bar').mousedown(function (e) {
		e.preventDefault();
		$(document).mousemove(function (e) {
			e.preventDefault();
			var x = e.pageX - $('#sidebar').offset().left;
			if (x > min && x < max && e.pageX < ($(window).width() - mainmin)) {  
				$('#sidebar').css("width", x);
				$('#main').css("margin-left", x);
			}
		})
	});
	$(document).mouseup(function (e) {
		$(document).unbind('mousemove');
	});
</script>
@endsection