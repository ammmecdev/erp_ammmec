@extends('layout')
@section('content')


<style type="text/css" media="screen">

#main {
    margin-left: 80px;
    height: 300px;

}
#sidebar {
    width: 250px;
    float: left;
    max-width: 350px;
}
#split-bar {    
	border: 1px solid rgba(0,0,0,.125);
    height:100%;
    float: right;
    width: 1px;
    cursor: col-resize;
}


td, th{
	padding-top:0.45rem !important;
	padding-bottom:0.45rem !important;

}

.bg-danger{
	background-color: #f31a0424 !important;
}

.table > tbody > tr > th, .table > tbody > tr > td {
 			border-top: 1px solid #dee2e6;
 		}
</style>

<!--Encabezado-->
<div class="card mt-3">
	<div class="card-body">
		<h1 class="display-1 my-3" style="font-size: 40px;" align="center">
			Solicitud de suministros
		</h1>
	</div>
</div>
<!--Encabezado-->

<div class="card mt-3">
	<div class="card-body py-0">
		<div class="row">
			<div  id="sidebar">
   				<div id="split-bar"></div>
				<h1 class="display-1 py-3" style="font-size: 35px;" align="center">
					Solicitudes
				</h1>
				<div class="px-2 ml-0 mr-0">
					<div class="table-responsive">
						<table class="table table-hover" >
							<thead class="bg-secondary text-white">
								<tr>
									<td scope="col" align="center"><strong>#</strong></td>
									<td scope="col"><strong>Nombre del proyecto</strong></td>
									<td scope="col"><strong>Fecha del servicio</strong></td>
								</tr>
							</thead>
							<tbody>
								<tr class="bg-danger">
									<td scope="col" align="center"><strong>1</strong></td>									
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">1 de agosto del 2019</td>
									
									
								</tr>
								<tr class="bg-danger">
									<td scope="col" align="center"><strong>2</strong></td>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">1 de agosto del 2019</td>
									
									
								</tr>
								<tr class="bg-danger">
									<td scope="col" align="center"><strong>3</strong>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">1 de agosto del 2019</td>
									
									
								</tr>
								<tr class="bg-danger">
									<td scope="col" align="center"><strong>4</strong>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">1 de agosto del 2019</td>
									
									
								</tr>
								<tr>
									<td scope="col" align="center"><strong>5</strong>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">10 de julio del 2019</td>
									
									
								</tr>
								<tr>
									<td scope="col" align="center"><strong>6</strong>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">10 de julio del 2019</td>
									
									
								</tr>
								<tr>
									<td scope="col" align="center"><strong>7</strong>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">10 de julio del 2019</td>
									
									
								</tr>
								<tr>
									<td scope="col" align="center"><strong>8</strong>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">10 de julio del 2019</td>
									
									
								</tr>
								<tr>
									<td scope="col" align="center"><strong>9</strong>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">10 de julio del 2019</td>
									
									
								</tr>
								<tr>
									<td scope="col" align="center"><strong>10</strong></td>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">10 de julio del 2019</td>
									
								
								</tr>
								<tr>
									<td scope="col" align="center"><strong>11</strong></td>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">10 de julio del 2019</td>
									
									
								</tr>
								<tr>
									<td scope="col" align="center"><strong>12</strong></td>
									<td scope="col">Alineación de motor en molino B</td>
									<td scope="col">10 de julio del 2019</td>
									
									
								</tr>
							</tbody>
							<tfoot>
								<tr class="bg-light">
									<td scope="col" align="center"><strong>#</strong></td>
									<td scope="col"><strong>Nombre del proyecto</strong></td>
									<td scope="col"><strong>Fecha del servicio</strong></td>
								</tr>
							</tfoot>
						</table>
					</div> {{-- table-responsive --}}
				</div> {{-- col-lg-12 --}}
			</div> {{-- col-lg-6 --}}
			<div  id="main">
				<h1 class="display-1 pb-3 pt-3" style="font-size: 35px;" align="center">
					Lista de requerimientos
				</h1>
				<div class="table-responsive bg-white">
					<table class="table table-striped table-bordered">
						<thead>
							<tr class="">
								<td scope="col" width="5%" align="center"><strong>#</strong></td>
								<td scope="col" width="5%" align="center"><strong>Cantidad</strong></td>
								<td scope="col" width="5%" align="center"><strong>Unidad</strong></td>
								<td scope="col"><strong>Descripción</strong></td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td scope="col" align="center">1</td>
								<td scope="col" align="center">2</td>
								<td scope="col" align="center">PZA</td>
								<td scope="col">Descripción de un producto</td>
							</tr>
							<tr>
								<td scope="col" align="center">1</td>
								<td scope="col" align="center">2</td>
								<td scope="col" align="center">PZA</td>
								<td scope="col">Descripción de un producto</td>
							</tr>
							<tr>
								<td scope="col" align="center">1</td>
								<td scope="col" align="center">2</td>
								<td scope="col" align="center">PZA</td>
								<td scope="col">Descripción de un producto</td>
							</tr>
							<tr>
								<td scope="col" align="center">1</td>
								<td scope="col" align="center">2</td>
								<td scope="col" align="center">PZA</td>
								<td scope="col">Descripción de un producto</td>
							</tr>
						</tbody>
						<tfoot>
							<tr class="bg-light">
								<td scope="col" align="center"><strong>#</strong></td>
								<td scope="col" align="center"><strong>Cantidad</strong></td>
								<td scope="col" align="center"><strong>Unidad</strong></td>
								<td scope="col"><strong>Descripción</strong></td>
							</tr>
						</tfoot>
					</table>
				</div> {{-- table-responsive --}}		
			</div> {{-- col-lg-6 --}}
		</div> {{-- row --}}
	</div> {{-- card-body --}}	
</div> {{-- card --}}


<!-- Crear solicitud de cotización Modal -->
<div class="modal fade" id="crearSolicitudModal" tabindex="-1" role="dialog" aria-labelledby="crearSolicitudModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="crearSolicitudModalCenterTitle"><strong>Solicitud de cotización</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<div id="divGeneral">
					<fieldset>
							<legend><font size="4"> Datos generales </font></legend>
							<br>
							<div class="row">
								<input name="idCostoOperativo" id="txtIdCostoOperativo" type="hidden" class="form-control">

								<div class="col-lg-4">
									<div class="form-group">
										<label><strong>Fecha de Emisión:</strong></label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center"><strong>1 de agosto del 2019</strong></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label><strong>Entrega Requerida:</strong></label>
										<input name="fechaEntrega" id="txtFechaEntrega" type="date" class="form-control" required>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label><strong>Requisición No.</strong></label>
										<input name="requisicion" id="txtRequisicion" type="text" class="form-control" required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-5">
									<div class="form-group">
										<label><strong>Cuenta:</strong></label>
										<select name="idNegocio" id="selectCuenta" class="form-control selectpicker" data-live-search="true" onchange="consultarPorCuenta()" required>
											<option value="">Seleccione la cuenta</option>
										</select>
									</div>
								</div>
								<div class="col-lg-7">
									<div class="form-group">
										<label><strong>Contacto:</strong></label>
										<select name="idContacto" id="selectContacto" class="form-control selectpicker" data-live-search="true" required>
											<option value="">Seleccione la persona de contacto</option>
										</select>
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group">
										<label><strong>Cliente:</strong></label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center"><strong>Minera fresnillo S.A. de C.V.</strong></p>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-7">
									<div class="form-group">
										<label><strong>Proyecto</strong></label>
										<div class="bg-light pt-1 pb-1">
											<div class="row">
												<div class="col-12">
													<p class="h6 pt-2 pr-0" align="center"><strong>Reparación de flecha en Molino B</strong></p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row" id="divCrear">
								
								<div class="col-lg-12">
									<hr style="margin-bottom: 10px;">
								</div>
							</div>
					</fieldset>
				</div>
				<div id="divCarrito" class="mt-2">
					<fieldset>
						<legend><font size="4"> Datos del servicio </font></legend>
						<input name="idSerCO" id="txtIdSerCO" type="hidden" class="form-control">
						<div class="row">
	                        <div class="col-lg-12">
								<div class="form-group">
									<p id="verificador" class="text-danger"></p>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Clave:</strong></label>
									<input name="clave" id="txtClave" type="text" class="form-control">
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Cantidad:</strong></label>
									<input name="cantidad" id="txtCantidad" type="number" class="form-control">
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Unidad:</strong></label>
									<select name="unidad" id="txtUnidad" class="form-control">
										<option value="">Seleccione...</option>
										<option value="Pieza">Pieza</option>
									</select>
								</div>
							</div>
							<div class="col-lg-10 pr-0">
								<div class="form-group">
									<label><strong>Descripción:</strong></label>
									<textarea name="descripcion" id="txtNotas" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-lg-2">
								<div class="col-xs-12 col-lg-12 pt-4">
									<div class="form-group" style="max-width: 60%">
										<button onclick="limpiarInputs()" type="button" class="btn btn-sm btn-light btn-block"><i class="fas fa-eraser"></i> Limpiar </button>
										<button onclick="agregarServicio()" type="button" class="btn btn-sm btn-danger btn-block"><i class="fas fa-cart-arrow-down"></i> Añadir </button>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<legend><font size="4"> Carrito de servicios </font></legend>
							</div>
						</div>
						<div class="row">
								<div class="table-responsive" id="tabla_carrito">
									<table class="table table-bordered">
										<thead>
											<tr class="bg-light" align="center">
												<td><strong>Clave</strong></td>
												<td style="width: 100px;"><strong>Cantidad</strong></td>
												<td style="width: 100px;"><strong>Unidad</strong></td>
												<td><strong>Descripción</strong></td>
											</tr>
											
										</thead>
										<tbody>
											<tr>
												<td align="center">1</td>
												<td align="center">1</td>
												<td align="center">Servicio</td>
												<td>Reparación de chumasera en el molino B</td>
											</tr>
										</tbody>
									</table>
								</div>
							
						</div>
					</fieldset>
				</div>
			</div> {{-- modal-body --}}
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Terminar sin enviar</button>
				<button type="button btn" class="btn btn-danger btn-sm">Guardar y enviar</button>
			</div> {{-- modal-footer --}}
		</div> {{-- modal-content --}}
	</div> {{-- modal-dialog --}}
</div> {{-- modal --}}

@endsection

@section('script')
<script>
	var min = 300;
var max = 3600;
var mainmin = 200;

$('#split-bar').mousedown(function (e) {
    e.preventDefault();
    $(document).mousemove(function (e) {
        e.preventDefault();
        var x = e.pageX - $('#sidebar').offset().left;
        if (x > min && x < max && e.pageX < ($(window).width() - mainmin)) {  
          $('#sidebar').css("width", x);
          $('#main').css("margin-left", x);
        }
    })
});
$(document).mouseup(function (e) {
    $(document).unbind('mousemove');
});
</script>
@endsection