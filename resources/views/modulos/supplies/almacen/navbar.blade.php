<div class="collapse navbar-collapse" id="navbarSupportedContent">
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link @if($nav_link=='costs') active @endif " href="{{ route('almacen.costs.index') }}">
				Solicitud de suministros
			</a>
		</li>
	</ul>
	<ul class="navbar-nav flex-row ml-md-auto">
		<li class="nav-item float-right dropdown mr-0 mt-2">
			<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				<i class="fas fa-bell"></i>  
				<span class="badge badge-danger">5</span> 
			</a>
		</li>

		<li class="nav-item dropdown">
			<a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
				<img src="{{ asset('img/profiles/avatar.png') }}" class="rounded-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;" />
				<label class="ml-1">Alejandro Castro</label>

			</a>
			<div class="dropdown-menu">
				<a class="dropdown-item" href="#">Mi cuenta</a>
				<a class="dropdown-item" href="{{ route('configuration.prices') }}">Configuración</a>
				<form method="POST" action="">
					{{ csrf_field() }}
					<button class="dropdown-item" type="submit">Cerrar sesión</button>
				</form>
			</div>
		</li>

	</ul>
</div>