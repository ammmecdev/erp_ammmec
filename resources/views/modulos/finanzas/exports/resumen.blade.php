<table>
		<thead>
			<tr >
				<th style="width: 20px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong># de comprobación</strong>
				</th>
				<th style="width: 60px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Nombre</strong>
				</th>
				<th style="width: 25px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>UN</strong>
				</th>
				<th style="width: 28px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Etapa</strong>
				</th>
				<th style="width: 20px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Tipo</strong>
				</th>
				<th style="width: 20px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Mes solicitud</strong>
				</th>
				<th style="width: 20px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Año solicitud</strong>
				</th>
				<th style="width: 22px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Monto solicitado</strong>
				</th>
				<th style="width: 22px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Monto dispersado</strong>
				</th>
				<th style="width: 22px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Monto comprobado</strong>
				</th>
				<th style="width: 22px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Reembolsar</strong>
				</th>
				<th style="width: 22px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Decrementar</strong>
				</th>
				<th style="width: 25px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Mes comprobación</strong>
				</th>
				<th style="width: 25px; text-align: center; background-color: #C00000; color: white; font-size: 13px;">
					<strong>Mes contabilización</strong>
				</th>
			</tr>
		</thead>
		<tbody>
			@foreach($resumen as $item)
				<tr>
					<td style="text-align: center;">{{ $item->idSolicitud }}</td>
					<td style="text-align: left;">{{ $item->nombreUsuario }}</td>
					<td style="text-align: center;">{{ $item->unidad_negocio }}</td>

					<td style="text-align: center;">{{ $item->etapa }}</td>
					<td style="text-align: center; background-color: #F2DCDB"><strong>{{ $item->tipo }}</strong></td>

					<td style="text-align: center;">
						{{ strtoupper($item->mesSolicitud)}}
					</td>
					<td style="text-align: center;">
						{{ $item->yearSolicitud }}
					</td>
					<td style="text-align: center; background-color: #EBEBEB">{{ $item->solicitado }}</td>
					<td style="text-align: center; background-color: #EBEBEB">{{ $item->dispersado }}</td>
					<td style="text-align: center; background-color: #EBEBEB">{{ $item->comprobado }}</td>
					<td style="text-align: center; background-color: #EBEBEB">{{ $item->reembolsado }}</td>
					<td style="text-align: center; background-color: #EBEBEB">{{ $item->decrementado }}</td>
					<td style="text-align: center;">
						{{ strtoupper($item->mesComprobacion)}}
					</td>
					<td style="text-align: center;">
						{{ strtoupper($item->mesContabilizacion)}}
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>