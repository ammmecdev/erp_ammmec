<!DOCTYPE html>
<html>
<head>
	<title>Comprobación de viáticos</title>
	<style>
		body {
			margin: 0;
			font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
			font-size: 1rem;
			font-weight: 400;
			line-height: 1.5;
			color: rgb(69, 85, 95) !important;
			text-align: left;
			background-color: #fff;
		}

		.bg-light {
			background-color: rgb(159, 161, 164, .1) !important;
        	color: rgb(69, 85, 95) !important;
		}

		.bg-danger {
			background-color: rgb(192, 0 ,0) !important;
        	color: white !important;
		}

		.border-bottom{
			border-bottom: 1px solid rgb(192, 0 ,0) !important;
		}

		.text-danger {
			color: rgb(192, 0 ,0) !important;
		}

		.table {
			border-collapse: collapse !important;
		}
		.table-bordered th,
		.table-bordered td {
			border: 1px solid #dee2e6;
		}

		small{
			font-size: 10px;
		}

		small.small{
			font-size: 9px !important;
		}

		small.small2{
			font-size: 8px !important;
		}

		td{
			padding: 5px 5px 5px 5px;
			margin: 0px;
		}

		.text-left {
			text-align: left !important;
		}

		.text-right {
			text-align: right !important;
		}

		.text-center {
			text-align: center !important;
		}

		.d-flex {
			display: -ms-flexbox !important;
			display: flex !important;
		}

		.justify-content-center {
			-ms-flex-pack: center !important;
			justify-content: center !important;
		}

		.row {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			margin-right: -15px;
			margin-left: -15px;
		}

		.p-0 {
			padding: 0 !important;
		}

		.pt-0,
		.py-0 {
			padding-top: 0 !important;
		}

		.pr-0,
		.px-0 {
			padding-right: 0 !important;
		}

		.pb-0,
		.py-0 {
			padding-bottom: 0 !important;
		}

		.pl-0,
		.px-0 {
			padding-left: 0 !important;
		}

		.p-1 {
			padding: 0.25rem !important;
		}

		.pt-1,
		.py-1 {
			padding-top: 0.25rem !important;
		}

		.pr-1,
		.px-1 {
			padding-right: 0.25rem !important;
		}

		.pb-1,
		.py-1 {
			padding-bottom: 0.25rem !important;
		}

		.pl-1,
		.px-1 {
			padding-left: 0.25rem !important;
		}

		.p-2 {
			padding: 0.5rem !important;
		}

		.pt-2,
		.py-2 {
			padding-top: 0.5rem !important;
		}

		.pr-2,
		.px-2 {
			padding-right: 0.5rem !important;
		}

		.pb-2,
		.py-2 {
			padding-bottom: 0.5rem !important;
		}

		.pl-2,
		.px-2 {
			padding-left: 0.5rem !important;
		}

		.p-3 {
			padding: 1rem !important;
		}

		.pt-3,
		.py-3 {
			padding-top: 1rem !important;
		}

		.pr-3,
		.px-3 {
			padding-right: 1rem !important;
		}

		.pb-3,
		.py-3 {
			padding-bottom: 1rem !important;
		}

		.pl-3,
		.px-3 {
			padding-left: 1rem !important;
		}

		.p-4 {
			padding: 1.5rem !important;
		}

		.pt-4,
		.py-4 {
			padding-top: 1.5rem !important;
		}

		.pr-4,
		.px-4 {
			padding-right: 1.5rem !important;
		}

		.pb-4,
		.py-4 {
			padding-bottom: 1.5rem !important;
		}

		.pl-4,
		.px-4 {
			padding-left: 1.5rem !important;
		}

		.p-5 {
			padding: 3rem !important;
		}

		.pt-5,
		.py-5 {
			padding-top: 3rem !important;
		}

		.pr-5,
		.px-5 {
			padding-right: 3rem !important;
		}

		.pb-5,
		.py-5 {
			padding-bottom: 3rem !important;
		}

		.pl-5,
		.px-5 {
			padding-left: 3rem !important;
		}
	</style>
</head>
<body>
	<div>
		<table class="table text-center" style="margin-left: -10px; width: 100%">
				<tr>
					<th class="text-left"> 
						COMPROBACIÓN DE VIÁTICOS #{{ $solicitud->idSolicitud }}
					</th>
					<th class="text-right">
						<img src="{{ public_path('img/logo/logoammmec.png')}}" style="width: 100px">
					</th>
				</tr>
				<tr class="bg-danger">
					<td style="width: 40%" class="text-center">
						<small><strong>INFORMACIÓN</strong></small>
					</td>
					<td style="width: 60%" class="text-center">
						<small><strong>RESUMEN</strong></small>
					</td>
				</tr>
			<tbody style="font-size: 8px;">
				<tr>
					<td>
						<table id="tablaDetalles" class="pr-0" style="width: 100%">
							<tbody>
								<tr class="text-left">
									<td style="width: 30%" class="bg-light">
										<strong> Nombre del Empleado </strong>
									</td>
									<td style="width: 70%">
										{{ $solicitud->usuario->nombreUsuario }}
									</td>
								</tr>
								<tr class="text-left">
									<td style="width: 30%" class="bg-light">
										<strong> Fecha de solicitud </strong>
									</td>
									<td style="width: 70%">
										{{ $solicitud->created_at }}
									</td>
								</tr>
								<tr class="text-left">
									<td style="width: 30%" class="bg-light">
										<strong> Unidad de Negocio </strong>
									</td>
									<td style="width: 70%">
										{{ $solicitud->unidad_negocio }}
									</td>
								</tr>
								<tr class="text-left">
									<td style="width: 30%" class="bg-light">
										<strong> Importe de solicitud </strong>
									</td>
									<td style="width: 70%">
										${{ $solicitud->importe_total }} MXN
									</td>
								</tr>
								<tr class="text-left">
									<td style="width: 30%" class="bg-light">
										<strong> Destino del gasto </strong>
									</td>
									<td style="width: 70%">
										{{ $solicitud->destinoGasto }}
									</td>
								</tr>
								<tr class="text-left">
									<td style="width: 30%" class="bg-light">
										<strong> Objetivo del gasto </strong>
									</td>
									<td style="width: 70%">
										{{ $solicitud->objetivoGasto }}
									</td>
								</tr>
							</tbody>
						</table>
					</td>
					<td>
						<table id="tablaResumen" style="width: 100%;">
							<thead>
								<tr class="bg-light">
									<th></th>
									<th>Solicitado</th>
									<th>Dispersado</th>
									<th>Comprobado</th>
									<th>Reembolsado</th>
									<th>Decrementado</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($resumen as $item)
								<tr>
									<td>
										{{ $item['formaPago'] }}
									</td>
									<td>
										$ {{ $item['solicitado'] }}
									</td>
									<td>
										$ {{ $item['dispersado'] }}
									</td>
									<td>
										$ {{ $item['comprobado'] }}
									</td>
									<td>
										$ {{ $item['reembolsado'] }}
									</td>
									<td>
										$ {{ $item['decrementado'] }}
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr class="bg-light">
									<td><strong> TOTALES (MXN) </strong></td>
									<td> 
										<strong> 
											$ {{ number_format($totaDts->totalSolicitado, 2) }} 
										</strong>
									</td>
									<td> 
										<strong> 
											$ {{ number_format($totaDts->totalDispersado, 2) }} 
										</strong>
									</td>
									<td> 
										<strong> 
											$ {{ number_format($totaDts->totalComprobado, 2) }} 
										</strong>
									</td>
									<td> 
										<strong> 
											$ {{ number_format($totaDts->totalReembolsado, 2) }} 
										</strong>
									</td>
									<td> 
										<strong> 
											$ {{ number_format($totaDts->totalDecrementado, 2) }} 
										</strong>
									</td>
								</tr>
								<tr>
									<td colspan="4" class="text-right pr-4">
										<strong>P e n d i e n t e</strong>
									</td>
									<td>
										<strong>  
											$ {{ $totaDts->totalReembolsar }} 
										</strong>
									</td>
									<td>
										<strong> 
											$ {{ $totaDts->totalDecrementar }} 
										</strong>	
									</td>
								</tr>
							</tfoot>
						</table>
					</td>
				</tr>
				<tr class="bg-danger">
					<td colspan="2" class="text-center">
						<small><strong>COMPROBACIÓN DE GASTOS</strong></small>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table style="width: 100%;">
							<tr class="bg-light">
								<th>Fecha</th>
								<th>Tipo C.</th>
								<th>Folio</th>
								<th>Cuenta</th>
								<th>Tipo G.</th>
								<th>Proveedor</th>
								<th>Descripción</th>
								<th>Subtotal</th>
								<th>Iva</th>
								<th>Otros</th>
								<th>Importe</th>
							</tr>
							<tbody>
								<?php $pag = 1; $i = 0; ?>
								@foreach ($comprobaciones as $comprobacion)
								<?php $i++; ?>

								<tr>
									<td>
										{{ $comprobacion['fechaGasto'] }}
									</td>
									<td>
										{{ $comprobacion['tipoComprobante'] }}
									</td>
									<td>
										{{ $comprobacion['folioComprobante'] }}
									</td>
									<td>
										{{ $comprobacion['cuenta'] }}
									</td>
									<td>
										{{ $comprobacion['tipoGasto'] }}
									</td>
									<td>
										{{ $comprobacion['proveedor'] }}
									</td>
									<td>
										{{ $comprobacion['descripcion'] }}
									</td>
									<td>
										$ {{ $comprobacion['subtotal'] }}
									</td>
									<td>
										$ {{ $comprobacion['iva'] }}
									</td>
									<td>
										$ {{ $comprobacion['otros'] }}
									</td>
									<td>
										$ {{ $comprobacion['importe'] }}
									</td>
								</tr>

								@if(($pag == 1 && $i == 10) || ($pag > 1 && $i==18))
								<tr>
									<td colspan="2">
										<div style="page-break-after:always;"></div>
										<?php $pag++; $i=0; ?>
									</td>
								</tr>		
								@endif 

								@endforeach
							</tbody>
							<tfoot>
								<tr class="text-center bg-light">
									<td colspan="6"></td>
									<td class="pr-3">
										<strong>T o t a l e s ( M X N )</strong>
									</td>
									<td>
										<strong> $ {{ $totalesCg['totalSubtotal'] }} </strong>
									</td>
									<td>
										<strong> $ {{ $totalesCg['totalIva'] }} </strong>
									</td>
									<td>
										<strong> $ {{ $totalesCg['totalOtros'] }} </strong>
									</td>
									<td>
										<strong> $ {{ $totalesCg['totalImporte'] }} </strong>
									</td>
								</tr>
							</tfoot>
						</table>
					</td>
				</tr>
			</tbody>
			<tfoot style="font-size: 10px;">
				<tr>
					<td colspan="2">
						<table style="width: 100%;">
							
							<tr>
								<td class="py-4" style="width: 33.33%;">
									
								</td>
								<td style="width: 33.33%;">
									
								</td>
								<td style="width: 33.33%;">
									
								</td>
							</tr>
							<tr>
								<td>
									<hr>
									<small class="small">
										{{ $solicitante }}
									</small>
								</td>
								<td>
									<hr>
									<small class="small">
										{{ $autorizador }}
									</small>
								</td>
								<td>
									<hr>
									<small class="small">
										Mary Carmen Monreal Ordaz
									</small>
								</td>
							</tr>
							<tr class="bg-light">
								<td style="width: 33.33%;">
									<small><strong>Responsable de los Gastos</strong></small>
								</td>
								<td style="width: 33.33%;">
									<small><strong>Revisión y Autorización de los Gastos</strong></small>
								</td>
								<td style="width: 33.33%;">
									<small><strong>Responsable de Recepción de Documentación y Caja Chica de Viáticos</strong></small>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</body>
</html>