@extends('layout')
@section('content')
<style>
	.bg-secondary{
		background-color: #6c757d52 !important;
	}
</style>
<div class="container">
	<div role="main" class="pt-4 px-2">
		<h1 class="display-4 text-danger pb-2" style="font-size: 40px;">Mis notificaciones</h1>
		<div class="list-group">
			@foreach (Auth::user()->notifications as $notification)
			<a href="{{ route('notifications.read', ['notification_id' => $notification->id, 'route_name' => $notification->data['route_name'], 'id' => $notification->data['id']]) }}" style="max-width: 100%" class="toast fade show list-group-item {{ $notification->read_at ? 'list-group-item-ligth' : 'list-group-item-secondary bg-secondary' }} list-group-item-action col-12">
				<div class="toast-header {{ $notification->read_at ? 'list-group-item-ligth' : 'list-group-item-secondary' }}" >

					<img src="{{ asset($notification->data['picture']) }}" class="rounded-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px; margin-right: 10px;"/>
					<font class="mr-auto"><?php echo $notification->data['text'] ?></font>
					<small class="text-muted">{{ $notification->created_at->diffForHumans() }}</small>
				</div>
			</a>
			@endforeach
		</div>
	</div>
</div>
@endsection


@if (Auth::user()->notifications->count()) 
								 
								<!-- {{-- @foreach(Auth::user()->notifications_limit as $notification)

								<a href="{{ route('notifications.read', ['notification_id' => $notification->id, 'route_name' => $notification->data['route_name'], 'id' => $notification->data['id']]) }}" class="{{ $notification->read_at ? 'list-group-item-ligth' : 'list-group-item-secondary' }} pt-2 pb-2 pr-3 list-group-item-action head-dark border-ligth">
									<div class="row">
										<div class="col-lg-2">

											<img src="{{ asset($notification->data['picture']) }}" class="rounded-circle" alt="Usuario" style="margin-left:11px;"/>

										</div>
										<div class="col-lg-10">
											<div class="d-flex w-100 justify-content-between">
												<p class="mb-0">
													<small><?php echo $notification->data['text'] ?></small>
												</p>
											</div>

											<small class="">
												<?php echo $notification->data['icon'] ?>
												{{ $notification->created_at->diffForHumans() }}
											</small>
										</div>
									</div>
								</a>
								@endforeach  --}} -->