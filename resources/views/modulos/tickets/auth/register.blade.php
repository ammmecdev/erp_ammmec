<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="assets/images/logo_completo.png">

	<title>Ammmec</title>
<!-- Bootstrap core CSS -->
 	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

 	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet">
	<link href="{{ asset('css/sign-in/floating-labels.css') }}" rel="stylesheet">
	
	<style>
	.bd-placeholder-img {
		font-size: 1.125rem;
		text-anchor: middle;
	}

	@media (min-width: 768px) {
		.bd-placeholder-img-lg {
			font-size: 3.5rem;
		}
	}
</style>
</head>

<body class="bg-dark">
	<form class="form-signin bg-light pl-4 pr-4" method="POST" action="{{ url('registro') }}">
		{{ csrf_field() }}

		<div class="text-center mb-4">
			<img  src="{{ asset('images/logo/logoammmec.png') }}" alt="" style="max-width: 90%;">
			<div class="panel-heading">
				<h1 class="h3 mb-3 font-weight-normal">Registra tus datos</h1>
			</div>
		</div>

		<div class="form-label-group">
			<input name="name" type="text" id="inputName" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="Nombre de usuario" value="{{ old('name') }}" autofocus>
			<label for="inputName">Nombre de usuario</label>
			{!! $errors->first('name', '<div class="invalid-feedback mb-2 mt-2">:message</div>') !!}
		</div>

		<div class="form-label-group">
			<input name="email" type="email" id="inputEmail" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Correo empresarial" value="{{ old('email') }}">
			<label for="inputEmail">Correo empresarial</label>
			{!! $errors->first('email', '<div class="invalid-feedback mb-2 mt-2">:message</div>') !!}
		</div>

		<div class="form-label-group">
			<select  name="department_id" id="" class="form-control {{ $errors->has('department_id') ? 'is-invalid' : '' }}" style="height: 3.125rem; padding: .75rem;">
				<option value="">&nbsp;Departamento</option>
				@foreach($departments as $department):
				<option value="{{ $department->id }}" {{ old('department_id') == $department->id ? 'selected' : '' }}>
					&nbsp;{{ $department->name }}
				</option>
				@endforeach
			</select>
			<label for="inputEmail" class="sr-only">Departamento</label>
			{!! $errors->first('department_id', '<div class="invalid-feedback mb-2 mt-2">:message</div>') !!}
		</div>

		<div class="form-row pb-2">
			<div class="col pr-1">
				<div class="form-label-group">
					<input name="password" type="password" id="inputPassword" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password">
					<label for="inputPassword">Contraseña</label>
					{!! $errors->first('password', '<div class="invalid-feedback mb-2 mt-2">:message</div>') !!}
				</div>
			</div>
			<div class="col pl-1">
				<div class="form-label-group">

					<input name="password_confirmation" type="password" id="inputPassword" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Password">
					<label for="inputPassword">Confirmar cont.</label>
				</div>
			</div>
		</div>

		<button class="btn btn-lg btn-dark btn-block" type="submit">Enviar</button>
		<a class="btn btn-lg btn-light btn-block" href="{{ url('/') }}">Cancelar</a>

	</form>
</body>
</html>