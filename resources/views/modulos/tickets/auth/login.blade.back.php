<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="assets/images/logo_completo.png">

	<title>Ammmec</title>
<!-- Bootstrap core CSS -->
 	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

 	<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet">
	<link href="{{ asset('css/sign-in/signin.css') }}" rel="stylesheet">
	
</head>

<body class="text-center bg-dark">
	<form method="POST" action="{{ route('login') }}" class="form-signin bg-light pl-5 pr-5 pb-5" style=" margin-top:-20px;">
		{{ csrf_field() }}
		<img src="{{ asset('images/logo/logoammmec.png') }}" alt="LOGO-AMMMEC" class="center-block imag" style="max-width: 90%; margin-bottom: 10px;">
		@if(session()->has('flash'))
		<div class="alert alert-info">{{ session('flash') }}</div>
		@endif
		<div class="panel-heading">
			<h1 class="h3 mb-3 font-weight-normal">Inicio de sesión</h1>
		</div>
		<div class="form-group">
			<label for="inputEmail" class="sr-only">Correo</label>
			<input type="email" name="email" id="inputEmail" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Correo electrónico" autofocus>
			{!! $errors->first('email', '<div class="invalid-feedback mb-2 mt-2">:message</div>') !!}

			<label for="inputPassword" class="sr-only">Contraseña</label> 
			<input name="password" type="password" id="inputPassword" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Contraseña" >
			{!! $errors->first('password', '<div class="invalid-feedback">:message</div>') !!}
		</div>
		<button class="btn btn-lg btn-dark btn-block" type="submit">Acceder</button>
		{{-- <a class="btn btn-lg btn-light btn-block" href="{{ route('register') }}">Registrarse</a> 
		<a class="btn btn-lg btn-light btn-block pt-0 mt-0" href="?c=inicio">Olvide mi contraseña</a> --}}
	</form>
</body>
</html>