<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ERP | MÓDULOS</title>
	<link rel="icon" href="{{ asset('/img/logo/logo-blanco.png') }}">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

	<link href="{{ asset('css/adminlte/adminlte.min.css') }}" rel="stylesheet">

</head>

<style>
	body {
		background-image: url({{ asset('img/system/fondo-login.jpg')}});
		background-position: center;
		background-repeat: no-repeat;
		background-attachment: fixed;
		background-size: cover;
		height: 100vh;
	}
	.small-box {
		background: -webkit-gradient(linear, 25% 0%, 10% 91%, from(#E0F7FA), to(#9E9E9E));

		border-top-left-radius: 5px;
		border-top-right-radius: 5px;
		border-bottom-left-radius: 5px;
		border-bottom-right-radius: 5px;
		box-shadow: 2px 2px 7px #bfbfbf;
	}
	.disabled {
		opacity: 0.5;
		cursor: not-allowed
	}
	.navbar {
		background-color: transparent;
		border: transparent;
		margin-bottom: 0px;
	}
	.navbar-inverse {
		background-image: linear-gradient(to bottom, transparent 0, transparent 100%);
		filter:
	}
	footer {
		height: 100px;
		bottom: 0;
		width: 100%;
		margin-top: 120px;
	}
	.bg-light {
		background-color: rgb(159, 161, 164, .1) !important;
		color: rgb(69, 85, 95) !important;
	}
	.text-dark {
		color: rgb(69, 85, 95) !important;
	}
	.text-dark-80 {
		color: rgb(69, 85, 95, .80) !important;
	}
	.text-secondary {
		color: rgb(106, 115, 123) !important;
	}
	.icon {
		-webkit-transition: all .3s linear;
		-o-transition: all .3s linear;
		transition: all .3s linear;
		position: sticky;
		z-index: 0;
		font-size: 60px;
		color: rgba(0, 0, 0, 0.15);
	}
	.activate:hover {
		-webkit-box-shadow: 0px 0px 23px -4px rgba(255, 102, 102, 1);
		-moz-box-shadow: 0px 0px 23px -4px rgba(255, 102, 102, 1);
		box-shadow: 0px 0px 23px -4px rgba(255, 102, 102, 1);
		transition: all .2s linear;
	}
</style>

<body>

	<?php 
		$idUser = Auth::user()->idUsuario; 
		$ventas = $permisos->where('idFuncion', 'vt.navegar')->first();
		$calidad = $permisos->where('idFuncion', 'ca.navegar')->first();
		$finanzas = $permisos->where('idFuncion', 'fn.navegar')->first();
		$seguridad = $permisos->where('idFuncion', 'se.navegar')->first();
		$sistemas = $permisos->where('idFuncion', 'si.navegar')->first();
		$compras = $permisos->where('idFuncion', 'co.navegar')->first();
		$servicios = $permisos->where('idFuncion', 'op.navegar')->first();
		$almacen = $permisos->where('idFuncion', 'alm.navegar')->first();
		$ss = $permisos->where('idFuncion', 'alm.ss.solicitudes.navegar')->first();
		$talentoHumano = $permisos->where('idFuncion', 'th.navegar')->first();
		// $vp = $permisos->where('idFuncion', 'th.vp.solicitudes')->first();
	?>

	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#modal-collapse" aria-expanded="false" style="margin-top: 8px;">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="#" class="navbar-text text-dark" data-toggle="dropdown" id="admin8" style="padding-bottom: 9px;">
					<img src="{{ asset(Auth::user()->ruta) }}" class="img-circle" alt="Usuario" style="min-width: 40px; min-height: 40px; max-width: 40px; max-height: 40px;" />
					<span>&nbsp;</span>{{ Auth::user()->nombreUsuario }}
				</a>
			</div>
			<div class="collapse navbar-collapse" id="modal-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="{{ route('logout') }}" style="margin-top: 8px;" class="text-dark"><i class="glyphicon glyphicon-log-out"></i> Cerrar Sesión</a></li>
				</ul>
			</div>
		</div>
	</nav>
	<div style="height: 50vh" class="div-content">
		<div class="container-fluid" align="center">

			<div>
				<img src="{{ asset('img/logo/logoammmec.png') }}" alt="LOGO-AMMMEC" style="width: 230px; margin-top: -60px">
				<h2 class="text-dark" style="margin-top: 0px; opacity:0.8">ERP MÓDULOS</h2>
			</div>

			<div class="container">
				<div style="padding: 0px 180px 0px 180px">
					<hr style="border-top: 1px solid #dedede; box-shadow: 0px 0px 5px 1px rgb(222, 222, 222);">
				</div>
				<div class="row" style="margin-top: 35px; margin-bottom: 35px;">

					@if($idUser == 1 || $idUser == 4) 
						<div class="col-lg-3 col-xs-12">
							<a href="{{ route('ad') }}">
								<div class="panel panel-success activate">
									<div class="panel-body" style="padding-bottom: 12px">
										<div class="icon" style="margin-bottom: -30px">
											<i class="fas fa-hdd"></i>
										</div>
										<h3 class="text-center text-secondary" style="font-size: 20px;">
											<strong>cAdmin</strong>
										</h3>
									</div>
								</div>
							</a>
						</div>
					@endif

					<div class="col-lg-3 col-xs-12">
						<a <?php if($ventas){ ?> href="{{ route('ventas') }}" <?php } ?>>
							<div class="panel panel-success <?php if($ventas){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -30px">
										<i class="fas fa-chart-line"></i>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>CRM</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-xs-12">
						<a <?php if($servicios){ ?> href="{{ route('servicios') }}" <?php } ?>>
							<div class="panel panel-success <?php if($servicios){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -30px">
										<span class="glyphicon glyphicon-cog"></span>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Servicios</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-xs-12">
						<a <?php if($calidad){ ?> href="{{ route('calidad') }}" <?php } ?>>
							<div class="panel panel-success <?php if($calidad){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -30px">
										<span class="glyphicon glyphicon-folder-open"></span>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Calidad</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-xs-12">
						<a <?php if($finanzas){ ?> href="{{ route('finanzas') }}" <?php } ?>>
							<div class="panel panel-success <?php if($finanzas){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -20px; margin-top:-10px">
										<i class="fas fa-dollar-sign"></i>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Finanzas</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>
					
					<div class="col-lg-3 col-xs-12">
						<a <?php if($seguridad){ ?> href="{{ route('seguridad') }}" <?php } ?>>
							<div class="panel panel-success <?php if($seguridad){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -20px; margin-top:-10px">
										<i class="fas fa-user-shield"></i>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Seguridad</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-xs-12">
						<a href="{{ route('finanzas.solicitudes') }}">
							<div class="panel panel-success <?php if($finanzas){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -25px; margin-top: -5px">
										<i class="fas fa-hand-holding-usd"></i>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Viáticos</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-xs-12">
						<a href="{{ route('tickets') }}">
							<div class="panel panel-success activate">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -30px">
										<span class="glyphicon glyphicon-comment"></span>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Tickets</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>	
					
					<div class="col-lg-3 col-xs-12">
						<a <?php if($compras){ ?> href="{{ route('compras') }}" <?php } ?>>
								<div class="panel panel-success <?php if($compras){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -20px; margin-top:-10px">
										<i class="fas fa-shopping-cart"></i>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Compras</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-xs-12">
						<a <?php if($sistemas){ ?> href="{{ route('sistemas') }}" <?php } ?>>
							<div class="panel panel-success <?php if($sistemas){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -20px; margin-top:-10px">
										<i class="fas fa-laptop"></i>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Sistemas</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-xs-12">
						<a <?php if($almacen){ ?> href="{{ route('almacen') }}" <?php } ?>>
								<div class="panel panel-success <?php if($almacen){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -20px; margin-top:-10px">
										<i class="fas fa-boxes"></i>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Almacén</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-xs-12">
						<a <?php if($ss){ ?> href="{{ route('alm.ss.solicitudes') }}" <?php } ?>>
								<div class="panel panel-success <?php if($ss){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -20px; margin-top:-10px">
										<i class="fas fa-people-carry"></i>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Suministros</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>

					<div class="col-lg-3 col-xs-12">
						<a <?php if($talentoHumano){ ?> href="{{ route('talentoHumano') }}" <?php } ?>>
								<div class="panel panel-success <?php if($talentoHumano){ ?> activate <?php } else { ?> disabled <?php } ?>">
								<div class="panel-body" style="padding-bottom: 12px">
									<div class="icon" style="margin-bottom: -20px; margin-top:-10px">
										<i class="fas fa-user-tie"></i>
									</div>
									<h3 class="text-center text-secondary" style="font-size: 20px;">
										<strong>Talento Humano</strong>
									</h3>
								</div>
							</div>
						</a>
					</div>


				</div>
			</div>
		</div>
	</div>
</body>

</html>