@extends('layout')

@section('content')
	<costo-component/>
@endsection

{{-- 

@extends('layout')
@section('content')
<style>
td, th{
	padding-top:0.45rem !important;
	padding-bottom:0.45rem !important;

}
</style>


<div class="card mt-3">
	<div class="card-body">
		<div class="ml-0 mr-0">
			<div class="row">
				<div role="main" class="col-lg-12 px-2">
					<div id="example-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="example-table_length"><label>Mostrar <select name="example-table_length" aria-controls="example-table" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> registros</label></div></div><div class="col-sm-12 col-md-6"><div id="example-table_filter" class="dataTables_filter"><label>Buscar:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example-table"></label></div></div></div><div class="row"><div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-hover table-bordered">
								<thead>
									<tr class="bg-secondary text-white">
										<td scope="col" align="center"><strong>#</strong></td>
										<td scope="col" align="center" width="5%"><strong>Status</strong></td>
										<td scope="col" align="center"><strong>Cuenta</strong></td>
										<td scope="col" align="center"><strong>Nombre del proyecto</strong></td>

										<td scope="col" align="center"><strong>Monto</strong></td>
										<td scope="col" align="center"><strong>Fecha de solicitud</strong></td>
										<td align="center"></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td scope="col" align="center"><strong>1</strong></td>
										<td scope="col" align="center"><span class='badge badge-secondary d-block'>Terminada</span></td>
										<td scope="col" align="center"><a href="{{ route('costs.plannedAccount') }}">FRE-0001-FTA</a></td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
											<button class="btn btn-outline-secondary btn-xs" st dropdown-toggley data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"le="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButtobutton">
	    										<button class="dropdown-item text-secondary" href="#" data-toggle="modal" data-target="#verSolicitudModal"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

	    									</div>
	    								</div>
	    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>2</strong></td>
										<td scope="col" align="center"><span class='badge badge-danger d-block'>Pendiente</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
											<button class="btn btn-outline-secondary btn-xs" st dropdown-toggley data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"le="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
	    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

	    									</div>
	    								</div>
	    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>3</strong></td>
										<td scope="col" align="center"><span class='badge badge-success d-block'>Aprobada</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

		    									</div>
		    								</div>    									
										</td>
									</tr>
									<tr>
										
										<td scope="col" align="center"><strong>4</strong></td>
										<td scope="col" align="center"><span class='badge badge-danger d-block'>Pendiente</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
												<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

		    									</div>
	    								</div>
	    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>5</strong></td>
										<td scope="col" align="center"><span class='badge badge-secondary d-block'>Terminada</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
												<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

		    									</div>
	    								</div>
	    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>6</strong></td>
										<td scope="col" align="center"><span class='badge badge-secondary d-block'>Terminada</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#" data-toggle="modal" data-target="#verSolicitudModal"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

		    									</div>
		    								</div>    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>7</strong></td>
										<td scope="col" align="center"><span class='badge badge-secondary d-block'>Terminada</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

		    									</div>
		    								</div>    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>8</strong></td>
										<td scope="col" align="center"><span class='badge badge-success d-block'>Aprobada</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

		    									</div>
		    								</div>    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>9</strong></td>
										<td scope="col" align="center"><span class='badge badge-secondary d-block'>Enviada</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver detalles</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

		    									</div>
		    								</div>    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>10</strong></td>
										<td scope="col" align="center"><span class='badge badge-secondary d-block'>Enviada</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

												</div>
	    									</div>
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>11</strong></td>
										<td scope="col" align="center"><span class='badge badge-success d-block'>Aprobada</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

		    									</div>
	    									</div>
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>12</strong></td>
										<td scope="col" align="center"><span class='badge badge-success d-block'>Aprobada</span></td>
										<td scope="col" align="center">FRE-0001-FTA</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
												<button class="btn btn-outline-secondary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
												<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

		    									</div>
		    								</div>    									
										</td>
									</tr>
								</tbody>
							<tfoot>
								<tr class="bg-light">
									<td scope="col" align="center"><strong>#</strong></td>
									<td scope="col" align="center"><strong>Status</strong></td>
									<td scope="col" align="center"><strong>Cuenta</strong></td>
									<td scope="col" align="center"><strong>Nombre del proyecto</strong></td>
									<td scope="col" align="center"><strong>Monto</strong></td>
									<td scope="col" align="center"><strong>Fecha de solicitud</strong></td>
									<td align="center"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="example-table_info" role="status" aria-live="polite">Mostrando 1 a 10 de 36 registros</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="example-table_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="example-table_previous"><a href="#" aria-controls="example-table" data-dt-idx="0" tabindex="0" class="page-link">Anterior</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="example-table" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example-table" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button age-item "><a href="#" aria-controls="example-table" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example-table" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item next" id="example-table_next"><a href="#" aria-controls="example-table" data-dt-idx="5" tabindex="0" class="page-link">Siguiente</a></li></ul></div></div></div></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Crear solicitud de cotización Modal -->
<div class="modal fade" id="crearSolicitudModal" tabindex="-1" role="dialog" aria-labelledby="crearSolicitudModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="crearSolicitudModalCenterTitle"><strong>Solicitud de cotización</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-body">
				<fieldset>
						<legend><font size="4"> Datos generales </font></legend>
						<br>
						<div class="row">
							<input name="idCostoOperativo" id="txtIdCostoOperativo" type="hidden" class="form-control">

							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Fecha de Emisión:</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center" ><strong>1 de agosto del 2019</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Entrega Requerida:</strong></label>
									<input name="fechaEntrega" id="txtFechaEntrega" type="date" class="form-control" required>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Requisición No.</strong></label>
									<input name="requisicion" id="txtRequisicion" type="text" class="form-control" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-5">
								<div class="form-group">
									<label><strong>Cuenta:</strong></label>
									<select name="idNegocio" id="selectCuenta" class="form-control selectpicker" data-live-search="true" onchange="consultarPorCuenta()" required>
										<option value="">Seleccione la cuenta</option>
									</select>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<label><strong>Contacto:</strong></label>
									<select name="idContacto" id="selectContacto" class="form-control selectpicker" data-live-search="true" required>
										<option value="">Seleccione la persona de contacto</option>
									</select>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="form-group">
									<label><strong>Cliente:</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center" ><strong>Minera fresnillo S.A. de C.V.</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<label><strong>Proyecto</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center" ><strong>Reparación de flecha en Molino B</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row" id="divCrear">
							
							<div class="col-lg-12">
								<hr style="margin-bottom: 10px;">
							</div>
						</div>
					</fieldset>

					<div id="divCarrito" class="mt-2">
						<fieldset>
							<legend><font size="4"> Datos del servicio </font></legend>
							<input name="idSerCO" id="txtIdSerCO" type="hidden" class="form-control">
							<div class="row">
                                <div class="col-lg-12">
									<div class="form-group">
										<p id="verificador" class="text-danger"></p>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label><strong>Clave:</strong></label>
										<input name="clave" id="txtClave" type="text" class="form-control">
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label><strong>Cantidad:</strong></label>
										<input name="cantidad" id="txtCantidad" type="number" class="form-control">
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group">
										<label><strong>Unidad:</strong></label>
										<select name="unidad" id="txtUnidad" class="form-control">
											<option value="">Seleccione...</option>
											<option value="Pieza">Pieza</option>
										</select>
									</div>
								</div>
								<div class="col-lg-10 pr-0">
									<div class="form-group">
										<label><strong>Descripción:</strong></label>
										<textarea name="descripcion" id="txtNotas" class="form-control"></textarea>
									</div>
								</div>
								<div class="col-lg-2">
									<div class="col-xs-12 col-lg-12 pt-4">
										<div class="form-group" style="max-width: 60%">
											<button onclick="limpiarInputs()" type="button" class="btn btn-sm btn-light btn-block"><i class="fas fa-eraser"></i> Limpiar </button>
											<button onclick="agregarServicio()" type="button" class="btn btn-sm btn-danger btn-block"><i class="fas fa-cart-arrow-down"></i> Añadir </button>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<legend><font size="4"> Carrito de servicios </font></legend>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="table-responsive" id="tabla_carrito">
										<table class="table table-bordered">
											<thead>
												<tr class="bg-light" align="center">
													<td><strong>Clave</strong></td>
													<td style="width: 100px;"><strong>Cantidad</strong></td>
													<td style="width: 100px;"><strong>Unidad</strong></td>
													<td><strong>Descripción</strong></td>
												</tr>
												
											</thead>
											<tbody>
												<tr>
													<td align="center">1</td>
													<td align="center">1</td>
													<td align="center">Servicio</td>
													<td>Reparación de chumasera en el molino B</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Terminar sin enviar</button>
				<button type="button btn" class="btn btn-danger btn-sm">Guardar y enviar</button>
			</div>
		</div>
	</div>
</div>


<!-- Crear solicitud de cotización Modal -->
<div class="modal fade" id="verSolicitudModal" tabindex="-1" role="dialog" aria-labelledby="verSolicitudModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				
			</div>
			<div class="modal-body">
				<fieldset>
						<legend><font size="4"> Datos generales </font></legend>
						<br>
						<div class="row">
							<input name="idCostoOperativo" id="txtIdCostoOperativo" type="hidden" class="form-control">

							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Fecha de Emisión:</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center" ><strong>1 de agosto del 2019</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Entrega Requerida:</strong></label>
									<input name="fechaEntrega" id="txtFechaEntrega" type="date" class="form-control" required value="2019-10-29">
								</div>
							</div>
							<div class="col-lg-4">
								<div class="form-group">
									<label><strong>Requisición No.</strong></label>
									<input name="requisicion" id="txtRequisicion" type="text" class="form-control" required value="102">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-5">
								<div class="form-group">
									<label><strong>Cuenta:</strong></label>
									<select name="idNegocio" id="selectCuenta" class="form-control selectpicker" data-live-search="true" onchange="consultarPorCuenta()" required>
										<option value="">FRE-0001-MOL-SUM-01</option>
									</select>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<label><strong>Contacto:</strong></label>
									<select name="idContacto" id="selectContacto" class="form-control selectpicker" data-live-search="true" required>
										<option value="">Joaquin Guzman De Loera</option>
									</select>
								</div>
							</div>
							<div class="col-lg-5">
								<div class="form-group">
									<label><strong>Cliente:</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center" ><strong>Minera fresnillo S.A. de C.V.</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<div class="form-group">
									<label><strong>Proyecto</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center" ><strong>Reparación de flecha en Molino B</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row" id="divCrear">
							
							<div class="col-lg-12">
								<hr style="margin-bottom: 10px;">
							</div>
						</div>
					</fieldset>

					<div id="divCarrito" class="mt-2">
						<fieldset>
							<input name="idSerCO" id="txtIdSerCO" type="hidden" class="form-control">
						
							<div class="row">
								<div class="col-lg-12">
									<legend><font size="4"> Carrito de servicios <a href="#" style="font-size: 12px;">Agregar un servicio</a></font></legend>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div class="table-responsive" id="tabla_carrito">
										<table class="table table-bordered">
											<thead>
												<tr class="bg-light" align="center">
													<td><strong>Clave</strong></td>
													<td style="width: 100px;"><strong>Cantidad</strong></td>
													<td style="width: 100px;"><strong>Unidad</strong></td>
													<td><strong>Descripción</strong></td>
												</tr>
												
											</thead>
											<tbody>
												<tr>
													<td align="center">1</td>
													<td align="center">1</td>
													<td align="center">Servicio</td>
													<td>Reparación de chumasera en el molino B</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</fieldset>
					</div>
			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Terminar sin enviar</button>
				<button type="button btn" class="btn btn-danger btn-sm">Guardar y enviar</button>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')

@endsection --}}