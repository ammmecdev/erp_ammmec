<table>
	<thead>
		<tr>
			<th style="width: 23px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Clave de negocio</strong></th>
			<th style="width: 16px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Unidad de negocio</strong></th>
			<th style="width: 130px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Descripción</strong></th>
			<th style="width: 14px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Clave cotización</strong></th>
			<th style="width: 12px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Oportunidad</strong></th>
			<th style="width: 14px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Costo operativo</strong></th>
			<th style="width: 13px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Importe MXN</strong></th>
			<th style="width: 13px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Importe USD</strong></th>
			<th style="width: 13px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Precio Neto</strong></th>
			<th style="width: 14px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Etapa</strong></th>
			<th style="width: 13px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Rentabilidad</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha SCO</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha entrega SCO</strong></th>
			<th style="width: 18px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Días de demora SCO</strong></th>
			<th style="width: 13px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>RFQ</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha RFQ</strong></th>
			<th style="width: 34px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Contacto</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha de cotización</strong></th>
			<th style="width: 25px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Causa</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha perdido</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Pedido</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha pedido</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha vencimiento</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha de entrega</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha de reporte</strong></th>
			<th style="width: 22px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Días de demora Reporte</strong></th>
			<th style="width: 10px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Factura</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha de factura</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Tiempo sin pagar</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha de pago</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Tiempo en pagar</strong></th>
			<th style="width: 12px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Realización</strong></th>
			<th style="width: 12px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Facturación</strong></th>
			<th style="width: 16px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Pago MXN con IVA</strong></th>
			<th style="width: 16px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Pago USD con IVA</strong></th>
			<th style="width: 18px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Recibo elec de pago</strong></th>
			<th style="width: 17px; text-align: center; background-color: #c00000; color: #FFFFFF;"><strong>Fecha Recibo</strong></th>
		</tr>
	</thead>
	<tbody>
		@foreach($negocios as $item)

			@php
				$arrCuenta = explode("-", $item->claveNegocioCompleta);
				$unidadNegocio = count($arrCuenta) > 3 ? $arrCuenta[2] : "";

				if ($item->tipoMoneda == "MXN") {
					$importe_usd = 0;
					$importe_mxn = $item->valorNegocio;
				}else {
					$importe_usd = $item->valorNegocio;
					$importe_mxn = $item->valorNegocio * $item->xCambio;
				}
				$precio_neto = $importe_mxn * 1.16;

				if ($importe_mxn > 0) {
					$rentabilidad = (1 - $item->monto_total / $importe_mxn) * 100;
				}else {
					$rentabilidad = 0;
				}

				if ($item->fechaEntrega) {
					setlocale(LC_ALL, 'es_ES');
					$mesNumero = date("m", strtotime($item->fechaEntrega));
					$datosFecha = DateTime::createFromFormat('!m', $mesNumero);
					$nombreMesRealizado = strftime('%B', $datosFecha->getTimestamp());
				}else $nombreMesRealizado = "---";

				if ($item->fecha_factura) {
					setlocale(LC_ALL, 'es_ES');
					$mesNumero = date("m", strtotime($item->fecha_factura));
					$datosFecha = DateTime::createFromFormat('!m', $mesNumero);
					$nombreMesFacturado = strftime('%B', $datosFecha->getTimestamp());
				}else $nombreMesFacturado = "---";
				
				// Tiempo en pagar
				if ($item->fecha_factura && $item->fecha_pago) {
					$fechaFactura = new DateTime($item->fecha_factura);
					$fechaPago = new DateTime($item->fecha_pago);
					$diffPagoFactura = $fechaFactura->diff($fechaPago)->format('%r%a días');
				}else {
					$diffPagoFactura = "Sin definir";
				}

				// Tiempo sin pagar
				if($item->fecha_pago){
					$diffFechaFactura = "Pagado";
				}else {						
					$hoy = new DateTime();
					if ($item->fecha_factura) {
						$fechaFactura = new DateTime($item->fecha_factura);
						$diffFechaFactura = $fechaFactura->diff($hoy)->format('%r%a días');
					}else {
						$diffFechaFactura = "Sin fecha factura";
					}
				}

				// Días de demora Reporte
				if ($item->fechaReporte && $item->fechaEntrega) {
					$fechaReporte = new DateTime($item->fechaReporte);
					$fechaEntrega = new DateTime($item->fechaEntrega);
					$diffEntregaReporte = $fechaEntrega->diff($fechaReporte)->format('%r%a días');
				}else $diffEntregaReporte = "Sin definir";

				// Días de demora SCO
				if ($item->fecha_solicitud && $item->fecha_entrega) {
					$fechaSolicitud = new DateTime($item->fecha_solicitud);
					$fechaEntrega = new DateTime($item->fecha_entrega);
					$diffEntregaSCO = $fechaSolicitud->diff($fechaEntrega)->format('%r%a días');
				}else $diffEntregaSCO = "Sin definir";
		
			@endphp

			<tr>
				<td style="text-align: center;">{{ $item->claveNegocioCompleta }}</td>
				<td style="text-align: center;">{{ $unidadNegocio }}</td>
				<td style="text-align: left;">{{ $item->tituloNegocio }}</td>
				<td style="text-align: left;">{{ $item->clave_cotizacion }}</td>
				<td style="text-align: center;">{{ $item->ponderacion }}</td>
				<td style="text-align: right;">{{ number_format($item->monto_total, 2, '.', '')  }}</td>
				<td style="text-align: right;">{{ number_format($importe_mxn, 2, '.', '') }}</td>
				<td style="text-align: right;">{{ number_format($importe_usd, 2, '.', '') }}</td>
				<td style="text-align: right;">{{ number_format($precio_neto, 2, '.', '') }}</td>
				<td style="text-align: center;">{{ $item->nombre_etapa }}</td>
				<td style="text-align: center;">{{ number_format($rentabilidad, 2, '.', '') }} %</td>
				<td style="text-align: center;">{{ $item->fecha_solicitud }}</td>
				<td style="text-align: center;">{{ $item->fecha_entrega }}</td>
				<td style="text-align: center;">{{ $diffEntregaSCO }}</td>
				<td style="text-align: center;">{{ $item->rfq }}</td>
				<td style="text-align: center;">{{ $item->fechaRfq }}</td>
				<td style="text-align: left;">{{ $item->nombre_contacto }}</td>
				<td style="text-align: center;">{{ $item->fecha_cotizacion }}</td>
				<td style="text-align: center;">{{ $item->nombre_causa }}</td>
				<td style="text-align: center;">{{ $item->fechaGanadoPerdido }}</td>
				<td style="text-align: center;">{{ $item->pedido }}</td>
				<td style="text-align: center;">{{ $item->fechaPedido }}</td>
				<td style="text-align: center;">{{ $item->fechaVencimientoPedido }}</td>
				<td style="text-align: center;">{{ $item->fechaEntrega }}</td>
				<td style="text-align: center;">{{ $item->fechaReporte }}</td>
				<td style="text-align: center;">{{ $diffEntregaReporte }}</td>
				<td style="text-align: center;">{{ $item->no_factura }}</td>
				<td style="text-align: center;">{{ $item->fecha_factura }}</td>
				<td style="text-align: center;">{{ $diffFechaFactura }}</td>
				<td style="text-align: center;">{{ $item->fecha_pago }}</td>
				<td style="text-align: center;">{{ $diffPagoFactura }}</td>
				<td style="text-align: center;">{{ strtoupper($nombreMesRealizado) }}</td>
				<td style="text-align: center;">{{ strtoupper($nombreMesFacturado) }}</td>
				<td style="text-align: center;">{{ number_format($item->pago_mxn_iva, 2, '.', '') }}</td>
				<td style="text-align: center;">{{ number_format($item->pago_usd_iva, 2, '.', '') }}</td>
				<td style="text-align: center;">{{ $item->no_pago }}</td>
				<td style="text-align: center;">{{ $item->fecha_recibo }}</td>
			</tr>
		@endforeach
	</tbody>
</table>