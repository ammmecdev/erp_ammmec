@extends('layout')
@section('content')
<style>
td, th{
	padding-top:0.45rem !important;
	padding-bottom:0.45rem !important;

}
</style>
<!--Encabezado-->
<div class="card mt-3">
	<div class="card-body">
		<h1 class="display-1 my-3" style="font-size: 40px;" align="center"> Cotizaciones</h1>
		<div align="right" style="margin-top: -20px;" class="mb-2">
			<button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#nuevaCotizacionModal">Nueva cotizacion <i class="fas fa-file-alt"></i></button>
		</div>
	</div>
</div>
<!--Encabezado-->

<div class="card mt-3">
	<div class="card-body">
		<div class="ml-0 mr-0">
			<div class="row">
				<div role="main" class="col-lg-12 px-2">
					<div id="example-table_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="example-table_length"><label>Mostrar <select name="example-table_length" aria-controls="example-table" class="form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> registros</label></div></div><div class="col-sm-12 col-md-6"><div id="example-table_filter" class="dataTables_filter"><label>Buscar:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example-table"></label></div></div></div><div class="row"><div class="col-sm-12">
						<div class="table-responsive">
							<table class="table table-hover table-bordered">
								<thead>
									<tr class="bg-secondary text-white">
										<td scope="col" align="center"><strong>#</strong></td>
										<td scope="col" align="center"><strong>Cuenta</strong></td>
										<td scope="col" align="center"><strong>Cliente</strong></td>
										<td scope="col" align="center"><strong>Nombre del proyecto</strong></td>
										<td scope="col" align="center"><strong>Monto</strong></td>
										<td scope="col" align="center"><strong>Fecha de envio</strong></td>
										<td align="center"></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td scope="col" align="center"><strong>1</strong></td>
										<td scope="col" align="center"><a href="{{ route('costs.plannedAccount') }}">FRE-0001-FTA</a></td>
										<td scope="col">Minera Fresnillo S.A de C.V.</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
											<button class="btn btn-outline-secondary btn-xs" st dropdown-toggley data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"le="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButtobutton">
	    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Editar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

	    									</div>
	    								</div>
	    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>1</strong></td>
										<td scope="col" align="center"><a href="{{ route('costs.plannedAccount') }}">FRE-0001-FTA</a></td>
										<td scope="col">Minera Fresnillo S.A de C.V.</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
											<button class="btn btn-outline-secondary btn-xs" st dropdown-toggley data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"le="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButtobutton">
	    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Editar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

	    									</div>
	    								</div>
	    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>1</strong></td>
										<td scope="col" align="center"><a href="{{ route('costs.plannedAccount') }}">FRE-0001-FTA</a></td>
										<td scope="col">Minera Fresnillo S.A de C.V.</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
											<button class="btn btn-outline-secondary btn-xs" st dropdown-toggley data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"le="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButtobutton">
	    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Editar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

	    									</div>
	    								</div>
	    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>1</strong></td>
										<td scope="col" align="center"><a href="{{ route('costs.plannedAccount') }}">FRE-0001-FTA</a></td>
										<td scope="col">Minera Fresnillo S.A de C.V.</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
											<button class="btn btn-outline-secondary btn-xs" st dropdown-toggley data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"le="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButtobutton">
	    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Editar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

	    									</div>
	    								</div>
	    									
										</td>
									</tr>
									<tr>
										<td scope="col" align="center"><strong>1</strong></td>
										<td scope="col" align="center"><a href="{{ route('costs.plannedAccount') }}">FRE-0001-FTA</a></td>
										<td scope="col">Minera Fresnillo S.A de C.V.</td>
										<td scope="col">Alineación de motor en molino B</td>
										<td class="pt-0 pb-0">
	 										<table class="table table-borderless pt-0 pb-0" style="margin-bottom: 0px;">

	 											<tr>
	 												<td>$</td>
	 												<td></td>
	 												<td align="right">1,800.00</td>
	 											</tr>

	 										</table>
	 									</td>
										<td scope="col">15 de julio del 2019</td>
										<td align="center">
											<div class="dropdown">
											<button class="btn btn-outline-secondary btn-xs" st dropdown-toggley data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"le="font-size: 10px;"><i class="fas fa-ellipsis-h"></i></button>
											<div class="dropdown-menu" aria-labelledby="dropdownMenuButtobutton">
	    										<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-edit"></i> Ver solicitud</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-trash"></i> Eliminar</button>

		    									<button class="dropdown-item text-secondary" href="#"><i class="pl-2 pr-2 fas text-secondary fa-print"></i> Imprimir</button>

	    									</div>
	    								</div>
	    									
										</td>
									</tr>
								</tbody>
							<tfoot>
								<tr class="bg-light">
									<td scope="col" align="center"><strong>#</strong></td>
									<td scope="col" align="center"><strong>Cuenta</strong></td>
									<td scope="col" align="center"><strong>Cliente</strong></td>
									<td scope="col" align="center"><strong>Nombre del proyecto</strong></td>
									<td scope="col" align="center"><strong>Monto</strong></td>
									<td scope="col" align="center"><strong>Fecha de solicitud</strong></td>
									<td align="center"></td>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="example-table_info" role="status" aria-live="polite">Mostrando 1 a 10 de 36 registros</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="example-table_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="example-table_previous"><a href="#" aria-controls="example-table" data-dt-idx="0" tabindex="0" class="page-link">Anterior</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="example-table" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example-table" data-dt-idx="2" tabindex="0" class="page-link">2</a></li><li class="paginate_button age-item "><a href="#" aria-controls="example-table" data-dt-idx="3" tabindex="0" class="page-link">3</a></li><li class="paginate_button page-item "><a href="#" aria-controls="example-table" data-dt-idx="4" tabindex="0" class="page-link">4</a></li><li class="paginate_button page-item next" id="example-table_next"><a href="#" aria-controls="example-table" data-dt-idx="5" tabindex="0" class="page-link">Siguiente</a></li></ul></div></div></div></div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Crear solicitud de cotización Modal -->
<div class="modal fade" id="nuevaCotizacionModal" tabindex="-1" role="dialog" aria-labelledby="nuevaCotizacionModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header pb-3 pt-4 bg-light">
				<h5 class="modal-title" id="nuevaCotizacionModalCenterTitle"><strong>Nueva cotización</strong></h5>
				<button type="button" class="btn btn-white pt-0" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="modal-header">
				<fieldset>
						<legend><font size="4"> Datos generales </font></legend>
						<br>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label><strong>Cuenta:</strong></label>
									<select name="idNegocio" id="selectCuenta" class="form-control selectpicker" data-live-search="true" onchange="consultarPorCuenta()" required>
										<option value="">Seleccione la cuenta</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label><strong>Costo:</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center" ><strong>$   120,000.00</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label><strong>Margen de utilidad</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<input class="form-control">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label><strong>Total:</strong></label>
									<div class="bg-light pt-1 pb-1">
										<div class="row">
											<div class="col-12">
												<p class="h6 pt-2 pr-0" align="center" ><strong>$    145,000.00</strong></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</fieldset>

			</div>
			<div class="modal-footer pt-4">
				<button type="button" class="btn btn-light btn-sm" data-dismiss="modal">Terminar sin enviar</button>
				<button type="button btn" class="btn btn-danger btn-sm">Guardar y enviar</button>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')

@endsection