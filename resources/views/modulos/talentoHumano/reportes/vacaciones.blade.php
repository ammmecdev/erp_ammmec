<!DOCTYPE html>
<html>
<head>
	<title>Solicitud</title>
	<style>
		body {
			margin: 0;
			font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
			font-size: 1rem;
			font-weight: 400;
			line-height: 1.5;
			color: rgb(69, 85, 95) !important;
			text-align: left;
			background-color: #fff;
		}

		.bg-light {
			background-color: rgb(159, 161, 164, .1) !important;
        	color: rgb(69, 85, 95) !important;
		}

		.bg-danger {
			background-color: rgb(192, 0 ,0) !important;
        	color: white !important;
		}

		.border-bottom {
			border-bottom: 1px solid rgb(192, 0 ,0) !important;
		}

		.text-danger {
			color: rgb(192, 0 ,0) !important;
		}

		.table {
			border-collapse: collapse !important;
			/*border: 1px solid #dee2e6;*/
		}
		.table-bordered th,
		.table-bordered td {
			border: 1px solid #dee2e6;
		}

		small{
			font-size: 10px;
		}

		small.small{
			font-size: 9px !important;
		}

		small.small2{
			font-size: 8px !important;
		}

		td{
			padding: 5px 5px 5px 5px;
			margin: 0px;
		}

		.text-left {
			text-align: left !important;
		}

		.text-right {
			text-align: right !important;
		}

		.text-center {
			text-align: center !important;
		}

		.d-flex {
			display: -ms-flexbox !important;
			display: flex !important;
		}

		.justify-content-center {
			-ms-flex-pack: center !important;
			justify-content: center !important;
		}

		.row {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			margin-right: -15px;
			margin-left: -15px;
		}

		.p-0 {
			padding: 0 !important;
		}

		.pt-0,
		.py-0 {
			padding-top: 0 !important;
		}

		.pr-0,
		.px-0 {
			padding-right: 0 !important;
		}

		.pb-0,
		.py-0 {
			padding-bottom: 0 !important;
		}

		.pl-0,
		.px-0 {
			padding-left: 0 !important;
		}

		.p-1 {
			padding: 0.25rem !important;
		}

		.pt-1,
		.py-1 {
			padding-top: 0.25rem !important;
		}

		.pr-1,
		.px-1 {
			padding-right: 0.25rem !important;
		}

		.pb-1,
		.py-1 {
			padding-bottom: 0.25rem !important;
		}

		.pl-1,
		.px-1 {
			padding-left: 0.25rem !important;
		}

		.p-2 {
			padding: 0.5rem !important;
		}

		.pt-2,
		.py-2 {
			padding-top: 0.5rem !important;
		}

		.pr-2,
		.px-2 {
			padding-right: 0.5rem !important;
		}

		.pb-2,
		.py-2 {
			padding-bottom: 0.5rem !important;
		}

		.pl-2,
		.px-2 {
			padding-left: 0.5rem !important;
		}

		.p-3 {
			padding: 1rem !important;
		}

		.pt-3,
		.py-3 {
			padding-top: 1rem !important;
		}

		.pr-3,
		.px-3 {
			padding-right: 1rem !important;
		}

		.pb-3,
		.py-3 {
			padding-bottom: 1rem !important;
		}

		.pl-3,
		.px-3 {
			padding-left: 1rem !important;
		}

		.p-4 {
			padding: 1.5rem !important;
		}

		.pt-4,
		.py-4 {
			padding-top: 1.5rem !important;
		}

		.pr-4,
		.px-4 {
			padding-right: 1.5rem !important;
		}

		.pb-4,
		.py-4 {
			padding-bottom: 1.5rem !important;
		}

		.pl-4,
		.px-4 {
			padding-left: 1.5rem !important;
		}

		.p-5 {
			padding: 3rem !important;
		}

		.pt-5,
		.py-5 {
			padding-top: 3rem !important;
		}

		.pr-5,
		.px-5 {
			padding-right: 3rem !important;
		}

		.pb-5,
		.py-5 {
			padding-bottom: 3rem !important;
		}

		.pl-5,
		.px-5 {
			padding-left: 3rem !important;
		}
	</style>
</head>
<body>
	<div style="border: 1rem solid black; padding: 15px; border-radius: 1rem;">
		<table class="table text-center" style="margin-left: -10px; width: 100%">
			<tr>
				<th colspan="5" class="text-left" style="width: 33.33%;">
					<img src="{{ public_path('img/logo/logoammmec.png')}}" style="width: 150px">
				</th>
				<th colspan="5" class="text-center" style="width: 33.33%;">
					<strong>AMMMEC S.A DE C.V</strong><br>
					<strong>SOLICITUD DE VACACIONES</strong>
				</th>
				<th colspan="5" style="width: 33.33%;"></th>
			</tr>

			<tbody style="font-size: 10px;">
				<tr>
					<td colspan="12"></td>
					<td colspan="2" class="text-center" >
						<b>Solicitud No.</b>
					</td>
					<td class="bg-light" ></td>
				</tr>
				
				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="2" class="text-left">
						No de Empleado:
					</td>
					<td colspan="4" class="bg-light">
						<b>{{ $noEmpleado }}</b>
					</td>
					<td colspan="2"></td>
					<td colspan="2" class="text-left">
						Nombre del Empleado:
					</td>
					<td colspan="4" class="bg-light">
						<b>{{ $nombreUsuario }}</b>
					</td>
					<td></td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="2" class="text-left">
						Puesto:
					</td>
					<td colspan="4" class="bg-light">
						<b>{{ $puesto }}</b>
					</td>
					<td colspan="2"></td>
					<td colspan="2" class="text-left">
						Área y/o Departamento:
					</td>
					<td colspan="4" class="bg-light">
						<b>{{ $departamento }}</b>
					</td>
					<td></td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="3" class="text-left">
						Fecha de Ingreso:
					</td>
					<td colspan="5" class="bg-light"></td>
					<td colspan="3"></td>
					<td colspan="2" class="text-left">
						Años de servicio:
					</td>
					<td class="bg-light"></td>
					<td class="text-center"><b>AÑOS</b></td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="4" class="text-center">
						Días que corresponden:
					</td>
					<td colspan="3" class="bg-light"></td>
					<td colspan="2" class="text-center">
						Días a disfrutar:
					</td>
					<td colspan="2" class="bg-light"></td>
					<td colspan="2" class="text-center">
						Días pendientes:
					</td>
					<td colspan="2" class="bg-light"></td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="15" class="text-left">
						Días que Inician sus Vacaciones:
					</td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="3" class="text-right">
						del:
					</td>
					<td></td>
					<td colspan="2" class="bg-light">
					</td>
					<td class="text-center">
						de
					</td>
					<td colspan="3" class="bg-light"></td>
					<td class="text-center">
						del
					</td>
					<td colspan="3" class="bg-light"></td>
					<td></td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="3" class="text-right">
						al:
					</td>
					<td></td>
					<td colspan="2" class="bg-light"></td>
					<td class="text-center">
						de
					</td>
					<td colspan="3" class="bg-light"></td>
					<td class="text-center">
						del
					</td>
					<td colspan="3" class="bg-light"></td>
					<td></td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="9" class="text-left">
						FECHA EN QUE DEBERÁ DE PRESENTARSE A TRABAJAR:
					</td>
					
					<td colspan="6" class="bg-light"></td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="2" class="text-left">
						OBSERVACIONES:
					</td>
					
					<td colspan="13" class="bg-light text-left">
						<b>{{ $comentarios }}</b>
					</td>
				</tr>

				<tr><td colspan="15"><hr></td></tr>
				<tr>
					<td colspan="15" class="text-left">
						POR EL PRESENTE EXPRESO MI CONFORMIDAD DE SOLICITAR Y GOZAR MIS VACACIONES DE ACUERDO A LO QUE ESTABLECE EL ARTICULO 76 DE LA LEY FEDERAL DEL TRABAJO, CONSIDERANDO LOS SIGUIENTES DATOS:
					</td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="3" class="text-left">
						<b>Nombre:</b>
					</td>
					<td colspan="5" class="bg-light">
						<b>{{ $nombreUsuario }}</b>
					</td>
					<td></td>
					<td class="bg-light"></td>
					<td class="text-center"> A </td>
					<td colspan="2" class="bg-light"></td>
					<td class="text-center"> DE </td>
					<td class="bg-light"></td>
				</tr>

				<tr><td style="padding: 2px;"></td></tr>
				<tr>
					<td colspan="13" class="text-right">
						<b>Semana de capturación:</b>
					</td>
					<td colspan="2" class="bg-light"></td>
				</tr>
			</tbody>

			<tfoot style="font-size: 10px;">
				<tr>
					<td colspan="15">
						<table style="width: 100%;">
							<tr>
								<td class="pt-2 bg-light">
									<b>{{ $nombreUsuario }}</b>
								</td>
								<td class="py-2"></td>
								<td class="py-2"></td>
							</tr>
							<tr>
								<td>
									<hr class="mt-0">
								</td>
								<td>
									<hr>
								</td>
								<td>
									<hr>
								</td>
							</tr>
							<tr>
								<td>
									<small><b>Firma de Conformidad</b></small><br>
									<small><b>del Empleado</b></small>
								</td>
								<td>
									<small><b>Firma del Jefe Inmediato</b></small>
								</td>
								<td>
									<small><b>Firma de Capturación</b></small><br><small><b>Finanzas</b></small>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</tfoot> 
		</table>
	</div>
</body>
</html>