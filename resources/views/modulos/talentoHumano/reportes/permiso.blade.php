<!DOCTYPE html>
<html>
<head>
	<title>Solicitud</title>
	<style>
		body {
			margin: 0;
			font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
			font-size: 1rem;
			font-weight: 400;
			line-height: 1.5;
			color: rgb(69, 85, 95) !important;
			text-align: left;
			background-color: #fff;
		}

		.bg-light {
			background-color: rgb(159, 161, 164, .1) !important;
        	color: rgb(69, 85, 95) !important;
		}

		.bg-danger {
			background-color: rgb(192, 0 ,0) !important;
        	color: white !important;
		}

		.border-bottom {
			border-bottom: 1px solid rgb(192, 0 ,0) !important;
		}

		.text-danger {
			color: rgb(192, 0 ,0) !important;
		}

		.table {
			border-collapse: collapse !important;
			/*border: 1px solid #dee2e6;*/
		}
		.table-bordered th,
		.table-bordered td {
			border: 1px solid #dee2e6;
		}

		small{
			font-size: 10px;
		}

		small.small{
			font-size: 9px !important;
		}

		small.small2{
			font-size: 8px !important;
		}

		td{
			padding: 5px 5px 5px 5px;
			margin: 0px;
		}

		.text-left {
			text-align: left !important;
		}

		.text-right {
			text-align: right !important;
		}

		.text-center {
			text-align: center !important;
		}

		.d-flex {
			display: -ms-flexbox !important;
			display: flex !important;
		}

		.justify-content-center {
			-ms-flex-pack: center !important;
			justify-content: center !important;
		}

		.row {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			margin-right: -15px;
			margin-left: -15px;
		}

		.p-0 {
			padding: 0 !important;
		}

		.pt-0,
		.py-0 {
			padding-top: 0 !important;
		}

		.pr-0,
		.px-0 {
			padding-right: 0 !important;
		}

		.pb-0,
		.py-0 {
			padding-bottom: 0 !important;
		}

		.pl-0,
		.px-0 {
			padding-left: 0 !important;
		}

		.p-1 {
			padding: 0.25rem !important;
		}

		.pt-1,
		.py-1 {
			padding-top: 0.25rem !important;
		}

		.pr-1,
		.px-1 {
			padding-right: 0.25rem !important;
		}

		.pb-1,
		.py-1 {
			padding-bottom: 0.25rem !important;
		}

		.pl-1,
		.px-1 {
			padding-left: 0.25rem !important;
		}

		.p-2 {
			padding: 0.5rem !important;
		}

		.pt-2,
		.py-2 {
			padding-top: 0.5rem !important;
		}

		.pr-2,
		.px-2 {
			padding-right: 0.5rem !important;
		}

		.pb-2,
		.py-2 {
			padding-bottom: 0.5rem !important;
		}

		.pl-2,
		.px-2 {
			padding-left: 0.5rem !important;
		}

		.p-3 {
			padding: 1rem !important;
		}

		.pt-3,
		.py-3 {
			padding-top: 1rem !important;
		}

		.pr-3,
		.px-3 {
			padding-right: 1rem !important;
		}

		.pb-3,
		.py-3 {
			padding-bottom: 1rem !important;
		}

		.pl-3,
		.px-3 {
			padding-left: 1rem !important;
		}

		.p-4 {
			padding: 1.5rem !important;
		}

		.pt-4,
		.py-4 {
			padding-top: 1.5rem !important;
		}

		.pr-4,
		.px-4 {
			padding-right: 1.5rem !important;
		}

		.pb-4,
		.py-4 {
			padding-bottom: 1.5rem !important;
		}

		.pl-4,
		.px-4 {
			padding-left: 1.5rem !important;
		}

		.p-5 {
			padding: 3rem !important;
		}

		.pt-5,
		.py-5 {
			padding-top: 3rem !important;
		}

		.pr-5,
		.px-5 {
			padding-right: 3rem !important;
		}

		.pb-5,
		.py-5 {
			padding-bottom: 3rem !important;
		}

		.pl-5,
		.px-5 {
			padding-left: 3rem !important;
		}
	</style>
</head>
<body>
	<div>
		<table class="table text-center" style="margin-left: -10px; width: 100%">
			<tr>
				<th colspan="5" class="text-left">
					<strong>SOLICITUD DE PERMISOS LABORALES</strong>
				</th>
				<th colspan="2" class="text-right">
					<img src="{{ public_path('img/logo/logoammmec.png')}}" style="width: 100px">
				</th>
			</tr>

			<tbody style="font-size: 10px;">
				<tr>
					<td colspan="6"></td>
					<td class="text-center" >
						<b>FOLIO N</b>
					</td>
				</tr>
				<tr>
					<td colspan="6"></td>
					<td class="bg-light" >
						
					</td>
				</tr>
				<tr>
					<td colspan="5"></td>
					<td class="text-center" >
						<b>FECHA DE SOLICITUD</b>
					</td>
					<td></td>
				</tr>
				<tr>
					<td colspan="5"></td>
					<td class="bg-light" >
						{{ $fechaSolicitud }}
					</td>
					<td></td>
				</tr>

				<tr>
					<td class="text-left" colspan="4">
						<b>NOMBRE COMPLETO</b>
					</td>
					<td></td>
					<td class="text-left" colspan="2">
						<b>N. DE EMPLEADO</b>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="bg-light"> 
						{{ $nombreUsuario }}
					</td>
					<td></td>
					<td colspan="2" class="bg-light">
						{{ $noEmpleado }}
					</td>
				</tr>

				<tr>
					<td class="text-left" colspan="4">
						<b>PUESTO</b>
					</td>
					<td></td>
					<td class="text-left" colspan="2">
						<b>DEPARTAMENTO</b>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="bg-light"> 
						{{ $puesto }}
					</td>
					<td></td>
					<td colspan="2" class="bg-light">
						{{ $departamento }}
					</td>
				</tr>

				<tr>
					<td colspan="4">
						<b>FECHA DE PERMISO</b>
					</td>
				</tr>

				<tr>
					<td class="text-right" colspan="3">
						DEL
					</td>
					<td class="bg-light">
						{{ $fechaInicio }}
					</td>
					<td class="text-right">
						AL
					</td>
					<td class="bg-light">
						
					</td>
					<td></td>
				</tr>

				<tr>
					<td colspan="4">
						<b>TIEMPO QUE PRETENDE AUSENTARSE</b>
					</td>
				</tr>

				<tr>
					<td class="text-right" colspan="3">
						DE LAS
					</td>
					<td class="bg-light">
						{{ $horaInicio }}
					</td>
					<td class="text-right">
						A LAS
					</td>
					<td class="bg-light">
						{{ $horaFin }}
					</td>
					<td></td>
				</tr>

				<tr>
					<td class="text-left" colspan="4">
						<b>MOTIVO DE AUSENCIA</b>
					</td>
					<td></td>
					<td class="text-left" colspan="2">
						<b>PARENTESCO</b>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="bg-light"> 
						{{ $motivoAusencia }}
					</td>
					<td></td>
					<td colspan="2" class="bg-light">
						
					</td>
				</tr>


				<tr>
					<td class="text-left" colspan="7">
						<b>COMENTARIOS</b>
					</td>
				</tr>
				<tr>
					<td colspan="7" class="bg-light">
						{{ $comentarios }}
					</td>
				</tr>

				<tr>
					<td colspan="7"></td>
				</tr>

				<tr>
					<td class="text-right" colspan="6">
						CAPTURADA EN LA SEMANA
					</td>
					<td class="bg-light">
						@foreach (json_decode($semanasCapturacion) as $item)
							<label><b>{{ $item }}</b></label>
						@endforeach
					</td>
				</tr>
			</tbody>

			<tfoot style="font-size: 10px;">
				<tr>
					<td colspan="7">
						<table style="width: 100%;">
							<tr>
								<td class="py-4"></td>
								<td class="py-4"></td>
								<td class="py-4"></td>
							</tr>
							<tr>
								<td>
									<hr>
									<small class="small">
										
									</small>
								</td>
								<td>
									<hr>
									<small class="small">
										
									</small>
								</td>
								<td>
									<hr>
									<small class="small">
										
									</small>
								</td>
							</tr>
							<tr>
								<td>
									<small>FIRMA DEL TRABAJADOR</small>
								</td>
								<td>
									<small>NOMBRE Y FIRMA</small><br><small>JEFE INMEDIATO</small>
								</td>
								<td>
									<small>NOMBRE Y FIRMA</small><br><small>TALENTO HUMANO</small>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</tfoot>
		</table>
	</div>
</body>
</html>