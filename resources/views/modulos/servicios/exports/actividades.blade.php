@if ($area == "OP")
	<table>
		<thead>
			<tr>
				<th style="width: 15px; text-align: center;"><strong>Semana</strong></th>
				<th style="width: 17px; text-align: center;"><strong>No Empleado</strong></th>
				<th style="width: 30px;"><strong>Empleado</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Mes</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Día</strong></th>
				<th style="width: 15px; text-align: center;"><strong>No Día</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Total de Horas</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Tipo Cuenta</strong></th>
				<th style="width: 18px; text-align: center;"><strong>Línea Negocio</strong></th>
				<th style="width: 30px; text-align: center;"><strong>Cuenta</strong></th>
			</tr>
		</thead>
		<tbody>
			@foreach($empleados as $item)
				<tr>
					<td style="text-align: center;">{{ $item['semana'] }}</td>
					<td style="text-align: center;">{{ $item['noEmpleado'] }}</td>
					<td>{{ $item['empleado'] }}</td>
					<td style="text-align: center;">{{ $item['mes'] }}</td>
					<td style="text-align: center;">{{ $item['dia'] }}</td>
					<td style="text-align: center;">{{ $item['noDia'] }}</td>
					<td style="text-align: center;">{{ $item['totalH'] }}</td>
					<td style="text-align: center;">{{ $item['tipoCuenta'] }}</td>
					<td style="text-align: center;">{{ $item['lineaNegocio'] }}</td>
					<td style="text-align: center;">{{ $item['cuenta'] }}</td>
				</tr>
			@endforeach

			{{-- @foreach($empleados as $empleado)
				@foreach($empleado->semana as $dia)
					<tr>
						<td style="text-align: center;">{{ $dia['semana'] }}</td>
						<td style="text-align: center;">{{ $empleado->consecutivo }}</td>
						<td>{{ $empleado->nombre }}</td>
						<td style="text-align: center;">{{ $dia['mes'] }}</td>
						<td style="text-align: center;">{{ $dia['dia'] }}</td>
						<td style="text-align: center;">{{ $dia['noDia'] }}</td>
						<td style="text-align: center;">{{ $dia['totalH'] }}</td>
						<td style="text-align: center;">{{ $dia['tipoCuenta'] }}</td>
						<td style="text-align: center;">{{ $dia['lineaNegocio'] }}</td>
						<td style="text-align: center;">{{ $dia['cuenta'] }}</td>
					</tr>
				@endforeach
			@endforeach --}}
		</tbody>
	</table>
@else
	<table>
		<thead>
			<tr>
				<th style="width: 15px; text-align: center;"><strong>Semana</strong></th>
				<th style="width: 17px; text-align: center;"><strong>Núm. de empleado</strong></th>
				<th style="width: 30px;"><strong>Empleado</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Mes</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Día</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Número de día</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Horas</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Extras</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Total de Horas</strong></th>
				<th style="width: 15px; text-align: center;"><strong>Incidencias</strong></th>
				<th style="width: 18px; text-align: center;"><strong>Tipo Cuenta</strong></th>
				<th style="width: 40px;"><strong>Cuenta</strong></th>
				<th style="width: 20px;"><strong>Observaciones</strong></th>
			</tr>
		</thead>
		<tbody>
			@foreach($empleados as $empleado)
				@foreach($empleado->semana as $dia)
					<tr>
						<td style="text-align: center;">{{ $dia['semana'] }}</td>
						<td style="text-align: center;">{{ $empleado->consecutivo }}</td>
						<td>{{ $empleado->nombre }}</td>
						<td style="text-align: center;">{{ $dia['mes'] }}</td>
						<td style="text-align: center;">{{ $dia['dia'] }}</td>
						<td style="text-align: center;">{{ $dia['noDia'] }}</td>
						<td style="text-align: center;">{{ $dia['horas'] }}</td>
						<td style="text-align: center;">{{ $dia['extras'] }}</td>
						<td style="text-align: center;">{{ $dia['totalH'] }}</td>
						<td></td>
						<td>{{ $dia['tipoCuenta'] }}</td>
						<td>{{ $dia['cuentas'] }}</td>
						<td></td>
					</tr>
				@endforeach
			@endforeach
		</tbody>
	</table>
@endif