<!DOCTYPE html>
<html>
<head>
	<title>Reporte de actividades</title>
	<style>

		body {
			margin: 0;
			font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
			font-size: 1rem;
			font-weight: 400;
			line-height: 1.5;
			color: rgb(69, 85, 95) !important;
			text-align: left;
			background-color: #fff;
		}

		.bg-light {
			background-color: rgb(159, 161, 164, .1) !important;
        	color: rgb(69, 85, 95) !important;
		}

		.bg-danger {
			background-color: rgb(192, 0 ,0) !important;
        	color: white !important;
		}

		.border-bottom{
			border-bottom: 1px solid rgb(192, 0 ,0) !important;
		}

		.text-danger {
			color: rgb(192, 0 ,0) !important;
		}

		.table {
			border-collapse: collapse !important;
		}
		.table-bordered th,
		.table-bordered td {
			border: 1px solid #dee2e6;
		}

		small{
			font-size: 10px;
		}

		small.small{
			font-size: 9px !important;
		}

		td{
			padding: 5px 5px 5px 5px;
			margin: 0px;
		}

		.text-left {
			text-align: left !important;
		}

		.text-right {
			text-align: right !important;
		}

		.text-center {
			text-align: center !important;
		}

		.d-flex {
			display: -ms-flexbox !important;
			display: flex !important;
		}

		.justify-content-center {
			-ms-flex-pack: center !important;
			justify-content: center !important;
		}

		.row {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			margin-right: -15px;
			margin-left: -15px;
		}

		.p-0 {
			padding: 0 !important;
		}

		.pt-0,
		.py-0 {
			padding-top: 0 !important;
		}

		.pr-0,
		.px-0 {
			padding-right: 0 !important;
		}

		.pb-0,
		.py-0 {
			padding-bottom: 0 !important;
		}

		.pl-0,
		.px-0 {
			padding-left: 0 !important;
		}

		.p-1 {
			padding: 0.25rem !important;
		}

		.pt-1,
		.py-1 {
			padding-top: 0.25rem !important;
		}

		.pr-1,
		.px-1 {
			padding-right: 0.25rem !important;
		}

		.pb-1,
		.py-1 {
			padding-bottom: 0.25rem !important;
		}

		.pl-1,
		.px-1 {
			padding-left: 0.25rem !important;
		}

		.p-2 {
			padding: 0.5rem !important;
		}

		.pt-2,
		.py-2 {
			padding-top: 0.5rem !important;
		}

		.pr-2,
		.px-2 {
			padding-right: 0.5rem !important;
		}

		.pb-2,
		.py-2 {
			padding-bottom: 0.5rem !important;
		}

		.pl-2,
		.px-2 {
			padding-left: 0.5rem !important;
		}

		.p-3 {
			padding: 1rem !important;
		}

		.pt-3,
		.py-3 {
			padding-top: 1rem !important;
		}

		.pr-3,
		.px-3 {
			padding-right: 1rem !important;
		}

		.pb-3,
		.py-3 {
			padding-bottom: 1rem !important;
		}

		.pl-3,
		.px-3 {
			padding-left: 1rem !important;
		}

		.p-4 {
			padding: 1.5rem !important;
		}

		.pt-4,
		.py-4 {
			padding-top: 1.5rem !important;
		}

		.pr-4,
		.px-4 {
			padding-right: 1.5rem !important;
		}

		.pb-4,
		.py-4 {
			padding-bottom: 1.5rem !important;
		}

		.pl-4,
		.px-4 {
			padding-left: 1.5rem !important;
		}

		.p-5 {
			padding: 3rem !important;
		}

		.pt-5,
		.py-5 {
			padding-top: 3rem !important;
		}

		.pr-5,
		.px-5 {
			padding-right: 3rem !important;
		}

		.pb-5,
		.py-5 {
			padding-bottom: 3rem !important;
		}

		.pl-5,
		.px-5 {
			padding-left: 3rem !important;
		}

	</style>

</head>
<body>
	<div class="">
		<table class="table table-bordered" style="margin-left: -10px; width: 100%">
				<tr>
					<th width="200px">
						<img src="{{ public_path('img/logo/logoammmec.png')}}" style="width: 100px">
					</th>
					<th class="text-danger">
						<strong>Resumen de Cuentas Semanales</strong>
					</th>
				</tr>
				<tr class="bg-danger">
					<td colspan="2"  class="text-left pb-2" style="border-color: rgb(192, 0 ,0) !important;">
						<small><strong>1. Cuentas registradas en Semana 30:</strong></small>
					</td>
				</tr>
			<tbody>
				<tr class="bg-light">
					<td colspan="2" class="pl-3">
						<strong>Cuentas Externas</strong>
					</td>
				</tr>
				@foreach ($cuentas as $cuenta)
				@if($cuenta['tipoCuenta'] == 'Externa')
				<tr>
					<td colspan="2" class="pl-5">
						<small>{{ $cuenta['cuenta'] }}</small>
					</td>
				</tr>
				@endif
				@endforeach
				<tr class="bg-light">
					<td colspan="2" class="pl-3">
						<strong>Cuentas Internas</strong>
					</td>
				</tr>
				@foreach ($cuentas as $cuenta)
				@if($cuenta['tipoCuenta'] == 'Interna')
				<tr>
					<td colspan="2" class="pl-5">
						<small>{{ $cuenta['cuenta'] }}</small>
					</td>
				</tr>
				@endif
				@endforeach
				<tr class="bg-light">
					<td colspan="2" class="pl-3">
						<strong>Cuentas Libres</strong>
					</td>
				</tr>
				@foreach ($cuentas as $cuenta)
				@if($cuenta['tipoCuenta'] == 'Libre')
				<tr>
					<td colspan="2" class="pl-5">
						<small>{{ $cuenta['cuenta'] }}</small>
					</td>
				</tr>
				@endif
				@endforeach
			</tbody>
		</table>
	</div>
</body>
</html>