<!DOCTYPE html>
<html>
<head>
	<title>Reporte de actividades</title>
	<style>

		body {
			margin: 0;
			font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
			font-size: 1rem;
			font-weight: 400;
			line-height: 1.5;
			color: rgb(69, 85, 95) !important;
			text-align: left;
			background-color: #fff;
		}

		.bg-light {
			background-color: rgb(159, 161, 164, .1) !important;
        	color: rgb(69, 85, 95) !important;
		}

		.bg-danger {
			background-color: rgb(192, 0 ,0) !important;
        	color: white !important;
		}

		.border-bottom{
			border-bottom: 1px solid rgb(192, 0 ,0) !important;
		}

		.text-danger {
			color: rgb(192, 0 ,0) !important;
		}

		.table {
			border-collapse: collapse !important;
		}
		.table-bordered th,
		.table-bordered td {
			border: 1px solid #dee2e6;
		}

		small{
			font-size: 10px;
		}

		small.small{
			font-size: 9px !important;
		}

		small.small2{
			font-size: 8px !important;
		}

		td{
			padding: 5px 5px 5px 5px;
			margin: 0px;
		}

		.text-left {
			text-align: left !important;
		}

		.text-right {
			text-align: right !important;
		}

		.text-center {
			text-align: center !important;
		}

		.d-flex {
			display: -ms-flexbox !important;
			display: flex !important;
		}

		.justify-content-center {
			-ms-flex-pack: center !important;
			justify-content: center !important;
		}

		.row {
			display: -ms-flexbox;
			display: flex;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			margin-right: -15px;
			margin-left: -15px;
		}

		.p-0 {
			padding: 0 !important;
		}

		.pt-0,
		.py-0 {
			padding-top: 0 !important;
		}

		.pr-0,
		.px-0 {
			padding-right: 0 !important;
		}

		.pb-0,
		.py-0 {
			padding-bottom: 0 !important;
		}

		.pl-0,
		.px-0 {
			padding-left: 0 !important;
		}

		.p-1 {
			padding: 0.25rem !important;
		}

		.pt-1,
		.py-1 {
			padding-top: 0.25rem !important;
		}

		.pr-1,
		.px-1 {
			padding-right: 0.25rem !important;
		}

		.pb-1,
		.py-1 {
			padding-bottom: 0.25rem !important;
		}

		.pl-1,
		.px-1 {
			padding-left: 0.25rem !important;
		}

		.p-2 {
			padding: 0.5rem !important;
		}

		.pt-2,
		.py-2 {
			padding-top: 0.5rem !important;
		}

		.pr-2,
		.px-2 {
			padding-right: 0.5rem !important;
		}

		.pb-2,
		.py-2 {
			padding-bottom: 0.5rem !important;
		}

		.pl-2,
		.px-2 {
			padding-left: 0.5rem !important;
		}

		.p-3 {
			padding: 1rem !important;
		}

		.pt-3,
		.py-3 {
			padding-top: 1rem !important;
		}

		.pr-3,
		.px-3 {
			padding-right: 1rem !important;
		}

		.pb-3,
		.py-3 {
			padding-bottom: 1rem !important;
		}

		.pl-3,
		.px-3 {
			padding-left: 1rem !important;
		}

		.p-4 {
			padding: 1.5rem !important;
		}

		.pt-4,
		.py-4 {
			padding-top: 1.5rem !important;
		}

		.pr-4,
		.px-4 {
			padding-right: 1.5rem !important;
		}

		.pb-4,
		.py-4 {
			padding-bottom: 1.5rem !important;
		}

		.pl-4,
		.px-4 {
			padding-left: 1.5rem !important;
		}

		.p-5 {
			padding: 3rem !important;
		}

		.pt-5,
		.py-5 {
			padding-top: 3rem !important;
		}

		.pr-5,
		.px-5 {
			padding-right: 3rem !important;
		}

		.pb-5,
		.py-5 {
			padding-bottom: 3rem !important;
		}

		.pl-5,
		.px-5 {
			padding-left: 3rem !important;
		}

	</style>

</head>
<body>
	<div class="">
		<table class="table table-bordered text-center" style="margin-left: -10px; width: 100%">
				<tr>
					<th colspan="2">
						<img src="{{ public_path('img/logo/logoammmec.png')}}" style="width: 100px">
					</th>
					<td colspan="9" class="text-danger">
						<strong>Reporte de Actividades de Servicio</strong>
					</td>
				</tr>
				<tr>
					<td colspan="2" class="border-bottom">
						<small><strong>Nombre del Técnico:</strong></small>
					</td>
					<td colspan="3" class="border-bottom">
						<small>{{ $actividades->nombreEmpleado }}</small>
					</td>
					<td colspan="2" class="border-bottom">
						<small><strong>No. Empleado:</strong></small>
					</td>
					<td class="border-bottom">
						<small>{{ $actividades->noEmpleado }}</small>
					</td>
					<td colspan="2" class="border-bottom">
						<small><strong>Semana:</strong></small>
					</td>	
					<td class="border-bottom">
						<small>{{ $actividades->noSemana }}</small>
					</td>
				</tr>
				<tr class="bg-danger">
					<td colspan="11" class="text-left pb-2" style="border-color: rgb(192, 0 ,0) !important;">
						<small><strong>1. Horas Utilizadas:</strong></small>
					</td>
				</tr>
			<tbody>
				<?php $pag = 1 ; $i = 0; ?>
				@foreach ($actividades as $actividad)
				<?php $i++; ?>
				@if($pag > 1 && $i == 1)
				<tr>
					<td colspan="11" style="padding-top : -4px; padding-bottom: -4px"></td>
				</tr>
				@endif
				<tr class="bg-light">
					<td>
						<small><strong>Día:</strong></small>
					</td>
					<td>
						<small>{{ $actividad->dia }}</small>
					</td>
					<td>
						<small><strong>Fecha:</strong></small>
					</td>
					<td colspan="2">
						<small>{{ $actividad->fecha }}</small>
					</td>
					<td>
						<small><strong>Turno:</strong> {{ $actividad->turno }}</small>
					</td>
					<td colspan="1">
						<small><strong>Equipo:</strong></small>
					</td>
					<td colspan="4" style="font-size: 8px">
						<small><small class="small2">{{ $actividad->equipo }}</small></small>
					</td>
				</tr>
				<tr class="bg-light">
					<td>
						<small><strong>Cuenta</strong></small>
					</td>
					<td colspan="2">
						<small>{{ $actividad->cuenta }}</small>
					</td>
					<td>
						<small><strong>CA:</strong></small>
					</td>
					<td>
						<small>P</small>
					</td>
					<td>
						<small><strong>Hrs Totales:</strong></small>
					</td>
					<td>
						<small>{{ $actividad->horas }}</small>
					</td>
					<td>
						<small><strong>Tipo tiempo:</strong></small>
					</td>
					<td colspan="3">
						<small>{{ $actividad->tipoTiempo }}</small>
					</td>
				</tr>
				<tr>
					<td colspan="11" height="10px" class="p-3 text-left">
						<small>
							{{ $actividad->descripcion }}
						</small>
					</td>
				</tr>
				<tr>
					<td class="py-4">
						<small><strong>Entrada:</strong></small>
					</td>
					<td>
						<small>{{ $actividad->inicio }}</small>
					</td>
					<td>
						<small><strong>Salida:</strong></small>
					</td>
					<td>
						<small>{{ $actividad->fin }}</small>
					</td>
					<td>
						<small><strong>Firma:</strong></small>
					</td>
					<td colspan="2">
						
					</td>
					<td>
						<small><strong>Huella:</strong></small>
					</td>
					<td colspan="3">
						
					</td>
				</tr>
				@if(($pag == 1 && $i == 5) || ($pag > 1 && $i==6))
				<tr>
					<td colspan="11" style="border-color: rgb(255, 255 ,255, 0) !important;">
					<div style="page-break-after:always;"></div>
					<?php $pag++; $i=0; ?>
					</td>
				</tr>
				@endif 
				@endforeach
				<tr class="bg-light">
					<td colspan="3">
						<small><strong>Nombre y Firma del Técnico</strong></small>
					</td>
					<td colspan="4">
						<small><strong>Nombre y Firma del Ing. de Servicio</strong></small>
					</td>
					<td colspan="4">
						<small><strong>Nombre y Firma del Lider de Servicio</strong></small>
					</td>
				</tr>
				<tr>
					<td colspan="3" class="py-4">
						
					</td>
					<td colspan="4">
						
					</td>
					<td colspan="4">
						
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<small class="small">Realizó</small>
					</td>
					<td colspan="4">
						<small class="small">Revisó</small>
					</td>
					<td colspan="4">
						<small class="small">Autorizó</small>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</body>
</html>