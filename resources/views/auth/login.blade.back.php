<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="icon" href="{{ asset('/img/logo/logo-blanco.png') }}">


    <title>ERP | LOGIN</title>
<!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet">
    <link href="{{ asset('css/sign-in/signin.css') }}" rel="stylesheet">
    
    <style>
        body{
          background-image: url({{ asset('img/logo/fondo-login.jpg') }});
          background-position: center;   
          background-repeat: no-repeat;
          background-attachment: fixed;
          background-size: cover;
          height: 100vh;
        }
        .btn-danger {
            color: #fff;
            background-color: #af222f;
            border-color: #af222f;
        } 

    </style>
</head>

<body class="bg-light">
    <form method="POST" action="{{ route('login') }}" class="form-signin bg-light p-5 mt-3">
        {{ csrf_field() }}
         <div class="row text-left">
            <div class="col-7 mt-2">
                <h1 class="text-secondary">ERP AMMMEC</h1>
            </div>
             <div class="col-5">
                      <img src="{{ asset('img/logo/logoammmec.png') }}" alt="LOGO-AMMMEC" class="center-block float-right imag" style="max-width: 100%; margin-top: -20px">
            </div>
        </div>
        @if(session()->has('flash'))
        <div class="alert alert-info">{{ session('flash') }}</div>
        @endif
        <div class="panel-heading">
            <div class="row text-left">
                <div class="col-12">
                    <h3 class="text-secondary">Inicio de Sesión</h3>
                    <p class="text-secondary">Ingresa tu correo y contraseña para acceder:
                    </p>    
                </div>
            </div>  
        </div>
        <div class="form-group">
            <label for="inputEmail" class="sr-only">Correo</label>
            <input type="email" name="email" id="inputEmail" value="{{ old('email') }}" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Correo electrónico" autofocus>
            {!! $errors->first('email', '<div class="invalid-feedback mb-2 mt-2">:message</div>') !!}

            <label for="inputPassword" class="sr-only">Contraseña</label> 
            <input name="password" type="password" id="inputPassword" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Contraseña" >
            {!! $errors->first('password', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <button class="btn btn-lg btn-danger btn-block" type="submit">Acceder</button>
        {{-- <a class="btn btn-lg btn-light btn-block" href="{{ route('register') }}">Registrarse</a> 
        <a class="btn btn-lg btn-light btn-block pt-0 mt-0" href="?c=inicio">Olvide mi contraseña</a> --}}
    </form>
</body>
</html>