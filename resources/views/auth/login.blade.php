<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('/img/logo/logo-not3.png') }}">

    <title>ERP AMMMEC</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" rel="stylesheet">
    <link href="{{ asset('css/sign-in/signin.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sign-in/floating-labels.css') }}" rel="stylesheet">
    
    <style>
        body{
          background-image: url({{ asset('img/system/fondo-login.jpg') }});
          background-position: center;   
          background-repeat: no-repeat;
          background-attachment: fixed;
          background-size: cover;
          height: 100vh;
        }        

        .btn-danger{
            background-color: rgb(227, 27, 35) !important
        }
        
        .text-dark{
            color: rgb(69, 85, 95) !important;
        }

        .text-dark-90{
            color: rgb(69, 85, 95, .9) !important;
        }

        .text-secondary{
           color: rgb(69, 85, 95, .85) !important;
        }

        .btn-dark{
            background-color: rgb(69, 85, 95) !important;
            color: white;
        }

    </style>
</head>

<body class="bg-light">

    <form method="POST" action="{{ route('login') }}" class="form-signin bg-white form-height shadow">
        {{ csrf_field() }}
        <div class="d-none d-sm-none d-md-block">
             <div class="row" style="margin-bottom: -40px">
                <div class="col-md-4 col-lg-6">
                    <h3 class="text-secondary">ERP AMMMEC</h3>
                </div>
                <div class="col-md-8 col-lg-6">
                    <img src="{{ asset('img/logo/logoammmec.png') }}" alt="LOGO-AMMMEC" class="center-block float-right imag" style="max-width: 80%; margin-top: -20px">
                </div>
            </div>
        </div>
        
        <div class="d-block d-sm-block d-md-none">
            <div class="row" style="margin-bottom: 3px">
                 <div class="col-sm-12">
                     <center>
                        <img src="{{ asset('img/logo/logoammmec.png') }}" alt="LOGO-AMMMEC" class="center-block imag" style="max-width: 50%; margin-top: -20px">
                    </center>
                </div>
                <div class="col-sm-12">
                    <h3 class="text-secondary text-center">ERP AMMMEC</h3>
                </div>
               
            </div>
        </div>
        
         
        @if(session()->has('flash'))
        <div class="alert alert-warning">{{ session('flash') }}</div>
        @endif
        <div class="panel-heading">
            <div class="row text-left">
                <div class="col-12">

                    <h4 class="text-secondary">Iniciar sesión</h4>
                    <p class="text-secondary">Ingresa tu número de empleado y contraseña para acceder:
                    </p>    
                </div>
        </div>
        <div class="form-label-group">
            <input name="noEmpleado" type="text" id="inputEmail" class="form-control {{ $errors->has('noEmpleado') ? 'is-invalid' : '' }}" placeholder="Correo electrónico" value="{{ old('noEmpleado') }}" autofocus>
            <label for="inputEmail">Usuario</label>
            {!! $errors->first('noEmpleado', '<div class="invalid-feedback mb-2 mt-2">:message</div>') !!}
        </div>

        <div class="form-label-group">
            <input name="password" type="password" id="inputPassword" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="Correo electrónico" value="{{ old('password') }}" autofocus>
            <label for="inputPassword">Contraseña</label>
            {!! $errors->first('password', '<div class="invalid-feedback mb-2 mt-2">:message</div>') !!}
        </div>

        <button class="btn btn-lg btn-dark btn-block mt-3 mb-1" type="submit">Acceder</button>
       {{--  <a class="btn btn-lg btn-light btn-block" href="{{ route('register') }}">Registrarse</a> 
        <a class="btn btn-lg btn-light btn-block pt-0 mt-0" href="?c=inicio">Olvide mi contraseña</a> --}}
    </form>

</body>
</html>


<style>
    @media only screen and (max-width: 700px) {
        body{
            height: calc(100vh - 100px)
        }
        .form-height{
            height: 445px;
            padding: 35px 20px 30px 20px;
        }
    }

      @media only screen and (min-width: 700px) {
        .form-height{
            padding: 40px;
        }
    }
</style>
