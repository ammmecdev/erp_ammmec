@extends('errors::layout')

@section('title', 'Pagina Expirada')

@section('message')
    Esta página ha expirado por inactividad.
    <br/><br/>
    Haga click a este link para continuar <a href="https://erp.ammmec.com">ERP-AMMMEC</a>
@stop
