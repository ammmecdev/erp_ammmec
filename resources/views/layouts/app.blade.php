<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="{{ asset('css/sidebar/style5.css') }}">
    <link id="favicon" rel="icon" href="{{ asset('/img/logo/isologo.png') }}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.13.1/css/v4-shims.css">
    <link href="{{ asset('/css/main.css') }}" rel="stylesheet" />
    <title>{{ config('app.name', 'Laravel') }}</title>
</head>
<body class="bg-light">
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <div id="app">
        {{-- <app :usuario="{{ Auth::user() }}" :not-count="{{ Auth::user()->unreadNotifications->count() }}" :module="'{{ $module }}'" /> --}}
        <app :usuario="{{ Auth::user() }}" :module="'{{ $module }}'" />                    
    </div>

    <!-- Scripts -->
    <script src="{{ asset('/js/app.js') }}"></script>
</body>
</html>
