<?php

namespace App;

use App\Events\NotificationSend;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
		'id', 'data' 
	];

	 protected $dispatchesEvents = [
        'saved' => NotificationSend::class,
    ];
}
