<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empleado extends Model
{
    protected $primaryKey = 'noEmpleado';
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';

    public function puesto()
    {
        return $this->belongsTo(Puesto::class, 'idPuesto');
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'noEmpleado');
    }
}


