<?php

namespace App\Notifications;

use App\Actividad;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CreatedActividad extends Notification
{
    use Queueable;

    public $actividades;
    public $user;
    public $detail;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($actividades, $user)
    {
        $this->actividades = $actividades;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        $idActividad = null;
        $not_name = explode(" " , $this->user->nombreUsuario);

        foreach($this->actividades as $actividad){
             $idActividad .= $actividad->idActividad . '-';
        }

        $ruta = json_encode($this->actividades);
        return (new MailMessage)
        ->from('jerocor54@gmail.com')
        ->subject('Actividades de hoy ')
        ->markdown('mails.mail', ['actividades' => $this->actividades, 'user' => $not_name])
        ->action('Ver actividades', url('/ventas/actividades?ia=' . $idActividad))
        ->salutation('ERP-AMMMEC ');
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
         //
        ];
    }
}
