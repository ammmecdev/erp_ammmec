<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CreateNota extends Notification
{
    use Queueable;

    public $nota;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($nota)
    {
        $this->nota = $nota;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $not_name = explode(" " , $notifiable->nombreUsuario);
        $nombreUsuario = explode(" " , $this->nota->nombre_usuario);

        return [
            'user' => $not_name[0],
            'text' => $nombreUsuario[0] . ' creo una nota.',
            'icon' => 'far fa-clock',
            'picture' => '/img/profiles/avatar.png',
            'route_name' => '/ventas/negocios/notas',
            'id' => $this->nota->idNota,
        ];
    }
}
