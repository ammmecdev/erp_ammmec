<?php

namespace App\Notifications\Finanzas\CG;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AsignacionSolicitud extends Notification
{
    use Queueable;

    private $solicitud;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($solicitud)
    {
        $this->solicitud = $solicitud;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $asignador = $this->solicitud->nombre_asignador;
        $notName = explode(" ", $notifiable->nombreUsuario);
        return (new MailMessage)
            ->from('erp@ammmec.com')
            ->subject('Asignación de Viático')
            ->markdown('mails.finanzas.cg.asignacion_solicitud', ['solicitud' => $this->solicitud, 'user' => $notName, 'asignador' => $asignador]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
