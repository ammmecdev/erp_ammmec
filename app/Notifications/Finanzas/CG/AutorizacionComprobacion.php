<?php

namespace App\Notifications\Finanzas\CG;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AutorizacionComprobacion extends Notification
{
    use Queueable;
    private $usuario;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($usuario)
    {
        $this->usuario = $usuario;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notName = explode(" ", $notifiable->nombreUsuario);
        $mensaje = ($this->usuario->contador > 1) ? $this->usuario->contador . " autorizaciones " : "una autorización "; 
        return (new MailMessage)
                    ->from('erp@ammmec.com')
                    ->subject('Autorizaciones pendientes')
                    ->greeting('Hola ' . $notName[0] . ',')
                    ->line('Tienes ' . $mensaje . ' de gastos que requieren de tu atención')
                    ->action('Ir a solicitudes', url('https://erp.ammmec.com/finanzas/cg/solicitudes'))
                    ->line('Agradecemos tu colaboración.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
