<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CreateComentario extends Notification
{
    use Queueable;

    public  $comentario;
   


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($comentario)
    {
        $this->comentario = $comentario;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        //
    }


    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $not_name = explode(" " , $notifiable->nombreUsuario);
        $nombreUsuario = explode(" " , $this->comentario->nombreUsuario);

        return [
            'user' => $not_name[0],
            'text' => $nombreUsuario[0] . ' agrego un comentario a la nota ' . $this->comentario->nombreNota . '.',
            'icon' => 'far fa-clock',
            'picture' => '/img/profiles/avatar.png',
            'route_name' => '/ventas/negocios/notas',
            'id' => $this->comentario->idComentario
        ];
        
    }
}
