<?php

namespace App\Notifications\Tickets;

use App\Models\Tickets\Ticket;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class TicketSend extends Notification
{
    use Queueable;
    protected $ticket;
    protected $message;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Ticket $ticket, $message)
    {
        $this->ticket = $ticket;
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    
    public function toMail($notifiable)
    {
        $not_name = explode(" " , $notifiable->nombreUsuario);

        return (new MailMessage)
        ->from('erp@ammmec.com')
        ->subject('Nuevo ticket de ' . $this->ticket->user->nombreUsuario)
        ->greeting('Hola ' . $not_name[0] . ',')
        ->line('Tienes un nuevo ticket de ' . $this->ticket->user->nombreUsuario . ':')
        ->line($this->ticket->subject)
        ->line($this->message)
        ->action('Ver ticket', route('tickets.show.admin',$this->ticket->id))
        ->salutation('TICKETS AMMMEC');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'text' => "<b>" . $this->ticket->user->nombreUsuario. "</b> ha creado un nuevo ticket para el departamento de " . $this->ticket->area->nombre,
            'icon' => "<i class='far fa-sticky-note'></i>",
            // 'picture' => $this->ticket->user->picture,
            'route_name' => 'tickets.show_admin',
            'id' => $this->ticket->id
        ];
    }
}
