<?php

namespace App\Notifications\Tickets;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UnreadMessages extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = '/tickets'; 
        $not_name = explode(" " , $notifiable->nombreUsuario);
        return (new MailMessage)
        ->from('erp@ammmec.com')
        ->greeting($not_name[0])
        ->subject('Aviso de mensajes pendientes | SISTEMA DE TICKETS')
        ->line('Tienes mensajes sin leer en Tickets.')
        ->line('Nos interesa que sigas conectado para darle seguimiento a los asuntos pendientes.')
        ->line('Si a primera instancia no vez tickets con mensajes sin leer, puedes aplicar un filtro para ver todos los tickets y ver cuales son los que tienen mensajes sin leer.')
        ->action('Ir a tickets', url($url))
        ->line('Agradecemos tu colaboración,')
        ->salutation('ERP-AMMMEC');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
