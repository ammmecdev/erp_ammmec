<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CreateActividad extends Notification
{
    use Queueable;

    public  $actividad;
   


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($actividad)
    {
        $this->actividad = $actividad;

    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */

    public function toMail($notifiable)
    {
        //
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $not_name = explode(" " , $notifiable->nombreUsuario);
        $name_user = explode(" " , $this->actividad->name_user);

        return [
            'user' => $not_name[0],
            'text' => $name_user[0] . ' te asigno una actividad.',
            'icon' => 'far fa-clock',
            'picture' => '/img/profiles/avatar.png',
            'route_name' => '/ventas/actividades',
            'id' => $this->actividad->idActividad
        ];
    }
}
