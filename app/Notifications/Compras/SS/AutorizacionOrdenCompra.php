<?php

namespace App\Notifications\Compras\SS;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AutorizacionOrdenCompra extends Notification
{
    use Queueable;
    private $ordenCompra;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ordenCompra)
    {
        $this->ordenCompra = $ordenCompra;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notName = explode(" ", $notifiable->nombreUsuario);
        return (new MailMessage)
            ->from('erp@ammmec.com')
            ->subject('Autorización de orden de compra #'.$this->ordenCompra->idOrdenCompra)
            ->markdown('mails.compras.ss.autorizacion_orden_compra', [
                'orden' => $this->ordenCompra, 
                'user' => $notName]
            );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
