<?php

namespace App\Notifications\Compras\SS;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrdenCompraRechazada extends Notification
{
    use Queueable;
    private $ordenCompra;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($ordenCompra)
    {
        $this->ordenCompra = $ordenCompra;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notName = explode(" ", $notifiable->nombreUsuario);
        return (new MailMessage)
            ->from('erp@ammmec.com')
            ->subject('Orden de compra #'.$this->ordenCompra->idOrdenCompra.' rechazada')
            ->markdown('mails.compras.ss.orden_compra_rechazada', [
                'orden' => $this->ordenCompra, 
                'user' => $notName
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
