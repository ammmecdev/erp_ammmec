<?php

namespace App\Notifications\Compras\SS;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewSolicitud extends Notification
{
    use Queueable;
    private $solicitud;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($solicitud)
    {
        $this->solicitud = $solicitud;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $notName = explode(" ", $notifiable->nombreUsuario);
        $user_solicita = $this->solicitud->usuario->nombreUsuario;   
        return (new MailMessage)
                    ->from('erp@ammmec.com')
                    ->subject('Nueva solicitud de suministros')
                    ->greeting('Hola ' . $notName[0] . ',')
                    ->line('Tienes una nueva solicitud de suministros de '.$user_solicita)
                    ->action('Ir a solicitudes', url('https://erp.ammmec.com/compras/ss/solicitudes'))
                    ->line('Agradecemos tu colaboración.')
                    ->salutation('ERP-AMMMEC');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
