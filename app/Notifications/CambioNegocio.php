<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CambioNegocio extends Notification
{
    public $negocio;
    public $user;
    public $urlImage = null;
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($negocio, $user)
    {
         $this->negocio = $negocio;
         $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */

    public function urlImagen($user)
    {
            $urlImage = '/img/profiles/' . $user->foto;
    }

    public function toArray($notifiable)
    {
        $nombreUsuario = explode(" " , $notifiable->nombreUsuario);
        $nameUser = explode(" " , $this->user->nombreUsuario);

        //$this->urlImagen($this->user->foto);
        
         return [
            'user' => $nombreUsuario[0],
            'text' => $nameUser[0] . ' ha cambiado ' . $this->negocio->notificationText . ' del negocio ' . $this->negocio->claveNegocio . ' de ' . $this->negocio->campo1 . ' a ' . $this->negocio->campo2,
            'icon' => 'far fa-clock',
           // 'picture' => $urlImage,
            'picture' => '/img/profiles/avatar.png',
            'route_name' => '/ventas/negocios/',
            'id' => $this->negocio->idNegocio
        ];
    }
}
