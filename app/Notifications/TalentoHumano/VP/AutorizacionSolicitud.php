<?php

namespace App\Notifications\talentoHumano\VP;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AutorizacionSolicitud extends Notification
{
    use Queueable;

    private $solicitud;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($solicitud)
    {
        $this->solicitud = $solicitud;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $solicitante = $this->solicitud->usuario->nombreUsuario;
        $notName = explode(" ", $notifiable->nombreUsuario);
        return (new MailMessage)
            ->from('erp@ammmec.com')
            ->subject('Solicitud de '. strtolower($this->solicitud->tipo))
            ->markdown('mails.talentoHumano.vp.solicitud_autorizacion', ['solicitud' => $this->solicitud, 'user' => $notName, 'solicitante' => $solicitante]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
