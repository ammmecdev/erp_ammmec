<?php

namespace App\Notifications\TalentoHumano\VP;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NuevaSolicitud extends Notification
{
    use Queueable;

    private $solicitud;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($solicitud)
    {
        $this->solicitud = $solicitud;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $solicitante = $this->solicitud->usuario->nombreUsuario;
        $notName = explode(" ", $notifiable->nombreUsuario);

        return (new MailMessage)
            ->from('erp@ammmec.com')
            ->subject('Solicitud de '. strtolower($this->solicitud->tipo))
            ->greeting('Hola ' . $notName[0] . ',')
            ->line('Tienes una nueva solicitud de '. strtolower($this->solicitud->tipo) .' de '. strtolower($solicitante))
            ->action('Ir a solicitudes', url('https://erp.ammmec.com/talentoHumano/vp/solicitudes'))
            ->line('Agradecemos tu colaboración.')
            ->salutation('ERP-AMMMEC');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
