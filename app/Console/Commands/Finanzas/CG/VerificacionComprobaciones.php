<?php

namespace App\Console\Commands\Finanzas\CG;

use Illuminate\Console\Command;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Models\Finanzas\CG\Solicitud;
use App\Notifications\Finanzas\CG\AutorizacionComprobacion;

class VerificacionComprobaciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'verificacion:comprobaciones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new Carbon();
        $today = $date->format('Y-m-d');
        $solicitudes = Solicitud::where('idEtapa', 6)->orWhere('idEtapa', 2)->get();
        $usuarios = Collect();

        foreach ($solicitudes as $solicitud) {
            $usuario = $usuarios->where('idUsuario', $solicitud->idAutorizador)->first();
            if ($usuario)
                $usuario->contador = $usuario->contador + 1;
            else {
                $usuario = User::find($solicitud->idAutorizador);
                $usuario->contador = 1;
                $usuarios->push($usuario);
            }
        }
        foreach($usuarios as $usuario) 
            $usuario->notify(new AutorizacionComprobacion($usuario));
    }
}
