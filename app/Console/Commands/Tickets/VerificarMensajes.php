<?php

namespace App\Console\Commands\Tickets;

use Illuminate\Console\Command;
use App\Models\Tickets\TicketUser;
use App\Models\Tickets\Ticket;

use App\Notifications\Tickets\UnreadMessages;
use App\User;

class VerificarMensajes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tk:verificar_mensajes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verifica los mensajes pendientes en tickets para notificar a los usuarios';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //Revisa los mensajes de los que abrieron tickets
        $usersMessageUnread = Ticket::where('messages_unread', '>', 0)->get()->unique('user_id')->values();
        foreach ($usersMessageUnread as $user) {
            $user = User::find($user->user_id);
            $user->notify(new UnreadMessages());
        }

        //Revisa los mensajes de los administradores
        $usersMessageUnread = TicketUser::where('messages_unread', '>', 0)->get()->unique('user_id')->values();

        dd($usersMessageUnread);
        // Log::info($usersMessageUnread);

        foreach($usersMessageUnread as $user) {
            $user = User::find($user->user_id);
            $user->notify(new UnreadMessages());
        }

      
    }
}
