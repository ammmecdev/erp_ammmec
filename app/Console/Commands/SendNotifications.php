<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Notifications\CreatedActividad;
use App\Actividad;
use App\User;
use Carbon\Carbon;

class SendNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hola:notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Every day will send notifications planned for the next day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = new Carbon();
        $today = $date->format('Y-m-d');
        $actividades = Actividad::all();
        $users = User::all();

        foreach($users as $user){
            $arrayActividades = [];
            foreach($actividades as $actividad){
               $fecha = strcmp($actividad->fechaActividad, $today);
                if($fecha === 0 && $actividad->idUsuario == $user->idUsuario){
                    array_push($arrayActividades, $actividad);
                }
            }
            if($arrayActividades){
                $user->notify(new CreatedActividad($arrayActividades, $user));
            }
                
        }
        Log::info("Se ha enviado satisfactoriamente el correo");
    }
}
