<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $namespace_tickets = 'App\Http\Controllers\Tickets';
    protected $namespace_finanzas = 'App\Http\Controllers\Finanzas';
    protected $namespace_seguridad = 'App\Http\Controllers\Seguridad';
    protected $namespace_servicios = 'App\Http\Controllers\Servicios';
    protected $namespace_sistemas = 'App\Http\Controllers\Sistemas';
    protected $namespace_planes = 'App\Http\Controllers\Planes';
    protected $namespace_compras = 'App\Http\Controllers\Compras';
    protected $namespace_almacen = 'App\Http\Controllers\Almacen';
    protected $namespace_talentoHumano = 'App\Http\Controllers\TalentoHumano';


    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        /* 
       Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
        */
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(function () {
                require base_path('routes/web.php');
                require base_path('routes/web/ventas.php');
                require base_path('routes/web/servicios.php');
                require base_path('routes/web/calidad.php');
                require base_path('routes/web/finanzas.php');
                require base_path('routes/web/sistemas.php');
                require base_path('routes/web/seguridad.php');
                require base_path('routes/web/compras.php');
                require base_path('routes/web/almacen.php');
                require base_path('routes/web/talentoHumano.php');
            });

        Route::middleware('web')
            ->namespace($this->namespace_tickets)
            ->group(function () {
                require base_path('routes/web/tickets.php');
        });
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        /*
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
        */
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(function () {
                require base_path('routes/api.php');
                require base_path('routes/api/ventas.php');
                require base_path('routes/api/calidad.php');
            });

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace_finanzas)
            ->group(base_path('routes/api/finanzas.php'));

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace_seguridad)
            ->group(base_path('routes/api/seguridad.php'));

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace_servicios)
            ->group(base_path('routes/api/servicios.php'));

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace_tickets)
            ->group(base_path('routes/api/tickets.php'));

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace_sistemas)
            ->group(base_path('routes/api/sistemas.php'));

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace_planes)
            ->group(base_path('routes/api/planes.php'));

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace_compras)
            ->group(base_path('routes/api/compras.php'));

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace_almacen)
            ->group(base_path('routes/api/almacen.php'));

        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace_talentoHumano)
            ->group(base_path('routes/api/talentoHumano.php'));
    }
}