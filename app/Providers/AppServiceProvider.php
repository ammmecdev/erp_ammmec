<?php

namespace App\Providers;

use Mail;

//use App\Negocio;
//use App\Observers\NegocioObserver;

use App\Actividad;
use App\ComentarioNotaLead;
use App\NotaLead;

use App\Models\Finanzas\CG\Solicitud;
// use App\Models\Compras\SS\SolicitudSuministro;

use App\Observers\ComentarioObserver;
use App\Observers\ActividadObserver;
use App\Observers\NotaObserver;

use App\Observers\Finanzas\CG\SolicitudObserver;
// use App\Observers\Compras\SS\SolicitudSuministroObserver;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //MAILTRAP
        $throttleRate = config('mail.throttleToMessagesPerMin');
        if ($throttleRate) {
            $throttlerPlugin = new \Swift_Plugins_ThrottlerPlugin($throttleRate, \Swift_Plugins_ThrottlerPlugin::MESSAGES_PER_MINUTE);
            Mail::getSwiftMailer()->registerPlugin($throttlerPlugin);
        }//FIN MAILTRAP
        Schema::defaultStringLength(251);
        //Negocio::observe(NegocioObserver::class);
        ComentarioNotaLead::observe(ComentarioObserver::class);
        Actividad::observe(ActividadObserver::class);
        NotaLead::observe(NotaObserver::class);
        Solicitud::observe(SolicitudObserver::class);
        // SolicitudSuministro::observe(SolicitudSuministroObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
