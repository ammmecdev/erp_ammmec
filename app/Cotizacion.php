<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model
{
    protected $table = 'cotizaciones';
    protected $primaryKey = 'idCotizacion';

    const CREATED_AT = 'fechaCreado';
    const UPDATED_AT = 'fechaActualizado';

    public function negocio()
    {
        return $this->belongsTo(Negocio::class,'idNegocio');
    }
}
