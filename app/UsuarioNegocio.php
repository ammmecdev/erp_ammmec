<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuarioNegocio extends Model
{
    protected $table = 'negocio_usuario';
    protected $primaryKey = 'idSeguidor';
    public $timestamps = false;
}
