<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Oportunidad extends Model
{
    protected $table = 'oportunidad';
    protected $primaryKey = 'idOportunidad';

    protected $appends = ['nombre_etapa'];

    const CREATED_AT = 'fechaCreado';
    const UPDATED_AT = 'fechaActualizado';

    public function getNombreEtapaAttribute()
    {
    	return $this->etapa()->first(['nombreEtapa'])->nombreEtapa;
    }

    public function etapa()
	{
		return $this->belongsTo(Etapa::class, 'idEtapa');
	}
}
