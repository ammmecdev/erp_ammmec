<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instruccion extends Model
{
    protected $table = 'instrucciones';
    protected $primaryKey = 'idInstruccion';
    public $timestamps = false;
}
