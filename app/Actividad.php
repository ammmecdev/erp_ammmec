<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'actividades';
    protected $primaryKey = 'idActividad';
   public $timestamps = false;

   protected $appends = ['titulo_negocio', 'nombre_cliente', 'nombre_usuario', 'clave_negocio'];

   public function getTituloNegocioAttribute(){
     return $this->negocio()->first(['tituloNegocio'])->tituloNegocio;
   }

    public function getClaveNegocioAttribute(){
     return $this->negocio()->first(['claveNegocio'])->claveNegocio;
   }

   public function getNombreClienteAttribute(){
       return $this->cliente()->first(['nombre'])->nombre;
   }

   public function getNombreUsuarioAttribute(){
       return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
   }

    public function usuario(){
        return $this->belongsTo(Usuario::class, 'idUsuario');
    }

    public function negocio(){
        return $this->belongsTo(Negocio::class, 'idNegocio');
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class, 'idCliente');
    }
}
