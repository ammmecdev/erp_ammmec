<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $actividades;
    public $user;
    public $today;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($actividades, $user, $today)
    {
        $this->actividades = $actividades;
        $this->user = $user;
        $this->today = $today;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('jerocor54@gmail.com')
            ->subject('Actividades para hoy')
            ->markdown('mails.mail')
            ->with([
                'name' => 'New Mailtrap User',
                'link' => 'http://localhost:8000/ventas/actividades'
            ]);
        //return $this->view('mails.mail');
    }
}
