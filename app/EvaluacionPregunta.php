<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluacionPregunta extends Model
{
    protected $table = 'evaluaciones_preguntas';
    public $timestamps = false;


    public function evaluacion()
	{
		return $this->belongsTo(EvaluacionServicio::class, 'idEvaluacion');
	}
}
