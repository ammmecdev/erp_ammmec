<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EvaluacionServicio extends Model
{
	public $timestamps = false;

   	protected $table = 'evaluaciones';
    protected $primaryKey = 'idEvaluacion';
 //    protected $appends = ['clave'];

 //   	public function getClaveAttribute()
	// {
	// 	return $this->cliente()->first(['clave'])->clave;
	// }

    public function evaluacion_pregunta()
	{
		return $this->hasMany(EvaluacionPregunta::class, 'idEvaluacion');
	}

	// public function cliente()
	// {
	// 	return $this->belongsTo(Cliente::class, 'idCliente');
	// }
}
