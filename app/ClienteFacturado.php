<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClienteFacturado extends Model
{
    protected $table = 'clientes_facturados';

	const CREATED_AT = 'fechaCreado';
    const UPDATED_AT = 'fechaActualizado';
    
    protected $appends = ['clave'];

    public function getClaveAttribute()
	{
		return $this->cliente()->first(['clave'])->clave;
	}
	
    public function cliente()
	{
		return $this->belongsTo(Cliente::class, 'idCliente');
	}
}
