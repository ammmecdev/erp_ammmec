<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $table = 'sector_industrial';
    protected $primaryKey = 'idSector';
    public $timestamps = false;
}
