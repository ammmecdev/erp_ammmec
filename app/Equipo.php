<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Equipo extends Model
{
    protected $primaryKey = 'idEquipo';
    protected $table = 'equipo';
	public $timestamps = false;
}
