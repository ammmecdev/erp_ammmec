<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pago extends Model
{
    protected $primaryKey = 'idPago';
    const CREATED_AT = 'fechaCreado';
    const UPDATED_AT = 'fechaActualizado';
}
