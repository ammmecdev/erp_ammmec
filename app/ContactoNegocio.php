<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactoNegocio extends Model
{
    protected $table = 'contacto_negocio';
    protected $primaryKey = 'idParticipante';
    public $timestamps = false;
}
