<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    protected $primaryKey = 'idFactura';

    const CREATED_AT = 'fechaCreado';
    const UPDATED_AT = 'fechaActualizado';

    public function negocio()
    {
        return $this->belongsTo(Negocio::class,'idNegocio');
    }
}
