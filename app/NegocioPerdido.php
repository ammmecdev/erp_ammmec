<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NegocioPerdido extends Model
{
   	protected $table = 'negocios_perdidos';
    public $timestamps = false;
}
