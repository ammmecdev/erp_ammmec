<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Models\Ventas\Embudo;
use App\Models\Ventas\Etapa;

class Negocio extends Model
{
    protected $primaryKey = 'idNegocio';

    const CREATED_AT = 'fechaCreado';
    const UPDATED_AT = 'fechaActualizado';

 	protected $appends = ['nombre_cliente', 'nombre_contacto', 'clave_equipo', 'nombre_equipo', 'clave_unidad_negocio', 'nombre_etapa', 'monto_total', 'fecha_solicitud', 'fecha_entrega', 'id_costo', 'id_cotizacion', 'fecha_cotizacion', 'clave_cotizacion', 'nombre_causa', 'id_factura', 'no_factura', 'fecha_factura', 'id_pago', 'fecha_pago', 'pago_mxn_iva', 'pago_usd_iva', 'no_pago', 'fecha_recibo', 'direccion_cliente', 'web_cliente', 'telefono_cliente', 'correo_contacto', 'puesto_contacto', 'telefono_contacto', 'nombre_usuario', 'nombre_embudo'];

 // 	public function getClaveServicioAttribute() 
	// {
	// 	return $this->servicio()->first(['claveServicio'])->claveServicio;
	// }

	// public function getNoServicioAttribute() 
	// {
	// 	return $this->servicio()->first(['noServicio'])->noServicio;
	// }

	// public function getServicioAttribute() 
	// {
	// 	return $this->servicio()->first(['servicio'])->servicio;
	// }

	public function getNombreEmbudoAttribute() 
	{
		return $this->embudo()->first(['nombre'])->nombre;
	}

	public function getNombreUsuarioAttribute() 
	{
		return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
	}

	public function getDireccionClienteAttribute() 
	{
		return $this->cliente()->first(['direccion'])->direccion;
	}

	public function getWebClienteAttribute() 
	{
		return $this->cliente()->first(['paginaWeb'])->paginaWeb;
	}

	public function getTelefonoClienteAttribute() 
	{
		return $this->cliente()->first(['telefono'])->telefono;
	}

	public function getNombreClienteAttribute() 
	{
		return $this->cliente()->first(['nombre'])->nombre;
	}

	public function getNombreContactoAttribute() 
	{
		return $this->contacto()->first(['nombre'])->nombre;
	}

	public function getCorreoContactoAttribute() 
	{
		return $this->contacto()->first(['email'])->email;
	}

	public function getPuestoContactoAttribute() 
	{
		return $this->contacto()->first(['puesto'])->puesto;
	}

	public function getTelefonoContactoAttribute() 
	{
		return $this->contacto()->first(['telefono'])->telefono;
	}

	public function getNombreEquipoAttribute() 
	{
		return $this->equipo()->first(['nombreEquipo'])->nombreEquipo;
	}

	public function getClaveEquipoAttribute() 
	{
		return $this->equipo()->first(['clave'])->clave;
	}

	public function getClaveUnidadNegocioAttribute() 
	{
		return $this->unidad_negocio()->first(['claveUnidadNegocio'])->claveUnidadNegocio;
	}

	public function getNombreEtapaAttribute() 
	{
		return $this->etapa()->first(['nombreEtapa'])->nombreEtapa;
	}

	public function getMontoTotalAttribute() 
	{
		return $this->costo()->first(['montoTotal'])->montoTotal;
	}

	public function getFechaSolicitudAttribute() 
	{
		return $this->costo()->first(['fechaSolicitud'])->fechaSolicitud;
	}

	public function getFechaEntregaAttribute() 
	{ 
		return $this->costo()->first(['fechaEntrega'])->fechaEntrega;
	}

	public function getIdCostoAttribute() 
	{
		return $this->costo()->first(['idCosto'])->idCosto;
	}

	public function getIdCotizacionAttribute() 
	{
		return $this->cotizacion()->first(['idCotizacion'])->idCotizacion;
	}

	public function getFechaCotizacionAttribute() 
	{
		return $this->cotizacion()->first(['fechaCotizacion'])->fechaCotizacion;
	}

	public function getClaveCotizacionAttribute() 
	{
		return $this->cotizacion()->first(['claveCotizacion'])->claveCotizacion;
	}

	public function getNombreCausaAttribute() 
	{
		return $this->causa()->first(['nombreCausa'])->nombreCausa;
	}

	public function getIdFacturaAttribute() 
	{
		return $this->factura()->first(['idFactura'])->idFactura;
	}

	public function getNoFacturaAttribute() 
	{
		return $this->factura()->first(['noFactura'])->noFactura;
	}

	public function getFechaFacturaAttribute() 
	{
		return $this->factura()->first(['fechaFactura'])->fechaFactura;
	}

	public function getIdPagoAttribute()
	{
		return $this->pago()->first(['idPago'])->idPago;
	}

	public function getFechaPagoAttribute()
	{
		return $this->pago()->first(['fechaPago'])->fechaPago;
	}

	public function getPagoMxnIvaAttribute()
	{
		return $this->pago()->first(['pagoMXNiva'])->pagoMXNiva;
	}

	public function getPagoUsdIvaAttribute()
	{
		return $this->pago()->first(['pagoUSDiva'])->pagoUSDiva;
	}

	public function getNoPagoAttribute()
	{
		return $this->pago()->first(['noPago'])->noPago;
	}

	public function getFechaReciboAttribute()
	{
		return $this->pago()->first(['fechaRecibo'])->fechaRecibo;
	}

	// public function servicio()
	// {
	// 	return $this->belongsTo(Servicio::class, 'idServicio');
	// }

	public function cliente()
	{
		return $this->belongsTo(Cliente::class, 'idCliente');
	}

	public function contacto()
	{
		return $this->belongsTo(Contacto::class, 'idContacto');
	}

	public function equipo()
	{
		return $this->belongsTo(Equipo::class, 'idEquipo');
	}

	public function unidad_negocio()
	{
		return $this->belongsTo(UnidadNegocio::class, 'idUnidadNegocio');
	}

	public function etapa()
	{
		return $this->belongsTo(Etapa::class, 'idEtapa');
	}

	public function embudo()
	{
		return $this->belongsTo(Embudo::class, 'idEmbudo');
	}

	public function causa()
	{
		return $this->belongsTo(Causa::class, 'idCausa');
	}


	public function costo()
	{
		return $this->hasOne(Costo::class, 'idNegocio');
	}

	public function pago()
	{
		return $this->hasOne(Pago::class, 'idNegocio');
	}

	public function factura()
	{
		return $this->hasOne(Factura::class, 'idNegocio');
	}

	public function cotizacion()
	{
		return $this->hasOne(Cotizacion::class, 'idNegocio');
	}

	public function actividades()
	{
		return $this->hasMany(Actividad::class, 'idNegocio');
	}

	public function usuario()
    {
        return $this->belongsTo(Usuario::class,'idUsuario');
	}
	
	public function negocioContacto(){
        return $this->belongsToMany(Contacto::class)->withPivot('negocio_idNegocio', 'contacto_idContacto');
	}
	
	public function negocioUsuario(){
        return $this->belongsToMany(Usuario::class)->withPivot('negocio_idNegocio', 'usuario_idUsuario');
    }

    public function negocioPerdido(){
    	return $this->hasOne(NegocioPerdido::class, 'idNegocio');
    }
    
}


//Entonces hemos de usar $developer->skills()->pluck('name', 'id'). De modo que la consulta use internamente un SELECT name, id, y construya el arreglo asociativo a partir de datos planos, que no son representaciones de nuestro modelo.