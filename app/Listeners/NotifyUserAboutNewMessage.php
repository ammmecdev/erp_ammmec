<?php

namespace App\Listeners;

use Illuminate\Support\Facades\DB;
use App\Models\Tickets\User;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketUser;
use App\Events\Tickets\TicketsNewMessage;
use App\Events\Tickets\NewMessage;
use App\Notifications\Tickets\TicketsMessageSend;
use App\Notifications\Tickets\MessageSend;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;

class NotifyUserAboutNewMessage
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendMessage  $event
     * @return void
     */
    public function handle(NewMessage $event)
    {
        $message = $event->message;
        $ticket = $event->ticket;
        $users = $ticket->users;

        $user_autor = User::find($message->user_id);

        if(auth()->user()->idUsuario == $ticket->user_id){
            if($users->isNotEmpty()){
                foreach($users as $user){
                    $user_not = TicketUser::where('user_id', $user->idUsuario)->where('ticket_id', $ticket->id)->first();
                    $user_not->messages_unread = $user_not->messages_unread + 1;
                    $success = $user_not->save();
                }  
            }
        }else{
            $ticket->messages_unread = $ticket->messages_unread + 1;
            $ticket->save();
            if($users->isNotEmpty()){
                foreach($users as $index => $user)
                {
                    if($user->idUsuario == $message->user_id)
                        $users->pull($index);
                }
            }
            $users->push($ticket->user);
        }

        foreach($users as $user) {
            if($user->telefono)
                $this->sendWhatsaap($user->idUsuario, $ticket->id, $user_autor->nombreUsuario);
        }
        
        Notification::send($users, new MessageSend($event->message));   
    }

    public function sendWhatsaap($idUsuario, $idTicket, $autor)
    {
        $autor = explode(" " , $autor);
        if(isset($autor[1]))
            $autor = $autor[0] . " " . $autor[1];
        else
            $autor = $autor[0];
        $user = User::find($idUsuario);
        $message = "ℹ️ *Notificación ERP - Tickets:* \n$autor ha respondido en el ticket $idTicket";
        $data = [
            'phone' => '52' . $user->telefono, // Receivers phone
            'body' => $message, // Message
        ];
        $json = json_encode($data); // Encode data to JSON
        // URL for request POST /message
        $token = '1148aesewyiq9fi1';
        $instanceId = '211454';
        $url = 'https://api.chat-api.com/instance' . $instanceId . '/message?token=' . $token;
        // Make a POST request
        $options = stream_context_create(['http' => [
            'method'  => 'POST',
            'header'  => 'Content-type: application/json',
            'content' => $json
        ]]);
        // Send a request
        // $result = file_get_contents($url, false, $options);
    }

}
