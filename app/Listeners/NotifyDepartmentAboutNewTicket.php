<?php

namespace App\Listeners;

use App\Models\Tickets\User;
use App\Models\Tickets\Message;
use App\Events\Tickets\TicketCreated;
use App\Notifications\Tickets\TicketSend;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Notification;


class NotifyDepartmentAboutNewTicket
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TicketCreated  $event
     * @return void
     */
    public function handle(TicketCreated $event)
    {
        $users = $event->ticket->users;
        $message = Message::where('ticket_id', $event->ticket->id)->first();
        Notification::send($users, new TicketSend($event->ticket, $message->message));
    }
}
