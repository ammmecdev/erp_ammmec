<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cadmin\Role;
use App\Models\TalentoHumano\Empleado;

class Usuario extends Model
{
    protected $table = 'usuarios';
    protected $primaryKey='idUsuario';
    public $timestamps = false;

    public function negocio()
    {
        return $this->belongsToMany(Negocio::class)->as('usuario_negocio');
    }

    public function roles() 
    {
        return $this->belongsToMany(Role::class, 'ad_usuarios_roles', 'idUsuario', 'idRole')->withPivot('idSubmodulo');
    }

    public function empleado()
    {
        return $this->belongsTo(Empleado::class, 'noEmpleado', 'noEmpleado');
    }
}   
