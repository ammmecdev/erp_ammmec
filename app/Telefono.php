<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefono extends Model
{
    //
    protected $primaryKey = 'idTelefono';
    public $timestamps = false;


    public function contacto(){
        return $this->belongsTo(Contacto::class, 'idContacto');
    }
}
