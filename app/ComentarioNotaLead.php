<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComentarioNotaLead extends Model
{
    protected $table='comentarios_notas';
    protected $primaryKey='idComentario';
    protected $appends=['nombre_usuario','foto_usuario'];
    const CREATED_AT='fechaCreado';
    const UPDATED_AT='fechaActualizado';
    

    public function nota()
    {
        return $this->belongsTo(NotaLead::class,'idNota');
    }

    public function usuario(){
        return $this->belongsTo(Usuario::class,'idUsuario');
    }

    public function actualizaciones(){
        return $this->hasMany(ActualizacionesComentarios::class, 'idComentario');
    }

    public function getNombreUsuarioAttribute(){
        return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
    }
    public function getFotoUsuarioAttribute(){
        return $this->usuario()->first(['foto'])->foto;
    }
    // public function getNombreClienteAttribute(){
    //     return $this->nota()->first(['nombre_cliente'])->nombre_cliente;
    // }
    // public function getNombreContactoAttribute(){
    //     return $this->nota()->first(['nombre_contacto'])->nombre_contacto;
    // }

    

}
