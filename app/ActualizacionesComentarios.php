<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActualizacionesComentarios extends Model
{
    //
   protected $primaryKey = 'idActualizacion';
   protected $table = 'actualizaciones_comentarios';
   protected $appends=['nombre_usuario','foto_usuario'];
   public $timestamps = false;

    public function usuario(){
        return $this->belongsTo(Usuario::class,'idUsuario');
    }
    public function comentario(){
        return $this->belongsTo(ComentarioNotaLead::class,'idComentario');
    }

    public function getNombreUsuarioAttribute(){
        return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
    }
    
    public function getFotoUsuarioAttribute(){
        return $this->usuario()->first(['foto'])->foto;
    }
}
