<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RegistroCambios extends Model
{
    protected $table = 'registro_cambios';
    protected $primaryKey = 'idRegistro';
    public $timestamps = false;

    protected $appends = ['nombre_usuario'];

    public function getNombreUsuarioAttribute() 
	{
		return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
	}
	public function usuario(){
        return $this->belongsTo(Usuario::class, 'idUsuario');
    }
    
}
