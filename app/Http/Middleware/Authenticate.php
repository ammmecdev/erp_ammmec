<?php

namespace App\Http\Middleware;

use Closure;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         //check here if the user is authenticated
    if ( ! $this->auth->user() )
    {
        return redirect('/home');
    }
        return $next($request);
    }
}
