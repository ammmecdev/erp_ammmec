<?php

namespace App\Http\Controllers\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ventas\Etapa;

class EtapaController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function store(Request $request)
    {
        $etapa = new Etapa;
        $etapa->nombreEtapa = $request->nombreEtapa;
        $etapa->probabilidad = $request->probabilidad;
        $etapa->inactividad = $request->inactividad;
        $etapa->idEmbudo = $request->idEmbudo;

        $saved = $etapa->save();

        $data = [];
        $data['success'] = $saved;
        $data['etapa'] = $etapa;
        return $data;
    }

    public function update(Etapa $etapa, Request $request)
    {
        $etapa->nombreEtapa = $request->nombreEtapa;
        $etapa->probabilidad = $request->probabilidad;
        $etapa->inactividad = $request->inactividad;
        $etapa->idEmbudo = $request->idEmbudo;

        $saved = $etapa->save();

        $data = [];
        $data['success'] = $saved;
        $data['etapa'] = $etapa;
        return $data;
    }

    public function updateOrden(Request $request)
    {
        $etapas = $request->etapas;
        $index = 1;

        foreach($etapas as $etapa) {
            $etapa = Etapa::find($etapa['idEtapa']);
            $etapa->orden = $index;
            $etapa->save();
            $index++;
        }

        // return Etapa::where('idEmbudo', $request->idEmbudo)->orderBy('orden', 'asc')->get();
        return 'true';
    }
    

    public function delete(Etapa $etapa)
    {
        $deleted = $etapa->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }


    // public function getEtapas(Request $request)
    // {
    // 	$idEmbudo = $request->idEmbudo;
    // 	return Etapa::where('idEmbudo', $idEmbudo)->orderBy('orden','ASC')->get();
    // }

 //    public function selectEtapas(Request $request)
	// {
 //    	$etapa = Etapa::where('idEmbudo', $request->idEmbudo)->get();
 //    	$data = [];
 //    	foreach($etapa as $key => $value){
 //    		$data[$key] = [
 //                'text'  => $value->nombreEtapa,
 //    			'value' =>  [
 //                                'nombreEtapa' => $value->nombreEtapa,
	// 							    'idEtapa' => $value->idEtapa,
 //                            ]
 //    		];
 //    	}
 //    	return response()->json($data);
	// }
}
