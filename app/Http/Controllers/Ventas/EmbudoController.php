<?php

namespace App\Http\Controllers\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ventas\Embudo;
use App\Negocio;
use Illuminate\Support\Facades\DB;

class EmbudoController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    public function selectEmbudos()
    {
		
    	$embudos = Embudo::get();
    	$data = [];
  
    	foreach($embudos as $key => $value){
    		$data[$key] = [
    			'value' => $value->idEmbudo,
    			'text'  => $value->nombre
    		];
    	}
    	return response()->json($data);
    }

    public function store(Request $request) 
    {
        $embudo = new Embudo; 
        $embudo->nombre = $request->nombre;
        $embudo->periodo = $request->periodo;   
        $saved = $embudo->save();

        $data = [];
        $data['success'] = $saved;
        $data['embudo'] = $embudo;
        return $data;
    }

    public function update(Embudo $embudo, Request $request) 
    {
        $embudo->nombre = $request->nombre;
        $embudo->periodo = $request->periodo;
        $saved = $embudo->save();

        $data = [];
        $data['success'] = $saved;
        $data['embudo'] = $embudo;
        return $data;
    }

    public function delete(Embudo $embudo) {
        $deleted = $embudo->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getEtapasByEmbudo(Embudo $embudo)
    {
        return $etapas = $embudo->etapas;
        // foreach ($etapas as $etapa) {
        //     $negocios = Negocio::select('valorNegocio', 'xCambio', 'tipoMoneda')
        //                         ->where('idEtapa',$etapa->idEtapa)->get();
        //     $valorTotalNegocios = 0;
        //     $negociosCount = 0;
        //     foreach ($negocios as $negocio) {
        //         if($negocio->tipoMoneda=='USD')
        //             $negocio->valorNegocio = $negocio->valorNegocio * $negocio->xCambio;
        //         $valorTotalNegocios = $negocio->valorNegocio + $valorTotalNegocios;
        //         $negociosCount ++; 
        //     }
        //     $porcentajeProbabilidad = ($etapa->probabilidad / 100);
        //     $etapa->valorTotal = $valorTotalNegocios * $porcentajeProbabilidad;
        //     $etapa->negociosCount = $negociosCount;
        // }
        // return $etapas;
    }

    public function getEmbudos()
    {
       return Embudo::all();
    }
}
