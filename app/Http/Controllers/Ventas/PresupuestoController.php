<?php

namespace App\Http\Controllers\Ventas;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ventas\Presupuesto;
use App\Models\Ventas\PresupuestoMeses;
use Illuminate\Support\Facades\DB;
use App\UnidadNegocio;

class PresupuestoController extends Controller
{
    public function __construct() {
        $this->middleware('guest', ['only' => 'showLoginForm']); 
    }
    
    public function store(Request $request) {
        foreach($request->negocios as $negocio) {
            $presupuesto = new Presupuesto();
            $presupuesto->idEmbudo = $request->id_embudo;
            $presupuesto->idNegocio = $negocio['idNegocio'];
            // $presupuesto->totalAnual = 0;
            $saved = $presupuesto->save();
        }
        $data = [];
        $data['success'] = $saved;    
        return $data;
    }

    public function store_presupuestoMes(Request $request) {
        $data = [];
        $pre_mes = new PresupuestoMeses();       
        $pre_mes->idPresupuesto = $request->idPresupuesto;
        $pre_mes->idMes = $request->idMes;
        $pre_mes->idNegocioMensual = $request->negocio;
        $pre_mes->totalMensual = $request->totalMensual;
        $saved = $pre_mes->save();
        $presupusto_mes = PresupuestoMeses::find($pre_mes->idPreMes);
        $data['success'] = $saved;
        $data['presupuestos_meses'] = $presupusto_mes;
        return $data;
    }

    public function update_presupuestoMes(Request $request) {
       $data = [];
       $oldTotalMensual = 0;
       $pre_mes = PresupuestoMeses::find($request->idPreMes);
       $oldTotalMensual = $pre_mes->totalMensual;
       $pre_mes->idNegocioMensual = $request->negocio;
       $pre_mes->totalMensual = $request->totalMensual;
       $saved = $pre_mes->save();
       $presupusto_mes = PresupuestoMeses::find($pre_mes->idPreMes);
       $data['success'] = $saved;
       $data['presupuestos_meses'] = $presupusto_mes;
       $data['old_totalMensual'] = $oldTotalMensual;      
       return $data;
    }

    public function getPresupuestos(Request $request) {
    	return Presupuesto::where('idEmbudo', $request->idEmbudo)->get();
    }

    public function getPresupuestosJoin(Request $request) {
       $presupuestos = DB::table('vt_presupuestos')
            ->join('negocios', 'vt_presupuestos.idNegocio', '=', 'negocios.idNegocio')
            ->join('equipo', 'negocios.idEquipo', '=', 'equipo.idEquipo')
            ->join('unidad_negocio', 'negocios.idUnidadNegocio', '=', 'unidad_negocio.idUnidadNegocio')
            ->join('facturas', 'negocios.idNegocio', '=', 'facturas.idNegocio')
            ->select(
                'vt_presupuestos.*', 
                'negocios.claveNegocio as clave_negocio',
                'negocios.idEmbudo as id_embudo',
                'negocios.claveCliente as clave_cliente',
                'negocios.tituloNegocio as titulo_negocio',
                'negocios.valorNegocio as valor_negocio',
                'negocios.tipoMoneda as tipo_moneda',
                'negocios.xCambio as x_cambio',
                'unidad_negocio.claveUnidadNegocio as clave_unidad_negocio',
                'equipo.clave as clave_equipo',
                'facturas.fechaFactura as fecha_factura'
            )
            ->where('vt_presupuestos.idEmbudo', $request->idEmbudo)
            ->orderBy('vt_presupuestos.idPresupuesto', 'asc')->get();
        return $presupuestos;
    }

    public function presupuestoById($id) {
        $presupuesto = DB::table('vt_presupuestos')
            ->join('negocios', 'vt_presupuestos.idNegocio', '=', 'negocios.idNegocio')
            ->join('equipo', 'negocios.idEquipo', '=', 'equipo.idEquipo')
            ->join('unidad_negocio', 'negocios.idUnidadNegocio', '=', 'unidad_negocio.idUnidadNegocio')
            ->join('facturas', 'negocios.idNegocio', '=', 'facturas.idNegocio')
            ->select(
                'vt_presupuestos.*', 
                'negocios.claveNegocio as clave_negocio',
                'negocios.idEmbudo as id_embudo',
                'negocios.claveCliente as clave_cliente',
                'negocios.tituloNegocio as titulo_negocio',
                'negocios.valorNegocio as valor_negocio',
                'negocios.tipoMoneda as tipo_moneda',
                'negocios.xCambio as x_cambio',
                'unidad_negocio.claveUnidadNegocio as clave_unidad_negocio',
                'equipo.clave as clave_equipo',
                'facturas.fechaFactura as fecha_factura'
            )
            ->where('vt_presupuestos.idPresupuesto', $id)->get();
        return $presupuesto;
    }

    public function getPresupuestosMesesByEmbudo($idEmbudo) {
        $presupuestosMeses = PresupuestoMeses::orderBy('idMes', 'ASC')->get();

        $collect=collect();
        foreach($presupuestosMeses as $presupuestoMes){
            if($presupuestoMes->id_embudo_pre == $idEmbudo){
                $unidadNegocio = UnidadNegocio::where('idUnidadNegocio', $presupuestoMes->id_unidad_negocio)->first();
                $presupuestoMes->unidadNegocio = $unidadNegocio->unidadNegocio;
                $collect->push($presupuestoMes);
            }
        }
        return $collect;
    }

    public function deletePresupuestoMes($id) {
        $pre_mes = PresupuestoMeses::find($id);
        $deleted = $pre_mes->delete();
        $data = [
            'success' => $deleted,
            'presupuesto_mes' => $pre_mes
        ];
        return $data;
    }

    public function delete($id)
    {
        $presupuesto = Presupuesto::find($id);
        $deleted = $presupuesto->delete();
        $data = [
            'success' => $deleted,
            'presupuesto' => $presupuesto
        ];
        return $data;
    }
}
