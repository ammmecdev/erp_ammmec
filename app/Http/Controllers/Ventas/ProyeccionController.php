<?php

namespace App\Http\Controllers\Ventas;

use Illuminate\Http\Request;
use App\Models\Ventas\Proyeccion;
use App\Http\Controllers\Controller;

class ProyeccionController extends Controller
{
    public function getProyecciones()
    {
        return Proyeccion::get();
        
    }

    public function save(Request $request)
    {
        $proyeccion = Proyeccion::where('idEmbudo', $request->idEmbudo)->where('idUnidadNegocio', $request->idUnidadNegocio)->first();
        if ($proyeccion)
            $proyeccion->monto = $request->monto;
        else {
            $proyeccion = new Proyeccion;
            $proyeccion->idEmbudo = $request->idEmbudo;
            $proyeccion->idUnidadNegocio = $request->idUnidadNegocio;
            $proyeccion->monto = $request->monto;
        }
        $saved = $proyeccion->save();
        $data = [];
        $data['success'] = $saved;
        $data['proyeccion'] = $proyeccion;

        return $data;
    }
}