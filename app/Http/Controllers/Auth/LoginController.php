<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\AuthemticatesUsers;

class LoginController extends Controller
{

    // use AuthenticatesUsers;

    // public function authenticated(Request $request, $user){
    //     dd($user);
    //     die();
    // }
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    function login()
    {
        $credentials = $this->validate(request(),[
            'noEmpleado' => 'required|string', 
            'password' => 'required|string',
        ],[
            'noEmpleado.required' => 'Debes ingresar un usuario',
            'password.required' => 'Debes ingresar una contraseña',
        ]);
        if (Auth::attempt($credentials))
        {
            return redirect()->intended('modulos');
        }
        return back()->withErrors(['noEmpleado' => trans('auth.failed')])
        ->withInput(request(['noEmpleado']));
    }

    // public function username()
    // {
    //     return 'noEmpleado';
    // }


    function showLoginForm()
    {
        return view('auth.login');
    }

    function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
