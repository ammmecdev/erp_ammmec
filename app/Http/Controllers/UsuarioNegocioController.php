<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UsuarioNegocio;
use App\Negocio;

class UsuarioNegocioController extends Controller
{
    public function store(Request $request){
        $seguidor = new UsuarioNegocio();

        $seguidor->negocio_idNegocio = $request->idNegocio;
        $seguidor->usuario_idUsuario = $request->idUsuario;

        $saved  = $seguidor->save();

        return $data = [
            'success' => $saved,
            'seguidor' => $seguidor
        ];
    }

    public function getSeguidores(Negocio $negocio){
        return $negocio->negocioUsuario;
    }
}
