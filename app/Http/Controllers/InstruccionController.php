<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InstruccionEmpleado;

class InstruccionController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getInstruccionByEmpleado($idInstruccion, $noEmpleado){
        return InstruccionEmpleado::where('noEmpleado', $noEmpleado)->where('idInstruccion', $idInstruccion)->first();
    }

    public function setInstruccionLeida($idInstruccion, $noEmpleado){
        $instruccion = new InstruccionEmpleado;
        $instruccion->idInstruccion = $idInstruccion;
        $instruccion->noEmpleado = $noEmpleado;
        
        $instruccion->status = 'leida';
        $save = $instruccion->save();

        $data = array();
        $data['success'] = $save;
        $data['instruccion'] = $instruccion;

        return $data;
    }
}
