<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacto;
use App\Telefono;



class ContactoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    public function getContactos()
    {
        return Contacto::all();
        
    }

    public function getContactoById(Contacto $contacto)
    {
        return $contacto;
    }

    public function store(Request $request){
        $contacto = new Contacto;
        $telefono = new Telefono;
        $contacto->idCliente = $request->idCliente;
        $contacto->idUsuario = $request->idUsuario;
        $contacto->nombre = $request->nombre;
        $contacto->email = $request->email;
        $contacto->honorifico = $request->honorifico;
        $contacto->puesto = $request->puesto;
        $contacto->lead = null;
        $telefonos=$request->telefonos;
       
        foreach ($telefonos as $key => $value) {
            
            if($key!=0){
                $telefono->telefono = $value['telefono'];
                $telefono->extension = $value['extension'];
                $telefono->tipoTelefono = $value['tipoTelefono'];
                $telefono->idContacto = $contacto->idContacto;
            } 
                $contacto->telefono = $value['telefono'];
                $contacto->extension = $value['extension'];
                $contacto->tipoTelefono = $value['tipoTelefono'];
                $saved = $contacto->save();
        }
        if(count($telefonos)>1)
        {
            $savedTelefono = $telefono->save();
        }
        
        $data = [];
        $data['success'] = $saved;
        $data['contacto'] = $contacto;

        return $data;
      
    }

    public function deleteContactos($id){
         $contacto = Contacto::find($id);

         $delete = $contacto->delete();

         $data = [
             'success' => $delete,
             'message' => 'Se elimino correctamente'
         ];

         return $data;  
    }

    public function updateContactos(Request $request){
        $contacto = Contacto::find($request->idContacto);

        $contacto->idCliente = $request->idCliente;
        $contacto->idUsuario = $request->idUsuario;
        $contacto->nombre = $request->nombre;
        //$contacto->telefono = $request->telefonos;
        $contacto->extension = $request->extension;
        $contacto->tipoTelefono = $request->tipoTelefono;
        $contacto->email = $request->email;
        $contacto->honorifico = $request->honorifico;
        $contacto->puesto = $request->puesto;
        $contacto->lead = null;

        $updated = $contacto->update();

        $data = [];
        $data['success'] = $updated;
        $data['contacto'] = $contacto;

        return $data;
    }

    public function selectContactos()
	{
    	$contacto = Contacto::get();
    	$data = [];
 
    	foreach($contacto as $key => $value){
    		$data[$key] = [
                'text'  => $value->nombre,
    			'value' => [
                                'text'  => $value->nombre,
                                'value' => $value->idContacto
                            ]
    		];
    	}
    	return response()->json($data);
	}

    public function selectClientesByIdContacto(Contacto $contacto)
    {
        $cliente = $contacto->cliente;
        
        $data[] = [
            'text'  => $cliente->nombre,
            'value' => [
                            'text'  => $cliente->clave,
                            'value' => $cliente->idCliente
                        ]
        ];
        return response()->json($data);
    }

} 
