<?php

namespace App\Http\Controllers\Servicios\RA;

use App;
use DB;
use App\Models\TalentoHumano\Empleado;
use Illuminate\Http\Request;
use App\Models\Servicios\RA\Reporte;
use App\Models\Servicios\RA\Actividad;
use App\Models\Servicios\RA\Semana;
use App\Exports\ActividadesDeServiciosExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;


class ReporteController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getReportesByEmpleado($noEmpleado)
    {
        return Reporte::where('noEmpleado', $noEmpleado)->get();
    }

    //Revisa cuales son las semanas disponibles del empelado para generar reporte
    public function getSemanasDisponibles($noEmpleado, $year)
    {
        $actividades = Actividad::where('noEmpleado', $noEmpleado)->get();
        $semanas = collect();
        foreach ($actividades as $actividad) {
            $semanaRegistrada = Semana::where("idSemana", $actividad->idSemana)
                ->where("year", $year)
                ->first();

           if ($semanaRegistrada)
               $semanas = $semanas->concat([$semanaRegistrada]);
        }
        $semanasRegistradas = $semanas->unique();

        $semanasDisponibles = collect();
        foreach ($semanasRegistradas as $semana) {
            $reporte = Reporte::where('idSemana', $semana->idSemana)->where('noEmpleado', $noEmpleado)->first();
            if ($reporte == null) {
                $semanasDisponibles = $semanasDisponibles->concat([Semana::find($semana->idSemana)]);
            }
        }

        return $semanasDisponibles;
    }

    public function getYearsDisponiblesToExport()
    {
        $years = DB::table('op_ra_actividades')
            ->join('op_ra_semanas', 'op_ra_actividades.idSemana', '=', 'op_ra_semanas.idSemana')
            ->select('op_ra_semanas.year')
            ->get();

        $yearsU = $years->unique();

        $years = collect();

        foreach ($yearsU as $year)
            $years->push($year->year);

        return $years;
    }

    //Revisa cuales las semanas disponibles para exportar a excel

    public function getSemanasDisponiblesToExport($year)
    {
        $semanas = DB::table('op_ra_actividades')
            ->join('op_ra_semanas', 'op_ra_actividades.idSemana', '=', 'op_ra_semanas.idSemana')
            ->select('op_ra_semanas.idSemana', 'op_ra_semanas.semana')
            ->where('op_ra_semanas.year', $year)
            ->get();

        $semanasU = $semanas->unique('idSemana');

        $semanas = collect();

        foreach ($semanasU as $semana) {
            $semanas->push($semana);
        }

        return $semanas;
    }

    //Genera el reporte del empleado en PDF

    public function generateReport(Request $request)
    {
        $actividades = Actividad::where('noEmpleado', $request->noEmpleado)->where('idSemana', $request->idSemana)->orderBy('fecha', 'ASC')->get();

        foreach ($actividades as $actividad)
            $actividad->fecha = date("d/m/Y", strtotime($actividad->fecha));

        $semana = Semana::find($request->idSemana);

        $actividades->noSemana = $semana->semana;
        $actividades->nombreEmpleado = $request->nombreEmpleado;
        $actividades->noEmpleado = $request->noEmpleado;

        $reporte = new Reporte;

        $reporte->noEmpleado = $request->noEmpleado;
        $reporte->idSemana = $request->idSemana;
        $reporte->reporte = 'RA-S' . $semana->semana . '-E' . $request->noEmpleado . '.pdf';
        $reporte->ruta = 'OP/RA/' . $request->noEmpleado . '/' . $reporte->reporte;

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('modulos.servicios.reportes.actividades', ['actividades' => $actividades]);
        // $pdf->setPaper('a4', 'landscape');

        Storage::put($reporte->ruta, $pdf->output());

        $saved = $reporte->save();

        $data = [];
        $data['success'] = $saved;
        $data['reporte'] = $reporte;

        return $data;
    }

    //Descarga el reporte del empleado

    public function downloadReport($idReporte)
    {
        $file = Reporte::find($idReporte);
        return Storage::download($file->ruta);
    }

    //Muestra el reporte en PDF sin descargarlo

    public function showReport($idReporte)
    {
        $file = Reporte::find($idReporte);
        return Storage::response($file->ruta);
    }

    //Reporte para revisar las cuentas registradas de una semana especifica

    public function generateReportCuentasRegistradas($idSemana)
    {
        $actividades = Actividad::select('tipoCuenta', 'cuenta')->where('idSemana', $idSemana)->get();
        $cuentas = collect();
        foreach ($actividades as $actividad) {
            $cuenta = array();
            $cuenta['tipoCuenta'] = $actividad->tipoCuenta;
            $cuenta['cuenta'] = $actividad->cuenta;
            $cuentas->push($cuenta);
        }
        $cuentasR = collect();
        foreach ($cuentas->unique() as $cuenta) {
            $cuentasR->push($cuenta);
        }
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('modulos.servicios.reportes.cuentas_semanales', ['cuentas' => $cuentasR]);
        return $pdf->stream();
    }

    public function exportExcelPeriodo()
    {
        $fechaInicio = $_GET['fechaInicio'];
        $fechaFin = $_GET['fechaFin'];
        $lineaNegocio = $_GET['lineaNegocio'];
        $nombre = "RA.xlsx";

        if ($fechaInicio != "null" && $fechaFin != "null") {
            // OBTENER ACT DENTRO DE LAS FECHAS ESTABLECIDAS
            $actividades = Actividad::whereBetween('fecha', [$fechaInicio, $fechaFin])
                ->orderBy('fecha', 'ASC')
                ->get();

            // DEFINIR ESTRUCTURA DE DATOS
            $result = $this->obtenerDatosActividades($actividades, $lineaNegocio);
            return (new ActividadesDeServiciosExport($result, "OP"))->download($nombre);
        }
    }

    public function obtenerDatosActividades($actividades, $lineaNegocio)
    {
        // OBTENER ACT POR LINEA DE NEGOCIO
        // OBTENER EMPLEADOS DE ACTIVIDADES
        // OBTENER SEMANAS DE ACTIVIDADES
        $actividadesCollect = collect();
        $empleados = collect();
        $semanas = collect();

        // NOTA: SI SE ACTUALIZA ESTA LISTA SE TIENE QUE ACTUALIZAR EN EL JS DE ACTIVIDADES
        $empleadosLineaNegocio = [
            "TM" => [
                "279-170322", // Hector mendez
                "295-270223", // Anahí
                "289-241122", // karen L. Vázquez
                "073-231211", // Wilibaldo (Supervisor)
            ],
            "CM" => [
                "204-120821", // Angel Manuel
                "015-030211", // Martin
                "290-241122", // Eduardo Daniel
                "288-200922", // Erik
                "284-040622", // Michelle
                "287-250822", // Francisco
                "203-120821", // Victor Hugo
                "047-270611", // Cande
                "212-271021", // Juan José
                "296-090323", // Alejandro Moreno
                "004-260422", // Jose Guadalupe
                "168-280319", // Ricardo
                "140-030817", // Margarita
                "297-090323", // Jesus alonso
                "293-230223", // Citlalli
                "294-230223", // Ana getsemani
                "300-290623", // Luis Antonio Alcalá
                "301-050723", // José Ricardo González
                "302-050723", // Juan González
                "303-280823", // Jose Ascencion Martinez
                "305-091023", // Rosa Yaneth Santillán Félix
                "268-050222", // Jesus Amador (Supervisor)
            ],
            "RD" => [
                "193-240221", // Juana
                "173-050919", // Cesar ricardo (Supervisor)
            ]
        ];

        if ($lineaNegocio == "Todas") {
            foreach ($actividades as $actividad) {
                if ($actividad->tipoCuenta == "Externa") {  
                    $arrCuenta = explode('-',$actividad->cuenta);
                    if (
                        $arrCuenta[2] == "TM" || 
                        $arrCuenta[2] == "CM" || 
                        $arrCuenta[2] == "RD"
                    ) {
                        $actividadesCollect->push($actividad);
                        $semanas->push(
                            Semana::where("idSemana", $actividad->idSemana)
                                ->first(["idSemana", "semana"])
                        );
                        $empleados = $empleados->concat(
                            [Empleado::where('noEmpleado', $actividad->noEmpleado)
                                ->first(["noEmpleado", "nombre"])
                            ]
                        );
                    }
                }else if ($actividad->tipoCuenta == "Interna") {
                    $arrCuenta = explode('-',$actividad->cuenta);
                    if ($actividad->fecha < "2023-08-15") {
                        if (
                            in_array($actividad->noEmpleado, $empleadosLineaNegocio["TM"]) || 
                            in_array($actividad->noEmpleado, $empleadosLineaNegocio["CM"]) ||
                            in_array($actividad->noEmpleado, $empleadosLineaNegocio["RD"]) 
                        ) {
                            $actividadesCollect->push($actividad);
                            $semanas->push(
                                Semana::where("idSemana", $actividad->idSemana)
                                    ->first(["idSemana", "semana"])
                            );
                            $empleados = $empleados->concat(
                                [Empleado::where('noEmpleado', $actividad->noEmpleado)
                                    ->first(["noEmpleado", "nombre"])
                                ]
                            );
                        }  
                    }else {
                        if (
                            $arrCuenta[0] == "TM" || 
                            $arrCuenta[0] == "CM" || 
                            $arrCuenta[0] == "RD"
                        ) {
                            $actividadesCollect->push($actividad);
                            $semanas->push(
                                Semana::where("idSemana", $actividad->idSemana)
                                    ->first(["idSemana", "semana"])
                            );
                            $empleados = $empleados->concat(
                                [Empleado::where('noEmpleado', $actividad->noEmpleado)
                                    ->first(["noEmpleado", "nombre"])
                                ]
                            );
                        }
                    }
                }else if ($actividad->tipoCuenta == "Capacitacion") {
                    if (
                        in_array($actividad->noEmpleado, $empleadosLineaNegocio["TM"]) ||
                        in_array($actividad->noEmpleado, $empleadosLineaNegocio["CM"]) ||
                        in_array($actividad->noEmpleado, $empleadosLineaNegocio["RD"]) 
                    ) {
                        $actividadesCollect->push($actividad);
                        $semanas->push(
                            Semana::where("idSemana", $actividad->idSemana)
                                ->first(["idSemana", "semana"])
                        );
                        $empleados = $empleados->concat(
                            [Empleado::where('noEmpleado', $actividad->noEmpleado)
                                ->first(["noEmpleado", "nombre"])
                            ]
                        );
                    }
                }
            }
        }else {
            foreach ($actividades as $actividad) {
                if ($actividad->tipoCuenta == "Externa") {  
                    $arrCuenta = explode('-',$actividad->cuenta);
                    if ($arrCuenta[2] == $lineaNegocio) {
                        $actividadesCollect->push($actividad);
                        $semanas->push(
                            Semana::where("idSemana", $actividad->idSemana)
                                ->first(["idSemana", "semana"])
                        );
                        $empleados = $empleados->concat(
                            [Empleado::where('noEmpleado', $actividad->noEmpleado)
                                ->first(["noEmpleado", "nombre"])
                            ]
                        );
                    }
                }else if ($actividad->tipoCuenta == "Interna") {
                    $arrCuenta = explode('-',$actividad->cuenta);
                    if ($actividad->fecha < "2023-08-15") {
                        
                        if (in_array($actividad->noEmpleado, $empleadosLineaNegocio[$lineaNegocio])) {

                            $actividadesCollect->push($actividad);
                            $semanas->push(
                                Semana::where("idSemana", $actividad->idSemana)
                                    ->first(["idSemana", "semana"])
                            );
                            $empleados = $empleados->concat(
                                [Empleado::where('noEmpleado', $actividad->noEmpleado)
                                    ->first(["noEmpleado", "nombre"])
                                ]
                            );
                        }  
                    }else {
                        if ($arrCuenta[0] == $lineaNegocio) {
                            $actividadesCollect->push($actividad);
                            $semanas->push(
                                Semana::where("idSemana", $actividad->idSemana)
                                    ->first(["idSemana", "semana"])
                            );
                            $empleados = $empleados->concat(
                                [Empleado::where('noEmpleado', $actividad->noEmpleado)
                                    ->first(["noEmpleado", "nombre"])
                                ]
                            );
                        }
                    }
                }else if ($actividad->tipoCuenta == "Capacitacion") {
                    if (in_array($actividad->noEmpleado, $empleadosLineaNegocio[$lineaNegocio])) {
                        $actividadesCollect->push($actividad);
                        $semanas->push(
                            Semana::where("idSemana", $actividad->idSemana)
                                ->first(["idSemana", "semana"])
                        );
                        $empleados = $empleados->concat(
                            [Empleado::where('noEmpleado', $actividad->noEmpleado)
                                ->first(["noEmpleado", "nombre"])
                            ]
                        );
                    }
                }
            }
        }
        $empleados = $empleados->unique();
        $semanas = $semanas->unique();
        $result = collect();
       
        foreach ($semanas as $semana) {
            $actividadesSemana = $actividadesCollect->where('idSemana', $semana->idSemana);
            foreach ($empleados as $empleado) {
                $actividadesEmpleado = $actividadesSemana->where('noEmpleado', $empleado->noEmpleado);

                //Separa las actividades del empleado segun el día
                $actDiaSemana = collect();
                $actDiaSemana->push($actividadesEmpleado->where('dia', 'Jueves'));
                $actDiaSemana->push($actividadesEmpleado->where('dia', 'Viernes'));
                $actDiaSemana->push($actividadesEmpleado->where('dia', 'Sábado'));
                $actDiaSemana->push($actividadesEmpleado->where('dia', 'Domingo'));
                $actDiaSemana->push($actividadesEmpleado->where('dia', 'Lunes'));
                $actDiaSemana->push($actividadesEmpleado->where('dia', 'Martes'));
                $actDiaSemana->push($actividadesEmpleado->where('dia', 'Miércoles'));

                foreach ($actDiaSemana as $actDia) {
                    if (!$actDia->isEmpty()) {
                        $cuentas = $this->obtenerActTipoCuenta($actDia);
                        if (!$cuentas->isEmpty()) {
                            foreach ($cuentas as $cuenta) {              
                                // ESTRUCTURA DE DATOS
                                $data = [
                                    'semana' => $semana->semana,
                                    'noEmpleado' => $empleado->noEmpleado,
                                    'empleado' => $empleado->nombre,
                                    'dia' => $cuenta->dia,
                                    'mes' => $cuenta->mes,
                                    'noDia' => $cuenta->noDia,
                                    'totalH' => $cuenta->horas,
                                    'tipoCuenta' => $cuenta->tipoCuenta,
                                    'cuenta' => $cuenta->cuenta,
                                    'lineaNegocio' => $cuenta->lineaNegocio,
                                ];
                                $result->push(collect($data));
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }

    public function obtenerHrsTipoCuenta($actividades, $tipo)
    {
        if ($tipo == "Externa" || $tipo == "Interna") {
            $cuentas = collect();
            $dataResult = collect();
            foreach ($actividades as $actividad) {
                $cuentas->push($actividad->cuenta);
            }
            $cuentas = $cuentas->unique();
            $temFirst = $actividades->first();
            foreach ($cuentas as $cuenta) {
                $hrs = 0;
                $actividadesCuenta = $actividades->where("cuenta", $cuenta);

                foreach ($actividadesCuenta as $actCuenta) {
                    $duracion = explode(' ', $actCuenta->horas, 2);
                    $hrsAct = $duracion[0];
                    $hrs = floatval($hrs) + floatval($hrsAct);
                }

                if ($tipo == "Externa") {
                    $arrCuenta = explode('-', $cuenta);
                    $lineaNegocio = $arrCuenta[2];
                }else {
                    $arrCuenta = explode('-', $cuenta);
                    $lineaNegocio = $arrCuenta[0];
                }
                $data = (object) [
                    "tipoCuenta" => $temFirst->tipoCuenta,
                    "cuenta" => $cuenta,
                    "horas" => $hrs,
                    "dia" => $temFirst->dia,
                    "noDia" => $temFirst->noDia,
                    "mes" => $temFirst->mes,
                    "lineaNegocio" => $lineaNegocio
                ];
                $dataResult->push($data);
            }
            return $dataResult;
        }else if ($tipo == "Capacitacion") {
            $hrs = 0;
            foreach ($actividades as $actividad) {
                $duracion = explode(' ', $actividad->horas, 2);
                $hrsAct = $duracion[0];
                $hrs = floatval($hrs) + floatval($hrsAct);
            }
            $temFirst = $actividades->first();
            $arrCuenta = explode('-', $temFirst->cuenta);
            $lineaNegocio = $arrCuenta[0];

            $data = (object) [
                "tipoCuenta" => $temFirst->tipoCuenta,
                "cuenta" => $temFirst->cuenta,
                "horas" => $hrs,
                "dia" => $temFirst->dia,
                "noDia" => $temFirst->noDia,
                "mes" => $temFirst->mes,
                "lineaNegocio" => $lineaNegocio
            ];
            return $data;
        }
    }

    public function obtenerActTipoCuenta($actividades)
    {
        $cuentas = collect();
        $actCapacitacion = $actividades->where("tipoCuenta", "Capacitacion");
        $actExterna = $actividades->where("tipoCuenta", "Externa");
        $actInterna = $actividades->where("tipoCuenta", "Interna");

        if (!$actCapacitacion->isEmpty()) {
            $dataCapacitacion = $this->obtenerHrsTipoCuenta($actCapacitacion, "Capacitacion");
            $cuentas->push($dataCapacitacion);
        }
        if (!$actExterna->isEmpty()) {
            $dataExterna = $this->obtenerHrsTipoCuenta($actExterna, "Externa");
            $cuentas = $cuentas->concat($dataExterna);
        }
        if (!$actInterna->isEmpty()) {
            $dataInterna = $this->obtenerHrsTipoCuenta($actInterna, "Interna");
            $cuentas = $cuentas->concat($dataInterna);
        }
        return $cuentas;
    }

    public function exportExcelReport($semana)
    {   
        $lineaNegocio = $_GET['lineaNegocio'];
        $nombre = 'RA-S' . $semana . '.xlsx';

        $actividades = Actividad::where('idSemana', $semana)->orderBy('fecha', 'ASC')->orderBy('inicio', 'ASC')->get();
         // dd($actividades);

        // DEFINIR ESTRUCTURA DE DATOS
        $result = $this->obtenerDatosActividades($actividades, $lineaNegocio);
        return (new ActividadesDeServiciosExport($result, "OP"))->download($nombre);
    }

    // public function obtenerCuentasDia($actividades)
    // {
    //     $cuentas = collect();
    //     $cuentaAnterior = "";
    //     foreach ($actividades as $actividad) {
    //         $duracion = explode(' ', $actividad->horas, 2);
    //         $horasAct = $duracion[0];
    //         if ($actividad->cuenta != $cuentaAnterior) {

    //             $cuenta = (object) [
    //                     "tipoCuenta" => $actividad->tipoCuenta,
    //                     "cuenta" => $actividad->cuenta,
    //                     "horas" => $horasAct
    //             ];
    //             $cuentas->push($cuenta);  
    //         } else {
    //             $act = $cuentas->where('cuenta', $cuentaAnterior)->last();
    //             $act->horas = $act->horas + $horasAct;
    //         }
    //         $cuentaAnterior = $actividad->cuenta;
    //     }
    //     return $cuentas;
    // }

    //Exporta a excel el resumen de todos los empleados de una semana específica
    public function exportExcelReportForTH($semana)
    {
        $nombre = 'RA-S' . $semana . '.xlsx';
        $actividades = Actividad::where('idSemana', $semana)->orderBy('fecha', 'ASC')->orderBy('inicio', 'ASC')->get();

        $empleados = collect();

        foreach ($actividades as $actividad) {
            $empleados = $empleados->concat([Empleado::where('noEmpleado', $actividad->noEmpleado)->first()]);
        }

        $empleados = $empleados->unique();
        $semana = collect();
        
        foreach ($empleados as $empleado) {
            $noEmpleadoCompleto = explode('-', $empleado->noEmpleado, 2);
            $empleado->consecutivo = $noEmpleadoCompleto[0];

            //Obtiene las actividades del empleado
            $act = $actividades->where('noEmpleado', $empleado->noEmpleado);

            //Separa las actividades del empleado segun el día
            $actSemana = collect();
            $actSemana->push($act->where('dia', 'Jueves'));
            $actSemana->push($act->where('dia', 'Viernes'));
            $actSemana->push($act->where('dia', 'Sábado'));
            $actSemana->push($act->where('dia', 'Domingo'));
            $actSemana->push($act->where('dia', 'Lunes'));
            $actSemana->push($act->where('dia', 'Martes'));
            $actSemana->push($act->where('dia', 'Miércoles'));

            $semana = collect();

            foreach ($actSemana as $actDia) {
                if (!$actDia->isEmpty()) {
                    $totalHoras = $this->calculaHoras($actDia);
                    $cuentas = $this->obtenerCuentasDiaOld($actDia);
                    $tipos =  $this->obtenerTipoCuentasDia($actDia);
                    //Se obtiene el días de la semana
                    $dia = $actDia->first();
                    $dia->semana = $dia->semana->semana;

                    // Se dividen en horas y horas extras
                    if ($totalHoras > 8) {
                        $horas = 8;
                        $extras = $totalHoras - 8;
                    } else {
                        $horas = $totalHoras;
                        $extras = '';
                    }
 
                    $data = [
                        'semana' => $dia->semana,
                        'dia' => $dia->dia,
                        'mes' => $dia->mes,
                        'noDia' => $dia->noDia,
                        'horas' => $horas,
                        'extras' => $extras,
                        'totalH' => $totalHoras,
                        'tipoCuenta' => $tipos,
                        'cuentas' => $cuentas,
                    ];
                   
                    $semana->push(collect($data));
                    $empleado->semana = $semana;
                }
            }
        }
        return (new ActividadesDeServiciosExport($empleados, "TH"))->download($nombre);
    }

    public function obtenerTipoCuentasDia($actividades)
    {
        $tipos = collect();
        foreach ($actividades as $actividad)
            $tipos->push($actividad->tipoCuenta);
        $tipos = $tipos->unique();
        $tiposConcat = '';
        $i = 1;
        foreach ($tipos as $tipo) {
            $actByTipo = $actividades->where('tipoCuenta', $tipo);
            $horasByTipo = 0;
            foreach ($actByTipo as $act) {
                $duracion = explode(' ', $act->horas, 2);
                $horasAct = $duracion[0];
                $horasByTipo += $horasAct;
            }
            if ($i > 1)
                $tiposConcat = $tiposConcat . ' / ';
            $tiposConcat = $tiposConcat . $tipo . ' (' . $horasByTipo . ')';
            $i++;
        }
        return $tiposConcat;
    }

    //Obtiene las cuentas del día con sus horas trabajadas
    public function obtenerCuentasDiaOld($actividades)
    {
        $cuentas = collect();
        foreach ($actividades as $actividad)
            $cuentas->push($actividad->cuenta);
        $cuentas = $cuentas->unique();
        $cuentasConcat = '';
        $i = 1;
        foreach ($cuentas as $cuenta) {
            $actByCuenta = $actividades->where('cuenta', $cuenta);
            $horasByCuenta = 0;
            foreach ($actByCuenta as $act) {
                $duracion = explode(' ', $act->horas, 2);
                $horasAct = $duracion[0];
                $horasByCuenta += $horasAct;
            }
            if ($i > 1)
                $cuentasConcat = $cuentasConcat . ' / ';
            $cuentasConcat = $cuentasConcat . $cuenta . ' (' . $horasByCuenta . ')';
            $i++;
        }
        return $cuentasConcat;
    }

    public function calculaHoras($actividades)
    {
        $suma = 0;
        foreach ($actividades as $actividad) {
            $duracion = explode(' ', $actividad->horas, 2);
            $total = $duracion[0];
            $suma = $suma + $total;
        }
        return $suma;
    }
}
