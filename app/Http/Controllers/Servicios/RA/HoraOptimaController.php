<?php

namespace App\Http\Controllers\Servicios\RA;
use App\Models\Servicios\RA\HoraOptima;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HoraOptimaController extends Controller
{
    public function getHorasOptimas($periodo) 
    {
        return HoraOptima::where('periodo', $periodo)->get();
    }

    public function store(Request $request)
    {
        if($request->id)
            $horaOptima = HoraOptima::find($request->id);
        else
            $horaOptima = new HoraOptima; 
            
        $horaOptima->mes = $request->mes;
        $horaOptima->tipoCuenta = $request->tipoCuenta;
        $horaOptima->horas = $request->horas;
        $horaOptima->periodo = $request->periodo;
        $save = $horaOptima->save();

        $data = [];
        $data['success'] = $save;
        $data['horaOptima'] = $horaOptima;

        return $data;
    }
}
