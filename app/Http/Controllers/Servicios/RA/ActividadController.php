<?php

namespace App\Http\Controllers\Servicios\RA;

use App;
use DB;
use App\Models\TalentoHumano\Empleado;
use Illuminate\Http\Request;
use App\Models\Servicios\RA\Reporte;
use App\Models\Servicios\RA\Actividad;
use App\Models\Servicios\RA\Semana;
use App\Http\Controllers\Controller;


class ActividadController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getActividadesAll(Request $request) 
    {
        if ($request->fechaInicio && $request->fechaFin) {
            $actividades = Actividad::whereBetween('fecha', [$request->fechaInicio, $request->fechaFin])
                ->orderBy('fecha', 'ASC')
                ->get();
            return $actividades;
        
        }else {
            $semanasPeriodo = Semana::where("year", $request->periodo)
                ->orderBy('inicio', 'DES')
                ->get();

            $fechaInicio = $semanasPeriodo[count($semanasPeriodo) - 1]->inicio;
            $fechaFin = $semanasPeriodo[0]->fin;

            $actividades = Actividad::whereBetween('fecha', [$fechaInicio, $fechaFin])
                ->orderBy('fecha', 'ASC')
                ->get();

            return $actividades;
        }
    }

    public function getActividades($noEmpleado, $fecha)
    {
        $actividades = DB::table('op_ra_actividades')->where('noEmpleado', $noEmpleado)->where('fecha', $fecha)->orderBy('inicio', 'ASC')->get();

        //AQUI ESTÁ EL PEDO DE LA SEMANA
        $semana = DB::table('op_ra_semanas')->where('inicio', '<=', $fecha)->where('fin', '>=', $fecha)->first();

        foreach ($actividades as $actividad) {
            // $semana = $actividad->semana;
            $actividad->noSemana = $semana->semana;
        }
        $data = [];
        $data['actividades'] = $actividades;
        $data['semana'] = $semana;
        return $data;
    }

    public function store(Request $request)
    {
        $actividad = new Actividad;
        $actividad->turno = $request->turno;
        $actividad->noEmpleado = $request->noEmpleado;
        $actividad->idSemana = $request->idSemana;
        $actividad->equipo = $request->equipo;
        $actividad->tipoCuenta = $request->tipoCuenta;
        $actividad->cuenta = $request->cuenta;
        $actividad->horas = $request->horas;
        $actividad->tipoTiempo = $request->tipoTiempo;
        $actividad->inicio = $request->inicio;
        $actividad->fin = $request->fin;
        $actividad->fecha = $request->fecha;
        $actividad->descripcion = $request->descripcion;
        $actividad->dia = $request->dia;
        $actividad->noDia = $request->noDia;
        $actividad->mes = $request->mes;
        $saved = $actividad->save();
        $actividad->noSemana = $actividad->semana->semana;
        $data = [];
        $data['success'] = $saved;
        $data['actividad'] = $actividad;
        return $data;
    }

    public function update(Actividad $actividad, Request $request)
    {
        $actividad = Actividad::find($actividad->idActividad);
        $actividad->turno = $request->turno;
        $actividad->equipo = $request->equipo;
        $actividad->tipoCuenta = $request->tipoCuenta;
        $actividad->cuenta = $request->cuenta;
        $actividad->horas = $request->horas;
        $actividad->tipoTiempo = $request->tipoTiempo;
        $actividad->inicio = $request->inicio;
        $actividad->fin = $request->fin;
        $actividad->fecha = $request->fecha;
        $actividad->descripcion = $request->descripcion;
        $actividad->dia = $request->dia;
        $actividad->noDia = $request->noDia;
        $actividad->mes = $request->mes;
        $saved = $actividad->save();
        $data = [];
        $data['success'] = $saved;
        $data['actividad'] = $actividad;

        return $data;
    }

    public function delete(Actividad $actividad)
    {
        $deleted = $actividad->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getCuentasRegistradas($idSemana, $tipoCuenta)
    {
        return DB::table('op_ra_actividades')->select('cuenta')->where('idSemana', $idSemana)->where('tipoCuenta', $tipoCuenta)->get()->unique('cuenta')->values();
    }

    public function getEquiposRegistrados($idSemana)
    {
        return DB::table('op_ra_actividades')->select('equipo')->where('idSemana', $idSemana)->get()->unique('equipo')->values();
    }
}
