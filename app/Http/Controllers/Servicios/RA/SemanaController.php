<?php

namespace App\Http\Controllers\Servicios\RA;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Servicios\RA\Semana;
use App\Http\Controllers\Controller;

class SemanaController extends Controller
{
    public function getSemanas($year)
    {
    	return Semana::where('year', $year)->get();
    }

    public function updateSemana(Semana $semana, Request $request)
    {

    	$semana->inicio = $request->inicio;
    	$semana->fin = $request->fin;
    	$saved = $semana->save();

    	$data = [];
        $data['success'] = $saved;
        return $data;
    }

    public function updateSemanaActiva(Semana $semana, Request $request)
    {
    	$semana->activa_inicio = $request->inicio;
    	$semana->activa_fin = $request->fin;
    	$saved = $semana->save();

    	$data = [];
        $data['success'] = $saved;
        return $data;
    }

    public function getSemanaByFecha($fecha)
    {
        return Semana::where('inicio','<=',$fecha)->where('fin','>=',$fecha)->first();
    }
}
