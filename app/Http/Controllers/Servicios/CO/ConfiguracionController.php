<?php

namespace App\Http\Controllers\Servicios\CO;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Servicios\CO\Inventarios\Activo;

class ConfiguracionController extends Controller
{
    public function storeActivo(Request $request)
    {
        Activo::truncate();
        $data = json_decode($request->getContent(), true);
        foreach ($data as $item) {
            $articulo = new Activo;
            $articulo->unidad = $item['unidad'];
            $articulo->articulo = $item['articulo'];
            $articulo->existencia = $item['existencia'] == "" ? 0 : $item['existencia'];
            $articulo->costoUnitario = $item['costoUnitario'] == "" ? 0 : $item['costoUnitario'];
            $articulo->categoria = $item['categoria'];
            $saved = $articulo->save();
        }
        $result = [];
        $result['success'] = true;
        return $result;
    }

    public function getActivos()
    {
        return Activo::all();
    }

    public function getActivoCategorias()
    {
        $activos = Activo::all();
        $activos = $activos->unique('categoria');
        $categorias = collect();
        $categoria = array();
        foreach($activos as $activo){
            if($activo->categoria != "") {
                $categoria['categoria'] = $activo->categoria;
                $categorias->push($categoria);
            }
        }
        return $categorias;
    }
}
