<?php

namespace App\Http\Controllers\Servicios\CO;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\Servicios\CO\Costo;
use App\Models\Servicios\CO\ManoObra;
use App\Models\Servicios\CO\CostoFactor;
use App\Models\Servicios\CO\GastoEspecial;
use App\Models\Servicios\CO\MaterialInsumo;
use App\Models\Servicios\CO\GastoOperacion;
use App\Models\Servicios\CO\CostoSeguridad;
use App\Models\Servicios\CO\CostoObservacion;
use App\Models\Servicios\CO\HerramientaEquipo;
use App\Models\Servicios\CO\CostoEtapa;
use App\Models\Ventas\Embudo;

use App\Models\Finanzas\CO\Factor;
use App\Models\Finanzas\CO\Puesto;
use App\Models\Finanzas\CO\HerramientaEquipo as InventarioHerramientaEquipo;

use App\Models\Servicios\CO\Inventarios\Activo;

class CostoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $costo = new Costo;
        $costo->cuenta = $request->cuenta;
        $costo->proyecto = $request->proyecto;
        $costo->duracion = $request->duracion;
        $costo->jornadaLaboral = $request->jornadaLaboral;
        $costo->jornadaInterna = $request->jornadaInterna;
        $costo->jornadaDominical = $request->jornadaDominical;
        $costo->cuentaReal = "Inactivo";
        $savedCosto = $costo->save();

        if ($savedCosto) {
            $factor = Factor::all()->first();
            $CostoFactor = new CostoFactor;
            $CostoFactor->iva = $factor->iva;
            $CostoFactor->domingo = $factor->domingo;
            $CostoFactor->jornadaOcho = $factor->jornadaOcho;
            $CostoFactor->jornadaDoce = $factor->jornadaDoce;
            $CostoFactor->moDisponible = $factor->moDisponible;
            $CostoFactor->idCosto = $costo->idCosto;
            $savedFactor = $CostoFactor->save();

            $puestos = Puesto::all();
            $data = [];
            foreach ($puestos as $puesto) {
                $obj = [
                    "cantidad" => 0,
                    "puesto" => $puesto->puesto,
                    "costoUnitario" => $puesto->sueldo,
                    "factorUtilizacion" => $puesto->factorUtilizacion,
                ];
                array_push($data, $obj);        
            }
            $manoObra = new ManoObra;
            $manoObra->manoObra = json_encode($data);
            $manoObra->cuenta = "Planeada";
            $manoObra->idCosto = $costo->idCosto;
            $savedManoObra = $manoObra->save();

            $articulos = InventarioHerramientaEquipo::all();
            $data = [];
            foreach ($articulos as $articulo) {
                $obj = [
                    "cantidad" => 0,
                    "unidad" => $articulo->unidad,
                    "descripcion" => $articulo->articulo,
                    "costoUnitario" => $articulo->costoUnitario,
                ];
                array_push($data, $obj);
            }
            $herramientaEquipo = new HerramientaEquipo;
            $herramientaEquipo->herramientasEquipos = json_encode($data);
            $herramientaEquipo->cuenta = "Planeada";
            $herramientaEquipo->idCosto = $costo->idCosto;
            $savedHerramientaEquipo = $herramientaEquipo->save(); 

            $materialInsumo = new MaterialInsumo;
            $materialInsumo->cuenta = "Planeada";
            $materialInsumo->idCosto = $costo->idCosto;
            $savedMaterialInsumo = $materialInsumo->save();

            $gastoOperacion = new GastoOperacion;
            $gastoOperacion->cuenta = "Planeada";
            $gastoOperacion->idCosto = $costo->idCosto;
            $savedGastoOperacion = $gastoOperacion->save();

            $gastoEspecial = new GastoEspecial;
            $gastoEspecial->cuenta = "Planeada";
            $gastoEspecial->idCosto = $costo->idCosto;
            $savedGastoEspecial = $gastoEspecial->save();

            $costoSeguridad = new CostoSeguridad;
            $costoSeguridad->contacto = 0;
            $costoSeguridad->incidente = 0;
            $costoSeguridad->accidente = 0;
            $costoSeguridad->idCosto = $costo->idCosto;        
            $savedCostoSeguridad = $costoSeguridad->save();
        }
        $data = [];
        $data['success'] = $savedCosto;
        $data['costo'] = $costo;
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Servicios\CO\Costo  $costo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Costo $costo)
    {
        $costo->jornadaLaboral = $request->jornadaLaboral;
        $costo->jornadaInterna = $request->jornadaInterna;
        $costo->jornadaDominical = $request->jornadaDominical;
        $costo->duracion = $request->duracion;
        $updated = $costo->update();
        $data = [];
        $data['success'] = $updated;
        $data['costo'] = $costo;
        return $data;
    }


    public function destroy(Costo $costo)
    {
        $factor = CostoFactor::where("idCosto", $costo->idCosto)->first();
        $deletedFactor = $factor->delete();

        $gastoEspecial = GastoEspecial::where("idCosto", $costo->idCosto)->first();
        $deletedGE = $gastoEspecial->delete();

        $gastoOperacion = GastoOperacion::where("idCosto", $costo->idCosto)->first();
        $deletedGO = $gastoOperacion->delete();

        $herramientaEquipo = HerramientaEquipo::where("idCosto", $costo->idCosto)->first();
        $deletedHE = $herramientaEquipo->delete();

        $manoObra = ManoObra::where("idCosto", $costo->idCosto)->first();
        $deletedMO = $manoObra->delete();

        $materialInsumo = MaterialInsumo::where("idCosto", $costo->idCosto)->first();
        $deletedMI = $materialInsumo->delete();

        $costoSeguridad = CostoSeguridad::where("idCosto", $costo->idCosto)->first();
        $deletedCS = $costoSeguridad->delete();

        $observaciones = CostoObservacion::where("idCosto", $costo->idCosto)->get();
        
        foreach ($observaciones as $observacion) {
            $observacion->delete();
        }

        $deleted = $costo->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getCostos()
    {
        $costos = DB::table('op_co_costos')
        ->join('negocios', 'op_co_costos.cuenta', '=', 'negocios.claveNegocioCompleta')
        ->select(
            'op_co_costos.*',
            'negocios.idEtapa as etapa_negocio',
            'negocios.idEmbudo'
        )
        ->where('negocios.estimacion', "==" , 0)
        ->get();
        return $costos;
    }

    public function getManoObra(Costo $costo)
    {
        return $costo->manoObra()->where('cuenta', "Planeada")->first();
    }

    public function getHerramientaEquipo(Costo $costo)
    {
        return $costo->herramientaEquipo()->where('cuenta', "Planeada")->first();
    }

    public function getMaterialInsumo(Costo $costo)
    {
        return $costo->materialesInsumos()->where('cuenta', "Planeada")->first();
    }

    public function getGastoOperacion(Costo $costo)
    {
        return $costo->gastosOperacion()->where('cuenta', "Planeada")->first();
    }

    public function getGastoEspecial(Costo $costo)
    {
        return $costo->gastosEspeciales()->where('cuenta', "Planeada")->first();
    }

    public function getObservaciones(Costo $costo)
    {
        return $costo->observaciones;
    }

    public function getCostoSeguridad(Costo $costo)
    {
        return $costo->seguridad;
    }

    public function getCostoFactores(Costo $costo)
    {
        $factores = DB::table('op_co_factores')
            ->where('idCosto', $costo->idCosto)
            ->first();
        return json_encode($factores);
    }

    public function updateManoObra(ManoObra $manoObra, Request $request)
    {
        $manoObra->manoObra = $request->manoObra;
        $updated = $manoObra->update();
        $data = [];
        $data['success'] = $updated;
        $data['manoObra'] = $manoObra;
        return $data;
    }

    public function updateHerramientaEquipo(HerramientaEquipo $herramientaEquipo, Request $request)
    {
        $herramientaEquipo->herramientasEquipos = $request->herramientasEquipos;
        $updated = $herramientaEquipo->update();
        $data = [];
        $data['success'] = $updated;
        $data['herramientaEquipo'] = $herramientaEquipo;
        return $data;
    }

    public function updateGastoEspecial(GastoEspecial $gastoEspecial, Request $request)
    {
        $gastoEspecial->gastosEspeciales = $request->gastos; 
        $updated = $gastoEspecial->update();
        $data = [];
        $data['success'] = $updated;
        $data['gastoEspecial'] = $gastoEspecial;
        return $data;
    }

    public function getCuentaContableViaticos()
    {
        return DB::table('fn_cuentas_contables')
            ->where('area', 'Campo')
            ->where('cuentaMayor', '12 Gastos de Viaje y Viáticos')
            ->get();
    }

    public function updateGastoOperacion(GastoOperacion $gastoOperacion, Request $request)
    {
        $gastoOperacion->gastosOperacion = $request->gastos;
        $updated = $gastoOperacion->update();
        $data = [];
        $data['success'] = $updated;
        $data['gastoOperacion'] = $gastoOperacion;
        return $data;
    }

    public function storeObservacion(Request $request)
    {
        $observacion = new CostoObservacion;
        $observacion->observacion = $request->observacion;
        $observacion->idUsuario = $request->idUsuario;
        $observacion->idCosto = $request->idCosto;
        $saved = $observacion->save();
        $data = [];
        $data['success'] = $saved;
        $data['observacion'] = $observacion;
        return $data;
    }

    public function destroyObservacion(CostoObservacion $observacion)
    {
        $deleted = $observacion->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function updateObservacion(CostoObservacion $observacion, Request $request)
    {
        $observacion->observacion = $request->observacion;
        $updated = $observacion->update();
        $data = [];
        $data['success'] = $updated;
        $data['observacion'] = $observacion;
        return $data;
    }

    public function updateMaterialInsumo(MaterialInsumo $materialInsumo, Request $request)
    {
        $materialInsumo->materialesInsumos = $request->articulos;
        $updated = $materialInsumo->update();
        $data = [];
        $data['success'] = $updated;
        $data['materialInsumo'] = $materialInsumo;
        return $data;
    }

    public function updateCostoSeguridad(CostoSeguridad $costoSeguridad, Request $request)
    {
        $costoSeguridad->contacto = $request->contacto;
        $costoSeguridad->incidente = $request->incidente;
        $costoSeguridad->accidente = $request->accidente;
        $updated = $costoSeguridad->update();
        $data = [];
        $data['success'] = $updated;
        $data['costoSeguridad'] = $costoSeguridad;
        return $data;
    }

    public function generateReport(Request $request) {
        $data = request();
        $cuenta = $data->costo['cuenta'];
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('modulos.servicios.reportes.cuenta_planeada', ['data' => $data]);
        Storage::put("OP/CO/CO-CP-".$cuenta.".pdf", $pdf->output());
        $result = [];
        $result['success'] = true; 
        return $result;
    }

    public function downloadReport($name) {
        return Storage::download("OP/CO/".$name.".pdf");
    }

    public function getConfCostoEtapa()
    {
        return CostoEtapa::all();
    }
}
