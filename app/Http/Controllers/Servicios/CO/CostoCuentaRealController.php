<?php

namespace App\Http\Controllers\Servicios\CO;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;

use App\Models\Servicios\CO\Costo;
use App\Models\Servicios\CO\GastoEspecial;
use App\Models\Servicios\CO\MaterialInsumo;
use App\Models\Servicios\CO\HerramientaEquipo;
use App\Models\Servicios\CO\CostoEtapa;
use App\Models\Ventas\Embudo;

use App\Notifications\Servicios\CO\CuentaReal;


class CostoCuentaRealController extends Controller
{
    public function getManoObra($cuenta)
    {
        return DB::table('op_ra_actividades')
            ->join('empleados', 'op_ra_actividades.noEmpleado', '=', 'empleados.noEmpleado')
            ->join('op_ra_semanas', 'op_ra_actividades.idSemana', '=', 'op_ra_semanas.idSemana')
            ->select(
                'op_ra_actividades.dia',
                DB::raw(
                    'SUM(SUBSTRING_INDEX(op_ra_actividades.horas, " ", 1)) as total_horas'
                ),
                'op_ra_semanas.year',
                'op_ra_semanas.semana',
                'op_ra_actividades.fecha',
                'op_ra_actividades.noEmpleado',
                'empleados.nombre as responsable',
                'op_ra_actividades.dia')
            ->where('op_ra_actividades.cuenta', $cuenta)
            ->groupBy(
                'op_ra_actividades.idSemana',
                'op_ra_actividades.fecha', 
                'op_ra_actividades.noEmpleado', 
                'op_ra_actividades.dia'
            )
            ->get();
    }

    public function getGastosOperacion($cuenta)
    {
        return DB::table('fn_cg_comprobaciones')
            ->join('fn_cg_solicitudes', 'fn_cg_comprobaciones.idSolicitud', '=', 'fn_cg_solicitudes.idSolicitud')
            ->join('usuarios', 'fn_cg_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
            ->select(
                DB::raw(
                    'SUM(fn_cg_comprobaciones.importe) as total'
                ),
                'fn_cg_comprobaciones.idSolicitud',
                'fn_cg_solicitudes.fechaFinGastoReal as fecha',
                'usuarios.nombreUsuario as responsable'
            )
            ->where('fn_cg_comprobaciones.cuenta', $cuenta)
            ->where('fn_cg_solicitudes.idEtapa', 9)

            ->groupBy(
                'fn_cg_comprobaciones.idSolicitud'
            )            
            ->get();
    }

    public function getSueldos()
    {
        return DB::table('rh_sueldos')->get();
    }

    public function getMaterialInsumo(Costo $costo)
    {
        return $costo->materialesInsumos()->where('cuenta', "Real")->first();
    }
    
    public function getGastoEspecial(Costo $costo)
    {
        return $costo->gastosEspeciales()->where('cuenta', "Real")->first();
    }

    public function getHerramientaEquipo(Costo $costo)
    {
        return $costo->herramientaEquipo()->where('cuenta', "Real")->first();
    }

    public function copyCuenta(Request $request)
    {
        // $year = date("Y");
        // $embudo = Embudo::where("periodo", $year)->first();
        $config = CostoEtapa::where("idEmbudo", $request->idEmbudo)->first();
        $data = [];
        if ($request->etapa == $config->etapaGanado) {   
            $costo = Costo::where("cuenta", $request->cuenta)->first();
            if ($costo) {
                if ($costo->cuentaReal == "Inactivo") {
                    //Materiales e Insumos
                    $cpMateriales = MaterialInsumo::where("idCosto", $costo->idCosto)->where("cuenta", "Planeada")->first();
                    $materialInsumo = new MaterialInsumo;
                    $materialInsumo->materialesInsumos = $cpMateriales->materialesInsumos;
                    $materialInsumo->cuenta = "Real";
                    $materialInsumo->idCosto = $costo->idCosto;
                    $savedMaterialInsumo = $materialInsumo->save();

                    //Gastos Especiales
                    $cpGastos = GastoEspecial::where("idCosto", $costo->idCosto)->where("cuenta", "Planeada")->first();
                    $gastoEspecial = new GastoEspecial;
                    $gastoEspecial->gastosEspeciales = $cpGastos->gastosEspeciales;
                    $gastoEspecial->cuenta = "Real";
                    $gastoEspecial->idCosto = $costo->idCosto;
                    $savedGastoEspecial = $gastoEspecial->save();

                    //herramienta y equipo
                    $cpHerramientas = HerramientaEquipo::where("idCosto", $costo->idCosto)->where("cuenta", "Planeada")->first();
                    $herramientaEquipo = new HerramientaEquipo;
                    $herramientaEquipo->herramientasEquipos = $cpHerramientas->herramientasEquipos;
                    $herramientaEquipo->cuenta = "Real";
                    $herramientaEquipo->idCosto = $costo->idCosto;
                    $savedHerramientaEquipo = $herramientaEquipo->save();
                    
                    if ($savedMaterialInsumo || $savedGastoEspecial || $savedHerramientaEquipo) { 
                        $costo->cuentaReal = "Activo";
                        $saved = $costo->save();
                        if ($saved) {
                            $data = [
                                "cuenta" => $request->cuenta
                            ];
                            // 70 - anahi almacen, 80 - Maribel compras, 11 - beatriz - finanzas, 23 - Guillermo seguridad
                            $users = [70, 80, 11, 23];
                            foreach($users as $id) {
                                $notifiable = User::find($id);
                                $notifiable->notify(new CuentaReal($data));
                            }
                        }
                        $data['success'] = $saved;
                        return $data;
                    }else {
                        $data['success'] = false;
                        return $data;
                    }
                } 
            }
        }
        $data['success'] = false;
        return $data;
    }

    public function copyHerramientaCR(Request $request)
    {
        $herramientaEquipo = new HerramientaEquipo;
        $herramientaEquipo->cuenta = $request->cuenta;
        $herramientaEquipo->idCosto = $request->idCosto;
        $herramientaEquipo->herramientasEquipos = $request->herramientasEquipos;
        $saved = $herramientaEquipo->save();
        $data = [];
        $data['success'] = $saved;
        return $data;
    }
}
