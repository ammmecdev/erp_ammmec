<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EvaluacionPregunta;

class EvaluacionPreguntaController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function getEvaluacionesPreguntas()
    {
        return EvaluacionPregunta::all();
        // $evaluacionesPreguntas = [];
        // $evaluaciones = EvaluacionServicio::where('idEmbudo', $request->idEmbudo)->get();
        // foreach ($evaluaciones as $object) {
        //     array_push($evaluacionesPreguntas, $object->evaluacion_pregunta);
        // }
    }

    public function update(Request $request)
    {
    	$evaluacionPregunta = EvaluacionPregunta::find($request->id);
    	$evaluacionPregunta->valor = $request->valor;
    	$saved = $evaluacionPregunta->save();
        $data = [];
        $data['success'] = $saved;
        $data['evaluacionPregunta'] = $evaluacionPregunta;
        return $data;
    }
}
