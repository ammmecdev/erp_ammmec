<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;

class NotificationsController extends Controller
{
	public function __construct()
    {
		//$this->middleware('auth');
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

	public function index()
	{
		return view('notifications.index');
	}

	public function getNotifications()
	{
		return auth()->user()->notifications;
	}

	public function read(Request $request)
	{
		DatabaseNotification::find($request->id)->markAsRead();
		$notification = Notification::find($request->id);
		return $notification;
	}

	public function destroy($id)
	{
		DatabaseNotification::find($id)->delete();
		return back()->with('flash', 'Notificacion eliminada');
	}
}
