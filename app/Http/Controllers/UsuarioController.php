<?php

namespace App\Http\Controllers;

use File;
use App\Usuario;
use Illuminate\Http\Request;
use App\Models\Cadmin\UsuarioRole;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class UsuarioController extends Controller
{
    public function getUsuarios() {
        return DB::table('usuarios')
            ->join('empleados', 'usuarios.noEmpleado', '=', 'empleados.noEmpleado')
            ->join('rh_puestos', 'empleados.idPuesto', '=', 'rh_puestos.idPuesto')
            ->join('rh_departamentos', 'rh_puestos.idDepartamento', '=', 'rh_departamentos.idDepartamento')
            ->select(
                'usuarios.*', 
                'rh_puestos.nombre as nombre_puesto',
                'rh_puestos.codigo as codigo_puesto',
                'rh_puestos.idPuesto as id_puesto',
                'rh_departamentos.idDepartamento as id_departamento',
                'rh_departamentos.nombre as nombre_departamento'
            )
            ->where('activo', 1)
            ->get();
    }

    public function getUsuariosAll()
    {
        return Usuario::all();
    }

    public function getUsuario(Usuario $usuario)
    {
        $db_usuario = DB::table('usuarios')
            ->join('empleados', 'usuarios.noEmpleado', '=', 'empleados.noEmpleado')
            ->join('rh_puestos', 'empleados.idPuesto', '=', 'rh_puestos.idPuesto')
            ->join('rh_departamentos', 'rh_puestos.idDepartamento', '=', 'rh_departamentos.idDepartamento')
            ->select(
                'usuarios.*', 
                'rh_puestos.nombre as nombre_puesto',
                'rh_puestos.codigo as codigo_puesto',
                'rh_puestos.idPuesto as id_puesto',
                'rh_departamentos.idDepartamento as id_departamento',
                'rh_departamentos.nombre as nombre_departamento',
                'empleados.tarjetaBanbajio as tarjeta_banbajio'
            )
            ->where('usuarios.idUsuario', $usuario->idUsuario)
            ->first();
        return json_encode($db_usuario);
    }

    public function getUsuarioSimple($idUsuario)
    {
        $db_usuario = DB::table('usuarios')
            ->select(
                'usuarios.idUsuario',
                'usuarios.nombreUsuario'
            )
            ->where('usuarios.idUsuario', $idUsuario)
            ->first();
        return json_encode($db_usuario);
    }

    public function getTarjetaEmpleado(Usuario $usuario)
    {
        return $usuario->empleado->tarjetaBanbajio;
    }

    public function store(Request $request) {
        $usuario = new Usuario;
        $usuario->nombreUsuario = $request->nombreUsuario;
        $usuario->usuario = $request->usuario;
        $usuario->email = $request->email;
        $usuario->pass = bcrypt($request->pass);
        if ($request->file('file')) {
            $usuario->foto = $request->file->getClientOriginalName();
            $request->file->move(public_path('img/profiles/'), $usuario->foto);
            $usuario->ruta = "/img/profiles/".$usuario->foto;
        }else {
            $usuario->foto = 'avatar.png';
            $usuario->ruta = '/img/profiles/avatar.png';
        }   
        $usuario->activo = $request->activo;
        $usuario->noEmpleado = $request->noEmpleado;
        $usuario->token_password = $request->token_password;
        $usuario->password_request = $request->password_request;
        $usuario->remember_token = $request->remember_token;
        $usuario->telefono = $request->telefono;
        $saved = $usuario->save();
        $data = [];
        $data['saved'] = $saved;
        $data['usuario'] = $usuario;
        return $data;
    }

    public function update(Request $request) {
        $usuario=Usuario::find($request->idUsuario);
        $usuario->nombreUsuario = $request->nombreUsuario;
        $usuario->usuario = $request->usuario;
        $usuario->email = $request->email;
        if ($request->pass != 'null')
           $usuario->pass = bcrypt($request->pass);

        if ($request->file('file')) {
            $nombreFoto = $request->file->getClientOriginalName();
            $coincidencias = Usuario::where('foto', $usuario->foto)->count();
            if ($coincidencias == 1) {
                File::delete(public_path('img/profiles/'.$usuario->foto));
            }
            $usuario->foto = $request->file->getClientOriginalName();
            $request->file->move(public_path('img/profiles/'), $usuario->foto);
            $usuario->ruta = "/img/profiles/".$usuario->foto;
        }
        $usuario->telefono = $request->telefono;
        $saved = $usuario->save();
        $data = [];
        $data['saved'] = $saved;
        $data['usuario'] = $usuario;
        return $data;
    }

    public function updateActivo(Request $request) {
        $usuario=Usuario::find($request->idUsuario);
        $usuario->activo = !$usuario->activo;
        $updated=$usuario->update();
        $data=[];
        $data['success']=$updated;
        $data['usuario']=Usuario::find($request->idUsuario);
        return $data;
    }

    public function delete($idUsuario) {
        $usuario = Usuario::find($idUsuario);
        $deleted = $usuario->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function selectUsuarios() {
        $usuario = Usuario::where('activo', 1)->get();
        $data = [];
    	foreach($usuario as $key => $value){
            if($value->nombreUsuario != 'Administrador'){
                $data[$key] = [
                    'text'  => $value->nombreUsuario,
                    'value' => [
                                'text' => $value->nombreUsuario,
                                'value' => $value->idUsuario
                                ]  
                ];
            }
    	}
    	return response()->json($data);
    }

    public function getRolesByUsuario(Usuario $usuario) 
    {
        return UsuarioRole::where('idUsuario', $usuario->idUsuario)->get();
    }

    public function getPermisosByUsuario(Usuario $usuario)
    {
        $permisosUsuario = collect();
        foreach($usuario->roles as $roleUsuario)
             $permisosUsuario = $permisosUsuario->concat($roleUsuario->permisos);
        return $permisosUsuario;
    }
}
