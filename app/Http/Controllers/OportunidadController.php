<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Oportunidad;


class OportunidadController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    public function getSelectOportunidad($idEmbudo)
	{
    	$oportunidad = $this->getOportunidad($idEmbudo);
        $data = [];
    	foreach($oportunidad as $key => $value){
    		$data[$key] = [
                'text'  => $value->oportunidad,
    			'value' =>  [
                                'idEtapa' => $value->idEtapa,
                                'nombreEtapa' => $value->nombreEtapa,
                                'value' => $value->idOportunidad
                            ]
    		];
    	}
    	return response()->json($data);
	}

    public function getSelectSegOportunidad($idEmbudo)
    {
        $oportunidad = $this->getOportunidad($idEmbudo);
        $data = [];
        foreach($oportunidad as $key => $value){
            $data[$key] = [
                'text'  => $value->numero.' - '.$value->oportunidad,
                'value' => $value->idOportunidad
            ];
        }
        return response()->json($data); 
    }

    public function getOportunidad($idEmbudo)
    {
        return Oportunidad::join('etapasventas','oportunidad.idEtapa', '=', 'etapasventas.idEtapa')->select('oportunidad.oportunidad','oportunidad.idOportunidad','oportunidad.idEtapa','oportunidad.numero', 'etapasventas.nombreEtapa','etapasventas.idEmbudo')->where('etapasventas.idEmbudo', $idEmbudo)->get();
    }
    public function getOportunidades()
    {
        return Oportunidad::all();
    }
     public function store(Request $request)
    {
        $oportunidad = new Oportunidad();
        $oportunidad->oportunidad = $request->nombre;
        $oportunidad->numero = $request->numero;
        $oportunidad->idEtapa = $request->idEtapa;
        $saved = $oportunidad->save();
   
        $data = [];
        $data['success'] = $saved;
        $data['oportunidad'] = $oportunidad;
        return $data;
    }
}
