<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Puesto;
use App\PerfilPuesto;
use Illuminate\Support\Facades\Storage;

class PerfilPuestoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']); 
    }

    public function store(Request $request)
    {
    	$perfil = new PerfilPuesto;

    	$ruta = "nr/puestos";

        $path = $request->file('file')->storeAs($ruta, request()->file->getClientOriginalName());

        $perfil->nombre = $request->file->getClientOriginalName();
        $perfil->tipo = $request->tipo;
        $perfil->clave = $request->clave;
        $perfil->version = $request->version;
        $perfil->codigo = $request->codigo;
        $perfil->ultima_modificacion = $request->ultimaModificacion;
        $perfil->size = $request->file->getSize();
        $perfil->formato = $request->file->extension();
        $perfil->ruta = $ruta;
        $perfil->idPuesto = $request->idPuesto;

        $saved = $perfil->save();

        $data = [];
        $data['success'] = $saved;
        $data['perfil'] = $perfil;

        return $data;
    }

     public function update(Request $request)
    {
    	$perfil = PerfilPuesto::find($request->idPerfil);

    	if($request->file('file')){
            
            $perfil_old = PerfilPuesto::find($request->idPerfil);
            Storage::delete($perfil_old->ruta."/".$perfil_old->nombre);

           	$ruta = "nr/puestos";

            $path = $request->file('file')->storeAs($ruta, request()->file->getClientOriginalName());

            $perfil->nombre = $request->file->getClientOriginalName();
            $perfil->ruta = $ruta;
            $perfil->size = $request->file->getSize();
            $perfil->formato = $request->file->extension();
        }
 
        $perfil->tipo = $request->tipo;
        $perfil->clave = $request->clave;
        $perfil->version = $request->version;
        $perfil->codigo = $request->codigo;
        $perfil->ultima_modificacion = $request->ultimaModificacion;

        $saved = $perfil->save();

        $data = [];
        $data['success'] = $saved;
        $data['perfil'] = $perfil;

        return $data;
    }
}
