<?php

namespace App\Http\Controllers;

use App;
use App\Exports\NegociosExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;


use Illuminate\Support\Facades\DB;
use Notification;
use App\User;
use App\Costo;
use App\Pago;
use App\Negocio;
use App\Models\Ventas\Etapa;
use App\Models\Ventas\Embudo;
use App\Factura;
use App\Actividad;
use App\Cotizacion;
use App\Participante;
use App\RegistroCambios;
use App\NegocioPerdido;
use Illuminate\Http\Request;
use App\Events\NotificationToUser;
use App\Models\Cadmin\CuentasExternas;
use App\Notifications\SendNotification;


class NegocioController extends Controller
{
    public function __construct(){
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function negocioById(Negocio $negocio){
    	return $negocio;
    }

    public function getNegociosActividades(Request $request){
        $negocios = DB::table('negocios')
        ->join('clientes', 'negocios.idCliente', '=', 'clientes.idCliente')
        ->select(
            'negocios.*', 
            'clientes.nombre as nombre_cliente')
        ->where('idEmbudo', $request->idEmbudo)->get();
        
        foreach ($negocios as $negocio) {
            $negocito = Negocio::find($negocio->idNegocio);
            $negocio->actividades = $negocito->actividades;
        }
        return $negocios;
    }

    public function getNegocio(Negocio $negocio){
        return $negocio;
    }

    public function getNegocios(Request $request){
        return DB::table('negocios')->where('idEmbudo', $request->idEmbudo)->get();   
    }

    public function getNegociosPage(Request $request){
         return Negocio::where('idEmbudo', $request->idEmbudo)->orderBy('idNegocio', 'DESC')->paginate(50);
    }

    public function getNegociosFacturados(Request $request){
       return DB::table('negocios')
            ->join('facturas', 'negocios.idNegocio', '=', 'facturas.idNegocio')
            ->join('pagos', 'negocios.idNegocio', '=', 'pagos.idNegocio')
            ->join('unidad_negocio', 'negocios.idUnidadNegocio', 'unidad_negocio.idUnidadNegocio')
            ->select('negocios.idNegocio', 'negocios.valorNegocio', 'negocios.tipoMoneda', 'negocios.xCambio', 'facturas.fechaFactura', 'pagos.fechaPago', 'negocios.idUnidadNegocio', 'unidad_negocio.unidadNegocio')
            ->where('negocios.idEmbudo', $request->idEmbudo)
            ->where('facturas.fechaFactura', '!=', null)
            ->get();
    }

    public function selectNegocios(Request $request){
        $negocio = Negocio::where('idEmbudo', $request->idEmbudo)->orderBy('idNegocio', 'DESC')->get();
        foreach($negocio as $key => $value){
            $data[$key] = [
                'text'  => $value->claveNegocio.' - '.$value->tituloNegocio,
                'value' => $value->idNegocio
            ];
        }
        return $data;
    }

    public function getConsecutivo(Request $request){
        $consecutivo = Negocio::where('claveCliente', $request->clave)->max('claveConsecutivo');
        if ($consecutivo == null)
            $consecutivo = str_pad(1, 4, "0", STR_PAD_LEFT);
        else
            $consecutivo = str_pad(++$consecutivo, 4, "0", STR_PAD_LEFT);
        return $consecutivo;
    }

    public function update(Request $request){
        date_default_timezone_set("America/Mexico_City");
        $campo1 = '';
        $campo2 = '';
        $fecha_actual = date("Y-m-d H:i:s");
        $negocio = Negocio::find($request->id);
        $statusNegocio = $negocio->status;
        if($statusNegocio == 0){
            $campo1 = 'Abierto';
        }else if($statusNegocio == 1){
            $campo1 = 'Ganado';
        }else{
            $campo1 = 'Perdido';
        }
        $negocio->idNegocio = $request->id;
        switch ($request->campo) {
            case 'tituloNegocio':
                $negocio->tituloNegocio = $request->value;
                break;
            case 'ponderacion':
                $negocio->ponderacion = $request->value;
                break;
            case 'valorNegocio':
                $negocio->valorNegocio = ($request->value != NULL) ? $request->value : 0;
                break;
            case 'precioNeto':
                $negocio->precioNeto = ($request->value != NULL) ? $request->value : 0;
                break;
            case 'idEtapa':
                $campo1 = $negocio->nombre_etapa;
                $negocio->idEtapa = $request->value;
                $negocio->contadorActivo = $fecha_actual;
                $campo2 = $negocio->nombre_etapa;
                $this->registroCambios('Etapa', $campo1, $campo2, $negocio->idNegocio);
                break;
            case 'rfq':
                $negocio->rfq = $request->value;
                break;
            case 'fechaRfq':
                $negocio->fechaRfq = $request->value;
                break;
            case 'idContacto':
                $negocio->idContacto = $request->value;
                break;
            case 'fechaPerdido':
                $negocio->fechaGanadoPerdido = $request->value;
                $negocio->status = 2;
                $campo2 = 'Perdido';
                if($request->idCausa)
                    $negocio->idCausa = $request->idCausa;
                $this->registroCambios('Estado', $campo1, $campo2, $negocio->idNegocio);
                break;
            case 'causa':
                $negocio->idCausa = $request->value;
                break;
            case 'pedido':
                $negocio->pedido = $request->value;
                break;
            case 'fechaPedido':
                $negocio->fechaPedido = $request->value;
                break;
            case 'fechaVencimientoPedido':
                $negocio->fechaVencimientoPedido = $request->value;
                break;      
            case 'fechaEntrega':
                $negocio->fechaEntrega = $request->value;
                break;
            case 'fechaReporte':
                $negocio->fechaReporte = $request->value;
                break;
            case 'xCambio':
                $negocio->xCambio = $request->value;
                break;
            case 'fechaGanado':
                $negocio->fechaGanadoPerdido = $request->value;
                $negocio->status = 1;
                $campo2 = 'Ganado';
                $this->registroCambios('Estado', $campo1, $campo2, $negocio->idNegocio);
                break;
            case 'reabrirNegocio':
                $negocio->fechaGanadoPerdido = null;
                $negocio->status = 0;
                $campo2 = 'Abierto';
                $negocio->idCausa = 1;
                $this->registroCambios('Estado', $campo1, $campo2, $negocio->idNegocio);
                break;
            default:
                break;
        }
        $saved = $negocio->save();
        $data = [];
        $data['success'] = $saved;
        $data['negocio'] = $negocio;
        return $data;
    }

    public function registroCambios($accion, $campo1, $campo2, $idNegocio){
        date_default_timezone_set("America/Mexico_City");
        $cambio = new RegistroCambios;
        $cambio->accion = $accion;
        $cambio->campo1 = $campo1;
        $cambio->campo2 = $campo2;
        $cambio->idNegocio = $idNegocio;
        $cambio->fechaRegistroCambio = date("Y-m-d H:i:s");
        $cambio->idUsuario = auth()->user()->idUsuario;
        $cambio->save();
    }

    public function store(Request $request){
        date_default_timezone_set("America/Mexico_City");
        $fecha_actual= date("Y-m-d H:i:s");
        $negocio = new Negocio();       
        $negocio->idUsuario = 1;
        $negocio->idEtapa = $request->etapa_id;
        $negocio->idCliente = $request->cliente_id;
        $negocio->idContacto = $request->contacto_id;
        $negocio->idEmbudo = $request->embudo_id;
        $negocio->idEquipo = $request->equipo_id;
        $negocio->tituloNegocio = $request->titulo_negocio;
        $negocio->claveCliente = $request->clave_cliente;
        $negocio->claveConsecutivo = $request->clave_consecutivo;
        $negocio->claveServicio = $request->clave_servicio;
        $negocio->valorNegocio = $request->valor_negocio;
        $negocio->tipoMoneda = $request->tipo_moneda;
        $negocio->fechaGanadoPerdido = null;
        $negocio->fechaCierre = $request->fecha_cierre;
        $negocio->contadorActivo = $fecha_actual;
        $negocio->status = 0;
        $negocio->ponderacion = $request->ponderacion;
        $negocio->xCambio = $request->cambio_x;
        $negocio->estimacion = 0;
        $negocio->idUnidadNegocio = $request->unidad_negocio_id;
        $negocio->claveNegocio = $request->clave_negocio;
        $negocio->claveNegocioCompleta = $request->clave_negocio_completa;
        $negocio->idServicio = $request->id_servicio;
        $negocio->precioNeto = null;
        $negocio->rfq = null;
        $negocio->fechaRfq = null;
        $negocio->idCausa = 1;
        $negocio->pedido = null;
        $negocio->fechaPedido = null;
        $negocio->fechaVencimientoPedido = null;
        $negocio->fechaEntrega = null;
        $negocio->fechaReporte = null;

        $saved = $negocio->save();

        $request->idNegocio = $negocio->idNegocio;

        if(isset($request->idUsuarioComentario))
            $this->sendNotificationNegocioNotaLead($request);

        $data = [];
        $data['success'] = $saved;
        $data['negocio'] = Negocio::find($negocio->idNegocio);      
        return $data;
    }

    public function sendNotificationNegocioNotaLead($request){
        $authUser = auth()->user();
        $notification = [];
        $userName = explode(" " , $authUser->nombreUsuario);
        $notification['text'] = $userName[0] . ' ' . $userName[1] . ' registró el comentario ' . $request->tituloComentario . ' como Negocio Lead';
        $notification['picture'] = '/img/profiles/' . $authUser->foto;
        $notification['route'] = '/ventas/negocios/'. $request->idNegocio;

        $user = User::find($request->idUsuarioComentario);

        if($authUser != $user){
            $notification = Notification::send($user, new SendNotification($notification));
            event(new NotificationToUser($user->idUsuario));
        }
    }

    public function store_estimacion(Request $request){        
        $oldNegocio = Negocio::find($request->old_negocio_id);
        // $this->update_valor($oldNegocio->idNegocio, $oldNegocio->valorNegocio, $request->valor_negocio);
        $negocio = new Negocio();      
        $negocio->idUsuario = $oldNegocio->idUsuario; 
        $negocio->idEtapa = $request->etapa_id;  
        $negocio->idCliente = $oldNegocio->idCliente;
        $negocio->idContacto = $request->contacto_id;  
        $negocio->idEmbudo = $oldNegocio->idEmbudo;
        $negocio->idEquipo = $request->equipo_id;  
        $negocio->tituloNegocio = $request->titulo_negocio;  
        $negocio->claveCliente = $oldNegocio->claveCliente;
        $negocio->claveConsecutivo = $oldNegocio->claveConsecutivo;
        $negocio->claveServicio = $oldNegocio->claveServicio;
        $negocio->valorNegocio = $request->valor_negocio;  
        $negocio->tipoMoneda = $oldNegocio->tipoMoneda;
        $negocio->fechaGanadoPerdido = $oldNegocio->fechaGanadoPerdido;
        $negocio->fechaCierre = $request->fecha_cierre;  
        $negocio->contadorActivo = $oldNegocio->contadorActivo;
        $negocio->status = $oldNegocio->status;
        $negocio->ponderacion = $oldNegocio->ponderacion;
        $negocio->xCambio = $oldNegocio->xCambio;
        $negocio->estimacion = $request->old_negocio_id;  
        $negocio->idUnidadNegocio = $oldNegocio->idUnidadNegocio;
        $negocio->claveNegocio = $request->clave_negocio;
        $negocio->claveNegocioCompleta = $request->clave_negocio_completa;
        $negocio->idServicio = $oldNegocio->idServicio;
        $negocio->precioNeto = $oldNegocio->precioNeto;
        $negocio->rfq = $oldNegocio->rfq;
        $negocio->fechaRfq = $oldNegocio->fechaRfq;
        $negocio->idCausa = $oldNegocio->idCausa;
        $negocio->pedido = $oldNegocio->pedido;
        $negocio->fechaPedido = $oldNegocio->fechaPedido;
        $negocio->fechaVencimientoPedido = $oldNegocio->fechaVencimientoPedido;
        $negocio->fechaEntrega = $oldNegocio->fechaEntrega;
        $negocio->fechaReporte = $oldNegocio->fechaReporte;
        $saved = $negocio->save();
        $data = [];
        $data['success'] = $saved;
        $data['estimacion'] = $negocio;
        return $data;
    }

    public function update_valor($oldIdNegocio, $valNegocio, $valEstimacion){
        $negocio = Negocio::find($oldIdNegocio);
        $negocio->idNegocio = $oldIdNegocio;
        $negocio->valorNegocio = ($valNegocio - $valEstimacion);
        $saved = $negocio->save();
        $data = [];
        $data['success'] = $saved;
        return $data;
    }

    public function update_clave(Request $request){
        $negocio = Negocio::find($request->negocio_id);
        $negocio->claveConsecutivo = $request->clave_consecutivo;
        $negocio->claveServicio = $request->clave_servicio;
        $negocio->idUnidadNegocio = $request->unidad_negocio_id;
        $negocio->idEquipo = $request->equipo_id;
        $negocio->claveNegocio = $request->clave_negocio;
        $negocio->claveNegocioCompleta = $request->clave_negocio_completa;
        $negocio->idServicio = $request->id_servicio;
        $negocio->claveCliente = $request->clave_cliente;
        $negocio->idCliente = $request->id_cliente;
        $negocio->idContacto = $request->id_contacto;
        $saved = $negocio->save();
        $data = [];
        $data['success'] = $saved;
        $data['negocio'] = $negocio;
        return $data;
    }

    public function update_embudo_negocio(Negocio $negocio, Request $request){

        date_default_timezone_set("America/Mexico_City");
        $fecha_actual = date("Y-m-d H:i:s");

        $campo1 = $negocio->nombre_embudo . ' (' . $negocio->nombre_etapa . ')';

        $negocio->idEmbudo = $request->id_embudo;
        $negocio->idEtapa = $request->id_etapa;
        $negocio->contadorActivo = $fecha_actual;
        $negocio->cambioEmbudo = 1;
        $saved = $negocio->save();

        $embudo = Embudo::where('idEmbudo', $request->id_embudo)->first();
        $etapa = Etapa::where('idEtapa', $request->id_etapa)->first();

        $campo2 = $embudo->nombre . ' (' . $etapa->nombreEtapa . ')';

        $this->registroCambios('Embudo', $campo1, $campo2, $negocio->idNegocio);

        $data = [];
        $data['success'] = $saved;
        $data['negocio'] = $negocio;

        return $data;
    }

    public function getActividadesByNegocio(Negocio $negocio){
        return $negocio->actividades;
    }

    public function selectEstimaciones(Request $request){
        $estimacion = Negocio::where('estimacion', $request->idNegocio)->get();
        $data = [];
        foreach($estimacion as $key => $value){
            $data[$key] = [
                'text'  => $value->claveNegocio.' - '.$value->tituloNegocio,
                'value' => $value->idNegocio
            ];
        }
        return $data;
    }

    public function getNegociosJoin(Request $request){
        $negocios = DB::table('negocios')
            ->join('clientes', 'negocios.idCliente', '=', 'clientes.idCliente')
            ->join('contactos', 'negocios.idContacto', '=', 'contactos.idContacto')
            ->join('equipo', 'negocios.idEquipo', '=', 'equipo.idEquipo')
            ->join('unidad_negocio', 'negocios.idUnidadNegocio', '=', 'unidad_negocio.idUnidadNegocio')
            ->join('causaperdida', 'negocios.idCausa', '=', 'causaperdida.idcausasPerdida')
            ->join('vt_etapas', 'negocios.idEtapa', '=', 'vt_etapas.idEtapa')
            ->join('costos', 'negocios.idNegocio', '=', 'costos.idNegocio')
            ->join('cotizaciones', 'negocios.idNegocio', '=', 'cotizaciones.idNegocio')
            ->join('facturas', 'negocios.idNegocio', '=', 'facturas.idNegocio')
            ->join('pagos', 'negocios.idNegocio', '=', 'pagos.idNegocio')
            ->select(
                'negocios.*', 
                'clientes.nombre as nombre_cliente', 
                'contactos.nombre as nombre_contacto', 
                'equipo.nombreEquipo as nombre_equipo', 
                'unidad_negocio.claveUnidadNegocio as clave_unidad_negocio', 
                'vt_etapas.nombreEtapa as nombre_etapa', 
                'costos.montoTotal as monto_total', 
                'costos.fechaSolicitud as fecha_solicitud', 
                'costos.fechaEntrega as fecha_entrega', 
                'costos.idCosto as id_costo', 
                'cotizaciones.idCotizacion as id_cotizacion',
                'cotizaciones.fechaCotizacion as fecha_cotizacion',
                'cotizaciones.claveCotizacion as clave_cotizacion',    
                'causaperdida.nombreCausa as nombre_causa',
                'facturas.idFactura as id_factura',
                'facturas.noFactura as no_factura',
                'facturas.fechaFactura as fecha_factura',
                'pagos.idPago as id_pago',
                'pagos.fechaPago as fecha_pago',
                'pagos.pagoMXNiva as pago_mxn_iva',
                'pagos.pagoUSDiva as pago_usd_iva',
                'pagos.noPago as no_pago',
                'pagos.fechaRecibo as fecha_recibo')

            ->where('negocios.idEmbudo', $request->idEmbudo)
            ->orderBy('negocios.idNegocio', 'desc')->get();

        return $negocios;
    }

    public function getBitacoras(Negocio $negocio){
        $bitacoras = DB::table('bitacoras_negocios')->where('idNegocio', $negocio->idNegocio)->get();
        $arrayLineaTiempo = [];
        foreach ($bitacoras as $bitacora) {
             if($bitacora->tabla == 'negocios'){
                $row_array['bitacora'] = 'negocio';
                $row_array['timestamp']=$bitacora->timestamp;
                $row_array['nombre_usuario']=$negocio->nombre_usuario;
                $row_array['claveNegocioCompleta']=$negocio->claveNegocioCompleta;

                array_push($arrayLineaTiempo, $row_array);
            } 
            if($bitacora->tabla == 'actividades'){
                $actividad = DB::table('actividades')
                            ->join('usuarios', 'actividades.idUsuario', 'usuarios.idUsuario')
                            ->join('clientes', 'actividades.idCliente', 'clientes.idCliente')
                            ->join('negocios', 'actividades.idNegocio', 'negocios.idNegocio')
                            ->select('actividades.*',
                                     'usuarios.nombreUsuario as nombre_usuario', 
                                     'clientes.nombre as nombre_cliente',
                                     'negocios.tituloNegocio as titulo_negocio')
                              ->where('actividades.idActividad', $bitacora->idRelacion)
                              ->first();
                if($actividad != null){
                    $row_array['bitacora'] = 'actividad';
                    $row_array['idActividad'] = $actividad->idActividad;
                    $row_array['fechaCompletado'] = $actividad->fechaCompletado;
                    $row_array['fechaActividad'] = $actividad->fechaActividad;
                    $row_array['tipo'] = $actividad->tipo;
                    $row_array['notas'] = $actividad->notas;
                    $row_array['duracion'] = $actividad->duracion;
                    $row_array['completado'] = $actividad->completado;
                    $row_array['comentario'] = $actividad->comentario;
                    $row_array['idCliente']=$actividad->idCliente;
                    $row_array['nombrePersona']=$actividad->nombrePersona;
                    $row_array['timestamp']=$bitacora->timestamp;
                    $row_array['nombre_cliente']=$actividad->nombre_cliente;
                    $row_array['nombre_usuario']=$actividad->nombre_usuario;
                    $row_array['tituloNegocio']=$actividad->titulo_negocio;
                    array_push($arrayLineaTiempo, $row_array);
                }
            } 
             if($bitacora->tabla == 'registro_cambios'){
                $cambio = DB::table('registro_cambios')
                             ->join('usuarios', 'registro_cambios.idUsuario', '=', 'usuarios.idUsuario')
                             ->select('registro_cambios.*',
                                     'usuarios.nombreUsuario as nombre_usuario')
                              ->where('registro_cambios.idRegistro', $bitacora->idRelacion)
                              ->first();
                if($cambio != null){
                    $row_array['bitacora'] = 'cambio';
                    $row_array['idRegistro'] = $cambio->idRegistro;
                    $row_array['accion'] = $cambio->accion;
                    $row_array['campo1'] = $cambio->campo1;
                    $row_array['campo2'] = $cambio->campo2;
                    $row_array['nombre_usuario']=$cambio->nombre_usuario;
                    $row_array['timestamp']=$bitacora->timestamp;
                    array_push($arrayLineaTiempo, $row_array);
                }
            } 
            if($bitacora->tabla == 'contenidos'){
                $contenido = DB::table('contenidos')
                             ->join('usuarios', 'contenidos.idUsuario', '=', 'usuarios.idUsuario')
                             ->select('contenidos.*',
                                     'usuarios.nombreUsuario as nombre_usuario')
                              ->where('contenidos.idContenido', $bitacora->idRelacion)
                              ->first();
                if($contenido != null){
                    $row_array['bitacora'] = 'contenido';
                    $row_array['idContenido'] = $contenido->idContenido;
                    $row_array['notas'] = $contenido->notas;
                    $row_array['file'] = $contenido->archivos;
                    $row_array['nombre_usuario']=$contenido->nombre_usuario;
                    $row_array['timestamp']=$bitacora->timestamp;
                    array_push($arrayLineaTiempo, $row_array);
                }
            } 
        }
        return $arrayLineaTiempo;
    }

    public function getNegociosServicios(){
        return DB::table('negocios')
                    ->join('unidad_negocio', 'negocios.idUnidadNegocio', '=', 'unidad_negocio.idUnidadNegocio')
                    ->join('equipo', 'negocios.idEquipo', '=', 'equipo.idEquipo')
                    ->select('negocios.*', 
                        'unidad_negocio.claveUnidadNegocio as clave_unidad', 
                        'equipo.clave as clave_equipo'
                        )
                    ->whereRaw('LENGTH(claveNegocioCompleta) = 12')
                    ->where('idEmbudo', 4)
                    ->get();
    }

    public function setClaves(Request $request){
        $negocio = Negocio::find($request->id_negocio);
        $negocio->claveServicio = $request->clave_servicio;
        $negocio->claveNegocioCompleta = $request->clave_negocio_completa;
        $negocio->claveNegocio = $request->clave_negocio;
        $negocio->idServicio = $request->id_servicio;
        $saved = $negocio->save();
        $data = [];
        $data['success'] = $saved;
        $data['negocio'] = $negocio;
        return $data;
    }

    public function update_moneda(Request $request){
        $negocio = Negocio::find($request->id);
        $negocio->tipoMoneda = $request->tipoMoneda;
        $negocio->xCambio = $request->xCambio;
        $negocio->valorNegocio = $request->valorNegocio;
        $saved = $negocio->save();
        $data = [];
        $data['success'] = $saved;
        $data['negocio'] = Negocio::find($request->id);
        return $data; 
    }

    public function getNegociosByCfCuentasExternas($seccion, $embudo) {
        $embudo = Embudo::where('periodo', $embudo)->first();
        $conf = CuentasExternas::where('idEmbudo', $embudo->idEmbudo)->first();

        if($seccion == 'fn.cg')
            $etapas = $conf->fn_cg_etapas;
        else if ($seccion == 'op.ra')
            $etapas = $conf->op_ra_etapas;
        else if ($seccion == 'se.sa')
            $etapas = $conf->se_sa_etapas;
        else if ($seccion == 'co.ss')
            $etapas = $conf->co_ss_etapas;
        else if ($seccion == 'fn.cg.sv')
            $etapas = $conf->fn_cg_sv_etapas;

        $negocios = DB::table('negocios')->select('tituloNegocio', 'claveNegocioCompleta', 'idEtapa')->where('idEmbudo', $embudo->idEmbudo)->get();

        $negociosCollect = collect();

        foreach (json_decode($etapas) as $etapa) {
            $negByEtapa = $negocios->where('idEtapa', $etapa->id);
            $negociosCollect = $negociosCollect->concat($negByEtapa);
        }

        return $negociosCollect;
    }

    public function createReportExcel(Request $request)
    {   
        $filename = "VT/NG/Reporte negocios.xlsx";
        $negocios = json_decode($request->data); 
        $saved = Excel::store(new NegociosExport($negocios), $filename);
        $fullPath = Storage::disk('local')->path($filename);        
        $data = [];
        $data["success"] = $saved;
        $data["filename"] = $filename;
        return $data;
    }

    public function exportReportExcel(Request $request)
    {
        return Storage::download($request->filename);
    }
}
