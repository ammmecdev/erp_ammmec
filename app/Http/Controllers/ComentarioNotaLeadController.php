<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ComentarioNotaLead;
use App\NotaLead;
use App\User;
use Notification;
use App\Notifications\SendNotification;
use App\Events\NotificationToUser;
use Illuminate\Support\Facades\DB;

class ComentarioNotaLeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    //
    public function store(Request $request){
        $comentario=new ComentarioNotaLead;
        $comentario->contenido = $request->contenido;
        $comentario->tituloNegocio = $request->tituloNegocio;
        $comentario->responsable = $request->responsable;
        $comentario->idNota = $request->idNota;
        $comentario->lead = $request->lead;
        $comentario->idUsuario = $request->idUsuario;
        $comentario->negocio = 0;

        if($request->file){
            $file = $request->file('file');
            $nombre = $file->getClientOriginalName();
            \Storage::disk('local')->put($nombre,  \File::get($file));   
            $comentario->file = $nombre;
        }
        
        $saved=$comentario->save();
       
        $nota = $comentario->nota;
        
        if($request->idResponsable != "null")
            $this->sendNotificationResponsable($nota, $request->idResponsable);

        $comentario = ComentarioNotaLead::find($comentario->idComentario);
        
        $data=[];
        $data['success']=$saved;
         
        $data['comentario'] = [
            'idComentario' => $comentario->idComentario,
            'contenido' => $comentario->contenido,
            'tituloNegocio' => $comentario->tituloNegocio,
            'responsable' => $comentario->responsable,
            'idNota' => $comentario->idNota,
            'lead' => $comentario->lead,
            'nombre_usuario' => $comentario->nombre_usuario,
            'foto_usuario' => $comentario->foto_usuario,
            'nombre_cliente' => $nota->nombre_cliente,
            'nombre_contacto' => $nota->nombre_contacto,
            'idCliente' => $nota->idCliente,
            'clave_cliente' => $nota->clave_cliente,
            'idContacto' => $nota->idContacto,
            'fechaCreado' => $comentario->fechaCreado,
            'fechaActualizado' => $comentario->fechaActualizado,
            'idUsuario' => $comentario->idUsuario,
            'file' => $comentario->file,
            'negocio' => $comentario->negocio
        ];

        return $data;
    }

    public function sendNotificationResponsable($nota, $idResponsable){
        $authUser = auth()->user();
        $notification = [];
        $userName = explode(" " , $authUser->nombreUsuario);
        $notification['text'] = $userName[0] . ' ' . $userName[1] . ' te asignó como responsable de un comentario en la nota ' . $nota->titulo;
        $notification['picture'] = '/img/profiles/' . $authUser->foto;
        $notification['route'] = '/ventas/negocios/notas/'. $nota->idNota;

        $user = User::find($idResponsable);

        if($authUser != $user){
            $notification = Notification::send($user, new SendNotification($notification));
            event(new NotificationToUser($user->idUsuario));
        }
    }

    public function updateComentario(Request $request){
        $comentario = ComentarioNotaLead::find($request->idComentario);
        $comentario->contenido = $request->contenido;
        $comentario->tituloNegocio = $request->tituloNegocio;
        $comentario->responsable = $request->responsable;
        $comentario->lead = $request->lead;
        $comentario->idUsuario = $request->idUsuario;
        $comentario->negocio = $request->negocio;
        $updated = $comentario->update();
        $data = [
            'success' => $updated,
            'comentario' => $comentario
        ];
        return $data;
 
    }

    public function updateComentarioNegocio(Request $request){
        $comentario = ComentarioNotaLead::find($request->idComentario);
        $comentario->negocio = $request->idNegocio;
        $updated = $comentario->update();
        $data = [
            'success' => $updated,
            'comentario' => $comentario
        ];
        return $data;
    }

    public function deleteComentario(ComentarioNotaLead $comentario){
        $deleted = $comentario->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function downloadFile($archivo){
        
        $public_path = public_path();
        $url = $public_path.'/storage/'.$archivo;
        //verificamos si el archivo existe y lo retornamos
        if (\Storage::exists($archivo))
        {
        return response()->download($url);
        }
        //si no se encuentra lanzamos un error 404.
        abort(404);
    }

    public function getActualizaciones(ComentarioNotaLead $comentario){
        return $comentario->actualizaciones;
    }
    
}
