<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UnidadNegocio;

class UnidadNegocioController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getUnidadesNegocio() 
    {
        // return UnidadNegocio::where('claveUnidadNegocio', '!=', 'S/UN')->get();
        return UnidadNegocio::get();
    }

    public function store(Request $request)
    {
        $unidadNegocio = new UnidadNegocio();
        $unidadNegocio->unidadNegocio = $request->unidadNegocio;
        $unidadNegocio->claveUnidadNegocio = $request->claveUnidadNegocio;
        $saved = $unidadNegocio->save();

        $data=[];
        $data['success'] = $saved;
        $data['unidadNegocio'] = $unidadNegocio;
        return $data;
    }

    public function update(UnidadNegocio $unidadNegocio, Request $request)
    {
        $unidadNegocio->unidadNegocio = $request->unidadNegocio;
        $unidadNegocio->claveUnidadNegocio = $request->claveUnidadNegocio;
        $saved = $unidadNegocio->save();

        $data = [];
        $data['success'] = $saved;
        $data['unidadNegocio'] = $unidadNegocio;
        return $data;
    }

    public function selectUnidad()
    {
    	$unidadNegocio = UnidadNegocio::get();
    	$data = [];
    
    	foreach($unidadNegocio as $key => $value){
    		$data[$key] = [
                'text'  => "(" . $value->claveUnidadNegocio . ") - " . $value->unidadNegocio,
    			'value' =>  [
                                'text' => $value->claveUnidadNegocio,
                                'value' => $value->idUnidadNegocio
                            ]
    		];
    	}
    	return response()->json($data);
    }
}
