<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empleado;

class EmpleadoController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

   	public function getEmpleadoByNumeroEmpleado($noEmpleado)
   	{
		
   		$empleados = Empleado::where('noEmpleado', 'like' ,'%'.$noEmpleado.'%')->get();

		foreach($empleados as $empleado) {
			if (isset($empleado)) {
				$noEmpleadoCompleto = explode('-', $empleado->noEmpleado, 2);

				$empleado->consecutivo = $noEmpleadoCompleto[0];

				if ($empleado->consecutivo == $noEmpleado) {
					return $empleado;
				}
			}
		}
   	}

    public function getEmpleados(){
      return Empleado::all();
    }
}
