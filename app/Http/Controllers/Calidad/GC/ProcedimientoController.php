<?php

namespace App\Http\Controllers\Calidad\GC;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Calidad\GC\Procedimiento;

class ProcedimientoController extends Controller
{
   	public function store(Request $request)
    {
        $procedimiento = new Procedimiento();       
        $procedimiento->nombre = $request->nombre;
        $procedimiento->idProceso = $request->idProceso;
        $saved = $procedimiento->save();
    
        $data = [];
        $data['success'] = $saved;
        $data['procedimiento'] = $procedimiento;     
        return $data;
    }

    public function update(Request $request)
    {
    	$procedimiento = Procedimiento::find($request->idProcedimiento);
        $procedimiento->nombre = $request->nombre;
        $procedimiento->idProceso = $request->idProceso;
        $saved = $procedimiento->save();

        $data = [];
        $data['success'] = $saved;
        $data['procedimiento'] = $procedimiento;     
        return $data;
    }

    public function delete(Procedimiento $procedimiento)
    {
        $deleted = $procedimiento->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

     public function getArchivosByProcedimiento($idProcedimiento)
    {
        return DB::table('nr_gc_archivos_procedimientos')
            ->join('nr_gc_procedimientos', 'nr_gc_archivos_procedimientos.idProcedimiento', '=', 'nr_gc_procedimientos.idProcedimiento')
            ->join('nr_gc_procesos', 'nr_gc_procedimientos.idProceso', '=', 'nr_gc_procesos.idProceso')
            ->join('rh_areas', 'nr_gc_procesos.idArea', '=', 'rh_areas.idArea')
            ->select('nr_gc_archivos_procedimientos.*')
            ->where('nr_gc_procedimientos.idProcedimiento', $idProcedimiento)
            ->get();
    }
}
