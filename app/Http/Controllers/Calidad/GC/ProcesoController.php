<?php

namespace App\Http\Controllers\Calidad\GC;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Calidad\GC\Proceso;

class ProcesoController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']); 
    }

    public function store(Request $request)
    {
        $proceso = new Proceso();       
        $proceso->nombre = $request->nombre;
        $proceso->clave = $request->clave;
        $proceso->version = $request->version;
        $proceso->codigo = $request->codigo;
        $proceso->idArea = $request->idArea;
        $saved = $proceso->save();
        $proceso->procedimientos = [];
        $data = [];
        $data['success'] = $saved;
        $data['proceso'] = $proceso;     
        return $data;
    }

    public function update(Request $request)
    {
    	$proceso = Proceso::find($request->idProceso);
        $proceso->nombre = $request->nombre;
        $proceso->clave = $request->clave;
        $proceso->version = $request->version;
        $proceso->codigo = $request->codigo;
        $proceso->idArea = $request->idArea;
        $saved = $proceso->save();
        $data = [];
        $data['success'] = $saved;
        $data['proceso'] = $proceso;     
        return $data;
    }

    public function delete(Proceso $proceso)
    {
        $deleted = $proceso->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function selectProcesos($idArea)
    {
        $procesos = Proceso::where('idArea', $idArea)->get();
        $data = [];
        foreach($procesos as $key => $value){
            $data[$key] = [
                'text'  => $value->nombre,
                'value' => $value->idProceso                    
            ];
        }
        return response()->json($data); 
    }

}
