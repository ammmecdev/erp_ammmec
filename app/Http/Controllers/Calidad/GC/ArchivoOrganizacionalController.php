<?php

namespace App\Http\Controllers\Calidad\GC;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Calidad\GC\ArchivoProcedimiento;
use App\Models\Calidad\GC\ArchivoOrganizacional;

class ArchivoOrganizacionalController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getArchivosByTipo($tipo)
    {
    	return ArchivoOrganizacional::where('tipo', $tipo)->get();
    }

	public function store(Request $request)
	{
		$archivo = new ArchivoOrganizacional;

        $ruta = $this->getRuta($request);

        $path = $request->file('file')->storeAs($ruta, request()->file->getClientOriginalName());

        $archivo->nombre = $request->file->getClientOriginalName();
        $archivo->tipo = $request->tipo;
        // $archivo->clave = $request->clave;
        // $archivo->version = $request->version;
        // $archivo->codigo = $request->codigo;
        $archivo->size = $request->file->getSize();
        $archivo->formato = $request->file->extension();
        $archivo->ruta = $ruta;

        $saved = $archivo->save();

        $data = [];
        $data['success'] = $saved;
        $data['archivo'] = $archivo;

        return $data;
	}

    public function update(Request $request)
    {
        $archivo = ArchivoOrganizacional::find($request->idArchivo);

        if($request->file('file')){
            
            $archivo_old = ArchivoOrganizacional::find($request->idArchivo);
            Storage::delete($archivo_old->ruta."/".$archivo_old->nombre);

            $ruta = $this->getRuta($request);

            $path = $request->file('file')->storeAs($ruta, request()->file->getClientOriginalName());

            $archivo->nombre = $request->file->getClientOriginalName();
            $archivo->ruta = $ruta;
            $archivo->size = $request->file->getSize();
            $archivo->formato = $request->file->extension();
        }

        $archivo->tipo = $request->tipo;
        $archivo->clave = $request->clave;
        $archivo->version = $request->version;
        $archivo->codigo = $request->codigo;
        $archivo->ultima_modificacion = $request->ultimaModificacion;
  
        $saved = $archivo->save();

        $data = [];
        $data['success'] = $saved;
        $data['archivo'] = $archivo;

        return $data;
    }

    public function getRuta($request)
    {
        if($request->tipo == 'filosofía')
            return 'NR/Files/Org/Filosofía Empresarial';
        else if($request->tipo == 'reglamento')
            return 'NR/Files/Org/Reglamento Interno';
        else if($request->tipo == 'politicas')
            return 'NR/Files/Org/Políticas';
        else if($request->tipo == 'imagen')
            return 'NR/Files/Org/Imagen Corporativa';
    }

    public function download($idArchivo)
    {
        $file = ArchivoOrganizacional::find($idArchivo);
        return Storage::download($file->ruta."/".$file->nombre);
    }

    public function show($idArchivo)
    {
        $file = ArchivoProcedimiento::find($idArchivo);
        return Storage::response($file->ruta."/".$file->nombre);
    }
}
