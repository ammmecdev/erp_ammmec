<?php

namespace App\Http\Controllers\Calidad\GC;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Calidad\GC\ArchivoProcedimiento;

class ArchivoProcedimientoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getArchivosByDepartment(Request $request)
    {
       return ArchivoProcedimiento::all();
    }

    public function searchArchivos(Request $request)
	{
		return DB::table('nr_gc_archivos_procedimientos')
		 	->join('nr_gc_procedimientos', 'nr_gc_archivos_procedimientos.idProcedimiento', '=', 'nr_gc_procedimientos.idProcedimiento')
		 	->join('nr_gc_procesos', 'nr_gc_procedimientos.idProceso', '=', 'nr_gc_procesos.idProceso')
		 	->join('rh_areas', 'nr_gc_procesos.idArea', '=', 'rh_areas.idArea')
	        ->select('nr_gc_archivos_procedimientos.nombre as nombreArchivo', 'nr_gc_procedimientos.nombre as nombreProcedimiento', 'nr_gc_procesos.nombre as nombreProceso', 'rh_areas.nombre as nombreArea', 'rh_areas.codigo as codigoArea')
	        ->where('nr_gc_archivos_procedimientos.nombre', 'like', '%' . $request->search . '%')
	        ->paginate(15); 
	}

	public function store(Request $request)
	{
		$archivo = new ArchivoProcedimiento;

        $ruta = $this->getRuta($request);

        $path = $request->file('file')->storeAs($ruta, request()->file->getClientOriginalName());

        $archivo->nombre = $request->file->getClientOriginalName();
        $archivo->tipo = $request->tipo;
        $archivo->clave = $request->clave;
        $archivo->version = $request->version;
        $archivo->codigo = $request->codigo;
        $archivo->ultima_modificacion = $request->ultimaModificacion;
        $archivo->idProcedimiento = $request->idProcedimiento;
        $archivo->size = $request->file->getSize();
        $archivo->formato = $request->file->extension();
        $archivo->ruta = $ruta;

        $saved = $archivo->save();

        $data = [];
        $data['success'] = $saved;
        $data['archivo'] = $archivo;

        return $data;
	}

    public function update(Request $request)
    {
        $archivo = ArchivoProcedimiento::find($request->idArchivo);

        if($request->file('file')){
            
            $archivo_old = ArchivoProcedimiento::find($request->idArchivo);
            Storage::delete($archivo_old->ruta."/".$archivo_old->nombre);

            $ruta = $this->getRuta($request);

            $path = $request->file('file')->storeAs($ruta, request()->file->getClientOriginalName());

            $archivo->nombre = $request->file->getClientOriginalName();
            $archivo->ruta = $ruta;
            $archivo->size = $request->file->getSize();
            $archivo->formato = $request->file->extension();
        }

        $archivo->tipo = $request->tipo;
        $archivo->clave = $request->clave;
        $archivo->version = $request->version;
        $archivo->codigo = $request->codigo;
        $archivo->ultima_modificacion = $request->ultimaModificacion;
        $archivo->idProcedimiento = $request->idProcedimiento;

        $saved = $archivo->save();

        $data = [];
        $data['success'] = $saved;
        $data['archivo'] = $archivo;

        return $data;
    }

    public function getRuta($request)
    {
        if($request->tipo == 'P')
            return 'NR/Files/Areas/'.$request->codigoArea.'/Procedimientos';
        else if($request->tipo == 'IT')
            return 'NR/Files/Areas/'.$request->codigoArea.'/IT';
        else if($request->tipo == 'F')
            return 'NR/Files/Areas/'.$request->codigoArea.'/Formatos';
    }

    public function download($idArchivo)
    {
        $file = ArchivoProcedimiento::find($idArchivo);
        return Storage::download($file->ruta."/".$file->nombre);
    }

    public function show($idArchivo)
    {
        $file = ArchivoProcedimiento::find($idArchivo);
        return Storage::response($file->ruta."/".$file->nombre);
    }

}
