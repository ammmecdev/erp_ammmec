<?php

namespace App\Http\Controllers\Calidad\GC;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Calidad\GC\ManualDepartamental;

class ManualDepartamentalController extends Controller
{
    public function store(Request $request)
    {
        $manual = new ManualDepartamental;

        $ruta = 'NR/Files/Areas/'.$request->codigoArea.'/Manual';

        if($request->file('file')){
            $path = $request->file('file')->storeAs($ruta, request()->file->getClientOriginalName());
        }

        $manual->nombre = request()->file->getClientOriginalName();
        $manual->clave = $request->clave;
        $manual->version = $request->version;
        $manual->codigo = $request->codigo;
        $manual->idArea = $request->idArea;
        $manual->ruta = $ruta;
        $manual->ultima_modificacion = $request->ultimaModificacion;

        $saved = $manual->save();

        $data = [];
        $data['success'] = $saved;
        $data['manual'] = $manual;
        return $data;
    }

    public function show($idManual)
    {
        $manual = ManualDepartamental::find($idManual);
        return Storage::response($manual->ruta . "/" . $manual->nombre);
    }

    public function update(Request $request)
    {
        $manual = ManualDepartamental::find($request->idManual);

        if($request->file('file')){
            $ruta = 'NR/Files/Areas/'.$request->codigoArea.'/Manual';

            $manual_old = ManualDepartamental::find($request->idManual);
            Storage::delete($ruta."/".$manual_old->nombre);

            $path = $request->file('file')->storeAs($ruta, request()->file->getClientOriginalName());

            $manual->nombre = $request->file->getClientOriginalName();
            $manual->ruta = $ruta;

        }

        $manual->clave = $request->clave;
        $manual->version = $request->version;
        $manual->codigo = $request->codigo;
        $manual->ultima_modificacion = $request->ultimaModificacion;

        $saved = $manual->save();

        $data = [];
        $data['success'] = $saved;
        $data['manual'] = $manual;
        return $data;
    }
}
