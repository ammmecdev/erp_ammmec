<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Servicio;

class ServicioController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function selectServicios()
    {
    	return $servicios = Servicio::get();
    }

    public function servicioById(Servicio $servicio)
    {
        return $servicio;
    }
}
