<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pregunta;

class PreguntaController extends Controller
{
   public function __construct()
   {
       $this->middleware('guest', ['only' => 'showLoginForm']);
   }
   public function getPreguntas()
   {
   		return Pregunta::all();
   }
}
