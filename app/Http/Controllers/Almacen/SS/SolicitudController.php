<?php

namespace App\Http\Controllers\Almacen\SS;

use App;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

use App\Models\Almacen\Inventarios\Activo;
use App\Models\Almacen\SS\SolicitudSuministro;
use App\Models\Almacen\SS\Etapa;
use App\Models\Almacen\SS\Articulo;
use App\Models\Almacen\SS\SolicitudCompra;

use App\Notifications\Compras\SS\NewSolicitud;
use App\Notifications\Compras\SS\SolicitudOrdenCompra;

class SolicitudController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $solicitud = new SolicitudSuministro;
        $solicitud->idEtapa = $request->idEtapa;
        $solicitud->idUsuario = $request->idUsuario;
        $solicitud->fechaSolicitud = $request->fechaSolicitud;
        $solicitud->fechaEntrega = $request->fechaEntrega;
        $solicitud->unidadNegocio = $request->unidadNegocio;
        $solicitud->tipoPedido = $request->tipoPedido;
        $solicitud->tipoCuenta = $request->tipoCuenta;
        $solicitud->cuenta = $request->cuenta;
        $solicitud->observaciones = $request->observaciones;

        if ($request->file('file')) {
            $file = $request->file('file');
            $path = $file->storeAs("ALM/SS/Comprobantes", $file->getClientOriginalName());
            $solicitud->routeFile = "/storage/".$path;
            $solicitud->file = $file->getClientOriginalName();
        }else {
            $solicitud->file = "S/A";
        }

        $saved = $solicitud->save();
        $solicitud->usuario = $solicitud->usuario;
        $notifyOC = false;

        if ($saved) { 
            foreach (json_decode($request->articulos, true) as $item) {
                $articulo = new Articulo;
                $articulo->cantidad = $item['cantidad'];
                $articulo->idSolicitud = $solicitud->idSolicitud;
                $articulo->unidad = $item['unidad'];
                $articulo->tipo = $item['tipo'];
                $articulo->descripcion = $item['descripcion'];
                $articulo->salida = false;
                $articulo->descuento = 0;
                $articulo->idActivo = $item['idActivo'];
                $savedArticulo = $articulo->save();
            }
        }
        if ($saved) {
            $notifiable = User::find(22); // 22 wili Notificar almacén
            $notifiable->notify(new NewSolicitud($solicitud)); 
        }
        $data = [];
        $data['success'] = $saved;
        $data['solicitud'] = $solicitud;
        return $data;
    }

    public function getSolicitudesAll()
    {
        // TODAS
        $solicitudes = SolicitudSuministro::all();
        foreach ($solicitudes as $solicitud) 
            $solicitud->usuario = $solicitud->usuario;
        return $solicitudes;
    }

    public function getSolicitudesByUsuario($idUsuario)
    {
        // POR USUARIO
        $solicitudes = SolicitudSuministro::where('idUsuario', $idUsuario)->get();
        foreach ($solicitudes as $solicitud) 
            $solicitud->usuario = $solicitud->usuario;
        return $solicitudes;
    }

    public function getArticulos(SolicitudSuministro $solicitud)
    {
        return $solicitud->articulos;
    }

    public function getEtapas() {
        return Etapa::all();
    }

    public function solicitarCompra(Request $request)
    {
        // CAMBIOS SOLICITUD DE SUMINISTROS
        $solicitudSuministro = SolicitudSuministro::find($request->idSolicitud);
        $solicitudSuministro->idEtapa = $request->idEtapaSS;
        $solicitudSuministro->fechaSolicitudCompra = date("Y-m-d H:i:s");
        $updatedSS = $solicitudSuministro->update();

        if ($updatedSS) {
            // CAMBIOS ARTÍCULOS
            $articulos = Articulo::where("idSolicitud", $request->idSolicitud)->get();
            foreach ($articulos as $articulo) {
                if (!$articulo->salida) {
                    if ($articulo->idActivo) {
                        $activo = Activo::find($articulo->idActivo);
                        if ($activo->existencia < $articulo->cantidad) {
                            $articulo->cantidadOC = $articulo->cantidad - $activo->existencia;
                        }
                    }else {
                        $articulo->cantidadOC = $articulo->cantidad;
                    }
                    $updatedART = $articulo->update();
                }
            }

            // NUEVA SOLICITUD DE COMPRA
            $solicitudCompra = new SolicitudCompra;
            $solicitudCompra->idSolicitudSuministros = $request->idSolicitud;
            $solicitudCompra->idEtapa = $request->idEtapaSC;
            $saved = $solicitudCompra->save();
        }

        $data = [];
        $data['success'] = $updatedSS;
        $data['articulos'] = $articulos;
        $data['solicitudSuministro'] = $solicitudSuministro;
        $data['solicitudCompra'] = $solicitudCompra;
        return $data;
    }

    public function getSolicitud(SolicitudSuministro $solicitud)
    {
        $solicitud->usuario = $solicitud->usuario;
        return $solicitud;
    }

    public function generateSolicitud(Request $request) {
        $data = request();
        $idAlmacenista = 22; // Almacenista
        $almacenista = User::where('idUsuario', $idAlmacenista)->first(['nombreUsuario']);
        $solicitante = User::where('idUsuario', $data->solicitud['idUsuario'])->first(['nombreUsuario']);

        $pdf = App::make('dompdf.wrapper');
        $pdf->loadView('modulos.almacen.reportes.solicitud_suministros', [
            'articulos' => $data->articulos, 
            'solicitud' => $data->solicitud, 
            'created_at' => $data->created_at, 
            'delivery_date' => $data->delivery_date, 
            'almacenista' => $almacenista->nombreUsuario, 
            'solicitante' => $solicitante->nombreUsuario
        ]);

        Storage::put("ALM/SS/Solicitud de Suministros.pdf", $pdf->output());
        $result = [];
        $result['success'] = true; 
        return $result;
    }

    public function downloadSolicitud($name) {
       return Storage::download("ALM/SS/".$name.".pdf");
    }

    public function downloadFile($name) {
       return Storage::download("ALM/SS/Comprobantes/".$name);
    }

    public function outputArticulo(Articulo $articulo)
    {
        $articulo->salida = true;
        $updated=$articulo->update();
        if ($updated) {
            $activo = Activo::find($articulo->idActivo);
            $activo->existencia = ($activo->existencia - $articulo->cantidad);
            $updatedActivo=$activo->update();
        }
        $data=[];
        $data['success']=$updated;
        $data['articulo']=$articulo;
        return $data;
    }

    public function generateVale(Request $request) {
        $data = request();
        $idUsuario = 22; // Almacenista
        $almacenista = User::where('idUsuario', $idUsuario)->first(['nombreUsuario']);
        $pdf = App::make('dompdf.wrapper');
        
        $pdf->loadView('modulos.almacen.reportes.vale_salida', [
            'articulos' => $data->articulos, 
            'solicitud' => $data->solicitud, 
            'date' => $data->date, 
            'almacenista' => $almacenista->nombreUsuario
        ]);
        
        Storage::put("ALM/SS/Vale de Salida.pdf", $pdf->output());
        $result = [];
        $result['success'] = true; 
        return $result;
    }

    public function downloadVale($name) {
        return Storage::download("ALM/SS/".$name.".pdf");
    }

    public function cambioEtapa(SolicitudSuministro $solicitud, $idEtapa) 
    {
        $solicitud->idEtapa = $idEtapa;
        switch ($solicitud->idEtapa) {
            case '5':
                $accion = 'envió solicitud a terminado.';
                $solicitud->fechaTermino = date("Y-m-d H:i:s");
                break;
            default:
                break;
        }
        $saved = $solicitud->save();
        $data = [];
        $data['success'] = true;
        $data['solicitud'] = SolicitudSuministro::find($solicitud->idSolicitud);
        return $data;  
    }

    public function getCuentasBySolicitud($tipoCuenta) 
    {
        return DB::table('alm_ss_solicitudes')->select('cuenta')->where('tipoCuenta', $tipoCuenta)->get()->unique('cuenta')->values();
    }

    public function archivarSolicitud(Request $request)
    {
        $solicitud = SolicitudSuministro::find($request->idSolicitud);
        $solicitud->idEtapa = 1;
        $solicitud->comentarios = $request->comentarios;
        $updated = $solicitud->update();
        $data = [];
        $data['success'] = $updated;
        $data['solicitudSuministro'] = SolicitudSuministro::find($solicitud->idSolicitud);
        return $data;  
    }
}
