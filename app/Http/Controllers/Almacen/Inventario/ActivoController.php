<?php

namespace App\Http\Controllers\Almacen\Inventario;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Almacen\Inventarios\Activo;
use App\Models\Almacen\SS\Articulo;

class ActivoController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activo = new Activo;
        $activo->articulo = $request->articulo;
        $activo->unidad = $request->unidad;
        $activo->existencia = $request->existencia;
        $activo->categoria = $request->categoria;
        $activo->costoUnitario = $request->costoUnitario;
        $saved=$activo->save(); 
        $data=[];
        $data['success']=$saved;
        $data['activo']=$activo;
        return $data;
    }

    public function storeActivos(Request $request)
    {
        foreach($request->activos as $item) {
            $activo = new Activo; 
            $activo->articulo = $item['descripcion'];
            $activo->unidad = $item['unidad'];
            $activo->existencia = $item['faltantes'];
            $activo->categoria = $item['categoria'];
            $activo->costoUnitario = $item['costoUnitario'];
            $saved=$activo->save();

            if ($saved) {
                $articulo = Articulo::find($item['idArticulo']);
                $articulo->idActivo = $activo->idActivo;
                $updated=$articulo->update();                
            }
        }
        $data=[];
        $data['success']=true;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Almacen\Inventarios\Activo  $activo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Activo $activo)
    {
        $activo->articulo = $request->articulo;
        $activo->unidad = $request->unidad;
        $activo->existencia = $request->existencia;
        $activo->categoria = $request->categoria;
        $activo->costoUnitario = $request->costoUnitario;
        $updated=$activo->update();
        $data=[];
        $data['success']=$updated;
        $data['activo']=$activo;
        return $data;
    }

    public function editActivos(Request $request)
    {
        foreach($request->activos as $item) {
            $activo = Activo::find($item['idActivo']);
            $activo->existencia = $activo->existencia + $item['faltantes'];
            $activo->costoUnitario = $item['costoUnitario'];
            $updated=$activo->update();
        }
        $data=[];
        $data['success']=true;
    }


    public function updateActivo(Request $request, Activo $activo)
    {
        if ($request->campo == "existencia") {
            $activo->existencia = $request->value;
        }else if ($request->campo == "costoUnitario") {
            $activo->costoUnitario = $request->value;
        }
        $updated=$activo->update();
        $data=[];
        $data['success']=$updated;
        $data['activo']=$activo;
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Almacen\Inventarios\Activo  $activo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activo $activo)
    {
        $deleted = $activo->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getActivos()
    {
        return Activo::all();
    }

    public function getCategorias()
    {
        $activos = Activo::all();
        $activos = $activos->unique('categoria');
        $categorias = collect();
        $categoria = array();
        foreach($activos as $activo){
            if($activo->categoria != "") {
                $categoria['categoria'] = $activo->categoria;
                $categorias->push($categoria);
            }
        }
        return $categorias;
    }

    public function loadActivos(Request $request)
    {
        Activo::truncate();
        $data = json_decode($request->getContent(), true);
        foreach ($data as $item) {
            $articulo = new Activo;
            $articulo->unidad = $item['unidad'];
            $articulo->articulo = $item['articulo'];
            $articulo->existencia = $item['existencia'] == "" ? 0 : $item['existencia'];
            $articulo->costoUnitario = $item['costoUnitario'] == "" ? 0 : $item['costoUnitario'];
            $articulo->categoria = $item['categoria'];
            $saved = $articulo->save();
        }
        $result = [];
        $result['success'] = true;
        return $result;
    }

    public function selectActivosLike(Request $query)
    {
        return Activo::where('articulo', 'like', '%' . $query->searchQuery . '%')->orWhere('categoria', 'like', '%' . $query->searchQuery . '%')->get();
    }

    public function getActivo(Activo $activo)
    {
        return $activo;
    }
}
