<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Factura;

class FacturaController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    public function update(Request $request)
    {
        $factura = Factura::find($request->id);
        $factura->idFactura = $request->id;
        switch ($request->campo) {
            case 'noFactura':
                $factura->noFactura = $request->value;
                break;
            case 'fechaFactura':
                $factura->fechaFactura = $request->value;
                break;
            default:
                break;
        }
        $saved = $factura->save();
        $data = [];
        $data['success'] = $saved;
        $data['factura'] = $factura;
        return $data;
    }
}
