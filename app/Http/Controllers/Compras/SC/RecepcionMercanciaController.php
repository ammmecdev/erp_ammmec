<?php

namespace App\Http\Controllers\Compras\SC;

use App\Models\Compras\SC\RecepcionMercancia;
use App\Models\Compras\SC\RecepcionArticulo;

use Illuminate\Support\Facades\Storage;
use App\Models\Almacen\SS\Articulo;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecepcionMercanciaController extends Controller
{
    public function store(Request $request)
    {
        $recepcionMercancia = new RecepcionMercancia;
        $recepcionMercancia->folio = $request->folio;
        $recepcionMercancia->idOrdenCompra = $request->idOrdenCompra;
        $recepcionMercancia->fecha = $request->fecha;
        $recepcionMercancia->total = $request->total;
        $recepcionMercancia->tipo = $request->tipo;
    
        if ($request->file('file')) {
            $file = $request->file('file');
            $path = $file->storeAs("CO/SC/OC/Facturas", $file->getClientOriginalName());
            $recepcionMercancia->rutaFile = "/storage/".$path;
            $recepcionMercancia->file = $file->getClientOriginalName();
        }else {
            $recepcionMercancia->file = "S/A";
        }
        $saved = $recepcionMercancia->save();

        if ($saved) {
            foreach (json_decode($request->articulos) as $articulo) {
                $recepcionArticulo = new RecepcionArticulo;
                $recepcionArticulo->idRecepcion = $recepcionMercancia->id;
                $recepcionArticulo->idArticulo = $articulo->idArticulo;
                $recepcionArticulo->cantidadOut = $articulo->cantidadOut;
                $savedRArt = $recepcionArticulo->save();
            } 
        }
        $data = [];
        $data['success'] = $saved;
        $data['recepcionMercancia'] = $recepcionMercancia;
        return $data;
    }
    
    public function getRecepcionMercanciasByOrdenCompra($orden)
    {
       return RecepcionMercancia::where("idOrdenCompra", $orden)->get();
    }

    public function downloadFile(Request $request)
    {
        return Storage::download("CO/SC/OC/Facturas/".$request->file);
    }
    
    public function reassignArticulos(Request $request)
    {
        foreach ($request->remisiones as $remision) {
            // Busca las recepciones de articulos
            $recepcionArticulos = RecepcionArticulo::where(
                "idRecepcion", $remision["id"]
            )->get();
            // Recorre y actualiza el idRecepcion por la nueva recepción
            foreach ($recepcionArticulos as $recepcionArticulo) {
                $recepcionArticulo->idRecepcion = $request->id;
                $updated = $recepcionArticulo->update();
            }
            // Elimina las recepciones seleccionada
            $recepcionMercancia = RecepcionMercancia::find($remision["id"]);
            $recepcionMercancia->delete();
        }
    }
    
    public function getRecepcionArticulosByOrdenCompra($idOrdenCompra)
    {
        $recepcionArticulos = DB::table('co_sc_recepcion_articulos')
        ->join('co_sc_recepcion_mercancia', 'co_sc_recepcion_articulos.idRecepcion', '=', 'co_sc_recepcion_mercancia.id')
        ->join('co_sc_orden_compra', 'co_sc_recepcion_mercancia.idOrdenCompra', '=', 'co_sc_orden_compra.idOrdenCompra')
        ->select('co_sc_recepcion_articulos.*')
        ->where('co_sc_orden_compra.idOrdenCompra', $idOrdenCompra)
        ->get();
        return $recepcionArticulos;
    }
}
