<?php

namespace App\Http\Controllers\Compras\SC;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Almacen\SS\SolicitudCompra;
use App\Models\Almacen\SS\SolicitudSuministro;
use App\Models\Compras\SC\OrdenCompra;
use App\Models\Compras\SC\Etapa;
use App\Models\Almacen\SS\Articulo;
use App\Models\Compras\SC\RecepcionMercancia;
use App\Exports\ResumenSolicitudesComprasExport;

use App\Notifications\Compras\SS\AutorizacionOrdenCompra;



class SolicitudController extends Controller
{
    public function getSolicitudesByAutorizador($idAutorizador)
    {
        // POR AUTORIZAR
        $solicitudesByAutorizar = DB::table('alm_ss_solicitudes_compra')
            ->join('alm_ss_solicitudes', 'alm_ss_solicitudes_compra.idSolicitudSuministros', '=', 'alm_ss_solicitudes.idSolicitud')
            ->join('usuarios', 'alm_ss_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
            ->join('co_sc_etapas', 'alm_ss_solicitudes_compra.idEtapa', '=', 'co_sc_etapas.idEtapa')
            ->select(
                'alm_ss_solicitudes_compra.*',
                'alm_ss_solicitudes.cuenta',
                'alm_ss_solicitudes.idUsuario',
                'usuarios.nombreUsuario',
                'co_sc_etapas.nombre as nombre_etapa',
                'usuarios.ruta as usuario_ruta'
            )
            ->where("alm_ss_solicitudes_compra.idEtapa", 3)
            ->get();

        $solicitudesByAutorizador = Collect();
        foreach ($solicitudesByAutorizar as $solicitud) {
            $ordenesCompra = OrdenCompra::where('idSolicitudCompra', $solicitud->idSolicitudCompra)->get();
            foreach ($ordenesCompra as $orden) {
                if ($orden->idAutorizador == $idAutorizador && !$orden->fechaAutorizacion){
                    $solicitudesByAutorizador->push($solicitud);
                    break;
                }
            }
        }
        return $solicitudesByAutorizador;
    }

    public function getSolicitudesAll()
    {
        // TODAS
       return DB::table('alm_ss_solicitudes_compra')
            ->join('alm_ss_solicitudes', 'alm_ss_solicitudes_compra.idSolicitudSuministros', '=', 'alm_ss_solicitudes.idSolicitud')
            ->join('usuarios', 'alm_ss_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
             ->join('co_sc_etapas', 'alm_ss_solicitudes_compra.idEtapa', '=', 'co_sc_etapas.idEtapa')
            ->select(
                'alm_ss_solicitudes_compra.*',
                'alm_ss_solicitudes.cuenta',
                'alm_ss_solicitudes.idUsuario',
                'alm_ss_solicitudes.tipoPedido',
                'alm_ss_solicitudes.unidadNegocio',
                'usuarios.nombreUsuario',
                'co_sc_etapas.nombre as nombre_etapa',
                'usuarios.ruta as usuario_ruta'
            )
            ->get();
    }

    public function getSolicitudCompra($idSolicitudCompra)
    {
        return DB::table('alm_ss_solicitudes_compra')
            ->join('alm_ss_solicitudes', 'alm_ss_solicitudes_compra.idSolicitudSuministros', '=', 'alm_ss_solicitudes.idSolicitud')
            ->join('usuarios', 'alm_ss_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
             ->join('co_sc_etapas', 'alm_ss_solicitudes_compra.idEtapa', '=', 'co_sc_etapas.idEtapa')
            ->select(
                'alm_ss_solicitudes_compra.*',
                'alm_ss_solicitudes.cuenta',
                'alm_ss_solicitudes.idUsuario',
                'alm_ss_solicitudes.tipoPedido',
                'alm_ss_solicitudes.unidadNegocio',
                'usuarios.nombreUsuario',
                'co_sc_etapas.nombre as nombre_etapa',
                'usuarios.ruta as usuario_ruta'
            )
            ->where("alm_ss_solicitudes_compra.idSolicitudCompra", $idSolicitudCompra)
            ->first();
    }

    public function autorizacion(OrdenCompra $orden)
    {
        $solicitudCompra = SolicitudCompra::find($orden->idSolicitudCompra);
        if ($solicitudCompra->idEtapa == 2) {
            $solicitudCompra->idEtapa = 3;
            $solicitudCompra->fechaAutorizacion = date("Y-m-d H:i:s");
            $updated = $solicitudCompra->update();
        }else {
            $updated = true;
        }
        if (!$orden->notificacion) {
            $notifiable = User::find($orden->idAutorizador);
            $notifiable->notify(new AutorizacionOrdenCompra($orden)); 
            $orden->notificacion = true;
            $orden->update();
        }   
        $data = [];
        $data['success'] = $updated;
        $data['solicitudCompra'] = $this->getSolicitudCompra($solicitudCompra->idSolicitudCompra);
        $data['ordenCompra'] = $orden;
        return $data;
    }

    public function cambioEtapa(SolicitudCompra $solicitudCompra, $idEtapa) 
    {
        $solicitudCompra->idEtapa = $idEtapa;
        switch ($solicitudCompra->idEtapa) {
            case '4':
                $accion = 'envió solicitud a terminado.';
                $solicitudCompra->fechaTermino = date("Y-m-d H:i:s");
                break;
            default:
                break;
        }
        $saved = $solicitudCompra->save();
        // Update Solicitud de suministro al terminar solicitud de compra
        if ($saved && $solicitudCompra->idEtapa == 4) { // Terminar
            $solicitud = SolicitudSuministro::find($solicitudCompra->idSolicitudSuministros);
            $solicitud->idEtapa = 4; // Recepción de mercancía
            $updated = $solicitud->update();
        }

        $data = [];
        $data['success'] = true;
        $data['solicitudCompra'] = $this->getSolicitudCompra($solicitudCompra->idSolicitudCompra);
        return $data; 
    }

    public function getEtapas() {
        return Etapa::all();
    }

    public function resumenSolicitudes()
    {
        $resumen = array();
        $solicitudesSuministros = DB::table('alm_ss_solicitudes')
            ->select(
                'alm_ss_solicitudes.idSolicitud',
                'alm_ss_solicitudes.fechaSolicitud',
                'alm_ss_solicitudes.cuenta',
                'alm_ss_solicitudes.fechaEntrega',
                'alm_ss_solicitudes.unidadNegocio'                
            )
            ->get();

        foreach ($solicitudesSuministros as $ss) {
            $solicitudCompra = SolicitudCompra::where("idSolicitudSuministros", $ss->idSolicitud)->first();

            if ($solicitudCompra) {
                $ordenesCompra = OrdenCompra::where("idSolicitudCompra", $solicitudCompra->idSolicitudCompra)->get();

               if (!$ordenesCompra->isEmpty()) {
                    foreach ($ordenesCompra as $oc) {
                        $descripcionMateriales = "";
                        $articulos = Articulo::where("idOrdenCompra", $oc->idOrdenCompra)->get();
                        if (!$articulos->isEmpty()) {
                            $lenght = count($articulos);
                            $num = 0;
                            foreach ($articulos as $articulo) {
                                $num += 1;
                                $descripcionMateriales = $descripcionMateriales."[".$articulo->descripcion.", ".$articulo->cantidadOC."]";

                                if ($lenght != 1 && $num != $lenght) {
                                    $descripcionMateriales = $descripcionMateriales.",";
                                }
                            }   
                        }
                        $recepcionMercancia = RecepcionMercancia::where("idOrdenCompra", $oc->idOrdenCompra)->get();
                        $folio = "";
                        $totalFactura = 0;
                        $fechaFactura = "";
                        if (!$recepcionMercancia->isEmpty()) {
                            $lenght_rm = count($recepcionMercancia);
                            $num_rm = 0;
                            foreach ($recepcionMercancia as $rm) {
                                $num_rm += 1;
                                $folio = $folio."[".$rm->folio.", ".$rm->tipo."]";
                                if ($lenght_rm != 1 && $num_rm != $lenght_rm) {
                                    $folio = $folio.",";
                                }
                                $totalFactura += $rm->total;
                                if ($rm->tipo == "Factura") {
                                    $fechaFactura = $fechaFactura."[".$rm->fecha."] ";
                                }
                            }
                        }   

                        $data = [
                            "idSolicitud" => $ss->idSolicitud,
                            "fechaSolicitud" => $ss->fechaSolicitud,
                            "cuenta" => $ss->cuenta,
                            "fechaEntrega" => $ss->fechaEntrega,
                            "unidadNegocio" => $ss->unidadNegocio,
                            "idOrdenCompra" => $oc->idOrdenCompra,
                            "fechaOrdenCompra" => $oc->created_at, 
                            "autorizacion" => $oc->autorizacion,
                            "tipoMoneda" => "MXN",
                            "tipoPago" => $oc->tipoPago,
                            "condicionPago" => $oc->condicionPago,
                            "razonSocial" => $oc->razon_social,
                            "nombreComercial" => $oc->nombre_proveedor,
                            "diasCredito" => $oc->dias_credito,
                            "descripcionMateriales" => $descripcionMateriales,
                            "folio" => $folio,
                            "fechaFactura" => $fechaFactura,
                            "totalFactura" => $totalFactura,
                        ];
                        array_push($resumen, $data); 
                    }
                }else {
                    $data = [
                        "idSolicitud" => $ss->idSolicitud,
                        "fechaSolicitud" => $ss->fechaSolicitud,
                        "cuenta" => $ss->cuenta,
                        "fechaEntrega" => $ss->fechaEntrega,
                        "unidadNegocio" => $ss->unidadNegocio,
                        "idOrdenCompra" => "S/O",
                        "fechaOrdenCompra" => "S/F", 
                        "autorizacion" => "S/A",
                        "tipoMoneda" => "S/M",
                        "tipoPago" => "S/P",
                        "condicionPago" => "S/M",
                        "razonSocial" => "S/R",
                        "nombreComercial" => "S/N",
                        "diasCredito" => "S/C",
                        "descripcionMateriales" => "S/D",
                        "folio" => "S/N",
                        "fechaFactura" => "S/F",
                        "totalFactura" => "S/M",
                    ];
                    array_push($resumen, $data);
                }
            }else {
                $data = [
                    "idSolicitud" => $ss->idSolicitud,
                    "fechaSolicitud" => $ss->fechaSolicitud,
                    "cuenta" => $ss->cuenta,
                    "fechaEntrega" => $ss->fechaEntrega,
                    "unidadNegocio" => $ss->unidadNegocio,
                    "idOrdenCompra" => "S/O",
                    "fechaOrdenCompra" => "S/F", 
                    "autorizacion" => "S/A",
                    "tipoMoneda" => "S/M",
                    "tipoPago" => "S/P",
                    "condicionPago" => "S/M",
                    "razonSocial" => "S/R",
                    "nombreComercial" => "S/N",
                    "diasCredito" => "S/C",
                    "descripcionMateriales" => "S/D",
                    "folio" => "S/N",
                    "fechaFactura" => "S/F",
                    "totalFactura" => "S/M",
                ];
                array_push($resumen, $data);
            }      
        }

        return (new ResumenSolicitudesComprasExport($resumen))->download("Resumen solicitudes.xlsx");
    }
}
