<?php

namespace App\Http\Controllers\Compras\SC;

use App\Models\Compras\SC\Autorizacion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AutorizacionController extends Controller
{
    public function update(Request $request, Autorizacion $autorizacion)
    {
        if ($request->campo == "montoInicial") {
            $autorizacion->montoInicial = $request->monto;
        }else if ($request->campo == "montoFin") {
            $autorizacion->montoFin = $request->monto;
        }
    
        $updated=$autorizacion->update();
        $data=[];
        $data['success']=$updated;
        $data['autorizacion']=$autorizacion;
        return $data;

    }

    public function getAutorizaciones()
    {
        return Autorizacion::all();
    }
}
