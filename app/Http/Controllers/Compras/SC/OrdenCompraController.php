<?php

namespace App\Http\Controllers\Compras\SC;

use App;
use App\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Almacen\SS\Articulo;
use App\Models\Compras\SC\OrdenCompra;

use App\Models\Compras\SC\Comentario;
use Illuminate\Support\Facades\Storage;
use App\Models\Compras\Configuracion\Proveedor;

use App\Notifications\Compras\SS\OrdenCompraAutorizada;
use App\Notifications\Compras\SS\OrdenCompraRechazada;
use App\Notifications\Compras\SC\PagoOrdenCompra;

class OrdenCompraController extends Controller
{
    public function store(Request $request)
    {
        $oc = new OrdenCompra;
        $oc->idSolicitudCompra = $request->idSolicitudCompra;
        $oc->idProveedor = $request->proveedor;
        $oc->fechaEntrega = $request->fechaEntrega;

        $oc->iva = $request->iva;
        $oc->retencionIVA = $request->retencionIVA;
        $oc->retencionISR = $request->retencionISR;
        $oc->otrosImpuestos = $request->otrosImpuestos;

        $oc->formaPago = $request->formaPago;
        $oc->metodoPago = $request->metodoPago;
        $oc->tipoMoneda = $request->tipoMoneda;

        $oc->tipoPedido = $request->tipoPedido;
        $oc->centroCosto = $request->centroCosto;
        $oc->referenciaCotizacion = $request->referenciaCotizacion;
        $oc->condicionesRM = $request->condicionesRM;
        $oc->cfdi = $request->cfdi;

        $oc->idAutorizador = $request->idAutorizador;
        $oc->autorizacion = false;
        $oc->observaciones = $request->observaciones;
        $oc->notificacion = false;
        $saved = $oc->save();
        if ($saved) { 
            foreach ($request->articulos as $item) {
                $articulo = Articulo::find($item['idArticulo']);
                $articulo->idOrdenCompra = $oc->idOrdenCompra;
                $articulo->costoUnitario = $item['costoUnitario'];
                $articulo->descuento = $item['descuento'];
                $articulo->codigo = $item['codigo'];
                $articulo->descripcion = $item['descripcion'];
                $updatedArticulo = $articulo->update();
            }
        }
        $data = [];
        $data['success'] = $saved;
        $data['ordenCompra'] = $oc;
        return $data;    
    }

    public function update(OrdenCompra $orden, Request $request)
    {
        $orden->idProveedor = $request->proveedor;
        $orden->fechaEntrega = $request->fechaEntrega;

        $orden->iva = $request->iva;
        $orden->retencionIVA = $request->retencionIVA;
        $orden->retencionISR = $request->retencionISR;
        $orden->otrosImpuestos = $request->otrosImpuestos;

        $orden->formaPago = $request->formaPago;
        $orden->metodoPago = $request->metodoPago;
        $orden->tipoMoneda = $request->tipoMoneda;
        $orden->tipoPedido = $request->tipoPedido;
        $orden->centroCosto = $request->centroCosto;
        $orden->referenciaCotizacion = $request->referenciaCotizacion;
        $orden->condicionesRM = $request->condicionesRM;
        $orden->cfdi = $request->cfdi;
        $orden->observaciones = $request->observaciones;
        $updated = $orden->update();

        if ($updated) { 
            //QUITAR ARTICULOS ANTERIORES
            $articulos_old = Articulo::where("idOrdenCompra", $orden->idOrdenCompra)->get();
            foreach ($articulos_old as $articulo) {
                $articulo->idOrdenCompra = null;
                $articulo->costoUnitario = 0;
                $articulo->descuento = 0;
                $articulo->codigo = "";
                $articulo->update();
            }

            //ASIGNAR ARTICULOS NUEVOS
            foreach ($request->articulos as $item) {
                $articulo = Articulo::find($item['idArticulo']);
                $articulo->idOrdenCompra = $orden->idOrdenCompra;
                $articulo->costoUnitario = $item['costoUnitario'];
                $articulo->descuento = $item['descuento'];
                $articulo->codigo = $item['codigo'];
                $articulo->update();
            }
        }
        $data = [];
        $data['success'] = $updated;
        $data['ordenCompra'] = $orden;
        return $data;   
    }
    
    public function getOrdenesCompra($idSolicitudCompra)
    {
        return OrdenCompra::where("idSolicitudCompra", $idSolicitudCompra)->get();
    }

    public function getOrdenCompra($ordenCompra)
    {
       return OrdenCompra::find($ordenCompra);
    }

    public function getArticulosByOrdenCompra(OrdenCompra $ordenCompra)
    {
       return $ordenCompra->articulos;
    }

    public function autorizacionOrdenCompra(OrdenCompra $orden, Request $request)
    {
        if ($request->status == "autorizado") {
            $orden->autorizacion = true; 
            $orden->fechaAutorizacion = date("Y-m-d H:i:s");
        }else {
            $orden->autorizacion = false; 
            $orden->fechaAutorizacion = date("Y-m-d H:i:s");
        }
        $saved = $orden->save();

        $orden->comentarios = Comentario::where('id', $orden->idOrdenCompra)
            ->where('idEtapa', 3)
            ->where('tabla', 'co_sc_orden_compra')
            ->latest()
            ->first();

        $orden->autorizador = User::find($orden->idAutorizador);

        if ($orden->autorizacion) {
            $notifiable = User::find(44); //44 PARA COMPRAS
            $notifiable->notify(new OrdenCompraAutorizada($orden));

            if ($orden->metodoPago == "Contado" || $orden->metodoPago == "PUE" ) {
                $data = [
                    "idOrdenCompra" => $orden->idOrdenCompra,
                    "nombre_proveedor" => $orden->nombre_proveedor,
                    "cuenta" => $request->cuenta,
                    "total" => number_format($request->total,2,".",","),
                    "metodoPago" => $orden->metodoPago,
                    "diasCredito" => $orden->dias_credito,
                ];
                $notifiableDFN = User::find(59); // CONTABILIDAD
                $notifiableDFN->notify(new PagoOrdenCompra($data)); 
            }
        }else {
            $notifiable = User::find(44); // 44 PARA COMPRAS
            $notifiable->notify(new OrdenCompraRechazada($orden));
        }

        $data = [];
        $data['ordenCompra'] = $orden;
        $data['success'] = $saved;
        return $data; 
    }

    public function generateOrdenCompra(Request $request) {
        $data = request();
        $solicitante = User::where('idUsuario', $data->id_solicitante)->first(['nombreUsuario']);
        $coordinadorCompras = User::where('idUsuario', 44)->first(['nombreUsuario']);
        //FALTA AGREGAR CONFIGURACIÓN DESDE TH
        $autorizador = User::where('idUsuario', $data->orden['idAutorizador'])->first(['nombreUsuario']);
        $proveedor = Proveedor::where('idProveedor', $data->orden['idProveedor'])->first(['nombre','razonSocial', 'contacto', 'email', 'rfc', 'telefono']);

        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A4', 'landscape'); // Hoja horizontal
        
        $viewReporte = ($data->unidadNegocio != "Smart") 
            ? "modulos.compras.reportes.orden_compra"
            : "modulos.compras.reportes.orden_compra_smart";

        $pdf->loadView($viewReporte, [
            'articulos' => $data->articulos, 
            'orden' => $data->orden, 
            'created_at' => $data->created_at, 
            'delivery_date' => $data->delivery_date, 
            'autorizador' => $autorizador->nombreUsuario,
            'solicitante' => $solicitante->nombreUsuario,
            'coordinadorCompras' => $coordinadorCompras->nombreUsuario,            
            'proveedor' => $proveedor,
            'subTotal' => number_format(floatval($data->subTotal),2,".",","),
            'montoDescuento' => number_format(floatval($data->importeDescuento),2,".",","),
            'iva' => number_format(floatval($data->orden['iva']),2,".",","),
            'retencionIVA' => number_format(floatval($data->orden['retencionIVA']),2,".",","),
            'retencionISR' => number_format(floatval($data->orden['retencionISR']),2,".",","),
            'otrosImpuestos' => number_format(floatval($data->orden['otrosImpuestos']),2,".",","),
            'total' => number_format(floatval($data->subTotal) + floatval($data->orden['iva']) + floatval($data->orden['retencionIVA']) + floatval($data->orden['retencionISR']) + floatval($data->orden['otrosImpuestos']),2,".",","),
        ]);

        Storage::put("CO/SC/Orden de Compra.pdf", $pdf->output());
        $result = [];
        $result['success'] = true; 
        return $result;
    }

    public function downloadOrdenCompra($name) {
       return Storage::download("CO/SC/".$name.".pdf");
    }

    public function updateMetodoPago(OrdenCompra $orden, Request $request)
    {
        $orden->metodoPago = $request->metodoPago;
        $updated = $orden->update();
        $data = [];
        $data['ordenCompra'] = $orden;
        $data['success'] = $updated;
        return $data; 
    }

    public function updateArticuloDescripcion(Articulo $articulo, Request $request)
    {
        $articulo->descripcion = $request->descripcion;
        $updated = $articulo->update();
        $data = [];
        $data['articulo'] = $articulo;
        $data['success'] = $updated;
        return $data; 
    }
}
