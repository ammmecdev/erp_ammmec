<?php

namespace App\Http\Controllers\Compras\SC;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Compras\SC\Comentario;

class ComentarioController extends Controller
{
    public function store(Request $request)
    {
        $saved = true;
        $comentario = Comentario::where('id', $request->idOrdenCompra)->where('tabla', $request->tabla)->where('idEtapa', $request->idEtapa)->first();
        if ($comentario && $request->comentarios == "") {
            $saved = $comentario->delete();  
        } 
        if($request->comentarios != "") {
            if (!$comentario) {
                $comentario = new Comentario;
                $comentario->idEtapa = $request->idEtapa;
                $comentario->id = $request->idOrdenCompra;
                $comentario->tabla = $request->tabla;
            }
            $comentario->comentario = $request->comentarios;
            $saved = $comentario->save();  
        }
        $data = [];
        $data['success'] = $saved;
        return $data; 
    }

    public function getComentarioByOrdenCompra($id)
    {
        return Comentario::where('id', $id)->where('tabla', 'co_sc_orden_compra')->where('idEtapa', 3)->first();
    }
}
