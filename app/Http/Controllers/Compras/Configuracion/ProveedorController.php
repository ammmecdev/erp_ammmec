<?php

namespace App\Http\Controllers\Compras\Configuracion;

use App\Models\Compras\Configuracion\Proveedor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\ProveedoresExport;

class ProveedorController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $proveedor = new Proveedor;
        $proveedor->nombre = $request->nombre;
        $proveedor->razonSocial = $request->razonSocial;
        $proveedor->rfc = $request->rfc;
        $proveedor->email = $request->email;
        $proveedor->pagoCredito = $request->pagoCredito;
        $proveedor->diasCredito = $request->diasCredito;
        $proveedor->anticipo = $request->anticipo;
        $proveedor->contacto = $request->contacto;
        $proveedor->telefono = $request->telefono;
        $proveedor->direccion = $request->direccion;
        $saved = $proveedor->save();
        $data = [];
        $data['success'] = $saved;
        $data['proveedor'] = $proveedor;
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Compras\Configuracion\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proveedor $proveedor)
    {
        $proveedor->nombre = $request->nombre;
        $proveedor->razonSocial = $request->razonSocial;
        $proveedor->rfc = $request->rfc;
        $proveedor->email = $request->email;
        $proveedor->pagoCredito = $request->pagoCredito;
        $proveedor->diasCredito = $request->diasCredito;
        $proveedor->anticipo = $request->anticipo;
        $proveedor->contacto = $request->contacto;
        $proveedor->telefono = $request->telefono;
        $proveedor->direccion = $request->direccion;
        $updated=$proveedor->update();
        $data=[];
        $data['success']=$updated;
        $data['proveedor']=$proveedor;
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Compras\Configuracion\Proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proveedor $proveedor)
    {
        $deleted = $proveedor->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getProveedores()
    {
        return Proveedor::all();
    }

    public function exportExcel()
    {
        $nombre = "Proveedores.xlsx";
        $proveedores = Proveedor::all();
        return (new ProveedoresExport($proveedores))->download($nombre);
    }
}
