<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Costo;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


class CostoController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
	public function ventas_index()
	{

	}

	public function edit()
	{

	}

	public function getCostos()
	{
   		return Costo::orderBy('id', 'DESC')->get();
	}

	public function store(Request $request)
	{
		$sco = new Costo();
		$sco->fecha_emision = Carbon::now();
		$sco->fecha_requerida = $request->fecha_requerida;
		$sco->no_requisicion = $request->no_requisicion;
		$sco->negocio_id = $request->negocio_id;
		$sco->contacto_id = $request->contacto_id;
		$sco->usuario_id = 7;
		$sco->tiempo_entrega = null;
		$sco->estado = 'Enviado';
		$sco->observaciones = null;
		$saved = $sco->save();

		$data = [];
		$data['success'] = $saved;
        $data['sco'] = $sco;

		return $data;
	}

	public function update(Request $request)
    {
        $costo = Costo::find($request->id);
        $costo->idCosto = $request->id;
        switch ($request->campo) {
            case 'fechaSolicitud':
                $costo->fechaSolicitud = $request->value;
                break;
            case 'fechaEntrega':
                $costo->fechaEntrega = $request->value;
                break;
            case 'montoTotal':
                $costo->montoTotal = $request->value;
                break;
            default:
                break;
        }
        $saved = $costo->save();
        $data = [];
        $data['success'] = $saved;
        $data['costo'] = $costo;
        return $data;
    }
}
