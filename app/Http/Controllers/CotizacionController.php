<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cotizacion;
use Illuminate\Support\Facades\DB;

class CotizacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function update(Request $request)
    {
        $cotizacion = Cotizacion::find($request->id);
        $cotizacion->idCotizacion = $request->id;
        switch ($request->campo) {
            case 'fechaCotizacion':
                $cotizacion->fechaCotizacion = $request->value;
                break;
            case 'claveCotizacion':
                $cotizacion->claveCotizacion = $request->value;
                break;
            default:
                break;
        }
        $saved = $cotizacion->save();
        $data = [];
        $data['success'] = $saved;
        $data['cotizacion'] = $cotizacion;
        return $data;
    }
}
