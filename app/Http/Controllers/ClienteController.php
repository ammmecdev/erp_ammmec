<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Contacto;
use App\Sector;

class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function getClientes()
    {
       return Cliente::all();
    }

    public function getClienteById(Cliente $cliente)
    {
        return $cliente;
    }

    public function store(Request $request)
    {
        $cliente=new Cliente;
        $cliente->idUsuario=$request['idUsuario'];
        $cliente->nombre=$request['nombre'];
        $cliente->direccion=$request['direccion'];
        $cliente->paginaWeb=$request['paginaWeb'];
        $cliente->telefono=$request['telefono'];
        $cliente->idSector=$request['sector'];
        $cliente->clave=$request['clave'];
        $cliente->localidadEstado=$request['localidadEstado'];
        $cliente->extensionTelefonica=$request['extensionTelefonica'];
        $cliente->tipoTelefono=$request['tipoTelefono'];
        $cliente->rfc=$request['rfc'];
        $saved=$cliente->save();
        $data=[];
        $data['success']=$saved;
        $data['cliente']=$cliente;

        return $data;
    }

    public function update(Request $request)
    {
       
        $cliente = Cliente::find($request->idCliente);
        $cliente->nombre = $request->nombre;
        $cliente->idUsuario = $request->idUsuario;
        $cliente->direccion = $request->direccion;
        $cliente->paginaWeb = $request->paginaWeb;
        $cliente->telefono = $request->telefono;
        $cliente->clave= $request->clave;
        $cliente->idSector = $request->sector;
        $cliente->localidadEstado = $request->localidadEstado;
        $cliente->extensionTelefonica = $request->extensionTelefonica;
        $cliente->tipoTelefono = $request->tipoTelefono;
        $cliente->rfc = $request->rfc;
        $cliente->lead = null;
        $updated = $cliente->update();
        $data = [];
        $data['success'] = $updated;
        $data['cliente'] = $cliente;
        return $data;
    }

    public function delete(Cliente $cliente)
    {
        $deleted = $cliente->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }
    public function selectClientes(Request $query)
	{ 
    	$cliente = Cliente::all();
        $data = [];
    	foreach($cliente as $key => $value){
        	$data[$key] = [
                'text'  => $value->nombre,
        	    'value' =>  [
                                'text' => $value->clave,
                                'value' => $value->idCliente
                            ]                    
    		];
    	}
    	return response()->json($data); 
    }
    public function selectClientesLike(Request $query)
    {
        return Cliente::where('clave', 'like', '%' . $query->searchQuery . '%')->orWhere('nombre', 'like', '%' . $query->searchQuery . '%')->get();
    }

    public function selectContactosByIdCliente(Cliente $cliente)
    {
        $contactos = $cliente->contacto;
   
        $data = [];
        foreach($contactos as $key => $value){
            $data[$key] = [
                'text'  => $value->nombre,
                'value' => $value->idContacto
            ];
        }
        return response()->json($data);
    }

    public function selectSector()
    {
        $sector = Sector::get();
        $data = [];
        foreach($sector as $key => $value){
            $data[$key] = [
                'text'  => $value['nombreSector'],
                'value' => $value['idSector']
                            
            ];
        }
        return response()->json($data);
    }

    public function getClaveCliente(Request $request)
    {
        $cliente = Cliente::where('idCliente', $request->idCliente)->first();
        return $cliente->clave;
    }
}
