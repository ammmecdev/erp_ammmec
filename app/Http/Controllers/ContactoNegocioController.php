<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactoNegocio;
use App\Negocio;

class ContactoNegocioController extends Controller
{
    public function store(Request $request){
        $participante = new ContactoNegocio();

        $participante->negocio_idNegocio = $request->idNegocio;
        $participante->contacto_idContacto = $request->idContacto;

        $saved  = $participante->save();

        return $data = [
            'success' => $saved,
            'participante' => $participante
        ];
    }

    public function getParticipantes(Negocio $negocio){
        return $negocio->negocioContacto;
    }
}
