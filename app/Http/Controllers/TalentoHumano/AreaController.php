<?php

namespace App\Http\Controllers\TalentoHumano;

use App\Models\TalentoHumano\Area;
use Illuminate\Support\Facades\DB;
use App\Models\Calidad\GC\ManualDepartamental;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getAreas()
    {
       return Area::all();
    }

    public function store(Request $request)
    {
        $area = new Area;
        $area->nombre = $request->nombre;       
        $saved = $area->save();
        $data = [];
        $data['success'] = $saved;
        $data['area'] = $area;
        return $data;
    }

    public function update(Request $request, Area $area)
    {
        $area->nombre = $request->nombre;
        $updated=$area->update();
        $data=[];
        $data['success']=$updated;
        $data['area']=$area;
        return $data;
    }

    public function destroy(Area $area)
    {
        $deleted = $area->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getArea($codigo) // PENDIENTES SE CAMBIO AREAS POR DEPARTAMENTOS
    {
      $area = Area::where('codigo', $codigo)->first();
      $manual = ManualDepartamental::where('idArea', $area->idArea)->first();
      $area->manual = $manual;
      return $area;
    }

    public function getProcesos(Area $area) // PENDIENTES SE CAMBIO AREAS POR DEPARTAMENTOS
    {
        $procesos = $area->procesos;
        foreach ($procesos as $proceso) {
            $proceso->procedimientos = $proceso->procedimientos;    
        }
        return $procesos;
    }

    public function getArchivosByArea($idArea) // PENDIENTES SE CAMBIO AREAS POR DEPARTAMENTOS
    {
        return DB::table('nr_gc_archivos_procedimientos')
            ->join('nr_gc_procedimientos', 'nr_gc_archivos_procedimientos.idProcedimiento', '=', 'nr_gc_procedimientos.idProcedimiento')
            ->join('nr_gc_procesos', 'nr_gc_procedimientos.idProceso', '=', 'nr_gc_procesos.idProceso')
            ->join('rh_areas', 'nr_gc_procesos.idArea', '=', 'rh_areas.idArea')
            ->select('nr_gc_archivos_procedimientos.*')
            ->where('rh_areas.idArea', $idArea)
            ->get();
    }
}
