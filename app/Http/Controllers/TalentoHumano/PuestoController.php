<?php

namespace App\Http\Controllers\TalentoHumano;

use App\Models\TalentoHumano\Puesto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PuestoController extends Controller
{
    public function store(Request $request)
    {
        $puesto = new Puesto;
        $puesto->nombre = $request->nombre;
        $puesto->codigo = $request->codigo; 
        $puesto->idArea = $request->area;
        $puesto->idDepartamento = $request->departamento;       
        $saved = $puesto->save();
        $data = [];
        $data['success'] = $saved;
        $data['puesto'] = $puesto;
        return $data;
    }

    public function update(Request $request, Puesto $puesto)
    {
        $puesto->nombre = $request->nombre;
        $puesto->codigo = $request->codigo;
        $puesto->idArea = $request->area;
        $puesto->idDepartamento = $request->departamento;
        $updated=$puesto->update();
        $data=[];
        $data['success']=$updated;
        $data['puesto']=$puesto;
        return $data;
    }

    public function destroy(Puesto $puesto)
    {
        $deleted = $puesto->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getPuestos()
    {
       return Puesto::all();
    }

    public function getPerfilPuesto(Puesto $puesto)
    {
        $puesto->area = $puesto->area;
        $puesto->perfil = $puesto->perfilPuesto;
        return $puesto;
    }
}
