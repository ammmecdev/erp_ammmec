<?php

namespace App\Http\Controllers\TalentoHumano;

use App\Models\TalentoHumano\MotivoAusencia;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MotivoAusenciaController extends Controller
{
    public function getMotivosAusencia()
    {
        return MotivoAusencia::all();
    }

    public function store(Request $request)
    {
        $motivoAusencia = new MotivoAusencia;
        $motivoAusencia->motivo = $request->motivo;
        $motivoAusencia->diasAnticipacion = $request->diasAnticipacion;        
        $saved = $motivoAusencia->save();
        $data = [];
        $data['success'] = $saved;
        $data['motivoAusencia'] = $motivoAusencia;
        return $data;
    }

    public function update(Request $request, MotivoAusencia $motivoAusencia)
    {
        $motivoAusencia->motivo = $request->motivo;
        $motivoAusencia->diasAnticipacion = $request->diasAnticipacion;
        $updated=$motivoAusencia->update();
        $data=[];
        $data['success']=$updated;
        $data['motivoAusencia']=$motivoAusencia;
        return $data;
    }

    public function destroy(MotivoAusencia $motivoAusencia)
    {
        $deleted = $motivoAusencia->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }
}
