<?php

namespace App\Http\Controllers\TalentoHumano;

use App\Models\TalentoHumano\DiaFestivo;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DiaFestivoController extends Controller
{
    public function store(Request $request)
    {
        $diaFestivo = new DiaFestivo;
        $diaFestivo->descripcion = $request->descripcion;
        $diaFestivo->fecha = $request->fecha; 
        $diaFestivo->fechaFeriado = $request->fechaFeriado;
        $diaFestivo->dia = $request->dia;       
        $diaFestivo->noDia = $request->noDia;       
        $diaFestivo->mes = $request->mes;
        $diaFestivo->periodo = $request->periodo;       
        $saved = $diaFestivo->save();
        $data = [];
        $data['success'] = $saved;
        $data['diaFestivo'] = $diaFestivo;
        return $data;
    }

    public function update(Request $request, DiaFestivo $diaFestivo)
    {
        $diaFestivo->descripcion = $request->descripcion;
        $diaFestivo->fecha = $request->fecha; 
        $diaFestivo->fechaFeriado = $request->fechaFeriado; 
        $diaFestivo->dia = $request->dia;       
        $diaFestivo->noDia = $request->noDia;       
        $diaFestivo->mes = $request->mes; 
        $diaFestivo->periodo = $request->periodo;       
        $updated=$diaFestivo->update();
        $data=[];
        $data['success']=$updated;
        $data['diaFestivo']=$diaFestivo;
        return $data;
    }

    public function destroy(DiaFestivo $diaFestivo)
    {
        $deleted = $diaFestivo->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getDiasFestivos()
    {
       return DiaFestivo::orderBy('fechaFeriado', 'ASC')->get();
    }
}
