<?php

namespace App\Http\Controllers\TalentoHumano;

use App\Models\TalentoHumano\Empleado;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmpleadoController extends Controller
{
    public function store(Request $request)
    {
        $empleado = new Empleado;
        $empleado->noEmpleado = $request->noEmpleado;
        $empleado->idPuesto = $request->puesto; 
        $empleado->nombre = $request->nombre;
        $empleado->nacionalidad = $request->nacionalidad;       
        $empleado->sexo = $request->sexo;       
        $empleado->edad = $request->edad;       
        $empleado->estadoCivil = $request->estadoCivil;       
        $empleado->domicilio = $request->domicilio;       
        $empleado->cp = $request->codigoPostal;       
        $empleado->curp = $request->curp;
        $empleado->rfc = $request->rfc;       
        $empleado->fechaIngreso = $request->fechaIngreso;       
        $empleado->tipoContrato = $request->tipoContrato;       
        $empleado->tipoPuesto = $request->tipoPuesto;       
        $empleado->fechaTermino = $request->fechaTermino;       
        $empleado->telefono = $request->telefono;       
        $empleado->email = $request->email;
        $empleado->emailPersonal = $request->emailPersonal;
        $empleado->nss = $request->nss;
        $empleado->unidadNegocio = $request->unidadNegocio;        
        $empleado->salario = $request->salario;
        $empleado->tarjetaBanbajio = $request->tarjetaBanbajio;
        $empleado->fechaBaja = $request->fechaBaja;
        $empleado->status = "Activo";
        $saved = $empleado->save();
        $data = [];
        $data['success']=$saved;
        $data['empleado']=$this->getEmpleado($request->noEmpleado);
        return $data;
    }

    public function update(Request $request, Empleado $empleado)
    {
        $empleado->idPuesto = $request->puesto; 
        $empleado->nombre = $request->nombre;
        $empleado->nacionalidad = $request->nacionalidad;       
        $empleado->sexo = $request->sexo;       
        $empleado->edad = $request->edad;       
        $empleado->estadoCivil = $request->estadoCivil;       
        $empleado->domicilio = $request->domicilio;       
        $empleado->cp = $request->codigoPostal;       
        $empleado->curp = $request->curp;
        $empleado->rfc = $request->rfc;       
        $empleado->fechaIngreso = $request->fechaIngreso;       
        $empleado->tipoContrato = $request->tipoContrato;       
        $empleado->tipoPuesto = $request->tipoPuesto;       
        $empleado->fechaTermino = $request->fechaTermino;       
        $empleado->telefono = $request->telefono;       
        $empleado->email = $request->email;
        $empleado->emailPersonal = $request->emailPersonal;
        $empleado->nss = $request->nss;
        $empleado->unidadNegocio = $request->unidadNegocio;
        $empleado->salario = $request->salario;
        $empleado->tarjetaBanbajio = $request->tarjetaBanbajio;
        $empleado->fechaBaja = $request->fechaBaja;
        $empleado->status = $request->status;
        $updated=$empleado->update();
        $data=[];
        $data['success']=$updated;
        $data['empleado']=$this->getEmpleado($request->noEmpleado);
        return $data;
    }

    public function getEmpleado($noEmpleado)
    {
        $data = DB::table('empleados')
            ->join('rh_puestos', 'empleados.idPuesto', '=', 'rh_puestos.idPuesto')
            ->join('rh_areas', 'rh_puestos.idArea', '=', 'rh_areas.idArea')
            ->join('rh_departamentos', 'rh_puestos.idDepartamento', '=', 'rh_departamentos.idDepartamento')

            ->select(
                'empleados.*', 
                'rh_puestos.nombre as nombre_puesto',
                'rh_areas.nombre as nombre_area',
                'rh_departamentos.nombre as nombre_departamento'
            )
            ->where("noEmpleado", $noEmpleado)
            ->first();
        return json_encode($data);
    }

    public function destroy(Empleado $empleado)
    {
        $deleted = $empleado->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getEmpleados()
    {
        $data = DB::table('empleados')
            ->join('rh_puestos', 'empleados.idPuesto', '=', 'rh_puestos.idPuesto')
            ->join('rh_areas', 'rh_puestos.idArea', '=', 'rh_areas.idArea')
            ->join('rh_departamentos', 'rh_puestos.idDepartamento', '=', 'rh_departamentos.idDepartamento')
            ->select(
                'empleados.*', 
                'rh_puestos.nombre as nombre_puesto',
                'rh_areas.nombre as nombre_area',
                'rh_departamentos.nombre as nombre_departamento'
            )->get();

        return $data;
    }

    public function getEmpleadoByNumeroEmpleado($noEmpleado)
    {
        $empleados = Empleado::where('noEmpleado', 'like' ,'%'.$noEmpleado.'%')->get();
        foreach($empleados as $empleado) {
            if (isset($empleado)) {
                $noEmpleadoCompleto = explode('-', $empleado->noEmpleado, 2);
                $empleado->consecutivo = $noEmpleadoCompleto[0];
                if ($empleado->consecutivo == $noEmpleado) {
                    return $empleado;
                }
            }
        }
    }

}
