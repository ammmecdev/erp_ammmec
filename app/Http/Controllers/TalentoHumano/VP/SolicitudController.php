<?php

namespace App\Http\Controllers\TalentoHumano\VP;

use App;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\TalentoHumano\VP\Solicitud;
use App\Models\TalentoHumano\VP\Comentario;
use App\Models\TalentoHumano\Empleado;
use App\Models\TalentoHumano\VP\VacacionesPendientes;

use App\Notifications\TalentoHumano\VP\AutorizacionSolicitud;
use App\Notifications\TalentoHumano\VP\SolicitudAutorizada;
use App\Notifications\TalentoHumano\VP\SolicitudRechazada;
use App\Notifications\TalentoHumano\VP\NuevaSolicitud;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SolicitudController extends Controller
{
    public function store(Request $request)
    {

        $solicitud = new Solicitud;
        $solicitud->idUsuario = $request->idUsuario;
        $solicitud->motivoAusencia = $request->motivoAusencia;
        $solicitud->fechaInicio = $request->fechaInicio;
        $solicitud->fechaFin = $request->fechaFin;
        $solicitud->dias = $request->dias;
        $solicitud->horaInicio = $request->horaInicio;
        $solicitud->horaFin = $request->horaFin;
        $solicitud->horas = $request->horas;
        $solicitud->descripcion = $request->descripcion;
        $solicitud->tipo = $request->tipo;
        $solicitud->idAutorizador = $request->idAutorizador;
        $solicitud->fechaAutorizacion = $request->fechaAutorizacion;
        $solicitud->autorizacion = $request->autorizacion;
        $solicitud->periodo = $request->periodo;
        $solicitud->idEtapa = $request->idEtapa;
        $solicitud->semanasCapturacion = null;
        
        if ($request->tipo == "Vacaciones" && $request->pendientes) {
            // $data = $this->updateVacacionesPendientes($request->noEmpleado, $request->dias);
            // $solicitud->diasTomadosPendiente = $data["pendientes"];
            // $solicitud->diasTomadosPendiente = 0;
        }else {
            // $solicitud->diasTomadosPendiente = 0;
        }
            
        $saved = $solicitud->save();
        if ($saved) {
            // $notifiableAutorizador = User::find(5);
            // $notifiableAutorizador->notify(new AutorizacionSolicitud($solicitud));
            // $notifiableTH = User::find(5);
            // $notifiableTH->notify(new NuevaSolicitud($solicitud));             
        }

        $data = [];
        $data['success'] = $saved;
        $data['solicitud'] = $solicitud;
        return $data;
    }

    public function updateVacacionesPendientes($noEmpleado, $dias)
    {
        $vp = VacacionesPendientes::find($noEmpleado);
        $diasTomados = $vp->diasTomados + $dias;

        if ($diasTomados >= $vp->diasAcumulados) {
            $vp->diasTomados = $vp->diasAcumulados;
            $pendientes = $dias - ($diasTomados - $vp->diasAcumulados);
        }else {
            $vp->diasTomados = $diasTomados;
            $pendientes = $dias;
        }
        $updated = $vp->update();
        $data = [];
        $data['success'] = $updated;
        $data['pendientes'] = $pendientes;
        return $data; 
    }   

    public function getSolicitudesByAutorizador($idAutorizador)
    {
        // POR AUTORIZAR
        $solicitudesByAutorizar = Solicitud::where('idEtapa', 1)->get();
        $solicitudesByAutorizador = Collect();
        foreach ($solicitudesByAutorizar as $solicitud) {
            if($solicitud->idAutorizador == $idAutorizador)
                $solicitudesByAutorizador->push($solicitud);
        }
        foreach ($solicitudesByAutorizador as $solicitud) 
            $solicitud->usuario = $solicitud->usuario;
        return $solicitudesByAutorizador;
    }

    public function getSolicitudesAll()
    {
        // TODAS
        $solicitudes = Solicitud::all();
        foreach ($solicitudes as $solicitud) 
            $solicitud->usuario = $solicitud->usuario;
        return $solicitudes;
    }

    public function getSolicitudesByUsuario($idUsuario)
    {
        // POR USUARIO
        $solicitudes = Solicitud::where('idUsuario', $idUsuario)->get();
        foreach ($solicitudes as $solicitud) 
            $solicitud->usuario = $solicitud->usuario;
        return $solicitudes;
    }

    // public function getSolicitudesByPeriodo($idUsuario, Request $request)
    // {
    //     $solicitudes = Solicitud::where('idUsuario', $idUsuario)
    //         ->whereBetween('fechaInicio', [$request->inicio, $request->termino])
    //         ->orderBy('fechaInicio', 'ASC')
    //         ->get();

    //     return $solicitudes;
    // }

     public function getSolicitudesByEmpleado($idUsuario)
    {
        $solicitudes = Solicitud::where('idUsuario', $idUsuario)
            ->orderBy('fechaInicio', 'ASC')
            ->get();

        return $solicitudes;
    }

    public function autorizacion(Solicitud $solicitud, Request $request)
    {
        $solicitud->idEtapa = $request->idEtapa;
        $solicitud->fechaAutorizacion = date("Y-m-d H:i:s");
        $solicitud->autorizacion = $request->status == "autorizado" ? true : false;
        $updated = $solicitud->update();

        $solicitud->comentarios = Comentario::where('idSolicitud', $solicitud->idSolicitud)->where('idEtapa', $request->idEtapa)->latest()->first();
        $solicitud->autorizador = $request->autorizador;
        
        if ($request->notificar) {
            $notifiable = User::find($solicitud->idUsuario);
            if ($request->status == "autorizado") {
                $notifiable->notify(new SolicitudAutorizada($solicitud));
            }else {
                $notifiable->notify(new SolicitudRechazada($solicitud));
            }
        }

        $data = [];
        $data['success'] = $updated;
        $data['solicitud'] = Solicitud::where("idSolicitud", $solicitud->idSolicitud)->first();
        return $data;
    }

    public function addSemanas(Solicitud $solicitud, Request $request)
    {
        $solicitud->semanasCapturacion = $request->semanas;
        $updated = $solicitud->update();
        $data = [];
        $data['success'] = $updated;
        $data['solicitud'] = $solicitud;
        return $data;
    }

     public function uploadReporte(Request $request) {
        $data = request();
       
        $empleado = DB::table('empleados')
            ->join('rh_puestos', 'empleados.idPuesto', '=', 'rh_puestos.idPuesto')
            ->join('rh_areas', 'rh_puestos.idArea', '=', 'rh_areas.idArea')
            ->select(
                'rh_puestos.nombre as nombre_puesto',
                'rh_areas.nombre as nombre_area'
            )
            ->where("noEmpleado", $data->usuario["noEmpleado"])
            ->first();

        $pdf = App::make('dompdf.wrapper');
        
        $viewReporte = ($data->tipo != "Permiso") 
            ? "modulos.talentoHumano.reportes.vacaciones"
            : "modulos.talentoHumano.reportes.permiso";

        $arrayData = [
            'fechaSolicitud' => $data->created_at, 
            'motivoAusencia' => $data->motivoAusencia,
            'fechaInicio' => $data->fechaInicio,
            'fechaFin' => $data->fechaFin,
            'horaInicio' => $data->horaInicio, 
            'horaFin' => $data->horaFin,
            'comentarios' => $data->descripcion,
            'nombreUsuario' => $data->usuario["nombreUsuario"], 
            'noEmpleado' => $data->usuario["noEmpleado"], 
            'puesto' => $empleado->nombre_puesto,
            'departamento' => $empleado->nombre_area,
            'semanasCapturacion' => $data->semanasCapturacion,
        ];

        if ($data->tipo == "Permiso") {
           $pdf->loadView($viewReporte, $arrayData);
        }else {
            $pdf->loadView($viewReporte, $arrayData)->setPaper('A4', 'landscape');
        }
     
        Storage::put("TH/VP/Solicitud.pdf", $pdf->output());

        $result = [];
        $result['success'] = true; 
        return $result;
    }

    public function downloadReporte($name) {
       return Storage::download("TH/VP/".$name.".pdf");
    }

    public function getVacacionesPendientes($noEmpleado)
    {
        return VacacionesPendientes::find($noEmpleado);
    }

}
