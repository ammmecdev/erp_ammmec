<?php

namespace App\Http\Controllers\TalentoHumano\VP;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TalentoHumano\VP\Comentario;

class ComentarioController extends Controller
{
    public function getComentario($idSolicitud, $idEtapa) 
    {
        return Comentario::where('idSolicitud', $idSolicitud)->where('idEtapa', $idEtapa)->latest()->first();
    }

    public function store(Request $request)
    {
        $saved = true;
        $comentario = Comentario::where('idSolicitud', $request->idSolicitud)->where('idEtapa', $request->idEtapa)->first();
        if ($comentario && $request->comentarios == "") {
            $saved = $comentario->delete();  
        } 
        if($request->comentarios != ""){
            if (!$comentario) {
                $comentario = new Comentario;
                $comentario->idEtapa = $request->idEtapa;
                $comentario->idSolicitud = $request->idSolicitud;
            }
            $comentario->comentario = $request->comentarios;
            $saved = $comentario->save();  
        }
        $data = [];
        $data['success'] = $saved;
        return $data;
    }
}
