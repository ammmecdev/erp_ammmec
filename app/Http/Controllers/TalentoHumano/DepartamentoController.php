<?php

namespace App\Http\Controllers\TalentoHumano;

use App\Models\TalentoHumano\Departamento;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartamentoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getDepartamentos()
    {
       return Departamento::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $departamento = new Departamento;
        $departamento->nombre = $request->nombre;
        $departamento->codigo = $request->codigo;        
        $saved = $departamento->save();
        $data = [];
        $data['success'] = $saved;
        $data['departamento'] = $departamento;
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TalentoHumano\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Departamento $departamento)
    {
        $departamento->nombre = $request->nombre;
        $departamento->codigo = $request->codigo;
        $updated=$departamento->update();
        $data=[];
        $data['success']=$updated;
        $data['departamento']=$departamento;
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TalentoHumano\Departamento  $departamento
     * @return \Illuminate\Http\Response
     */
    public function destroy(Departamento $departamento)
    {
        $deleted = $departamento->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }
}
