<?php

namespace App\Http\Controllers\Finanzas;

use Illuminate\Http\Request;
use App\Models\Finanzas\CuentaContable;
use App\Http\Controllers\Controller;

class CuentaContableController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getCuentas()
    {
        return CuentaContable::all();
    }

    public function getCuentasByArea($area)
    {
        return CuentaContable::where('area', $area)->get();
    }

    public function getCuentasMayoresByArea($area)
    {
        $cuentas = CuentaContable::where('area', $area)->get();
        $cuentasMayores = $cuentas->unique('cuentaMayor');
        $cuentas = collect();
        foreach($cuentasMayores as $cuenta){
            $cuentas->push($cuenta);
        }
        return $cuentas;
    }

    public function getAreas()
    {
        $cuentas = CuentaContable::all();
        $cuentas = $cuentas->unique('area');
        $areas = collect();
        foreach($cuentas as $area){
            $areas->push($area);
        }
        return $areas;
    }

    public function store(Request $request)
    {
        $cuenta = new CuentaContable;
        $cuenta->area = $request->area;
        $cuenta->cuenta_mayor = $request->cuenta_mayor;
        $cuenta->subcuenta = $request->subcuenta;
        $cuenta->no_cuenta = $request->no_cuenta;

        $saved = $cuenta->save();

        $data = [];
        $data['saved'] = $saved;
        $data['cuenta'] = $cuenta;

        return $data;
    }

    public function edit(Request $request)
    {
        $cuenta = CuentaContable::find($request->idCuenta);
        $cuenta->area = $request->area;
        $cuenta->cuenta_mayor = $request->cuenta_mayor;
        $cuenta->subcuenta = $request->subcuenta;
        $cuenta->no_cuenta = $request->no_cuenta;

        $saved = $cuenta->update();

        $data = [];
        $data['saved'] = $saved;
        $data['cuenta'] = $cuenta;

        return $data;
    }

    public function delete($idCuenta)
    {
        $cuenta = CuentaContable::find($idCuenta);
        $deleted = $cuenta->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }


    public function getCuentasContablesUnique() {
        $cuentasC = CuentaContable::select('subcuenta')->get();
        $cuentasU = $cuentasC->unique('subcuenta');

        $cuentas = Collect();

        foreach ($cuentasU as $cuentaU) 
           $cuentas->push($cuentaU);

        return $cuentas;
    }
}
