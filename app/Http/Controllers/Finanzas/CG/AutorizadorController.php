<?php

namespace App\Http\Controllers\Finanzas\CG;

use Illuminate\Http\Request;
use App\Models\Finanzas\CG\Autorizador;
use App\Http\Controllers\Controller;

class AutorizadorController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function getAutorizadores() {
        return Autorizador::all();
    }

    public function store(Request $request) {
        $autorizador = new Autorizador;
        $autorizador->idUsuario = $request->idUsuario;
        $autorizador->disponible = 1;
        $saved = $autorizador->save();
        $data = [];
        $data['success'] = $saved;
        $data['autorizador'] = $autorizador;
        return $data;
    }

    public function update(Autorizador $autorizador, $status) {
        $autorizador->disponible = $status;
        $saved = $autorizador->save();
        $data = [];
        $data['success'] = $saved;
        $data['autorizador'] = $autorizador;
        return $data;
    }
}
