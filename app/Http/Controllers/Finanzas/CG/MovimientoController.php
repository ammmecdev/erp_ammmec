<?php

namespace App\Http\Controllers\Finanzas\CG;

use Illuminate\Http\Request;
use App\Usuario;
use App\Models\Finanzas\CG\Solicitud;
use App\Models\Finanzas\CG\Movimiento;
use App\Models\Finanzas\CG\Resumen;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MovimientoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getMovimientosBySolicitud(Solicitud $solicitud)
    {
        return $solicitud->movimientos;
    }

    public function getMovimientosByEmpleado($idUsuario, Request $request)
    {
        if ($request->formaPago == "Todos") {
            $movimientos = Movimiento::where('idUsuario', $idUsuario)
                ->whereDate('fecha', "<", $request->inicio)
                ->orderBy('fecha', 'ASC')
                ->get();
        }else {
            $movimientos = Movimiento::where('idUsuario', $idUsuario)
                ->where('formaPago', $request->formaPago)
                ->whereDate('fecha', "<", $request->inicio)
                ->orderBy('fecha', 'ASC')
                ->get();
        }
        
        $saldoInicial = 0;

        foreach($movimientos as $movimiento) {

            $accion = $movimiento->accion;
            $importe = $movimiento->importe;

            if($accion == 'dispersion' || $accion == 'reembolso')
                $saldoInicial += $importe;
            else if($accion == 'gasto' || $accion == 'decremento')
                $saldoInicial -= $importe;      
        }

        $movimientos = Movimiento::where('idUsuario', $idUsuario)
            ->where('formaPago', $request->formaPago)
            ->whereBetween('fecha', [$request->inicio, $request->fin])
            ->orderBy('fecha', 'ASC')
            ->get();

        foreach($movimientos as $movimiento) {
            $movimiento->solicitud = $movimiento->solicitud;
            if($movimiento->accion == 'gasto')
                $movimiento->comporobacion = $movimiento->comprobacion;
        }

        $data = [];
        $data['saldoInicial'] = $saldoInicial;
        $data['movimientos'] = $movimientos;
        
        return $data;
    }

    public function getMovimientosByCuenta($cuenta, Request $request) 
    {
        if ($request->formaPago == "Todos") {
            if ($cuenta == "Interna" || $cuenta == "Externa") {
                $movimientos = Movimiento::where('tipoCuenta', $cuenta)
                    ->whereBetween('fecha', [$request->inicio, $request->fin])
                    ->orderBy('fecha', 'ASC')
                    ->get(); 
            }else {
                $movimientos = Movimiento::where('cuenta', $cuenta)
                    ->whereBetween('fecha', [$request->inicio, $request->fin])
                    ->orderBy('fecha', 'ASC')
                    ->get(); 
            }
        }else {
            if ($cuenta == "Interna" || $cuenta == "Externa") {
                $movimientos = Movimiento::where('tipoCuenta', $cuenta)
                    ->where('formaPago', $request->formaPago)
                    ->whereBetween('fecha', [$request->inicio, $request->fin])
                    ->orderBy('fecha', 'ASC')
                    ->get();
            }else {
                $movimientos = Movimiento::where('cuenta', $cuenta)
                    ->where('formaPago', $request->formaPago)
                    ->whereBetween('fecha', [$request->inicio, $request->fin])
                    ->orderBy('fecha', 'ASC')
                    ->get();
            }
        }
        foreach($movimientos as $movimiento) {
            $movimiento->comporobacion = $movimiento->comprobacion;
            $movimiento->solicitante = $movimiento->usuario;
        }    
        return $movimientos;
    }

    public function save($idSolicitud, Request $request)
    {
        $mov = new Movimiento;
        $mov->idSolicitud = $idSolicitud;
        $mov->formaPago = $request->formaPago;
        $mov->importe = $request->importe;
        $mov->fecha = $request->fecha;
        $mov->accion = $request->accion;
        $mov->idUsuario = $request->idUsuario;
        $saved = $mov->save(); 
        $data = [];
        $data['success'] = $saved;
        $data['movimiento'] = $mov;
        return $data;
    }

    public function store(Request $request) {
        //REGISTRA EL SOLICITANTE
        $movimiento = new Movimiento;
        $movimiento->idSolicitud = $request->idSolicitud;
        $movimiento->idComprobacion = $request->idComprobacion;
        $movimiento->formaPago = $request->formaPago;
        $movimiento->importe = $request->importe;
        $movimiento->fecha = $request->fecha;
        $movimiento->accion = $request->accion;
        $movimiento->idUsuario = $request->idUsuario;
        $movimiento->tipoCuenta = $request->tipoCuenta;
        $movimiento->cuenta = $request->cuenta;
        $saved = $movimiento->save();
        $data = [];
        $data['success'] = $saved;
        return $data;
    }

    public function update($idComprobacion, Request $request) {
        $movimiento = Movimiento::where('idComprobacion', $idComprobacion)->first();
        $movimiento->formaPago = $request->formaPago;
        $movimiento->importe = $request->importe;
        $movimiento->fecha = $request->fecha;
        $movimiento->accion = $request->accion;
        $saved = $movimiento->save();
        $data = [];
        $data['success'] = $saved;
        return $data;
    }

    public function updateMovDispersion(Movimiento $movimiento, Request $request) {
        $movimiento->formaPago = $request->formaPago;
        $movimiento->importe = $request->importe;
        $movimiento->fecha = $request->fecha;
        $saved = $movimiento->save();
        $data = [];
        $data['success'] = $saved;
        return $data;
    }

    public function getEmpleados() {
        $movimientos = Movimiento::all();
        $movimientos = $movimientos->unique('idUsuario');
        $empleados = collect();
        foreach($movimientos as $movimiento){
            $empleados->push($movimiento->usuario);
        }
        return $empleados;
    }

      public function getCuentas($tipoCuenta) {
        $movimientos = Movimiento::where('tipoCuenta', $tipoCuenta)->get();
        $movimientos = $movimientos->unique('cuenta');
        $cuentas = collect();
        foreach($movimientos as $movimiento) {
            $cuentas->push($movimiento);
        }
        return $cuentas;
    }

    public function destroy(Movimiento $movimiento)
    {
        $resumen = Resumen::where('idSolicitud', $movimiento->idSolicitud)
            ->where('formaPago', $movimiento->formaPago)
            ->first(); 

        if ($resumen) {
            if ($movimiento->accion == "dispersion") {
                $resumen->dispersado -= $movimiento->importe;
            }else if ($movimiento->accion == "reembolso") {
                $resumen->reembolsado -= $movimiento->importe;
            }else if ($movimiento->accion == "decremento") {
                $resumen->decrementado -= $movimiento->importe;
            }

            $updated = $resumen->update();
            
            if ($updated) {
                $deleted = $movimiento->delete();
                $data = [];
                $data['success'] = $deleted;
                return $data; 
            }else {
                $data = [];
                $data['success'] = $updated;
                return $data; 
            }
        }else {
            $data = [];
            $data['success'] = false;
            return $data; 
        }    
    }
}