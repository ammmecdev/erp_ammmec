<?php

namespace App\Http\Controllers\Finanzas\CG;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Finanzas\CG\Comprobacion;
use App\Models\Finanzas\CG\Resumen;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ComprobacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getComprobaciones($idSolicitud)
    {
        return Comprobacion::where('idSolicitud', $idSolicitud)->get();
    }

    public function store(Request $request)
    {
        $comprobacion = new Comprobacion;
        $comprobacion->idSolicitud = $request->idSolicitud;
        $comprobacion->fechaGasto = $request->fechaGasto;
        $comprobacion->tipoComprobante = $request->tipoComprobante;
        $comprobacion->folioComprobante = $request->folioComprobante;
        $comprobacion->proveedor = $request->proveedor;
        $comprobacion->descripcion = $request->descripcion;
        $comprobacion->noCuenta = $request->noCuenta;
        $comprobacion->tipoCuenta = $request->tipoCuenta;
        $comprobacion->cuenta = $request->cuenta;
        $comprobacion->tipoGasto = $request->formaPago;
        $comprobacion->subtotal = $request->subtotal;
        $comprobacion->iva = $request->iva;
        $comprobacion->otros = $request->otros;
        $comprobacion->otro = $request->otro;
        $comprobacion->importe = $request->importe;
        $comprobacion->revision = 0;

        if ($request->file('file')) {
            $file = $request->file('file');
            $path = $file->storeAs("FN/CG/Comprobaciones", $file->getClientOriginalName());
            $comprobacion->ruta = "/storage/".$path;
            $comprobacion->file = $file->getClientOriginalName();
        }else {
            $comprobacion->file = "S/A";
        }

        if ($request->file('fileXML')) {
            $fileXML = $request->file('fileXML');
            $path = $fileXML->storeAs("FN/CG/Comprobaciones", $fileXML->getClientOriginalName());
            $comprobacion->rutaXML = "/storage/".$path;
            $comprobacion->fileXML = $fileXML->getClientOriginalName();
        }else {
            $comprobacion->fileXML = "S/A";
        }

        $data = [];
        $saved = $comprobacion->save();
        
        if ($saved && !filter_var($request->propinaEnFactura, FILTER_VALIDATE_BOOLEAN)) {  
            $comprobacionPropina = new Comprobacion;
            $comprobacionPropina->idSolicitud = $request->idSolicitud;
            $comprobacionPropina->fechaGasto = $request->fechaGasto;
            $comprobacionPropina->tipoComprobante = "Ticket";
            $comprobacionPropina->folioComprobante = $request->folioComprobante;
            $comprobacionPropina->proveedor = $request->proveedor;
            $comprobacionPropina->descripcion = $request->otro;
            $comprobacionPropina->noCuenta = $request->noCuenta;
            $comprobacionPropina->tipoCuenta = $request->tipoCuenta;
            $comprobacionPropina->cuenta = $request->cuenta;
            $comprobacionPropina->tipoGasto = $request->formaPagoPropina;
            $comprobacionPropina->subtotal = $request->propina;
            $comprobacionPropina->iva = 0;
            $comprobacionPropina->otros = 0;
            $comprobacionPropina->importe = $request->propina;
            $comprobacionPropina->file = "S/A";
            $comprobacionPropina->fileXML = "S/A";
            $comprobacionPropina->otro = $request->otro;
            $savedPropina = $comprobacionPropina->save();
            $data['successPropina'] = $savedPropina;
            $data['comprobacionPropina'] = $comprobacionPropina;
        }else {
            $data['successPropina'] = false; 
        }

        $data['success'] = $saved;
        $data['comprobacion'] = $comprobacion;
        return $data;
    }

    public function update(Request $request)
    {
        $comprobacion = Comprobacion::find($request->idComprobacion);
        $comprobacion->fechaGasto = $request->fechaGasto;
        $comprobacion->tipoComprobante = $request->tipoComprobante;
        $comprobacion->folioComprobante = $request->folioComprobante;
        $comprobacion->proveedor = $request->proveedor;
        $comprobacion->descripcion = $request->descripcion;
        $comprobacion->noCuenta = $request->noCuenta;
        $comprobacion->tipoCuenta = $request->tipoCuenta;
        $comprobacion->cuenta = $request->cuenta;
        $comprobacion->tipoGasto = $request->formaPago;
        $comprobacion->subtotal = $request->subtotal;
        $comprobacion->iva = $request->iva;
        $comprobacion->otros = $request->otros;
        $comprobacion->importe = $request->importe;
        if ($request->file('file')) {
            $coincidencias = Comprobacion::where('file', $comprobacion->file)->count();
            if ($coincidencias == 1) {
                Storage::delete('FN/CG/Comprobaciones/'.$comprobacion->file);
            }
            $path = $request->file('file')->storeAs("FN/CG/Comprobaciones", $request->file->getClientOriginalName());            
            $comprobacion->file = $request->file->getClientOriginalName();
            $comprobacion->ruta = "/storage/".$path;
        }
        $saved = $comprobacion->save();
        $data = [];
        $data['success'] = $saved;
        $data['comprobacion'] = $comprobacion;

        return $data;
    }

    public function updateFiles(Request $request)
    {
        $comprobacion = Comprobacion::find($request->idComprobacion);

        if ($request->file('file')) {
            $coincidencias = Comprobacion::where('file', $comprobacion->file)->count();
            if ($coincidencias == 1) {
                Storage::delete('FN/CG/Comprobaciones/'.$comprobacion->file);
            }
            $path = $request->file('file')->storeAs("FN/CG/Comprobaciones", $request->file->getClientOriginalName());            
            $comprobacion->file = $request->file->getClientOriginalName();
            $comprobacion->ruta = "/storage/".$path;
        }

        if ($request->file('fileXML')) {
            $coincidencias = Comprobacion::where('fileXML', $comprobacion->fileXML)->count();
            if ($coincidencias == 1) {
                Storage::delete('FN/CG/Comprobaciones/'.$comprobacion->fileXML);
            }
            $path = $request->file('fileXML')->storeAs("FN/CG/Comprobaciones", $request->fileXML->getClientOriginalName());            
            $comprobacion->fileXML = $request->fileXML->getClientOriginalName();
            $comprobacion->rutaXML = "/storage/".$path;
        }

        $saved = $comprobacion->save();
        $data = [];
        $data['success'] = $saved;
        $data['comprobacion'] = $comprobacion;
        return $data;
    }

    public function setRevision(Request $request)
    {
        $comprobacion = Comprobacion::find($request->idComprobacion);
        $comprobacion->revision = $request->revision;
        $saved = $comprobacion->save();
        $data = [];
        $data['success'] = $saved;
        $data['comprobacion'] = $comprobacion;
        return $data;
    }

    public function delete(Comprobacion $comprobacion)
    {
        $this->deleteFilesComprobacion($comprobacion);
        $this->movimientosResumenComprobacion($comprobacion, "delete");
        $deleted = $comprobacion->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function movimientosResumenComprobacion($comprobacion, $accion)
    {       
        $resumen = Resumen::where("formaPago", $comprobacion->tipoGasto)
                ->where("idSolicitud", $comprobacion->idSolicitud)
                ->first();

        if ($accion == "delete") {
            if ($resumen) {
               $resumen->comprobado -= $comprobacion->importe;
               $resumen->save();
            }
        }
    }

    public function deleteFilesComprobacion($comprobacion)
    {
        $coincidenciasFile = Comprobacion::where('file', $comprobacion->file)->count();
        if ($coincidenciasFile == 1) {
            Storage::delete('FN/CG/Comprobaciones/'.$comprobacion->file);
        }
        $coincidenciasXML = Comprobacion::where('fileXML', $comprobacion->fileXML)->count();
        if ($coincidenciasXML == 1) {
            Storage::delete('FN/CG/Comprobaciones/'.$comprobacion->fileXML);
        }
    }

    public function downloadFile(Request $request)
    {
        return Storage::download("FN/CG/Comprobaciones/".$request->file);
    }

    public function getNoDeducibles(Request $request)
    {
        $comprobaciones = DB::table('fn_cg_comprobaciones')
        ->join('fn_cg_solicitudes', 'fn_cg_comprobaciones.idSolicitud', '=', 'fn_cg_solicitudes.idSolicitud')
        ->join('usuarios', 'fn_cg_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
        ->select(
            'fn_cg_comprobaciones.*', 
            'fn_cg_solicitudes.fechaComprobacion as fecha_comprobacion',
            'usuarios.nombreUsuario as nombre_usuario')
        ->where('fn_cg_comprobaciones.tipoComprobante', '<>', 'Factura')
        ->whereBetween('fn_cg_solicitudes.fechaComprobacion', [$request->inicio, $request->fin])
        ->get();

        $comp_usuarios = $comprobaciones->unique('nombre_usuario');
        $comp_cuenta = $comprobaciones->unique('cuenta');

        $cuentas_internas = collect();
        $cuentas_externas = collect();
        $usuarios = collect();

        $usuario = array();
        $ci = array();
        $ce = array();

        foreach($comp_usuarios as $item){
            if($item->nombre_usuario != "") {
                $usuario['usuario'] = $item->nombre_usuario;
                $usuarios = $usuarios->concat($usuario);
            }
        }
        foreach($comp_cuenta as $item) {
            if($item->cuenta != "") {
                if ($item->tipoCuenta === "Interna") {
                    $ci['cuenta_interna'] = $item->cuenta;
                    $cuentas_internas = $cuentas_internas->concat($ci);
                }else {
                    $ce['cuenta_externa'] = $item->cuenta;
                    $cuentas_externas = $cuentas_externas->concat($ce);
                }
            }
        }
        $data = [
            "empleados" => $usuarios,
            "cuentas_externas" => $cuentas_externas,
            "cuentas_internas" => $cuentas_internas,
            "no_deducibles" => $comprobaciones,
        ];
        return $data;
    }

    public function getComprobacionesAll(Request $request)
    {
        $comprobaciones = DB::table('fn_cg_comprobaciones')
        ->join('fn_cg_solicitudes', 'fn_cg_comprobaciones.idSolicitud', '=', 'fn_cg_solicitudes.idSolicitud')
        ->select(
            'fn_cg_comprobaciones.*', 
            'fn_cg_solicitudes.fechaComprobacion as fecha_comprobacion')
        ->whereBetween('fn_cg_solicitudes.fechaComprobacion', [$request->inicio, $request->fin])
        ->get();

        $comp_cuenta = $comprobaciones->unique('cuenta');
        $cuentas_internas = collect();
        $cuentas_externas = collect();
        $ci = array();
        $ce = array();

        foreach($comp_cuenta as $item) {
            if($item->cuenta != "") {
                if ($item->tipoCuenta === "Interna") {
                    $ci['cuenta_interna'] = $item->cuenta;
                    $cuentas_internas = $cuentas_internas->concat($ci);
                }else {
                    $ce['cuenta_externa'] = $item->cuenta;
                    $cuentas_externas = $cuentas_externas->concat($ce);
                }
            }
        }
        $data = [
            "cuentas_internas" => $cuentas_internas,
            "cuentas_externas" => $cuentas_externas,
            "comprobaciones" => $comprobaciones,
        ];
        return $data;
    }
}
