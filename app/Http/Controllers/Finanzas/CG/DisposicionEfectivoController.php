<?php

namespace App\Http\Controllers\Finanzas\CG;

use App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Finanzas\CG\DisposicionEfectivo;
use App\Models\Finanzas\CG\Resumen;
use App\Http\Controllers\Controller;


class DisposicionEfectivoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getDisposicionesEfectivo($idSolicitud)
    {
        return DisposicionEfectivo::where('idSolicitud', $idSolicitud)->get();
    }

    public function store(Request $request)
    {
        $disposicion = new DisposicionEfectivo;
        $disposicion->idSolicitud = $request->idSolicitud;
        $disposicion->fecha = $request->fecha;
        $disposicion->formaPago = $request->formaPago;
        $disposicion->importe = $request->importe;
        $disposicion->comision = $request->comision;

        if ($request->file('file')) {
            $file = $request->file('file');
            $path = $file->storeAs("FN/CG/Disposiciones", $file->getClientOriginalName());
            $disposicion->ruta = "/storage/".$path;
            $disposicion->file = $file->getClientOriginalName();
        }else {
            $disposicion->file = null;
            $disposicion->ruta = null;
        }

        $saved = $disposicion->save();
        if ($saved) {
            $this->movimientosResumenDispersion($disposicion, "store", $disposicion->importe);
        }
        $data = [];
        $data['success'] = $saved;
        $data['disposicion'] = $disposicion;
        return $data;
    }

    public function movimientosResumenDispersion($disposicion, $accion, $importe)
    {
        if ($disposicion->formaPago == "Tarjeta Banbajio") {
            $formaPagoDsp = "Banbajio Efectivo";
        }else if ($disposicion->formaPago == "Tarjeta Personal") {
            $formaPagoDsp = "TP Efectivo";
        }

        $resumenOgn = Resumen::where("formaPago", $disposicion->formaPago)
            ->where("idSolicitud", $disposicion->idSolicitud)
            ->first();

        $resumenDps = Resumen::where("formaPago", $formaPagoDsp)
            ->where("idSolicitud", $disposicion->idSolicitud)
            ->first();

        if ($accion == "store") {
            // Se agrega la disposición
            if ($resumenDps) {
               $resumenDps->dispersado += $disposicion->importe;
            }else {
                $resumenDps = new Resumen;
                $resumenDps->idSolicitud = $disposicion->idSolicitud;
                $resumenDps->formaPago = $formaPagoDsp;
                $resumenDps->solicitado = 0;
                $resumenDps->dispersado = $disposicion->importe;
                $resumenDps->comprobado = 0;
                $resumenDps->reembolsado = 0;
                $resumenDps->decrementado = 0;
            }
            $saved = $resumenDps->save();
            // disminuir de lo dispersado
            if ($saved) {
               $resumenOgn->dispersado -= $disposicion->importe;
               $resumenOgn->save();
            }
        }else if ($accion == "update") {
            // Disminuir de la disposición
            $resumenDps->dispersado -= $disposicion->importe;
            $resumenDps->dispersado += $importe;
            $saved = $resumenDps->save();

            if ($saved) {
                // disminuir de lo dispersado
                $resumenOgn->dispersado += $disposicion->importe;
                $resumenOgn->dispersado -= $importe;
                $resumenOgn->save();
            }
        }else {
            $resumenDps->dispersado -= $disposicion->importe;
            $saved = $resumenDps->save();
            if ($saved) {
                $resumenOgn->dispersado += $disposicion->importe;
                $resumenOgn->save();
            }
        }
        $this->removeResumenEmpty($disposicion->idSolicitud);
    }

    public function update(Request $request)
    {
        $disposicion = DisposicionEfectivo::find($request->idDisposicion);
        $this->movimientosResumenDispersion($disposicion, "update", $request->importe);
        $disposicion->fecha = $request->fecha;
        $disposicion->importe = $request->importe;
        $disposicion->comision = $request->comision;

        if ($request->file('file')) {
            $coincidencias = DisposicionEfectivo::where('file', $disposicion->file)->count();
            if ($coincidencias == 1) {
                Storage::delete("FN/CG/Disposiciones/".$disposicion->file);
            }
            $path = $request->file('file')->storeAs("FN/CG/Disposiciones", $request->file->getClientOriginalName());            
            $disposicion->file = $request->file->getClientOriginalName();
            $disposicion->ruta = "/storage/".$path;
        }

        $saved = $disposicion->save();
        $data = [];
        $data['success'] = $saved;
        $data['disposicion'] = $disposicion;
        return $data;
    }

    public function delete(DisposicionEfectivo $disposicion)
    {
        $this->movimientosResumenDispersion($disposicion, "delete", $disposicion->importe);
        
        if ($disposicion->file) {
            $coincidencias = DisposicionEfectivo::where('file', $disposicion->file)->count();
            if ($coincidencias == 1) {
                Storage::delete("FN/CG/Disposiciones/".$disposicion->file);
            }
        }
        $deleted = $disposicion->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function removeResumenEmpty($idSolicitud)
    {
        $resumen = Resumen::where("idSolicitud", $idSolicitud)->get();
        foreach($resumen as $importe) {
            if(
                $importe->solicitado == 0 && 
                $importe->dispersado == 0 && 
                $importe->comprobado == 0 && 
                $importe->reembolsado == 0 && 
                $importe->decrementado == 0
            )
                $importe->delete();
        }
    }

    public function downloadComprobante(Request $request)
    {
        return Storage::download("FN/CG/Disposiciones/".$request->file);
    }
}
