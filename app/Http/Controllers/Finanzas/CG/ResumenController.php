<?php

namespace App\Http\Controllers\Finanzas\CG;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Finanzas\CG\Resumen;
use App\Models\Finanzas\CG\Movimiento;
use App\Models\Finanzas\CG\Solicitud;
use App\Models\Finanzas\CG\Comprobacion;
use App\Http\Controllers\Controller;
use App\Exports\ResumenSolicitudesExport;
use PHPUnit\Framework\Constraint\IsTrue;

class ResumenController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getResumen($idSolicitud)
    {
        return Resumen::where('idSolicitud', $idSolicitud)->get();
    }

    public function store($idSolicitud, Request $request)
    {
        // Se utiliza en nueva solicitud
        foreach($request->resumen as $resumen) {
            $importe = new Resumen;
            $importe->idSolicitud = $idSolicitud;
            $importe->formaPago = $resumen["formaPago"];
            $importe->solicitado = $resumen["solicitado"];
            $importe->dispersado = 0;
            $importe->comprobado = 0;
            $importe->reembolsado = 0;
            $importe->decrementado = 0;
            $importe->save();  
        }
        $data = [];
        $data['success'] = true;
        return $data;
    }

    public function dispersionSolicitud($idSolicitud, Request $request)
    {
        $resumen = Resumen::where("idSolicitud", $idSolicitud)
            ->where("formaPago", $request->formaPago)
            ->first();

        if ($resumen) {
            $resumen->dispersado = $resumen->dispersado + $request->importe;
        }else {
            $resumen = new Resumen;
            $resumen->idSolicitud = $idSolicitud;
            $resumen->formaPago = $request->formaPago;
            $resumen->solicitado = 0;
            $resumen->dispersado = $request->importe;
            $resumen->comprobado = 0;
            $resumen->reembolsado = 0;
            $resumen->decrementado = 0;  
        }
        $saved = $resumen->save();
        $this->removeResumenEmpty($idSolicitud);
        // $this->timbradoSolicitud($solicitud->idSolicitud);
        $data = [];
        $data['success'] = $saved;
        $data['resumen'] = $resumen;
        return $data;
    }

    public function updateDispersionSolicitud($idSolicitud, Request $request)
    {
        $resumen = Resumen::where("idSolicitud", $request->movimiento["idSolicitud"])
            ->where("formaPago", $request->movimiento["formaPago"])
            ->first();

        if ($resumen) {
            $resumen->dispersado = 
                ($resumen->dispersado - $request->movimiento["importe"]) + $request->importe;
            $saved = $resumen->save();
            $this->removeResumenEmpty($idSolicitud);
            $data = [];
            $data['success'] = $saved;
            return $data;
        }else {
            $data = [];
            $data['success'] = false;
            return $data;
        }
    }

    public function removeResumenEmpty($idSolicitud)
    {
        $resumen = Resumen::where("idSolicitud", $idSolicitud)->get();
        foreach($resumen as $importe) {
            if(
                $importe->solicitado == 0 && 
                $importe->dispersado == 0 && 
                $importe->comprobado == 0 && 
                $importe->reembolsado == 0 && 
                $importe->decrementado == 0
            )
                $importe->delete();
        }
    }

    public function comprobacionSolicitud(Request $request)
    {   
        $resumen = Resumen::where('idSolicitud', $request->idSolicitud)
            ->where('formaPago', $request->formaPago)
            ->first();

        if ($resumen) {
            $resumen->comprobado += $request->importe;
        }else {
            $resumen = new Resumen;
            $resumen->idSolicitud = $request->idSolicitud;
            $resumen->formaPago = $request->formaPago;
            $resumen->solicitado = 0;
            $resumen->dispersado = 0;
            $resumen->comprobado = $request->importe;
            $resumen->reembolsado = 0;
            $resumen->decrementado = 0;
        }
        $saved = $resumen->save();
        $this->removeResumenEmpty($request->idSolicitud);
        $data = [];
        $data['success'] = $saved;
        return $data;
    }

    public function updateComprobacionSolicitud(Request $request)
    {   
        $resumen = Resumen::where('idSolicitud', $request->idSolicitud)->get();
        $comprobaciones = Comprobacion::where('idSolicitud', $request->idSolicitud)->get();
        $formaPagoUnique = $comprobaciones->unique('tipoGasto');
        if ($resumen && $formaPagoUnique) {
            foreach($resumen as $importe) {
                $importe->comprobado = 0;
                $importe->update();
            }
            foreach($formaPagoUnique as $item) {
                $comprobacionesByFormaPago = $comprobaciones->where('tipoGasto', $item->tipoGasto);
                $totalComprobado = $comprobacionesByFormaPago->sum('importe');
                $resumenByFormaPago = $resumen->where('formaPago', $item->tipoGasto)->first();

                if($resumenByFormaPago != null) { 
                    $resumenByFormaPago->comprobado = $totalComprobado;
                    $resumenByFormaPago->save();
                }else {
                    $importe = new Resumen;
                    $importe->idSolicitud = $request->idSolicitud;
                    $importe->formaPago = $item->tipoGasto;
                    $importe->solicitado = 0;
                    $importe->dispersado = 0;
                    $importe->comprobado = $totalComprobado;
                    $importe->reembolsado = 0;
                    $importe->decrementado = 0;
                    $saved = $importe->save();
                }
            }
        }
        $this->removeResumenEmpty($request->idSolicitud);
        $data = [];
        $data['success'] = true;
        return $data; 
    }

    public function reembolsoDecremento($accion, $idSolicitud, Request $request)
    {
        $resumen = Resumen::where("idSolicitud", $idSolicitud)
            ->where("formaPago", $request->formaPago)
            ->first();
            
        if ($resumen) {
            if ($accion == "reembolsado")
                $resumen->reembolsado += $request->importe;
            else 
                $resumen->decrementado += $request->importe;
        }else {
            $resumen = new Resumen;
            $resumen->idSolicitud = $idSolicitud;
            $resumen->formaPago = $request->formaPago;
            $resumen->solicitado = 0;
            $resumen->dispersado = 0;
            $resumen->comprobado = 0;
            if ($accion == "reembolsado") {
                $resumen->reembolsado = $request->importe;
                $resumen->decrementado = 0;  
            }else {
                $resumen->reembolsado = 0;
                $resumen->decrementado = $request->importe; 
            }
        }
        $saved = $resumen->save();
        $this->removeResumenEmpty($idSolicitud);
        $data = [];
        $data['success'] = $saved;
        $data['resumen'] = $resumen;
        return $data; 
    }

    public function updateReembolsoDecremento($accion, $idSolicitud, Request $request)
    {
        $resumen = Resumen::where("idSolicitud", $request->movimiento["idSolicitud"])
            ->where("formaPago", $request->movimiento["formaPago"])
            ->first();

        if ($resumen) {
            if ($accion == "reembolsado") {
                $resumen->reembolsado -= $request->movimiento["importe"];
                $resumen->reembolsado += $request->importe;
            }else {
                $resumen->decrementado -= $request->movimiento["importe"];
                $resumen->decrementado += $request->importe;   
            }
            $saved = $resumen->save();
            $this->removeResumenEmpty($idSolicitud);
            $data = [];
            $data['success'] = $saved;
            return $data;
        }else {
            $data = [];
            $data['success'] = false;
            return $data;
        }
    }

    public function exportResumenExcel()
    {
        $fechaInicio = $_GET['fechaInicio'];
        $fechaFin = $_GET['fechaFin'];
        $nombre = "Reporte de solicitudes.xlsx";
        if ($fechaInicio != "null" && $fechaFin != "null") {
            $resumen = DB::table('fn_cg_resumenes')
                ->join('fn_cg_solicitudes', 'fn_cg_resumenes.idSolicitud', '=', 'fn_cg_solicitudes.idSolicitud')
                ->join('usuarios', 'fn_cg_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
                ->join('fn_cg_etapas', 'fn_cg_solicitudes.idEtapa', '=', 'fn_cg_etapas.idEtapa')
                ->select(
                    'fn_cg_resumenes.*',
                    'fn_cg_solicitudes.unidad_negocio',
                    'fn_cg_etapas.nombre as etapa',
                    DB::raw("ELT(MONTH(fn_cg_solicitudes.created_at), 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') as mesSolicitud"),
                    DB::raw("ELT(MONTH(fn_cg_solicitudes.fechaComprobacion), 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') as mesComprobacion"),
                    DB::raw("ELT(MONTH(fn_cg_solicitudes.fechaContabilizacion), 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') as mesContabilizacion"),
                    DB::raw("YEAR(fn_cg_solicitudes.created_at) as yearSolicitud"),
                    'usuarios.nombreUsuario'
                )
                ->whereBetween('fn_cg_solicitudes.created_at', [$fechaInicio, $fechaFin])
                ->get();
        }else {
            $resumen = DB::table('fn_cg_resumenes')
                ->join('fn_cg_solicitudes', 'fn_cg_resumenes.idSolicitud', '=', 'fn_cg_solicitudes.idSolicitud')
                ->join('usuarios', 'fn_cg_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
                ->join('fn_cg_etapas', 'fn_cg_solicitudes.idEtapa', '=', 'fn_cg_etapas.idEtapa')
                ->select(
                    'fn_cg_resumenes.*',
                    'fn_cg_solicitudes.unidad_negocio',
                    'fn_cg_etapas.nombre as etapa',
                    DB::raw("ELT(MONTH(fn_cg_solicitudes.created_at), 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') as mesSolicitud"),
                    DB::raw("ELT(MONTH(fn_cg_solicitudes.fechaComprobacion), 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') as mesComprobacion"),
                    DB::raw("ELT(MONTH(fn_cg_solicitudes.fechaContabilizacion), 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre') as mesContabilizacion"),
                    DB::raw("YEAR(fn_cg_solicitudes.created_at) as yearSolicitud"),
                    'usuarios.nombreUsuario'
                )
                ->get();
        }       
        return (new ResumenSolicitudesExport($resumen))->download($nombre);
    }

    public function update($idSolicitud, Request $request)
    {
        $resumenOld = Resumen::where("idSolicitud", $idSolicitud)->get();
        $formaPagoUnique = $resumenOld->unique('formaPago');
        if ($resumenOld) {
            foreach($resumenOld as $importe) {
                $importe->solicitado = 0;
                $importe->save();
            }
        }
        foreach($request->resumen as $resumen) {
            $importe = $resumenOld->where("formaPago", $resumen["formaPago"])->first();
            if (!$importe) {
                $importe = new Resumen;
                $importe->idSolicitud = $idSolicitud;
                $importe->formaPago = $resumen["formaPago"];
                $importe->dispersado = 0;
                $importe->comprobado = 0;
                $importe->reembolsado = 0;
                $importe->decrementado = 0;
            }
            $importe->solicitado = $resumen["solicitado"];
            $importe->save();  
        }
        $this->removeResumenEmpty($idSolicitud);
        $data = [];
        $data['success'] = true;
        return $data;
    }

    // public function comprobacionSolicitud($idSolicitud)
    // {
    //     $comprobaciones = Comprobacion::where('idSolicitud', $idSolicitud)->get();
    //     $resumen = Resumen::where('idSolicitud', $idSolicitud)->get();
    //     $tiposGasto = $comprobaciones->unique('tipoGasto');

    //     foreach($resumen as $importe) {
    //         $importe->comprobado = 0;
    //         $importe->save();
    //     }
    //     foreach ($tiposGasto as $tipoGasto) {
    //         $comprobacionesByTipo = $comprobaciones->where('tipoGasto', $tipoGasto->tipoGasto);
    //         $totalComprobado = $comprobacionesByTipo->sum('importe');
    //         $resumenByTipo = $resumen->where('formaPago', $tipoGasto->tipoGasto)->first();
    //         if($resumenByTipo != null) { 
    //             $resumenByTipo->comprobado = $totalComprobado;
    //             $resumenByTipo->save();
    //         } else {
    //             $importe = new Resumen;
    //             $importe->idSolicitud = $tipoGasto->idSolicitud;
    //             $importe->formaPago = $tipoGasto->tipoGasto;
    //             $importe->solicitado = 0;
    //             $importe->dispersado = 0;
    //             $importe->comprobado = $totalComprobado;
    //             $importe->reembolsado = 0;
    //             $importe->decrementado = 0;
    //             $saved = $importe->save();
    //         }
    //     }
    //     $this->removeResumenEmpty($idSolicitud);
    //     $data = [];
    //     $data['success'] = true;
    //     return $data;    
    // }

    // public function timbradoSolicitud($idSolicitud)
    // {
    //     $resumen = Resumen::where('idSolicitud', $idSolicitud)->get();
    //     $tarjetaPersonal = false;
    //     foreach($resumen as $tipoGasto) {
    //         if($tipoGasto->formaPago == 'Tarjeta Personal')
    //             $tarjetaPersonal = true;
    //     }
    //     $solicitud = Solicitud::find($idSolicitud);
    //     if($tarjetaPersonal == true) {
    //         $solicitud->timbrar = 1;
    //         $solicitud->save();
    //     } else {
    //         $solicitud->timbrar = 0;
    //         $solicitud->save();
    //     }
    // }
}
