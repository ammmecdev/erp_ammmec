<?php

namespace App\Http\Controllers\Finanzas\CG;

use App;
use App\User;
use App\Models\TalentoHumano\Empleado;
use Illuminate\Http\Request;
use App\Models\Cadmin\Role;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Finanzas\CG\Resumen;
use App\Models\Finanzas\CG\Comentario;
use App\Models\Finanzas\CG\Solicitud;
use App\Models\Finanzas\CG\Autorizacion;
use App\Http\Controllers\Finanzas\CG;
use App\Models\Finanzas\CG\Comprobacion;
use App\Models\Finanzas\CG\Bitacora;
use App\Models\Finanzas\CG\Archivo;
use App\Models\Finanzas\CG\PlaneacionGasto;
use App\Notifications\Finanzas\CG\AutorizacionSolicitud;
use App\Notifications\Finanzas\CG\AsignacionSolicitud;
use App\Notifications\Finanzas\CG\SolicitudAutorizada;
use App\Notifications\Finanzas\CG\SolicitudDispersada;
use App\Notifications\Finanzas\CG\SolicitudRechazada;
use Illuminate\Support\Facades\Storage;

class SolicitudController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getSolicitud(Solicitud $solicitud)
    {
        $solicitud->usuario = $solicitud->usuario;
        return $solicitud;
    }

    public function getSolicitudesAll()
    {
        $solicitudes = DB::table('fn_cg_solicitudes')
            ->join('usuarios', 'fn_cg_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
            ->join('usuarios as autorizadores', 'fn_cg_solicitudes.idAutorizador', '=', 'autorizadores.idUsuario')
            ->join('fn_cg_etapas', 'fn_cg_solicitudes.idEtapa', '=', 'fn_cg_etapas.idEtapa')
            ->select(
                'fn_cg_solicitudes.*',
                'fn_cg_etapas.nombre as nombre_etapa',
                'usuarios.nombreUsuario as nombre_usuario',
                'usuarios.ruta as ruta_usuario',
                'autorizadores.nombreUsuario as nombre_autorizador'
            )
            ->where('fn_cg_solicitudes.idEtapa', '!=', '10')
            ->orderBy('fn_cg_solicitudes.created_at', 'ASC')
            ->get();

        return $solicitudes;
    }

    public function getSolicitudesByEmpleado($noEmpleado)
    {
        return Solicitud::where('noEmpleado', $noEmpleado)->get();
    }

    public function getSolicitudesByUsuario($idUsuario)
    {
        $solicitudes = DB::table('fn_cg_solicitudes')
            ->join('usuarios', 'fn_cg_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
            ->join('usuarios as autorizadores', 'fn_cg_solicitudes.idAutorizador', '=', 'autorizadores.idUsuario')
            ->join('fn_cg_etapas', 'fn_cg_solicitudes.idEtapa', '=', 'fn_cg_etapas.idEtapa')
            ->select(
                'fn_cg_solicitudes.*',
                'fn_cg_etapas.nombre as nombre_etapa',
                'usuarios.nombreUsuario as nombre_usuario',
                'usuarios.ruta as ruta_usuario',
                'autorizadores.nombreUsuario as nombre_autorizador'
            )
            ->where('fn_cg_solicitudes.idUsuario', $idUsuario)
            ->where('fn_cg_solicitudes.idEtapa', '!=', '10')
            ->orderBy('fn_cg_solicitudes.created_at', 'ASC')
            ->get();

        return $solicitudes;
    }

    public function getSolicitudesByAutorizador($idAutorizador)
    {

        $solicitudes = DB::table('fn_cg_solicitudes')
            ->join('usuarios', 'fn_cg_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
            ->join('usuarios as autorizadores', 'fn_cg_solicitudes.idAutorizador', '=', 'autorizadores.idUsuario')
            ->join('fn_cg_etapas', 'fn_cg_solicitudes.idEtapa', '=', 'fn_cg_etapas.idEtapa')
            ->select(
                'fn_cg_solicitudes.*',
                'fn_cg_etapas.nombre as nombre_etapa',
                'usuarios.nombreUsuario as nombre_usuario',
                'usuarios.ruta as ruta_usuario',
                'autorizadores.nombreUsuario as nombre_autorizador'
            )
            ->where('fn_cg_solicitudes.aceptacion',  1)
            ->where('fn_cg_solicitudes.idEtapa', 2)
            ->orWhere('fn_cg_solicitudes.idEtapa', 6)
            ->orderBy('fn_cg_solicitudes.created_at', 'ASC')
            ->get();

        return $solicitudes;
    }

    public function store(Request $request)
    {
        $solicitud = new Solicitud;
        $solicitud->idUsuario = $request->idUsuario;
        $solicitud->destinoGasto = $request->destinoGasto;
        $solicitud->objetivoGasto = $request->objetivoGasto;
        $solicitud->tipoCuenta = $request->tipoCuenta;
        $solicitud->cuenta = $request->cuenta;
        $solicitud->idAsignador = $request->idAsignador;
        $solicitud->fechaInicioGasto = $request->fechaInicioGasto;
        $solicitud->fechaFinGasto = $request->fechaFinGasto;
        $solicitud->numeroPersonas = $request->numeroPersonas;
        $solicitud->tipoMoneda = $request->tipoMoneda;

        if ($request->tarjeta) {
            $solicitud->tarjeta = $request->tarjeta;
        }else $solicitud->tarjeta = "Sin definir";
        
        if($request->importe_total > 0) {
            $solicitud->idEtapa = 2;
            $solicitud->idEtapaOld = 1;
            $solicitud->etapaInicial = 2;
        } else {
            $solicitud->idEtapa = 4;
            $solicitud->idEtapaOld = 1;
            $solicitud->etapaInicial = 4;
        }
        $solicitud->fechaCambioEtapa = date("Y-m-d H:i:s");  
        $solicitud->aumento_importe = 0;
        $solicitud->timbrar = 0;
        $solicitud->importe_total = $request->importe_total;

        if ($request->idAsignador)
            $solicitud->aceptacion = 0;
        else
            $solicitud->aceptacion = 1;
        
        $autorizacion = Autorizacion::where('idUsuario', $request->idUsuario)->first();
        $solicitud->idAutorizador = $autorizacion->idAutorizador1;
        $saved = $solicitud->save();
        $solicitud->usuario = $solicitud->usuario;

        if($solicitud->idEtapa == 2) {
            if ($solicitud->aceptacion) {
                $this->sendEmail("autorizacionSolicitud", $solicitud->idAutorizador, $solicitud); 
            }else {
                 $this->sendEmail("asignacionSolicitud", $solicitud->idUsuario, $solicitud);
            }
        }

        if($request->importe_total > 0)
            $accion = 'envió una nueva solicitud por $' . $solicitud->importe_total . ' a la etapa de autorización.';
        else 
            $accion = 'registró una nueva solicitud en $' . $solicitud->importe_total . ' para comprobar directamente.';
    
        $dataBitacora = [
            'idSolicitud' => $solicitud->idSolicitud,
            'responsable' => $solicitud->usuario->nombreUsuario,
            'accion' => $accion,
            'comentarios' => '',
            'idEtapa' => 1
        ];
        $this->registrarBitacora($dataBitacora);
        $this->registrarPlaneacionGasto($request->planeacionGasto, $solicitud->idSolicitud);
        $data = [];
        $data['success'] = $saved;
        $data['solicitud'] = $solicitud;
        return $data;
    }

    public function sendEmail($tipo, $idDestinatario, $data)
    {
        if ($tipo == "autorizacionSolicitud") {
            $notifiable = User::find($idDestinatario);
            $notifiable->notify(new AutorizacionSolicitud($data)); 
        }else if($tipo == "asignacionSolicitud"){
            $notifiable = User::find($idDestinatario);
            $notifiable->notify(new AsignacionSolicitud($data));
        }else if ($tipo == "solicitudAutorizada") {
            $role = Role::find('fn.cg.administrador');
            $usuario = $role->usuarios->first();
            $notifiable = User::find($usuario->idUsuario);
            $notifiable->notify(new SolicitudAutorizada($data));
        }else if ($tipo == "solicitudRechazada") {
            $notifiable = User::find($idDestinatario);
            $notifiable->notify(new SolicitudRechazada($data));
        }
    }

    public function update($idSolicitud, Request $request)
    {
        $solicitud = Solicitud::find($idSolicitud);
        $solicitud->destinoGasto = $request->destinoGasto;
        $solicitud->objetivoGasto = $request->objetivoGasto;
        $solicitud->fechaInicioGasto = $request->fechaInicioGasto;
        $solicitud->fechaFinGasto = $request->fechaFinGasto;
        $solicitud->numeroPersonas = $request->numeroPersonas;
        $solicitud->tipoMoneda = $request->tipoMoneda;
        $solicitud->tarjeta = $request->tarjeta;
        $solicitud->importe_total = $request->importe_total;
        $solicitud->tipoCuenta = $request->tipoCuenta;
        $solicitud->cuenta = $request->cuenta;
        $solicitud->idEtapa = 2;
        $solicitud->idEtapaOld = 1;
        $solicitud->etapaInicial = 2;
        $saved = $solicitud->save();

        //ENVIAR A AUTORIZACIÓN
        $this->sendEmail("autorizacionSolicitud", $solicitud->idAutorizador, $solicitud);

        $dataBitacora = [
            'idSolicitud' => $solicitud->idSolicitud,
            'responsable' => $solicitud->usuario->nombreUsuario,
            'accion' => 'editó la solicitud y envió la solicitud por $' . $solicitud->importe_total . 'a la etapa de autorización.',
            'comentarios' => '',
            'idEtapa' => 1
        ];
        $this->registrarBitacora($dataBitacora);
        $this->registrarPlaneacionGasto($request->planeacionGasto, $solicitud->idSolicitud);
        $data = [];
        $data['success'] = $saved;
        $data['solicitud'] = $solicitud;
        return $data;
    }

    public function registrarPlaneacionGasto($planeacionGasto, $idSolicitud)
    {
        $planeacionOld = PlaneacionGasto::where("idSolicitud", $idSolicitud)->get();
        if ($planeacionOld) {
            foreach($planeacionOld as $item) {
                $item->delete();
            }
        }
        foreach ($planeacionGasto as $itemPlaneacion) {
            $planeacion = new PlaneacionGasto;
            $planeacion->idSolicitud = $idSolicitud;
            $planeacion->clasificacion = $itemPlaneacion["clasificacion"];
            $planeacion->formaPago = $itemPlaneacion["formaPago"];
            $planeacion->importe = $itemPlaneacion["importe"];
            $planeacion->save();
        }
    }

    public function getPlaneacionGasto($idSolicitud)
    {
        return planeacionGasto::where('idSolicitud', $idSolicitud)->get();
    }

    public function setFechaFinGastosReal(Solicitud $solicitud, Request $request)
    {
        $solicitud->fechaFinGastoReal = $request->fechaFinGastoReal;
        $saved = $solicitud->save();
        $data = [];
        $data['success'] = $saved;
        $data['solicitud'] = $solicitud;
        return $data;
    }

    public function setPeriodoDispersion(Solicitud $solicitud, Request $request)
    {
        $solicitud->periodoInicioDispersion = $request->periodoInicio;
        $solicitud->periodoFinDispersion = $request->periodoFin;
        $saved = $solicitud->save();
        $data = [];
        $data['success'] = $saved;
        $data['solicitud'] = Solicitud::where("idSolicitud", $solicitud->idSolicitud)->first();
        return $data;
    }

    public function aceptacionSolicitud(Solicitud $solicitud, Request $request)
    {
        $solicitud->aceptacion = $request->aceptacion;   
        if ($request->status == 'rechazado') {
            $solicitud->idEtapaOld = $solicitud->idEtapa;
            $solicitud->idEtapa = 1;
        }
        $updated = $solicitud->update();

        if ($solicitud->aceptacion) {
            //ENVIAR A AUTORIZACIÓN
            $this->sendEmail("autorizacionSolicitud", $solicitud->idAutorizador, $solicitud);
        }
        $data = [];
        $data['success'] = $updated;
        $data['solicitud'] = $solicitud;
        return $data; 
    }

    public function autorizacionSolicitud(Solicitud $solicitud, Request $request)
    {
        if ($request->status == 'autorizado') {
            $solicitud->idEtapa = $request->idEtapa;
            $solicitud->idEtapaOld = $request->idEtapaOld;
        } else if ($request->status == 'rechazado') {
            $solicitud->idEtapa = $request->idEtapa;
            $solicitud->idEtapaOld = $request->idEtapaOld;
        }

        if ($request->idEtapaOld == 2)
            $solicitud->fechaAutorizacionSolicitud = date("Y-m-d H:i:s");
        else
            $solicitud->fechaAutorizacionComprobacion = date("Y-m-d H:i:s");
        
        $solicitud->fechaCambioEtapa = date("Y-m-d H:i:s");  
        $solicitud->update();

        $solicitud->comentarios = Comentario::where('idSolicitud', $solicitud->idSolicitud)
            ->where('idEtapa', $request->idEtapa)
            ->latest()
            ->first();
        
        $solicitud->autorizador = $request->autorizador;

        if (
            $request->status == 'autorizado' && 
            $request->notificar == true
        ) {
            $solicitud->resumen = Resumen::where('idSolicitud', $solicitud->idSolicitud)->get();
            $this->sendEmail("solicitudAutorizada", $solicitud->idUsuario, $solicitud);
        } else if ($request->status == 'rechazado') {
            $this->sendEmail("solicitudRechazada", $solicitud->idUsuario, $solicitud);
        }

        if ($request->idEtapaOld == 2) {
            if ($request->status == 'autorizado')
                $accion = 'autorizó la solicitud.';
            else {
                $accion = 'rechazó la solicitud.';
            }
        }
        else if ($request->idEtapaOld == 5) {
            if ($request->status == 'autorizado')
                $accion = 'envió la comprobación a autorización.';
            else {
                $accion = 'regresó a comprobación para correción.';
            }
        } else if ($request->idEtapaOld == 6) {
            if ($request->status == 'autorizado')
                $accion = 'autorizó la comprobación.';
            else {
                $accion = 'rechazó la comprobación.';
            }
        }

        $dataBitacora = [
            'idSolicitud' => $solicitud->idSolicitud,
            'responsable' => $solicitud->autorizador,
            'accion' => $accion,
            'comentarios' => $request->comentarios,
            'idEtapa' => $request->idEtapaOld
        ];

        $this->registrarBitacora($dataBitacora);
        $data = [];
        $data['solicitud'] = $solicitud;
        $data['success'] = true;
        return $data;  
    }

    public function dispersarSolicitud(Solicitud $solicitud, Request $request)
    {
        if($solicitud->idEtapa == 3) {
            $solicitud->idEtapa = 4;
            $solicitud->idEtapaOld = 3;
            $solicitud->fechaDispersion = date("Y-m-d H:i:s");  
            $solicitud->fechaCambioEtapa = date("Y-m-d H:i:s");  
            $saved = $solicitud->save();   
        }else $saved = false;
        if($request->notificar == true){
            $solicitud->resumen = Resumen::where('idSolicitud', $solicitud->idSolicitud)->get();
            $notifiable = User::find($solicitud->idUsuario);
            $notifiable->notify(new SolicitudDispersada($solicitud));
        }
        $dataBitacora = [
            'idSolicitud' => $solicitud->idSolicitud,
            'responsable' => $request->responsable,
            'accion' => 'realizó la dispersión de la solicitud.',
            'comentarios' => '',
            'idEtapa' => $solicitud->idEtapaOld
        ];
        $this->registrarBitacora($dataBitacora);
        $data = [];
        $data['success'] = $saved;
        $data['solicitud'] = $solicitud;
        return $data;  
    }

    public function cambioEtapa($idSolicitud, $idEtapa, Request $request) 
    {
        $solicitud = Solicitud::find($idSolicitud);
        $solicitud->idEtapa = $idEtapa;
        if($request->idEtapaOld)
            $solicitud->idEtapaOld = $request->idEtapaOld;
        $solicitud->fechaCambioEtapa = date("Y-m-d H:i:s");

        switch ($solicitud->idEtapaOld) {
            case '1':
            case '2':
                if ($idEtapa == 10) {
                    $accion = 'eliminó la solicitud.';
                }
                break;
            case '4':
                $accion = 'envió comprobación a revisión.';
                $solicitud->fechaComprobacion = date("Y-m-d H:i:s");
                break;
            case '5':
                $accion = 'envió comprobación a autorización.';
                $solicitud->fechaRevision = date("Y-m-d H:i:s");  
                break;
            case '7':
                $accion = 'envió comprobación a contabilización.';
                $solicitud->fechaReembolsoDecremento = date("Y-m-d H:i:s");
                break;
            case '8':
                $accion = 'terminó la contabilización.';
                $solicitud->fechaContabilizacion = date("Y-m-d H:i:s");
                break;
            default:
                break;
        }
        $dataBitacora = [
            'idSolicitud' => $solicitud->idSolicitud,
            'responsable' => $request->responsable,
            'accion' => $accion,
            'comentarios' => '',
            'idEtapa' => $solicitud->idEtapaOld
        ];
        $this->registrarBitacora($dataBitacora);
        $saved = $solicitud->save();
        $data = [];
        $data['success'] = true;
        $data['solicitud'] = $solicitud;
        return $data;  
    }

    // public function timbrarSolicitud(Solicitud $solicitud, Request $request) 
    // {
    //     $solicitud->noNominaTimbrada = $request->noNomina;
    //     $solicitud->importeTimbrado = $request->importe;
    //     $solicitud->timbrar = 2;
    //     $saved = $solicitud->save();
    //     $dataBitacora = [
    //         'idSolicitud' => $solicitud->idSolicitud,
    //         'responsable' => $request->responsable,
    //         'accion' => 'registró el timbrado de la solicitud.',
    //         'comentarios' => '',
    //         'idEtapa' => 8
    //     ];
    //     $this->registrarBitacora($dataBitacora);
    //     $saved = $solicitud->save();
    //     $data = [];
    //     $data['success'] = true;
    //     $data['solicitud'] = $solicitud;
    //     return $data;  

    //     $data['success'] = $saved;
    //     $data['solicitud'] = $solicitud;
    //     return $data;  
    // }

    public function getCuentasBySolicitud($idSolicitud, $tipoCuenta) 
    {
        return DB::table('fn_cg_comprobaciones')->select('cuenta')->where('idSolicitud', $idSolicitud)->where('tipoCuenta', $tipoCuenta)->get()->unique('cuenta')->values();
    }

    public function getCuentas($tipoCuenta) 
    {
         return DB::table('fn_cg_solicitudes')->select('cuenta')->where('tipoCuenta', $tipoCuenta)->get()->unique('cuenta')->values();
    }

    public function generateReport(Request $request)
    {
        $solicitud = Solicitud::find($request->idSolicitud);
        $solicitante = User::where("idUsuario", $solicitud->idUsuario)->first(['nombreUsuario']);
        $autorizador = User::where("idUsuario", $solicitud->idAutorizador)->first(['nombreUsuario']);
        $decrementar = 0;
        $reembolsar = 0;
        $totaDts = (object) [
            "totalSolicitado" => 0,
            "totalDispersado" => 0,
            "totalComprobado" => 0,
            "totalReembolsado" => 0,
            "totalDecrementado" => 0,
            "totalReembolsar" => 0,
            "totalDecrementar" => 0,
        ];
        if ($request->resumen) {
            foreach ($request->resumen as $item) {
                $totaDts->totalSolicitado += $item['solicitado'];
                $totaDts->totalDispersado += $item['dispersado'];
                $totaDts->totalComprobado += $item['comprobado'];
                $totaDts->totalReembolsado += $item['reembolsado'];
                $totaDts->totalDecrementado += $item['decrementado'];
            } 
            $reembolsar = $totaDts->totalComprobado - $totaDts->totalDispersado;
            $decrementar = $totaDts->totalDispersado - $totaDts->totalComprobado;
            if ($reembolsar > 0)
                $totaDts->totalReembolsar = $reembolsar - $totaDts->totalReembolsado;
            else $totaDts->totalReembolsar = $totaDts->totalReembolsado;
            if ($decrementar > 0)
                $totaDts->totalReembolsar = $decrementar - $totaDts->totalDecrementado;
            else $totaDts->totalReembolsar = $totaDts->totalDecrementado;
        }
        $nameFile = "Solicitud de gasto #". $solicitud->idSolicitud .".pdf";
        $ruta = "FN/CG/Reportes/". $nameFile;

        $pdf = App::make('dompdf.wrapper');
        $pdf->setPaper('A4', 'landscape');
        $pdf->loadView('modulos.finanzas.reportes.solicitud', [
            'solicitud' => $solicitud, 
            'resumen' => $request->resumen,
            'comprobaciones' => $request->comprobaciones,
            'totalesCg' => $request->totalesCg,
            'totaDts' => $totaDts,
            'solicitante' => $solicitante->nombreUsuario,
            'autorizador' => $autorizador->nombreUsuario,
        ]);      
        $saved = Storage::put($ruta, $pdf->output());
        $data = [];
        $data['success'] = $saved;
        $data['file'] = $ruta;
        return $data;
    }

    public function downloadSolicitud($idSolicitud)
    {
        $file = "FN/CG/Reportes/Solicitud de gasto #". $idSolicitud .".pdf";
        return Storage::download($file);
    }

    public function deleteSolicitud($idSolicitud)
    {
        $file = "FN/CG/Reportes/Solicitud de gasto #". $idSolicitud .".pdf";
        $delete = Storage::delete($file);
        return response()->json($delete);
    }

    public function getSolicitudesRango(Request $request)
    {
        $solicitudes = DB::table('fn_cg_solicitudes')
        ->join('usuarios', 'fn_cg_solicitudes.idUsuario', '=', 'usuarios.idUsuario')
        ->join('fn_cg_etapas', 'fn_cg_solicitudes.idEtapa', '=', 'fn_cg_etapas.idEtapa')
        ->select(
            'fn_cg_solicitudes.*',
            'usuarios.nombreUsuario as nombre_usuario',
            'fn_cg_etapas.nombre as etapa')
        ->where('fn_cg_solicitudes.idEtapa', '!=', '10')
        ->where('fn_cg_solicitudes.idEtapa', '!=', '1')
        ->whereBetween('fn_cg_solicitudes.created_at', [$request->inicio, $request->fin])
        ->get();

        $sol_usuarios = $solicitudes->unique('nombre_usuario');
        $usuarios = collect();
        $usuario = array();

        foreach($sol_usuarios as $item){
            if($item->nombre_usuario != "") {
                $usuario['usuario'] = $item->nombre_usuario;
                $usuarios = $usuarios->concat($usuario);
            }
        }
        return [
            "empleados" => $usuarios,
            "solicitudes" => $solicitudes,
        ];
    }

    public function registrarBitacora($data) {
        $bitacora = new Bitacora;
        $bitacora->idSolicitud = $data['idSolicitud'];
        $bitacora->responsable = $data['responsable'];
        $bitacora->accion = $data['accion'];
        $bitacora->comentarios = $data['comentarios'];
        $bitacora->idEtapa = $data['idEtapa'];
        $bitacora->save();
    }
}
