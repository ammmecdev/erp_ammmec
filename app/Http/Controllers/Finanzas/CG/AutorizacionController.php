<?php

namespace App\Http\Controllers\Finanzas\CG;

use Illuminate\Http\Request;
use App\Models\Finanzas\CG\Autorizacion;
use App\Http\Controllers\Controller;
use App\Models\Cadmin\Role;

class AutorizacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getAutorizacionByUsuario($idUsuario)
    {
        return Autorizacion::where('idUsuario', $idUsuario)->first();
    }

    public function getAutorizaciones()
    {
        return Autorizacion::all();
    }

    public function getAutorizadores()
    {
        $role = Role::where('idRole', 'fn.cg.autorizador')->first();

        return $role->usuarios;
    }

    public function store(Request $request)
    {
        $autorizacion = new Autorizacion;
        $autorizacion->idUsuario = $request->idUsuario;
        $autorizacion->idAutorizador1 = $request->autorizador1;
        $autorizacion->idAutorizador2 = $request->autorizador2;
        $autorizacion->idAutorizador3 = $request->autorizador3;
        $saved = $autorizacion->save();

        $data = [];
        $data['success'] = $saved;
        $data['autorizacion'] = $autorizacion;
        return $data;
    }

    public function update(Autorizacion $autorizacion, Request $request)
    {
        switch ($request->opcion) {
            case '1':
                $autorizacion->idAutorizador1 = $request->value;
                break;
            case '2':
                $autorizacion->idAutorizador2 = $request->value;
                break;
            case '3':
                $autorizacion->idAutorizador3 = $request->value;
                break;
        }
        $saved = $autorizacion->save();
        $data = [];
        $data['success'] = $saved;
        $data['autorizacion'] = $autorizacion;
        return $data;
    }
}
