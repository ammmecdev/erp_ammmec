<?php

namespace App\Http\Controllers\Finanzas\CG;

use Illuminate\Http\Request;
use App\Models\Finanzas\CG\Comentario;
use App\Http\Controllers\Controller;

class ComentarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function getComentario($idSolicitud, $idEtapa) 
    {
        return Comentario::where('idSolicitud', $idSolicitud)->where('idEtapa', $idEtapa)->latest()->first();
    }

    public function store(Request $request)
    {
        $saved = true;
        $comentario = Comentario::where('idSolicitud', $request->idSolicitud)->where('idEtapa', $request->idEtapa)->first();
        if ($comentario && $request->comentarios == "") {
            $saved = $comentario->delete();  
        } 
        if($request->comentarios != ""){
            if (!$comentario) {
                $comentario = new Comentario;
                $comentario->idEtapa = $request->idEtapa;
                $comentario->idSolicitud = $request->idSolicitud;
            }
            $comentario->comentario = $request->comentarios;
            $saved = $comentario->save();  
        }
        $data = [];
        $data['success'] = $saved;
        return $data;
    }
}
