<?php

namespace App\Http\Controllers\Finanzas\CG;

use Illuminate\Http\Request;
use App\Models\Finanzas\CG\Bitacora;
use App\Http\Controllers\Controller;

class BitacoraController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getBitacorasBySolicitud($idSolicitud)
    {
        return Bitacora::where('idSolicitud', $idSolicitud)->get();
    }

    public function store(Request $request)
    {
        $bitacora = new Bitacora;
        $bitacora->idSolicitud = $request->idSolicitud;
        $bitacora->responsable = $request->responsable;
        $bitacora->accion = $request->accion;
        $saved = $bitacora->save();
        
        $data = [];
        $data['success'] = $saved;
        $data['bitacora'] = $bitacora;
    }

}
