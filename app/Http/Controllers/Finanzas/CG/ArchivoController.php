<?php

namespace App\Http\Controllers\Finanzas\CG;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Finanzas\CG\Archivo;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ArchivoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getArchivos($idSolicitud)
    {
        return Archivo::where("idSolicitud", $idSolicitud)->get();
    }
    
    public function store(Request $request)
    {
        if ($request->file('file')) {
            $archivo = new Archivo;
            $archivo->idSolicitud = $request->idSolicitud;
            $archivo->tipo = $request->tipo;
            $archivo->etiquetas = $request->etiquetas;
            $archivo->color = $request->color;

            $file = $request->file('file');
            $path = $file->storeAs("FN/CG/Comprobantes", $file->getClientOriginalName());
            $archivo->ruta = "/storage/".$path;
            $archivo->file = $file->getClientOriginalName();

            $saved = $archivo->save();
            $data = [];
            $data['success'] = $saved;
            $data['archivo'] = $archivo;
            return $data;
        }else {
            $data = [];
            $data['success'] = false;
            return $data;
        }        
    }

    public function update(Request $request)
    {
        $archivo = Archivo::find($request->idArchivo);
        $archivo->etiquetas = $request->etiquetas;
        $archivo->color = $request->color;

        if ($request->file('file')) {
            $coincidencias = Archivo::where('file', $archivo->file)->count();
            if ($coincidencias == 1) {
                Storage::delete('FN/CG/Comprobantes/'.$archivo->file);
            }
            $path = $request->file('file')->storeAs("FN/CG/Comprobantes", $request->file->getClientOriginalName());            
            $archivo->file = $request->file->getClientOriginalName();
            $archivo->ruta = "/storage/".$path;
        }
        $saved = $archivo->save();
        $data = [];
        $data['success'] = $saved;
        $data['archivo'] = $archivo;
        return $data;
    }

    public function download(Request $request)
    {
        return Storage::download("FN/CG/Comprobantes/".$request->file);
    }

    public function delete(Archivo $archivo)
    {
        $coincidencias = Archivo::where('file', $archivo->file)->count();
        if ($coincidencias == 1) {
            Storage::delete('FN/CG/Comprobantes/'.$archivo->file);
        }
        $deleted = $archivo->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }
}
