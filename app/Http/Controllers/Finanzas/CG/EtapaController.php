<?php

namespace App\Http\Controllers\Finanzas\CG;

use Illuminate\Http\Request;
use App\Models\Finanzas\CG\Etapa;
use App\Http\Controllers\Controller;

class EtapaController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function getEtapas() {
        return Etapa::all();
    }
}
