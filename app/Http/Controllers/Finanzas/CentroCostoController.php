<?php

namespace App\Http\Controllers\Finanzas;

use Illuminate\Http\Request;
use App\Models\Finanzas\CC\Departamento;
use App\Models\Finanzas\CC\Proceso;
use App\Models\Finanzas\CC\Categoria;
use App\Models\Finanzas\CC\NoProductivo;
use App\UnidadNegocio;
use App\Models\TalentoHumano\Empleado;

use App\Http\Controllers\Controller;

class CentroCostoController extends Controller
{
    public function getUnidadesNegocio()
    {
        return UnidadNegocio::where('idUnidadNegocio', '!=', 1)->get();
    }

    public function getDepartamentos() 
    {
        return Departamento::all();
    }

    public function getCategorias() 
    {
        return Categoria::all();
    }

    public function getColaboradores() 
    {
        return Empleado::where('status', 'Activo')->orderBy('noEmpleado', 'ASC')->get();
    }

    public function getProcesos() 
    {
        return Proceso::all();
    }

    public function getNoProductivos() 
    {
        return NoProductivo::all();
    }

    public function getActivos() 
    {
        //queda pendiente
    }
}
