<?php

namespace App\Http\Controllers\Finanzas;

use App\Http\Controllers\Controller;
use App\Models\Finanzas\CuentasUsuario;
use App\Models\Finanzas\CuentaContable;
use Illuminate\Http\Request;

class CuentasUsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getCuentasByUsuario($idUsuario)
    {
       return CuentasUsuario::where('idUsuario', $idUsuario)->first();
    }

    public function save($tipo, Request $request)
    {
        $cuentas_usuario = CuentasUsuario::where('idUsuario', $request->idUsuario)->first();
        if($cuentas_usuario == null)
            $cuentas_usuario = new CuentasUsuario;
        $cuentas_usuario->idUsuario = $request->idUsuario;
        if($tipo=="contable")
            $cuentas_usuario->cuentas_contables = $request->cuentas;
        if($tipo=="interna")
            $cuentas_usuario->cuentas_internas = $request->cuentas;

        $saved = $cuentas_usuario->save();

        $data = [];
        $data['success'] = $saved;
        $data['cuentas_usuario'] = $cuentas_usuario;

        return $data;
    }

    public function getCuentasContablesByUsuario($idUsuario, $unidadNegocio)
    {
        $cuentas_usuario = CuentasUsuario::where('idUsuario', $idUsuario)->first();
        $cuentas = collect();
        if ($cuentas_usuario != null) {
            $areas = json_decode($cuentas_usuario->cuentas_contables);
            foreach($areas as $area) {
                foreach($area->cuentas as $cuenta) {
                    foreach($cuenta->subcuentas as $noCuenta) {
                        $cc = CuentaContable::where('noCuenta', $noCuenta)->first();
                        if($unidadNegocio=="Campo"){
                            if($area->area == 'Campo') $cuentas->push($cc);
                        }else {
                            if($area->area != 'Campo') $cuentas->push($cc);
                        }
                    }
                }
            } 
        }
        return $cuentas;
    }

    public function getCuentasInternasByUsuario($idUsuario)
    {
        $cuentas_usuario = CuentasUsuario::where('idUsuario', $idUsuario)->first();
        $cuentas = collect();
        if($cuentas_usuario != null) {
            $areas = json_decode($cuentas_usuario->cuentas_internas);
            foreach ($areas as $area) {
                foreach ($area->unidades_negocio as $un) {
                    foreach ($un->cuentas as $cuenta) {
                        $cuentas->push($cuenta);
                    }
                }
            } 
        }
        return $cuentas;
    }
}
