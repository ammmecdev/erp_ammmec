<?php

namespace App\Http\Controllers\Finanzas;

use Illuminate\Http\Request;
use App\Models\Finanzas\CuentaInterna;
use App\Http\Controllers\Controller;

class CuentaInternaController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getCuentas()
    {
        return CuentaInterna::all();
    }

    public function getAreas()
    {
        $cuentas = CuentaInterna::all();
        $cuentas = $cuentas->unique('area');
        $areas = collect();
        foreach ($cuentas as $area) {
            $areas->push($area);
        }
        return $areas;
    }

    public function getCuentasByArea($area)
    {
        return CuentaInterna::where('area', $area)->get();
    }

    public function getUnidadesNegocioByArea($area)
    {
        $cuentas = CuentaInterna::where('area', $area)->get();
        $usNegocio = $cuentas->unique('unidadNegocio');
        $unidadesNegocio = collect();
        foreach ($usNegocio as $uNegocio) {
            $unidadesNegocio->push($uNegocio);
        }
        return $unidadesNegocio;
    }

    public function store(Request $request)
    {
        $cuenta = new CuentaInterna;
        $cuenta->unidadNegocio = $request->unidadNegocio;
        $cuenta->area = $request->area;
        $cuenta->tipo = $request->tipo;
        $cuenta->nombre = $request->nombre;
        $cuenta->cuenta = $request->cuenta;

        $saved = $cuenta->save();

        $data = [];
        $data['saved'] = $saved;
        $data['cuenta'] = $cuenta;

        return $data;
    }

    public function edit(Request $request)
    {
        $cuenta = CuentaInterna::find($request->idCuenta);
        $cuenta->unidadNegocio = $request->unidadNegocio;
        $cuenta->area = $request->area;
        $cuenta->tipo = $request->tipo;
        $cuenta->nombre = $request->nombre;
        $cuenta->cuenta = $request->cuenta;

        $saved = $cuenta->update();

        $data = [];
        $data['saved'] = $saved;
        $data['cuenta'] = $cuenta;

        return $data;
    }

    public function delete($idCuenta)
    {
        $cuenta = CuentaInterna::find($idCuenta);
        $deleted = $cuenta->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getUnidadesNegocio()
    {
        $cuentas = CuentaInterna::all();
        $cuentas = $cuentas->unique('unidadNegocio');
        $unidades_negocio = collect();
        $unidad = array();
        foreach($cuentas as $cuenta){
            if($cuenta->unidadNegocio != ""){
                $unidad['unidadNegocio'] = $cuenta->unidadNegocio;
                $unidades_negocio->push($unidad);
            }
        }
        return $unidades_negocio;
    }

    public function getNombres()
    {
        $cuentas = CuentaInterna::where('tipo', 'Proceso')->orWhere('tipo', 'Activo')->get();
        $cuentas = $cuentas->unique('nombre');
        $nombres = collect();
        $nombre = array();
        foreach($cuentas as $cuenta){
            if($cuenta->nombre != ""){
                $nombre['nombre'] = $cuenta->nombre;
                $nombres->push($nombre);
            }
        }
        return $nombres;
    }
}
