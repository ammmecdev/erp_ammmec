<?php

namespace App\Http\Controllers\Finanzas;

use Illuminate\Http\Request;
use App\Models\Finanzas\CO\Factor;
use App\Models\Finanzas\CO\Puesto;
use App\Http\Controllers\Controller;

class ManoObraController extends Controller
{
    public function getPuestos()
    {
        return Puesto::all();
    }

    public function getFactores()
    {
       return Factor::all()->first();
    }

    public function updatePuesto(Puesto $puesto, Request $request)
    {
        $puesto->sueldo = $request->sueldo;
        $puesto->factorUtilizacion = $request->factorUtilizacion;
        $updated = $puesto->update();
        $data = [];
        $data['success'] = $updated;
        $data['puesto'] = $puesto;
        return $data;
    }

    public function updateFactor(Factor $factor, Request $request)
    {
        $factor->iva = $request->iva;
        $factor->domingo = $request->domingo;
        $factor->jornadaOcho = $request->jornadaOcho;
        $factor->jornadaDoce = $request->jornadaDoce;
        $factor->moDisponible = $request->moDisponible;
        $updated = $factor->update();
        $data = [];
        $data['success'] = $updated;
        $data['factor'] = $factor;
        return $data;
    }
}
