<?php

namespace App\Http\Controllers\Sistemas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sistemas\PeriodoLicencia;

class PeriodoLicenciaController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getPeriodos($idLicencia)
    {
        $periodos = PeriodoLicencia::where('idLicencia', $idLicencia)->get();
        foreach ($periodos as $periodo) {   	
    		$periodo->status = $this->getStatusPeriodo($periodo);
        }
        return $periodos;
    }
    public function store(Request $request)
    {
        $periodo = new PeriodoLicencia;
        $periodo->monto = number_format((float)$request->monto, 2, '.', '');;
        $periodo->fechaInicio = $request->fechaInicio;
        $periodo->fechaFin = $request->fechaFin;
        $periodo->fechaPago = $request->fechaPago;
        $periodo->mesVencimiento = $request->mesVencimiento;
        $periodo->yearVencimiento = $request->yearVencimiento;
        $periodo->pagado = 0;
        $periodo->idLicencia = $request->idLicencia;
        $saved = $periodo->save();
        $periodo->status = $this->getStatusPeriodo($periodo);
        $data = array();
        $data['success'] = $saved;
        $data['periodo'] = $periodo;
        return $data;
    }
    public function update($idPeriodo, Request $request)
    {
        $periodo = PeriodoLicencia::find($idPeriodo);
        $periodo->monto = $request->monto;
        $periodo->fechaInicio = $request->fechaInicio;
        $periodo->fechaFin = $request->fechaFin;
        $periodo->fechaPago = $request->fechaPago;
        $periodo->mesVencimiento = $request->mesVencimiento;
        $periodo->yearVencimiento = $request->yearVencimiento;
        $saved = $periodo->save();
        $periodo->status = $this->getStatusPeriodo($periodo);
        $data = array();
        $data['success'] = $saved;
        $data['periodo'] = $periodo;
        return $data;
    }

    public function updatePagado($idPeriodo, $pagado)
    {
    	$periodo = PeriodoLicencia::find($idPeriodo);
    	$periodo->pagado = $pagado;
    	$saved = $periodo->save();
    	$periodo->status = $this->getStatusPeriodo($periodo);
        $data = array();
        $data['success'] = $saved;
        $data['periodo'] = $periodo;
        return $data;
    }

    public function getStatusPeriodo($periodo)
    {
    	$today = date("Y-m-d");
    	if($periodo->pagado == 1)       	
			return $periodo->status = 'PAGADO';
		else if($periodo->fechaFin < $today)
			return $periodo->status = 'VENCIDO';
		else if($periodo->fechaFin == $today)
			return $periodo->status = 'PAGAR HOY';
		else if($periodo->fechaFin > $today)
			return $periodo->status = 'PENDIENTE';
    }
}
