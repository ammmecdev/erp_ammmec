<?php

namespace App\Http\Controllers\Sistemas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sistemas\CuentaServicio;
use Illuminate\Support\Facades\Crypt;

class CuentaServicioController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getCuentasServicios($idCuenta)
    {
        $cuentasServicios = CuentaServicio::where('idCuenta', $idCuenta)->get();
        foreach ($cuentasServicios as $servicio)
            $servicio->pass = Crypt::decryptString($servicio->pass);
        return $cuentasServicios;
    }

    public function store(Request $request)
    {
        $cuentaServicio = new CuentaServicio;
        $cuentaServicio->idCuenta = $request->idCuenta;
        $cuentaServicio->idServicio = $request->idServicio;
        $cuentaServicio->pass = Crypt::encryptString($request->pass);
        $saved = $cuentaServicio->save();
        $data = [];
        $data['success'] = $saved;
        $data['cuentaServicio'] = $cuentaServicio;
        return $data;
    }

    public function update(CuentaServicio $cuentaServicio, Request $request)
    {
        $cuentaServicio->idCuenta = $request->idCuenta;
        $cuentaServicio->idServicio = $request->idServicio;
        $cuentaServicio->pass = Crypt::encryptString($request->pass);
        $saved = $cuentaServicio->save();
        $data = [];
        $data['success'] = $saved;
        $data['cuentaServicio'] = $cuentaServicio;
        return $data;
    }
}
