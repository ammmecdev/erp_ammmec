<?php

namespace App\Http\Controllers\Sistemas;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Sistemas\EquipoElectronico;

class EquipoElectronicoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
	
   public function getEquipos()
    {
    	return EquipoElectronico::all();
    }

     public function store(Request $request)
    {
    	$equipo = new EquipoElectronico;
    	$equipo->responsable = $request->responsable;
    	$equipo->clave = $request->clave;
    	$equipo->status = $request->status;
    	$equipo->tipo = $request->tipo;
    	$equipo->marca = $request->marca;
    	$equipo->noSerie = $request->noSerie;
    	$equipo->modelo = $request->modelo;
    	$equipo->area = $request->area;

    	$save = $equipo->save();

    	$data = array();

    	$data['success'] = $save;
    	$data['equipo'] = $equipo;

    	return $data;
    }

    public function update($idEquipo, Request $request)
    {
    	$equipo = EquipoElectronico::find($idEquipo);
    	$equipo->responsable = $request->responsable;
    	$equipo->clave = $request->clave;
    	$equipo->status = $request->status;
    	$equipo->tipo = $request->tipo;
    	$equipo->marca = $request->marca;
    	$equipo->noSerie = $request->noSerie;
    	$equipo->modelo = $request->modelo;
    	$equipo->area = $request->area;

    	$save = $equipo->save();

    	$data = array();

    	$data['success'] = $save;
    	$data['equipo'] = $equipo;

    	return $data;
    }

    public function getProxClave($tipo)
    {
        $equipos = DB::table('equipos_electronicos')->where('tipo', $tipo)->get();
        $consecutivos=collect();
        foreach ($equipos as $equipo) {
            $claveCompleta = explode('-', $equipo->clave, 3); 
            $consecutivos->push(intval($claveCompleta[1]));
        }
        $ultimoConsecutivo = $consecutivos->max();
        $siguienteConsecutivo = $ultimoConsecutivo + 1;
        $tipoClave = $this->getTipoClave($tipo);
        return $siguienteClave = 'AMMMEC-' . '0' . $siguienteConsecutivo . '-' . $tipoClave;
    }

    public function getTipoClave($tipo)
    {
        switch ($tipo) {
            case 'PC':
                return 'PC';
                break;
            case 'Laptop':
                return 'LAP';
                break;
            case 'Celular':
                return 'CEL';
                break;
        }
    }
}
