<?php

namespace App\Http\Controllers\Sistemas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sistemas\CategoriaLicencia;

class CategoriaLicenciaController extends Controller
{
     public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

   	public function getCategorias()
   	{
   		return CategoriaLicencia::all();
   	}

    public function store(Request $nombre)
    {
    	$categoria = new CategoriaLicencia;
    	$categoria->nombre = $nombre;
    	$saved = $categoria->save();
    	$data = array();
    	$data['success'] = $saved;
    	$data['categoria'] = $categoria;
    	return $data;
    }

   	public function getCategoriasWhitLicencias()
   	{
   		$categorias = CategoriaLicencia::all();
   		foreach ($categorias as $categoria) {
            $totalCategoria = 0;
   			$categoria->licencias = $categoria->licencias;
            foreach($categoria->licencias as $licencia){
                $totalCategoria += $licencia->monto; 
                $periodos = $licencia->periodos->where('pagado', 1);
                if(!$periodos->isEmpty()){
                    $max = $periodos->max();
                    $licencia->ultimoPago = $max->fechaInicio;
                    $licencia->fechaVencimiento = $max->fechaFin;
                }else{
                    $licencia->ultimoPago = 'No definido';
                    $licencia->fechaVencimiento = 'No definido';
                }
            }
            $categoria->total = number_format((float)$totalCategoria, 2, '.', '');
   		}
   		return $categorias;
   	}
}
