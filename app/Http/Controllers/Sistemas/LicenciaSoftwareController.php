<?php

namespace App\Http\Controllers\Sistemas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sistemas\LicenciaSoftware;

class LicenciaSoftwareController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

   	public function getLicencias()
   	{
   		return LicenciaSoftware::all();
   	}

   	public function store(Request $request)
    {
    	$licencia = new LicenciaSoftware;
    	$licencia->nombre = $request->nombre;
    	$licencia->monto = $request->monto;
    	$licencia->periodoPago = $request->periodoPago;
    	$licencia->tipoPago = $request->tipoPago;
    	$licencia->cuenta = $request->cuenta;
    	$licencia->idCategoria = $request->idCategoria;
    	$save = $licencia->save();

    	$data = array();

    	$data['success'] = $save;
    	$data['licencia'] = $licencia;

    	return $data;
    }

    public function update($idLicencia, Request $request)
    {
        $licencia = LicenciaSoftware::find($idLicencia);
    	$licencia->nombre = $request->nombre;
    	$licencia->monto = $request->monto;
    	$licencia->periodoPago = $request->periodoPago;
    	$licencia->tipoPago = $request->tipoPago;
    	$licencia->cuenta = $request->cuenta;
    	$licencia->idCategoria = $request->idCategoria;
    	$save = $licencia->save();

    	$data = array();

    	$data['success'] = $save;
    	$data['licencia'] = $licencia;

    	return $data;
    }
}
