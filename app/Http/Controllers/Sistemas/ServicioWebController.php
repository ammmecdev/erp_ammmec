<?php

namespace App\Http\Controllers\Sistemas;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sistemas\ServicioWeb;

class ServicioWebController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getServicios()
    {
        return ServicioWeb::get();
    }
    public function store(Request $request)
    {
        $servicio = new ServicioWeb;
        $servicio->nombre = $request->nombreServicio;
        $saved = $servicio->save();

        $data = array();
        $data['success'] = $saved;
        $data['servicio'] = $servicio;
        return $data;
    }
}
