<?php

namespace App\Http\Controllers\Sistemas;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Sistemas\CuentaElectronica;
use Illuminate\Support\Facades\Crypt;


class CuentaElectronicaController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getCuentas()
    {
		$cuentas = CuentaElectronica::all();
		foreach($cuentas as $cuenta) 
			$cuenta->password_cuenta = Crypt::decryptString($cuenta->password_cuenta);
		return $cuentas;
    }

    public function store(Request $request)
    {
    	$cuenta = new CuentaElectronica;
    	$cuenta->item = $request->item;
    	$cuenta->cuenta = $request->cuenta_completa;
    	$cuenta->responsable = $request->responsable;
    	$cuenta->dominio = $request->dominio;
    	$cuenta->password_cuenta = Crypt::encryptString($request->password_cuenta);
    	$cuenta->status = 'Activa';
		$save = $cuenta->save();

		$cuenta->password_cuenta = Crypt::decryptString($cuenta->password_cuenta);

    	$data = array();

    	$data['success'] = $save;
    	$data['cuenta'] = $cuenta;

    	return $data;
    }

    public function update($idCuenta, Request $request)
    {
    	$cuenta = CuentaElectronica::find($idCuenta);
    	$cuenta->cuenta = $request->cuenta_completa;
    	$cuenta->responsable = $request->responsable;
    	$cuenta->dominio = $request->dominio;
    	if(isset($request->password_cuenta))
    		$cuenta->password_cuenta = Crypt::encryptString($request->password_cuenta);
		$save = $cuenta->save();

		$cuenta->password_cuenta = Crypt::decryptString($cuenta->password_cuenta);

    	$data = array();

    	$data['success'] = $save;
    	$data['cuenta'] = $cuenta;

    	return $data;
    }

    public function getProxItem($dominio)
    {
    	$maxItem = DB::table('si_cuentas_electronicas')->where('dominio',$dominio)->max('item');
    	return $maxItem + 1;
    }

    public function getDominios()
    {
    	$cuentas = CuentaElectronica::all();
        $dominios = collect();
        foreach($cuentas as $cuenta){
        	$dominio = array();
            $dominio['text'] = $cuenta->dominio;
            $dominio['value'] = $cuenta->dominio;

            $dominios->push($dominio);
        }
        $dominiosRegistrados = $dominios->unique();
        return $dominiosRegistrados;
    }
}
