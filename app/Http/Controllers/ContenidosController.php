<?php

namespace App\Http\Controllers;

use App\User;
use App\Negocio;
use Notification;
use App\Contenido;
use Illuminate\Http\Request;
use App\Events\NotificationToUser;
use App\Notifications\SendNotification;

class ContenidosController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function store(Request $request)
    {
        $contenido = new Contenido();       
        $contenido->idUsuario = $request->usuario_id;
        $contenido->idContacto = null;
        $contenido->idCliente = null;
        $contenido->idNegocio = $request->negocio_id;
        $contenido->notas = $request->notas;
        $contenido->fechaSubido = $request->fecha_subido;
        
        if($request->file){
            $file = $request->file('file');
            $nombre = $file->getClientOriginalName();
            \Storage::disk('local')->put($nombre, \File::get($file));   
            $contenido->archivos = $nombre;
        }
       
        $saved = $contenido->save();

        $negocio = Negocio::find($request->negocio_id);

        $this->sendNotificationNotaNegocio($negocio);

        $data = [];
        $data['success'] = $saved;
        $data['contenido'] = $contenido;      
        return $data;
    }

     public function sendNotificationNotaNegocio($negocio){
        $authUser = auth()->user();
        $notification = [];
        $userName = explode(" " , $authUser->nombreUsuario);
        $notification['text'] = $userName[0] . ' ' . $userName[1] . ' registró una nota en el negocio ' . $negocio->claveNegocio . ' ' . $negocio->tituloNegocio;
        $notification['picture'] = '/img/profiles/' . $authUser->foto;
        $notification['route'] = '/ventas/negocios/'. $negocio->idNegocio;

        $user = User::find(4); //Mandar a yunuen

        if($authUser != $user){
            $notification = Notification::send($user, new SendNotification($notification));
            event(new NotificationToUser($user->idUsuario));
        }
    }
}
