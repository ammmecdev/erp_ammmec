<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Causa;

class CausaController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    public function selectCausas()
    {
    	$causas = Causa::get();
    	$data = [];
 
    	foreach($causas as $key => $value){
    		$data[$key] = [
                'text'  => $value->nombreCausa,
    			'value' => $value->idcausasPerdida
    		];
    	}
    	return response()->json($data);
    }

    public function getCausas()
    {
        return Causa::get();
    }
}
