<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Equipo;

class EquipoController extends Controller
{
	public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
	
   public function selectEquipos()
    {
    	$equipos = Equipo::get();
    	$data = [];
 
    	foreach($equipos as $key => $value){
    		$data[$key] = [
                'text'  => $value->nombreEquipo,
    			'value' => [
                                'text' => $value->clave,
                                'value' => $value->idEquipo
                            ]
    		];
    	}
    	return response()->json($data);
    }
}
