<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EvaluacionServicio;
use App\EvaluacionPregunta;
use App\Pregunta;
use Illuminate\Support\Facades\DB;

class EvaluacionServicioController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function store(Request $request)
    {
    	$evaluacion_servicio = new EvaluacionServicio();       
        $evaluacion_servicio->idCliente = $request->id_cliente;
        $evaluacion_servicio->contacto = $request->id_contacto;
        $evaluacion_servicio->cuatrimestre = $request->cuatrimestre;
        $evaluacion_servicio->observaciones = null;
        $evaluacion_servicio->idEmbudo = $request->id_embudo;

        $saved = $evaluacion_servicio->save();
        $data = [];
        $data['success'] = $saved;
        $data['evaluacion'] = $evaluacion_servicio; 
        $evaluacionesPreguntas = $this->evaluacionesPreguntas($evaluacion_servicio);
        $data['evaluacionesPreguntas'] = $evaluacionesPreguntas;
        return $data;
    }

    public function evaluacionesPreguntas($evaluacion)
    {
        $data = [];
        $preguntas = Pregunta::all();
        foreach ($preguntas as $pregunta) {
           $eva_pregunta = new EvaluacionPregunta();
           $eva_pregunta->idEvaluacion = $evaluacion->idEvaluacion;
           $eva_pregunta->idPregunta = $pregunta->idPregunta;
           $eva_pregunta->valor = 0;
           $eva_pregunta->save();
           array_push($data, $eva_pregunta);
        }
        return $data;
    }

    public function getEvaluaciones(Request $request)
    {
        $evaluaciones = DB::table('evaluaciones')
            ->join('clientes_facturados', 'evaluaciones.idCliente', '=', 'clientes_facturados.id')
            ->join('clientes', 'clientes_facturados.idCliente', '=', 'clientes.idCliente')
            ->join('contactos', 'evaluaciones.contacto', '=', 'contactos.idContacto')
          
            ->select(
                'evaluaciones.*', 
                'clientes.clave as clave_cliente',
                'contactos.nombre as nombre_contacto'
            )
            ->where('evaluaciones.idEmbudo', $request->idEmbudo)
            ->where('evaluaciones.cuatrimestre', $request->cuatrimestre)->get();
        return $evaluaciones;
    }

    public function update(Request $request)
    {
        $evaluacion = EvaluacionServicio::find($request->idEvaluacion);
        $evaluacion->observaciones = $request->observaciones;
        $saved = $evaluacion->save();
        $data = [];
        $data['success'] = $saved;
        $data['evaluacion'] = $evaluacion;
        return $data;
    }

    public function evaluacionById($idEvaluacion)
    {
        $data = [];
        $evaluacion = DB::table('evaluaciones')
            ->join('clientes_facturados', 'evaluaciones.idCliente', '=', 'clientes_facturados.id')
            ->join('clientes', 'clientes_facturados.idCliente', '=', 'clientes.idCliente')
            ->join('contactos', 'evaluaciones.contacto', '=', 'contactos.idContacto')
          
            ->select(
                'evaluaciones.*', 
                'clientes.clave as clave_cliente',
                'contactos.nombre as nombre_contacto'
            )
            ->where('evaluaciones.idEvaluacion', $idEvaluacion)->first();
        $data['evaluacion'] = $evaluacion;
        return $data;
    }
}
