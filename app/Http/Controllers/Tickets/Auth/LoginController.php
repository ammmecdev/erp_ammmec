<?php

namespace App\Http\Controllers\Tickets\Auth;

use Auth;
use App\Http\Controllers\Controller;
//use Illuminate\Foundation\Auth\AuthemticatesUsers;

class LoginController extends Controller
{

    // use AuthenticatesUsers;

    // public function authenticated(Request $request, $user){
    //     dd($user);
    //     die();
    // }
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    function login()
    {
        $credentials = $this->validate(request(),[
            'email' => 'email|required|string', 
            'password' => 'required|string',
        ],[
            'email.email' => 'Debes ingresar un correo electrónico válido', 
            'email.required' => 'Debes ingresar un correo empresarial',
            'password.required' => 'Debes ingresar una contraseña',
        ]);
        if (Auth::attempt($credentials))
        {
            return redirect()->intended('modulos');
        }
        return back()->withErrors(['email' => trans('auth.failed')])
        ->withInput(request(['email']));
    }   

    function showLoginForm()
    {
        return view('auth.login');
    }

    function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
