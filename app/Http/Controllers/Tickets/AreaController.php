<?php

namespace App\Http\Controllers\Tickets;

use Illuminate\Support\Facades\DB;
use App\Models\TalentoHumano\Departamento;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection as Collection;

class AreaController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest', ['only' => 'showLoginForm']);
	}
	
	function getAreas()
	{
		return Departamento::get();
	}

	public function getAreasAdmins()
	{
		$users = DB::table('usuarios')
	        ->join('empleados', 'usuarios.noEmpleado', '=', 'empleados.noEmpleado')
	        ->join('rh_puestos', 'empleados.idPuesto', '=', 'rh_puestos.idPuesto')
	        ->select(
	            'usuarios.*', 
	            'empleados.idPuesto',
	            'rh_puestos.idDepartamento'
	        )
	        ->where('usuarios.is_admin_tk', 1)->get();

		$ids = collect();
		foreach($users as $user)
			$ids = $ids->concat([$user->idArea]);
		$ids = $ids->unique();

		$array = collect();
		foreach ($ids as $id)
			$array = $array->concat([Departamento::find($id)]);

		return $array;
	}
}