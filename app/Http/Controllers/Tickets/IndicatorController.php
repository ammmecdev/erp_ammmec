<?php

namespace App\Http\Controllers\Tickets;

use Illuminate\Http\Request;
use App\Models\Ticket;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class IndicatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

	public function getTicketsProductivity(Request $request)
	{
		$tickets_status = DB::table('tk_tickets')   
            ->select( 
                'tk_tickets.status_admin as status')

            ->whereBetween('tk_tickets.created_at', [$request->start, $request->end])
            ->where('tk_tickets.departament_id', $request->departament_id)
            ->get();

        return $tickets_status;
	}

    public function getTicketsByServices(Request $request)
    {
        $tickets_services = DB::table('tk_tickets') 
            ->join('tk_services', 'tk_tickets.service_id', '=', 'tk_services.id')  
            ->select(DB::raw('tk_services.name as service, count(*) as total'))

            ->where('tk_tickets.departament_id', $request->departament_id)
            ->whereBetween('tk_tickets.created_at', [$request->start, $request->end])
            ->groupBy('tk_services.name')
            ->get();
        
        return $tickets_services;
    }

    public function getTicketsBySubjects(Request $request)
    {
       $tickets_subjects = DB::table('tk_tickets')  
            ->select(DB::raw('tk_tickets.subject, count(*) as total'))
            ->where('tk_tickets.departament_id', $request->departament_id)
            ->whereBetween('tk_tickets.created_at', [$request->start, $request->end])
            ->groupBy('tk_tickets.subject')
            ->get();
        
        return $tickets_subjects;
    }

    public function getTicketsByDepartament(Request $request)
    {
        $tickets_dep = DB::table('tk_tickets')
            ->join('usuarios', 'tk_tickets.user_id', '=', 'usuarios.idUsuario')
            ->join('empleados', 'usuarios.noEmpleado', '=', 'empleados.noEmpleado')
            ->join('rh_puestos', 'empleados.idPuesto', '=', 'rh_puestos.idPuesto')
            ->join('rh_departamentos', 'rh_puestos.idDepartamento', '=', 'rh_departamentos.idDepartamento')

            ->select(DB::raw('rh_departamentos.nombre as departamento, count(IF(tk_tickets.status_admin = "Cerrado", 1, NULL)) as tickets_cerrados, count(IF(tk_tickets.status_admin = "Abierto" OR tk_tickets.status_admin = "Nuevo" OR tk_tickets.status_admin = "Contestado", 1, NULL)) as tickets_abiertos'))
            ->where('tk_tickets.departament_id', $request->departament_id)
            ->whereBetween('tk_tickets.created_at', [$request->start, $request->end])
            ->groupBy('rh_departamentos.nombre')
            ->get();
        
        return $tickets_dep;
    }

    public function getAttentionTime(Request $request)
    {
        $tickets_attentionTime = DB::table('tk_tickets')
            ->join('tk_services', 'tk_tickets.service_id', '=', 'tk_services.id')
           
            ->select(DB::raw('tk_services.name as services, count(*) as tickets, SUM(TIMESTAMPDIFF(DAY, tk_tickets.created_at, tk_tickets.updated_at)) as total_dias'))

            ->where('tk_tickets.departament_id', $request->departament_id)
            ->whereBetween('tk_tickets.created_at', [$request->start, $request->end])
            ->groupBy('tk_services.name')
            ->get();
        
        return $tickets_attentionTime;
    }

}
