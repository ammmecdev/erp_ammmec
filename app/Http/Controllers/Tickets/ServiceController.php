<?php

namespace App\Http\Controllers\Tickets;

use Illuminate\Support\Facades\DB;
use App\Models\Tickets\User;
use App\Models\Tickets\Service;
use App\Models\Tickets\Subject;
use Carbon\Carbon;
use App\Models\Tickets\Departament;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServiceController extends Controller
{
	public function __construct() {
		$this->middleware('guest', ['only' => 'showLoginForm']);
	}

	function index() {
		$departments=Departament::all();
		$services=Service::all();
		return view('modulos.tickets.services.index', compact('services', 'departments'));
	}

	public function show(Service $service) {
		$services=true;
		Carbon::setLocale('es');
		return view('services.show', compact('service', 'services'));
	}

	public function store(Request $request) {
		$service = new Service;
		$service->name = $request->name;
		$service->description = $request->description;
		$service->status = $request->status;
		$service->departament_id = $request->departament_id;	
		$saved=$service->save();
		$data=[];
		$data['saved']=$saved;
        $data['service']=$service;
        return $data;
	}

	public function update(Request $request) {
		$service = Service::find($request->id);
		$service->name = $request->name;
		$service->description = $request->description;
		$updated=$service->update();
		$data=[];
        $data['saved']=$updated;
        $data['service']=$service;
		return $data;
	}

	public function getServices() {
		return Service::orderBy('name', 'ASC')->get();
	}

	public function updateStatus(Request $request) {
		$service = Service::find($request->id);
		$service->status = ($request->status == true) ? "Activo" : "Inactivo";
		$updated=$service->update();
		$data=[];
        $data['success']=$updated;
		return $data;
	}

	public function getUsersService(Request $request) {
		$service = Service::find($request->id);
		$users=$service->users;
		return $users;
	}

	public function getUsersByDepartament(Request $request) {
		return DB::table('usuarios')
		 	->join('empleados', 'usuarios.noEmpleado', '=', 'empleados.noEmpleado')
		 	->join('rh_puestos', 'empleados.idPuesto', '=', 'rh_puestos.idPuesto')
		 	->join('rh_departamentos', 'rh_puestos.idDepartamento', '=', 'rh_departamentos.idDepartamento')
            ->select('usuarios.*')
            ->where('rh_departamentos.idDepartamento', $request->id)
            ->get();
	}

	public function attachUser(Request $request) {
		$service=Service::findOrFail($request->service_id);
		$service->users()->attach($request->user_id);
		$users = $service->users;
		$data=[];
        $data['success']=true;
        $data['users']=$users;
		return $data;
	}

	public function detachUser(Request $request) {
		$service=Service::findOrFail($request->service_id);
		$deleted=$service->users()->detach($request->user_id);
		$data=[];
        $data['success']=$deleted;
		return $data;
	}

	public function getServicesByDepartament($departament_id) {
		return Service::where('departament_id', $departament_id)->orderBy('name', 'ASC')->get();
	}
}
