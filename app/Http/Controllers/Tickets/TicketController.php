<?php

namespace App\Http\Controllers\Tickets;

use Illuminate\Support\Facades\DB;

use App\Events\Tickets\TicketCreated;
use App\Events\Tickets\TicketClosed;

use App\Http\Controllers\Controller;

use App\Models\Tickets\Ticket;
use App\Models\Tickets\TicketUser;
use App\Models\Tickets\Departament;
use App\Models\Tickets\User;
use App\Models\Tickets\Service;
use App\Models\Tickets\Message;
use App\Models\Tickets\File;
use App\Models\Tickets\Subject;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Query\Builder;

class TicketController extends Controller
{
	public function __construct()
	{
		$this->middleware('guest', ['only' => 'showLoginForm']);
	}
	
	public function getTicketById(Ticket $ticket)
	{
		return $ticket;
	}

	public function getTicketsByUser(User $user, Request $request)
	{
		return DB::table('tk_tickets')
			->join('usuarios', 'usuarios.idUsuario', '=', 'tk_tickets.user_id')
			->join('rh_departamentos', 'rh_departamentos.idDepartamento', '=', 'tk_tickets.departament_id')
			->join('tk_services', 'tk_services.id', '=', 'tk_tickets.service_id')

			->select('usuarios.nombreUsuario as user_name', 'usuarios.foto as user_picture', 'usuarios.email as user_email', 'tk_tickets.*', 'rh_departamentos.nombre as departament_name', 'tk_services.name as service_name')
			->where(function ($query) use ($request) {
				if ($request->status == 'Abierto') {
					$query->where('tk_tickets.status', '!=', 'Cerrado')->orWhere('tk_tickets.messages_unread', '>', 0);
				} else if ($request->status == 'Cerrado')
					$query->where('tk_tickets.status', 'Cerrado');
				else
					$query->where('tk_tickets.status', '!=', '');
			})
			->where('tk_tickets.user_id', $user->idUsuario)
			->where(function ($query) use ($request) {
				$query->where('tk_tickets.id', 'like', '%' . $request->search . '%')
					->orWhere('tk_tickets.subject', 'like', '%' . $request->search . '%')
					->orWhere('usuarios.nombreUsuario', 'like', '%' . $request->search . '%');
			})
			->orderBy('tk_tickets.updated_at', 'DESC')
			->paginate(15);
	}

	public function getTicketsByUserAdmin(User $user, Request $request)
	{
		return  DB::table('tk_tickets')
			->join('usuarios', 'usuarios.idUsuario', '=', 'tk_tickets.user_id')
	        ->join('rh_departamentos', 'rh_departamentos.idDepartamento', '=', 'tk_tickets.departament_id')
	        ->join('tk_services', 'tk_services.id', '=', 'tk_tickets.service_id')
		 	->join('tk_ticket_user', 'tk_ticket_user.ticket_id', '=', 'tk_tickets.id')
	        ->select('usuarios.nombreUsuario as user_name', 'usuarios.email as user_email', 'tk_services.name as service_name', 'usuarios.foto as user_picture', 'tk_tickets.*', 'tk_ticket_user.messages_unread')
	        ->where('tk_ticket_user.user_id', $user->idUsuario)
	        ->where(function ($query) use ($request){
	        	if($request->status == 'Abierto'){
	        		$query->where('tk_tickets.status', '!=', 'Cerrado')->orWhere('tk_ticket_user.messages_unread', '>', 0);
	        	}
	        	else if($request->status == 'Cerrado')
	        		$query->where('tk_tickets.status', 'Cerrado');
	        	else
	        		$query->where('tk_tickets.status', '!=', '');
	        })
	        ->where(function($query) use ($request){
	        	$query->where('tk_tickets.id', 'like', '%' . $request->search . '%')
		        ->orWhere('tk_tickets.subject', 'like', '%' . $request->search . '%')
				->orWhere('usuarios.nombreUsuario', 'like', '%' . $request->search . '%');
	        })
	        ->orderBy('tk_tickets.updated_at', 'DESC')
	        ->paginate(15);
	}

	public function store(Request $request)
	{
		$ticket = new Ticket;
		$ticket->departament_id = $request->departament_id;
		$ticket->service_id = $request->service_id;
		$ticket->subject = $request->subject_name;
		$ticket->user_id = $request->user_id;
		$ticket->status = 'Abierto';
		$ticket->status_admin = 'Nuevo';
		$saved = $ticket->save();

		$ticket->departament_name = $request->departament_name;
		$users = $ticket->service->users;

		if (!$users->isNotEmpty())
			$users = $this->getAdminUsers($request->departament_id);
		
		foreach ($users as $user)
			$ticket->users()->attach($user->idUsuario);

		$subject = Subject::where('name', $request->subject_name)->first();

		if (!$subject) {
			$subject = new Subject;
			$subject->name = $request->subject_name;
			$subject->service_id = $request->service_id;
			$subject->save();
		}

		$message = new Message;
		$message->message = $request->message;
		$message->user_id = $request->user_id;
		$message->ticket_id = $ticket->id;
		$message->save();

		if ($request->file != "null") {
			$file = $request->file;
			$name = $file->getClientOriginalName();
			$type = $file->getClientOriginalExtension();
			$route = time() . $name;
			$file->move(public_path() . '/files', $route);

			File::create([
				'name' => $name,
				'type' => $type,
				'route' => 'files/' . $route,
				'message_id' => $message->id
			]);
		}

		broadcast(new TicketCreated($ticket, auth()->user()))->toOthers();

		$data = [];
		$data['success'] = $saved;
		$data['ticket'] = $ticket;

		return $data;
	}

	public function getMessagesByTicket(Ticket $ticket)
	{
		$messages = Message::where('ticket_id', $ticket->id)->orderBy('created_at')->get();
		foreach ($messages as $message) {
			$message->files = $message->files;
		}
		// return array_reverse($messages);
		return $messages;
	}

	public function getFilesByTicket(Ticket $ticket)
	{
		$messages = $ticket->messages;

		$array = collect();
		foreach ($messages as $message){
			$files = $message->files;
			$array = $array->concat($files);
		}

		return $array;
	}

	public function getUsersByTicket(Ticket $ticket)
	{
		$ticket_users = $ticket->users;
		foreach ($ticket_users as $ticket_user) {
			$user = User::find($ticket_user->idUsuario);
			// $ticket_user->user_departament_name = $user->departament->nombre;
			$ticket_user->online = false;
		}
		return $ticket_users;
	}

	public function closeTicket(Request $request)
	{
		$ticket = Ticket::find($request->ticket_id);
		$ticket->status = $request->status;
		$ticket->status_admin = $request->status_admin;
		$saved = $ticket->save();

		event(new TicketClosed($ticket));

		$data = [];
		$data['success'] = $saved;
		$data['ticket'] = $ticket;
		return $data;
	}

	public function download(File $file)
	{
		$pathtoFile = public_path() . '/' . $file->route;
		return response()->download($pathtoFile);
	}

	public function updateTkServiceSubject(Request $request)
	{
		$ticket = Ticket::find($request->ticket_id);
		$ticket->subject = $request->subject_name;
		$ticket->service_id = $request->service_id;
		$saved = $ticket->save();
		$data = [];
		$data['success'] = $saved;
		$data['ticket'] = $ticket;
		return $data;
	}

	public function escalateTicket(Request $request)
	{
		$ticket = Ticket::find($request->ticket_id);
		$this->detachTicket_user($ticket->id);

		$ticket->subject = $request->subject_name;
		$ticket->service_id = $request->service_id;
		$ticket->departament_id = $request->departament_id;
		$saved = $ticket->save();

		$message = Message::create([
			'message' =>   $request->message,
			'ticket_id' => $request->ticket_id,
			'user_id' 	=> $request->user_id
		]);

		$this->attachTicket_user($ticket->id);

		$data = [];
		$data['success'] = $saved;
		$data['ticket'] = $ticket;
		$data['message'] = $message;
		return $data;
	}

	public function getAdminUsers($id_departament)
	{
		return DB::table('usuarios')
		 	->join('empleados', 'usuarios.noEmpleado', '=', 'empleados.noEmpleado')
		 	->join('rh_puestos', 'empleados.idPuesto', '=', 'rh_puestos.idPuesto')
		 	->join('rh_departamentos', 'rh_puestos.idDepartamento', '=', 'rh_departamentos.idDepartamento')
            ->select('usuarios.*')
            ->where('rh_departamentos.idDepartamento', $id_departament)
            ->where('usuarios.is_admin_tk', 1)
            ->get();
	}

	public function detachTicket_user($ticket_id)
	{
		$ticket = Ticket::find($ticket_id);
		$users = $ticket->service->users;
		if ($users->isNotEmpty()) {
			foreach ($users as $user) :
				$ticket->users()->detach($user->idUsuario);
			endforeach;
		} else {
			$users = $this->getAdminUsers($ticket->departament_id);
			foreach ($users as $user) :
				if ($user->is_admin_tk == '1')
				$ticket->users()->detach($user->idUsuario);
			endforeach;
		}
	}

	public function attachTicket_user($ticket_id)
	{
		$ticket = Ticket::find($ticket_id);
		$users = $ticket->service->users;
		if ($users->isNotEmpty()) {
			foreach ($users as $user) :
				$ticket->users()->attach($user->idUsuario);
			endforeach;
		} else {
			$users = $this->getAdminUsers($ticket->departament_id);
			foreach ($users as $user) :
				if ($user->is_admin_tk == '1')
				$ticket->users()->attach($user->idUsuario);
			endforeach;
		}
	}

	public function deleteMessagesUnread(Ticket $ticket, Request $request)
	{
		if (auth()->user()->idUsuario == $ticket->user_id) {
			$ticket->messages_unread = 0;
			$ticket->save();
		} else {
			$ticket_user = TicketUser::where('ticket_id', $ticket->id)->where('user_id', auth()->user()->idUsuario)->first();
			$ticket_user->messages_unread = 0;
			$ticket_user->save();
		}
	}

	public function addParticipants(Ticket $ticket, Request $request)
	{
		$array = collect();
		foreach ($request->newParticipants as $user => $id) {
			$ticket->users()->attach($id);
			$participant = User::find($id);
			$participant->online = false;
			$participant_departament_name = $participant->departament->nombre;
			$array->push($participant);
		}
		return $array;
	}

	// function index()
	// {
	// 	// $tickets = auth()->user()->tickets_all;
	// 	$messages = 'null';
	// 	$ticket = 'null';
	// 	$isAdmin = 'false';
	// 	return view('modulos.tickets.tickets.show', compact('ticket', 'messages', 'isAdmin'));
	// }

	// function create()
	// {
	// 	// $areas = Area::all();
	// 	$areas = Area::where('name', 'Sistemas')->orWhere('name', 'Conservación')->get();
	// 	// $services_dep = Service::where('departament_id', $areas->id)->get();
	// 	$abrir_ticket = true;
	// 	return view('modulos.tickets.tickets.create', compact('areas', 'abrir_ticket', 'services_dep'));
	// }

	// function create_admin()
	// {
	// 	$users_in_open_ticket = User::all();
	// 	$areas = Area::where('name', auth()->user()->area->nombre)->first();
	// 	$services_dep = Service::where('departament_id', $areas->id)->get();
	// 	$abrir_ticket = true;
	// 	return view('modulos.tickets.tickets.create_admin', compact('areas', 'abrir_ticket', 'services_dep', 'users_in_open_ticket'));
	// }

	// function created(Ticket $ticket)
	// {
	// 	return redirect("tickets/nuevo")->with('success', 'Ticket creado #' . $ticket->id)->with('idTicket', $ticket->id);
	// }

	// function created_admin(Ticket $ticket)
	// {
	// 	return redirect("tickets/nuevo/admin")->with('success', 'Ticket creado #' . $ticket->id)->with('idTicket', $ticket->id);
	// }

	// function show(Ticket $ticket)
	// {
	// 	$isAdmin = 'false';
	// 	$messages=Message::where('ticket_id', $ticket->id)->orderBy('created_at')->get();
	// 	foreach ($messages as $message) {
	// 	 	$message->files = $message->files;
	// 	 } 
	// 	Carbon::setLocale('es');
	// 	return view('modulos.tickets.tickets.show', compact('ticket','isAdmin', 'messages'));
	// }

	// function show_admin(Ticket $ticket)
	// {
	// 	$isAdmin = 'true';
	// 	$messages=Message::where('ticket_id', $ticket->id)->orderBy('created_at')->get();
	// 	foreach ($messages as $message) {
	// 	 	$message->files = $message->files;
	// 	 } 
	// 	Carbon::setLocale('es');
	// 	return view('modulos.tickets.tickets.show', compact('ticket', 'isAdmin', 'messages'));
	// }

	// public function store()
	// {
	// 	$data = request()->validate([
	// 		'user_id' => 'required',
	// 		'email' => '',
	// 		'message' => 'required',
	// 		'subject' => 'required',
	// 		'departament_id' => 'required',
	// 		'service_id' => 'required',	
	// 		'file' => '',
	// 	],[
	// 		'user_id.required' => 'Se debe asignar un responsable al ticket',
	// 		'message.required' => 'El campo mensaje es obligatorio.',
	// 		'departament_id.required' => 'El campo departamento es obligatorio.',
	// 		'subject.required' => 'El campo asunto es obligatorio.',
	// 		'service_id.required' => 'El campo servicio es obligatorio.',
	// 	]);

	// 	$ticket=Ticket::create([
	// 		'subject' => $data['subject'],
	// 		'service_id' => $data['service_id'],
	// 		'user_id' => $data['user_id'],
	// 		'departament_id' => $data['departament_id']
	// 	]);

	// 	$message=Message::create([
	// 		'message' => '</p>'.$data['message'].'</p>',
	// 		'ticket_id' => $ticket->id,
	// 		'user_id' 	=> auth()->user()->id,
	// 	]);

	// 	if(auth()->user()->id==$data['user_id'])
	// 			$this->createMessageResponse($ticket);

	// 	$countSubject = Subject::where('name', $data['subject'])->count();
	// 	if ($countSubject == 0) {
	// 		Subject::create([
	// 			'name' => $data['subject'],
	// 			'service_id' => $data['service_id']
	// 		]);
	// 	}
		
	// 	if (isset($data['file'])) {
	// 		foreach ($data['file'] as $file):
	// 			$name = $file->getClientOriginalName();
	// 			$type = $file->getClientOriginalExtension();
	// 			$route = time().$name;
	// 			$file->move(public_path().'/files', $route);

	// 			File::create([
	// 				'name' => $name,
	// 				'type' => $type,
	// 				'route' => 'files/'.$route,
	// 				'message_id' => $message->id
	// 			]);
	// 		endforeach;
	// 	}
	// 	echo $ticket->id;
	// }

	// function listServices($idArea){	
	// 	$datos = array();
	// 	foreach (Service::findById($idArea)->where('status', 'Activo') as $service){
	// 		$row_array['ids']  = $service->id;
	// 		$row_array['nombre']  = $service->name;
	// 		$row_array['descripcion'] = $service->description;
	// 		array_push($datos, $row_array);
	// 	}    
	// 	echo json_encode($datos, JSON_FORCE_OBJECT);
	// }

	// function indexAdmin()
	// {
	// 	$messages = 'null';
	// 	$ticket = 'null';
	// 	$isAdmin = 'true';
	// 	return view('modulos.tickets.tickets.show', compact('ticket', 'messages', 'isAdmin'));
	// }

	// function getTickets($status)
	// {	
	// 	$tickets = ($status!='All') ? Ticket::findByStatus($status) : auth()->user()->tickets_all;
	// 	return $this->dataTable($tickets);
	// }

	// function getTickets_admin($status)
	// {	
	// 	/*$tickets = ($status!='All') ? auth()->user()->tickets->where('status_admin',$status) : auth()->user()->tickets;
	// 	return $this->dataTable_back($tickets);*/
	// 	if ($status!='All') {
	// 		$tickets = DB::table('tickets')
	//             ->join('users', 'users.id', '=', 'tickets.user_id')
	//             ->join('areas', 'areas.idArea', '=', 'tickets.departament_id')
	//             ->join('ticket_user', 'ticket_user.ticket_id', '=', 'tickets.id')
	//             ->select('users.name as user_name', 'tickets.*')
	//             ->where('ticket_user.user_id', auth()->user()->id)
	//             ->where('tickets.status_admin', $status)
	//             ->orderBy('tickets.updated_at')
	//             ->get();
	//     }else{
	// 		$tickets = DB::table('tickets')
	//             ->join('users', 'users.id', '=', 'tickets.user_id')
	//             ->join('areas', 'areas.idArea', '=', 'tickets.departament_id')
	//             ->join('ticket_user', 'ticket_user.ticket_id', '=', 'tickets.id')
	//             ->select('users.name as user_name', 'tickets.*')
	//             ->where('ticket_user.user_id', auth()->user()->id)
	//             ->orderBy('tickets.updated_at')
	//             ->get();
	// 	}
	// 	return $this->dataTable($tickets);
	// }

	// function dataTable($tickets)
	// {
	// 	return Datatables::of($tickets)
	// 	->setRowId(function($ticket){
	// 		return $ticket->id;
	// 	})
	// 	->setRowData(['data-name' => 'row-{{ $subject }}',])
	// 	->addColumn('user_name', function($ticket){
	// 		return  $ticket->user_name;
	// 	})
	// 	->editColumn('updated_at', function($ticket){
	// 		Carbon::setLocale('es');
	// 		$updated_at = Carbon::parse($ticket->updated_at);
	// 		return "<center>".$updated_at->diffForHumans()."<center>";
	// 	})
	// 	->editColumn('status', function($ticket){
	// 		$status = $ticket->status;
	// 		$val = $status == "Abierto" ? "success" : ($status == "Cerrado" ? "secondary" : "danger");
	// 		$span = "<span class='badge badge-$val d-block'>$status</span>";
	// 		return $span;
	// 	})	
	// 	->editColumn('status_admin', function($ticket){
	// 		$status = $ticket->status_admin;
	// 		$val = $status == "Abierto" ? "success" : ($status == "Cerrado" ? "secondary" : "danger");
	// 		$span = "<span class='badge badge-$val d-block'>$status</span>";
	// 		return $span;
	// 	})	
	// 	->editColumn('id', '<b>{{ $id }}</b>')
	// 	->rawColumns(['status', 'id', 'status_admin','updated_at', 'user_name'])
	// 	->toJson();
	// }

	// function dataTable_back($tickets)
	// {
	// 	return Datatables::of($tickets)
	// 	->setRowId(function($ticket){
	// 		return $ticket->id;
	// 	})
	// 	->setRowData(['data-name' => 'row-{{ $subject }}',])
	// 	->addColumn('user_name', function(Ticket $ticket){
	// 		return  $ticket->user->name;
	// 	})
	// 	->editColumn('updated_at', function(Ticket $ticket){
	// 		Carbon::setLocale('es');
	// 		return "<center>".$ticket->updated_at->diffForHumans()."<center>";
	// 	})
	// 	->editColumn('status', function(Ticket $ticket){
	// 		$status = $ticket->status;
	// 		$val = $status == "Abierto" ? "success" : ($status == "Cerrado" ? "secondary" : "danger");
	// 		$span = "<span class='badge badge-$val d-block'>$status</span>";
	// 		return $span;
	// 	})	
	// 	->editColumn('status_admin', function(Ticket $ticket){
	// 		$status = $ticket->status_admin;
	// 		$val = $status == "Abierto" ? "success" : ($status == "Cerrado" ? "secondary" : "danger");
	// 		$span = "<span class='badge badge-$val d-block'>$status</span>";
	// 		return $span;
	// 	})	
	// 	->editColumn('id', '<b>{{ $id }}</b>')
	// 	->rawColumns(['status', 'id', 'status_admin','updated_at', 'user_name'])
	// 	->toJson();
	// }

}



































