<?php

namespace App\Http\Controllers\Tickets;

use App\Models\Tickets\Subject;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubjectController extends Controller
{
	public function __construct() {
		$this->middleware('guest', ['only' => 'showLoginForm']); 
	}
	
	public function getSubjectsByService($service_id) {
		return Subject::where('service_id', $service_id)->get();
	}

	public function delete(Request $request) {
        $deleted = Subject::destroy($request->id);
        $data = [];
        $data['success'] = $deleted;
        return $data;
	}

	public function store(Request $request) {
    	$subject = new Subject;
        $subject->name = $request->name;
        $subject->service_id = $request->service_id;
       	$saved=$subject->save();
        $data = [];
        $data['success'] = $saved;
        $data['subject'] = $subject;
        return $data;
    }

	// function listSubjects($service_id) {
	// 	$datos = array();
	// 	foreach (Subject::findById($service_id) as $subject):
	// 		$row_array['id']  = $subject->id;
	// 		$row_array['nombre']  = $subject->name;
	// 		array_push($datos, $row_array);
	// 	endforeach;     
	// 	echo json_encode($datos, JSON_FORCE_OBJECT);
	// }

	// public function addSubject() {
	// 	$data = request()->validate([
	// 		'subject' => 'required',
	// 		'service_id' => 'required',
	// 	],[
	// 		'subject.required' => 'Debe ingresar un asunto',
	// 	]);
	// 	$subject=Subject::create([
	// 		'name' => $data['subject'],
	// 		'service_id' => $data['service_id'],
	// 	]);
	// 	return back();
	// }
}
