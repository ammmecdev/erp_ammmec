<?php

namespace App\Http\Controllers\Tickets;

use App\Models\Tickets\Ticket;
use App\Models\Tickets\File;
use App\Models\Tickets\Message;
use App\Events\Tickets\NewMessage;
use App\Events\Tickets\SendMessage;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Notifications\MessageSend;

class MessageController extends Controller
{
    public function store(Ticket $ticket, Request $request)
	{	
		$message = new Message();

		$message->message = $request->message;
		$message->ticket_id = $ticket->id;
		$message->user_id = auth()->user()->idUsuario;
		$save = $message->save();

		if($request->file){
	        $file = $request->file;
	
	        $name = $file->getClientOriginalName();
	        $type = $file->getClientOriginalExtension();
	        $route = $request->time.$name;
	        $file->move(public_path().'/files', $route);  

	        File::create([
				'name' => $name,
				'type' => $type,
				'route' => 'files/'.$route,
				'message_id' => $message->id
			]);  
	    }

		$message->files = $message->files;

		$ticket = Ticket::find($message->ticket_id);
		$ticket->updated_at = $message->created_at;

		if($ticket->user_id == auth()->user()->idUsuario){
			$ticket->status = "Abierto";
			$ticket->status_admin = "Contestado";
		}else{
			$ticket->status = "Contestado";
		 	$ticket->status_admin = "Abierto";
		}
		$ticket->save();

		broadcast(new NewMessage($message, $ticket, auth()->user()))->toOthers();
		broadcast(new SendMessage($ticket))->toOthers();

		$data = [];
        $data['success'] = $save;
        $data['ticket'] = $ticket;
        $data['message'] = $message;
        return $data;
	}
}
