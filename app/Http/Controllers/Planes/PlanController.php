<?php

namespace App\Http\Controllers\Planes;

use Illuminate\Support\Facades\Storage;
use App\Models\Planes\PlanResponsable;
use App\Http\Controllers\Controller;
use App\Models\Planes\PlanUsuario;
use Illuminate\Support\Facades\DB;
use App\Models\Planes\Actividad;
use App\Models\Planes\Seccion;
use Illuminate\Http\Request;
use App\Models\Planes\Plan;
use Carbon\Carbon;

class PlanController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $plan = new Plan;
        $plan->nombre = $request->nombre;
        $plan->tipo = $request->tipo;
        $saved = $plan->save();
        if ($saved) {
            $planUsuario = new PlanUsuario;
            $planUsuario->idPlan = $plan->idPlan;
            $planUsuario->idUsuario = $request->idUsuario;
            $planUsuario->tipo = "Propietario";
            $saved = $planUsuario->save();

            $planResponsable = new PlanResponsable;
            $planResponsable->responsable = $request->usuario;
            $planResponsable->iniciales = "BPG";
            $planResponsable->idPlan = $plan->idPlan;
            $saved = $planResponsable->save();
        }
        $plan['tipoUsuario'] = $planUsuario->tipo;
        $plan['idUsuario'] = $planUsuario->idUsuario;
        $data = [];
        $data['success'] = $saved;
        $data['plan'] = $plan;
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Plan $plan, Request $request)
    {
        $plan->nombre = $request->nombre;
        $updated = $plan->update();
        $data = [];
        $data['success'] = $updated;
        $data['plan'] = $plan;
        return $data;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Planes\Plan  $plan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plan $plan)
    {
        $deleted = $plan->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getPlanes(Request $request)
    {
        return DB::table('planes_usuarios')
        ->join('planes', 'planes_usuarios.idPlan', '=', 'planes.idPlan')
        ->select(
            'planes.*', 
            'planes_usuarios.tipo as tipoUsuario',
            'planes_usuarios.idUsuario')
        ->where('planes_usuarios.idUsuario', $request->idUsuario)->get();
    }

    public function getSecciones(Plan $plan)
    {
        return $plan->secciones;
    }

    public function getActividades(Plan $plan)
    {
       return $plan->actividades;
    }

    public function getResponsables(Plan $plan)
    {
       return $plan->responsables;
    }

    public function storeActividad(Request $request)
    {
        $actividad = new Actividad;
        $actividad->idSeccion = $request->idSeccion;
        $actividad->idPlan = $request->idPlan;
        $actividad->actividad = $request->actividad;
        $actividad->responsable = $request->responsable;
        $actividad->inicioP = $request->inicioP;
        $actividad->finP = $request->finP;
        $actividad->observaciones = $request->observaciones;
        $saved = $actividad->save();
        $data = [];
        $data['success'] = $saved;
        $data['actividad'] = Actividad::find($actividad->idActividad);
        return $data;
    }

    public function storeSeccion(Request $request)
    {
        $seccion = new Seccion;
        $seccion->nombre = $request->nombre;
        $seccion->idPlan = $request->idPlan;
        $saved = $seccion->save();
        $data = [];
        $data['success'] = $saved;
        $data['seccion'] = $seccion;
        return $data;
    }

    public function storeResponsable(Request $request)
    {
        $responsable = new PlanResponsable;
        $responsable->responsable = $request->responsable;
        $responsable->iniciales = $request->iniciales;
        $responsable->idPlan = $request->idPlan;
        $saved = $responsable->save();
        $data = [];
        $data['success'] = $saved;
        $data['responsable'] = $responsable;
        return $data;
    }

    public function updateActividad(Actividad $actividad, Request $request)
    {
        $actividad->actividad = $request->actividad;
        $actividad->responsable = $request->responsable;
        $actividad->inicioP = $request->inicioP;
        $actividad->finP = $request->finP;
        $actividad->inicioR = $request->inicioR;
        $actividad->finR = $request->finR;
        $actividad->avance = $request->avance;
        $actividad->observaciones = $request->observaciones;
        $updated = $actividad->update();

        $plan = Plan::where('idPlan', $actividad->idPlan)->first();
        $plan->updated_at = Carbon::now();
        $plan->update();
        $data = [];
        $data['success'] = $updated;
        $data['actividad'] = $actividad;
        return $data;
    }

    public function destroySeccion(Seccion $seccion)
    {
        $deleted = $seccion->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function destroyActividad(Actividad $actividad)
    {
        $deleted = $actividad->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getUsuariosCompartidos(Plan $plan)
    {
        return $plan->usuarios;
    }

    public function storePlanUsuario(Request $request)
    {
        $planUsuario = new PlanUsuario;
        $planUsuario->idPlan = $request->idPlan;
        $planUsuario->idUsuario = $request->usuario;
        $planUsuario->tipo = $request->tipo;
        $saved = $planUsuario->save();
        $data = [];
        $data['success'] = $saved;
        $data['planUsuario'] = $planUsuario;
        return $data;
    }

    public function destroyUsuario(PlanUsuario $usuario)
    {
        $deleted = $usuario->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }
}
