<?php

namespace App\Http\Controllers\Cadmin;

use App\Models\Cadmin\Role;
use Illuminate\Http\Request;
use App\Models\Cadmin\Modulo;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{

    public function getRoles() 
    {
        return Role::get(); 
    }

    public function getRolesByModulo(Modulo $modulo)
    {
        // dd(Role::where('idModulo', $modulo->idModulo)->get()); 
        return $roles = $modulo->roles;
    }

    public function store(Request $request)
    {
        $role = new Role;
        $role->idRole = $request->idRole;
        $role->nombre = $request->nombre;
        $role->idModulo = $request->idModulo;
        $role->idSubmodulo = $request->idSubmodulo;
        $saved = $role->save();

        $data = [];
        $data['success'] = $saved;
        $data['role'] = $role;

        return $data;
    }

    public function update(Role $role, Request $request)
    {
        $idRoleOld = $role->idRole;
        $role->idRole = $request->idRole;
        $role->nombre = $request->nombre;
        $saved = $role->save();
        $role->idRoleOld = $idRoleOld;

        $data = [];
        $data['success'] = $saved;
        $data['role'] = $role;

        return $data;
    }

    public function delete(Role $role)
    {   
        $deleted = $role->delete();

        $data = [];
        $data['success'] = $deleted;
        return $data;
    }
}
