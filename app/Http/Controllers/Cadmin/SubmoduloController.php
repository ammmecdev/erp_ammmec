<?php

namespace App\Http\Controllers\Cadmin;

use Illuminate\Http\Request;
use App\Models\Cadmin\Submodulo;
use App\Http\Controllers\Controller;

class SubmoduloController extends Controller
{
    public function getSubmodulos() {
        return Submodulo::get();
    }

    public function getSubmodulosByModulo($idModulo) {
        return Submodulo::where('idModulo', $idModulo)->get();
    }

    public function store(Request $request) {
        $submodulo = new Submodulo();
        $submodulo->idSubmodulo = $request->idSubmodulo;
        $submodulo->nombre = $request->nombre;
        $submodulo->idModulo = $request->idModulo;
        $saved = $submodulo->save();
        $data = [];
        $data['success'] = $saved;
        $data['submodulo'] = $submodulo;
        return $data;
    }

    public function update(Submodulo $submodulo, Request $request) {
        $submodulo->nombre = $request->nombre;
        $submodulo->idSubmodulo = $request->idSubmodulo;
        $saved = $submodulo->save();
        $data = [];
        $data['success'] = $saved;
        $data['submodulo'] = $submodulo;
        return $data;
    }
}
