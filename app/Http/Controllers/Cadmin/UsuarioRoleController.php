<?php

namespace App\Http\Controllers\Cadmin;

use App\Usuario;
use Illuminate\Http\Request;
use App\Models\Cadmin\UsuarioRole;
use App\Http\Controllers\Controller;

class UsuarioRoleController extends Controller
{
    public function save(Usuario $usuario, Request $request)
    {
        $data_to_sync = [];
        foreach ($request->rolesUsuario as $roleUsuario)
        {
            $pivot_data = ['idSubmodulo' => $roleUsuario['idSubmodulo']];
            $data_to_sync[$roleUsuario['idRole']] = $pivot_data;
        }

        $usuario->roles()->sync($data_to_sync);
        
        // $rolesUsuarioOld = UsuarioRoles::where('idUsuario', $usuario->idUsuario);
        
        // foreach($rolesUsuarioOld as $roleUsuarioOld) {
        //     $existe = false;
        //     foreach($request->rolesUsuario as $roleUsuario) {
        //         if($roleUsuarioOld->idRole == $roleUsuario['idRole'])
        //             $existe = true;
        //     }
        //     if($existe == false)
        //         $roleUsuarioOld->delete();
        // }

        // foreach($request->rolesUsuario as $roleUsuario) {
        //     $obj = null;
        //     $obj = $rolesUsuarioOld->where('idRole', $roleUsuario['idRole']) ->first();
        //     if($obj == null) {
        //         $roleFuncion = new UsuarioRole;
        //         $roleFuncion->idRole = $roleUsuario['idRole'];
        //         $roleFuncion->idUsuario = $roleUsuario['idUsuario'];
        //         $roleFuncion->idSubmodulo = $roleUsuario['idSubmodulo'];
        //         $roleFuncion->save();
        //     }
        // }   

        $data = [];
        $data ['success'] = true;
        return $data;
    }
}
