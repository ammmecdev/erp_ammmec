<?php

namespace App\Http\Controllers\Cadmin;

use Illuminate\Http\Request;
use App\Models\Cadmin\Modulo;
use App\Http\Controllers\Controller;

class ModuloController extends Controller
{
    public function getModulos() {
        return Modulo::get();
    }

    public function store(Request $request) {
        $modulo = new Modulo();
        $modulo->idModulo = $request->idModulo;
        $modulo->nombre = $request->nombre;
        $saved = $modulo->save();
        $data = [];
        $data['success'] = $saved;
        $data['modulo'] = $modulo;
        return $data;
    }
}
