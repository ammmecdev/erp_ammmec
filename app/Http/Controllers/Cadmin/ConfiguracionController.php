<?php

namespace App\Http\Controllers\Cadmin;

use Illuminate\Http\Request;
use App\Models\Cadmin\CuentasExternas;
use App\Http\Controllers\Controller;

class ConfiguracionController extends Controller
{
    public function getConfCuentasExternas()
    {
    	return CuentasExternas::get();
    }

    public function store(Request $request)
    {
		$cuentaExterna = new CuentasExternas;
        $cuentaExterna->idEmbudo = $request->idEmbudo;
        switch ($request->seccion) {
		    case "CG": // Comprobación de gastos
		        $cuentaExterna->fn_cg_etapas = $request->etapas;
		        break;
		    case "RA": // Reporte de actividades
		        $cuentaExterna->op_ra_etapas = $request->etapas;
		        break;
		    case "SA": // Salud
		        $cuentaExterna->se_sa_etapas = $request->etapas;
		        break;
		    case "SS": // Solicitud de Suministros
		        $cuentaExterna->co_ss_etapas = $request->etapas;
		        break;
		    case "SV": // Solicitud de Suministros
		        $cuentaExterna->fn_cg_sv_etapas = $request->etapas;
		        break;

		}
        $saved = $cuentaExterna->save();
        $data = [];
        $data['success'] = $saved;
        $data['cf_cuentaExterna'] = $cuentaExterna;
        return $data;
    }

    public function update(Request $request)
    {
    	$cuentaExterna = CuentasExternas::find($request->id);
        switch ($request->seccion) {
		    case "CG":
		        $cuentaExterna->fn_cg_etapas = $request->etapas;
		        break;
		    case "RA":
		        $cuentaExterna->op_ra_etapas = $request->etapas;
		        break;
		    case "SA":
		        $cuentaExterna->se_sa_etapas = $request->etapas;
		        break;
		    case "SS":
		        $cuentaExterna->co_ss_etapas = $request->etapas;
		        break;
		    case "SV":
		        $cuentaExterna->fn_cg_sv_etapas = $request->etapas;
		        break;
		}
        $saved = $cuentaExterna->update();
        $data = [];
        $data['success'] = $saved;
        $data['cf_cuentaExterna'] = $cuentaExterna;
        return $data;
    }
}
