<?php

namespace App\Http\Controllers\Cadmin;

use Illuminate\Http\Request;
use App\Models\Cadmin\Seccion;
use App\Models\Cadmin\Funcion;
use App\Http\Controllers\Controller;

class SeccionController extends Controller
{
    public function getSeccionesByModulo($idModulo)
    {
        $secciones = Seccion::where('idModulo', $idModulo)->get();
        foreach($secciones as $seccion) 
            $seccion->funciones = $seccion->funciones;
        return $secciones;
    }

    public function store(Request $request)
    {
        $seccion = new Seccion;
        $seccion->idSeccion = $request->idSeccion;
        $seccion->nombre = $request->nombre;
        $seccion->idModulo = $request->idModulo;
        $seccion->idSubmodulo = $request->idSubmodulo;
        $saved = $seccion->save();
        $seccion->funciones = [];

        $data = [];
        $data['success'] = $saved;
        $data['seccion'] = $seccion;

        return $data;
    }

    public function update(Seccion $seccion, Request $request)
    {
        $idSeccionOld = $seccion->idSeccion;
        $seccion->idSeccion = $request->idSeccion;
        $seccion->nombre = $request->nombre;
        $saved = $seccion->save();
        $seccion->idSeccionOld = $idSeccionOld;
        $data = [];
        $data['success'] = $saved;
        $data['seccion'] = $seccion;

        return $data;
    }

    public function delete(Seccion $seccion)
    {   
        $deleted = $seccion->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }
}
