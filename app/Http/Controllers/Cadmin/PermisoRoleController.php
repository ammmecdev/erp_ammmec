<?php

namespace App\Http\Controllers\Cadmin;

use Illuminate\Http\Request;
use App\Models\Cadmin\PermisoRole;
use App\Http\Controllers\Controller;

class PermisoRoleController extends Controller
{

    public function getPermisosByRole($idRole)
    {
        return PermisoRole::where('idRole', $idRole)->get();
    } 

    public function save($idRole, Request $request)
    {
        $permisosOld = PermisoRole::where('idRole', $idRole)->get();
        
        foreach($permisosOld as $permisoOld) {
            $existe = false;
            foreach($request->permisos as $permiso) {
                if($permisoOld->idFuncion == $permiso['idFuncion'])
                    $existe = true;
            }
            if($existe == false)
                $permisoOld->delete();
        }

        foreach($request->permisos as $permiso) {
            $per = null;
            $per = $permisosOld->where('idFuncion', $permiso['idFuncion']) ->first();
            if($per == null) {
                $roleFuncion = new PermisoRole;
                $roleFuncion->idRole = $permiso['idRole'];
                $roleFuncion->idFuncion = $permiso['idFuncion'];
                $roleFuncion->idSeccion = $permiso['idSeccion'];
                $roleFuncion->save();
            }
        }   

        $data = [];
        $data ['success'] = true;
        return $data;
    }
}
