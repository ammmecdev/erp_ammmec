<?php

namespace App\Http\Controllers\Cadmin;

use Illuminate\Http\Request;
use App\Models\Cadmin\Funcion;
use App\Http\Controllers\Controller;

class FuncionController extends Controller
{
    public function store(Request $request)
    {
        $fun = Funcion::where('idFuncion', $request->idFuncion)->first();
        $data = [];

        if($fun == null) {
            $funcion = new Funcion;
            $funcion->idFuncion = $request->idFuncion;
            $funcion->nombre = $request->nombre;
            $funcion->descripcion = $request->descripcion;
            $funcion->idSeccion = $request->idSeccion;
            $saved = $funcion->save();

            $data['success'] = $saved;
            $data['funcion'] = $funcion;

        } else 
            $data['duplicada'] = true;

        return $data;
    }

    public function update(Funcion $funcion, Request $request)
    {
        $idFuncionOld = $funcion->idFuncion;

        $funcion->idFuncion = $request->idFuncion;
        $funcion->nombre = $request->nombre;
        $funcion->descripcion = $request->descripcion;
        $saved = $funcion->save();

        $funcion->idFuncionOld = $idFuncionOld;
        
        $data = [];
        $data['success'] = $saved;
        $data['funcion'] = $funcion;

        return $data;
    }

    public function delete(Funcion $funcion)
    {    
        $deleted = $funcion->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }
}
