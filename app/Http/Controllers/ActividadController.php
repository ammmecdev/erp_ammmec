<?php

namespace App\Http\Controllers;

use App\User;
use Notification;
use App\Actividad;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Events\NotificationToUser;
use App\Notifications\CreateActividad;
use App\Notifications\CreatedActividad;
use App\Notifications\SendNotification;

class ActividadController extends Controller
{
    public function __construct(){
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getActividades(){
        return Actividad::all();
    }

    public function store(Request $request){ 
        $actividad = new Actividad();
        $actividad->idUsuario = $request->idUsuario;
        $actividad->idNegocio = $request->idNegocio;
        $actividad->idCliente = $request->idCliente;
        $actividad->tipo = $request->tipo;
        $actividad->notas = $request->notas;
        $actividad->completado = 0;
        $actividad->fechaActividad = $request->fechaActividad;
        $actividad->horaInicio = $request->horaInicio;
        $actividad->horaFin = $request->horaFin;
        $actividad->nombrePersona = $request->nombrePersona;
        $saved = $actividad->save();
        $data = [];
        $data['success'] = $saved;
        $data['actividad'] = $actividad;

        $this->sendNotificationActividad($actividad);

        return $data;
    }

    public function sendNotificationActividad($actividad){
        $authUser = auth()->user();
        $notification = [];
        $userName = explode(" " , $authUser->nombreUsuario);
        $notification['text'] = $userName[0] . ' ' . $userName[1] . ' te asignó una actividad en el negocio ' . $actividad->clave_negocio . ' ' . $actividad->titulo_negocio;
        $notification['picture'] = '/img/profiles/' . $authUser->foto;
        $notification['route'] = '/ventas/actividades/'. $actividad->idActividad;

        $user = User::find($actividad->idUsuario);

        if($authUser != $user){
            $notification = Notification::send($user, new SendNotification($notification));
            event(new NotificationToUser($user->idUsuario));
        }
    }

    public function deleteActividades($id){
        $actividad = Actividad::find($id);
        $deleted = $actividad->delete();
        $data = [
            'success' => $deleted,
            'message' => 'Se eliminó correctamente'
        ];
        return $data;
    }

    public function updateActividades(Request $request){
        $actividad = Actividad::find($request->idActividad);
        $actividad->idUsuario = $request->idUsuario;
        $actividad->idNegocio = $request->idNegocio;
        $actividad->idCliente = $request->idCliente;
        $actividad->tipo = $request->tipo;
        $actividad->notas = $request->notas;
        $actividad->completado = 0;
        $actividad->fechaActividad = $request->fechaActividad;
        $actividad->horaInicio = $request->horaInicio; 
        $actividad->fechaCompletado = $request->fechaCompletado;
        $actividad->nombrePersona = $request->nombrePersona;
        $updated = $actividad->update();
        $data = [
            'success' => $updated,
            'actividad' => $actividad
        ];
        return $data;
    }

    public function updateActividad(Actividad $actividad, Request $request){
        $actividad->fechaCompletado = $request->fechaCompletado;
        $actividad->comentario = $request->comentario;
        $actividad->completado = 1;
        $updated = $actividad->update();
        $data = [
            'actividad' => $actividad,
            'success' => $updated
        ];
        return $data;
    }

    public function sendEmail(){
        $date = new Carbon();
        $today = $date->format('Y-m-d');
        $actividades = Actividad::all();
        $users = User::all();

        foreach($users as $user){
            $arrayActividades = [];
            foreach($actividades as $actividad){
               $fecha = strcmp($actividad->fechaActividad, $today);
                if($fecha === 0 && $actividad->idUsuario == $user->idUsuario){
                    array_push($arrayActividades, $actividad);
                }
            }
            if($arrayActividades){
                $user->notify(new CreatedActividad($arrayActividades, $user));
            }
                
        }
        return  'Correo enviado exitosamente' ;
    }
}