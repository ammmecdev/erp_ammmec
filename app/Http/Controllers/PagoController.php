<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pago;

class PagoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    public function update(Request $request)
    {
        $pago = Pago::find($request->id);
        $pago->idPago = $request->id;

        switch ($request->campo) {
            case 'fechaPago':
                $pago->fechaPago = $request->value;
                break;
            case 'pagoMXNiva':
                $pago->pagoMXNiva = ($request->value != NULL) ? $request->value : 0;
                break;
            case 'pagoUSDiva':
                $pago->pagoUSDiva = ($request->value != NULL) ? $request->value : 0;
                break;
            case 'fechaRecibo':
                $pago->fechaRecibo = $request->value;
                break;
            case 'noPago':
                $pago->noPago = $request->value;
                break;
            default:
                break;
        }

        $saved = $pago->save();
        $data = [];
        $data['success'] = $saved;
        $data['pago'] = $pago;
        return $data;
    }
}
