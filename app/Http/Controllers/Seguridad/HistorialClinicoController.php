<?php

namespace App\Http\Controllers\Seguridad;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seguridad\HistorialClinico;
use Illuminate\Support\Facades\Storage;

class HistorialClinicoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }

    public function getHistoriales()
    {
    	return HistorialClinico::all();
    }

    public function store(Request $request)
    {
        $historialClinico = new HistorialClinico;
        $historialClinico->tipoExamen = $request->tipoExamen;
        $historialClinico->fecha = $request->fecha;
        $historialClinico->persona = $request->persona;
        $historialClinico->clinica = $request->clinica;
        $historialClinico->costo = $request->costo;
        $historialClinico->resultado = $request->resultados;
        $historialClinico->cuenta = $request->cuenta;
        $historialClinico->observaciones = $request->observaciones;
        if ($request->file('file')) {
	        $path = $request->file('file')->storeAs("SE/Salud/HC", request()->file->getClientOriginalName());
	        $historialClinico->file = $path;
            $historialClinico->nombreFile = $request->file->getClientOriginalName();
	    }else {
	    	$historialClinico->file = '';
            $historialClinico->nombreFile = 'S/A';
	    }
        $historialClinico->visible = ($request->visible == "true") ? true : false;
        $saved = $historialClinico->save();
        $data = [];
        $data['success'] = $saved;
        $data['hc'] = $historialClinico;
        return $data;
    }

    public function update(Request $request)
    {
    	$historialClinico = HistorialClinico::find($request->id);
        $historialClinico->tipoExamen = $request->tipoExamen;
        $historialClinico->fecha = $request->fecha;
        $historialClinico->persona = $request->persona;
        $historialClinico->clinica = $request->clinica;
        $historialClinico->costo = $request->costo;
        $historialClinico->resultado = $request->resultados;
        $historialClinico->cuenta = $request->cuenta;
        $historialClinico->observaciones = $request->observaciones;
        if ($request->file('file')) {
            $hc_length = HistorialClinico::where('file', $historialClinico->file)->count();
            if ($hc_length == 1)
                 Storage::delete($historialClinico->file);
	        $path = $request->file('file')->storeAs("SE/Salud/HC", request()->file->getClientOriginalName());
	        $historialClinico->file = $path;
            $historialClinico->nombreFile = $request->file->getClientOriginalName();
	    }
        $historialClinico->visible = ($request->visible == "true") ? true : false;
        $updated=$historialClinico->update();
        $data=[];
        $data['success']=$updated;
        $data['hc']=$historialClinico;
        return $data;
    }

    public function delete($id)
    {
    	$historialClinico = HistorialClinico::find($id);
        $deleted = $historialClinico->delete();
        $data = [];
        $data['success'] = $deleted;
        return $data;
    }

    public function getTiposExamenes()
    {
    	$historialClinico = HistorialClinico::all();
        $historialClinico = $historialClinico->unique('tipoExamen');
        $tipo_examen = collect();
        $tipo = array();
        foreach($historialClinico as $hc){
            if($hc->tipoExamen != ""){
            	$tipo['tipo'] = $hc->tipoExamen;
                $tipo_examen->push($tipo);
            }
        }
        return $tipo_examen;
    }

    public function getClinicas()
    {
    	$historialClinico = HistorialClinico::all();
        $historialClinico = $historialClinico->unique('clinica');
        $clinicas = collect();
        $clinica = array();
        foreach($historialClinico as $hc){
            if($hc->tipoExamen != ""){
            	$clinica['clinica'] = $hc->clinica;
                $clinicas->push($clinica);
            }
        }
        return $clinicas;
    }

    public function downloadFile($id)
    {
        $hc = HistorialClinico::find($id);   
        return Storage::download($hc->file);
    }
}

