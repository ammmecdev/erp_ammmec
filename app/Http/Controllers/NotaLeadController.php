<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\NotaLead;
use App\User;
use App\Cliente;
use App\Contacto;
use Notification;
use App\Notifications\CreateNota;
use App\Notifications\SendNotification;

class NotaLeadController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
    
    public function getNotas(){
        $nota = NotaLead::all();
        
        return $nota;
    }

    public function selectNotasById($id){
        $nota = NotaLead::find($id);
        
        return $nota;
    }

    public function store(Request $request){
        
        if($request->newClient==true){
            $cliente = new Cliente();
            $cliente->nombre = $request->nombreCliente;
            $cliente->idSector = 1;
            $cliente->idUsuario = auth()->user()->idUsuario;
            $cliente->lead = 1;
            $cliente->save();
            $request->idCliente = $cliente->idCliente;
        }
        if($request->newClient == true || $request->newContact == true){
            $contacto = new Contacto();
            $contacto->nombre = $request->nombreContacto;
            $contacto->telefono = $request->telefonoContacto;
            $contacto->idCliente = $request->idCliente;
            $contacto->idUsuario = auth()->user()->idUsuario;
            $contacto->lead = 1;
            $contacto->save();
            $request->idContacto = $contacto->idContacto;
        }

        $nota = new NotaLead();

        $nota->titulo=$request->titulo;
        $nota->idCliente=$request->idCliente;
        $nota->idContacto=$request->idContacto;
        $nota->idUsuario=$request->idUsuario;
        $saved = $nota->save();
        $data = [];
        $data['success']=$saved;
        $data['nota']=$nota;

        // //NOTIFICATION

        // $authUser = auth()->user();

        // $notification = [];
        // $notification['text'] = $authUser->nombreUsuario . ' creo una nota';
        // $notification['picture'] = '/img/profiles/' . $authUser->foto;
        // $notification['route'] = '/ventas/negocios/notas/'. $nota->idNota;

        // $user = User::find(4); //Mandar a yunuen

        // if($authUser != $user)
        //     $notification = Notification::send($user, new SendNotification($notification));

        return $data;
    }

    public function getComentariosByIdNota(NotaLead $nota)
    {

        $comentario = $nota->comentarios;
        
        $data=[];
        foreach($comentario as $key => $value){
            $data[$key] = [
                'idComentario' => $value->idComentario,
                'contenido' => $value->contenido,
                'tituloNegocio' => $value->tituloNegocio,
                'responsable' => $value->responsable,
                'idNota' => $value->idNota,
                'lead' => $value->lead,
                'nombre_usuario' => $value->nombre_usuario,
                'foto_usuario' => $value->foto_usuario,
                'nombre_cliente' => $nota->nombre_cliente,
                'nombre_contacto' => $nota->nombre_contacto,
                'idCliente' => $nota->idCliente,
                'clave_cliente' => $nota->clave_cliente,
                'idContacto' => $nota->idContacto,
                'fechaCreado' => $value->fechaCreado,
                'fechaActualizado' => $value->fechaActualizado,
                'idUsuario' => $value->idUsuario,
                'file' => $value->file,
                'negocio' => $value->negocio
            ];
        }
        
        

        return $data; 
    }

    public function updateNota(Request $request){
        $nota = NotaLead::find($request->idNota);
        $nota->titulo = $request->titulo;
        $nota->idCliente = $request->idCliente;
        $nota->idContacto = $request->idContacto;
        $nota->idUsuario = $request->idUsuario;
        $updated = $nota->update();
        $data= [
            'success' => $updated,
            'nota' => $nota
        ];
        return $data;
    }

    public function updateFechaNota(Request $request){
        $nota = NotaLead::find($request->idNota);
        $nota->fechaActualizado = $request->fechaActualizado;
        $updated = $nota->update();
        $data= [
            'success' => $updated,
            'nota' => $nota
        ];
        return $data;
        
    }

    public function deleteNota(NotaLead $nota){
        $deleted = $nota->delete();
        $data= [
            'success' => $deleted
        ];
        return $data;

    }
}
