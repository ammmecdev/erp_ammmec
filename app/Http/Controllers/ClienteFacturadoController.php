<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClienteFacturado;
use Illuminate\Support\Facades\DB;
use App\Negocio;
use App\Cliente;

class ClienteFacturadoController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['only' => 'showLoginForm']);
    }
	public function getClientesFacturados(Request $request)
	{
        $clientesFacturados = ClienteFacturado::where('idEmbudo', $request->idEmbudo)->get();

        if (count($clientesFacturados) > 0) {
            foreach($clientesFacturados as $key => $value){
        
                $embudo = DB::table('vt_embudos')
                    ->select('vt_embudos.periodo')
                    ->where('vt_embudos.idEmbudo', $value->idEmbudo)->first();

                
                if ($key == 10) {
                   dd($value->idCliente);
                }
                    
                $fecha_factura = DB::table('negocios')
                    ->join('facturas', 'negocios.idNegocio', '=', 'facturas.idNegocio')
                    ->select('facturas.fechaFactura')
                    ->where('negocios.idCliente', $value->idCliente)
                    ->where('idEmbudo', $request->idEmbudo)
                    ->whereBetween('facturas.fechaFactura', [$embudo->periodo.'-01-1', $embudo->periodo.'-12-31'])
                    ->min('facturas.fechaFactura');

                    
                


                if ($fecha_factura != null) {
                    $dateInteger = strtotime($fecha_factura);
                    $periodo = date("Y", $dateInteger);
                    $facturado = ($periodo == $embudo->periodo) ? true : false;
                }else {
                    $facturado = false;
                }
                
                $cliente = Cliente::where('idCliente', $value->idCliente)->first();

                $clientes[$key] = [
                    'id'  => $value->id,
                    'idCliente' => $value->idCliente,
                    'tipo' => $value->tipo,
                    'clave' => $value->clave,
                    'fecha_factura' => $fecha_factura,
                    'facturado' => $facturado,
                    'nombre' => $cliente->nombre
                ];
            }
            return $clientes;

        }else{
            return [];
        }
	}

    public function selectClientesFacturados(Request $request)
    {
        return ClienteFacturado::where('idEmbudo', 2)->get();
    }

    public function store(Request $request)
    {
        $cliente_facturado = new ClienteFacturado();       
        $cliente_facturado->idCliente = $request->cliente_id;
        $cliente_facturado->tipo = $request->tipo;
        $cliente_facturado->idEmbudo = $request->id_embudo;

        $saved = $cliente_facturado->save();

        
        $data = [];
        $data['success'] = $saved;
        $data['cliente_facturado'] = $cliente_facturado;        
        return $data;
    }

    public function delete(ClienteFacturado $cliente)
    { 
        $removed = $cliente->delete();
        $data = [];
        $data['success'] = $removed;
        $data['cliente_facturado'] = $cliente;
        return $data;
    }

    public function clinteFacturadoById($id)
    {
        $clientesFacturados = ClienteFacturado::where('id', $id)->first();
 
        $fecha_factura = DB::table('negocios')
            ->join('facturas', 'negocios.idNegocio', '=', 'facturas.idNegocio')
            ->select('facturas.fechaFactura')
            ->where('negocios.idCliente', $clientesFacturados->idCliente)
            ->where('idEmbudo', $clientesFacturados->idEmbudo)
            ->min('facturas.fechaFactura');

        $facturado = $fecha_factura != null ? true : false;

        $cliente = Cliente::where('idCliente', $clientesFacturados->idCliente)->first();

        $clientes = [
            'id'  => $clientesFacturados->id,
            'idCliente' => $clientesFacturados->idCliente,
            'tipo' => $clientesFacturados->tipo,
            'clave' => $clientesFacturados->clave,
            'fecha_factura' => $fecha_factura,
            'facturado' => $facturado,
            'nombre' => $cliente->nombre
        ];
        
        return $clientes;
    }
}
