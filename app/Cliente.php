<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $primaryKey = 'idCliente';
	public $timestamps = false;  
	protected $appends=['nombre_sector'];

    public function contacto()
	{
		return $this->hasMany(Contacto::class, 'idCliente');
	}

	public function sector(){
        return $this->belongsTo(Sector::class, 'idSector');
	}
	
    public function negocio()
	{
		return $this->hasMany(Negocio::class, 'idCliente');
	}
	
	public function getNombreSectorAttribute()
	{
        return $this->sector()->first(['nombreSector'])->nombreSector;

    }
}
