<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotaLead extends Model
{
    //
    protected $table='notas_ventas';
    protected $primaryKey='idNota';
    protected $appends=['nombre_cliente', 'nombre_contacto', 'nombre_usuario', 'clave_cliente', 'cliente_lead', 'contacto_lead', 'foto_usuario'];

    const CREATED_AT='fechaCreado';
    const UPDATED_AT='fechaActualizado';

    public function cliente(){
        return $this->belongsTo(Cliente::class,'idCliente');
    }

    public function contacto(){
        return $this->belongsTo(Contacto::class,'idContacto');
    }
   
    public function usuario(){
        return $this->belongsTo(Usuario::class,'idUsuario');
    }
   
    public function getNombreClienteAttribute(){
        return $this->cliente()->first(['nombre'])->nombre;
    }
    
    public function getNombreContactoAttribute(){
        return $this->contacto()->first(['nombre'])->nombre;
    }
    
    public function getNombreUsuarioAttribute(){
        return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
    }

    public function getClaveClienteAttribute(){
        return $this->cliente()->first(['clave'])->clave;
    }

    public function comentarios(){
        return $this->hasMany(ComentarioNotaLead::class,'idNota');
    }

    public function getClienteLeadAttribute(){
        return $this->cliente()->first(['lead'])->lead;
    }

    public function getContactoLeadAttribute(){
        return $this->contacto()->first(['lead'])->lead;
    }

    public function getFotoUsuarioAttribute(){
        return $this->usuario()->first(['foto'])->foto;
    }
}
