<?php

namespace App\Observers;

use App\Actividad;
use App\Events\ActividadSend;
use App\Notifications\CreateActividad;
use Illuminate\Support\Facades\Notification;


class ActividadObserver
{
    /**
     * Handle the Actividad "created" event.
     *
     * @param  \App\Actividad  $actividad
     * @return void
     */
    public function created(Actividad $actividad)
    {
        //event(new ActividadSend($actividad));
    }

    /**
     * Handle the Actividad "updated" event.
     *
     * @param  \App\Actividad  $actividad
     * @return void
     */
    public function updated(Actividad $actividad)
    {
        // $user = auth()->user();
        // Notification::send($user, new CambioEtapa($negocio));
        // event(new NegocioSend($negocio));
    }

    /**
     * Handle the Actividad "deleted" event.
     *
     * @param  \App\Actividad  $actividad
     * @return void
     */
    public function deleted(Actividad $actividad)
    {
        //
    }
}