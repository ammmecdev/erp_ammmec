<?php

namespace App\Observers\Finanzas\CG;

use App\Models\Finanzas\CG\Solicitud;
use Illuminate\Support\Facades\Notification;
use App\Events\Finanzas\CG\SolicitudSend;
use App\Events\Finanzas\CG\SolicitudUpdate;

class SolicitudObserver
{
    public function created(Solicitud $solicitud)
    {
        event(new SolicitudSend($solicitud, $solicitud->usuario));
    }

    public function updated(Solicitud $solicitud)
    {
        event(new SolicitudUpdate($solicitud, $solicitud->usuario));
    }
}
