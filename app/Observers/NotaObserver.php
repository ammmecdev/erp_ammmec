<?php

namespace App\Observers;

use App\NotaLead;
use App\Events\NotaSend;
use App\Notifications\CreateNota;
use Illuminate\Support\Facades\Notification;


class NotaObserver
{
    /**
     * Handle the Nota "created" event.
     *
     * @param  \App\NotaLead  $nota
     * @return void
     */
    public function created(NotaLead $nota)
    {
        // event(new NotaSend($nota));
    }

    /**
     * Handle the Nota "updated" event.
     *
     * @param  \App\NotaLead  $nota
     * @return void
     */
    public function updated(NotaLead $nota)
    {
        // $user = auth()->user();
        // Notification::send($user, new CambioEtapa($negocio));
        // event(new NegocioSend($negocio));
    }

    /**
     * Handle the Nota "deleted" event.
     *
     * @param  \App\NotaLead  $nota
     * @return void
     */
    public function deleted(NotaLead $nota)
    {
        //
    }
}