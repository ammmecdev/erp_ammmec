<?php

namespace App\Observers;

use App\Negocio;
use App\Events\NegocioSend;
use App\Notifications\CambioEtapa;
use Illuminate\Support\Facades\Notification;


class NegocioObserver
{
    /**
     * Handle the Negocio "created" event.
     *
     * @param  \App\Negocio  $negocio
     * @return void
     */
    public function created(Negocio $negocio)
    {
       
    }

    /**
     * Handle the Negocio "updated" event.
     *
     * @param  \App\Negocio  $negocio
     * @return void
     */
    public function updated(Negocio $negocio) 
    {
        // $user = auth()->user();
        // Notification::send($user, new CambioEtapa($negocio));
        // event(new NegocioSend($negocio));
    }

    /**
     * Handle the Negocio "deleted" event.
     *
     * @param  \App\Negocio  $negocio
     * @return void
     */
    public function deleted(Negocio $negocio)
    {
        //
    }
}