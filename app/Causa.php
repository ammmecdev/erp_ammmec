<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Causa extends Model
{
   protected $primaryKey = 'idcausasPerdida';
   protected $table = 'causaperdida';
   public $timestamps = false;
}
