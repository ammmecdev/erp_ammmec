<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstruccionEmpleado extends Model
{
 	protected $table = 'instrucciones_empleados';
    protected $primaryKey = 'idInstruccionEmpleado';
    public $timestamps = false;
}
