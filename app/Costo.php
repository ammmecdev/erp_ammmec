<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Costo extends Model
{
	protected $table = 'costos';
	protected $primaryKey = 'idCosto';
	
	const CREATED_AT = 'fechaCreado';
    const UPDATED_AT = 'fechaActualizado';

	protected $appends = ['titulo_negocio', 'clave_negocio'];

	public function getTituloNegocioAttribute()
	{
		return $this->negocio()->first(['tituloNegocio'])->tituloNegocio;
	}

	public function getClaveNegocioAttribute()
	{
		return $this->negocio()->first(['claveNegocio'])->claveNegocio;
	}

	public function negocio()
    {
        return $this->belongsTo(Negocio::class,'idNegocio');
    }

}
