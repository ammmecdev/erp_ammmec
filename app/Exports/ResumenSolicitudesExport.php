<?php

namespace App\Exports;

use App\Models\Finanzas\CG\Resumen;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;


class ResumenSolicitudesExport implements FromView
{
    use Exportable;

    private $resumen;

    public function __construct($resumen)
    {
        $this->resumen = $resumen;
    }

    public function view(): View
    {
        return view("modulos.finanzas.exports.resumen", [
            'resumen' => $this->resumen
        ]);
    }
}
