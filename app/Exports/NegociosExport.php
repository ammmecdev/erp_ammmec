<?php

namespace App\Exports;

use App\Negocio;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;
// use Maatwebsite\Excel\Concerns\FromCollection;

class NegociosExport implements FromView
{
    use Exportable;
    private $negocios;

    public function __construct($negocios)
    {
        $this->negocios = $negocios;
    }

    public function view(): View
    {
        return view("modulos.ventas.exports.negocios", [
            'negocios' => $this->negocios
        ]);
    }
}
