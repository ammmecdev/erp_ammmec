<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class ResumenSolicitudesComprasExport implements FromView
{
    use Exportable;

    private $resumen;

    public function __construct($resumen)
    {
        $this->resumen = $resumen;
    }

    public function view(): View
    {
        return view("modulos.compras.exports.resumen", [
            'resumen' => $this->resumen
        ]);
    }
}
