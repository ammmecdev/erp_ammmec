<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;

class ProveedoresExport implements FromView
{
    use Exportable;

    private $proveedores;

    public function __construct($proveedores)
    {
        $this->proveedores = $proveedores;
    }

    public function view(): View
    {
        return view("modulos.compras.exports.proveedores", [
            'proveedores' => $this->proveedores
        ]);
    }
}
