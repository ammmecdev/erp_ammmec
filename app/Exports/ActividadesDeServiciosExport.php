<?php

namespace App\Exports;

use App\Models\Servicios\Actividad;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;


class ActividadesDeServiciosExport implements FromView
{
    use Exportable;

    private $empleados;
    private $area;

    public function __construct($empleados, $area)
    {
    	$this->empleados = $empleados;
        $this->area = $area;
    }

    public function view(): View
    {
	    return view("modulos.servicios.exports.actividades", [
        	'empleados' => $this->empleados,
            'area' => $this->area,
        ]);
    }
}
