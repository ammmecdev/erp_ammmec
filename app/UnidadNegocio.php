<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnidadNegocio extends Model
{
    protected $table = 'unidad_negocio';
    protected $primaryKey = 'idUnidadNegocio';
    public $timestamps = false;
}
