<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Contacto extends Model
{
    protected $primaryKey = 'idContacto';
    
    public $timestamps = false;
   protected $appends = ['nombre_cliente'];

    public function getNombreClienteAttribute(){
        return $this->cliente()->first(['nombre'])->nombre;
    }

    public function cliente(){
        return $this->belongsTo(Cliente::class, 'idCliente');
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class,'idUsuario');
    }

    public function telefono(){
        return $this->hasMany(Telefono::class, 'idTelefono');
    }

    public function negocio(){
        return $this->belongsToMany(Negocio::class)->as('negocio_contacto');
    }

    // public function participante(){
    //     return $this->belongsTo(Participante::class, 'idParticipante');
    // }

}
