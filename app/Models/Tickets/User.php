<?php

namespace App\Models\Tickets;

use App\Models\Tickets\Message;
use App\Models\Tickets\Service;
use App\Models\Tickets\Ticket;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuarios';
    protected $primaryKey = 'idUsuario';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombreUsuario', 'is_admin_tk'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'is_admin_tk' => 'boolean'
    ];

    static public function findById($id)
    {
        return static::where(compact('id'))->first();
    }

    public static function findByEmail($email){
        return static::where(compact('email'))->first();
    } 

    public function isAdmin()
    {
        return $this->is_admin_tk;
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function services(){
        return $this->belongsToMany(Service::class, 'tk_service_user', 'user_id', 'services_id');
    }

    public function tickets(){
        return $this->belongsToMany(Ticket::class, 'tk_ticket_user', 'user_id', 'ticket_id');
    }

    public function tickets_user_limit(){
        return $this->belongsToMany(Ticket::class, 'tk_ticket_user', 'user_id', 'ticket_id')->orderby('id','DESC')->limit(5);
    }

    public function tickets_limit()
    {
        return $this->hasMany(Ticket::class)->orderby('id','DESC')->limit(5);
    }  

    public function tickets_all()
    {
        return $this->hasMany(Ticket::class)->orderby('id','DESC');
    } 

    public static function findByStatus($filter)
    {
        $user = $filter == "Admin" ? 1 : ($filter == "Regular" ? 0 : "");
        return static::where('is_admin_tk', 'like', '%' . $user . '%')->orderby('id','ASC')->get();
    }  
}
