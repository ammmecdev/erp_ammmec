<?php

namespace App\Models\Tickets;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
	protected $table = 'tk_subjects';

	protected $guarded = ['id'];
	
	protected $fillable = [
		'name', 'service_id', 
	];

	public function service(){
		return $this->belongsTo(Service::class);
	} 

	public static function findById($service_id){
		return static::where(compact('service_id'))->get();
	} 
}
