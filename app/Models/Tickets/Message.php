<?php

namespace App\Models\Tickets;

use App\Models\Tickets\User;
use App\Models\Tickets\File;
use App\Models\Tickets\Ticket;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
	protected $table = 'tk_messages';

    protected $fillable = [
		'message', 'ticket_id', 'user_id'
	];

	protected $appends = ['user_name', 'user_picture'];

	public function getUserNameAttribute()
	{
		return $this->user()->first(['nombreUsuario'])->nombreUsuario;
	}

	public function getUserPictureAttribute()
	{
		return $this->user()->first(['foto'])->foto;
	}

    public function ticket(){
		return $this->belongsTo(Ticket::class);
	}

	public function user(){
		return $this->belongsTo(User::class, 'user_id', 'idUsuario');
	}

	public function files()
	{
		return $this->hasMany(File::class);
	}
}
