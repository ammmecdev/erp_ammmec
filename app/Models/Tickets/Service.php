<?php

namespace App\Models\Tickets;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\Subject;
use App\Models\Tickets\Area;
use App\Models\Tickets\User;

class Service extends Model
{
	protected $table = 'tk_services';

	protected $guarded = ['id'];

	protected $fillable = [
		'name', 'status', 'description', 'departament_id', 
	];

	public function tickets(){
		return $this->hasMany(Ticket::class);
	} 

	public function subjects(){
		return $this->hasMany(Subject::class);
	}

	public function department(){
		return $this->belongsTo(Area::class);
	} 

	public function users(){
		return $this->belongsToMany(User::class, 'tk_service_user', 'service_id', 'user_id');
	} 

	public static function findById($departament_id){
		return static::where(compact('departament_id'))->get();
	} 

	static public function findByStatus($status){
		if ($status == 'All') {
			return static::where('departament_id', auth()->user()->department->idDepartamento)
			->orderby('id','DESC')->get();
		}else{
			return static::where('status', $status)
			->where('departament_id', auth()->user()->department->idDepartamento)
			->orderby('id','DESC')->get();
		}
	}
}
