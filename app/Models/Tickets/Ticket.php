<?php

namespace App\Models\Tickets;

use App\Models\Tickets\User;
use App\Models\Tickets\File;
use App\Models\Tickets\Message;
use App\Models\Tickets\Service;
use App\Traits\DatesTranslator;
use App\Models\Tickets\Department;
use Illuminate\Support\Facades\DB;
use App\Events\Tickets\TicketCreated;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
	// use DatesTranslator;
	protected $table = 'tk_tickets';

	protected $fillable = [
		'subject', 'status', 'service_id', 'user_id', 'department_id'
	];

	protected $guarded = ['id'];

	// protected $dispatchesEvents = [
	// 	'created' => TicketCreated::class
	// ];

	protected $appends = ['user_name', 'user_email', 'service_name', 'user_picture'];

	public function getUserNameAttribute()
	{
		return $this->user()->first(['nombreUsuario'])->nombreUsuario;
	}

	public function getUserEmailAttribute()
	{
		return $this->user()->first(['email'])->email;
	}

	public function getUserPictureAttribute()
	{
		return $this->user()->first(['foto'])->foto;
	}

	public function getServiceNameAttribute()
	{
		return $this->service()->first(['name'])->name;
	}

	public function messages(){
		return $this->hasMany(Message::class)->orderby('id','DESC');
	} 

	public function files(){
		return $this->hasMany(File::class);
	} 

	public function user(){
		return $this->belongsTo(User::class, 'user_id', 'idUsuario');
	}

	public function users(){
		return $this->belongsToMany(User::class, 'tk_ticket_user', 'ticket_id', 'user_id');
	}

	public function service(){
		return $this->belongsTo(Service::class);
	}

	public function departament(){
		return $this->belongsTo(Department::class, 'departament_id', 'idDepartamento');
	}

	static public function findById()
	{
		return static::select('id')->orderby('created_at','DESC')->first();
	}

	static public function findByStatus($status)
	{
		return static::where('status','like', '%' . $status . '%')
		->where('user_id','=',auth()->user()->id)
		->orderby('id','DESC')->get();
	}

	public static function findStatusCount()
	{
		return DB::table('tickets')->select('status', DB::raw('count(*) as status_count'))->groupBy('status')->where('department_id', '=', auth()->user()->department->id)->get();
	}	
}
