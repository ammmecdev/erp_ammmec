<?php

namespace App\Models\Tickets;

use App\Models\Tickets\User;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\Service;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
	protected $table = 'rh_departamentos';
	protected $primaryKey = 'idDepartamento';

	protected $fillable = [
		'nombre', 'codigo'
	];

	protected $guarded = ['id'];

	public function services(){
		return $this->hasMany(Service::class);
	} 

	public function users(){
		return $this->hasMany(User::class);
	} 

	public function tickets(){
		return $this->hasMany(Ticket::class);
	}
}
