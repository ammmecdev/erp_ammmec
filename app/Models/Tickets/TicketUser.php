<?php

namespace App\Models\Tickets;

use Illuminate\Database\Eloquent\Model;

class TicketUser extends Model
{
    protected $table = 'tk_ticket_user';
}
