<?php

namespace App\Models\Tickets;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	protected $table = 'tk_notification';

    protected $fillable = [
		'id', 'data' 
	];
}
