<?php

namespace App\Models\Tickets;

use App\Models\Tickets\Message;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	protected $table = 'tk_files';

    protected $fillable = [
		'name', 'type', 'route', 'message_id' 
	];
    
    public function message(){
		return $this->belongsTo(Message::class);
	}
}
