<?php

namespace App\Models\Tickets;

use App\Models\Tickets\User;
use App\Models\Tickets\Ticket;
use App\Models\Tickets\Service;
use Illuminate\Database\Eloquent\Model;

class Area extends Model
{
    protected $table = 'rh_areas';
    protected $primaryKey = 'idArea';
    protected $guarded = ['idArea'];

    public function services()
    {
        return $this->hasMany(Service::class, 'area_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'idArea');
    }

    public function admin_users()
    {
        return $this->hasMany(User::class,'idArea')->where('is_admin_tk', '1');
    }

    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
