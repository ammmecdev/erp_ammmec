<?php

namespace App\Models\Finanzas;

use Illuminate\Database\Eloquent\Model;

class CuentaContable extends Model
{
    protected $table = 'fn_cuentas_contables';
    protected $primaryKey = 'idCuenta';
    public $timestamps = false;
}
