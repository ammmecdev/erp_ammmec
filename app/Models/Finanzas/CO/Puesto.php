<?php

namespace App\Models\Finanzas\CO;

use Illuminate\Database\Eloquent\Model;

class Puesto extends Model
{
    protected $table = 'fn_co_puestos';
    protected $primaryKey = 'idPuesto';
    public $timestamps = false;
}
