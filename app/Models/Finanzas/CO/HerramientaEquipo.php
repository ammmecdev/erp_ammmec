<?php

namespace App\Models\Finanzas\CO;

use Illuminate\Database\Eloquent\Model;

class HerramientaEquipo extends Model
{
    protected $table = 'fn_co_herramienta_equipos';
    protected $primaryKey = 'idHerramienta';
    public $timestamps = false;
}
