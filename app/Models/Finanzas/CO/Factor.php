<?php

namespace App\Models\Finanzas\CO;

use Illuminate\Database\Eloquent\Model;

class Factor extends Model
{
    protected $table = 'fn_co_factores';
    protected $primaryKey = 'idFactor';
    public $timestamps = false;
}
