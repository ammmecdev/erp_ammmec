<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    protected $table = 'fn_cg_archivos';
    protected $primaryKey = 'idArchivo';
    public $timestamps = false;
}
