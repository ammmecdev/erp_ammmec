<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;
use App\Models\Finanzas\CG\Solicitud;


class Comprobacion extends Model
{
    protected $table = 'fn_cg_comprobaciones';
    protected $primaryKey = 'idComprobacion';
   
	public function solicitud()
    {
        return $this->belongsTo(Solicitud::class, 'idSolicitud', 'idSolicitud');
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'idUsuario');
    }
}
