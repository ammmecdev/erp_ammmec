<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;

class PlaneacionGasto extends Model
{
    protected $table = 'fn_cg_planeacion_gasto';
    protected $primaryKey = 'idPlaneacion';
    public $timestamps = false;
}
