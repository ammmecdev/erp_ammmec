<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;
use App\Usuario;
use App\Models\Finanzas\CG\Comentario;
use App\Models\Finanzas\CG\Resumen;
use App\Models\Finanzas\CG\Movimiento;
use App\Models\Finanzas\CG\Etapa;

class Solicitud extends Model
{
    protected $table = 'fn_cg_solicitudes';
    protected $primaryKey = 'idSolicitud';

    protected $appends = [
        'nombre_etapa', 
        'nombre_asignador', 
        'nombre_usuario', 
        'ruta_usuario'
    ];
    
    public function getNombreEtapaAttribute()
    {
        return $this->etapa()->first(['nombre'])->nombre;
    }

    public function getNombreUsuarioAttribute()
    {
        return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
    }

    public function getRutaUsuarioAttribute()
    {
        return $this->usuario()->first(['ruta'])->ruta;
    }

    public function getNombreAsignadorAttribute()
    {
        return $this->asignador()->first(['nombreUsuario'])["nombreUsuario"];
    }

    public function usuario()
    {
    	return $this->belongsTo(Usuario::class, 'idUsuario');
    }

    public function asignador()
    {
        return $this->belongsTo(Usuario::class, 'idAsignador', 'idUsuario')->withDefault();
    }

    public function movimientos()
    {
        return $this->hasMany(Movimiento::class, 'idSolicitud', 'idSolicitud');
    }

    public function etapa() 
    {
        return $this->belongsTo(Etapa::class, 'idEtapa', 'idEtapa');
    }    
}
