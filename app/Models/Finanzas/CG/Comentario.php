<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = 'fn_cg_comentarios';
    protected $primaryKey = 'idComentario';
}
