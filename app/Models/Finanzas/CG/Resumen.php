<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;

class Resumen extends Model
{
    protected $table = 'fn_cg_resumenes';
    protected $primaryKey = 'idImporte';
    public $timestamps = false;
}
