<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;
use App\Usuario;

class Autorizacion extends Model
{
    protected $table = 'fn_cg_autorizaciones';
    protected $primaryKey = 'idAutorizacion';
    public $timestamps = false;

    protected $appends = ['nombre_usuario'];

    public function usuario()
    {
    	return $this->belongsTo(Usuario::class, 'idUsuario');
    }
    
    public function getNombreUsuarioAttribute() 
	{
		return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
	}
}
