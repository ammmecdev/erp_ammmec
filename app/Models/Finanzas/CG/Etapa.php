<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;

class Etapa extends Model
{
    protected $table = 'fn_cg_etapas';
    protected $primaryKey = 'idEtapa';
    public $timestamps = false;
}
