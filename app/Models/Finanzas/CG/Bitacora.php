<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
    protected $table = 'fn_cg_bitacoras';
    protected $primaryKey = 'idBitacora';
}
