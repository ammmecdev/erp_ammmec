<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;

class DisposicionEfectivo extends Model
{
    protected $table = 'fn_cg_disposicion_efectivo';
    protected $primaryKey = 'idDisposicion';
    public $timestamps = false;
}
