<?php

namespace App\Models\Finanzas\CG;

use Illuminate\Database\Eloquent\Model;
use App\Models\Finanzas\CG\Solicitud;
use App\Models\Finanzas\CG\Comprobacion;
use App\Usuario;

class Movimiento extends Model
{
    protected $table = 'fn_cg_movimientos';
    protected $primaryKey = 'idMovimiento';

    public function solicitud()
    {
        return $this->belongsTo(Solicitud::class, 'idSolicitud', 'idSolicitud');
    }

    public function comprobacion()
    {
        return $this->belongsTo(Comprobacion::class, 'idComprobacion', 'idComprobacion');
    }

    public function usuario()
    {
    	return $this->belongsTo(Usuario::class, 'idUsuario');
    }
}
