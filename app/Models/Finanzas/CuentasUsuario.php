<?php

namespace App\Models\Finanzas;

use Illuminate\Database\Eloquent\Model;

class CuentasUsuario extends Model
{
    protected $table = 'fn_cuentas_usuarios';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
