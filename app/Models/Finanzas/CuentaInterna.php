<?php

namespace App\Models\Finanzas;

use Illuminate\Database\Eloquent\Model;

class CuentaInterna extends Model
{
    protected $table = 'fn_cuentas_internas';
    protected $primaryKey = 'idCuenta';
    public $timestamps = false;
}
