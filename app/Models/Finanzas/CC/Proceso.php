<?php

namespace App\Models\Finanzas\CC;

use Illuminate\Database\Eloquent\Model;
use App\Models\Finanzas\CC\Departamento;

class Proceso extends Model
{
    protected $table = 'fn_cc_procesos';
    protected $primaryKey = 'idProceso';
    public $timestamps = false;

    protected $appends = ['departamento', 'codigo_departamento'];
   
    public function getDepartamentoAttribute()
    {
        return $this->departamento()->first(['nombre'])->nombre;
    }

    public function getCodigoDepartamentoAttribute()
    {
        return $this->departamento()->first(['codigo'])->codigo;
    }

    public function departamento()
    {
        return $this->belongsTo(Departamento::class, 'idDepartamento');
    }
}
