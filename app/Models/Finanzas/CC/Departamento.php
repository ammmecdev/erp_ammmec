<?php

namespace App\Models\Finanzas\CC;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'fn_cc_departamentos';
    protected $primaryKey = 'idDepartamento';
    public $timestamps = false;
}
