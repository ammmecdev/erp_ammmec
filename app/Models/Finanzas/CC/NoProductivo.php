<?php

namespace App\Models\Finanzas\CC;

use Illuminate\Database\Eloquent\Model;

class NoProductivo extends Model
{
    protected $table = 'fn_cc_no_productivos';
    protected $primaryKey = 'idNoProductivo';
    public $timestamps = false;
}
