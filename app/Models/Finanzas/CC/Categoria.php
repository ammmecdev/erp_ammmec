<?php

namespace App\Models\Finanzas\CC;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'fn_cc_categorias';
    protected $primaryKey = 'idCategoria';
    public $timestamps = false;
}
