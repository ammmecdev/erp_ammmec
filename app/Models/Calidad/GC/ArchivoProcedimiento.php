<?php

namespace App\Models\Calidad\GC;

use Illuminate\Database\Eloquent\Model;

class ArchivoProcedimiento extends Model
{
    protected $table = 'nr_gc_archivos_procedimientos';
    protected $primaryKey = 'idArchivo';
}
