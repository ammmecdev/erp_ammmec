<?php

namespace App\Models\Calidad\GC;

use Illuminate\Database\Eloquent\Model;
use App\Models\Calidad\GC\Procedimiento;

class Proceso extends Model
{
    protected $table = 'nr_gc_procesos';
    protected $primaryKey = 'idProceso';

    public function procedimientos()
    {	
    	return $this->hasMany(Procedimiento::class, 'idProceso');
    }
}
