<?php

namespace App\Models\Calidad\GC;

use App\Models\TalentoHumano\Puesto;
use Illuminate\Database\Eloquent\Model;

class PerfilPuesto extends Model
{
	protected $table = 'nr_gc_perfiles_puestos';
    protected $primaryKey = 'idPerfil';

    // public function modulo(){
    //     return $this->belongsTo(Modulo::class,'idModulo');
    // }
    // public function nivel(){
    //     return $this->belongsTo(Nivel::class,'idNivel');
    // }
    // 
    public function puesto()
    {
    	return $this->belongsTo(Puesto::class, 'idPuesto');
    }
}
