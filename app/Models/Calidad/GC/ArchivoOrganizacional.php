<?php

namespace App\Models\Calidad\GC;

use Illuminate\Database\Eloquent\Model;

class ArchivoOrganizacional extends Model
{
    protected $table = 'nr_gc_archivos_organizacionales';
    protected $primaryKey = 'idArchivo';
}
