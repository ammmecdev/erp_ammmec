<?php

namespace App\Models\Calidad\GC;

use Illuminate\Database\Eloquent\Model;

class ManualDepartamental extends Model
{
    protected $table = 'nr_gc_manual_departamental';
    protected $primaryKey = 'idManual';
}
