<?php

namespace App\Models\Calidad\GC;

use Illuminate\Database\Eloquent\Model;
use App\Models\Calidad\GC\Proceso;
use Illuminate\Support\Facades\DB;

class Procedimiento extends Model
{
	protected $table = 'nr_gc_procedimientos';
    protected $primaryKey = 'idProcedimiento';

    public function proceso()
    {	
    	return $this->belongsTo(Proceso::class, 'idProceso');
    }

    public function getArchivosByProcedimiento($idProcedimiento)
    {
    	return DB::table('archivos_procedimientos')
		 	->join('procedimientos', 'archivos_procedimientos.idProcedimiento', '=', 'procedimientos.idProcedimiento')
		 	->join('procesos', 'procedimientos.idProceso', '=', 'procesos.idProceso')
		 	->join('areas', 'procesos.idArea', '=', 'areas.idArea')
	        ->select('archivos_procedimientos.*')
	        ->where('prcedimientos.idProcedimiento', $idProcedimiento)
	        ->get();
    }
}
