<?php

namespace App\Models\Planes;

use Illuminate\Database\Eloquent\Model;

class PlanUsuario extends Model
{
    protected $table = 'planes_usuarios';
    protected $primaryKey = 'id';
}
