<?php

namespace App\Models\Planes;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'planes_actividades';
    protected $primaryKey = 'idActividad';
}
