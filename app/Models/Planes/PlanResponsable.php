<?php

namespace App\Models\Planes;

use Illuminate\Database\Eloquent\Model;

class PlanResponsable extends Model
{
    protected $table = 'planes_responsables';
    protected $primaryKey = 'idResponsable';
    public $timestamps = false;
}
