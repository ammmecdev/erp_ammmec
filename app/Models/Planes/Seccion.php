<?php

namespace App\Models\Planes;

use Illuminate\Database\Eloquent\Model;

class Seccion extends Model
{
    protected $table = 'planes_secciones';
    protected $primaryKey = 'idSeccion';
    public $timestamps = false;

    public function actividades()
    {   
        return $this->hasMany(Actividad::class, 'idSeccion')->orderBy('idActividad');
    }
}
