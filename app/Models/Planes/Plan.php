<?php

namespace App\Models\Planes;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $table = 'planes';
    protected $primaryKey = 'idPlan';

    public function secciones()
    {   
        return $this->hasMany(Seccion::class, 'idPlan')->orderBy('idSeccion');
    }

    public function actividades()
    {   
        return $this->hasMany(Actividad::class, 'idPlan')->orderBy('inicioP');
    }

    public function responsables()
    {   
        return $this->hasMany(PlanResponsable::class, 'idPlan')->orderBy('idResponsable');
    }

    public function usuarios()
    {   
        return $this->hasMany(PlanUsuario::class, 'idPlan')->orderBy('id');
    }
}
