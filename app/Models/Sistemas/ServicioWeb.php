<?php

namespace App\Models\Sistemas;

use Illuminate\Database\Eloquent\Model;

class ServicioWeb extends Model
{
    protected $table = 'si_servicios_web';
    protected $primaryKey = 'idServicio';
    public $timestamps = false;
}
