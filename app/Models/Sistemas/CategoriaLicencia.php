<?php

namespace App\Models\Sistemas;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sistemas\LicenciaSoftware;

class CategoriaLicencia extends Model
{
    protected $table = 'si_categorias_licencias';
    protected $primaryKey = 'idCategoria';
    public $timestamps = false;

    public function licencias()
	{
		return $this->hasMany(LicenciaSoftware::class, 'idCategoria');
	}
}
