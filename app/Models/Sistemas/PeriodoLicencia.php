<?php

namespace App\Models\Sistemas;

use Illuminate\Database\Eloquent\Model;

class PeriodoLicencia extends Model
{
    protected $table = 'si_periodos_licencias';
    protected $primaryKey = 'idPeriodo';
}
