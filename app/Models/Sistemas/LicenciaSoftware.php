<?php

namespace App\Models\Sistemas;

use Illuminate\Database\Eloquent\Model;

class LicenciaSoftware extends Model
{
    protected $table = 'si_licencias_software';
    protected $primaryKey = 'idLicencia';

    public function periodos()
    {	
    	return $this->hasMany(PeriodoLicencia::class, 'idLicencia');
    }
}
