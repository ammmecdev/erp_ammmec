<?php

namespace App\Models\Sistemas;

use Illuminate\Database\Eloquent\Model;
use App\Models\Sistemas\ServicioWeb;

class CuentaServicio extends Model
{
    protected $table = 'si_cuentas_servicios';
    protected $primaryKey = 'idCuentaServicio';
    public $timestamps = false;

    protected $appends = ['nombre_servicio'];

    public function getNombreServicioAttribute()
    {
        return $this->servicio()->first(['nombre'])->nombre;
    }

    public function servicio()
    {
        return $this->belongsTo(ServicioWeb::class, 'idServicio');
    }
}
