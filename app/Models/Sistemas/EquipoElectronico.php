<?php

namespace App\Models\Sistemas;

use Illuminate\Database\Eloquent\Model;

class EquipoElectronico extends Model
{
    protected $table = 'si_equipos_electronicos';
    protected $primaryKey = 'idEquipo';
}
