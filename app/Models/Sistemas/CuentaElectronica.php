<?php

namespace App\Models\Sistemas;

use Illuminate\Database\Eloquent\Model;

class CuentaElectronica extends Model
{
    protected $table = 'si_cuentas_electronicas';
    protected $primaryKey = 'idCuenta';
}
