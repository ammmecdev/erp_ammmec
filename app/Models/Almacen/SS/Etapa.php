<?php

namespace App\Models\Almacen\SS;

use Illuminate\Database\Eloquent\Model;

class Etapa extends Model
{
    protected $table = 'alm_ss_etapas';
    protected $primaryKey = 'idEtapa';
    public $timestamps = false;
}
