<?php

namespace App\Models\Almacen\SS;

use Illuminate\Database\Eloquent\Model;
use App\Models\Almacen\SS\Etapa;
use App\Usuario;

class SolicitudSuministro extends Model
{
    protected $table = 'alm_ss_solicitudes';
    protected $primaryKey = 'idSolicitud';
    public $timestamps = true;

    protected $appends = ['nombre_etapa'];

    public function getNombreEtapaAttribute()
    {
        return $this->etapa()->first(['nombre'])->nombre;
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'idUsuario');
    }

    public function etapa() 
    {
        return $this->belongsTo(Etapa::class, 'idEtapa', 'idEtapa');
    }

    public function articulos()
    {
        return $this->hasMany(Articulo::class, 'idSolicitud');
    }
}
