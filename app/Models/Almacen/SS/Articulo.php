<?php

namespace App\Models\Almacen\SS;

use Illuminate\Database\Eloquent\Model;
use App\Models\Almacen\Inventarios\Activo;


class Articulo extends Model
{
    protected $table = 'alm_ss_articulos';
    protected $primaryKey = 'idArticulo';
    public $timestamps = true;

    public function activo()
    {
        return $this->belongsTo(Activo::class, 'idActivo', 'idActivo');
    }
}
