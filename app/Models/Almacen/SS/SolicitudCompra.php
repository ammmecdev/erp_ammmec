<?php

namespace App\Models\Almacen\SS;

use Illuminate\Database\Eloquent\Model;
use App\Models\Compras\SC\Etapa;

class SolicitudCompra extends Model
{
    protected $table = 'alm_ss_solicitudes_compra';
    protected $primaryKey = 'idSolicitudCompra';
    public $timestamps = true;
}
