<?php

namespace App\Models\Almacen\Inventarios;

use Illuminate\Database\Eloquent\Model;

class Activo extends Model
{
    protected $table = 'co_ss_activos';
    protected $primaryKey = 'idActivo';
    public $timestamps = true;
}
