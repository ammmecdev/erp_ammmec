<?php

namespace App\Models\Cadmin;

use Illuminate\Database\Eloquent\Model;
use App\Usuario;

class Role extends Model
{
    protected $table = 'ad_roles';
    protected $primaryKey = 'idRole';
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $casts = [
    //     'idModulo' => 'string',
    // ];

    public function permisos() 
    {
        return $this->hasMany('App\Models\Cadmin\PermisoRole', 'idRole', 'idRole');
    }

     public function usuarios() 
    {
        return $this->belongsToMany(Usuario::class, 'ad_usuarios_roles', 'idRole', 'idUsuario')->withPivot('idSubmodulo');
    }
}
