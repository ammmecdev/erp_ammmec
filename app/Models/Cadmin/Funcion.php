<?php

namespace App\Models\Cadmin;

use Illuminate\Database\Eloquent\Model;

class Funcion extends Model
{
    protected $table = 'ad_funciones';
    protected $primaryKey = 'idFuncion';
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $casts = [
    //     'idFuncion' => 'string',
    // ];
}
