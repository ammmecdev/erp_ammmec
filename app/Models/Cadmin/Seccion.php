<?php

namespace App\Models\Cadmin;

use Illuminate\Database\Eloquent\Model;
use App\Models\Cadmin\Funcion;

class Seccion extends Model
{
    protected $table = 'ad_secciones';
    protected $primaryKey = 'idSeccion';
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';
    
    public function funciones()
    {
        return $this->hasMany(Funcion::class, 'idSeccion');
    }
}
