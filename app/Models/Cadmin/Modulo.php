<?php

namespace App\Models\Cadmin;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    protected $table = 'ad_modulos';
    protected $primaryKey = 'idModulo';
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';

    // protected $casts = [
    //     'idModulo' => 'string',
    // ];

     public function roles()
    {
        return $this->hasMany('App\Models\Cadmin\Role', 'idModulo', 'idModulo');
    }
}
