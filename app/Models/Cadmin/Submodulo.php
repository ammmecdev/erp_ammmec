<?php

namespace App\Models\Cadmin;

use Illuminate\Database\Eloquent\Model;

class Submodulo extends Model
{
    protected $table = 'ad_submodulos';
    protected $primaryKey = 'idSubmodulo';
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';

    //  protected $casts = [
    //     'idSubmodulo' => 'string',
    // ];
}
