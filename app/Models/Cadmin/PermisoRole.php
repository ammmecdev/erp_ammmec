<?php

namespace App\Models\Cadmin;

use Illuminate\Database\Eloquent\Model;

class PermisoRole extends Model
{
    protected $table = 'ad_permisos_roles';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function roles()
    {
        return $this->belongsTo('App\Models\Cadmin\Role', 'idRole');
    }

    public function funciones()
    {
        return $this->belongsTo('App\Models\Cadmin\Funcion', 'idFuncion');
    }
}
