<?php

namespace App\Models\Cadmin;

use Illuminate\Database\Eloquent\Model;

class UsuarioRole extends Model
{
    protected $table = 'ad_usuarios_roles';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function role()
    {
        return $this->belongsTo('App\Models\Cadmin\Role', 'idRole', 'idRole');
    }

    public function usuario()
    {
        return $this->belongsTo('App\Usuario', 'idUsuario', 'idUsuario');
    }
}
