<?php

namespace App\Models\Cadmin;

use Illuminate\Database\Eloquent\Model;

class CuentasExternas extends Model
{
    protected $table = 'ad_cf_cuentas_externas';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
