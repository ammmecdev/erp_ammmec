<?php

namespace App\Models\Compras\SC;

use Illuminate\Database\Eloquent\Model;
use App\Usuario;

class Autorizacion extends Model
{
    protected $table = 'co_sc_autorizaciones';
    protected $primaryKey = 'idAutorizacion';
    public $timestamps = false;

    protected $appends = ['nombre_usuario'];

    public function getNombreUsuarioAttribute()
    {
        return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'idUsuario');
    }
}
