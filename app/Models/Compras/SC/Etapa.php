<?php

namespace App\Models\Compras\SC;

use Illuminate\Database\Eloquent\Model;

class Etapa extends Model
{
    protected $table = 'co_sc_etapas';
    protected $primaryKey = 'idEtapa';
    public $timestamps = false;
}
