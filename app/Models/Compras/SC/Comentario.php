<?php

namespace App\Models\Compras\SC;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = 'co_sc_comentarios';
    protected $primaryKey = 'idComentario';
    public $timestamps = true;
}
