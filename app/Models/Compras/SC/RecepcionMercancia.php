<?php

namespace App\Models\Compras\SC;

use Illuminate\Database\Eloquent\Model;

class RecepcionMercancia extends Model
{
    protected $table = 'co_sc_recepcion_mercancia';
    protected $primaryKey = 'id';
    public $timestamps = true;
}
