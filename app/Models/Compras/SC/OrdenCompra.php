<?php

namespace App\Models\Compras\SC;

use Illuminate\Database\Eloquent\Model;
use App\Models\Compras\Configuracion\Proveedor;
use App\Models\Almacen\SS\Articulo;

class OrdenCompra extends Model
{
    protected $table = 'co_sc_orden_compra';
    protected $primaryKey = 'idOrdenCompra';
    public $timestamps = true;

    protected $appends = ['nombre_proveedor', 'dias_credito', 'razon_social_proveedor', 'rfc_proveedor'];

    public function getNombreProveedorAttribute()
    {
        return $this->proveedor()->first(['nombre'])->nombre;
    }
    
    public function getRazonSocialProveedorAttribute()
    {
        return $this->proveedor()->first(['razonSocial'])->razonSocial;
    }

    public function getRfcProveedorAttribute()
    {
        return $this->proveedor()->first(['rfc'])->rfc;
    }

    public function getDiasCreditoAttribute()
    {
        return $this->proveedor()->first(['diasCredito'])->diasCredito;
    }

    public function getRazonSocialAttribute()
    {
        return $this->proveedor()->first(['razonSocial'])->razonSocial;
    }

    public function proveedor() 
    {
        return $this->belongsTo(Proveedor::class, 'idProveedor');
    }

    public function articulos()
    {
        return $this->hasMany(Articulo::class, 'idOrdenCompra');
    }

    // public function recepcionMercancias()
    // {
    //     return $this->hasMany(RecepcionMercancia::class, 'idOrdenCompra');
    // }

}
