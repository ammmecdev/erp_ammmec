<?php

namespace App\Models\Compras\SC;

use Illuminate\Database\Eloquent\Model;

class RecepcionArticulo extends Model
{
    protected $table = 'co_sc_recepcion_articulos';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
