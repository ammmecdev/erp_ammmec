<?php

namespace App\Models\Compras\Configuracion;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'co_proveedores';
    protected $primaryKey = 'idProveedor';
    public $timestamps = true;
}
