<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;

class Embudo extends Model
{
    protected $table = 'vt_embudos';
    protected $primaryKey = 'idEmbudo';
    public $timestamps = false;

    //ELIINAR ESTE USUARIO?
    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'idUsuario');
    }
    public function etapas()
    {	
    	return $this->hasMany(Etapa::class, 'idEmbudo')->orderBy('orden');
    }
}
