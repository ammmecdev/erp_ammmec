<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;
use App\UnidadNegocio;

class Proyeccion extends Model
{
    protected $table = 'vt_proyecciones';
    protected $primaryKey = 'idProyeccion';
    public $timestamps = false;

    protected $appends = ['unidad_negocio'];

    public function getUnidadNegocioAttribute()
    {
        return $this->unidadNegocio()->first(['unidadNegocio'])->unidadNegocio;
    }

    public function unidadNegocio()
    {
        return $this->belongsTo(UnidadNegocio::class, 'idUnidadNegocio');
    }
}
