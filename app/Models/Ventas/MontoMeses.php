<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;

class MontoMeses extends Model
{
    protected $table = 'vt_presupuestos_monto_meses';
    public $timestamps = false;
}
