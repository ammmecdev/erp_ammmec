<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;
use App\Negocio;

class Presupuesto extends Model
{
    protected $table = 'vt_presupuestos';
    protected $primaryKey = 'idPresupuesto';
    public $timestamps = false;

    protected $appends = ['clave_negocio', 'titulo_negocio'];

    public function getClaveNegocioAttribute()
	{
		return $this->negocio()->first(['claveNegocio'])->claveNegocio;
	}

	public function getTituloNegocioAttribute()
	{
		return $this->negocio()->first(['tituloNegocio'])->tituloNegocio;
	}

	public function negocio()
	{
		return $this->belongsTo(Negocio::class, 'idNegocio');
	}
}
