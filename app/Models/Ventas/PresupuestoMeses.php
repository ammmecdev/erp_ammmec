<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;

use App\Negocio;

class PresupuestoMeses extends Model
{
    protected $table = 'vt_presupuestos_meses';
    protected $primaryKey = 'idPreMes';
    public $timestamps = false;

    protected $appends = ['id_negocio', 'clave_negocio', 'titulo_negocio', 'id_embudo', 'id_unidad_negocio', 'id_embudo_pre'];

    public function getIdNegocioAttribute()
	{
		return $this->presupuesto()->first(['idNegocio'])->idNegocio;
	}

	public function getIdEmbudoPreAttribute()
	{
		return $this->presupuesto()->first(['idEmbudo'])->idEmbudo;
	}

	public function getClaveNegocioAttribute()
	{
		return $this->negocio()->first(['claveNegocio'])->claveNegocio;
	}

	public function getTituloNegocioAttribute()
	{
		return $this->negocio()->first(['tituloNegocio'])->tituloNegocio;
	}

	public function getIdUnidadNegocioAttribute()
	{
		return $this->negocio()->first(['idUnidadNegocio'])->idUnidadNegocio;
	}

	public function getIdEmbudoAttribute()
	{
		return $this->negocio()->first(['idEmbudo'])->idEmbudo;
	}

	public function presupuesto()
	{
		return $this->belongsTo(Presupuesto::class, 'idPresupuesto');
	}

	public function negocio()
	{
		return $this->belongsTo(Negocio::class, 'idNegocioMensual');
	}
}
