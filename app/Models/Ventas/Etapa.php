<?php

namespace App\Models\Ventas;

use Illuminate\Database\Eloquent\Model;

class Etapa extends Model
{
   	protected $table = 'vt_etapas';
    protected $primaryKey = 'idEtapa';
    public $timestamps = false;

    public function embudo()
    {
        return $this->belongsTo(Embudo::class,'idEmbudo');
    }

    public function negocios()
    {
    	return $this->hasMany(Negocio::class, 'idEtapa');
    }
}
