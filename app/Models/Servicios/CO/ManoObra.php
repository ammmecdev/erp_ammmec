<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;

class ManoObra extends Model
{
    protected $table = 'op_co_mano_obra';
    protected $primaryKey = 'idManoObra';
    public $timestamps = false;
}
