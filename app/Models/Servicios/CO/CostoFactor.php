<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;

class CostoFactor extends Model
{
    protected $table = 'op_co_factores';
    protected $primaryKey = 'idFactor';
    public $timestamps = false;
}
