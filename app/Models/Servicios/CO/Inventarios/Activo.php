<?php

namespace App\Models\Servicios\CO\Inventarios;

use Illuminate\Database\Eloquent\Model;

class Activo extends Model
{
    protected $table = 'op_co_inventario_activos';
    protected $primaryKey = 'idActivo';
    public $timestamps = false;
}
