<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;

class GastoOperacion extends Model
{
    protected $table = 'op_co_gastos_operacion';
    protected $primaryKey = 'idGastoOperacion';
    public $timestamps = false;
}
