<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;

class GastoEspecial extends Model
{
    protected $table = 'op_co_gastos_especiales';
    protected $primaryKey = 'idGastoEspecial';
    public $timestamps = false;
}
