<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;
use App\Negocio;

class Costo extends Model
{
    protected $table = 'op_co_costos';
    protected $primaryKey = 'idCosto';

    public function herramientaEquipo()
    {   
        return $this->hasOne(HerramientaEquipo::class, 'idCosto');
    }

    public function manoObra()
    {   
        return $this->hasOne(ManoObra::class, 'idCosto');
    }

    public function gastosEspeciales()
    {
         return $this->hasMany(GastoEspecial::class, 'idCosto');
    }

    public function gastosOperacion()
    {
         return $this->hasMany(GastoOperacion::class, 'idCosto');
    }

    public function materialesInsumos()
    {
         return $this->hasMany(MaterialInsumo::class, 'idCosto');
    }

    public function observaciones()
    {
         return $this->hasMany(CostoObservacion::class, 'idCosto');
    }

    public function factores()
    {   
        return $this->hasOne(CostoFactor::class, 'idCosto');
    }

    public function seguridad()
    {   
        return $this->hasOne(CostoSeguridad::class, 'idCosto');
    }
}
