<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;
use App\User;

class CostoObservacion extends Model
{
    protected $table = 'op_co_observaciones';
    protected $primaryKey = 'idObservacion';
    public $timestamps = false;

    protected $appends = ['ruta'];

    public function getRutaAttribute()
    {
        return $this->user()->first(['ruta'])->ruta;
    }

    public function user()
    {
       return $this->belongsTo(User::class, 'idUsuario');
    }
}
