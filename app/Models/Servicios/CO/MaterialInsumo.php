<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;

class MaterialInsumo extends Model
{
    protected $table = 'op_co_material_insumos';
    protected $primaryKey = 'idMaterial';
    public $timestamps = false;
}
