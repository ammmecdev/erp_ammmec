<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;

class CostoSeguridad extends Model
{
    protected $table = 'op_co_seguridad';
    protected $primaryKey = 'idSeguridad';
    public $timestamps = false;
}
