<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;

class CostoEtapa extends Model
{
    protected $table = 'op_co_conf_etapas';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
