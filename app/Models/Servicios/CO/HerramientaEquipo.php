<?php

namespace App\Models\Servicios\CO;

use Illuminate\Database\Eloquent\Model;

class HerramientaEquipo extends Model
{
    protected $table = 'op_co_herramienta_equipo';
    protected $primaryKey = 'idHerramienta';
    public $timestamps = false;
}
