<?php

namespace App\Models\Servicios\RA;

use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    protected $table = 'op_ra_actividades';
    protected $primaryKey = 'idActividad';

    public function empleado(){
        return $this->belongsTo(Empleado::class, 'idEmpleado');
    }

    public function semana()
    {
    	return $this->belongsTo(Semana::class, 'idSemana');
    }
}
