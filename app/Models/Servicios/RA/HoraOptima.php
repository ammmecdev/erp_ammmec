<?php

namespace App\Models\Servicios\RA;

use Illuminate\Database\Eloquent\Model;

class HoraOptima extends Model
{
    protected $table = 'op_ra_ind_horas_optimas';
    public $timestamps = false;  
}
