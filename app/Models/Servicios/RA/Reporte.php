<?php

namespace App\Models\Servicios\RA;

use Illuminate\Database\Eloquent\Model;

class Reporte extends Model
{
    protected $table = 'op_ra_reportes';
    protected $primaryKey = 'idReporte';
}
