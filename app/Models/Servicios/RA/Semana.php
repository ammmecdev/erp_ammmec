<?php

namespace App\Models\Servicios\RA;

use Illuminate\Database\Eloquent\Model;

class Semana extends Model
{
    protected $table = 'op_ra_semanas';
    protected $primaryKey = 'idSemana';
   	public $timestamps = false;  
}
