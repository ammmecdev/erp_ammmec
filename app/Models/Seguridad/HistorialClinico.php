<?php

namespace App\Models\Seguridad;

use Illuminate\Database\Eloquent\Model;

class HistorialClinico extends Model
{
    protected $table = 'historial_clinico';
    protected $primaryKey = 'id';
    public $timestamps = false;
}
