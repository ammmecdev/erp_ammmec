<?php

namespace App\Models\TalentoHumano;

use Illuminate\Database\Eloquent\Model;

class Departamento extends Model
{
    protected $table = 'rh_departamentos';
    protected $primaryKey = 'idDepartamento';

}
