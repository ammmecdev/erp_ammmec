<?php

namespace App\Models\TalentoHumano\VP;

use Illuminate\Database\Eloquent\Model;
use App\Usuario;
use App\Models\TalentoHumano\VP\Etapa;
use App\Models\TalentoHumano\VP\Comentario;

class Solicitud extends Model
{
    protected $table = 'rh_vp_solicitudes';
    protected $primaryKey = 'idSolicitud';
    public $timestamps = true;

    protected $appends = ['nombre_etapa'];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'idUsuario');
    }
    
    public function getNombreEtapaAttribute()
    {
        return $this->etapa()->first(['nombre'])->nombre;
    }

    public function etapa() 
    {
        return $this->belongsTo(Etapa::class, 'idEtapa', 'idEtapa');
    }
}
