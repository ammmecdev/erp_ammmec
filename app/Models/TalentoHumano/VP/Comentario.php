<?php

namespace App\Models\TalentoHumano\VP;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model
{
    protected $table = 'rh_vp_comentarios';
    protected $primaryKey = 'idComentario';
    public $timestamps = true;
}
