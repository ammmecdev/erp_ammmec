<?php

namespace App\Models\TalentoHumano\VP;

use Illuminate\Database\Eloquent\Model;

class VacacionesPendientes extends Model
{
    protected $table = 'rh_vacaciones_pendientes';
    protected $primaryKey = 'noEmpleado';
    public $timestamps = false;
}
