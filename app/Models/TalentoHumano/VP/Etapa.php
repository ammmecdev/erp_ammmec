<?php

namespace App\Models\TalentoHumano\VP;

use Illuminate\Database\Eloquent\Model;

class Etapa extends Model
{
    protected $table = 'rh_vp_etapas';
    protected $primaryKey = 'idEtapa';
    public $timestamps = false;
}
