<?php

namespace App\Models\TalentoHumano;

use Illuminate\Database\Eloquent\Model;

class MotivoAusencia extends Model
{
    protected $table = 'rh_vp_motivos_ausencia';
    protected $primaryKey = 'idMotivoAusencia';
    public $timestamps = false;
}
