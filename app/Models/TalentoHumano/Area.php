<?php

namespace App\Models\TalentoHumano;

use App\Models\TalentoHumano\Puesto;
use Illuminate\Database\Eloquent\Model;
use App\Models\Calidad\GC\Proceso;
use App\Models\Calidad\GC\ManualDepartamental;

class Area extends Model
{
    protected $table = 'rh_areas';
   	protected $primaryKey = 'idArea';


    // PENDIENTES DE REVISAR PARA EL MÓDULO DE CALIDAD, SE CAMBIO AREAS POR DEPARTAMENTOS
   	// public function procesos()
    // {	
    // 	return $this->hasMany(Proceso::class, 'idArea');
    // }

    // public function manual()
    // {
    // 	return $this->belongsTo(ManualDepartamental::class, 'idArea');
    // }

    // public function puestos()
    // {
    // 	return $this->hasMany(Puesto::class, 'idArea');
    // }

    // public function admin_users()
    // {
    //     return $this->hasMany(Usuario::class)->where('is_admin', '1');
    // } 
}
