<?php

namespace App\Models\TalentoHumano;

use Illuminate\Database\Eloquent\Model;

class DiaFestivo extends Model
{
    protected $table = 'rh_vp_dias_festivos';
    protected $primaryKey = 'idDiaFestivo';
    public $timestamps = false;

    
}
