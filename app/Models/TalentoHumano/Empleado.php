<?php

namespace App\Models\TalentoHumano;

use Illuminate\Database\Eloquent\Model;
use App\Models\TalentoHumano\Puesto;


class Empleado extends Model
{
    protected $table = 'empleados';
    protected $primaryKey = 'noEmpleado';
    public $timestamps = false;
    public $incrementing = false;
    protected $keyType = 'string';

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'noEmpleado');
    }

    public function puesto()
    {
        return $this->belongsTo(Puesto::class, 'idPuesto');
    }
}
