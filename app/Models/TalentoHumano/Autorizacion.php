<?php

namespace App\Models\TalentoHumano;
use App\Usuario;

use Illuminate\Database\Eloquent\Model;

class Autorizacion extends Model
{
    protected $table = 'rh_vp_autorizaciones';
    protected $primaryKey = 'idAutorizacion';
    public $timestamps = false;

    protected $appends = ['nombre_usuario'];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'idUsuario');
    }
    
    public function getNombreUsuarioAttribute() 
    {
        return $this->usuario()->first(['nombreUsuario'])->nombreUsuario;
    }
}
