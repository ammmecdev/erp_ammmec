<?php

namespace App\Models\TalentoHumano;

use Illuminate\Database\Eloquent\Model;

class Puesto extends Model
{
    protected $table = 'rh_puestos';
    protected $primaryKey = 'idPuesto';
    public $timestamps = false;

    protected $appends = ['nombre_area', 'nombre_departamento'];

    public function getNombreAreaAttribute()
    {
        return $this->area()->first(['nombre'])->nombre;
    }

    public function getNombreDepartamentoAttribute()
    {
        return $this->departamento()->first(['nombre'])->nombre;
    }

    public function area()
    {
        return $this->belongsTo(Area::class, 'idArea');
    }

    public function departamento()
    {
        return $this->belongsTo(Departamento::class, 'idDepartamento');
    }
}
