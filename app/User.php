<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\TalentoHumano\Empleado;
use App\Models\TalentoHumano\Departamento;


class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'usuarios';
    protected $primaryKey = 'idUsuario';
    protected $appends = ['id_puesto'];

    public function getIdPuestoAttribute() 
    {
        return $this->empleado()->first(['idPuesto'])->idPuesto;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombreUsuario', 'email', 'pass', 'noEmpleado'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pass', 'remember_token',
    ];

    public function getAuthPassword() { 
        return $this->pass; 
    }

    public function empleado()
    {
        return $this->belongsTo(Empleado::class, 'noEmpleado', 'noEmpleado');
    }
}
