<?php

use Faker\Generator as Faker;
use App\Cliente;

$factory->define(Cliente::class, function (Faker $faker) {
    return [
        'nombre' => $faker->sentence(2, false),
        'clave' => strtoupper(str_random(3)),
        'idUsuario' => 3,
        'idSector' => 1, 
    ];
});
