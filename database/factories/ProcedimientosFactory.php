<?php

use Faker\Generator as Faker;
use App\Procedimiento;
use App\Proceso;

$factory->define(Procedimiento::class, function (Faker $faker) {
	$proceso = Proceso::all()->random(1)->first();
    return [
        'nombre' => $faker->sentence(3, false),
        'idProceso' => $proceso->idProceso,
        'codigo' => 'CODIGO-PROCEDIMIENTO'
    ];
});
