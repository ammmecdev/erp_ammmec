<?php

use Faker\Generator as Faker;
use App\ArchivoProcedimiento;
use App\Procedimiento;

$factory->define(ArchivoProcedimiento::class, function (Faker $faker) {
	$procedimiento = Procedimiento::all()->random(1)->first();
    // $categorias = array('procedimiento', 'it', 'formato');
    // $categoria = array_rand($categorias, 1);
    // dd($categoria);
    $tipos = array("P", "IT", "F");
    $tipo = $tipos[array_rand($tipos)];
    return [
        'nombre' => $faker->sentence(4, false),
        'tipo' => $tipo,
        'clave' => 'ABCD',
        'version' => rand(1,20),
        'codigo_completo' => 'CODIGO-COMPLETO',
        'ultima_modificacion' => '2020-02-02',
        'ruta' => '/img/logoammmec.png',
        'formato' => 'xlsx',
        'size' => '6000',
        'idProcedimiento' => $procedimiento->idProcedimiento,
    ];
});
