<?php

use Faker\Generator as Faker;
use App\Proceso;
use App\Area;

$factory->define(Proceso::class, function (Faker $faker) {
	$area = Area::all()->random(1)->first();
    return [
        'nombre' => $faker->sentence(3, false),
        'idArea' => $area->idArea,
        'codigo' => 'CODIGO-PROCESO'
        
    ];
});
