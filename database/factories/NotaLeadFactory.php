<?php

use Faker\Generator as Faker;
use App\NotaLead;
use App\Cliente;
use App\Contacto;

$factory->define(NotaLead::class, function (Faker $faker) {
    $cliente = Cliente::all()->random(1)->first();
    $contacto = Contacto::where('idCliente', $cliente->idCliente)->first();
    return [
        'titulo' => 'venta zacatecas',
        'idCliente'=>$cliente->idCliente,
       'idContacto'=>$contacto->idContacto,
        'idUsuario'=>1
    ];
});
