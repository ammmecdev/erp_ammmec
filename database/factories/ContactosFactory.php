<?php

use Faker\Generator as Faker;
use App\Cliente;
use App\Contacto;

$factory->define(Contacto::class, function (Faker $faker) {
    $cliente = Cliente::all()->random(1)->first();
    return [
        'idCliente' => $cliente->idCliente,
        'idUsuario' => 3,
        'nombre' => $faker->name, // secret
        'email'=>$faker->email,
    ];
});
