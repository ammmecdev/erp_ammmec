<?php

use Faker\Generator as Faker;

use App\Negocio;
use App\Cliente;
use App\Contacto;
use App\Models\Ventas\Etapa;
use App\Equipo;
use App\Servicio;
use App\UnidadNegocio;
use App\Causa;

$factory->define(Negocio::class, function (Faker $faker) {

    $cliente = Cliente::all()->random(1)->first();
    $contacto = Contacto::where('idCliente', $cliente->idCliente)->first();
    $etapa = Etapa::all()->random(1)->first();
    $equipo = Equipo::all()->random(1)->first();
    $servicio = Servicio::all()->random(1)->first();
    $unidad = UnidadNegocio::all()->random(1)->first();
    $causa = Causa::all()->random(1)->first();

    $consecutivo = Negocio::where('claveCliente', $cliente->clave)->max('claveConsecutivo');
     if ($consecutivo == null)
            $consecutivo = str_pad(1, 4, "0", STR_PAD_LEFT);
    else
        $consecutivo = str_pad(++$consecutivo, 4, "0", STR_PAD_LEFT);

    $claveNegocio = $cliente->clave . '-' . $consecutivo . '-' . $unidad->claveUnidadNegocio . '-' . $equipo->clave . '-' . $servicio->claveServicio;

    return [
        'idUsuario' => 3,
        'idEtapa' => $etapa->idEtapa,
        'idCliente' => $cliente->idCliente,
        'idContacto' => $contacto->idContacto,
        'idEmbudo' => $etapa->idEmbudo,
        'idEquipo' => $equipo->idEquipo,
        'tituloNegocio' => $faker->sentence(4, false),
        'claveCliente' => $cliente->clave,
        'claveConsecutivo' => $consecutivo,
        'claveServicio' => $servicio->claveServicio,
        'valorNegocio' => 0,
        'tipoMoneda' => 'MXN',
        'ponderacion' => rand(1,3),
        'estimacion' => 0,
        'claveNegocio' => $claveNegocio,
        'status' => rand(0,2),
        'idUnidadNegocio' => $unidad->idUnidadNegocio,
        'idCausa' => $causa->idcausasPerdida
    ];
});
