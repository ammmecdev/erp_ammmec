<?php

use Faker\Generator as Faker;
use App\Cliente;
use App\Negocio;
use App\Actividad;
use Carbon\Carbon;

$factory->define(Actividad::class, function (Faker $faker) {

$negocio = Negocio::all()->random(1)->first();
 $cliente = Cliente::where('idCliente', $negocio->idCliente)->first();

    return [
            'idUsuario' => 1,
            'idNegocio' => $negocio->idNegocio,
            'idCliente' => $cliente->idCliente,
            'tipo' => 'Llamada',
            'notas' => $faker->sentence(4, false),
            'completado' => 0,
            'fechaActividad' => $date = Carbon::now(),
            'nombrePersona' => $faker->sentence(2, false)
    ];
});
