<?php

use Illuminate\Database\Seeder;
use App\NotaLead;

class NotaLeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(NotaLead::class,7)->create();
    }
}
