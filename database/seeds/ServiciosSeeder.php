<?php

use Illuminate\Database\Seeder;
use App\Servicio;

class ServiciosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Servicio::create([
            'idServicio' => 1,
            'claveServicio' => '01',
            'servicio' => 'Análisis de Vibraciones'
        ]);

        Servicio::create([
            'idServicio' => 2,
            'claveServicio' => '02',
            'servicio' => 'Pruebas No Destructivas'
        ]);

        Servicio::create([
            'idServicio' => 3,
            'claveServicio' => '03',
            'servicio' => 'Ultrasonido Pasivo'
        ]);

        Servicio::create([
            'idServicio' => 4,
            'claveServicio' => '04',
            'servicio' => 'Pruebas Eléctricas'
        ]);

        Servicio::create([
            'idServicio' => 5,
            'claveServicio' => '05',
            'servicio' => 'Alineación'
        ]);

        Servicio::create([
            'idServicio' => 6,
            'claveServicio' => '06',
            'servicio' => 'Cambio'
        ]);

        Servicio::create([
            'idServicio' => 7,
            'claveServicio' => '07',
            'servicio' => 'Balanceo'
        ]);

        Servicio::create([
            'idServicio' => 8,
            'claveServicio' => '08',
            'servicio' => 'Mantenimiento Anual'
        ]);

        Servicio::create([
            'idServicio' => 9,
            'claveServicio' => '09',
            'servicio' => 'Instalación'
        ]);

        Servicio::create([
            'idServicio' => 10,
            'claveServicio' => '10',
            'servicio' => 'Fabricación'
        ]);

        Servicio::create([
            'idServicio' => 11,
            'claveServicio' => '11',
            'servicio' => 'Reparación'
        ]);

        Servicio::create([
            'idServicio' => 12,
            'claveServicio' => '12',
            'servicio' => 'Modificación'
        ]);

        Servicio::create([
            'idServicio' => 13,
            'claveServicio' => '13',
            'servicio' => 'Maquinado'
        ]);

        Servicio::create([
            'idServicio' => 14,
            'claveServicio' => '14',
            'servicio' => 'Suministro'
        ]);

        Servicio::create([
            'idServicio' => 15,
            'claveServicio' => '15',
            'servicio' => 'Montaje'
        ]);

        Servicio::create([
            'idServicio' => 16,
            'claveServicio' => '16',
            'servicio' => 'Asesoría'
        ]);

        Servicio::create([
            'idServicio' => 17,
            'claveServicio' => '17',
            'servicio' => 'Inspección'
        ]);

        Servicio::create([
            'idServicio' => 18,
            'claveServicio' => '18',
            'servicio' => 'Capacitación'
        ]);

        Servicio::create([
            'idServicio' => 19,
            'claveServicio' => '19',
            'servicio' => 'Comercialización'
        ]);

		Servicio::create([
            'idServicio' => 20,
            'claveServicio' => '20',
            'servicio' => 'Tomografía Infrarroja'
        ]);

        Servicio::create([
            'idServicio' => 21,
            'claveServicio' => '21',
            'servicio' => 'Diseño de Planes de MTTO (RCM-R)'
        ]);

        Servicio::create([
            'idServicio' => 22,
            'claveServicio' => '22',
            'servicio' => 'Mano obra'
        ]);

        Servicio::create([
            'idServicio' => 23,
            'claveServicio' => '23',
            'servicio' => 'Renta de equipo'
        ]);

        Servicio::create([
            'idServicio' => 24,
            'claveServicio' => '24',
            'servicio' => 'Rehabilitación'
        ]);

        Servicio::create([
            'idServicio' => 25,
            'claveServicio' => '25',
            'servicio' => 'Giro'
        ]);
    }
}
