<?php

use Illuminate\Database\Seeder;
use App\Actividad;

class ActividadesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(Actividad::class, 10)->create();
    }
}
