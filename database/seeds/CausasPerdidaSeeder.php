<?php

use Illuminate\Database\Seeder;
use App\Causa;

class CausasPerdidaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Causa::create([
            'idcausasPerdida' => 1,
            'nombreCausa' => 'S/C'
        ]);

        Causa::create([
            'idcausasPerdida' => 2,
            'nombreCausa' => 'Precio'
        ]);

        Causa::create([
            'idcausasPerdida' => 3,
            'nombreCausa' => 'El cliente no lo requiere'
        ]);

        Causa::create([
            'idcausasPerdida' => 4,
            'nombreCausa' => 'Otro proveedor'
        ]);

        Causa::create([
            'idcausasPerdida' => 5,
            'nombreCausa' => 'Mucho tiempo entrega'
        ]);

        Causa::create([
            'idcausasPerdida' => 6,
            'nombreCausa' => 'Disponibilidad'
        ]);

        Causa::create([
            'idcausasPerdida' => 7,
            'nombreCausa' => 'No cotizamos a tiempo'
        ]);

        Causa::create([
            'idcausasPerdida' => 8,
            'nombreCausa' => 'No realizamos ese servicio'
        ]);

        Causa::create([
            'idcausasPerdida' => 9,
            'nombreCausa' => 'Lo realizara el cliente'
        ]);

        Causa::create([
            'idcausasPerdida' => 10,
            'nombreCausa' => 'Cambió de número de cuenta'
        ]);

        Causa::create([
            'idcausasPerdida' => 11,
            'nombreCausa' => 'Alcance incompleto'
        ]);

        Causa::create([
            'idcausasPerdida' => 12,
            'nombreCausa' => 'Falta de personal'
        ]);

        Causa::create([
            'idcausasPerdida' => 13,
            'nombreCausa' => 'Se realizará otro servicio'
        ]);
    }
}
