<?php

use Illuminate\Database\Seeder;
use App\Procedimiento;

class ProcedimientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Procedimiento::class, 100)->create();
    }
}
