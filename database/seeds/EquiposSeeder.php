<?php

use Illuminate\Database\Seeder;
use App\Equipo;

class EquiposSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Equipo::create([
            'idEquipo' => 1,
            'nombreEquipo' => 'Sin equipo asignado',
            'clave' => 'S/E'
        ]); 

        Equipo::create([
            'idEquipo' => 2,
            'nombreEquipo' => 'Molino',
            'clave' => 'MO'
        ]); 

        Equipo::create([
            'idEquipo' => 3,
            'nombreEquipo' => 'Ventilador',
            'clave' => 'VE'
        ]); 

        Equipo::create([
            'idEquipo' => 4,
            'nombreEquipo' => 'Bomba',
            'clave' => 'BO'
        ]); 

        Equipo::create([
            'idEquipo' => 5,
            'nombreEquipo' => 'Equipo auxiliar',
            'clave' => 'EA'
        ]); 

        Equipo::create([
            'idEquipo' => 6,
            'nombreEquipo' => 'Quebradora',
            'clave' => 'QU'
        ]); 

        Equipo::create([
            'idEquipo' => 7,
            'nombreEquipo' => 'Malacate',
            'clave' => 'MA'
        ]); 

        Equipo::create([
            'idEquipo' => 8,
            'nombreEquipo' => 'Sistema de lubricación',
            'clave' => 'SL'
        ]); 
    }
}
