<?php

use Illuminate\Database\Seeder;
use App\Models\Ventas\Etapa;

class EtapasVentasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'Leads',
            'probabilidad'=>0,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>1    
        ]);

        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'Solicitud de Costo Operativo',
            'contenido'=>'',
            'probabilidad'=>0,
            'inactividad'=>3,
            'consecutivo'=>1,
            'orden'=>2    
        ]);

        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'Cotización',
            'probabilidad'=>25,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>3    
        ]);

        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'Autorización',
            'contenido'=>'',
            'probabilidad'=>50,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>4    
        ]);

        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'pedido',
            'probabilidad'=>75,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>5   
        ]);

        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'Realizándose',
            'probabilidad'=>100,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>6   
        ]);

        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'Reporte',
            'contenido'=>'',
            'probabilidad'=>100,
            'inactividad'=>3,
            'consecutivo'=>1,
            'orden'=>7   
        ]);

        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'Facturación',
            'probabilidad'=>100,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>8   
        ]);

        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'Pagado',
            'probabilidad'=>100,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>9   
        ]);

        Etapa::create([
            'idEmbudo'=>1,
            'nombreEtapa'=>'Perdido',
            'probabilidad'=>0,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>10   
        ]);

        Etapa::create([
            'idEmbudo'=>2,
            'nombreEtapa'=>'Leads',
            'probabilidad'=>0,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>1   
        ]);

        Etapa::create([
            'idEmbudo'=>2,
            'nombreEtapa'=>'Seguimiento',
            'probabilidad'=>0,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>2   
        ]);

        Etapa::create([
            'idEmbudo'=>2,
            'nombreEtapa'=>'Cotización',
            'probabilidad'=>10,
            'inactividad'=>8,
            'consecutivo'=>1,
            'orden'=>4   
        ]);

        Etapa::create([
            'idEmbudo'=>2,
            'nombreEtapa'=>'Autroización',
            'contenido'=>'76,487',
            'probabilidad'=>25,
            'inactividad'=>15,
            'consecutivo'=>1,
            'orden'=> 5  
        ]);

        Etapa::create([
            'idEmbudo'=>2,
            'nombreEtapa'=>'Pedido',
            'probabilidad'=>75,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>6   
        ]);

        Etapa::create([
            'idEmbudo'=>2,
            'nombreEtapa'=>'Realización',
            'contenido'=>'278,416,467',
            'probabilidad'=>100,
            'inactividad'=>0,
            'consecutivo'=>1,
            'orden'=>7   
        ]);
    }
}
