<?php

use Illuminate\Database\Seeder;
use App\Models\Ventas\Embudo;

class EmbudosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Embudo::create([
           'idEmbudo' => 1,
           'nombre' => 'AMMMEC 2018'
        ]);
        Embudo::create([
           'idEmbudo' => 2,
           'nombre' => 'AMMMEC 2019'
        ]);
        Embudo::create([
           'idEmbudo' => 4,
           'nombre' => 'AMMMEC 2020'
        ]);
    }
}
