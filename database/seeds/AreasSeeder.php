<?php

use Illuminate\Database\Seeder;
use App\Area;

class AreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Area::create([
            'idArea' => 1,
            'nombre' => 'Dirección',
            'codigo' => 'DSI'
        ]);
         
    	Area::create([
    		'idArea' => 2,
    		'nombre' => 'Sistemas',
            'codigo' => 'DSI'
    	]);

    	Area::create([
    		'idArea' => 3,
    		'nombre' => 'Finanzas',
            'codigo' => 'DSI'
    	]);

    	Area::create([
    		'idArea' => 4,
    		'nombre' => 'Compras',
            'codigo' => 'DSI'
    	]);

    	Area::create([
    		'idArea' => 5,
    		'nombre' => 'Conservación',
            'codigo' => 'DSI'
    	]);

        Area::create([
            'idArea' => 6,
            'nombre' => 'Suministros',
            'codigo' => 'DSI'
        ]);

    	Area::create([
    		'idArea' => 7,
    		'nombre' => 'Ventas',
            'codigo' => 'DSI'
    	]);

    	Area::create([
    		'idArea' => 8,
    		'nombre' => 'Operaciones',
            'codigo' => 'DSI'
    	]);

        Area::create([
            'idArea' => 9,
            'nombre' => 'Recursos Humanos',
            'codigo' => 'DSI'
        ]);

        Area::create([
            'idArea' => 10,
            'nombre' => 'Seguridad',
            'codigo' => 'DSI'
        ]);

        Area::create([
            'idArea' => 11,
            'nombre' => 'Calidad',
            'codigo' => 'DSI'
        ]);

        Area::create([
            'idArea' => 12,
            'nombre' => 'Mercadotecnia',
            'codigo' => 'DSI'
        ]);

        Area::create([
            'idArea' => 13,
            'nombre' => 'Almacén',
            'codigo' => 'DSI'
        ]);
    }
}
