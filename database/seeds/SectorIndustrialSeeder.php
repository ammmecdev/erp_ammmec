<?php

use Illuminate\Database\Seeder;
use App\Sector;

class SectorIndustrialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sector::create([
            'idSector' => 1,
            'nombreSector' => 'Minero',
            'claveSector' => 'MIN'
        ]);
    }
}
