<?php

use Illuminate\Database\Seeder;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         Department::create([
            'idDepartamento' => 1,
            'nombre' => 'Dirección',
            'clave' => 'DSI'
        ]);
         
    	Department::create([
    		'idDepartamento' => 2,
    		'nombre' => 'Sistemas',
            'clave' => 'DSI'
    	]);

    	Department::create([
    		'idDepartamento' => 3,
    		'nombre' => 'Finanzas',
            'clave' => 'DSI'
    	]);

    	Department::create([
    		'idDepartamento' => 4,
    		'nombre' => 'Compras',
            'clave' => 'DSI'
    	]);

    	Department::create([
    		'idDepartamento' => 5,
    		'nombre' => 'Conservación',
            'clave' => 'DSI'
    	]);

        Department::create([
            'idDepartamento' => 6,
            'nombre' => 'Suministros',
            'clave' => 'DSI'
        ]);

    	Department::create([
    		'idDepartamento' => 7,
    		'nombre' => 'Ventas',
            'clave' => 'DSI'
    	]);

    	Department::create([
    		'idDepartamento' => 8,
    		'nombre' => 'Operaciones',
            'clave' => 'DSI'
    	]);

        Department::create([
            'idDepartamento' => 9,
            'nombre' => 'Recursos Humanos',
            'clave' => 'DSI'
        ]);

        Department::create([
            'idDepartamento' => 10,
            'nombre' => 'Seguridad',
            'clave' => 'DSI'
        ]);

        Department::create([
            'idDepartamento' => 11,
            'nombre' => 'Calidad',
            'clave' => 'DSI'
        ]);

        Department::create([
            'idDepartamento' => 12,
            'nombre' => 'Mercadotecnia',
            'clave' => 'DSI'
        ]);

        Department::create([
            'idDepartamento' => 13,
            'nombre' => 'Almacén',
            'clave' => 'DSI'
        ]);
    }
}
