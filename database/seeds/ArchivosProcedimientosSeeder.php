<?php

use Illuminate\Database\Seeder;
use App\ArchivoProcedimiento;

class ArchivosProcedimientosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ArchivoProcedimiento::class, 200)->create();
    }
}
