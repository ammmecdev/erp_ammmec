<?php

use Illuminate\Database\Seeder;
use App\Negocio;

class NegociosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Negocio::class, 50)->create();
    }
}
