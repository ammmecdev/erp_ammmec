<?php

use Illuminate\Database\Seeder;
use App\Proceso;

class ProcesosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Proceso::class, 50)->create();
    }
}
