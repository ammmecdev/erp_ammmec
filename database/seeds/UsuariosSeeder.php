<?php

use Illuminate\Database\Seeder;
use App\Usuario;

class UsuariosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Usuario::create([
            'idUsuario' => 1,
            'nombreUsuario'=>'Alejandro Castro',
            'usuario'=>'acastro',
            'email'=>'sistemas@ammmec.com',
            'pass'=>bcrypt('Admin.DSI'),
            'foto'=>'avatar.png',
            'activo'=>1,
            'idEmpleado'=>2
        ]);

        Usuario::create([
            'idUsuario' => 2,
            'nombreUsuario'=>'Oscar Esaúl Martínez Villagrana',
            'usuario'=>'omartinez',
            'email'=>'oscar_martinez@ammmec.com',
            'pass'=>bcrypt('DOP.31'),
            'foto'=>'avatar.png',
            'activo'=>1,
            'idEmpleado'=>11
        ]);

        Usuario::create([
            'idUsuario' => 4,
            'nombreUsuario'=>'Yunuen Tafolla',
            'usuario'=>'ytafolla',
            'email'=>'yunuen@ammmec.com',
            'pass'=>bcrypt('DVT.4'),
            'foto'=>'avatar.png',
            'activo'=>1,
            'idEmpleado'=>16
        ]);

        Usuario::create([
            'idUsuario' => 5,
            'nombreUsuario'=>'Esquivel Rico Felipe Antonio',
            'usuario'=>'fesquivel',
            'email'=>'fesquivel@ammmec.com',
            'pass'=>bcrypt('DOP.3'),
            'foto'=>'avatar.png',
            'activo'=>1,
            'idEmpleado'=>2
        ]);

        Usuario::create([
            'idUsuario' => 6,
            'nombreUsuario'=>'Felipe Esquivel Hernández',
            'usuario'=>'fehernandez',
            'email'=>'felipe_esquivel@ammmec.com',
            'pass'=>bcrypt('DDG.6'),
            'foto'=>'avatar.png',
            'activo'=>1,
            'idEmpleado'=>2
        ]);

        Usuario::create([
            'idUsuario' => 7,
            'nombreUsuario'=>'Guillermo Murillo',
            'usuario'=>'gmurillo',
            'email'=>'guillermo_murillo@ammmec.com',
            'pass'=>bcrypt('DSE.19'),
            'foto'=>'avatar.png',
            'activo'=>1,
            'idEmpleado'=>8
        ]);

        Usuario::create([
            'idUsuario' => 8,
            'nombreUsuario'=>'Stephanie C. Qualls Portillo',
            'usuario'=>'squalls',
            'email'=>'mercadotecnia_fresnillo@ammmec.com',
            'pass'=>bcrypt('DMK.11'),
            'foto'=>'avatar.png',
            'activo'=>1,
            'idEmpleado'=>3
        ]);
    }
}
