<?php

use Illuminate\Database\Seeder;
use App\UnidadNegocio;

class UnidadNegocioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        UnidadNegocio::create([
            'claveUnidadNegocio'=>'S/UN',
            'unidadNegocio'=>'Sin unidad de negocio'
        ]);

        UnidadNegocio::create([
            'claveUnidadNegocio'=>'CM',
            'unidadNegocio'=>'Campo'
        ]);

        UnidadNegocio::create([
            'claveUnidadNegocio'=>'TM',
            'unidadNegocio'=>'Taller'
        ]);
        UnidadNegocio::create([
            'claveUnidadNegocio'=>'MM',
            'unidadNegocio'=>'Montajes'
        ]);

        UnidadNegocio::create([
            'claveUnidadNegocio'=>'AS',
            'unidadNegocio'=>'Asesoría'
        ]);

    }
}
