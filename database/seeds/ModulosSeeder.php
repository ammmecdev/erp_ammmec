<?php

use Illuminate\Database\Seeder;
use App\Modulo;

class ModulosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Modulo::create([
            'idModulo'=>1,
            'modulo'=>'Administración',
            'idElement'=>'mod_admin'
        ]);

        Modulo::create([
            'idModulo'=>2,
            'modulo'=>'Servicios',
            'idElement'=>'mod_servicios'
        ]);

        Modulo::create([
            'idModulo'=>3,
            'modulo'=>'Ventas',
            'idElement'=>'mod_ventas'
        ]);

        Modulo::create([
            'idModulo'=>4,
            'modulo'=>'Compras',
            'idElement'=>'mod_compras'
        ]);

        Modulo::create([
            'idModulo'=>5,
            'modulo'=>'Almacen',
            'idElement'=>'mod_almacen'
        ]);

        Modulo::create([
            'idModulo'=>6,
            'modulo'=>'Talento humano',
            'idElement'=>'mod_rh'
        ]);

         Modulo::create([
            'idModulo'=>7,
            'modulo'=>'Finanzas',
            'idElement'=>'mod_finanzas'
        ]);
    }
}
