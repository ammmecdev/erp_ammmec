<?php

use Illuminate\Database\Seeder;
use App\Puesto;

class PuestosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Puesto::create([
            'idPuesto'=>1,
            'puesto'=>'Director General',
            'idModulo'=>1,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>2,
            'puesto'=>'Mercadotecnia',
            'idModulo'=>1,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>3,
            'puesto'=>'Subdirección Administrativa',
            'idModulo'=>1,
            'idNivel'=>1
        ]);
        
        Puesto::create([
            'idPuesto'=>4,
            'puesto'=>'Líder de finanzas',
            'idModulo'=>7,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>5,
            'puesto'=>'Coordinandod de Contabilidad',
            'idModulo'=>7,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>6,
            'puesto'=>'Coordinador de Talento Humano',
            'idModulo'=>6,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>7,
            'puesto'=>'Coordinador de Calidad',
            'idModulo'=>1,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>8,
            'puesto'=>'Coordinador de Seguridad',
            'idModulo'=>1,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>9,
            'puesto'=>'Ingeniero de Sistemas',
            'idModulo'=>1,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>10,
            'puesto'=>'Subdirección de Servicio',
            'idModulo'=>2,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>11,
            'puesto'=>'Lider de Servicio',
            'idModulo'=>2,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>12,
            'puesto'=>'Planeador',
            'idModulo'=>2,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>13,
            'puesto'=>'Ingeniero de Servicio en Campo',
            'idModulo'=>2,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>14,
            'puesto'=>'Ingeniero de Centro de Servicio',
            'idModulo'=>2,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>15,
            'puesto'=>'Ingeniero de Servicio de Monitoreo',
            'idModulo'=>2,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>16,
            'puesto'=>'Líder de Ventas',
            'idModulo'=>3,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>17,
            'puesto'=>'Coordinador Administrativo de Ventas',
            'idModulo'=>3,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>18,
            'puesto'=>'Asesor Comercial',
            'idModulo'=>2,
            'idNivel'=>1
        ]);
        
        Puesto::create([
            'idPuesto'=>19,
            'puesto'=>'Líder de Suministros',
            'idModulo'=>2,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>20,
            'puesto'=>'Coordinador de Compras',
            'idModulo'=>4,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>21,
            'puesto'=>'Coordinador de Conservación',
            'idModulo'=>2,
            'idNivel'=>1
        ]);

        Puesto::create([
            'idPuesto'=>22,
            'puesto'=>'Almacen',
            'idModulo'=>2,
            'idNivel'=>1
        ]);
    }
}
