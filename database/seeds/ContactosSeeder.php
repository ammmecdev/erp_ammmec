<?php

use Illuminate\Database\Seeder;
use App\Contacto;

class ContactosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Contacto::class,100)->create();
    }
}
