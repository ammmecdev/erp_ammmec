<?php

use Illuminate\Database\Seeder;
use App\Empleado;

class EmpleadosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Empleado::create([
            'nombre' => 'AMMMEC S.A DE C.V',
            'email' => 'ammmec@ammmec.com',
            'noEmpleado' => '0000',
            'idPuesto' => 1
        ]);

        Empleado::create([
            'nombre' => 'Felipe de Jesús Esquivel Hernández',
            'email' => 'felipe_esquivel@ammmec.com',
            'noEmpleado' => '001-010512',
            'idPuesto' => 1
        ]);

        Empleado::create([
            'nombre' => 'Stephanie C. Qualls Portillo',
            'email' => 'mercadotecnia_fresnillo@ammmec.com',
            'noEmpleado' => '148-310518',
            'idPuesto' => 2
        ]);

        Empleado::create([
            'nombre' => 'Estela Haydde Hernández Salas',
            'email' => 'h_hernandez@ammmec.com',
            'noEmpleado' => '008-280111',
            'idPuesto' => 3
        ]);

        Empleado::create([
            'nombre' => 'Heriberto Sánchez Triana',
            'email' => 'finanzas_fresnillo@ammmec.com',
            'noEmpleado' => '121-020216',
            'idPuesto' => 4
        ]);

        Empleado::create([
            'nombre' => 'Jessica E. Guardado Araiza',
            'email' => 'contabilidad_fresnillo@ammmec.com',
            'noEmpleado' => '136-300117',
            'idPuesto' => 5
        ]);

        Empleado::create([
            'nombre' => 'Mayra G. Ortega Saldaña',
            'email' => 'mayra_ortega@ammmec.com',
            'noEmpleado' => '103-231115',
            'idPuesto' => 6
        ]);

        Empleado::create([
            'nombre' => 'Guillermo Murillo Ramírez',
            'email' => 'guillermo_murillo@ammmec.com',
            'noEmpleado' => '083-120813',
            'idPuesto' => 7
        ]);

        Empleado::create([
            'nombre' => 'Alejandro Castro Saucedo',
            'email' => 'alejandro_castro@ammmec.com',
            'noEmpleado' => '157-170918',
            'idPuesto' => 9
        ]);

        Empleado::create([
            'nombre' => 'Bruno Padilla Guerrero',
            'email' => 'bruno_padilla@ammmec.com',
            'noEmpleado' => '158-170918',
            'idPuesto' => 9
        ]);

        Empleado::create([
            'nombre' => 'Oscar Esaúl Martínez Villagrana',
            'email' => 'oscar_martinez@ammmec.com',
            'noEmpleado' => '0000',
            'idPuesto' => 10
        ]);

         Empleado::create([
            'nombre' => 'Ricardo Alvarado',
            'email' => 'ricardo_alvarado@ammmec.com',
            'noEmpleado' => '000-000-00',
            'idPuesto' => 11
        ]);

        Empleado::create([
            'nombre' => 'Margarita Martínez Hinostroza',
            'email' => 'planeacion_fresnillo@ammmec.com',
            'noEmpleado' => '140-030817',
            'idPuesto' => 12
        ]);

        Empleado::create([
            'nombre' => 'José Carlos Alvarado Caneles',
            'email' => 'servicioencampo@ammmec.com',
            'noEmpleado' => '144-261017',
            'idPuesto' => 13
        ]);

        Empleado::create([
            'nombre' => 'Victor O. Arroyo Ibañez',
            'email' => 'centrodeservicio@ammmec.com',
            'noEmpleado' => '149-180618',
            'idPuesto' => 14
        ]);

        Empleado::create([
            'nombre' => 'Yunuen C. Tafolla Briones',
            'email' => 'yunuen@ammmec.com',
            'noEmpleado' => '127-250816',
            'idPuesto' => 16
        ]);

        Empleado::create([
            'nombre' => 'Felipe Antonio Lomelí Fernández',
            'email' => 'felipe_lomeli@ammmec.com',
            'noEmpleado' => '138-240717',
            'idPuesto' => 17
        ]);

        Empleado::create([
            'nombre' => 'Beatriz Rico Perez',
            'email' => 'suministros_fresnillo@ammmec.com',
            'noEmpleado' => '147-150318',
            'idPuesto' => 19
        ]);

        Empleado::create([
            'nombre' => 'Edgar I Mauricio R.',
            'email' => 'compras_fresnillo@ammmec.com',
            'noEmpleado' => '0000',
            'idPuesto' => 20
        ]);

        Empleado::create([
            'nombre' => 'Eduardo Zamarripa Luján',
            'email' => 'conservacion_fresnillo@ammmec.com',
            'noEmpleado' => '0000',
            'idPuesto' => 21
        ]);

        Empleado::create([
            'nombre' => 'Luis Gerardo Santacruz Pineda',
            'email' => 'almacen_fresnillo@ammmec.com',
            'noEmpleado' => '0000',
            'idPuesto' => 22
        ]);
    }
}
