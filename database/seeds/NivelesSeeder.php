<?php

use Illuminate\Database\Seeder;
use App\Nivel;

class NivelesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       Nivel::create([
            'idNivel' => 1,
            'nivel' => 'Nivel 1 - Superusuario',
            'back_color' => '#337ab7',
            'color' => '#fff'
        ]);

       Nivel::create([
            'idNivel' => 2,
            'nivel' => 'Nivel 2 - Subusuario',
            'back_color' => '#FFF9800',
            'color' => '#fff'
        ]);

       Nivel::create([
            'idNivel' => 3,
            'nivel' => 'Nivel 3 - Administrador',
            'back_color' => '#d9534f',
            'color' => '#fff'
        ]);
       
       Nivel::create([
            'idNivel' => 4,
            'nivel' => 'Nivel 4 - Avanzado',
            'back_color' => '#5cb85c',
            'color' => '#fff'         
        ]); 
            
       Nivel::create([
            'idNivel' => 5,
            'nivel' => 'Nivel 5 - Regular',
            'back_color' => '#efe24c',
            'color' => '#fff'        
        ]);
       
       
       
    }
}
